function tb_menu(formid,id){
    var checked = false;
    var url = '';
    var jml = '';
    var met = '';
    var con = '';
    var div = '';
    var chk = $("#tb_chk" + formid + ":checked").length;
    id = typeof id !== 'undefined' ? id : '';
    if (id == ''){
        var isi = $("#tb_menu" + formid).val();
        $("#tb_menu" + formid + " option").each(function() {
            if ($(this).text().trim() == isi.trim()) {
                url = $(this).attr('url');
                jml = $(this).attr('jml');
                met = $(this).attr('met');
                con = $(this).attr('content');
                div = $(this).attr('div');
            }
        });
    }else{
        url = $('#'+id).attr('url');
        jml = $('#'+id).attr('jml');
        met = $('#'+id).attr('met');
        con = $('#'+id).attr('content');
        div = $('#'+id).attr('div');
    }

    if (url == '') return false;
    if (chk == 0 && jml != '0') {
        jAlert('Maaf, Data belum dipilih.', site_name);
        $("#tb_menu" + formid).val(0);
        return false;
    }
    if ((jml == '1' || jml == '2') && chk > 1) {
        jAlert('Maaf, Data yang bisa diproses hanya 1 (satu).', site_name);
        $("#tb_menu" + formid).val(0);
        return false;
    }
    if (met == "GETAJAX") {
        jConfirm('Proses data terpilih sekarang?', site_name,
            function(r) {
                if (r == true) {
                    if (jml == '0') {
                        call(url,con)
                    }
                    else {
                        var val = $("#tb_chk" + formid + ":checked").val().toLowerCase().split("|");
                        var valdata = "";
                        for (var a = 0; a < val.length; a++) {
                            valdata = valdata + '/' + val[a];
                        }
                        call(url+valdata,con);
                    }
                }
                else {
                    return false;
                }
            });
    }
    else if (met == "ADDAJAX") {
        jConfirm('Proses data terpilih sekarang?', site_name,
        function(r) {
            if (r == true) {
                $('#' + con).slideDown("slow");
                $('#' + con).html('<div style="margin-left: auto; margin-top: auto;"><img src="' + base_url + 'img/loaders/facebook.gif" /> loading...</div>');
                $.ajax({
                    url: url,
                    type : 'POST',
                    data: '&act=add'
                }).done(function(data){
                    $('#' + con).toggle(function() {
                        $('#' + con).html(data);
                        $('#' + con).slideDown("slow"); 
                    });
                });
            } else {
                return false;
            }
        });
    }
    else if (met == 'EDITAJAX') {
        jConfirm('Proses data terpilih sekarang?', site_name,
        function(r) {
            if (r == true) {
                var val = $("#" + formid + " input:checkbox").serialize();
                $('#' + con).slideDown("slow"); 
                $('#' + con).html('<div style="margin-left: auto; margin-top: auto;"><img src="' + base_url + 'img/loaders/facebook.gif" /> loading...</div>');
                $.ajax({
                    url: url,
                    type : 'POST',
                    data: val + '&act=edit'
                }).done(function(data){
                    $('#' + con).toggle(function() {
                        $('#' + con).html(data);
                        $('#' + con).slideDown("slow"); 
                    });
                });
            } else {
                return false;
            }
        });
    }
    else if (met == "DELETEAJAX") {
        jConfirm('Proses data terpilih sekarang?', site_name,
        function(r) {
            if (r == true) {
                var val = $("#" + formid + " input").serialize();
                $.ajax({
                    url: url,
                    type : 'POST',
                    data: val + '&act=delete'
                }).done(function(data){
                    $('#msgConfirm').removeClass();
                    var hdrMsg = '';
                    var isiMsg = '';
                    var arrdata = data.split('|');
                    if (arrdata[0].trim()==='MSG') {
                        arrdata[2] = ReplaceAll(arrdata[2], '`', '|');
                        if(arrdata[2].trim()!=''){call(arrdata[2], con)};
                        if (arrdata[1] === 'OK') {
                            $('#msgConfirm').addClass('notify notify-success');
                            hdrMsg = 'Success';
                            isiMsg = arrdata[3];
                        } else {
                            $('#msgConfirm').addClass('notify notify-error');
                            hdrMsg = 'Error';
                            isiMsg = arrdata[3];
                        }
                    } else {
                        $('#msgConfirm').addClass('notify');
                        hdrMsg = 'Notifikasi';
                        isiMsg = data;
                    }
                    $('#msgConfirm').slideDown("slow");
                    $('#msgConfirm').html('<a onclick="$(\'#msgConfirm\').hide();" class="closex">&times;</a><h5>'+hdrMsg+'</h5><p>'+isiMsg+'</p>');
                });
            } else {
                return false;
            }
        });
    }
    else if (met == "DOWNLOADAJAX") {
        jConfirm('Proses data terpilih sekarang?', site_name,
        function(r) {
            if (r == true) {
                var val = $("#" + formid + " input").serialize();
                $.ajax({
                    url: url,
                    type : 'POST',
                    data: val + '&act=download'
                }).done(function(data){
                    $('#msgConfirm').removeClass();
                    var hdrMsg = '';
                    var isiMsg = '';
                    var arrdata = data.split('|');
                    if (arrdata[0].trim()==='MSG'){
                        arrdata[2] = ReplaceAll(arrdata[2], '`', '|');
                        if (arrdata[1] === 'OK') {
                            $('#msgConfirm').addClass('notify notify-success');
                            hdrMsg = 'Success';
                            isiMsg = arrdata[3];
                            var str = arrdata[2];
                            window.open(str,'Download','scrollbars=yes, resizable=yes,width=10,height=10');
                            
                        } else {
                            $('#msgConfirm').addClass('notify notify-error');
                            hdrMsg = 'Error';
                            isiMsg = arrdata[3];
                        }
                    } else {
                        $('#msgConfirm').addClass('notify');
                        hdrMsg = 'Notifikasi';
                        isiMsg = data;
                    }
                    $('#msgConfirm').slideDown("slow");
                    $('#msgConfirm').html('<a onclick="$(\'#msgConfirm\').hide();" class="closex">&times;</a><h5>'+hdrMsg+'</h5><p>'+isiMsg+'</p>');
                });
            } else {
                return false;
            }
        });
    }
    else if (met == "GETPOP") {
        var valdata = "";
        if(jml != '0'){
            var val = $("#tb_chk" + formid + ":checked").val().toLowerCase().split("|");
            for (var a = 0; a < val.length; a++) {
                valdata = valdata + '/' + val[a];
            }
        }
        var arrCon = con.split('|');
        //alert('tset');
        Dialog(url + valdata, 'msgbox', arrCon[0], arrCon[1], arrCon[2]);
    }
    else if(met == "NEWPOPCETAK"){
        var valdata = "";
        if(jml != '0'){
            var val = $("#tb_chk" + formid + ":checked").val().toLowerCase().split("|");
            for (var a = 0; a < val.length; a++) {
                valdata = valdata + '/' + val[a];
            }
        }
        var arrCon = con.split('|');
        window.open(url + valdata,arrCon[0],"scrollbars=yes, resizable=yes,width="+arrCon[1]+",height="+arrCon[2]);
    }
    else if (met == "MSGBOX") {
        jConfirm('Proses data terpilih sekarang?', site_name,
            function(r) {
                if (r == true) {
                    jloadings();
                    var val = $("#tb_chk" + formid + ":checked").val().toLowerCase().split("|");
                    var valdata = "";
                    for (var a = 0; a < val.length; a++) {
                        valdata = valdata + '/' + val[a];
                    }
                    $.ajax({
                        type: 'POST',
                        url: url + valdata,
                        success: function(data) {
                            Clearjloadings();
                            jAlert(data, site_name);
                        }
                    });
                }else {
                    return false;
                }
            });
    } 
    else if (met == "POST") {
        jConfirm('Proses data terpilih sekarang?', site_name,
                function(r) {
                    if (r == true) {
                        $('#labelload' + formid).fadeIn('Slow');
                        $.ajax({
                            type: 'POST',
                            url: url,
                            data: $('#' + formid).serialize(),
                            success: function(data) {
                                if (data.search("MSG") >= 0) {
                                    arrdata = data.split('MSG#');
                                    arrdata = arrdata[1].split('#');
                                    jAlert(arrdata[0], site_name);
                                    if (arrdata.length > 1)
                                        location.href = arrdata[1];
                                } else {
                                    jAlert('Proses Gagal.', site_name);
                                }
                                $('#labelload' + formid).fadeOut('Slow');
                            }
                        });
                    } else {
                        return false;
                    }
                });
    }
    else if (met == "ADD") { //digunakan oleh header
        jConfirm('Proses data terpilih sekarang?', site_name,
                function(r) {
                    if (r == true) {
                        $('#labelload' + formid).fadeIn('Slow');
                        $.ajax({
                            type: 'POST',
                            url: url,
                            data: 'edit=add',
                            success: function(data) {
                                $("#" + formid + "_form").html(data);
                                $('#labelload' + formid).fadeOut('Slow');
                            }
                        });
                    } else {
                        return false;
                    }
                });
    }
    else if (met == "EDIT") {
        jConfirm('Proses data terpilih sekarang?', site_name,
                function(r) {
                    if (r == true) {
                        var val = $("#" + formid + " input:checkbox").serialize();
                        $('#labelload' + formid).fadeIn('Slow');
                        $.ajax({
                            type: 'POST',
                            url: url,
                            data: val + '&edit=edit',
                            success: function(data) {
                                $("#" + formid + "_form").html(data);
                                $('#labelload' + formid).fadeOut('Slow');
                                $('#notify').fadeOut('Slow');
                            }
                        });
                    } else {
                        return false;
                    }
                });
    }
    else if (met == "DEL") {
        jConfirm('Proses data terpilih sekarang?', site_name,
                function(r) {
                    if (r == true) {
                        var data = $("#" + formid + " input:checkbox").serialize();
                        var val = data;
                        $('#labelload' + formid).fadeIn('Slow');
                        $.ajax({
                            type: 'POST',
                            url: url,
                            data: val + '&edit=del&act=delete',
                            success: function(data) {
                                if (data.search("MSG") >= 0) {
                                    var arrdata = data.split('#');
                                    if (arrdata[1] == "OK") {
                                        $('#labelload' + formid).css('color', 'green');
                                        $('#labelload' + formid).html(arrdata[2]);
                                        $('#' + formid + "_list").load(arrdata[3]);
                                    } else {
                                        $('#labelload' + formid).css('color', 'red');
                                        $('#labelload' + formid).html(arrdata[2]);
                                        jAlert(arrdata[2], site_name);
                                    }
                                } else {
                                    $('#labelload' + formid).css('color', 'red');
                                    $('#labelload' + formid).html('Proses Gagal.');
                                }
                                $('#labelload' + formid).fadeOut('Slow');
                            }
                        });
                    } else {
                        return false;
                    }
                });
    } 
    else if (met == "PROCESS") {
        jConfirm('Proses data terpilih sekarang?', site_name,
                function(r) {
                    if (r == true) {
                        var val = $("#" + formid + " input:checkbox").serialize();
                        $('#labelload' + formid).fadeIn('Slow');
                        $.ajax({
                            type: 'POST',
                            url: url,
                            data: val + '&act=process',
                            beforeSend: function() {
                                jloadings();
                            },
                            complete: function() {
                                Clearjloadings();
                            },
                            success: function(data) {
                                if (data.search("MSG") >= 0) {
                                    arrdata = data.split('#');
                                    if (arrdata[1] == "OK") {
                                        $('#labelload' + formid).css('color', 'green');
                                        $('#labelload' + formid).html(arrdata[2]);
                                        $('#' + div).load(arrdata[3]);
                                    } else {
                                        jAlert(arrdata[2], site_name);
                                    }
                                } else {
                                    jAlert("Proses gagal.", site_name);
                                }
                            }
                        });
                    } else {
                        return false;
                    }
                });
    } 
    else if (met == "COPY") {
        jConfirm('Proses data terpilih sekarang?', site_name,
                function(r) {
                    if (r == true) {
                        $('#labelload' + formid).fadeIn('Slow');
                        var val = $("#tb_chk" + formid + ":checked").val();
                        $.ajax({
                            type: 'POST',
                            url: url,
                            data: "data=" + val,
                            success: function(data) {
                                if (data.search("MSG") >= 0) {
                                    arrdata = data.split('#');
                                    if (arrdata[1] == "OK") {
                                        $('#labelload' + formid).css('color', 'green');
                                        $('#labelload' + formid).html(arrdata[2]);
                                    } else {
                                        $('#labelload' + formid).css('color', 'red');
                                        $('#labelload' + formid).html(arrdata[2]);
                                    }
                                    if (arrdata.length > 3) {
                                        setTimeout(function() {
                                            location.href = arrdata[3];
                                        }, 2000);
                                        return false;
                                    }
                                } else {
                                    $('#labelload' + formid).css('color', 'red');
                                    $('#labelload' + formid).html('Proses Gagal.');
                                }
                            }
                        });
                    } else {
                        return false;
                    }
                });
    }
    $("#tb_menu" + formid).val(0);
    $("#msgConfirm").hide();
    return false;
}
function tb_chkall(formid) {
    $("#" + formid).find(':checkbox').attr('checked', $("#tb_chkall" + formid).attr('checked'));
    $('#newtr').remove();
    if (!$("#tb_chkall" + formid).attr('checked') == true) {
        $("#" + formid + " input:checkbox:not(#tb_chkall" + formid + ")").parent().parent().removeClass("selected");
    } else {
        $("#" + formid + " input:checkbox:not(#tb_chkall" + formid + ")").parent().parent().addClass("selected");
    }
}
function tb_chk() {
    $('input:not(:checked)').parent().parent().removeClass("selected");
    $('input:checked').parent().parent().addClass("selected");
}
function tb_hals(formid, id) {
    form = $("#tb_menu" + formid).attr('formid');
    newhal = $(id).val();
    newhal++;
    redirect_url(newhal, form);
    return false;
}
function td_click(id) {
    $("#detils_bawah").html('<center><img src=\"' + base_url + 'img/_indicator.gif\" alt=\"\" /><br> Please wait ...</center>');
    $.ajax({
        type: 'POST',
        url: $(".tabelajax #bawah").attr('urldetil') + "/" + id,
        data: 'ajax=1',
        success: function(html) {
            $("#detils_bawah").html(html);
        }
    });
}
function redirect_url(newhal, formid) {
    newlocation = $("#" + formid).attr('action') + '/row/' + $("#tb_view").val() + '/page/' + newhal + '/order/' + $("#orderby").val() + '/' + $("#sortby").val();
    if ($("#tb_cari").val() != "")
        newlocation += '/search/' + $("#tb_keycari").val() + '/' + $("#tb_cari").val().replace('/', '');
    location.href = newlocation;
}
function tb_order(formid, divid, data, key, cari) {
    var action = $("#" + formid + " form").attr('action');
    if (!action) {
        var action = $("#" + formid + "hidden_action").val();
    }
    tb_pagging(action, divid, data, key, cari);
}
function tb_go(action, divid, hal, orderby, sortby, input, key, cari) {
    var value = $("#" + input).val();
    var data = "row|" + hal + '|page|' + value + '|order|' + orderby + '|' + sortby + '|';
    tb_pagging(action, divid, data, key, cari);
}
function tb_pagging(action, divid, data, key, cari) {
    var dtid = $("#" + key).find('option:selected').attr('tag');
    if (dtid == 'tag-tanggal') {
        var valueCari = 'Tag-Tanggal;' + $('#' + cari + '_tgl1').val() + ';' + $('#' + cari + '_tgl2').val();
    } else if (dtid == 'tag-tanggal-2field') {
        var valueCari = 'tag-tanggal-2field;' + $('#' + cari + '_tgl1').val() + ';' + $('#' + cari + '_tgl2').val();
    } else if (dtid == 'tag-select') {
        var valueCari = 'Tag-Select;' + $("#" + cari).val();
    } else {
        var valueCari = $("#" + cari).val();
    }
    $.ajax({
        type: 'POST',
        url: action,
        data: 'ajax=1&uri=' + data + 'search|' + $("#" + key).val() + '|' + valueCari + '|',
        beforeSend: function() {
            jloadings();
        },
        complete: function() {
            Clearjloadings();
        },
        success: function(html) {
            $("#" + divid).html(html);
        }
    });
}
function tb_cari(action, divid, hal, orderby, sortby, input, cari, key) {
    var value = $("#" + input).val();
    var data = "row|" + hal + '|page|' + value + '|order|' + orderby + '|' + sortby + '|';
    tb_pagging(action, divid, data, key, cari);
}
function td_pilih(id) {
    var arr = id.split("|");
    var formName = arr[0];//alert(arr[0]);
    var fIndexEdit = arr[1];
    var inputField = arr[2];
    var input = inputField.split(";");
    var val = fIndexEdit.split(";");
    for (var c = 0; c < (input.length) - 1; c++) {
        if (typeof($("#" + input[c]).get(0)) == "undefined") {
            jAlert('<b>ERROR:\n</b>Ada Elemen Form (' + input[c] + ') yang tak terdefinisi.\nMohon periksa script codenya.', site_name);
            return false;
            break;
        }
        var tipe = $("#" + input[c]).get(0).tagName;
        if (tipe === 'INPUT' || tipe === 'SELECT') {
            $("#" + input[c]).val(val[c]);
        } else {
            $("#" + input[c]).html(val[c]);
        }
    }
    closedialog('Dialog-dok');
    $("#" + input[0]).focus();
}
//paging

function pilih_page(urlChange,divId,divChange) {
    //alert(id);die();
    //alert(urlChange);return false;
    closedialog(divId);
    call(urlChange, divChange, '');
    return false;
}

//paging
function tb_search(tipe, inputField, title, formName, width, height, getelement) {
    var getdata = "";
    if (typeof(getelement) != "undefined") {
        var data = getelement.split(";");
        var arr = "";
        for (var a = 0; a < data.length - 1; a++) {
            if ($("#" + formName + " #" + data[a]).val() != "") {
                arr = arr + "" + $("#" + formName + " #" + data[a]).val() + ";";
            }
        }
        if (arr != "")
            getdata = "/" + arr;
    }
    var judul = title.toUpperCase();
    var dataAjax = tipe + "/" + inputField + "/" + formName + getdata;
    Dialog(site_url + '/search/getsearch/' + dataAjax, 'Dialog-dok', judul, width, height);
}
function td_detil_priview(id) {//alert(id);return false;
    var arr = id.split("|");
    var div = arr[0];
    var fIndexKey = arr[1];
    var val = fIndexKey.split(";");
    alert(div);return false;
    /*var val = fIndexEdit.split(";");*/
    if (div == "divmutasi_bb") {
        document.getElementById('frmLaporanMutBB').TANGGAL_AWAL.value = val[0];
        document.getElementById('frmLaporanMutBB').TANGGAL_AKHIR.value = val[1];
        LaporanList('frmLaporanMutBB', 'msg_laporan', 'divLapMutasiBB', 'divListMutasiBB', 'btnExitBB',
                '' + base_url + 'index.php/laporan/daftar_dok/mutasi_bb', 'laporan');
    } else if (div == "divmutasi_bj") {
        document.getElementById('frmLaporanMutBJ').TANGGAL_AWAL.value = val[0];
        document.getElementById('frmLaporanMutBJ').TANGGAL_AKHIR.value = val[1];
        LaporanList('frmLaporanMutBJ', 'msg_laporan', 'divLapMutasiBj', 'divListMutasiBj', 'btnExitBj',
                '' + base_url + 'index.php/laporan/daftar_dok/mutasi_bj', 'laporan');
    } else if (div == "divmutasi_bs") {
        document.getElementById('frmLaporanMutBS').TANGGAL_AWAL.value = val[0];
        document.getElementById('frmLaporanMutBS').TANGGAL_AKHIR.value = val[1];
        LaporanList('frmLaporanMutBS', 'msg_laporan', 'divLapMutasiBS', 'divListMutasiBS', 'btnExitBS',
                '' + base_url + 'index.php/laporan/daftar_dok/mutasi_bs', 'laporan');
    } else if (div == "divmutasi_ms") {
        document.getElementById('frmLaporanMutMS').TANGGAL_AWAL.value = val[0];
        document.getElementById('frmLaporanMutMS').TANGGAL_AKHIR.value = val[1];
        LaporanList('frmLaporanMutMS', 'msg_laporan', 'divLapMutasiMS', 'divListMutasiMS', 'btnExitMS',
                '' + base_url + 'index.php/laporan/daftar_dok/mutasi_bs', 'laporan');
    }
}

function td_detil_priview_bottom(id, thisid) {
    $("tr").removeClass("selected");
    $(thisid).addClass("selected");
    var obj = $(thisid).next().attr("id");
    if (obj == "newtr") {
        $('#newtr').remove();
        $(thisid).removeClass("selected");
    } else {
        if ($(thisid).attr('urldetil')) {
            if ($(thisid).attr('urldetil') != "") {
                $('#newtr').remove();
                var jmltd = $('td', $(thisid)).length;
                if ($(".tabelajax input:checkbox").length > 0) {
                    jmltd--;
                }
                $(thisid).after('<tr id="newtr"><td id="filltd" colspan="' + (jmltd + 1) + '"></td></tr>');
                call($(thisid).attr('urldetil'), 'filltd', '');
                //$('#filltd').html($(thisid).attr('urldetil'));
                //$('#filltd').load($(thisid).attr('urldetil'));
            }
        }
        return false;
    }
}

function td_detil_priview_blank(id, thisid) {
    var url = $(thisid).attr('urldetil');
    var key = $(thisid).attr('keyz');
    var id = multiReplaces(key, '.', '/');
    if ($(thisid).attr('urldetil')) {
        location.href = $(thisid).attr('urldetil') + '/' + id;
    }
    return false;
}

function multiReplaces(str, match, repl) {
    do {
        str = str.replace(match, repl);
    } while (str.indexOf(match) !== -1);
    return str;
}
