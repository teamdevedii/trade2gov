(function(){
    startUp();
    $.ajaxSetup({
        timeout: 60000,
        cache: false,
        error: function (x, e) {
            var msg = '';
            if (x.status == 0) {
                msg = 'Request Aborted!';
            } else if (x.status == 404) {
                msg = 'Permintaan URL tidak ditemukan!\n Atau Login Anda Sudah Expire!';
            } else if (x.status == 500) {
                msg = 'Internal Server Error!';
            } else if (e == 'parsererror') {
                msg = 'Error.\nParsing JSON Request failed!';
            } else if (e == 'timeout') {
                msg = 'Request Time out!';
            } else {
                msg = 'Error tidak diketahui: \n' + x.responseText;
            }
            $('#msgConfirm').removeClass('notify-success');
            $('#msgConfirm').addClass('notify-error');
            $('#msgConfirm').slideDown("slow");
            $('#msgConfirm').html('<a onclick="$(\'#msgConfirm\').hide();" class="closex">&times;</a><h5>Error</h5><p>'+msg+'</p>');
            $('#loadingImg').remove();
            Clearjloadings();
        }
    });
})();
function autoRespon(){
    setTimeout( function(){
        $.ajax({
            url: site_url + '/komunikasi/autoRespon',
            type:'POST',
            timeout: 300000,
            cache: false,
            data: {key: $.now()},
            success: function(datax) {
                $('#msgConfirm').removeClass();
                var arrdata = datax.split('|');
                if (arrdata[0].trim()==='RES'){
                    $('#msgConfirm').addClass('notify notify-success');
                    $('#msgConfirm').slideDown("slow");
                    $('#msgConfirm').html('<a onclick="$(\'#msgConfirm\').hide();" class="closex">&times;</a><h5>Respon</h5><p>'+arrdata[1]+'</p>');
                }
                autoRespon();
            }
        });
    },300000);
}
function call(url, div, tit) {
    //$(window).scrollTop(0);
    $('#msgConfirm').hide();
    $('#' + div).slideDown("slow");
    $('#' + div).html('<div id="loadingImg" style="margin-left: auto; margin-top: auto;"><img src="' + base_url + 'img/loaders/facebook.gif" /> loading...</div>');
    jloadings();
    $.ajax({
        url: url,
        type:'POST',
        timeout: 60000,
        cache: false,
        data: {
            keyoke: $.now()
        },
        success: function(data) {
            if(tit!==''){$('#contentHeader h1').html(tit);}
            $('#' + div).toggle(function () {
                $('#' + div).html(data);
                $('#' + div).slideDown("slow");
            });
            Clearjloadings();
        }
    });
    
}
function callnoAnim(url, div) {
    $('#' + div).html('<div style="margin-left: auto; margin-top: auto;"><img src="' + base_url + 'img/loaders/facebook.gif" /> loading...</div>');
    $.ajax({
        url: url,
        type: 'POST',
        data: {
            keyoke: $.now()
        }
    }).done(function (data) {
        $('#' + div).html(data);
    });
}
function change_captcha() {
    document.getElementById('captcha').src = base_url + "app/libraries/captcha/captcha.php?rnd=" + Math.random();
}
function ReplaceAll(Source, stringToFind, stringToReplace) {
    var temp = Source;
    var index = temp.indexOf(stringToFind);
    while (index != -1) {
        temp = temp.replace(stringToFind, stringToReplace);
        index = temp.indexOf(stringToFind);
    }
    return temp;
}
function formatNPWP(id) {
    var npwp = $('#' + id).val();
    var result = '';
    if (npwp == '') {
        $('#' + id).val('');
        return false;
    }
    result = npwp.substr(0, 2) + "." + npwp.substr(2, 3) + "." + npwp.substr(5, 3) + "." + npwp.substr(8, 1) + "-" + npwp.substr(9, 3) + "." + npwp.substr(12, 3);
    $('#' + id).val(result);
    return false;
}
function unformatNPWP(id) {
    var npwp = $('#' + id).val();

    var result = ReplaceAll(ReplaceAll(npwp, '-', ''), '.', '');

    $('#' + id).val(result);
    //$('#' + id).focus();
    return false;
}
function f_MAXLENGTH(idFrm) {
    var arrComp = '';
    var arrElm = '';
    var i = 0;
    arrComp = $('#' + idFrm + ' #MAXLENGTH').val().split('|');
    for (i = 0; i < arrComp.length; i++) {
        arrElm = arrComp[i].split('-');
        $('#' + idFrm + ' #' + arrElm[0]).attr('maxlength', arrElm[1]);
    }
}
function Autocomp(id, grp) {
    if (grp === '' || typeof (grp) === 'undefined')
        var grp = '';

    $("#" + id).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: $("#" + id).attr('url'),
                data: {
                    keys: request.term,
                    grp: grp},
                dataType: "json",
                type: "POST",
                success: function (data) {
                    response(data);
                }
            });
        }, select: function (event, ui) {
            var arrUrai = $("#" + id).attr('urai').split(';');
            var arrVal = ui.item.value.split('|');
            var i = 0;
            for (i = 0; i < arrUrai.length; i++) {
                var tipe = $("#" + arrUrai[i]).get(0).tagName;
                if (tipe === 'INPUT' || tipe === 'SELECT') {
                    $("#" + arrUrai[i]).val(arrVal[i]);
                } else {
                    $("#" + arrUrai[i]).html(arrVal[i]);
                }
            }
            return false;
        }, focus: function (event, ui) {
            var arrUrai = $("#" + id).attr('urai').split(';');
            var arrVal = ui.item.value.split('|');
            var i = 0;
            for (i = 0; i < arrUrai.length; i++) {
                var tipe = $("#" + arrUrai[i]).get(0).tagName;
                if (tipe === 'INPUT' || tipe === 'SELECT') {
                    $("#" + arrUrai[i]).val(arrVal[i]);
                } else {
                    $("#" + arrUrai[i]).html(arrVal[i]);
                }
            }
            return false;
        },
        minLength: 1,
        autofocus: true
    });
}
function Dialog(url, Divid, title, width, height) {
    c_dialog(Divid, ':: ' + title + ' ::', '<div id="idv_popup"></div>', width, height, "run-in", true, false);
    $("#" + Divid).html('<center><img src="' + base_url + 'img/loaders/facebook.gif" /> loading...</center>');
    $('#' + Divid).load(url);
}
function DialogPost(url, Divid, title, width, height,data){
    c_dialog(Divid, ':: ' + title + ' ::', '<div id="idv_popup"></div>', width, height, "run-in", true, false);
    $("#" + Divid).html('<center><img src="' + base_url + 'img/loaders/facebook.gif" /> loading...</center>');
    $.ajax({
        url: url,
        type: 'POST',
        timeout: 300000,
        data: {
            data: data
        }
    }).done(function (data) {
        $('#' + Divid).html(data);
    });
}
function ShowDP(obj_date) {
    if(!$("#" + obj_date).is('[readonly]')){
        $("#" + obj_date).datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: 'dd-mm-yy',
            regional: 'id'
        });
    }
    return false;
}
function declareDP(formid){
    var a = '';
    $.each($('#' + formid + " input[formatDate=yes]"), function (n, element) {
        ShowDP(this.id);
        a = $(this).val();
        if(a !== ''){
            if(a === '0000-00-00'){
                $(this).val('');
            }else{
                var arr = a.split('-');
                if(arr[0].length===4){
                    $(this).val(arr[2] + '-' + arr[1] + '-' + arr[0]);
                }
            }
        }
    });
}
function formatTanggal(formid){
    var a = '';
    $.each($('#' + formid + " input[formatDate=yes]"), function (n, element) {
        a = $(this).val();
        if(a !== ''){
            if(a === '0000-00-00'){
                $(this).val('');
            }else{
                var arr = a.split('-');
                if(arr[0].length===4){
                    $(this).val(arr[2] + '-' + arr[1] + '-' + arr[0]);
                }
            }
        }
    });
}
function formatNumeral(formid){
    $.each($('#' + formid + " input[numeral=yes]"), function (n, element) {
        $(element).blur(function(){
            var data = $(element).val();
            var format = $(element).attr('format');
            var strFormat = numeral(data).format(format);
            $(element).val(strFormat);
        });

        $(element).focus(function(){
            var data = $(element).val();
            var strUnFormat = numeral().unformat(data);
            $(element).val(strUnFormat);
        });
        
        $(element).keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) || 
                 // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    });
}
function ThausandSeperator(hidden, value, digit) {
    var thausandSepCh = ",";
    var decimalSepCh = ".";
    var tempValue = "";
    var realValue = value + "";
    var devValue = "";
    if (digit == "")
        digit = 3;
    realValue = characterControl(realValue);
    var comma = realValue.indexOf(decimalSepCh);
    if (comma != -1) {
        tempValue = realValue.substr(0, comma);
        devValue = realValue.substr(comma);
        devValue = removeCharacter(devValue, thausandSepCh);
        devValue = removeCharacter(devValue, decimalSepCh);
        devValue = decimalSepCh + devValue;
        if (devValue.length > digit) {
            devValue = devValue.substr(0, digit + 1);
        }
    } else {
        tempValue = realValue;
    }
    tempValue = removeCharacter(tempValue, thausandSepCh);
    var result = "";
    var len = tempValue.length;
    while (len > 3) {
        result = thausandSepCh + tempValue.substr(len - 3, 3) + result;
        len -= 3;
    }
    result = tempValue.substr(0, len) + result;
    if (hidden != "") {
        $("#" + hidden).val(tempValue + devValue);
    }
    return result + devValue;
}
function characterControl(value) {
    var tempValue = "";
    var len = value.length;
    for (i = 0; i < len; i++) {
        var chr = value.substr(i, 1);
        if ((chr < '0' || chr > '9') && chr != '.' && chr != ',') {
            chr = '';
        }
        tempValue = tempValue + chr;
    }
    return tempValue;
}
function removeCharacter(v, ch) {
    var tempValue = v + "";
    var becontinue = true;
    while (becontinue == true) {
        var point = tempValue.indexOf(ch);
        if (point >= 0) {
            var myLen = tempValue.length;
            tempValue = tempValue.substr(0, point) + tempValue.substr(point + 1, myLen);
            becontinue = true;
        } else {
            becontinue = false;
        }
    }
    return tempValue;
}
function c_dialog(id, title, inner, width, height, clobtn, modal, resize) {
    $('#' + id).remove();
    c_div(id, '<div style="margin:20px 25px 5px 12px;">' + inner + '</div>');
    $("#" + id).dialog({
        bgiframe: true,
        resizable: false,
        autoOpen: true,
        modal: modal,
        title: title,
        height: height,
        width: width,
        resize: resize,
        clobtn: clobtn,
        draggable: false,
        overlay: {backgroundColor: '#000', opacity: 0.5, height: '100%'},
        show: {effect: 'fade', direction: "up"},
        hide: {effect: 'fade', direction: "up"}
    });
}
function limitChars(textid, limit, infodiv) {
    var text = $('#' + textid).val();
    var textlength = text.length;
    if (textlength > limit)
    {
        $('#' + infodiv).html('<font color="red">Tidak bisa lebih dari ' + limit + ' karakter!</font>');
        $('#' + textid).val(text.substr(0, limit));
        return false;
    }
    else
    {
        $('#' + infodiv).html('<font color="green">' + (limit - textlength) + ' karakter yang tersisa.</font>');
        return true;
    }
}
function c_div(id, inner) {
    div = document.createElement("div");
    div.innerHTML = '<div id="' + id + '" style="display: none;">' + inner + '</div>';
    document.body.appendChild(div);
}
function closedialog(id) {
    $("#" + id).dialog('close');
}
function Clearjloadings() {
    $("#popup_container").remove();
    $("#popup_overlay").remove();
}
function save_post_msg(formid, iddiv, divcon) {
    //return "MSG|OK|".site_url('bc20/daftarDetil/barang/'.$car).'/|Delete data barang berhasil.';
    if (formid === "" || typeof (formid) === "undefined")
        var formid = "";
    if (iddiv === "" || typeof (iddiv) === "undefined")
        var iddiv = "msgConfirm";
    if (divcon === "" || typeof (divcon) === "undefined")
        var divcon = "_content_";
        var hdrMsg = '';
        var isiMsg = '';
    if($('#' + formid).attr('action')==='' || $('#' + formid).attr('action') === site_url){return false;}
    if (validasi(iddiv, '#' + formid)) {
        jloadings();
        $.ajax({
            type: 'POST',
            url: $('#' + formid).attr('action'),
            data: $('#' + formid).serialize(),
            success: function (datax) {
                $('#' + iddiv).removeClass();
                var arrdata = datax.split('|');
                if (arrdata[0].trim()==='MSG') {
                    if(arrdata[2].trim() !== ''){arrdata[2] = ReplaceAll(arrdata[2], '`', '|');}
                    if(arrdata[2]!=='msgbox' && arrdata[2]!==''){call(arrdata[2], divcon);}
                    if (arrdata[1] === 'OK') {
                        $('#' + iddiv).addClass('notify notify-success');
                        hdrMsg = 'Success';
                        if(arrdata[2]==='msgbox'){$('#msgbox').dialog('close');}
                    } else {
                        $('#' + iddiv).addClass('notify notify-error');
                        hdrMsg = 'Error';
                    }
                    isiMsg = arrdata[3];
                } else {
                    $('#' + iddiv).addClass('notify');
                    hdrMsg = 'Notifikasi';
                    isiMsg = datax;
                }
                $('#' + iddiv).slideDown("slow");
                $('#' + iddiv).html('<a onclick="$(\'#'+iddiv+'\').hide();" class="closex">&times;</a><h5>'+hdrMsg+'</h5><p>'+isiMsg+'</p>');
                Clearjloadings();
            }
        });
        return true;
    }
    return false;
}
function save_post_msgPop(formid, iddiv, divcon) {
    if (formid === "" || typeof (formid) === "undefined")
        var formid = "";
    if (iddiv === "" || typeof (iddiv) === "undefined")
        var iddiv = "msgConfirm";
    if (divcon === "" || typeof (divcon) === "undefined")
        var divcon = "_content_";
        var hdrMsg = '';
        var isiMsg = '';
    if($('#' + formid).attr('action')==='' || $('#' + formid).attr('action') === site_url){return false;}
    if (validasi(iddiv, '#' + formid)) {
        jloadings();
        $.ajax({
            type: 'POST',
            url: $('#' + formid).attr('action'),
            data: $('#' + formid).serialize(),
            success: function (datax) {
                $('#' + iddiv).removeClass();
                var arrdata = datax.split('|');
                if (arrdata[0].trim()==='MSG') {
                    arrdata[2] = ReplaceAll(arrdata[2], '`', '|');
                    if(arrdata[2]!=='msgbox' && arrdata[2]!==''){call(arrdata[2], divcon);}
                    if (arrdata[1] === 'OK') {
                        $('#' + iddiv).addClass('notify notify-success');
                        hdrMsg = 'Success';
                        if(arrdata[2]==='msgbox'){$('#msgbox').dialog('close');}
                    } else {
                        $('#' + iddiv).addClass('notify notify-error');
                        hdrMsg = 'Error';
                    }
                    isiMsg = arrdata[3];
                } else {
                    $('#' + iddiv).addClass('notify');
                    hdrMsg = 'Notifikasi';
                    isiMsg = datax;
                }
                $('#' + iddiv).slideDown("slow");
                $('#' + iddiv).html('<a onclick="$(\'#'+iddiv+'\').hide();" class="closex">&times;</a><h5>'+hdrMsg+'</h5><p>'+isiMsg+'</p>');
                Clearjloadings();
            }
        });
        return true;
    }
    return false;
}
function validasi(divid, formid) {
    if (formid == "" || typeof (formid) == "undefined")
        var formid = "";
    if (divid == "" || typeof (divid) == "undefined")
        var divid = "msg_";

    if(divid === 'msg_'){
        $(formid + " ." + divid).hide();
        $(formid + " ." + divid).css('color', '');
        $(formid + " ." + divid).html('<img src="' + base_url + 'img/loaders/facebook.gif" /> loading...');
        $(formid + " ." + divid).fadeIn('slow');
    }
    
    var notvalid = 0;
    var notnumber = 0;
    var mustfull = 0;
    var regNumber = /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/;
    $.each($(formid + " input:visible, " + formid + " select:visible, " + formid + " textarea:visible, " + formid + " input:checkbox, " + formid + " input:radio"), function (n, element) {
        $(this).css("border", "1px #D0D0D0 solid");
        if ($(this).attr('wajib') == "yes" && ($(this).val() == "" || $(this).val() == null)) {
            $(this).css("border", "1px #ff6666 solid");
            //$(this).addClass('wajib');
            notvalid++;
        }
        if ($(this).attr('angka') == "yes" && (!regNumber.test($(this).val()) && $(this).val() != "")) {
            $(this).css("border", "1px #44dd44 solid");
            notnumber++;
        }
        if ($(this).attr('full') == "yes" && $(this).attr('maxlength') != null && $(this).val().length != parseInt($(this).attr('maxlength'))) {
            $(this).css("border", "1px #6666ff solid");
            mustfull++;
        }
    });
    if (notvalid > 0 || notnumber > 0 || mustfull > 0) {
        var val = "";
        if (notvalid > 0) {
            val += '<span style="color: #ff6666">Ada ' + notvalid + ' Kolom Yang Harus Diisi.</span><br>';
        }
        if (notnumber > 0) {
            val += '<span style="color: #44dd44">Ada ' + notnumber + ' Kolom Yang Harus Diisi Dengan Angka.</span><br>';
        }
        if (mustfull > 0) {
            val += '<span style="color: #6666ff">Ada ' + mustfull + ' Kolom Yang Harus dilengkapi.</span><br>';
        }
        if(divid === 'msg_'){
            $(formid + " ." + divid).css('color', 'red');
            $(formid + " ." + divid).html(val);
            $(formid + " ." + divid).fadeIn('slow');
        }else{
            $('#' + divid).removeClass();
            $('#' + divid).addClass('notify notify-error');
            $('#' + divid).slideDown("slow");
            $('#' + divid).html('<a onclick="$(\'#'+divid+'\').hide();" class="closex">&times;</a><h5>Error</h5><p>'+val+'</p>');
            
        }
        return false;
    }
    else {
        return true;
    }
}
function save_post(formid) {
    if (formid === '' || typeof (formid) === 'undefined')var formid = "";
    formid = '#' + formid;
    if (validasi('', formid)) {
        $.ajax({
            type: 'POST',
            url: $(formid).attr('action'),
            data: $(formid).serialize(),
            success: function (data) {
                //alert(data);
                if (data.search("MSG") >= 0) {
                    var arrdata = data.split('#');
                    if (arrdata[1] == "OK") {
                        $(".msg_").css('color', 'green');
                        $(".msg_").html(arrdata[2]);
                    }
                    else {
                        $(".msg_").css('color', 'red');
                        $(".msg_").html(arrdata[2]);
                        if(formid==='frmRegistrasi')change_captcha();
                    }
                    if (arrdata.length > 3) {
                        setTimeout(function () {
                            call(arrdata[3], '_content_')
                        }, 2500);
                        return false;
                    }
                }
                else {
                    $(".msg_").css('color', 'red');
                    $(".msg_").html('Proses Gagal.');
                }
            }
        });
    }
    return false;
}
function cancel(formid) {
    $('input, textarea, select').removeClass('wajib');
    $('#' + formid + ' span.uraian').html('');
    document.getElementById(formid).reset();    
}
function FormatHS(ID) {
    var hs = ReplaceAll($('#'+ID).val(),'.','') ;
    $('#'+ID).val(hs.substr(0, 4) + "." + hs.substr(4, 2) + "." + hs.substr(6, 2) + "." + hs.substr(8));
}
function unFormatHS(ID) {
    var hs = ReplaceAll($('#'+ID).val(),'.','') ;
    $('#'+ID).val(hs);
}
function checkCode(obj){
    var grp = $(obj).attr('grp');
    var div = $(obj).attr('urai');
    var max = parseInt($(obj).attr('maxlength'));
    var nocom = $(obj).attr('nocomplate');
    var val = $(obj).val();
    if(max === val.length || nocom === 'true'){
        $('#' + div).html('<span style="margin-left: auto; margin-top: auto;"><img src="' + base_url + 'img/loaders/facebook.gif" /> loading...</span>');
        $.ajax({
            url: site_url + '/referensi/checkCode/' + grp ,
            type: 'POST',
            data: {
                keyoke: $.now(),
                data: val
            }
        }).done(function (data) {
            $('#' + div).html(data);
        });
    }else{
        $('#' + div).html('');
    }
    
}

function formatTime(ID){
    var data = ReplaceAll($('#' + ID).val(),':','');
    if(data!==''){
        $('#' + ID).val(data.substr(0,2) + ':' + data.substr(2,2));
    }
}