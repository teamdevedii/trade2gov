<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class actManageUser extends CI_Model{
    
    function lstUser($type=''){
        $this->load->library('newtable');
        if ($type == "registrasi") {
            $judul = "Daftar Perusahaan yang melakukan registrasi online.";
            $SQL = "SELECT  concat(b.URAIAN,' - ',a.ID) AS Identitas,
                            NAMA AS `Nama Perusahaan`,
                            ALAMAT AS `Alamat Perusahaan`,
                            TELEPON AS `No.Telp Perusahaan`,
                            FAX AS `No.Fax Perusahaan`,
                            NAMA_PEMILIK AS `Nama Pemilik`,
                            KODE_TRADER
                    FROM    m_trader_tmp a Left Join m_tabel b on b.MODUL = 'BC20' AND b.KDTAB = 'JENIS_IDENTITAS' AND b.KDREC = a.KODE_ID";
            $this->newtable->keys(array('KODE_TRADER'));
            $this->newtable->hiddens(array('KODE_TRADER'));
            $this->newtable->search(array(
                array("concat(b.URAIAN,' - ',a.ID)", 'Identitas'),
                array('NAMA', 'Nama Perusahaan'),
                array('ALAMAT', 'Alamat Perusahaan'),
                array('TELEPON', 'No.Telp Perusahaan'),
                array('FAX', 'No.Fax Perusahaan'),
                array('NAMA_PEMILIK', 'Nama Pemilik')));
            $this->newtable->orderby(3);
            $this->newtable->sortby("DESC");
            $this->newtable->action(site_url('usermanage/regOnline'));
            $process = array(
                'Approve Registrasi' => array('GETAJAX',  site_url('usermanage/viwRegister'), '1','_content_'));
        }else {
            return "Failed";
            exit();
        }
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->rowcount(10);
        $this->newtable->clear();
        $this->newtable->set_formid("f" . $type);
        $this->newtable->set_divid("div" . $type);
        $this->newtable->menu($process);
        $tabel = $this->newtable->generate($SQL);
        $arrdata = array("tabel" => $tabel,'judul'=>$judul);
        if ($this->input->post("ajax"))
            return $tabel;
        else
            return $arrdata;
    }
    
    function lstPerusahaan($type='',$kd_trader=''){
        $this->load->library('newtable');
        switch(strtolower($type)){
            case'perusahaan':
                $judul = "Daftar Perusahaan.";
                $SQL = "SELECT  concat(b.URAIAN,' - ',a.ID) AS Identitas,
                                NAMA AS `Nama Perusahaan`,
                                ALAMAT AS `Alamat Perusahaan`,
                                TELEPON AS `No.Telp Perusahaan`,
                                FAX AS `No.Fax Perusahaan`,
                                NAMA_PEMILIK AS `Nama Pemilik`,
                                KODE_TRADER
                        FROM    m_trader a Left Join m_tabel b on b.MODUL = 'BC20' AND b.KDTAB = 'JENIS_IDENTITAS' AND b.KDREC = a.KODE_ID ";
                $this->newtable->keys(array('KODE_TRADER'));
                $this->newtable->hiddens(array('KODE_TRADER'));
                $this->newtable->search(array(
                    array("concat(b.URAIAN,' - ',a.ID)", 'Identitas'),
                    array('NAMA', 'Nama Perusahaan'),
                    array('ALAMAT', 'Alamat Perusahaan'),
                    array('TELEPON', 'No.Telp Perusahaan'),
                    array('FAX', 'No.Fax Perusahaan'),
                    array('NAMA_PEMILIK', 'Nama Pemilik')));
                $this->newtable->orderby('KODE_TRADER');
                $this->newtable->sortby("DESC");
                $this->newtable->action(site_url('usermanage/lstPerusahaan/perusahaan'));
                $this->newtable->detail(site_url('usermanage/lstPerusahaan/userperusahaan/'));
                $this->newtable->detail_tipe('detil_priview_bottom');
                $process = array(
                    'Ubah Perusahaan'     => array('GETAJAX',  site_url('usermanage/mngPerusahaan/edit'), '1','_content_'),
                    'Hapus Perusahaan'    => array('GETAJAX',  site_url('usermanage/mngPerusahaan/hapus'), '1','_content_'),
                    'Reset Passsword Admin Perusahaan' => array('MSGBOX', site_url('usermanage/mngPerusahaan/resetPass'), '1'));
                break;
            case'userperusahaan':
                $this->load->model('actMain');
                $judul = "Daftar User.";
                $SQL = "SELECT  Username,
                                NAMA as `Nama`,
                                ALAMAT as `Alamat`,
                                TELEPON as `Telepon`,
                                JABATAN as `Jabatan`,
                                b.URAIAN as `Tipe User`,
                                c.URAIAN as `Status`,
                                LAST_LOGIN as `Login Terakhir`,
                                EXPIRED_DATE as `Aktif Sampai`,
                                KODE_TRADER,USER_ID
                        FROM    t_user a Left Join m_tabel b on b.MODUL = 'BC20' AND b.KDTAB = 'TIPE_USER' AND b.KDREC = a.TIPE_USER
                                         Left Join m_tabel c on c.MODUL = 'BC20' AND c.KDTAB = 'STATUS_USER' AND c.KDREC = a.STATUS_USER
                        WHERE KODE_TRADER = '".$kd_trader."'";
                $this->newtable->keys(array('KODE_TRADER','USER_ID'));
                $this->newtable->hiddens(array('KODE_TRADER','USER_ID'));
                $this->newtable->search(array(
                    array("Username", 'Username'),
                    array('NAMA', 'Nama'),
                    array('ALAMAT', 'Alamat'),
                    array('TELEPON', 'Telepon'),
                    array('JABATAN', 'Jabatan'),
                    array('b.KDREC', 'Tipe User', 'tag-select', $this->actMain->get_mtabel('TIPE_USER')),
                    array('c.KDREC', 'Status', 'tag-select', $this->actMain->get_mtabel('STATUS_USER'))));
                $this->newtable->orderby(3);
                $this->newtable->sortby("DESC");
                $this->newtable->action(site_url('usermanage/lstPerusahaan/userperusahaan/'.$kd_trader));
                $process = array(
                    'Ubah User'     => array('GETAJAX',  site_url('usermanage/mngPerusahaan/edit'), '1','_content_'),
                    'Hapus User'    => array('GETAJAX',  site_url('usermanage/mngPerusahaan/hapus'), '1','_content_'),
                    'Reset Passsword User'    => array('GETAJAX',  site_url('usermanage/mngPerusahaan/resetPass'), '1','_content_'));
                break;
        }
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->rowcount(10);
        $this->newtable->clear();
        $this->newtable->set_formid("f" . $type);
        $this->newtable->set_divid("div" . $type);
        $this->newtable->menu($process);
        $tabel = $this->newtable->generate($SQL);
        $arrdata = array('tabel' => $tabel,'judul'=>$judul);
        if ($this->input->post("ajax")){
            echo $tabel;die();
        }else{
            return $arrdata;
        }
    }
    function getProfile($id_user){
        $this->load->model('actMain');
        $query = "SELECT a.KODE_TRADER, a.USERNAME, a.PASSWORD, a.NAMA, a.ALAMAT, a.TELEPON, a.JABATAN, 
                         a.STATUS_USER,a.TIPE_USER AS KODE_TIPE_USER, a.EXPIRED_DATE ,b.KODE_ID,b.ID,
                         b.FAX,b.TIPE_TRADER,b.NO_PPJK,b.TANGGAL_PPJK,b.STATUS,b.NAMA_PEMILIK,b.ALAMAT_PEMILIK,
                         b.TEMPAT_LAHIR_PEMILIK,b.TANGGAL_LAHIR_PEMILIK,b.EMAIL_PEMILIK,b.EDINUMBER,
                         b.DOKUMEN,b.TIPE_TRADER, b.NAMA AS NAMA_TRADER, b.LOGO
                         
                  FROM   t_user a  INNER JOIN m_trader b    ON a.KODE_TRADER = b.KODE_TRADER
                  WHERE  a.USER_ID = '".$id_user."'";
		/*SELECT a.KODE_TRADER, a.USERNAME, a.PASSWORD, a.NAMA, a.ALAMAT, a.TELEPON, a.JABATAN, 
                         a.STATUS_USER,a.TIPE_USER AS KODE_TIPE_USER, a.EXPIRED_DATE ,b.KODE_ID,b.ID,
                         b.FAX,b.TIPE_TRADER,b.NO_PPJK,b.TANGGAL_PPJK,b.STATUS,b.NAMA_PEMILIK,b.ALAMAT_PEMILIK,
                         b.TEMPAT_LAHIR_PEMILIK,b.TANGGAL_LAHIR_PEMILIK,b.EMAIL_PEMILIK,b.EDINUMBER,
                         b.DOKUMEN,b.TIPE_TRADER, b.NAMA AS NAMA_TRADER, b.LOGO
                         
                  FROM   t_user a  INNER JOIN m_trader b    ON a.KODE_TRADER = b.KODE_TRADER
                  WHERE  a.USER_ID = '2';
				  SELECT a.KODE_TRADER, a.USERNAME, a.PASSWORD, a.NAMA, a.ALAMAT, a.TELEPON, a.JABATAN, 
                         a.STATUS_USER,a.TIPE_USER AS KODE_TIPE_USER, a.EXPIRED_DATE ,b.KODE_ID,b.ID,
                         b.FAX,b.TIPE_TRADER,b.NO_PPJK,b.TANGGAL_PPJK,b.IMPSTATUS,b.NAMA_PEMILIK,b.ALAMAT_PEMILIK,
                         b.TEMPAT_LAHIR_PEMILIK,b.TANGGAL_LAHIR_PEMILIK,b.EMAIL_PEMILIK,b.KPBC,b.EDINUMBER,
                         b.NOREG,c.URAIAN_KPBC as 'URKPBC',b.DOKUMEN,b.TIPE_TRADER, b.NAMA AS NAMA_TRADER, b.LOGO
                         
                  FROM   t_user a  Inner Join m_trader b    on a.KODE_TRADER = b.KODE_TRADER 
                                   Left Join  m_KPBC c      on b.KPBC        = c.KDKPBC
                  WHERE  a.USER_ID = '".$kd_user."'";*/
        $hsl['DATA']            = $this->actMain->get_result($query);
        $hsl['JENIS_IDENTITAS'] = $this->actMain->get_mtabel('JENIS_IDENTITAS', 1, FALSE);
        $hsl['TIPE_TRADER']     = $this->actMain->get_mtabel('TIPE_TRADER', 1, FALSE,'','KODEDANUR');
        return $hsl;
    }
    function approve(){
        //die('tes');
        //insert data ke table m_trader
        $this->load->model('actMain');
        $data = $this->input->post('DATA');
        $KODE_TRADERtmp = $data['KODE_TRADER'];
        unset($data['KODE_TRADER']);
        $data['DOKUMEN']= implode(",", $data['DOKUMEN']);
        $data['ID']     = str_replace(".", "",str_replace("-", "",$data['ID']));
        $data['NOREG']  = substr('000000'.$this->actMain->get_uraian("SELECT LAST_NOREG+1 AS NOREG FROM M_KPBC WHERE KDKPBC = '".$data['KPBC']."'",'NOREG'),-6);
        //print_r($data);die();
        $exec           = $this->db->insert('m_trader', $data);
        if($exec){
            $USER['KODE_TRADER']=$this->db->insert_id();
            
            //set last noregistrasi di table m_kpbc
            $sql = "call P_SETNOREG('".$data['KPBC']."')";
            $this->db->query($sql);
            
            //insert user di table t_user
            $pass   = $this->actMain->genPassword();
            $USER['USERNAME']   = $data['EMAIL_PEMILIK'];
            $USER['PASSWORD']   = md5($pass);
            $USER['NAMA']       = $data['NAMA_PEMILIK'];
            $USER['ALAMAT']     = $data['ALAMAT_PEMILIK'];
            $USER['TIPE_USER']  = '4';
            $USER['STATUS_USER']= '1';
            $USER['EXPIRED_DATE']= date("Y-m-d",strtotime(date()." +2 day"));
            $this->db->insert('t_user', $USER);
            //send email 
            $isiEmail = "<pre>Anda dapat login ke Sistem Trade to Goverment dengan menggunakan username dan password sebagai berikut:
email   	: ".$USER['USERNAME']."
Password	: ".$pass."


* User anda akan kadaluarsa pada tanggal ".$USER['EXPIRED_DATE'].", untuk memperlama waktu aktif user anda,silahkan segera login.</pre>";
            $kirimEmail = $this->actMain->send_mail($USER['USERNAME'],'User dan Password Trade to Goverment',$isiEmail);
            
            //delete data di table m_trader_tmp
            $this->db->where('KODE_TRADER', $KODE_TRADERtmp);
            $this->db->delete('m_trader_tmp');
            if($kirimEmail){
                return "MSG#OK#Aprove user Berhasil.<br>dan Email konfirmasi user password berhasil dikirm.#".site_url('home');
            }else{
                return "MSG#OK#Aprove user Berhasil.<br>dan Email konfirmasi user password gagal dikirm.<br>Kirim Email konfirmasi user password setelah user terdaftar.#".site_url('home');
            }
        }
        return "MSG#ERROR#Aprove user gagal.";
    }
    function resetPassword($kd_trader,$id_user=''){
        $this->load->model('actMain');
        if($id_user==''){
            //ambil id admin yang ada di kd_trader tersebut
            $sql = "SELECT GROUP_CONCAT(USER_ID) 'UID'
                    FROM t_user 
                    WHERE KODE_TRADER = ".$kd_trader." AND TIPE_USER = '4'";//die($sql);
            $arrData = $this->actMain->get_result($sql);
            $id_user = $arrData['UID'];
        }
        if($id_user!=''){
            $sql  = "SELECT USER_ID,USERNAME,NAMA,EXPIRED_DATE
                     FROM   t_user
                     WHERE  USER_ID in (".$id_user.")";//die($sql);
            $data = $this->actMain->get_result($sql,true);
            foreach($data as $a){
                $pass   = $this->actMain->genPassword();
                $USER['USER_ID']    = $a['USER_ID'];
                $USER['PASSWORD']   = md5($pass);
                $USER['EXPIRED_DATE']= date("Y-m-d",strtotime(date()." +2 day"));
                $this->actMain->insertRefernce('t_user',$USER);
                //$this->db->insert('t_user', $USER);
$isiEmail = "<pre>Password login anda telah direset:
email   	: ".$a['USERNAME']."
Password	: ".$pass."

* User anda akan kadaluarsa pada tanggal ".$USER['EXPIRED_DATE'].", untuk memperlama waktu aktif user anda,silahkan segera login.</pre>";
                $kirimEmail = $this->actMain->send_mail($USER['USERNAME'],'Reset Password Trade to Goverment',$isiEmail);
                if($kirimEmail){
                    $hasil[] = 'OK|'.$a['USERNAME'];
                }else{
                    $hasil[] = 'ER|'.$a['USERNAME'];
                }
            }
        }else{
            $hasil[] = '00|';
        }
        return $hasil;
    }
    
}