<?php
if (!defined('BASEPATH')){exit('No direct script access allowed');}
class dokBC30PKBE extends CI_Model {
    var $main = '';      #load actMain
    var $pdf = '';       #load fpdf library
    var $nPungutan = ''; #pungutan
    var $car = '';       #Car
    var $hdr = '';       #data header dalam array 1D
    var $res = '';       #data header dalam array 1D
    var $dok = '';       #data dokumen dalam array 2D
    var $con = '';       #data container dalam array 2D
    var $conr = '';      #data container dalam array 2D
    var $kms = '';       #data kemasan dalam array 2D
    var $dtl = '';       #data detil dalam array 2D
    var $pgt = '';       #data pungutan dalam array 2D
    var $ntb = '';       #data ntb dalam array 1D
    var $npt = '';       #data ntp dalam array 1D
    var $ktr = '';       #data kantor kpbc dalam array 1D
    var $rpt = '';       #data caption respon NPP
    var $kd_trader = ''; #session data kode trader
    var $tp_trader = ''; #session data tipe trader
    var $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
    var $segmentUri = array(); #segment url

    function fpdf($pdf) {
        $this->pdf = $pdf;
    }

    function ciMain($main) {
        $this->main = $main;
    }

    function segmentUri($arr) {
        $this->segmentUri = $arr;
    }

    function showPage($car, $jnPage) {
        $this->car = $car;
        $this->kd_trader = $this->newsession->userdata('KODE_TRADER');
        $this->tp_trader = $this->newsession->userdata('TIPE_TRADER');
        $this->pdf->AddPage();
        switch ($jnPage) {
            case'1':
                $this->getHDR();
                $this->getDTL();
                $this->drawHeaderPKBE();
                break;
            case'2':
                $this->getNTB();
                $this->drawSTAFFING();
                break;
            case'3':
                $this->getHDR();
                $this->getDTL();
                $this->drawSTAFFING();
                break;
            case'4':
                $this->getHDR();
                $this->getDTL();
                $this->drawLembarCatPemerikasaanFisikBrg();
                break;
            case'100':case'110':case'120':case'200':case'210':case'220':case'230':case'240':case'270':case'900':
            case'470':case'130':case'600':case'700':case'295':case'250':
                $this->getHDR();
                $this->getRES();
                $this->isiRespon();
                $this->drawResponUmum();
                break;
            case'280';
                $this->getHDR();
                $this->getRES();                
                $this->drawResponSPPB();
                break;
            case'470';
                $this->getHDR();
                $this->getRES();                
                $this->drawResponPembatalan(); 
                break;
            case'500';
                $this->getHDR();
                $this->getRES();                
                $this->drawResponSPKPBM(); 
                break;
            default :
                $this->pdf->SetFont('times', 'B', '12');
                $this->pdf->cell(131.6, 4, 'Tidak ada jenis laporan ini', 0, 0, 'R', 0);
                break;
        }
        $this->pdf->Output();
    }

    function getHDR() {
        $sql = "SELECT A.*, 
                        F_KPBC(KDKTR)   as URKDKTR ,
                        F_KPBC(KDKTRPERIKSA)  as URKDKTRPERIKSA ,
                        F_KPBC(KDKTRMUAT)  as URKDKTRMUAT ,
                        F_KPBC(KDHANGGAR)  as URKDHANGGAR ,
                        F_NEGARA(KDNEGTUJU) as URKDNEGTUJU ,
                        F_NEGARA(BENDERA) as URBENDERA ,
                        F_TABEL('BC30','JENIS_IDENTITAS',IDEKS) as URIDEKS ,
                        F_TABEL('BC30','STATUS_KONS',JNKONS) as URJNKONS ,
                        F_TABEL('BC30','MODA',JNSCARRIER) as URJNSCARRIER ,
                        F_TABEL('BC30','SIZE_CONT',B.SIZE) as URSIZE ,
                        DATE_FORMAT(A.TANGGAL,'%d-%m-%Y') as TANGGAL,
			DATE_FORMAT(A.TGSTUFFING,'%d-%m-%Y') as TGSTUFFING,
                        concat(substr(B.NOCONT,1,4),' - ',substr(B.NOCONT,4)) as 'NOCONT'
                FROM t_bc30pkbehdr A
                     	LEFT JOIN t_bc30pkbecon B ON A.CAR = B.CAR AND A.KODE_TRADER = B.KODE_TRADER 
                       WHERE A.CAR = '" . $this->car . "' AND A.KODE_TRADER = '" . $this->kd_trader . "'";
        $this->hdr = $this->main->get_result($sql);
        //print_r($this->hdr);die();
    } 

    function getDTL() {
        $sql = "SELECT * ,
			F_KPBC(KDKTRPEB)  as URKDKTRPEB ,
                        F_TABEL('BC30','JNS_DOK',JNDOKEKSPOR) as URJNDOKEKSPOR ,
                        F_TABEL('BC30','KATEKS',KATEKS) as URKATEKS ,
                        F_TABEL('BC30','JNS_PERIKSA',EXMERAH) as UREXMERAH ,
                        F_TABEL('BC30','JNS_RESPON',JNDOK) as URJNDOK ,
                        DATE_FORMAT(TGPEB,'%d-%m-%Y') as TGPEB,
                        DATE_FORMAT(TGPE,'%d-%m-%Y') as TGPE
                from t_bc30pkbedtl
                        WHERE CAR = '" . $this->car . "' AND KODE_TRADER = '" . $this->kd_trader . "'";
        $this->dtl = $this->main->get_result($sql, true);        
    }

    function getCON() {
        $sql = "SELECT CONTNO,CONCAT(CONTUKUR,' Feed') as CONTUKUR,CONCAT(CONTTIPE,'CL') as CONTTIPE
                FROM t_bc23con 
                WHERE CAR = '" . $this->car . "' AND KODE_TRADER = '" . $this->kd_trader . "'";
        $this->con = $this->main->get_result($sql, true);        
    }   
    
    
    function getPGT() {
        $sql = "SELECT KDBEBAN,KDFASIL,NILBEBAN
                FROM  t_bc23pgt 
                WHERE t_bc23pgt.CAR = '" . $this->car . "' AND t_bc23pgt.KODE_TRADER = '" . $this->kd_trader . "'";
        $this->pgt = $this->main->get_result($sql, true);
        //print_r($this->pgt);die();
    }
     
    function getKTR($kdKPBC){
        $SQL = "SELECT URAIAN_KPBC,KOTA,ESELON FROM m_kpbc WHERE KDKPBC = '".$kdKPBC."'";
        $this->ktr = $this->main->get_result($SQL);
        $this->ktr['KWBC'] = $this->main->get_uraian("SELECT URAIAN_KPBC FROM m_kpbc WHERE KDKPBC = '".$this->ktr['ESELON']."'",'URAIAN_KPBC');
        if($kdKPBC==='040300'){
            $this->ktr['KOP'] = "KEMENTERIAN KEUANGAN REPUBLIK INDONESIA\nDIREKTORAT JENDERAL BEA DAN CUKAI\nKANTOR PELAYANAN UTAMA TANJUNG PRIOK";
        }elseif($kdKPBC==='020400'){
            $this->ktr['KOP'] = "KEMENTERIAN KEUANGAN REPUBLIK INDONESIA\nDIREKTORAT JENDERAL BEA DAN CUKAI\nKANTOR PELAYANAN UTAMA BATAM";
        }else{
            $this->ktr['KOP'] = "KEMENTERIAN KEUANGAN REPUBLIK INDONESIA\nDIREKTORAT JENDERAL BEA DAN CUKAI\n" . (($this->ktr['KWBC']=='')?'':$this->ktr['KWBC']."\n"). str_replace('KPPBC', 'KANTOR PENGAWASAN DAN PELAYANAN',str_replace('KPU', 'KANTOR PELAYANAN UTAMA',strtoupper($this->ktr['URAIAN_KPBC'])));
        }
    }
    
    function getNTB() {
        $query = "SELECT a.*,URAIAN_KPBC as 'URKPBC' " .
                 "FROM   t_bc23ntb a Left Join m_kpbc b on a.KDKPBC = b.KDKPBC ".
                 "WHERE  CAR = '" . $this->car . "' AND KODE_TRADER = " . $this->kd_trader;
        $ntb = $this->main->get_result($query);

        $query = "SELECT AKUN_SSPCP,NILAI FROM t_bc23ntbakun WHERE CAR = '" . $this->car . "' AND KODE_TRADER = " . $this->kd_trader;
        $arrakun = $this->actMain->get_result($query, true);
        foreach ($arrakun as $a) {
            $hasil[$a['AKUN_SSPCP']] = $a['NILAI'];
            $hasil['TOTAL'] += $a['NILAI'];
        }
        $this->ntb = $hasil + $ntb; 
    }
       
    
    //PKBE
    function drawHeaderPKBE() {//
        $this->pdf->Rect(4.3, 8, 201, 235, 1.5, 'F');
        $this->pdf->SetMargins(5.4, 0, 0);
        $this->pdf->SetAutoPageBreak(0, 0);
        $this->pdf->SetY(5.4);
        $this->pdf->SetFont('times', 'B', '12');
        $this->pdf->SetX(30);
        $this->pdf->cell(131.6, 10, 'PEMBERITAHUAN KONSOLIDASI BARANG EKSPOR (PKBE)', 0, 0, 'R', 0);
        $this->pdf->SetFont('times', '', '8');
        $this->pdf->cell(43, 10, 'BCF 3.07', 0, 1, 'R', 0);
        $this->pdf->Ln(2); 
        
       $this->pdf->SetFont('times', '', '9');
       $this->pdf->cell(53, 4, 'Nomor Pengajuan', 0, 0, 'L', 0);
       $this->pdf->cell(0, 4, substr($this->hdr['CAR'], 0, 6) . " - " . substr($this->hdr['CAR'], 6, 6) . " - " . substr($this->hdr['CAR'], 12, 8) . " - " . substr($this->hdr['CAR'], 20), 0, 0, 'L', 0);
       $this->pdf->Ln(); 
       $this->pdf->cell(50, 4, 'Nomor dan Tanggal Pendaftaran', 0, 0, 'L', 0);
       $this->pdf->cell(3, 4, ':', 0, 0, 'L', 0);
       $this->pdf->cell(0, 4, $this->hdr['KDKTR'].' / '.$this->hdr['TANGGAL'], 0, 0, 'L', 0);
       $this->pdf->Ln(); 
       $this->pdf->cell(50, 4, 'Merek/Nomor Peti Kemas', 0, 0, 'L', 0);
       $this->pdf->cell(3, 4, ':', 0, 0, 'L', 0);
       $this->pdf->cell(35, 4, $this->hdr['NOCONT'], 1, 0, 'L', 0);
       $this->pdf->Ln(5); 
       $this->pdf->cell(50, 4, 'Ukuran Peti Kemas', 0, 0, 'L', 0);
       $this->pdf->cell(3, 4, ':', 0, 0, 'L', 0);
       $this->pdf->cell(0, 4, $this->hdr['URSIZE'], 0, 0, 'L', 0);
       $this->pdf->Ln(); 
       $this->pdf->cell(56.9, 4, 'Tempat & Tanggal Pelaksanaan Stuffering', 0, 0, 'L', 0);
       $this->pdf->cell(3, 4, ':', 0, 0, 'L', 0);
       $this->pdf->cell(0, 4, $this->hdr['ALMTSTUFFING'].' / '.$this->hdr['TGSTUFFING'], 0, 0, 'L', 0);
       $this->pdf->Ln();
      
       $this->pdf->Rect(7, 40, 98, 30, 1.5, 'F');
       $this->pdf->SetFont('times', '', '9');
       $this->pdf->SetX(7);
       $this->pdf->cell(99.6, 8.5, 'PIHAK YANG MELAKUKAN KONSOLIDASI', 0, 0, 'L', 0);
       
       $this->pdf->setX(105);
       $this->pdf->cell(2, 8, 'KANTOR PABEAN', 0, 0, 'L', 0);
       $this->pdf->cell(33, 13, 'PEMUATAN ASAL', 0, 0, 'L', 0);
       $this->pdf->cell(2,11 , ':', 0, 0, 'L', 0);
       $this->pdf->cell(2,11.8 ,$this->hdr['URKDKTRMUAT'], 0, 0, 'L', 0);
       $this->pdf->Ln(5); 
       
       $this->pdf->SetX(7);
       $this->pdf->cell(25, 8.5, 'NPWP', 0, 0, 'L', 0);
       $this->pdf->cell(2, 8.5, ':', 0, 0, 'L', 0);
       $this->pdf->cell(0, 8.5, $this->formatNPWP($this->hdr['NPWPEKS']), 0, 0, 'L', 0);
       
       $this->pdf->setX(105);
       $this->pdf->cell(2, 12, 'KANTOR PABEAN', 0, 0, 'L', 0);
       $this->pdf->cell(33, 17, 'PEMUATAN EKSPOR', 0, 0, 'L', 0);
       $this->pdf->cell(2,15 , ':', 0, 0, 'L', 0);
       $this->pdf->cell(2,15 ,$this->hdr['URKDKTRMUAT'], 0, 0, 'L', 0);
       $this->pdf->Ln(5); 
       
       $this->pdf->SetX(7);
       $this->pdf->cell(25, 8.5, 'NAMA', 0, 0, 'L', 0);
       $this->pdf->cell(2, 8.5, ':', 0, 0, 'L', 0);
       $this->pdf->cell(0, 8.5, $this->hdr['NAMAEKS'], 0, 0, 'L', 0); 
       
       $this->pdf->setX(105);
       $this->pdf->cell(35, 17, 'NEGARA TUJUAN', 0, 0, 'L', 0);
       $this->pdf->cell(2,16 , ':', 0, 0, 'L', 0);
       $this->pdf->cell(2,16 ,$this->hdr['URKDNEGTUJU'], 0, 0, 'L', 0);
       $this->pdf->Ln(5);     
       
       $this->pdf->SetX(7);
       $this->pdf->cell(25, 8.5, 'ALAMAT', 0, 0, 'L', 0);
       $this->pdf->cell(2, 8.5, ':', 0, 0, 'L', 0);
       $this->pdf->cell(0, 8.5, $this->hdr['ALMTEKS'], 0, 0, 'L', 0);
       
       $this->pdf->setX(105);
       $this->pdf->cell(48, 17, 'NAMA SARANA PENGANGKUT', 0, 0, 'L', 0);
       $this->pdf->cell(2,16 , ':', 0, 0, 'L', 0);
       $this->pdf->cell(2,16 ,$this->hdr['CARRIER'], 0, 0, 'L', 0);
       
       $this->pdf->setX(105);
       $this->pdf->cell(35, 25, 'NO. VOY / FLIGHT', 0, 0, 'L', 0);
       $this->pdf->cell(2,24.5 , ':', 0, 0, 'L', 0);
       $this->pdf->cell(2,24.5 ,$this->hdr['VOYFLIGHT'], 0, 0, 'L', 0);
       $this->pdf->Rect(105, 40, 98, 30, 1.5, 'F');
       
       
       $this->pdf->SetY(72);     
       $this->pdf->multicell(10, 6, "No. \n Urut", 1, 'C');
       $this->pdf->SetXY(15.5,72);
       $this->pdf->cell(60, 6, 'PEB', 1, 0, 'C', 0);
       $this->pdf->cell(68, 6, 'NPE', 1, 0, 'C', 0); 
       $this->pdf->cell(59.7, 12, 'Keterangan', 1, 0, 'C', 0);
       $this->pdf->setXY(15.5,78);
       $this->pdf->cell(30, 6, 'Nomor', 1, 0, 'C', 0);
       $this->pdf->setXY(45.6,78);
       $this->pdf->cell(29.8, 6, 'Tanggal', 1, 0, 'C', 0);
       $this->pdf->setXY(75.5,78);
       $this->pdf->cell(38, 6, 'Nomor', 1, 0, 'C', 0);
       $this->pdf->setXY(113.5,78);
       $this->pdf->cell(30, 6, 'Tanggal', 1, 0, 'C', 0);
       $this->pdf->SetY(84.1);   
       $this->pdf->cell(10, 6, '1', 1, 0 , 'C', 0);
       $this->pdf->SetXY(15.5,84.1);
       $this->pdf->cell(30, 6, '3', 1, 0 , 'C', 0);
       $this->pdf->setXY(45.6,84.1);
       $this->pdf->cell(29.8, 6, '4', 1, 0, 'C', 0);
       $this->pdf->setXY(75.5,84.1);
       $this->pdf->cell(38, 6, '5', 1, 0, 'C', 0);
       $this->pdf->setXY(113.5,84.1);
       $this->pdf->cell(30, 6, '6', 1, 0, 'C', 0);
       $this->pdf->setXY(143.5,84.1);
       $this->pdf->cell(59.7, 6, '7', 1, 0, 'C', 0);
       #isi 
       #NO URUT
       $this->pdf->Rect(5.4, 90, 10, 50, 1.5, 'F');
       #NOMOR
       $this->pdf->Rect(15.5, 90, 30, 50, 1.5, 'F');
       #TANGGAL
       $this->pdf->Rect(45.6, 90, 29.9, 50, 1.5, 'F');
       #NOMOR
       $this->pdf->Rect(75.5, 90, 38, 50, 1.5, 'F');
       #TANGGGAL
       $this->pdf->Rect(113.5, 90, 30, 50, 1.5, 'F');
       #KETERANGAN
       $this->pdf->Rect(143.5, 90, 59.7, 50, 1.5, 'F');
       $this->pdf->SetY(90.5);
       $this->dataLampiranDetil();
       
       $this->pdf->Ln(65); 
       
       $this->pdf->Rect(5.2, 145, 99, 45, 1.5, 'F');    
       $this->pdf->SetFont('times', '', '9');
       $this->pdf->SetX(7);
       $this->pdf->cell(99.6, 8.5, 'Petugas Pengawas Stuffering', 0, 0, 'L', 0);
       
      
       $this->pdf->multicell(99.6, 5, "Dengan ini saya menyatakan bertanggung jawab atas kebenaran hak-hal \n yang diberitahukan dalam dokumen ini.", 0, 'C');
       $this->pdf->Ln(3); 
       
       $this->pdf->SetXY(7,160);
       $this->pdf->cell(25, 8.5, '(tanda tangan)', 0, 0, 'L', 0);
       $this->pdf->cell(2, 8.5, ':', 0, 0, 'L', 0);
       $this->pdf->cell(0, 8.5, $this->formatNPWP($this->hdr['NPWPEKS']), 0, 0, 'L', 0);
            
       $this->pdf->setX(90);
       $this->pdf->cell(0, 5, 'JAKARTA' .', '. substr($this->hdr['CAR'], 18,2).'-'.substr($this->hdr['CAR'], 16,2).'-'.substr($this->hdr['CAR'], 12,4), 0, 0, 'C', 0);
       $this->pdf->Ln(5); 
       
       $this->pdf->SetX(7);
       $this->pdf->cell(25, 8.5, 'Nama', 0, 0, 'L', 0);
       $this->pdf->cell(2, 8.5, ':', 0, 0, 'L', 0);
       $this->pdf->cell(0, 8.5, $this->hdr['NAMAEKS'], 0, 0, 'L', 0); 
       
       
       $this->pdf->Ln(5);     
       
       $this->pdf->SetX(7);
       $this->pdf->cell(25, 8.5, 'NIP', 0, 0, 'L', 0);
       $this->pdf->cell(2, 8.5, ':', 0, 0, 'L', 0);
       $this->pdf->cell(0, 8.5, $this->hdr['ALMTEKS'], 0, 0, 'L', 0);
       
       $this->pdf->setX(112);
       $this->pdf->cell(80, 30, '-                                                           -', 0, 0, 'C', 0);
       $this->pdf->Rect(104.3, 145, 99, 45, 1.5, 'F');  
       
       $this->pdf->Ln(21);
       $this->pdf->Rect(5.2, 145, 99, 90, 1.5, 'F');
       $this->pdf->SetFont('times', '', '9');
       $this->pdf->multicell(100, 3, 'CATATAN PEMASUKAN BARANG EKSPOR KE KAWASAN PABEAN', 0, 'L');
       $this->pdf->SetXY(104.5,191);
       $this->pdf->multicell(100, 3, 'CATATAN PEMASUKAN BARANG EKSPOR KE SARANA PENGANGKUT', 0, 'L');
       
       $this->pdf->Ln();
       $this->pdf->setX(5);
       $this->pdf->cell(20, 8.5, 'SEGEL :', 0, 0, 'L', 0);
       $this->pdf->cell(5, 5, '', 1, 0, 'L', 0);
       $this->pdf->cell(15, 8.5, 'Baik', 0, 0, 'L', 0);
       $this->pdf->cell(5, 5, '', 1, 0, 'L', 0);
       $this->pdf->cell(15, 8.5, 'Rusak', 0, 0, 'L', 0);
       $this->pdf->cell(5, 5, '', 1, 0, 'L', 0);
       $this->pdf->cell(15, 8.5, 'Tidak Sesuai', 0, 0, 'L', 0);
       $this->pdf->Ln();
       $this->pdf->multicell(95, 3, "Selesai masuk tanggal : ............................... Pukul ......................\nPetugas Dinas Luar", 0, 'L');
       $this->pdf->Ln();
       $this->pdf->cell(22, 6, '(tanda tangan) :', 0, 1, 'C', 0);
       $this->pdf->cell(22, 6, 'Nama               :', 0, 1, 'C', 0);
       $this->pdf->cell(22, 6, 'NIP                  :', 0, 1, 'C', 0);
       $this->pdf->Rect(104.3, 145, 99, 90, 1.5, 'F');
       
       
       $this->pdf->Ln(1);
       $this->pdf->SetXY(104,200);
       $this->pdf->cell(20, 8.5, 'SEGEL :', 0, 0, 'L', 0);
       $this->pdf->cell(5, 5, '', 1, 0, 'L', 0);
       $this->pdf->cell(15, 8.5, 'Baik', 0, 0, 'L', 0);
       $this->pdf->cell(5, 5, '', 1, 0, 'L', 0);
       $this->pdf->cell(15, 8.5, 'Rusak', 0, 0, 'L', 0);
       $this->pdf->cell(5, 5, '', 1, 0, 'L', 0);
       $this->pdf->cell(15, 8.5, 'Tidak Sesuai', 0, 0, 'L', 0);
       $this->pdf->SetXY(104,208.5);
       $this->pdf->multicell(95, 3, "Selesai masuk tanggal : ............................... Pukul ......................\nPetugas Dinas Luar", 0, 'L');
       $this->pdf->Ln();
       $this->pdf->SetXY(104.3,217.7);
       $this->pdf->cell(22, 6, '(tanda tangan) :', 0, 0, 'C', 0);
       $this->pdf->SetXY(104.3,223.5);
       $this->pdf->cell(22, 6, 'Nama               :', 0, 0, 'C', 0);
       $this->pdf->SetXY(104.3,229.8);
       $this->pdf->cell(22, 6, 'NIP                  :', 0, 0, 'C', 0);
       $this->pdf->Rect(104.3, 145, 99, 90, 1.5, 'F', 1);
       $this->pdf->Ln(); 
       $this->pdf->SetX(173);
       $this->pdf->SetFont('times', 'I', '9');
       $this->pdf->cell(15, 8.5, 'Tgl Cetak : '. date('m/d/Y') , 0, 0, 'L', 0);
       $this->pdf->Ln(20);
       $this->pdf->cell(198, 6, '-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------', 0, 0, 'C', 0);   
    }

    function dataLampiranDetil() {
        $this->pdf->SetFont('times', '', '8');
        $Yawl = $this->pdf->getY();
        $Y = $this->pdf->getY(); 
        $totData = count($this->dtl);//print_r($this->dtl);die();
        for ($i = $idPrint; $i < $totData; $i++) {
            
            $this->pdf->setXY($this->pdf->getX() + 0, $Yawl);
            $this->pdf->cell(10, 4, ($i + 1), 0, 0, 'C', 0);
            #45
            $this->pdf->setXY($this->pdf->getX() + 0, $Yawl);
            $this->pdf->multicell(29.5, 4, $this->dtl[$i]['KDKTRPEB'].'/'.$this->dtl[$i]['NOPEB'], 0, 'L');
            $h[] = $this->pdf->getY();
            #45
            $this->pdf->setXY($this->pdf->getX() + 40.2, $Yawl);
            $this->pdf->multicell(29.5, 4, $this->dtl[$i]['TGPEB'], 0, 'L');
            $h[] = $this->pdf->getY();
            #45
            $this->pdf->setXY($this->pdf->getX() + 70.2, $Yawl);
            $this->pdf->multicell(37, 4, $this->dtl[$i]['NOPE'], 0, 'L');
            $h[] = $this->pdf->getY();
            #45
            $this->pdf->setXY($this->pdf->getX() + 108.1, $Yawl);
            $this->pdf->multicell(29.5, 4, $this->dtl[$i]['TGPE'], 0, 'L');
            $h[] = $this->pdf->getY();
            #45
            $this->pdf->setXY($this->pdf->getX() + 138.1, $Yawl);
            $this->pdf->multicell(59, 4, $this->dtl[$i]['KETERANGAN'], 0, 'C');
            $h[] = $this->pdf->getY();
            
            $Yawl = max($h) + 2;
            if ($Yawl > 230) {
                $i++;
                break;
            }
        }
    }

    function dataLampiranDokumen($noPrint, $idPrint = 0) {
        $this->headerLampiran('dokumen', 'LANJUTAN DOKUMEN');
        $this->pdf->SetFont('times', '', '8');
        $Yawl = $this->pdf->getY();
        $Y = $this->pdf->getY();
        $totData = count($this->dok);
        for ($i = $idPrint; $i < $totData; $i++) {
            if (!strstr($noPrint, $this->dok[$i]['DOKKD'])) {
                $h = '';
                $this->pdf->setY($Yawl);
                //Uraian Kode Dokumen
                $this->pdf->cell(10, 4, '', 0, 0, 'L', 0);
                $this->pdf->multicell(65, 4, $this->dok[$i]['URDOKKD'], 0, 'L');
                $h[] = $this->pdf->getY();
                //Nomor Dokumen
                $this->pdf->setXY($this->pdf->getX() + 75, $Yawl);
                $this->pdf->multicell(64.2, 4, $this->dok[$i]['DOKNO'], 0, 'L');
                $h[] = $this->pdf->getY();
                //Tanggal Dokumen
                $this->pdf->setXY($this->pdf->getX() + 139.2, $Yawl);
                $this->pdf->multicell(60, 4, $this->dok[$i]['DOKTG'], 0, 'L');
                $h[] = $this->pdf->getY();

                $Yawl = max($h);
                if ($Yawl > 230) {
                    $i++;
                    break;
                }
            }
        }
        $this->pdf->Rect(5.4, $Y, 199.3, ($Yawl - $Y));
        $this->footerLampiran();
        if ($i < $totData) {
            $this->dataLampiranDokumen($noPrint, $i);
        }
    }

    function dataLampiranKemasan($idPrint = 0) {
        $this->headerLampiran('kemasan', 'LANJUTAN KEMASAN');
        $this->pdf->SetFont('times', '', '8');
        $Yawl = $this->pdf->getY();
        $Y = $this->pdf->getY(); //print_r($this->kms);die();
        $totData = count($this->kms);
        for ($i = $idPrint; $i < $totData; $i++) {
            $h = '';
            $this->pdf->setY($Yawl);
            //Uraian Kode Dokumen
            $this->pdf->cell(10, 4, '', 0, 0, 'L', 0);
            $this->pdf->multicell(35, 4, $this->kms[$i]['JMKEMAS'], 0, 'R');
            $h[] = $this->pdf->getY();
            //Nomor Dokumen
            $this->pdf->setXY($this->pdf->getX() + 45, $Yawl);
            $this->pdf->multicell(79.2, 4, $this->kms[$i]['JNKEMAS'] . ' / ' . $this->kms[$i]['URJNKEMAS'], 0, 'L');    
            $h[] = $this->pdf->getY();
            //Tanggal Dokumen
            $this->pdf->setXY($this->pdf->getX() + 124.2, $Yawl);
            $this->pdf->multicell(75, 4, $this->kms[$i]['MERKKEMAS'], 0, 'L');
            $h[] = $this->pdf->getY();
            $Yawl = max($h);
            if ($Yawl > 230) {
                $i++;
                break;
            }
        }
        $this->pdf->Rect(5.4, $Y, 199.3, ($Yawl - $Y));
        $this->footerLampiran();
        if ($i < $totData) {
            $this->dataLampiranKemasan($i);
        }
    }

    function dataLampiranKontainer($idPrint = 0) {
        $this->headerLampiran('kontainer', 'LANJUTAN KEMASAN');
        $this->pdf->SetFont('times', '', '8');
        $totData = count($this->con);
        $Y = $this->pdf->getY();
        for ($i = $idPrint; $i < $totData; $i++) {
            $this->pdf->Ln();
            $this->pdf->cell(12.6, 4, ($i + 1), 0, 0, 'C', 0);
            $this->pdf->cell(37, 4, $this->setnocont($this->con[$i]['CONTNO']), 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, $this->con[$i]['CONTUKUR'], 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, $this->con[$i]['CONTTIPE'], 0, 0, 'L', 0);
            $i++;
            $this->pdf->cell(12.6, 4, ($this->con[$i]['CONTNO'] != '') ? ($i + 1) : '', 'L', 0, 'C', 0);
            $this->pdf->cell(37, 4, $this->setnocont($this->con[$i]['CONTNO']), 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, $this->con[$i]['CONTUKUR'], 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, $this->con[$i]['CONTTIPE'], 0, 0, 'L', 0);
        }
        $this->pdf->Ln();
        $this->pdf->Rect(5.4, $Y, 199.3, ( $this->pdf->getY() - $Y));
        $this->footerLampiran();
        if ($i < $totData) {
            $this->dataLampiranKontainer($i);
        }
    }

    function footerLampiran() {
        $this->pdf->setXY(150, 255);
        $this->pdf->SetFont('times', '', '9');
        $imp = ($this->tp_trader == '1') ? 'Importir' : 'PPJK';
        $this->pdf->multicell(50, 4, "Jakarta, " . $this->hdr['TANGGAL_TTD'] . "\n" . $imp . "\n\n\n\n\n\n" . $this->hdr['NAMA_TTD'], 0, 'C');
        $this->pdf->SetFont('times', 'I', '9');
        $this->pdf->multicell(99.6, 4, "Tgl.Cetak " . date('d-m-Y'), 0, 'L');
    }
    
    function drawLembarCatPencocokan(){        
        $noPrinLampiranDok = '';
        $this->pdf->SetMargins(5.4, 0, 0);
        $this->pdf->SetAutoPageBreak(0, 0);
        $this->pdf->SetY(5.4);
        $this->pdf->SetFont('times', 'B', '12');        
        $this->pdf->cell(200, 4, 'LEMBAR LAMPIRAN III', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(200, 4, 'PEMBERITAHUAN IMPOR BARANG UNTUK DITIMBUN ', 0, 0, 'C', 0);
        $this->pdf->Ln(); 
        $this->pdf->cell(200, 4, 'DI TEMPAT PENIMBUNAN BERIKAT', 0, 0, 'C', 0);              
        $this->pdf->Ln(10);         
        $this->pdf->cell(200, 4, 'UNTUK CATATAN PENCOCOKAN', 0, 0, 'C', 0);
        $this->pdf->SetX(137);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->cell(67.6, 4, 'BC 2.3', 10, 0, 'R', 0);
        
        //KPBC
        $this->pdf->Ln(2);                 
        $this->pdf->Rect(5.4, 29.4, 199, 14, 1.5, 'F');
        $this->pdf->Ln();
        $this->pdf->cell(45, 4, 'Kantor Pabean', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(103, 4, $this->hdr['URKDKPBC'], 0, 0, 'L', 0);
        $this->pdf->cell(15, 4, $this->hdr['KDKPBC'], 1, 0, 'C', 0);                
        $this->pdf->Ln(); 
        $this->pdf->cell(45, 4, 'Nomor Pengajuan', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(80, 4, substr($this->hdr['CAR'], 0, 6) . " - " . substr($this->hdr['CAR'], 6, 6) . " - " . substr($this->hdr['CAR'], 12, 8) . " - " . substr($this->hdr['CAR'], 20), 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(45, 4, 'Nomor Pendaftaran', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(103, 4, $this->hdr[''], 0, 0, 'L', 0);
        $this->pdf->setX(73);
        $this->pdf->cell(15, 4, 'Tgl.', 0, 0, 'C', 0);               
        
        $this->pdf->Ln(7);
        $this->pdf->cell(45, 4, 'DIISI DALAM HAL DILAKUKAN  PENCOCOKAN JUMLAH & JENIS KEMASAN/PETI KEMAS', 0, 0, 'L', 0);
        $this->pdf->Ln(10);
        $this->pdf->cell(20, 4, 'PETUGAS', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(20, 4, 'NAMA', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(20, 4, 'NIP', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        
        $this->pdf->Ln();
        $this->pdf->setX(110);
        $yAWL = $this->pdf->getY();
        $this->pdf->multicell(49.3, 4, "...................................", 0, 'C');
        $this->pdf->setY($yAWL);
        $this->pdf->setX(141);
        $this->pdf->multicell(49.3, 4, "Tgl ...................................", 0, 'C');        
        $this->pdf->setX(120);        
        $this->pdf->cell(49.3, 4, 'Pejabat', 0, 0, 'L', 0);
        $this->pdf->Ln(8);
        $this->pdf->setX(120);        
        $this->pdf->cell(49.3, 4, 'ttd', 0, 0, 'L', 0);
        
        $this->pdf->Ln(10);
        $this->pdf->setX(120);
        $this->pdf->cell(20, 4, 'NAMA', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->setX(120);
        $this->pdf->cell(20, 4, 'NIP.', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        
        $this->pdf->Ln(10);
        $this->pdf->cell(45, 4, 'TEMPAT PENCOCOKAN', 0, 0, 'L', 0);              
        $this->pdf->setX(120);
        $this->pdf->cell(45, 4, 'TGL. PENCOCOKAN', 0, 0, 'L', 0);
        
        $this->pdf->Rect(5.4, 43.5, 199, 235, 1.5, 'F');
        
        $this->pdf->Ln(120);
        $this->pdf->setX(110);
        $yAWL = $this->pdf->getY();
        $this->pdf->multicell(49.3, 4, "...................................", 0, 'C');
        $this->pdf->setY($yAWL);
        $this->pdf->setX(141);
        $this->pdf->multicell(49.3, 4, "Tgl ...................................", 0, 'C');        
        $this->pdf->setX(120);        
        $this->pdf->cell(49.3, 4, 'Pemeriksa Bea dan Cukai', 0, 0, 'L', 0);
        $this->pdf->Ln(8);
        $this->pdf->setX(120);        
        $this->pdf->cell(49.3, 4, 'ttd', 0, 0, 'L', 0);
        
        $this->pdf->Ln(10);
        $this->pdf->setX(120);
        $this->pdf->cell(20, 4, 'NAMA', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->setX(120);
        $this->pdf->cell(20, 4, 'NIP', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
    }
    
    function drawLembarCatPemerikasaanFisikBrg(){
        $noPrinLampiranDok = '';
        $this->pdf->SetMargins(5.4, 0, 0);
        $this->pdf->SetAutoPageBreak(0, 0);
        $this->pdf->SetY(5.4);
        $this->pdf->SetFont('times', 'B', '12');        
        $this->pdf->cell(200, 4, 'LEMBAR LAMPIRAN IV', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(200, 4, 'PEMBERITAHUAN IMPOR BARANG UNTUK DITIMBUN ', 0, 0, 'C', 0);
        $this->pdf->Ln(); 
        $this->pdf->cell(200, 4, 'DI TEMPAT PENIMBUNAN BERIKAT', 0, 0, 'C', 0);              
        $this->pdf->Ln(10);         
        $this->pdf->cell(200, 4, 'UNTUK CATATAN PEMERIKSAAN FISIK BARANG', 0, 0, 'C', 0);
        
        $this->pdf->SetX(137);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->cell(67.6, 4, 'BC 2.3', 10, 0, 'R', 0);
        //KPBC
        $this->pdf->Ln(2);                 
        $this->pdf->Rect(5.4, 29.4, 199, 14, 1.5, 'F');
        $this->pdf->Ln();
        $this->pdf->cell(45, 4, 'Kantor Pabean', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(103, 4, $this->hdr['URKDKPBC'], 0, 0, 'L', 0);
        $this->pdf->cell(15, 4, $this->hdr['KDKPBC'], 1, 0, 'C', 0);                
        $this->pdf->Ln();
        $this->pdf->cell(45, 4, 'Nomor Pengajuan', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(80, 4, substr($this->hdr['CAR'], 0, 6) . " - " . substr($this->hdr['CAR'], 6, 6) . " - " . substr($this->hdr['CAR'], 12, 8) . " - " . substr($this->hdr['CAR'], 20), 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(45, 4, 'Nomor Pendaftaran', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(103, 4, $this->hdr[''], 0, 0, 'L', 0);
        $this->pdf->setX(73);
        $this->pdf->cell(15, 4, 'Tgl.', 0, 0, 'C', 0);               
        
        $this->pdf->Ln(7);
        $this->pdf->cell(45, 4, 'DIISI DALAM HAL DILAKUKAN  PEMERIKSAAN FISIK BARANG', 0, 0, 'L', 0);
        $this->pdf->Ln(10);
        $this->pdf->cell(20, 4, 'PETUGAS', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(20, 4, 'NAMA', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(20, 4, 'NIP', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(20, 4, 'TINGKAT PEMERIKSAAN', 0, 0, 'L', 0);        
        
        $this->pdf->Ln();
        $this->pdf->setX(110); 
        $yAWL = $this->pdf->getY();
        $this->pdf->multicell(49.3, 4, "...................................", 0, 'C');
        $this->pdf->setY($yAWL);
        $this->pdf->setX(141);
        $this->pdf->multicell(49.3, 4, "Tgl ...................................", 0, 'C');        
        $this->pdf->setX(120);        
        $this->pdf->cell(49.3, 4, 'Pejabat', 0, 0, 'L', 0);
        $this->pdf->Ln(8);
        $this->pdf->setX(120);        
        $this->pdf->cell(49.3, 4, 'ttd', 0, 0, 'L', 0);
        
        $this->pdf->Ln(10);
        $this->pdf->setX(120);
        $this->pdf->cell(20, 4, 'NAMA', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->setX(120);
        $this->pdf->cell(20, 4, 'NIP', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        
        $this->pdf->Ln(10);
        $this->pdf->cell(45, 4, 'TEMPAT PEMERIKSAAN FISIK', 0, 0, 'L', 0);              
        $this->pdf->setX(120);
        $this->pdf->cell(45, 4, 'TGL. DILAKUKAN PEMERIKSAAN FISIK BARANG', 0, 0, 'L', 0);
        $this->pdf->Ln(10   );
        $this->pdf->cell(45, 4, 'IKHTISAR PEMERIKSAAN', 0, 0, 'L', 0);
        
        $this->pdf->Rect(5.4, 43.5, 199, 235, 1.5, 'F');
        
        $this->pdf->Ln(120);
        $this->pdf->setX(110);
        $yAWL = $this->pdf->getY();
        $this->pdf->multicell(49.3, 4, "...................................", 0, 'C');
        $this->pdf->setY($yAWL);
        $this->pdf->setX(141);
        $this->pdf->multicell(49.3, 4, "Tgl ...................................", 0, 'C');        
        $this->pdf->setX(120);        
        $this->pdf->cell(49.3, 4, 'Pemeriksa Bea dan Cukai', 0, 0, 'L', 0);
        $this->pdf->Ln(8);
        $this->pdf->setX(120);        
        $this->pdf->cell(49.3, 4, 'ttd', 0, 0, 'L', 0);
        
        $this->pdf->Ln(10);
        $this->pdf->setX(120);
        $this->pdf->cell(20, 4, 'NAMA', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->setX(120);
        $this->pdf->cell(20, 4, 'NIP', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        
    }
    /* ------------------ fungsi pendukung --------------------- */

    function trimstr($strpotong, $panjang) {
        $hsl = '';
        $strpotong = trim($strpotong);
        $len = strlen($strpotong);
        $str = 0;
        while ($str < $len) {
            $hsl[] = substr($strpotong, $str, $panjang);
            $str += $panjang;
        }
        return $hsl;
    }

    function formatNPWP($npwp) {
        $strlen = strlen($npwp);
        if ($strlen == 15) {
            $npwpnya = substr($npwp, 0, 2) . "." . substr($npwp, 2, 3) . "." . substr($npwp, 5, 3) . "." . substr($npwp, 8, 1) . "-" . substr($npwp, 9, 3) . "." . substr($npwp, 12, 3);
        } else if ($strlen == 12) {
            $npwpnya = substr($npwp, 0, 2) . "." . substr($npwp, 2, 3) . "." . substr($npwp, 5, 3) . "." . substr($npwp, 8, 1) . "-" . substr($npwp, 9, 3);
        } else {
            $npwpnya = $npwp;
        }
        return $npwpnya;
    }

    function setnocont($nocont) {
        return substr($nocont, 0, 4) . '-' . substr($nocont, 4, 11);
    }

    function formaths($hs) {
        $formaths = substr($hs, 0, 4) . '.' . substr($hs, 4, 2) . '.' . substr($hs, 6, 4);
        return $formaths;
    }

    function strip($strstrip) {
        if (trim($strstrip) != 0) {
            $hasile = $strstrip . '%';
        } else {
            $hasile = ' - ';
        }
        return $hasile;
    }

    function showDok($jnDok, $arrDok) {//'380|365|861'
        $hasil = array();
        foreach ($arrDok as $a) {
            if (strstr($jnDok, $a['DOKKD'])) {
                $hasil['NO'][] = $a['DOKNO'];
                $hasil['TG'][] = $a['DOKTG'];
            }
        }
        return $hasil;
    }

    function formatDokArr($arr) {
        $hasil = '';
        foreach ($arr as $a) {
            $hasil[$a['DOKKD']]['NO'][] = $a['DOKNO'];
            $hasil[$a['DOKKD']]['TG'][] = $a['DOKTG'];
        }
        return $hasil;
    }

    function getfas($kodefas) {
        switch ($kodefas) {
            case 1:
                $kdfaspbm_nama = 'DTP';
                break;
            case 2:
                $kdfaspbm_nama = 'DTG';
                break;
            case 3:
                $kdfaspbm_nama = 'BKL';
                break;
            case 4:
                $kdfaspbm_nama = 'BBS';
                break;
        }
        return $kdfaspbm_nama;
    }

    function terbilang($bilangan) {
        $angka = array('0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
        $kata = array('', 'satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 'tujuh', 'delapan', 'sembilan');
        $tingkat = array('', 'ribu', 'juta', 'milyar', 'triliun');
        $panjang_bilangan = strlen($bilangan);

        if ($panjang_bilangan > 15) {
            $kalimat = "Diluar Batas";
            return $kalimat;
        }

        /* mengambil angka-angka yang ada dalam bilangan, dimasukkan ke dalam array */
        for ($i = 1; $i <= $panjang_bilangan; $i++) { 
            $angka[$i] = substr($bilangan, -($i), 1);
        }

        $i = 1;
        $j = 0;
        $kalimat = "";

        /* mulai proses iterasi terhadap array angka */
        while ($i <= $panjang_bilangan) {
            $subkalimat = "";
            $kata1 = "";
            $kata2 = "";
            $kata3 = "";

            /* untuk ratusan */
            if ($angka[$i + 2] != "0") {
                if ($angka[$i + 2] == "1") {
                    $kata1 = "seratus";
                } else {
                    $kata1 = $kata[$angka[$i + 2]] . " ratus";
                }
            }

            /* untuk puluhan atau belasan */
            if ($angka[$i + 1] != "0") {
                if ($angka[$i + 1] == "1") {
                    if ($angka[$i] == "0") {
                        $kata2 = "sepuluh";
                    } elseif ($angka[$i] == "1") {
                        $kata2 = "sebelas";
                    } else {
                        $kata2 = $kata[$angka[$i]] . " belas";
                    }
                } else {
                    $kata2 = $kata[$angka[$i + 1]] . " puluh";
                }
            }

            /* untuk satuan */
            if ($angka[$i] != "0") {
                if ($angka[$i + 1] != "1") {
                    $kata3 = $kata[$angka[$i]];
                }
            }

            /* pengujian angka apakah tidak nol semua, lalu ditambahkan tingkat */
            if (($angka[$i] != "0") OR ( $angka[$i + 1] != "0") OR ( $angka[$i + 2] != "0")) {
                $subkalimat = "$kata1 $kata2 $kata3 " . $tingkat[$j] . " ";
            }

            /* gabungkan variabel sub kalimat (untuk satu blok 3 angka) ke variabel kalimat */
            $kalimat = $subkalimat . $kalimat;
            $i = $i + 3;
            $j = $j + 1;
        }
        /* mengganti satu ribu jadi seribu jika diperlukan */
        if (($angka[5] == "0") AND ( $angka[6] == "0")) {
            $kalimat = str_replace("satu ribu", "seribu", $kalimat);
        }
        return trim($kalimat);
    }

    function formatTglMysql($tglMysql){
        $arr = explode('-', $tglMysql);
        return $arr[2].' '.$this->bulan[(int)$arr[1]].' '.$arr[0];
    }
    
    function formatCar($CAR){
        //$arr = explode('-', $tglMysql);
        return substr($CAR, 0,6).'-'.substr($CAR, 6,6).'-'.substr($CAR, 12,8).'-'.substr($CAR, 20,6);
    }
    
    
    function fixLen($str, $len, $chr = ' ', $alg = STR_PAD_RIGHT) {
        $hasil = str_pad(substr($str, 0, $len), $len, $chr, $alg);
        return $hasil;
    }
    
    /* ------------------ fungsi pendukung --------------------- */
   /* 
    private function isiRespon(){
        
        $this->rpt['LBLKOP']        = $this->ktr['KOP'];
        $this->rpt['LBLNODAFTAR']   = $this->res['CAR'];
        $this->rpt['LABEL33']       = $this->res['PIBTG'];
        $this->rpt['LBLTGLDAFTAR']  = substr($this->res['CAR'],18,2).'-'.substr($this->res['CAR'],16,2).'-'.substr($this->res['CAR'],12,4);
        $this->rpt['LBLDESKRIPSI']  = $this->res['DESKRIPSI'];
        
        $this->rpt['IMPNPWP']       = $this->formatNPWP($this->hdr['IMPNPWP']);
        $this->rpt['IMPNAMA']       = $this->hdr['IMPNAMA'];
        $this->rpt['IMPALMT']       = $this->hdr['IMPALMT']."\n".$this->hdr['INDALMT'];
        
        $this->rpt['PPJKNPWP']      = $this->formatNPWP($this->hdr['PPJKNPWP']);
        $this->rpt['PPJKNAMA']      = $this->hdr['PPJKNAMA'];
        $this->rpt['PPJKALMT']      = $this->hdr['PPJKALMT'];
        $this->rpt['PPJKNP']        = $this->hdr['PPJKNO'];
        $this->rpt['LBL_13']        = $this->hdr['PIBNO'];
        switch($this->res['RESKD']){
            case'011':case'110':
                if($this->res['RESKD']=='110'){
                    $this->rpt['LBLJUDUL']= 'NOTA PEMBERITAHUAN PENOLAKAN (NPP)' ;
                }
                else{
                    $this->rpt['LBLJUDUL']= 'NOTA PEMBERITAHUAN PENOLAKAN (NPP) - NSW';
                }
                $this->rpt['LABEL28'] = '';
                $this->rpt['LABEL29'] = '';
                $this->rpt['LABEL30'] = '';
                $this->rpt['LABEL31'] = '';
                break;
            case'100':case'010': //Penerimaan PIB
                if($this->res['RESKD']==='100'){
                    $this->rpt['LBLJUDUL']= 'PENERIMAAN PIB' ;
                }
                else{
                    $this->rpt['LBLJUDUL']= 'PENERIMAAN PIB - NSW';
                }
                $this->rpt['LBL_11'] = 'Diterima pada    : ';
                $this->rpt['LBL_13'] = $this->res['RESTG'].' '.$this->res['RESWK'];
                $this->rpt['LBLNARASI'] = 'Catatan      : ';
                break;
            case'200'://Penerimaan PIB 
                $this->rpt['LBLJUDUL']= 'KONFIRMASI JAMINAN' ;
                $this->rpt['LBLNARASI']= 'Sehubungan dengan belum adanya surat tanda jaminan atas PIB Saudara Nomor Pengajuan seperti di atas, dengan ini Saudara diminta agar mengirimkan / menyampaikan dokumen bukti jaminan kepada Pejabat Pengelola Jaminan' ;
                $this->rpt['LBL_11']= 'Waktu Konfirmasi  : ' ;
                $this->rpt['LBL_13'] = $this->res['RESTG'].' '.$this->res['RESWK'];
                break;
            case '205':
                $this->rpt['LBLJUDUL']= 'KONFIRMASI SURAT TANDA TERIMA JAMINAN' ;
                $this->rpt['LBLNARASI']= 'Sehubungan dengan belum adanya surat tanda terima jaminan atas PIB Saudara Nomor Pengajuan seperti di atas, dengan ini Saudara diminta agar mengirimkan / menyampaikan dokumen STTJ kepada Pejabat Pengelola Jaminan.' ;
                $this->rpt['LBL_11']= 'Waktu Konfirmasi  : ' ;
                $this->rpt['LBL_13'] = $this->res['RESTG'].' '.$this->res['RESWK'];
                break;
            case '210':
                $this->rpt['LBLJUDUL']= 'KONFIRMASI PEMBAYARAN' ;
                $this->rpt['LBLNARASI']= 'Sehubungan dengan belum adanya Surat Tanda Pembayaran atas PIB Saudara Nomor Pengajuan seperti di atas, dengan ini Saudara diminta untuk :' ;
                $this->rpt['LBLDESKRIPSI']= $this->res['DESKRIPSI'] ;
                
                $this->rpt['LBL_11']= 'Waktu Konfirmasi  : ' ;
                $this->rpt['LBL_13'] = $this->res['RESTG'].' '.$this->res['RESWK'];
                break;
            case'023':case'230':case'600':
                switch ($this->res['RESKD']){
                    case'230':$this->rpt['LBLJUDUL']= 'NOTA PEMBERITAHUAN BARANG LARANGAN/PEMBATASAN (NPBL)' ;break;
                    case'023':$this->rpt['LBLJUDUL']= 'NOTA PEMBERITAHUAN BARANG LARANGAN/PEMBATASAN (NPBL) - NSW';break;
                    case'600':$this->rpt['LBLJUDUL']= 'NOTA PEMBERITAHUAN' ;break;    
                }
                $this->rpt['LBLNARASI'] = 'Dalam PIB yang Saudara sampaikan terdapat barang yang terkena ketentuan larangan / pembatasan. Untuk itu  diminta  menyerahkan  persetujuan dari  instansi terkait dalam waktu 3 (tiga) hari sejak tanggal Nota Pemberitahuan ini.' ;
                $this->rpt['LABEL2']    = 'Pejabat Peneliti Barang Larangan/Pembatasan';
                $this->rpt['LBL_11']    = 'Nomor Pendaftaran';
                $this->rpt['LABEL33']   = date('d-m-Y', strtotime($this->hdr['PIBTG'])) ;
                break;
            case '240':
                $this->rpt['LBLJUDUL']  = 'KONFIRMASI BC 1.1' ;
                $this->rpt['LBLNARASI'] = "Sehubungan dengan belum adanya dokumen BC 1.1 atas PIB Saudara Nomor Pengajuan seperti di atas, dengan ini Saudara diminta agar mengirimkan / menyampaikan dokumen BC 1.1 kepada Pejabat: \n"
                        . " NIP         : " . $this->res['NIP1']."\n"
                        . " Nama        : " . $this->res['PEJABAT1'];
                $this->rpt['LBLDESKRIPSI'] = "Catatan : \n" . $this->res['DESKRIPSI'];
                $this->rpt['LBL_11']= 'Waktu Konfirmasi  : ' ;
                $this->rpt['LBL_13'] = $this->res['RESTG'].' '.$this->res['RESWK'];
                break;
            case '250':
                $this->rpt['LBLJUDUL']  = 'PEMBERITAHUAN PENERIMAAN PIB PENYELESAIAN' ;
                $this->rpt['LBL_11']    = 'Nomor Pendaftaran : ' ;
                $this->rpt['LABEL33']   = date('d-m-Y', strtotime($this->hdr['PIBTG'])) ;
                break;
            case '900':
                $arrJDL = explode("\n", $this->res['DESKRIPSI']);
                $this->rpt['LBLJUDUL']  = $arrJDL[0] ." \n( RESPON UMUM )";
                
                break;
        }
        $this->rpt['LBLKOTATTD']  = $this->ktr['KOTA'].', '.$this->formatTglMysql($this->res['RESTG']);
        $this->rpt['LBLNMTTD']  = $this->res['PEJABAT1'];
        $this->rpt['LBLNIPTTD']  = $this->res['NIP1'];
        
    }
    
    function drawResponNPP(){
        $this->pdf->SetMargins(5, 5, 5, 5);
        $this->pdf->SetAutoPageBreak(true, 5);
        $this->pdf->setY(5);
        $this->pdf->SetFont('courier', '', '11');
        $this->pdf->multicell(0, 5, $this->rpt['LBLKOP'], 0, 'L');
        $this->pdf->Ln(10);
        $this->pdf->SetFont('courier', 'BU', '13');
        $this->pdf->multicell(0, 6,  str_replace('  ', '', $this->rpt['LBLJUDUL']), 0, 'C');
        $this->pdf->Ln(8);
        $this->pdf->SetFont('courier', '', '10');
        $this->pdf->cell(0, 4.5, $this->rpt['LBLNOTGL'], 0, 0, 'C', 0);
        $this->pdf->Ln(6);
        $this->pdf->cell(45, 4.5, 'Nomor Pengajuan  : ', 0, 0, 'L', 0);
        $this->pdf->cell(85, 4.5, $this->formatCar($this->rpt['LBLNODAFTAR']), 0, 0, 'L', 0);
        $this->pdf->cell(24, 4.5, 'Tanggal : ', 0, 0, 'L', 0);
        $this->pdf->cell(46, 4.5, $this->rpt['LBLTGLDAFTAR'], 0, 1, 'L', 0);
        $this->pdf->cell(45, 4.5, $this->rpt['LBL_11'], 0, 0, 'L', 0);
        $this->pdf->cell(85, 4.5, $this->rpt['LBL_13'], 0, 0, 'L', 0);
        if($this->rpt['LABEL33']!=''){
            $this->pdf->cell(24, 4.5, 'Tanggal : ', 0, 0, 'L', 0);
            $this->pdf->cell(46, 4.5, $this->rpt['LABEL33'], 0, 0, 'L', 0);
        }
        $this->pdf->Ln(10);
        $this->pdf->cell(0, 6,'Kepada       :', 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'IMPORTIR' , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NPWP    : ' . $this->rpt['IMPNPWP'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Nama    : ' . $this->rpt['IMPNAMA'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Alamat  : ' . $this->rpt['IMPALMT'], 0, 1, 'L', 0);
        $this->pdf->Ln(2);
        $this->pdf->cell(0, 4.5, 'PPJK' , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NPWP    : ' . $this->rpt['PPJKNPWP'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Nama    : ' . $this->rpt['PPJKNAMA'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Alamat  : ' . $this->rpt['PPJKALMT'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NP PPJK : ' .$this->rpt['PPJKNP'], 0, 1, 'L', 0);
        $this->pdf->multicell(0, 6,$this->rpt['LBLNARASI'], 0, 'J');
        $this->pdf->Ln(2);
        $this->pdf->multicell(0, 6, str_replace("\n\n", "\n", $this->rpt['LBLDESKRIPSI']), 0, 'J');
        
        #---------------------footer----------------------
        $this->pdf->Ln();
        $this->pdf->cell(70, 5, $this->rpt['LBLKOTATTD'], 0, 0, 'L', 0);
        //$this->pdf->cell(5,  5, ',', 0, 0, 'C', 0);
        //$this->pdf->cell(40, 5, $this->rpt['LBLTGLTTD'], 1, 0, 'C', 0);
        $this->pdf->Ln(15);
        $this->pdf->cell(70, 5, $this->rpt['LABEL2'], 0, 0, 'L', 0);
        $this->pdf->Ln(10);
        $this->pdf->cell(35, 5, 'Tanda tangan', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5,  ':', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(35, 5, 'Nama', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5,  ':', 0, 0, 'L', 0);
        $this->pdf->cell(0, 5, $this->rpt['LBLNMTTD'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(35, 5, 'Nip', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5,  ':', 0, 0, 'L', 0);
        $this->pdf->cell(0, 5,  $this->rpt['LBLNIPTTD'], 0, 0, 'L', 0);
        $this->footerNPP();
    }
        
    function footerNPP(){
        $this->pdf->SetY(-25);
        $this->pdf->SetFont('courier','',10);
        $this->pdf->cell(0, 5, $this->rpt['LABEL28'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 5, $this->rpt['LABEL29'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 5, $this->rpt['LABEL30'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 5, $this->rpt['LABEL31'], 0, 0, 'L', 0);
    }
    
    function footerSPPB(){
        $this->pdf->SetY(-30);
        $this->pdf->line(5, $this->pdf->getY(), 205, $this->pdf->getY());
        $this->pdf->SetFont('courier','',10);
        $this->pdf->cell(0, 5,'Peruntukan :', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 5,'1. Importir.', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 5,'2. Pejabat yang mengawasi pengeluaran barang.', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->multicell(0, 5, 'Formulir ini dicetak secara otomatis oleh sistem komputer dan tidak memerlukan nama, tanda tangan pejabat, dan cap dinas.', 0, 'J');
    }

    function drawResponSPPB() {
        $this->pdf->SetMargins(5, 5, 5, 5);
        $this->pdf->SetAutoPageBreak(0, 10);
        $this->pdf->setY(5);
        $this->pdf->SetFont('courier', '', '11');
        $this->pdf->multicell(0, 5, $this->ktr['KOP'], 0, 'L');
        $this->pdf->Ln(10);
        $this->pdf->SetFont('courier', 'BU', '13');
        $this->pdf->cell(0, 5, 'SURAT PERSETUJUAN PENGELUARAN BARANG (SPPB)', 0, 0, 'C', 0);
        
        $this->pdf->Ln();
        $this->pdf->SetFont('courier', '', '10');
        $this->pdf->multicell(0, 4.5, 'Nomor : ' . $this->res['DOKRESNO'] . '   Tanggal : '. date('d-m-Y',strtotime($this->res['DOKRESTG'])) , 0, 'C');
        $this->pdf->Ln(8);
        $this->pdf->cell(180, 4.5, 'Nomor Pengajuan           : ' . $this->formatCar($this->hdr['CAR']), 0, 1, 'L', 0);
        $this->pdf->cell(180, 4.5, 'Nomor Pendaftaran PIB     : ' . $this->hdr['PIBNO'] . '   Tanggal : ' . date('d-m-Y',strtotime($this->hdr['PIBTG'])) , 0, 0, 'L', 0);
        $this->pdf->Ln(7);
        $this->pdf->cell(0, 4.5,'Kepada       :', 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'IMPORTIR' , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NPWP    : ' . $this->hdr['IMPNPWP'] , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Nama    : ' . $this->hdr['IMPNAMA'] , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Alamat  : ' . $this->hdr['IMPALMT'] , 0, 1, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 4.5, 'PPJK' , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NPWP    : ' . $this->hdr['PPJKNPWP'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Nama    : ' . $this->hdr['PPJKNAMA'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Alamat  : ' . $this->hdr['PPJKALMT'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NP PPJK : ' . $this->hdr['PPJKNO'], 0, 1, 'L', 0);
        $this->pdf->Ln(2);
        $this->pdf->cell(0, 4.5, 'Lokasi Barang             : ' . $this->hdr['TMPTBN'] . ' - ' . $this->main->get_uraian("SELECT URAIAN FROM m_gudang WHERE KDKPBC = '".$this->hdr['KDKPBC']."' AND KDGDG = '".$this->hdr['TMPTBN']."'",'URAIAN') , 0, 1, 'L', 0);
    if($this->hdr['MODA']=='1'){
        $dok = explode('|', $this->main->get_uraian("SELECT CONCAT(DOKNO,'|',DOKTG) AS UR FROM t_bc20dok WHERE CAR = '".$this->hdr['CAR']."' and KODE_TRADER = '".$this->hdr['KODE_TRADER']."' and DOKKD in ('705','704')",'UR'));
        $this->pdf->cell(0, 4.5, 'No. dan Tanggal B/L       : ' . $this->fixLen($dok[0],30) .'  Tanggal : '.date('d-m-Y',strtotime($dok[1])) , 0, 1, 'L', 0);
    }
    elseif($this->hdr['MODA']=='4'){
        $dok = explode('|', $this->main->get_uraian("SELECT CONCAT(DOKNO,'|',DOKTG) AS UR FROM t_bc20dok WHERE CAR = '".$this->hdr['CAR']."' and KODE_TRADER = '".$this->hdr['KODE_TRADER']."' and DOKKD in ('741','740')",'UR'));
        $this->pdf->cell(0, 4.5, 'No. dan Tanggal AWB       : ' . $this->fixLen($dok[0],30) .'  Tanggal : '.date('d-m-Y',strtotime($dok[1])) , 0, 1, 'L', 0);
    }
        $this->pdf->cell(0, 4.5, 'Sarana Pengangkut         : ' . $this->hdr['ANGKUTNAMA'] . '    ' . $this->hdr['ANGKUTNO'] , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'No Voy./Flight            : ' . $this->hdr['ANGKUTNO'] , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'No/Tgl BC 1.1             : ' . $this->fixLen($this->hdr['DOKTUPNO'],6) . '    Tanggal : ' . $this->fixLen(date('d-m-Y', strtotime($this->hdr['doktuptg'])),10) . '  Pos     : ' . $this->fixLen($this->hdr['POSNO'],4) . ' ' . $this->fixLen($this->hdr['POSSUB'],4) . ' ' . $this->fixLen($this->hdr['POSSUBSUB'],4) . ' ' , 0, 1, 'L', 0);
        
        $kms = $this->main->get_uraian("SELECT group_concat(concat(a.JMKEMAS,' ',a.JNKEMAS) SEPARATOR ';') as UR FROM t_bc20kms a WHERE a.KODE_TRADER = '".$this->hdr['KODE_TRADER']."' AND a.CAR = '".$this->hdr['CAR']."'",'UR');
        $this->pdf->cell(0, 4.5, 'Jumlah / Jenis Kemasan    : ' . $this->fixLen($kms, 31) . ' Berat   : ' . number_format($this->hdr['BRUTO'], 2, '.', ',') . ' KGM' , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'Merk Kemasan              : ' , 0, 1, 'L', 0);
        $this->pdf->Ln();
        $jmlConr = count($this->conr);
        $this->pdf->cell(0, 4.5, 'Jumlah Peti Kemas         : ' . $jmlConr , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'Nomor Peti Kemas / Ukuran : ' , 0, 1, 'L', 0);
        $arr = array(1=>array('CAPTION'=>'No.','WEIGHT'=>'10','FIELD'=>''),
                     2=>array('CAPTION'=>'No.Peti Kemas','WEIGHT'=>'30','FIELD'=>'CONTNO'),
                     3=>array('CAPTION'=>'Ukuran','WEIGHT'=>'20','FIELD'=>'CONTUKUR'),
                     4=>array('CAPTION'=>'Penegahan','WEIGHT'=>'25','FIELD'=>'CONTTIPE'),
                     5=>array('CAPTION'=>'Ket','WEIGHT'=>'10','FIELD'=>''));
        
        foreach($arr as $a){
            $this->pdf->cell($a['WEIGHT'], 5, $a['CAPTION'] , 1, 0, 'L', 0);
        }
        if($jmlConr>1){
            $this->pdf->setX($this->pdf->getX()+5);
            foreach($arr as $a){
                $this->pdf->cell($a['WEIGHT'], 5, $a['CAPTION'] , 1, 0, 'L', 0);
            }
        }
        $this->pdf->Ln();
        $akr = ($jmlConr > 14)?14:$jmlConr;
        for($i=1;$i<$akr;$i++){
            foreach($arr as $a){
                if($a['CAPTION']=='No.'){
                    $this->pdf->cell($a['WEIGHT'], 5, $i , 1, 0, 'L', 0);
                }else{
                    $this->pdf->cell($a['WEIGHT'], 5, $this->conr[$i-1][$a['FIELD']] , 1, 0, 'L', 0);
                }
            }
            if($i % 2 == 1){
                $this->pdf->setX($this->pdf->getX()+5);
            }else{
                $this->pdf->Ln();
            }
        }
        $this->pdf->Ln(2);
        $this->pdf->cell(30, 4.5, 'Catatan     : ' , 0, 0, 'L', 0);
        $this->pdf->multicell(0, 4.5, $this->res['DESKRIPSI'] , 0, 'L');
        $this->pdf->Ln(2);
        $this->pdf->cell(100, 4.5, $this->ktr['KOTA'].' , ' . date('d-m-Y',strtotime($this->res['RESTG'])) , 0, 0, 'L', 0);
        $this->pdf->cell(100, 4.5, $this->ktr['KOTA'].' , ' . date('d-m-Y',strtotime($this->res['RESTG'])), 0, 1, 'L', 0);
        $this->pdf->cell(100, 4.5, 'Pejabat Pemeriksa Dokumen', 0, 0, 'L', 0);
        $this->pdf->cell(100, 4.5, 'Pejabat yang mengawasi pengeluaran barang', 0, 0, 'L', 0);
        $this->pdf->Ln(15);
        $this->pdf->cell(100, 4.5, 'Tanda Tangan  :', 0, 0, 'L', 0);
        $this->pdf->cell(100, 4.5, 'Tanda Tangan  :', 0, 1, 'L', 0);
        $this->pdf->cell(100, 4.5, 'Nama          : ' . $this->res['PEJABAT1'], 0, 0, 'L', 0);
        $this->pdf->cell(100, 4.5, 'Nama          : ' . $this->res['NIP1'], 0, 1, 'L', 0);
        $this->pdf->cell(100, 4.5, 'NIP           : ' . $this->res['PEJABAT2'], 0, 0, 'L', 0);
        $this->pdf->cell(100, 4.5, 'NIP           : ' . $this->res['NIP2'], 0, 0, 'L', 0);
        
        $this->pdf->SetY(-30);
        $this->pdf->line(5, $this->pdf->getY(), 205, $this->pdf->getY());
        $this->pdf->SetFont('courier','',10);
        $this->pdf->cell(0, 4.5,'Peruntukan :', 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5,'1. Importir.', 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5,'2. Pejabat yang mengawasi pengeluaran barang.', 0, 1, 'L', 0);
        $this->pdf->multicell(0, 4.5, 'Formulir ini dicetak secara otomatis oleh sistem komputer dan tidak memerlukan nama, tanda tangan pejabat, dan cap dinas.', 0, 'J');
        if($jmlConr>14){
            while($i<=$jmlConr){
                $this->pdf->addPage();
                $this->pdf->SetFont('courier', '', '11');
                $this->pdf->multicell(0, 5, $this->ktr['KOP'], 0, 'L');
                $this->pdf->Ln(10);
                $this->pdf->SetFont('courier', 'BU', '13');
                $this->pdf->cell(0, 5, 'SURAT PERSETUJUAN PENGELUARAN BARANG (SPPB)', 0, 0, 'C', 0);
                $this->pdf->Ln();
                $this->pdf->SetFont('courier', '', '10');
                $this->pdf->multicell(0, 4.5, 'Nomor : ' . $this->res['DOKRESNO'] . '   Tanggal : '. date('d-m-Y',strtotime($this->res['DOKRESTG'])) , 0, 'C');
                $this->pdf->Ln(8);
                $this->pdf->cell(180, 4.5, 'Nomor Pengajuan           : ' . $this->formatCar($this->hdr['CAR']), 0, 1, 'L', 0);
                $this->pdf->cell(180, 4.5, 'Nomor Pendaftaran PIB     : ' . $this->hdr['PIBNO'] . '   Tanggal : ' . date('d-m-Y',strtotime($this->hdr['PIBTG'])) , 0, 1, 'L', 0);
                
                $awl = $i;
                $akr = ($jmlConr > $awl+79)?$awl+79: $jmlConr;
                foreach($arr as $a){
                    $this->pdf->cell($a['WEIGHT'], 5, $a['CAPTION'] , 1, 0, 'L', 0);
                }
                if(($akr - $awl)>0){
                    $this->pdf->setX($this->pdf->getX()+5);
                    foreach($arr as $a){
                        $this->pdf->cell($a['WEIGHT'], 5, $a['CAPTION'] , 1, 0, 'L', 0);
                    }
                }
                $this->pdf->Ln();
                for ($i = $awl; $i <= $akr; $i++) {
                    foreach ($arr as $a) {
                        if ($a['CAPTION'] == 'No.') {
                            $this->pdf->cell($a['WEIGHT'], 5, $i, 1, 0, 'L', 0);
                        } else {
                            $this->pdf->cell($a['WEIGHT'], 5, $this->conr[$i - 1][$a['FIELD']], 1, 0, 'L', 0);
                        }
                    }
                    if ($i % 2 == 1) {
                        $this->pdf->setX($this->pdf->getX() + 5);
                    } else {
                        $this->pdf->Ln();
                    }
                }
                
                $this->pdf->SetY(-30);
                $this->pdf->line(5, $this->pdf->getY(), 205, $this->pdf->getY());
                $this->pdf->SetFont('courier','',10);
                $this->pdf->cell(0, 4.5,'Peruntukan :', 0, 1, 'L', 0);
                $this->pdf->cell(0, 4.5,'1. Importir.', 0, 1, 'L', 0);
                $this->pdf->cell(0, 4.5,'2. Pejabat yang mengawasi pengeluaran barang.', 0, 1, 'L', 0);
                $this->pdf->multicell(0, 4.5, 'Formulir ini dicetak secara otomatis oleh sistem komputer dan tidak memerlukan nama, tanda tangan pejabat, dan cap dinas.', 0, 'J');
            }
        }
        
        
        
        
        
        //$this->footerSPPB();
    }
    
    function drawResponINP(){
        $arr = explode("\n", $this->res['DESKRIPSI']);
        $this->pdf->SetMargins(7, 5, 5);
        $this->pdf->SetAutoPageBreak(2, 10);
        $this->pdf->setY(5);
        $this->pdf->SetFont('courier', '', '11');
        $this->pdf->multicell(0, 5, $this->ktr['KOP'], 0, 'L');
        $this->pdf->Ln(4);
        $this->pdf->SetFont('courier', 'BU', '13');
        $this->pdf->cell(0, 5, 'INFORMASI NILAI PABEAN', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('courier', '', '10');
        $this->pdf->multicell(0, 5, ($this->res['DOKRESNO']=='')?'':'Nomor : '.$this->res['DOKRESNO'] , 0, 'C');
        $this->pdf->Ln(3);
        $this->pdf->cell(0, 6,'Kepada Yth.', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 6, 'PEMBELI' , 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(33, 4.5, '     NPWP    : ' , 0, 0, 'L', 0);$this->pdf->cell(0, 5, $this->hdr['IMPNPWP'] , 0, 1, 'L', 0);
        $this->pdf->cell(33, 4.5, '     Nama    : ' , 0, 0, 'L', 0);$this->pdf->cell(0, 5, $this->hdr['IMPNAMA'] , 0, 1, 'L', 0);
        $this->pdf->cell(33, 4.5, '     Alamat  : ' , 0, 0, 'L', 0);$this->pdf->multicell(0, 5, $this->hdr['IMPALMT'], 0, 'L');
        $this->pdf->cell(0, 4.5, 'PEMBERITAHU' , 0, 1, 'L', 0);
        $this->pdf->cell(33, 4.5, '     NPWP    : ' , 0, 0, 'L', 0);$this->pdf->cell(0, 5, $this->hdr['PPJKNPWP'] , 0, 1, 'L', 0);
        $this->pdf->cell(33, 4.5, '     Nama    : ' , 0, 0, 'L', 0);$this->pdf->cell(0, 5, $this->hdr['PPJKNAMA'] , 0, 1, 'L', 0);
        $this->pdf->cell(33, 4.5, '     Alamat  : ' , 0, 0, 'L', 0);$this->pdf->cell(0, 5, $this->hdr['PPJKALMT'] , 0, 1, 'L', 0);
        $this->pdf->cell(33, 4.5, '     NP PPJK : ' , 0, 0, 'L', 0);$this->pdf->cell(0, 5, $this->hdr['PPJKNO'] , 0, 1, 'L', 0);
        $this->pdf->Ln(2);
        $this->pdf->cell(0, 4.5, 'PIB ' , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '    Nomor Pengajuan    : ' . $this->formatCar($this->hdr['CAR']) , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '    Nomor Pendaftaran  : ' . $this->hdr['PIBNO'] , 0, 1, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->multicell(0, 4.5, "   Berhubung nilai pabean yang Saudara beritahukan di dalam pemberitahuan pabean impor diragukan kebenarannya, yaitu untuk jenis barang nomor urut ".$arr[0]." pada pemberitahuan pabean impor No: ".$this->res['PIBNO'] .' Tanggal : ' . date('d-m-Y', strtotime($this->res['PIBTG'])) . ", maka dengan ini diminta Saudara untuk menyerahkan Deklarasi Nilai Pabean (DNP) yang merupakan deklarasi atas fakta transaksi jual-beli/importasi yang berkaitan dengan barang yang Saudara impor disertai dengan dokumen-dokumen pendukung yang menguatkan deklarasi yang diajukan, misalnya:

a. Kontrak Penjualan (Sales Contract);
b. Kontrak (Agreement) lainnya, misalnya Royalty Agreement, Kontrak Penunjukan
   Keagenan untuk Pembelian Barang, Kontrak Penunjukan sebagai Agen/Distributor;
c. Purchase Order;
d. Letter of Credit (L/C);
e. Bukti pelunasan atas pembelian barang impor (bukti transfer uang);
f. Rekening Koran yang berkaitan dengan transaksi tersebut;
g. Bukti lain yang menyatakan kewajiban importir kepada penjual yang belum
   dipenuhi terkait dengan transaksi impor;
h. Bukti pembayaran atas barang yang sama pada penjual yang sama untuk transaksi
   sebelumnya;
i. Dokumen/bukti negosiasi terbentuknya harga;
j. Dokumen lainnya yang terkait dengan transaksi tersebut.

    DNP dan dokumen-dokumen pendukung tersebut harus diserahkan paling lambat dalam waktu 3 (tiga) hari kerja setelah tanggal pengiriman Informasi Nilai Pabean (INP) kepada Pejabat Bea dan Cukai:" , 0, 'J');
        $this->pdf->cell(0, 4.5, '            Nama                  : ' . $this->res['PEJABAT1'] , 0, 2, 'L', 0);
        $this->pdf->cell(0, 4.5, '            Nip                   : ' . $this->res['NIP1'] , 0, 2, 'L', 0);
        $this->pdf->cell(0, 4.5, '            Jabatan               : ' . $this->res['JABATAN1'] , 0, 2, 'L', 0);
        $this->pdf->cell(0, 4.5, '            Telepon dan Faxsimile : ' . $arr[1] . ' / ' . $arr[2] , 0, 2, 'L', 0);
        
        $this->pdf->multicell(0, 4.5, "   Apabila DNP dan dokumen pendukungnya tidak diserahkan dalam jangka waktu tersebut di atas, nilai pabean ditentukan berdasarkan nilai transaksi barang identik sampai dengan metode pengulangan sesuai hierarki penggunaannya." , 0, 'J');
        $this->pdf->setX($this->pdf->getX()+100);
        $this->pdf->cell(0, 4.5, $this->ktr['KOTA'] . ' , ' . $this->formatTglMysql($this->res['RESTG']) , 0, 2, 'C', 0);
        $this->pdf->cell(0, 4.5, 'Pejabat yang memeriksa dokumen' , 0, 2, 'C', 0);
        $this->pdf->ln(12);
        $this->pdf->setX($this->pdf->getX()+100);
        $this->pdf->cell(0, 4.5, $this->res['PEJABAT1'] , 0, 2, 'C', 0);
        $this->pdf->cell(0, 4.5, $this->res['NIP1'] , 0, 0, 'C', 0);
    }
    
    function drawResponSPTNP() {
        $this->pdf->SetMargins(5, 5, 5);
        $this->pdf->SetAutoPageBreak(0, 0);
        $this->pdf->setY(5);
        
        $this->pdf->SetFont('courier', '', '10');
        $this->pdf->cell(0, 5, 'KEMENTERIAN KEUANGAN REPUBLIK INDONESIA', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 5, 'DIREKTORAT JENDERAL BEA DAN CUKAI', 0, 0, 'L', 0);
        $this->pdf->Ln(15);
        $this->pdf->SetFont('courier', 'UB', '13');
        $this->pdf->cell(0, 5, 'SURAT PENETAPAN  TARIF DAN/ ATAU NILAI PABEAN (SPTNP)', 0, 0, 'C', 0);
        $this->pdf->Ln(5);
        $this->pdf->SetFont('courier', '', '11');
        $this->pdf->setX(48);
        $this->pdf->cell(0, 5, 'Nomor      :'.$this->res['DOKRESNO'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->setX(48);
        $this->pdf->cell(0, 5, 'Tanggal    :'.$this->res['DOKRESTG'], 0, 0, 'L', 0);
        
        $this->pdf->Ln(8);
        $this->pdf->SetFont('courier', '', '10');
        $this->pdf->cell(40, 5, 'Kepada Yth.', 0, 0, 'L', 0);
        $this->pdf->Ln(5);
        $this->pdf->cell(0, 5, 'Nama               : '.$this->hdr['IMPNAMA'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 5, 'Alamat             : '.$this->hdr['IMPALMT'], 0, 0, 'L', 0);
        $this->pdf->Ln(6);
        $this->pdf->cell(0, 5, 'Dengan ini diberitahukan atas Pemberitahuan Pabean Impor (PIB)   :', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(120, 5, 'Nomor Pendaftaran  : '.$this->hdr['PIBNO'], 0, 0, 'L', 0);
        $this->pdf->cell(0, 5, 'Tanggal  : '.$this->res['PIBTG'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(120, 5, 'Importir           : '.$this->hdr['IMPNAMA'], 0, 0, 'L', 0);
        $this->pdf->cell(0, 5, 'NPWP     : '.$this->formatNPWP($this->hdr['IMPNPWP']), 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(120, 5, 'PPJK               : '.$this->hdr['PPJKNAMA'], 0, 0, 'L', 0);
        $this->pdf->cell(0, 5, 'NPWP     : '.$this->formatNPWP($this->hdr['PPJKNPWP']), 0, 0, 'L', 0);
        $this->pdf->Ln(6);
        $this->pdf->multicell(200, 4, 'ditetapkan tarif dan / atau nilai pabean sehingga mengaibatkan kekurangan / kelebihan pembayaran bea masuk, pajak dalam rangka impor, dan/ atau sanksi administrasi berupa denda dengan rincian sebagai berikut :', 0, 'J');
        
        $this->pdf->Ln();
        $this->pdf->cell(40, 5, 'URAIAN', 1, 0, 'C', 0);
        $this->pdf->cell(40, 5, 'DIBERITAHUKAN', 1, 0, 'C', 0);
        $this->pdf->cell(40, 5, 'DITETAPKAN', 1, 0, 'C', 0);
        $this->pdf->cell(40, 5, 'KEKURANGAN', 1, 0, 'C', 0);
        $this->pdf->cell(40, 5, 'KELEBIHAN', 1, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40, 5, '1. Bea Cukai', 'LR', 0, 'L', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['BM_ASAL'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['BMBYR'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['BM_KURANG'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['BM_LEBIH'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40, 5, '2. Cukai', 'LR', 0, 'L', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['CUK_ASAL'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['CUKBYR'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['CUK_KURANG'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['CUK_LEBIH'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40, 5, '3. PPN', 'LR', 0, 'L', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPN_ASAL'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPNBYR'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPN_KURANG'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPN_LEBIH'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40, 5, '4. PPnBM', 'LR', 0, 'L', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPNBM_ASAL'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPNBMBYR'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPNBM_KURANG'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPNBM_LEBIH'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40, 5, '5. PPh Pasal 22', 'LR', 0, 'L', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPH_ASAL'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPHBYR'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPH_KURANG'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPH_LEBIH'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40, 5, '6. Denda', 'LR', 0, 'L', 0);
        $this->pdf->cell(40, 5, '0', 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, '0', 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['DENDA'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, '0', 'LR', 0, 'R', 0);
        $this->pdf->Ln();
        $total = $this->ntp['BM_KURANG'] + $this->ntp['CUK_KURANG'] + $this->ntp['PPN_KURANG'] + $this->ntp['PPNBM_KURANG'] + $this->ntp['PPH_KURANG'] + $this->ntp['DENDA'];
        $totalLbh = $this->ntp['BM_LEBIH'] + $this->ntp['CUK_LEBIH'] + $this->ntp['PPN_LEBIH'] + $this->ntp['PPNBM_LEBIH'] + $this->ntp['PPH_LEBIH'];
        $this->pdf->cell(120, 5, 'JUMLAH KEKURANGAN / KELEBIHAN PEMBAYARAN', 1, 0, 'C', 0);
        $this->pdf->cell(40, 5, number_format($total, 0, '.', ','), 1, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($totalLbh, 0, '.', ','), 1, 0, 'R', 0);
        
        $this->pdf->Ln(6);
        $this->pdf->cell(0, 4, 'Dengan rincian kesalahan sebagai berikut : ', 0, 0, 'L', 0);
        $this->pdf->Ln(6);
        
        $this->pdf->cell(60, 4, 'JENIS KESALAHAN', 1, 0, 'C', 0);
        $this->pdf->cell(0, 4, 'NOMOR URUT BARANG', 1, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(60, 4, '1. Jenis Barang', 'LTR', 0, 'L', 0);$this->pdf->cell(0, 4, $this->ntp['S_JNSBRG'], 'TR', 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(60, 4, '2. Jumlah Barang', 'LR', 0, 'L', 0);$this->pdf->cell(0, 4, $this->ntp['S_JMLBRG'], 'R', 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(60, 4, '3. Tarif', 'LR', 0, 'L', 0);$this->pdf->cell(0, 4, $this->ntp['S_TARIF'], 'R', 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(60, 4, '4. Nilai Pabean', 'LBR', 0, 'L', 0);$this->pdf->cell(0, 4, $this->ntp['S_NILPAB'], 'BR', 0, 'L', 0);
        $this->pdf->Ln(8);
        
        if($total > 0){
            $jthTemp = $this->formatTglMysql($this->res['JATUHTEMPO']);
        }else{
            $jthTemp = ' - ';
        } 
        
        $this->pdf->multicell(200, 4,'     Dalam hal terdapat kekurangan pembayaran, Saudara  wajib melunasi kekurangan pembayaran tersebut paling lambat pada tanggal ' . $jthTemp . ', dan bukti  pelunasan  agar disampaikan kepada Kepala Kantor '. $this->hdr['URKDKPBC'].'.', 0, 'J');
        $this->pdf->Ln();
        $this->pdf->multicell(200, 4, '	    Apabila tagihan tidak dilunasi atau tidak diajukan keberatan smapai dengan tanggal ' . $jthTemp . ', dikenakan bunga sebesar 2% (dua persen) setiap bulan untuk paling lama 24 (dua puluh empat) bulan dari jumlah kekurangan pembayaran, bagian bulan dihitung satu bulan penuh.', 0, 'J');
        $this->pdf->Ln();
        $this->pdf->multicell(200, 4, '     Dalam hal terdapat kelebihan pembayaran, Saudara dapat mengajukan permohonan pengembalian sesuai ketentuan peraturan perundang-undangan.', 0, 'J');
        $this->pdf->Ln();
        $this->pdf->multicell(200, 4, '     Keberatan atas penetapan ini hanya dapat diajukan secara tertulis kepada Direktur Jenderal Bea dan Cukai melalui ' . $this->hdr['URKDKPBC'] . ' sesuai dengan ketentuan tentang keberatan, paling lambat pada tanggal  ' . $jthTemp . ".", 0, 'J');
        $this->pdf->SetFont('courier', '', '11');
        $this->pdf->setXY(100,$this->pdf->getY()+10);
        $this->pdf->cell(100, 5, $this->res['JABATAN1'], 0, 0, 'C', 0);
        $this->pdf->setXY(100,$this->pdf->getY()+20);
        $this->pdf->cell(100, 5, $this->res['PEJABAT1'], 0, 0, 'C', 0);
        $this->pdf->setXY(100,$this->pdf->getY()+5);
        $this->pdf->cell(100, 5, $this->res['NIP1'], 0, 0, 'C', 0);
        $this->pdf->Ln(6);
        $this->pdf->cell(0, 5, 'STNP ini dibuat 3 (tiga) :', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 5, '- Rangkap ke-1 untuk Importir;', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 5, '- Rangkap ke-2 untuk Kepala Kantor;', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 5, '- Rangkap ke-3 untuk arsip Pejabar Bea dan Cukai.', 0, 0, 'L', 0);
    }
    
    function drawResponSPKNP(){
        $arr = explode("\n", $this->res['DESKRIPSI']);
        $this->pdf->SetMargins(7, 5, 5);
        $this->pdf->SetAutoPageBreak(2, 10);
        $this->pdf->setY(5);
        $this->pdf->SetFont('courier', '', '11');
        $this->pdf->multicell(0, 5, $this->ktr['KOP'], 0, 'L');
        $this->pdf->Ln(4);
        $this->pdf->SetFont('courier', 'BU', '13');
        $this->pdf->cell(0, 5, 'SURAT PEMBERITAHUAN KONSULTASI NILAI PABEAN', 0, 1, 'C', 0);
        $this->pdf->SetFont('courier', '', '10');
        $this->pdf->multicell(0, 5, ($this->res['DOKRESNO']=='')?'':'Nomor : '.$this->res['DOKRESNO'] , 0, 'C');
        $this->pdf->Ln(3);
        $this->pdf->cell(0, 6,'Kepada Yth.', 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'Direktur PT  : ' . $this->hdr['IMPNAMA'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'Alamat       : ' . $this->hdr['IMPALMT'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'NPWP         : ' . $this->hdr['IMPNPWP'], 0, 1, 'L', 0);
        $this->pdf->Ln(2);
        $this->pdf->multicell(0, 4.5, '    Sehubungan dengan Deklarasi Nilai Pabean (DNP) atas nilai transaksi yang diberitahukan dalam PIB:', 0, 'J');
        $this->pdf->Ln(4);
        $this->pdf->cell(0, 4.5, 'No. Aju               : ' . $this->hdr['CAR'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'No / Tgl Pendaftaran  : ' . $this->hdr['PIBNO'] . '  Tangga : ' . date('d-m-Y',strtotime($this->hdr['PIBTG'])), 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'Nomor Urut            : ' . $arr[0], 0, 1, 'L', 0);
        $this->pdf->Ln(2);
        $this->pdf->multicell(0, 4.5, '    Saudara diharapkan hadir di '.$this->ktr['URAIAN_KPBC'].' dalam waktu 2 (dua) hari kerja sejak tanggal Surat Pemberitahuan Konsultasi Nilai Pabean untuk melakukan konsultasi tentang nilai transaksi yang Saudara beritahukan dalam pemberitahuan pabean impor di atas dengan membawa data dan/atau informasi tambahan berupa :', 0, 'J');
        $this->pdf->Ln(2);
        $this->pdf->multicell(0, 4.5, str_replace($arr[0] . "\n", '', $this->res['DESKRIPSI']) , 0, 'J');
        $this->pdf->Ln(2);
        $this->pdf->multicell(0, 4.5, '    Apabila Saudara tidak hadir dalam jangka waktu tersebut, nilai pabean ditentukan berdasarkan nilai transaksi barang identik sampai dengan metode pengulangan sesuai hierarki penggunaannya.', 0, 'J');
        
        
        $this->pdf->setXY($this->pdf->getX()+100,-70);
        $this->pdf->cell(0, 4.5, $this->ktr['KOTA'] . ' , ' . $this->formatTglMysql($this->res['RESTG']) , 0, 2, 'C', 0);
        $this->pdf->cell(0, 4.5, 'Pejabat Bea dan Cukai' , 0, 2, 'C', 0);
        $this->pdf->ln(25);
        $this->pdf->setX($this->pdf->getX()+100);
        $this->pdf->cell(0, 4.5, $this->res['PEJABAT1'] , 0, 2, 'C', 0);
        $this->pdf->cell(0, 4.5, $this->res['NIP1'] , 0, 0, 'C', 0);
    }
       
    function drawResponSPJM() {
        $arr = explode("\n", $this->res['DESKRIPSI']."\n");
        $this->pdf->SetMargins(7, 5, 5);
        $this->pdf->SetAutoPageBreak(2, 10);
        $this->pdf->setY(5);
        $this->pdf->SetFont('courier', '', '11');
        $this->pdf->multicell(0, 5, $this->ktr['KOP'], 0, 'L');
        $this->pdf->Ln(4);
        $this->pdf->SetFont('courier', 'BU', '13');
        $this->pdf->cell(0, 5, ($this->res['RESKD']=='400')?'SURAT PEMBERITAHUAN JALUR MERAH (SPJM)':'SURAT PEMBERITAHUAN JALUR KUNING (SPJK)', 0, 1, 'C', 0);
        $this->pdf->SetFont('courier', '', '10');
        $this->pdf->multicell(0, 5, ($this->res['DOKRESNO']=='')?'':'Nomor : ' . $this->res['DOKRESNO'] . '   Tanggal : ' . date('d-m-Y',strtotime($this->res['DOKRESTG'])), 0, 'C');
        $this->pdf->Ln(3);
        $this->pdf->cell(0, 4.5, 'Nomor Pengajuan    : ' . $this->formatCar($this->hdr['CAR']) , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'Nomor Pendaftaran  : ' . $this->hdr['PIBNO'] , 0, 1, 'L', 0);
        $this->pdf->Ln();
        
        $this->pdf->cell(0, 4.5, 'Kepada       :'  , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'IMPORTIR'       , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NPWP    : ' . $this->hdr['IMPNPWP'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Nama    : ' . $this->hdr['IMPNAMA'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Alamat  : ' . $this->hdr['IMPALMT'], 0, 1, 'L', 0);
        $this->pdf->Ln(4);
        $this->pdf->cell(0, 4.5, 'PPJK' , 0, 0, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NPWP    : ' . $this->rpt['PPJKNPWP'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Nama    : ' . $this->rpt['PPJKNAMA'] , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Alamat  : ' . $this->rpt['PPJKALMT'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NP PPJK : ' . $this->rpt['PPJKNP'], 0, 1, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(32, 4.5, 'Lokasi Barang ', 0, 0, 'L', 0);
        $this->pdf->multicell(0, 4.5, $arr[0] , 0, 'L');
        $this->pdf->Ln();
        $this->pdf->multicell(0, 4.5, str_replace($arr[0]."\n", '', $this->res['DESKRIPSI']."\n") , 0, 'L');
        $this->pdf->Ln();
        $this->pdf->multicell(0, 4.5, ($this->res['RESKD']=='400')?"Berdasarkan  hasil penelitian  dokumen, PIB Saudara ditetapkan JALUR MERAH.  Agar Saudara menyerahkan hasil cetak PIB dan dokumen pelengkap pabean serta menyiapkan barang untuk dilakukan pemeriksaan fisik dalam jangka waktu 3 ( tiga ) hari kerja setelah tanggal SPJM ini.":"Berdasarkan  hasil penelitian  dokumen, PIB Saudara ditetapkan JALUR KUNING.  Agar Saudara menyerahkan hasil cetak PIB dan dokumen pelengkap pabean dalam jangka waktu 3 ( tiga ) hari kerja setelah tanggal SPJK ini." , 0, 'L');
        
        $this->pdf->setY(-75);
        $this->pdf->cell(0, 4.5, 'Pejabat yang menangani pelayanan pabean / ' , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'Pejabat Pemeriksa Dokumen' , 0, 1, 'L', 0);
        $this->pdf->ln(15);
        $this->pdf->cell(0, 4.5, 'Tanda Tangan     :' , 0, 1, 'L', 0);
        $this->pdf->ln();
        $this->pdf->cell(0, 4.5, 'Nama             : ' . $this->res['PEJABAT1'] , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'Nip              : ' . $this->res['NIP1'] , 0, 1, 'L', 0);
        $this->pdf->multicell(0, 4.5, "Peruntukan :
1. Importir;
2. Pejabat pemeriksa barang
Formulir ini dicetak secara otomatis oleh sistem komputer dan tidak memerlukan nama, tanda tangan pejabat, dan cap dinas." , 0, 'L');
    }
    
    function drawRespon450(){
        $this->pdf->SetMargins(5, 5, 5, 5);
        $this->pdf->SetAutoPageBreak(0, 10);
        $this->pdf->setY(5);
        $this->pdf->SetFont('courier', '', '11');
        $this->pdf->multicell(0, 5, $this->ktr['KOP'], 0, 'L');
        $this->pdf->Ln(10);
        $this->pdf->SetFont('courier', 'BU', '13');
        $this->pdf->cell(0, 5, 'SURAT PEMBERITAHUAN PEMERIKSAAN FISIK (SPPF)', 0, 0, 'C', 0);
        
        $this->pdf->Ln();
        $this->pdf->SetFont('courier', '', '10');
        $this->pdf->multicell(0, 4.5, 'Nomor : ' . $this->res['DOKRESNO'] . '   Tanggal : '. date('d-m-Y',strtotime($this->res['DOKRESTG'])) , 0, 'C');
        $this->pdf->Ln(8);
        $this->pdf->cell(180, 4.5, 'Nomor Pengajuan           : ' . $this->formatCar($this->hdr['CAR']), 0, 1, 'L', 0);
        $this->pdf->cell(180, 4.5, 'Nomor Pendaftaran PIB     : ' . $this->hdr['PIBNO'] . '   Tanggal : ' . date('d-m-Y',strtotime($this->hdr['PIBTG'])) , 0, 0, 'L', 0);
        $this->pdf->Ln(7);
        $this->pdf->cell(0, 4.5,'Kepada       :', 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'IMPORTIR' , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NPWP    : ' . $this->hdr['IMPNPWP'] , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Nama    : ' . $this->hdr['IMPNAMA'] , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Alamat  : ' . $this->hdr['IMPALMT'] , 0, 1, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 4.5, 'PPJK' , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NPWP    : ' . $this->hdr['PPJKNPWP'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Nama    : ' . $this->hdr['PPJKNAMA'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Alamat  : ' . $this->hdr['PPJKALMT'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NP PPJK : ' . $this->hdr['PPJKNO'], 0, 1, 'L', 0);
        $this->pdf->Ln(2);
        $this->pdf->cell(0, 4.5, 'Lokasi Barang             : ' . $this->hdr['TMPTBN'] . ' - ' . $this->main->get_uraian("SELECT URAIAN FROM m_gudang WHERE KDKPBC = '".$this->hdr['KDKPBC']."' AND KDGDG = '".$this->hdr['TMPTBN']."'",'URAIAN') , 0, 1, 'L', 0);
    if($this->hdr['MODA']=='1'){
        $dok = explode('|', $this->main->get_uraian("SELECT CONCAT(DOKNO,'|',DOKTG) AS UR FROM t_bc20dok WHERE CAR = '".$this->hdr['CAR']."' and KODE_TRADER = '".$this->hdr['KODE_TRADER']."' and DOKKD in ('705','704')",'UR'));
        $this->pdf->cell(0, 4.5, 'No. dan Tanggal B/L       : ' . $this->fixLen($dok[0],30) .'  Tanggal : '.date('d-m-Y',strtotime($dok[1])) , 0, 1, 'L', 0);
    }
    elseif($this->hdr['MODA']=='4'){
        $dok = explode('|', $this->main->get_uraian("SELECT CONCAT(DOKNO,'|',DOKTG) AS UR FROM t_bc20dok WHERE CAR = '".$this->hdr['CAR']."' and KODE_TRADER = '".$this->hdr['KODE_TRADER']."' and DOKKD in ('741','740')",'UR'));
        $this->pdf->cell(0, 4.5, 'No. dan Tanggal AWB       : ' . $this->fixLen($dok[0],30) .'  Tanggal : '.date('d-m-Y',strtotime($dok[1])) , 0, 1, 'L', 0);
    }
        $this->pdf->cell(0, 4.5, 'Sarana Pengangkut         : ' . $this->hdr['ANGKUTNAMA'] . '    ' . $this->hdr['ANGKUTNO'] , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'No Voy./Flight            : ' . $this->hdr['ANGKUTNO'] , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'No/Tgl BC 1.1             : ' . $this->fixLen($this->hdr['DOKTUPNO'],6) . '    Tanggal : ' . $this->fixLen(date('d-m-Y', strtotime($this->hdr['doktuptg'])),10) . '  Pos     : ' . $this->fixLen($this->hdr['POSNO'],4) . ' ' . $this->fixLen($this->hdr['POSSUB'],4) . ' ' . $this->fixLen($this->hdr['POSSUBSUB'],4) . ' ' , 0, 1, 'L', 0);
        
        $kms = $this->main->get_uraian("SELECT group_concat(concat(a.JMKEMAS,' ',a.JNKEMAS) SEPARATOR ';') as UR FROM t_bc20kms a WHERE a.KODE_TRADER = '".$this->hdr['KODE_TRADER']."' AND a.CAR = '".$this->hdr['CAR']."'",'UR');
        $this->pdf->cell(0, 4.5, 'Jumlah / Jenis Kemasan    : ' . $this->fixLen($kms, 31) . ' Berat   : ' . number_format($this->hdr['BRUTO'], 2, '.', ',') . ' KGM' , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'Merk Kemasan              : ' , 0, 1, 'L', 0);
        $this->pdf->Ln();
        $jmlConr = count($this->conr);
        $this->pdf->cell(0, 4.5, 'Jumlah Peti Kemas         : ' . $jmlConr , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'Nomor Peti Kemas / Ukuran : ' , 0, 1, 'L', 0);
        $arr = array(1=>array('CAPTION'=>'No.','WEIGHT'=>'10','FIELD'=>''),
                     2=>array('CAPTION'=>'No.Peti Kemas','WEIGHT'=>'30','FIELD'=>'CONTNO'),
                     3=>array('CAPTION'=>'Ukuran','WEIGHT'=>'20','FIELD'=>'CONTUKUR'),
                     4=>array('CAPTION'=>'Penegahan','WEIGHT'=>'25','FIELD'=>'CONTTIPE'),
                     5=>array('CAPTION'=>'Ket','WEIGHT'=>'10','FIELD'=>''));
        
        foreach($arr as $a){
            $this->pdf->cell($a['WEIGHT'], 5, $a['CAPTION'] , 1, 0, 'L', 0);
        }
        if($jmlConr>1){
            $this->pdf->setX($this->pdf->getX()+5);
            foreach($arr as $a){
                $this->pdf->cell($a['WEIGHT'], 5, $a['CAPTION'] , 1, 0, 'L', 0);
            }
        }
        $this->pdf->Ln();
        $akr = ($jmlConr > 14)?14:$jmlConr;
        for($i=1;$i<$akr;$i++){
            foreach($arr as $a){
                if($a['CAPTION']=='No.'){
                    $this->pdf->cell($a['WEIGHT'], 5, $i , 1, 0, 'L', 0);
                }else{
                    $this->pdf->cell($a['WEIGHT'], 5, $this->conr[$i-1][$a['FIELD']] , 1, 0, 'L', 0);
                }
            }
            if($i % 2 == 1){
                $this->pdf->setX($this->pdf->getX()+5);
            }else{
                $this->pdf->Ln();
            }
        }
        $this->pdf->setY(-93);
        $this->pdf->multicell(0, 4.5, "Petunjuk : 
1. Importir;
2. Pejabat pemeriksa barang;
3. Pejabat yang mengawasi pengeluaran barang.
Formulir ini dicetak secara otomatis  oleh sistem komputer dan tidak memerlukan nama, tanda tangan pejabat, dan cap dinas.

Diberitahukan  bahwa dari hasil penelitian dokumen, terhadap barang dalam PIB dengan nomor pendaftaran  tersebut di atas disetujui untuk  dikeluarkan dengan pemeriksaan fisik di tempat Saudara, dengan ketentuan sebagai berikut:
1. Dilakukan penyegelan dan/atau pengawalan oleh Pejabat Bea dan Cukai;
2. Wajib memberikan bantuan yang layak kepada Pejabat Bea dan Cukai yang melaksanakan.

Pejabat Pemeriksa Dokumen


Tanda Tangan :
Nama        : " . $this->res['PEJABAT1'] . "  
NIP         : " . $this->res['NIP1'], 0, 'L');
        if($jmlConr>14){
            while($i<=$jmlConr){
                $this->pdf->addPage();
                $this->pdf->SetFont('courier', '', '11');
                $this->pdf->multicell(0, 5, $this->ktr['KOP'], 0, 'L');
                $this->pdf->Ln(10);
                $this->pdf->SetFont('courier', 'BU', '13');
                $this->pdf->cell(0, 5, 'SURAT PEMBERITAHUAN PEMERIKSAAN FISIK (SPPF)', 0, 0, 'C', 0);
                $this->pdf->Ln();
                $this->pdf->SetFont('courier', '', '10');
                $this->pdf->multicell(0, 4.5, 'Nomor : ' . $this->res['DOKRESNO'] . '   Tanggal : '. date('d-m-Y',strtotime($this->res['DOKRESTG'])) , 0, 'C');
                $this->pdf->Ln(8);
                $this->pdf->cell(180, 4.5, 'Nomor Pengajuan           : ' . $this->formatCar($this->hdr['CAR']), 0, 1, 'L', 0);
                $this->pdf->cell(180, 4.5, 'Nomor Pendaftaran PIB     : ' . $this->hdr['PIBNO'] . '   Tanggal : ' . date('d-m-Y',strtotime($this->hdr['PIBTG'])) , 0, 1, 'L', 0);
                
                $awl = $i;
                $akr = ($jmlConr > $awl+79)?$awl+79: $jmlConr;
                foreach($arr as $a){
                    $this->pdf->cell($a['WEIGHT'], 5, $a['CAPTION'] , 1, 0, 'L', 0);
                }
                if(($akr - $awl)>0){
                    $this->pdf->setX($this->pdf->getX()+5);
                    foreach($arr as $a){
                        $this->pdf->cell($a['WEIGHT'], 5, $a['CAPTION'] , 1, 0, 'L', 0);
                    }
                }
                $this->pdf->Ln();
                for ($i = $awl; $i <= $akr; $i++) {
                    foreach ($arr as $a) {
                        if ($a['CAPTION'] == 'No.') {
                            $this->pdf->cell($a['WEIGHT'], 5, $i, 1, 0, 'L', 0);
                        } else {
                            $this->pdf->cell($a['WEIGHT'], 5, $this->conr[$i - 1][$a['FIELD']], 1, 0, 'L', 0);
                        }
                    }
                    if ($i % 2 == 1) {
                        $this->pdf->setX($this->pdf->getX() + 5);
                    } else {
                        $this->pdf->Ln();
                    }
                }
                
                $this->pdf->setY(-30);
                $this->pdf->cell(100, 4.5, 'Pejabat yang memeriksa dokumen I/II' , 0, 0, 'L', 0);
                $this->pdf->cell(100, 4.5, 'Pejabat yang melaksanakan pengeluaran barang:' , 0, 1, 'L', 0);
                $this->pdf->cell(100, 4.5, 'Tanda Tangan : .............................' , 0, 0, 'L', 0);
                $this->pdf->cell(100, 4.5, 'Tanda Tangan : .............................' , 0, 1, 'L', 0);
                $this->pdf->cell(100, 4.5, 'Nama         : ' . $this->res['PEJABAT1'] , 0, 0, 'L', 0);
                $this->pdf->cell(100, 4.5, 'Nama         : ' . $this->res['PEJABAT2'] , 0, 1, 'L', 0);
                $this->pdf->cell(100, 4.5, 'NIP          : ' . $this->res['NIP1'], 0, 0, 'L', 0);
                $this->pdf->cell(100, 4.5, 'NIP          : ' . $this->res['NIP2'], 0, 1, 'L', 0);
                $this->pdf->line(5,$this->pdf->getY(),205,$this->pdf->getY());
                $this->pdf->cell(0, 4.5, 'Lembar 1 Untuk DJBC' . $this->res['NIP2'], 0, 1, 'L', 0);
            }
        }
    #=====================================================
    }
    */
}