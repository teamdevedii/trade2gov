<?php
if (!defined('BASEPATH')){
    exit('No direct script access allowed');
}
class actTranslator extends CI_Model {
    const dirMAP = '/home/dev/t2g/Mapper/';
    const dirEDI = '/home/dev/t2g/EDI/';
    const dirFLT = '/home/dev/t2g/FLAT/';
    private function roleEDIFILE2($APRF){ //Read EDF
        $dirEDF = self::dirMAP . $APRF . '.EDF';
        $arrEDF = file($dirEDF);
        $rtn    = array();
        $grp    = false;
        foreach ($arrEDF as $a){//looping isi file EDF
            $a = trim($a);
            if(($a !== '') && !$this->startsWith($a,'*') && !$this->startsWith($a,'PRM')){
                $arrSem = explode(',', $a);
                $arrNum = explode(' ', $arrSem[count($arrSem) - 1]);
                $arrSem[count($arrSem) - 1] = $arrNum[0];
                switch ($arrSem[0]){
                    case'SEG':
                        $key= $arrSem[5];
                        $i[$key]++;
                        $rtn[$key][$i[$key]]['SEGMEN']  = $arrSem[1];
                        break;
                    case'DEL':
                        $rtn[$key][$i[$key]]['VALUE'][] = 'PLUS';
                        if($arrSem[3]!=''){
                            $rtn[$key][$i[$key]]['VALUE'][] = $arrSem[3];
                        }    
                        break;
                    case'':
                        $rtn[$key][$i[$key]]['VALUE'][] = 'TITIKDUA';
                        $rtn[$key][$i[$key]]['VALUE'][] = $arrSem[3];
                        break;
                }
            }
        }
        return $rtn;
    }
    private function roleFLTFILE2($APRF){
        $dirHDF = self::dirMAP . $APRF . '.HDF';
        $arrHDF = file($dirHDF);
        $rtn    = array();
        foreach ($arrHDF as $a){
            $a = trim($a);
            if (($a !== '') && (!$this->startsWith($a,'*'))) {
                $num    = explode(' ', $a);
                $arrSem = explode(',',$num[0]);
                if ($arrSem[0] === 'REC'){
                    $key = $arrSem[1];$i=0;$start=0;
                    $rtn[$key] = array();
                }elseif($arrSem[0] === 'FLD'){
                    if($arrSem[1]!=='TYPE'){
                        $rtn[$key][$i]['VARIABLE']  = $arrSem[1];
                        $rtn[$key][$i]['START']     = $start;
                        $rtn[$key][$i]['LENGTH']    = $arrSem[2] + $arrSem[4];
                        $rtn[$key][$i]['TYPE']      = $arrSem[3];
                        $rtn[$key][$i]['DESC']      = $arrSem[4];
                        $rtn[$key][$i]['MODEL']     = $arrSem[5];
                        $i++;
                    }
                    $start += $arrSem[2] + $arrSem[4];;
                }elseif($arrSem[0] === 'PRM'){
                    $rtn['PRM'][$arrSem[1]][$arrSem[2]] = $arrSem[3];
                }
            }
        }
        return $rtn;
    }
    private function roleEDIFILE($APRF){ //Read EDF
        $dirEDF = self::dirMAP . $APRF . '.EDF';
        $arrEDF = file($dirEDF);
        $group  = '';
        $level  = 0;
        $key    = -1;
        foreach ($arrEDF as $a){//looping isi file EDF
            $a = trim($a);
            if(($a !== '') && !$this->startsWith($a,'*') && !$this->startsWith($a,'PRM')){
                $arrSem = explode(',', $a);
                $arrNum = explode(' ', $arrSem[count($arrSem) - 1]);
                $arrSem[count($arrSem) - 1] = $arrNum[0];
                switch ($arrSem[0]){
                    case'SEG':
                        $key++;
                        $sub    = -1;
                        $rtn[$key]['SEGMEN']= $arrSem[1];
                        $rtn[$key]['WAJIB'] = $arrSem[2];
                        $rtn[$key]['LEVEL'] = $arrSem[3];
                        $rtn[$key]['LOOP']  = $arrSem[4];
                        $rtn[$key]['KDHDF'] = $arrSem[5];
                        break;
                    case'DEL':
                        $sub++;
                        if($arrSem[3]!=''){
                            $rtn[$key]['VALUE'][$sub]['VARIABLE'][] = $arrSem[3];
                            $rtn[$key]['VALUE'][$sub]['WAJIB'][] = $arrSem[2];
                        }
                        break;
                    case'':
                        $rtn[$key]['VALUE'][$sub]['VARIABLE'][] = $arrSem[3];
                        $rtn[$key]['VALUE'][$sub]['WAJIB'][] = $arrSem[2];
                        break;
                    case'GRP':
                        $key++;
                        $rtn[$key]['SEGMEN']= 'GRP';
                        $rtn[$key]['GROUP'] = $arrSem[1];
                        $rtn[$key]['WAJIB'] = $arrSem[2];
                        $rtn[$key]['LOOP']  = $arrSem[3];
                        $rtn[$key]['LEVEL'] = $arrSem[4];
                        break;
                }
            }
        }
        foreach ($rtn as $a){
            if($a['KDHDF']!='' || $a['SEGMEN']=='GRP'){
                $x[] = $a;
            }
        }
        return $x;
    }
    private function roleFLTFILE($APRF){
        $dirHDF = self::dirMAP . $APRF . '.HDF';
        $arrHDF = file($dirHDF);
        $rtn    = array();
        foreach ($arrHDF as $a){
            $a = trim($a);
            if (($a !== '') && (!$this->startsWith($a,'*'))) {
                $num    = explode(' ', $a);
                $arrSem = explode(',',$num[0]);
                if ($arrSem[0] === 'REC'){
                    $key = $arrSem[1];$i=0;$start=0;
                    $rtn[$key] = array();
                }elseif($arrSem[0] === 'FLD'){
                    if($arrSem[1]!=='TYPE'){
                        $rtn[$key][$i]['VARIABLE']  = $arrSem[1];
                        $rtn[$key][$i]['START']     = $start;
                        $rtn[$key][$i]['LENGTH']    = $arrSem[2] + $arrSem[4];
                        $rtn[$key][$i]['TYPE']      = $arrSem[3];
                        $rtn[$key][$i]['DESC']      = $arrSem[4];
                        $rtn[$key][$i]['MODEL']     = $arrSem[5];
                        $i++;
                    }
                    $start += $arrSem[2] + $arrSem[4];;
                }elseif($arrSem[0] === 'PRM'){
                    $rtn['PRM'][$arrSem[1]][$arrSem[2]] = $arrSem[3];
                }
            }
        }
        return $rtn;
    }
    private function strReplacePRM($EDIFACT, $SNRF, $NOSG){
        $EDIFACT = str_replace(chr(252).'SNRF'.chr(252), $SNRF, $EDIFACT);
        $EDIFACT = str_replace(chr(252).'DATE'.chr(252), date('ymd'), $EDIFACT);
        $EDIFACT = str_replace(chr(252).'TIME'.chr(252), date('Hi'), $EDIFACT);
        $EDIFACT = str_replace(chr(252).'NOSG'.chr(252), $NOSG, $EDIFACT);
        return $EDIFACT;
    }
    function getEDIFILE($EDINUMBER, $APRF, $SNRF, $FILENAME){
        $dirEDI  = self::dirEDI . $EDINUMBER . '/';
        $dirFLT  = self::dirFLT . $EDINUMBER . '/';
        $roleFLT = $this->roleFLTFILE2($APRF);
        $roleEDI = $this->roleEDIFILE2($APRF);
        $arr = $roleFLT['PRM'];
        $PETIKSATU   = chr(255);
        $PLUS        = chr(254);
        $TITIKDUA    = chr(253);
        foreach($arr as $k=>$a){
            $prm[$k] = str_replace('XXXXX', chr(252), $a);
        }
        $filedir = $dirFLT . $FILENAME;
        if(file_exists($filedir)){
            $arrFile = file($filedir);
            #flt 2 array
            foreach($arrFile as $k=>$a){//loop isi flatfile
                $key        = substr($a,0,8);
                $keyFLT     = $roleFLT[$key];
                $dataFLT    = $prm[$key];
                $dataFLT['PETIKSATU']   = $PETIKSATU;
                $dataFLT['PLUS']        = $PLUS;
                $dataFLT['TITIKDUA']    = $TITIKDUA;
                foreach($keyFLT as $vFLT){//loop key yang telah terpilih substr($a,0,8)
                    $data = rtrim(substr($a,$vFLT['START'],$vFLT['LENGTH']));
                    $data = str_replace("'","?'",str_replace('+','?+',str_replace(':','?:',str_replace('?','??',$data))));
                    if($data!==''){
                        if($vFLT['LENGTH']!=='' && $vFLT['TYPE']!=='X'){
                            switch ($vFLT['TYPE']){
                                case'N':
                                    if($vFLT['DESC']>0){
                                        $desc = '0.'.substr($data,$vFLT['DESC'] * -1)+0;
                                        $data = substr($data,0,$vFLT['LENGTH']-$vFLT['DESC'])+$desc;
                                        if($vFLT['MODEL']=='I'){
                                            $data = str_replace('.', '', $data);
                                        }
                                    }else{
                                        $data = (int)$data;
                                    }
                                    break;
                                case'ZN':
                                    if($vFLT['DESC']>0){
                                        switch ($vFLT['MODEL']){
                                            case'I':$desc = substr($data,$vFLT['DESC'] * -1);break;
                                            case'E':$desc = '.'.substr($data,$vFLT['DESC'] * -1);break;
                                        }
                                    }
                                    $data = substr($data,0,$vFLT['LENGTH']-$vFLT['DESC']).$desc;
                                    break;
                            }
                        }
                        $dataFLT[$vFLT['VARIABLE']] = $data;
                    }
                }
                $keyEDI     = $roleEDI[$key];
                foreach ($keyEDI as $tagEDI){
                    $perBaris   = $tagEDI['SEGMEN'];
                    foreach($tagEDI['VALUE'] as $val){
                        $perBaris .= $dataFLT[$val];
                    }
                    $chk = $perBaris;
                    $chk = str_replace(chr(253), '', str_replace($PLUS, '', str_replace($tagEDI['SEGMEN'], '', $chk)));
                    if($chk!==''){
                        $EDIFACT .= $perBaris . $dataFLT['PETIKSATU'];
                        ++$ii;
                    }
                }
            }
            #menghilangkan tag yang kosong
            
            while(strpos($EDIFACT,$TITIKDUA.$PLUS) || strpos($EDIFACT, $PLUS.$PETIKSATU) || strpos($EDIFACT,$TITIKDUA.$PETIKSATU)){
                $EDIFACT = str_replace($TITIKDUA.$PLUS, $PLUS, $EDIFACT);
                $EDIFACT = str_replace($PLUS.$PETIKSATU, $PETIKSATU, $EDIFACT);
                $EDIFACT = str_replace($TITIKDUA.$PETIKSATU, $PETIKSATU, $EDIFACT);
            }
            $EDIFACT = str_replace($PLUS.$TITIKDUA, $PLUS, $EDIFACT);
            $EDIFACT = str_replace($PETIKSATU, "'\n", $EDIFACT);
            $EDIFACT = str_replace($PLUS, '+', $EDIFACT);
            $EDIFACT = str_replace($TITIKDUA, ':', $EDIFACT);
            $EDIFACT = $this->strReplacePRM($EDIFACT, $SNRF, ($ii-2));
            
            $handle = fopen($dirEDI.substr($FILENAME,0,-4).'.EDI', 'w' );
            fwrite($handle, $EDIFACT);
            fclose($handle);
            $rtn = true;
        }else{
            $rtn = false;
        }
        return $rtn;
    }
    
    function fixLen($str, $len, $chr = ' ', $alg = STR_PAD_RIGHT) {
        $hasil = str_pad(substr($str, 0, $len), $len, $chr, $alg);
        return $hasil;
    }
    
    function cetakFF($key,$arrKey,$dataEDF){
        $rtn = $key;
        foreach($arrKey as $arr){
            $str = $dataEDF[$arr['VARIABLE']];
            switch ($arr['TYPE']){
                case'X':
                    $rtn .= $this->fixLen($str,$arr['LENGTH']);
                    break;
                case'N':
                    if($str != ''){
                        if($arr['DESC']>0){
                            $angka = explode('.', $str);
                            $rtn .= $this->fixLen($angka[0],$arr['LENGTH'] - $arr['DESC'],'0',STR_PAD_LEFT) . $this->fixLen($angka[1],$arr['DESC'],'0');
                        }else{
                            $rtn .= $this->fixLen($str,$arr['LENGTH'],'0',STR_PAD_LEFT);
                        }
                    }else{
                        $rtn .= $this->fixLen('',$arr['LENGTH']);
                    }
                    break;
                case'ZN':
                    if($arr['DESC']>0){
                        $angka = explode('.', $str);
                        $rtn .= $this->fixLen($angka[0],$arr['LENGTH'] - $arr['DESC'],'0',STR_PAD_LEFT) . $this->fixLen($str,$arr['DESC'],'0');
                    }else{
                        $rtn .= $this->fixLen($angka[0],$arr['LENGTH'],'0',STR_PAD_LEFT);
                    }
                    break;
            }
        }
        return trim($rtn) . "\n";
    }
    
    function getFLTFILE($EDINUMBER,$APRF,$FILENAME){
        
        
        $dirEDI  = self::dirEDI . $EDINUMBER . '/';
        $dirFLT  = self::dirFLT . $EDINUMBER . '/';
        $roleEDI = $this->roleEDIFILE($APRF);
        foreach($roleEDI as $v){
            $keyRoleEDI[$v['KDHDF']] .= '|' . $v['SEGMEN'];
        }unset($keyRoleEDI['']);
        $roleFLT = $this->roleFLTFILE($APRF);unset($roleFLT['PRM']);
        foreach($roleFLT as $k=>$v){
            foreach ($v as $i=>$vv){
                $keyRoleFLT[$k][$i] = $vv['VARIABLE'];
            }
        }
        $filedir = $dirEDI . $FILENAME;
        if(file_exists($filedir)){
            $arrFile    = file($filedir);
            $arrFile    = str_replace("?'",chr(255),str_replace('?+',chr(254),str_replace('?:',chr(253),str_replace('??',chr(252),$arrFile))));
            $keyEDF     = 0;
            foreach ($arrFile as $ln => $lnfile){#loop every line of edi file
                $arr        = explode("'", $lnfile);
                $lnfile     = $arr[0];
                $nextKey    = true;
                while($nextKey){#perulangan untuk mencari kunci yang cocok
                    $role   = $roleEDI[$keyEDF];
                    if($role['LEVEL'] == 0){unset($grp);}
                    $keyTerjauh = ($keyTerjauh <= $keyEDF)?$keyEDF:$keyTerjauh;
                    if($role==''){$roleHabis = true;break;}#break poin
                    if($this->startsWith($lnfile, $role['SEGMEN'])){
                        if($role['LEVEL'] > 0){
                            $grp[$role['LEVEL']]['KETEMU'] = true;
                        }
                        $KDHDF  = $role['KDHDF'];
                        $nextKey= false;
                        $str    = substr($lnfile,4);
                        $arrSub = explode('+', $str);
                        foreach ($arrSub as $i=>$a){
                            $arrSubSub = explode(':', $a);
                            foreach ($arrSubSub as $ii=>$aa){
                                $data[$role['VALUE'][$i]['VARIABLE'][$ii]] = $aa;
                            }
                        }
                        $segGroup  .= '|'. $role['SEGMEN'];
                        $nextSegStr = substr($arrFile[$ln+1],0,3);
                        if(strpos($keyRoleEDI[$KDHDF], $nextSegStr) === false || $segGroup == $keyRoleEDI[$KDHDF] || $nextSegStr == $role['SEGMEN']){
                            $FF    .= $this->cetakFF($KDHDF,$roleFLT[$KDHDF],$data);
                            unset($data);
                            unset($segGroup);
                        }
                    }
                    elseif($role['SEGMEN']==='GRP'){#daftarin group yang akan diulang sebanyak $role['LOOP']
                        $jmlGrp = count($grp);
                        if((($grp[$jmlGrp]['KETEMU'] == true || $jmlGrp == 0) && $role['LEVEL'] > $jmlGrp) || $roleEDI[$keyEDF-1]['SEGMEN'] == 'GRP'){
                            if($role['LEVEL']<$jmlGrp){
                                for($i=$role['LEVEL'];$i<=$jmlGrp;$i++){
                                    unset($grp[$i]);
                                }
                            }
                            $grp[$role['LEVEL']]['KEYEDF']  = $keyEDF + 1;
                            $grp[$role['LEVEL']]['NAMA']    = $role['GROUP'];
                            $grp[$role['LEVEL']]['KETEMU']  = false;
                            $keyEDF++;
                        }
                        elseif($grp[$jmlGrp]['KETEMU']  == true && $role['LEVEL'] == $jmlGrp){
                            if(is_array($data)){
                                $FF    .= $this->cetakFF($KDHDF,$roleFLT[$KDHDF],$data);
                                unset($data);
                                unset($segGroup);
                            }
                            $grp[$role['LEVEL']]['KETEMU']  = false;
                            $keyEDF =  $grp[$jmlGrp]['KEYEDF'];
                        }
                        elseif($grp[$jmlGrp]['KETEMU']  == false && $jmlGrp == 1){
                            unset($grp[$jmlGrp]);
                            $keyEDF =  $keyTerjauh;
                        }
                        else{
                            unset($grp[$jmlGrp]);
                            if(is_array($data)){
                                $FF    .= $this->cetakFF($KDHDF,$roleFLT[$KDHDF],$data);
                                unset($data);
                                unset($segGroup);
                            }
                            if($jmlGrp > 1){
                                $keyEDF = $grp[$jmlGrp - 1]['KEYEDF'];
                                $grp[$jmlGrp - 1]['KETEMU'] = false;
                            }else{
                                unset($grp[0]);
                                $keyEDF++;
                            }
                        }
                    }
                    else{
                        $keyEDF++;
                    }
                }
                if($roleHabis){#berakhir jika role habis meskipun file masih ada.
                    break;
                }
            }
            $FF = str_replace(chr(255),"'",str_replace(chr(254),'+',str_replace(chr(253),':',str_replace(chr(252),'?',$FF))));
            $handle = fopen($dirFLT.substr($FILENAME,0,-4).'.FLT', 'w' );
            fwrite($handle, $FF);
            fclose($handle);
            $rtn = true;
        }else{
            $rtn = false;
        }
        
    }
    private function startsWith($haystack, $needle) {
        return $needle === '' || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
    }
    private function endsWith($haystack, $needle) {
        return $needle === '' || strpos($haystack, $needle, strlen($haystack) - strlen($needle)) !== FALSE;
    }
    
}