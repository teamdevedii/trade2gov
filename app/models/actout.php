<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class actOut extends CI_Model {

    function getRegistrasi($kd_trader='') {
        $this->load->model('actMain');
        if ($kd_trader != '') { #Gak Dipake
            $query = "SELECT a.* FROM m_trader a WHERE a.KODE_TRADER = '" . $kd_trader . "'";
            $data = $this->actMain->get_result($query);
            $act = site_url('usermanage/approvalRegistrasi');
        } else {
            $act = site_url('outLogin/regCustomer/Simpan');
        }
        $arrdata = array(
            'DATA' => $data,
            'kpbc_edinumber' => $this->newsession->userdata('LOGGED'),
            'JENIS_IDENTITAS' => $this->actMain->get_mtabel('JENIS_IDENTITAS', 1, false),
            'TIPE_TRADER' => $this->actMain->get_mtabel('TIPE_TRADER', 1, false),
            'MAXLENGTH' => '<input type="hidden" id="MAXLENGTH" value="' . $this->actMain->get_uraian("SELECT F_MAXLENGTH('m_trader') as MAXLENGTH", 'MAXLENGTH') . '"/>',
            'act' => $act);
        return $arrdata;
    }

    function getRequest() {
        $this->load->model('actMain');
        $query = "SELECT a.* FROM t_formrequest where 1=1";
    }

    function setRegistrasi($act) {
        $this->load->model('actMain');
        $data = $this->input->post('DATA');
        $data['ID'] = str_replace(".", "", $data['ID']);
        $data['ID'] = str_replace("-", "", $data['ID']);
        $data['DOKUMEN'] = implode(",", $data['DOKUMEN']);
        switch (strtolower($act)) {
            case'simpan':
                $strValid = $this->validationRegister($data);
                if ($strValid == '') {
                    $exec = $this->actMain->insertRefernce('m_trader', $data);
                    $id = $this->db->insert_id();
                } else {
                    return $strValid;
                }
                break;
            case'approve':
                $exec = $this->actMain->insertRefernce('m_trader', $data);
                break;
        }
        if ($exec) {
            return '1|' . $id;
        } else {
            return '0|';
        }
    }

    function validationRegister($data) {
        $this->load->model("actMain");
        $hasil = '';
        //validasi email
        $chkMail = $this->actMain->get_uraian(
                "SELECT ((SELECT COUNT(KODE_TRADER) AS JML FROM m_trader WHERE EMAIL_PEMILIK = '" . $data['EMAIL_PEMILIK'] . "')+"
                . "(SELECT COUNT(USERNAME) AS JML FROM t_user WHERE USERNAME = '" . $data['EMAIL_PEMILIK'] . "')) AS JML FROM DUAL", 'JML');
        if ($chkMail > 0)
            $hasil .= 'e-Mail sudah terdaftar sebelumnya.<br>';

        //validasi nama Perusahaan
        $chkComp = $this->actMain->get_uraian("SELECT COUNT(NAMA) as JML FROM m_trader WHERE NAMA = '" . $data['NAMA'] . "'", 'JML');
        if ($chkComp > 0)
            $hasil .= 'Nama Perusahaan telah Terdaftar.<br>';

        //validasi ID Perusahaan
        $chkNPWP = $this->actMain->get_uraian("SELECT COUNT(ID) as JML FROM m_trader WHERE ID = '" . $data['ID'] . "'", 'JML');
        if ($chkNPWP > 0)
            $hasil .= 'Identitas Perusahaan telah Terdaftar.<br>';

        if ($hasil != '') {
            $rtn = 'MSG|ERR||' . $hasil;
        } else {
            $rtn = '';
        }
        return $rtn;
    }

    function setRequest($act) {
        $this->load->model("actMain");
        $data = $this->input->post('DATA');
        if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
            $rtn = 'Gagal Request';
            exit();
        } else {
            $code = $this->input->post('_code');
            $xxx = "SELECT COUNT(EMAIL) as TOTAL FROM t_formrequest WHERE EMAIL = '" . $data['EMAIL'] . "'";
            $cek = $this->actMain->get_uraian($xxx, 'TOTAL');
            if ($cek > 0) {
                $rtn = 'E-Mail Anda telah Terdaftar. Gunakan E-mail Lainnya';
            } elseif (strtolower($code) != $_SESSION['captkodex']) {
                $rtn = 'Kode Captcha tidak sesuai';
            } else {
                $exec = $this->db->insert('t_formrequest', $data);
                if (!$exec) {
                    $rtn = 'Gagal';
                } else {
                    $rtn = 'Request berhasil Dikirim, Silahkan Tunggu Konfirmasi';
                }
            }
        }
        echo $rtn;
    }
}
?>