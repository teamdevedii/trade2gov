<?php
if (!defined('BASEPATH')){exit('No direct script access allowed');}
class dokBC10 extends CI_Model {
    var $main = '';      #load actMain
    var $pdf = '';       #load fpdf library
    var $nPungutan = ''; #pungutan
    var $car = '';       #Car
    var $hdr = '';       #data header dalam array 1D
    var $res = '';       #data header dalam array 1D
    var $dok = '';       #data dokumen dalam array 2D
    var $con = '';       #data container dalam array 2D
    var $conr = '';      #data container dalam array 2D
    var $kms = '';       #data kemasan dalam array 2D
    var $dtl = '';       #data detil dalam array 2D
    var $pgt = '';       #data pungutan dalam array 2D
    var $ntb = '';       #data ntb dalam array 1D
    var $npt = '';       #data ntp dalam array 1D
    var $ktr = '';       #data kantor kpbc dalam array 1D
    var $rpt = '';       #data caption respon NPP
    var $kd_trader = ''; #session data kode trader
    var $tp_trader = ''; #session data tipe trader
    var $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
    var $segmentUri = array(); #segment url

    function fpdf($pdf) {
        $this->pdf = $pdf;
    }

    function ciMain($main) {
        $this->main = $main;
    }

    function segmentUri($arr) {
        $this->segmentUri = $arr;
    }

    function showPage($car, $jnPage) {
        $this->car = $car;
        $this->kd_trader = $this->newsession->userdata('KODE_TRADER');
        $this->tp_trader = $this->newsession->userdata('TIPE_TRADER');
        $this->pdf->AddPage();
        switch ($jnPage) {
            case'1':
                $this->getHDR();      
                $this->drawHeaderRKSP();
                break;            
            case'100':case'110':case'120':case'200':case'210':case'220':case'230':case'240':case'270':case'900':
            case'470':case'130':case'600':case'700':case'295':case'250':
                $this->getHDR();
                $this->getRES();
                $this->isiRespon();
                $this->drawResponUmum();
                break;
            case'280';
                $this->getHDR();
                $this->getRES();                
                $this->drawResponSPPB();
                break;
            case'470';
                $this->getHDR();
                $this->getRES();                
                $this->drawResponPembatalan(); 
                break;
            case'500';
                $this->getHDR();
                $this->getRES();                
                $this->drawResponSPKPBM(); 
                break;
            default :
                $this->pdf->SetFont('times', 'B', '12');
                $this->pdf->cell(131.6, 4, 'Tidak ada jenis laporan ini', 0, 0, 'R', 0);
                break;
        }
        $this->pdf->Output();
    }
    
    function getHDR() {
        $sql = "SELECT *, 
            F_TABEL('BC10','STATUS',STATUS) AS URSTATUS,
                    F_TABEL('BC10','JNSMANIFEST',JNSMANIFEST) AS JNSMANIFEST,
                    F_KPBC(KDKPBC)   AS URKDKPBC ,                    
                    DATE_FORMAT(TGBC10,'%d-%m-%Y') AS TGBC10,
                    DATE_FORMAT(ETATGVOY,'%d-%m-%Y') AS ETATGVOY,
                    DATE_FORMAT(ETDTGVOY,'%d-%m-%Y') AS ETDTGVOY,       
                    F_NEGARA(FLAGMODA) AS URFLAGMODA,       
                    F_PELAB(PELTRANSIT) AS URPELTRANSIT,
                    F_PELAB(PELBERIKUT) AS URPELBERIKUT,
                    F_PELAB(PELMUAT) AS URPELMUAT,
                    F_PELAB(PELBONGKAR) AS URPELBONGKAR  
                FROM t_bc10hdr WHERE CAR = '" . $this->car . "' AND KODE_TRADER = '" . $this->kd_trader . "'"; //die($sql);
        $this->hdr = $this->main->get_result($sql);
        //print_r($this->hdr);die();
    }

    function getRESTPB() {
        $sql = "SELECT *,concat(KDGUDANG,'    /  ',b.URAIAN) as 'URGUDANG'
                FROM   t_bc23RESTPB a LEFT JOIN  M_GUDANG b ON a.KDGUDANG = b.KDGDG
                WHERE  CAR = '" . $this->car . "' AND "
                . "    KODE_TRADER = '" . $this->kd_trader . "' AND "
                . "    RESKD = '" . $this->segmentUri[4] . "' AND "
                . "    RESTG = '" . $this->segmentUri[5] . "' AND "
                . "    RESWK = '" . $this->segmentUri[6] . "'";
        $this->res = $this->main->get_result($sql);
        $this->getKTR($this->res['KPBC']);
        //print_r($this->res);die();
    }

    function getKTR($kdKPBC){
        $SQL = "SELECT URAIAN_KPBC,KOTA,ESELON FROM m_kpbc WHERE KDKPBC = '".$kdKPBC."'";
        $this->ktr = $this->main->get_result($SQL);
        $this->ktr['KWBC'] = $this->main->get_uraian("SELECT URAIAN_KPBC FROM m_kpbc WHERE KDKPBC = '".$this->ktr['ESELON']."'",'URAIAN_KPBC');
        if($kdKPBC==='040300'){
            $this->ktr['KOP'] = "KEMENTERIAN KEUANGAN REPUBLIK INDONESIA\nDIREKTORAT JENDERAL BEA DAN CUKAI\nKANTOR PELAYANAN UTAMA TANJUNG PRIOK";
        }elseif($kdKPBC==='020400'){
            $this->ktr['KOP'] = "KEMENTERIAN KEUANGAN REPUBLIK INDONESIA\nDIREKTORAT JENDERAL BEA DAN CUKAI\nKANTOR PELAYANAN UTAMA BATAM";
        }else{
            $this->ktr['KOP'] = "KEMENTERIAN KEUANGAN REPUBLIK INDONESIA\nDIREKTORAT JENDERAL BEA DAN CUKAI\n" . (($this->ktr['KWBC']=='')?'':$this->ktr['KWBC']."\n"). str_replace('KPPBC', 'KANTOR PENGAWASAN DAN PELAYANAN',str_replace('KPU', 'KANTOR PELAYANAN UTAMA',strtoupper($this->ktr['URAIAN_KPBC'])));
        }
    }
    
    function drawHeaderRKSP(){
        $noPrinLampiranDok = '';
        $this->pdf->SetMargins(5.4, 0, 0);
        $this->pdf->SetAutoPageBreak(0, 0);
        $this->pdf->SetY(5.4);
        $this->pdf->Ln();
        $this->pdf->SetX(137);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->cell(67.6, 4, 'BC 1.0', 10, 0, 'R', 0);
        $this->pdf->SetFont('times', '', '10');
        $this->pdf->Ln(5); 
        $this->pdf->multicell(200, 6, $this->hdr['NMAGEN']. ' ( NPWP : '. $this->formatNPWP($this->hdr['NOIDAGEN']).' ) ' ."\n" .$this->hdr['ALAGEN'], 1, 'C');
        $this->pdf->Ln(2); 
        $this->pdf->SetFont('times', 'B', '12');
        $this->pdf->multicell(200, 6, "RENCANA KEDATANGAN SARANA PENGANGKUT \n" . ' ( '. $this->hdr['JNSMANIFEST'].' ) ', 0, 'C');
        
        
        $this->pdf->SetFont('times', '', '9');
        //KPBC
        $this->pdf->Ln(2);                 
        #$this->pdf->Rect(5.4, 29.4, 199, 14, 1.5, 'F');
        $this->pdf->Ln();
        $this->pdf->cell(45, 4, 'No. & Tgl BC 1.0', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(103, 4, $this->hdr['NOBC10'].' / '.$this->hdr['TGBC10'], 0, 0, 'L', 0);                       
        $this->pdf->Ln();
        $this->pdf->cell(45, 4, 'Nomor Pengajuan', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(80, 4, substr($this->hdr['CAR'], 0, 6) . " - " . substr($this->hdr['CAR'], 6, 6) . " - " . substr($this->hdr['CAR'], 12, 8) . " - " . substr($this->hdr['CAR'], 20), 0, 0, 'L', 0);
        
        $this->pdf->Line(205.4,53,6.5,53);
        
        $this->pdf->Ln(5);
        $this->pdf->cell(45, 4, 'KEPADA :', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(20, 4, 'KANTOR PELAYANAN BEA DAN CUKAI', 0, 0, 'L', 0);        
        $this->pdf->Ln();
        $this->pdf->cell(20, 4, $this->hdr['URKDKPBC'], 0, 0, 'L', 0);        
        $this->pdf->Ln(7);
        
        $this->pdf->Line(205.4,66,6.5,66);
        
        $this->pdf->cell(45, 4, 'Nama Sarana Pengangkut', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(20, 4, $this->hdr['NMMODA'], 0, 0, 'L', 0); 
        $this->pdf->Ln(5);
        $this->pdf->cell(45, 4, 'G.R.T', 0, 0, 'L', 0);        
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(20, 4, $this->hdr['GT'], 0, 0, 'R', 0); 
        #$this->pdf->setX(55); 
        $this->pdf->cell(8, 4, 'TON', 0, 0, 'C', 0);
        $this->pdf->Ln(5);
        $this->pdf->cell(45, 4, 'LOA', 0, 0, 'L', 0);        
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(20, 4, $this->hdr['LOA'], 0, 0, 'R', 0); 
        #$this->pdf->setX(55); 
        $this->pdf->cell(8, 4, 'MTR', 0, 0, 'C', 0);
        $this->pdf->Ln(5);
        $this->pdf->cell(45, 4, 'Draft Depan', 0, 0, 'L', 0);        
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(20, 4, $this->hdr['DRAFTDEPAN'], 0, 0, 'R', 0); 
        #$this->pdf->setX(55); 
        $this->pdf->cell(8, 4, 'MTR', 0, 0, 'C', 0);
        $this->pdf->Ln(5);
        $this->pdf->cell(45, 4, 'Draft Belakang', 0, 0, 'L', 0);        
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(20, 4, $this->hdr['DRAFTBELAKANG'], 0, 0, 'R', 0);
        #$this->pdf->setX(55); 
        $this->pdf->cell(8, 4, 'MTR', 0, 0, 'C', 0);
        $this->pdf->Ln(5);
        $this->pdf->cell(45, 4, 'Bendera', 0, 0, 'L', 0);        
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(20, 4, $this->hdr['URFLAGMODA'].' ( '.$this->hdr['FLAGMODA'].' ) ', 0, 0, 'L', 0); 
        $this->pdf->Ln(5);
        $this->pdf->cell(45, 4, 'No. Register', 0, 0, 'L', 0);        
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(20, 4, $this->hdr['REGMODA'], 0, 0, 'L', 0); 
        $this->pdf->Ln(5);
        $this->pdf->cell(45, 4, 'No. / Tanggal Voy/Flight', 0, 0, 'L', 0);        
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(103, 4, $this-> hdr['NOBC10'].' / '.$this->hdr['TGBC10'], 0, 0, 'L', 0);  
        $this->pdf->Ln(5);
        $this->pdf->cell(45, 4, 'Nama Pengangkut', 0, 0, 'L', 0);        
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(20, 4, $this-> hdr['KDAGEN'].' - '.$this->hdr['NMAGEN'], 0, 0, 'L', 0);  
        $this->pdf->Ln(10);
        $this->pdf->cell(45, 4, 'Pelabuhan Asal', 0, 0, 'L', 0);        
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(20, 4, $this->hdr['URPELMUAT'].' ( '.$this->hdr['PELMUAT'].' ) ', 0, 0, 'L', 0);  
        $this->pdf->Ln(5);
        $this->pdf->cell(45, 4, 'Pelabuhan Singgah Terakhir', 0, 0, 'L', 0);        
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(20, 4, $this->hdr['URPELTRANSIT'].' ( '.$this->hdr['PELTRANSIT'].' ) ', 0, 0, 'L', 0);  
        $this->pdf->Ln(5);
        $this->pdf->cell(45, 4, 'Pelabuhan Tujuan (Bongkar)', 0, 0, 'L', 0);        
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(20, 4, $this->hdr['URPELBONGKAR'].' ( '.$this->hdr['PELBONGKAR'].' ) ', 0, 0, 'L', 0);  
        $this->pdf->Ln(5);
        $this->pdf->cell(45, 4, 'Pelabuhan Tujuan Berikutnya', 0, 0, 'L', 0);        
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(20, 4, $this->hdr['URPELBERIKUT'].' ( '.$this->hdr['PELBERIKUT'].' ) ', 0, 0, 'L', 0);  
        $this->pdf->Ln(10);
        $this->pdf->cell(45, 4, 'Tanggal / Jam Kedatangan', 0, 0, 'L', 0);        
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(103, 4, $this->hdr['ETATGVOY'], 0, 0, 'L', 0);
        $this->pdf->setX(73); 
        $this->pdf->cell(15, 4, 'Jam :', 0, 0, 'C', 0); 
        $this->pdf->cell(103, 4, $this->hdr['ETAWKVOY'], 0, 0, 'L', 0);
        $this->pdf->Ln(5);
        $this->pdf->cell(45, 4, 'Tanggal / Jam Keberangkatan', 0, 0, 'L', 0);        
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(103, 4, $this->hdr['ETDTGVOY'], 0, 0, 'L', 0);
        $this->pdf->setX(73); 
        $this->pdf->cell(15, 4, 'Jam :', 0, 0, 'C', 0); 
        $this->pdf->cell(103, 4, $this->hdr['ETDWKVOY'], 0, 0, 'L', 0);
        $this->pdf->Ln(5);
        
        #$this->pdf->Rect(5.4, 43.5, 199, 90, 1.5, 'F');
        
        $this->pdf->Ln(15);
        $this->pdf->setX(160);
        
        $this->pdf->Line(205.4,160,6.5,160);
        
        $this->pdf->cell(49.3, 4, "Pengangkut", 0, 'C');
        $this->pdf->Ln(5);
        $this->pdf->setX(144);
        $this->pdf->cell(49.3, 4, $this->hdr['NMAGEN'], 0, 0, 'C', 0);        
        $this->pdf->Ln(15); 
        $this->pdf->setX(144);
        $this->pdf->cell(49.3, 4, $this->hdr['PEMBERITAHU'], 0, 0, 'C', 0); 
        
        $this->pdf->Ln(80);
        $this->pdf->SetFont('courier', '', '8');
        $this->pdf->cell(49.3, 4, 'Dicetak di (GLS) '.$this->hdr['NMAGEN'].' // '.date('m/d/Y').' // '.date('H:i:s'), 0, 0, 'L', 0); 
         $this->pdf->Ln();
        $this->pdf->SetFont('courier', '', '8');
        $this->pdf->cell(49.3, 4, 'Dikirim pada  '.$this->hdr['TGBC10'].' // '.date('H:i:s').' // '.'SNRF :'.$this->hdr['SNRF'].' Status : '.$this->hdr['URSTATUS'], 0, 0, 'L', 0); 
        
        
    }
    
    function headerLampiran($tipe, $lamp) {
        $this->pdf->AddPage();
        $this->pdf->setY(5);
        $this->pdf->Rect(5.4, 17, 199.2, 12, 3.5, 'F');
        $this->pdf->SetFont('times', 'B', '10');
        $this->pdf->cell(200, 4, 'LEMBAR ' . $lamp, 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(200, 4, 'PEMBERITAHUAN IMPOR BARANG UNTUK DITIMBUN DI', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(200, 4, 'TEMPAT PENIMBUNAN BERIKAT', 0, 0, 'C', 0);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->Ln();
        $this->pdf->cell(45, 4, 'Kantor Pabean', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(103, 4, $this->hdr['URKDKPBC'], 0, 0, 'L', 0);
        $this->pdf->cell(15, 4, $this->hdr['KDKPBC'], 1, 0, 'C', 0);
        $this->pdf->cell(30, 4, " Halaman " . $this->pdf->PageNo() . " dari {nb}", 0, 0, 'R', 0);
        //$this->pdf->AliasNbPages();
        $this->pdf->Ln();
        $this->pdf->cell(45, 4, 'Nomor Pengajuan', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(80, 4, substr($this->hdr['CAR'], 0, 6) . " - " . substr($this->hdr['CAR'], 6, 6) . " - " . substr($this->hdr['CAR'], 12, 8) . " - " . substr($this->hdr['CAR'], 20), 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(45, 4, 'Nomor Pendaftaran', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(80, 4, $this->pibno . ' / ' . $this->pibtg, 0, 0, 'L', 0);
        switch ($tipe) {
            case 'barang':
                $this->pdf->Ln();
                $Yawl = $this->pdf->getY();
                $this->pdf->multicell(7, 3.5, "31.\nNo\n\n\n ", 1, 'L');
                $this->pdf->setXY($this->pdf->getX() + 7, $Yawl);
                $this->pdf->multicell(72, 3.5, "32 - Pos tarif /HS\n - Uraian barang secara lengkap meliputi jenis, jumlah,\n    merk, type, ukuran, spesifikasi lainnya\n - Jenis Fasilitas\n ", 1, 'L');
                $this->pdf->setXY($this->pdf->getX() + 79, $Yawl);
                $this->pdf->multicell(30, 3.5, "33. Negara Asal\n\n\n\n ", 1, 'L');
                $this->pdf->setXY($this->pdf->getX() + 109, $Yawl);
                $this->pdf->multicell(30, 3.5, "34. Tarif & Fasilitas\n - BM -PPN - PPnBM\n - Cukai       - PPh\n\n ", 1, 'L');
                $this->pdf->setXY($this->pdf->getX() + 139, $Yawl);
                $this->pdf->multicell(30, 3.5, "35. Jumlah & Jenis\n    satuan Barang\n - Berat bersih(Kg)\n - Jumlah & Jenis\n    Kemasan ", 1, 'L');
                $this->pdf->setXY($this->pdf->getX() + 169, $Yawl);
                $this->pdf->multicell(30.2, 3.5, "36. Jumlah Nilai CIF\n\n\n\n ", 1, 'L');
                //-------------------------------------------------------------------------------------------//
                break;
            case 'dokumen':
                $this->pdf->Ln();
                $this->pdf->cell(10, 4, '', 'LTB', 0, 'L', 0);
                $this->pdf->cell(65, 4, 'Jenis Dokumen', 'TB', 0, 'L', 0);
                $this->pdf->cell(64.2, 4, 'Nomor Dokumen', 'TB', 0, 'L', 0);
                $this->pdf->cell(60, 4, 'Tanggal Dokumen', 'TBR', 0, 'L', 0);
                $this->pdf->Ln();
                break;
            case 'kemasan':
                $this->pdf->Ln();
                $this->pdf->cell(10, 4, '', 'LTB', 0, 'L', 0);
                $this->pdf->cell(35, 4, 'Jumlah', 0, 'L', 0);
                $this->pdf->cell(79.2, 4, 'Jenis Kemasan', 'TB', 0, 'L', 0);
                $this->pdf->cell(75, 4, 'Merk Kemasan', 'TBR', 0, 'L', 0);
                $this->pdf->Ln();
                break;
            case 'kontainer':
                $this->pdf->Ln();
                $this->pdf->cell(12.6, 4, 'No.Urut', 'LTB', 0, 'C', 0);
                $this->pdf->cell(37, 4, 'Nomor Kontainer', 'TB', 0, 'L', 0);
                $this->pdf->cell(25, 4, 'Ukuran', 'TB', 0, 'L', 0);
                $this->pdf->cell(25, 4, 'Tipe', 'TBR', 0, 'L', 0);

                $this->pdf->cell(12.6, 4, 'No.Urut', 'LTB', 0, 'C', 0);
                $this->pdf->cell(37, 4, 'Nomor Kontainer', 'TB', 0, 'L', 0);
                $this->pdf->cell(25, 4, 'Ukuran', 'TB', 0, 'L', 0);
                $this->pdf->cell(25, 4, 'Tipe', 'TBR', 0, 'L', 0);
                break;
        }
    }

    function dataLampiranBarang($idPrint = 0) {
        $this->headerLampiran('barang', 'LANJUTAN BARANG');
        $this->pdf->SetFont('times', '', '8');
        $Yawl = $this->pdf->getY();
        $Y = $this->pdf->getY();
        $totData = count($this->dtl);
        for ($i = $idPrint; $i < $totData; $i++) {
            $h = '';
            $this->pdf->setY($Yawl);
            $this->pdf->multicell(7, 3.5, ($i + 1), 0, 'L');
            $h[] = $this->pdf->getY();
            //Uraiana
            $this->pdf->setXY($this->pdf->getX() + 7, $Yawl);
            $fas = ($this->dtl[$i]['KDFASDTL'] != '') ? "\nFas : " . $this->dtl[$i]['KDFASDTL'] . '/' . $this->dtl[$i]['URKDFASDTL'] : '';
            $urai = $this->formaths($this->dtl[$i]['NOHS']) . "\n" . $this->dtl[$i]['BRGURAI'] . "\n" . $this->dtl[$i]['MERK'] . " - " . $this->dtl[$i]['TIPE'] . " - " . $this->dtl[$i]['SPFLAIN'] . $fas;
            $this->pdf->multicell(72, 3.5, $urai, 0, 'L');
            $h[] = $this->pdf->getY();
            //Barang asal
            $this->pdf->setXY($this->pdf->getX() + 79, $Yawl);
            $this->pdf->multicell(30, 3.5, $this->dtl[$i]['BRGASAL'] . ' / ' . $this->dtl[$i]['URBRGASAL'], 0, 'L');
            $h[] = $this->pdf->getY();
            //Perhitungan
            $this->pdf->setXY($this->pdf->getX() + 109, $Yawl);
            $FASBM = ($this->dtl[$i]['FASBM'] > 0) ? $this->getfas($this->dtl[$i]['KDFASBM']) . ' : ' . ($this->dtl[$i]['FASBM'] / 100) . ' %' : '';
            $BM = $this->strip($this->dtl[$i]['TRPBM']) . ' ' . $FASBM;
            $FASCUK = ($this->dtl[$i]['FASCUK'] > 0) ? $this->getfas($this->dtl[$i]['KDFASCUK']) . ' : ' . ($this->dtl[$i]['FASCUK'] / 100) . ' %' : '';
            $CUK = $this->strip($this->dtl[$i]['TRPCUK']) . ' ' . $FASCUK;
            $FASPPN = ($this->dtl[$i]['FASPPN'] > 0) ? $this->getfas($this->dtl[$i]['KDFASPPN']) . ' : ' . ($this->dtl[$i]['FASPPN'] / 100) . ' %' : '';
            $PPN = $this->strip($this->dtl[$i]['TRPPPN']) . ' ' . $FASPPN;
            $FASPBM = ($this->dtl[$i]['FASPBM'] > 0) ? $this->getfas($this->dtl[$i]['KDFASPBM']) . ' : ' . ($this->dtl[$i]['FASPBM'] / 100) . ' %' : '';
            $PBM = $this->strip($this->dtl[$i]['TRPPBM']) . ' ' . $FASPBM;
            $FASPPH = ($this->dtl[$i]['FASPPH'] > 0) ? $this->getfas($this->dtl[$i]['KDFASPPH']) . ' : ' . ($this->dtl[$i]['FASPPH'] / 100) . ' %' : '';
            $PPH = $this->strip($this->dtl[$i]['TRPPPH']) . ' ' . $FASPPH;
            $this->pdf->multicell(30, 3.5, 'BM : ' . $BM . "\nCukai : " . $CUK . "\nPPN : " . $PPN . "\nPBM : " . $PBM . "\nPPh : " . $PPH, 0, 'L');
            $h[] = $this->pdf->getY();
            //kemasan
            $this->pdf->setXY($this->pdf->getX() + 139, $Yawl);
            $satuan = ($this->dtl[$i]['KDSAT'] != '') ? ' ' . $this->dtl[$i]['KDSAT'] . ' / ' . $this->dtl[$i]['URKDSAT'] . ',' . ' BB: ' . number_format($this->dtl[$i]['NETTODTL'], 4, '.', ',') . ' Kg' : '';
            $kms = ((int) $this->dtl[$i]['KEMASJM'] > 0) ? "\n" . $this->dtl[$i]['KEMASJM'] . ' ' . $this->dtl[$i]['KEMASJN'] . ' / ' . $this->dtl[$i]['URKEMASJN'] : '';
            $this->pdf->multicell(30, 3.5, number_format($this->dtl[$i]['JMLSAT'], 4, '.', ',') . $satuan . $kms, 0, 'L');
            $h[] = $this->pdf->getY();
            //Jumlah Nilain CIF
            $this->pdf->setXY($this->pdf->getX() + 169, $Yawl);
            $this->pdf->multicell(30.2, 3.5, number_format($this->dtl[$i]['DCIF'], 4, '.', ','), 0, 'R');
            $Yawl = max($h) + 2;
            if ($Yawl > 230) {
                $i++;
                break;
            }
        }
        //Rect($x, $y, $w, $h, $style='')
        $this->pdf->Rect(5.4, $Y, 7, ($Yawl - $Y));
        $this->pdf->Rect(12.4, $Y, 72, ($Yawl - $Y));
        $this->pdf->Rect(84.4, $Y, 30, ($Yawl - $Y));
        $this->pdf->Rect(114.4, $Y, 30, ($Yawl - $Y));
        $this->pdf->Rect(144.4, $Y, 30, ($Yawl - $Y));
        $this->pdf->Rect(174.6, $Y, 30.2, ($Yawl - $Y));
        $this->footerLampiran();
        if ($i < $totData) {
            $this->dataLampiranBarang($i);
        }
    }

    function dataLampiranDokumen($noPrint, $idPrint = 0) {
        $this->headerLampiran('dokumen', 'LANJUTAN DOKUMEN');
        $this->pdf->SetFont('times', '', '8');
        $Yawl = $this->pdf->getY();
        $Y = $this->pdf->getY();
        $totData = count($this->dok);
        for ($i = $idPrint; $i < $totData; $i++) {
            if (!strstr($noPrint, $this->dok[$i]['DOKKD'])) {
                $h = '';
                $this->pdf->setY($Yawl);
                //Uraian Kode Dokumen
                $this->pdf->cell(10, 4, '', 0, 0, 'L', 0);
                $this->pdf->multicell(65, 4, $this->dok[$i]['URDOKKD'], 0, 'L');
                $h[] = $this->pdf->getY();
                //Nomor Dokumen
                $this->pdf->setXY($this->pdf->getX() + 75, $Yawl);
                $this->pdf->multicell(64.2, 4, $this->dok[$i]['DOKNO'], 0, 'L');
                $h[] = $this->pdf->getY();
                //Tanggal Dokumen
                $this->pdf->setXY($this->pdf->getX() + 139.2, $Yawl);
                $this->pdf->multicell(60, 4, $this->dok[$i]['DOKTG'], 0, 'L');
                $h[] = $this->pdf->getY();

                $Yawl = max($h);
                if ($Yawl > 230) {
                    $i++;
                    break;
                }
            }
        }
        $this->pdf->Rect(5.4, $Y, 199.3, ($Yawl - $Y));
        $this->footerLampiran();
        if ($i < $totData) {
            $this->dataLampiranDokumen($noPrint, $i);
        }
    }

    function dataLampiranKemasan($idPrint = 0) {
        $this->headerLampiran('kemasan', 'LANJUTAN KEMASAN');
        $this->pdf->SetFont('times', '', '8');
        $Yawl = $this->pdf->getY();
        $Y = $this->pdf->getY(); //print_r($this->kms);die();
        $totData = count($this->kms);
        for ($i = $idPrint; $i < $totData; $i++) {
            $h = '';
            $this->pdf->setY($Yawl);
            //Uraian Kode Dokumen
            $this->pdf->cell(10, 4, '', 0, 0, 'L', 0);
            $this->pdf->multicell(35, 4, $this->kms[$i]['JMKEMAS'], 0, 'R');
            $h[] = $this->pdf->getY();
            //Nomor Dokumen
            $this->pdf->setXY($this->pdf->getX() + 45, $Yawl);
            $this->pdf->multicell(79.2, 4, $this->kms[$i]['JNKEMAS'] . ' / ' . $this->kms[$i]['URJNKEMAS'], 0, 'L');    
            $h[] = $this->pdf->getY();
            //Tanggal Dokumen
            $this->pdf->setXY($this->pdf->getX() + 124.2, $Yawl);
            $this->pdf->multicell(75, 4, $this->kms[$i]['MERKKEMAS'], 0, 'L');
            $h[] = $this->pdf->getY();
            $Yawl = max($h);
            if ($Yawl > 230) {
                $i++;
                break;
            }
        }
        $this->pdf->Rect(5.4, $Y, 199.3, ($Yawl - $Y));
        $this->footerLampiran();
        if ($i < $totData) {
            $this->dataLampiranKemasan($i);
        }
    }

    function dataLampiranKontainer($idPrint = 0) {
        $this->headerLampiran('kontainer', 'LANJUTAN KEMASAN');
        $this->pdf->SetFont('times', '', '8');
        $totData = count($this->con);
        $Y = $this->pdf->getY();
        for ($i = $idPrint; $i < $totData; $i++) {
            $this->pdf->Ln();
            $this->pdf->cell(12.6, 4, ($i + 1), 0, 0, 'C', 0);
            $this->pdf->cell(37, 4, $this->setnocont($this->con[$i]['CONTNO']), 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, $this->con[$i]['CONTUKUR'], 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, $this->con[$i]['CONTTIPE'], 0, 0, 'L', 0);
            $i++;
            $this->pdf->cell(12.6, 4, ($this->con[$i]['CONTNO'] != '') ? ($i + 1) : '', 'L', 0, 'C', 0);
            $this->pdf->cell(37, 4, $this->setnocont($this->con[$i]['CONTNO']), 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, $this->con[$i]['CONTUKUR'], 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, $this->con[$i]['CONTTIPE'], 0, 0, 'L', 0);
        }
        $this->pdf->Ln();
        $this->pdf->Rect(5.4, $Y, 199.3, ( $this->pdf->getY() - $Y));
        $this->footerLampiran();
        if ($i < $totData) {
            $this->dataLampiranKontainer($i);
        }
    }

    function footerLampiran() {
        $this->pdf->setXY(150, 255);
        $this->pdf->SetFont('times', '', '9');
        $imp = ($this->tp_trader == '1') ? 'Importir' : 'PPJK';
        $this->pdf->multicell(50, 4, "Jakarta, " . $this->hdr['TANGGAL_TTD'] . "\n" . $imp . "\n\n\n\n\n\n" . $this->hdr['NAMA_TTD'], 0, 'C');
        $this->pdf->SetFont('times', 'I', '9');
        $this->pdf->multicell(99.6, 4, "Tgl.Cetak " . date('d-m-Y'), 0, 'L');
    }
    
    /* ------------------ fungsi pendukung --------------------- */

    function trimstr($strpotong, $panjang) {
        $hsl = '';
        $strpotong = trim($strpotong);
        $len = strlen($strpotong);
        $str = 0;
        while ($str < $len) {
            $hsl[] = substr($strpotong, $str, $panjang);
            $str += $panjang;
        }
        return $hsl;
    }

    function formatNPWP($npwp) {
        $strlen = strlen($npwp);
        if ($strlen == 15) {
            $npwpnya = substr($npwp, 0, 2) . "." . substr($npwp, 2, 3) . "." . substr($npwp, 5, 3) . "." . substr($npwp, 8, 1) . "-" . substr($npwp, 9, 3) . "." . substr($npwp, 12, 3);
        } else if ($strlen == 12) {
            $npwpnya = substr($npwp, 0, 2) . "." . substr($npwp, 2, 3) . "." . substr($npwp, 5, 3) . "." . substr($npwp, 8, 1) . "-" . substr($npwp, 9, 3);
        } else {
            $npwpnya = $npwp;
        }
        return $npwpnya;
    }

    function setnocont($nocont) {
        return substr($nocont, 0, 4) . '-' . substr($nocont, 4, 11);
    }

    function formaths($hs) {
        $formaths = substr($hs, 0, 4) . '.' . substr($hs, 4, 2) . '.' . substr($hs, 6, 4);
        return $formaths;
    }

    function strip($strstrip) {
        if (trim($strstrip) != 0) {
            $hasile = $strstrip . '%';
        } else {
            $hasile = ' - ';
        }
        return $hasile;
    }

    function showDok($jnDok, $arrDok) {//'380|365|861'
        $hasil = array();
        foreach ($arrDok as $a) {
            if (strstr($jnDok, $a['DOKKD'])) {
                $hasil['NO'][] = $a['DOKNO'];
                $hasil['TG'][] = $a['DOKTG'];
            }
        }
        return $hasil;
    }

    function formatDokArr($arr) {
        $hasil = '';
        foreach ($arr as $a) {
            $hasil[$a['DOKKD']]['NO'][] = $a['DOKNO'];
            $hasil[$a['DOKKD']]['TG'][] = $a['DOKTG'];
        }
        return $hasil;
    }

    function getfas($kodefas) {
        switch ($kodefas) {
            case 1:
                $kdfaspbm_nama = 'DTP';
                break;
            case 2:
                $kdfaspbm_nama = 'DTG';
                break;
            case 3:
                $kdfaspbm_nama = 'BKL';
                break;
            case 4:
                $kdfaspbm_nama = 'BBS';
                break;
        }
        return $kdfaspbm_nama;
    }

    function terbilang($bilangan) {
        $angka = array('0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
        $kata = array('', 'satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 'tujuh', 'delapan', 'sembilan');
        $tingkat = array('', 'ribu', 'juta', 'milyar', 'triliun');
        $panjang_bilangan = strlen($bilangan);

        if ($panjang_bilangan > 15) {
            $kalimat = "Diluar Batas";
            return $kalimat;
        }

        /* mengambil angka-angka yang ada dalam bilangan, dimasukkan ke dalam array */
        for ($i = 1; $i <= $panjang_bilangan; $i++) { 
            $angka[$i] = substr($bilangan, -($i), 1);
        }

        $i = 1;
        $j = 0;
        $kalimat = "";

        /* mulai proses iterasi terhadap array angka */
        while ($i <= $panjang_bilangan) {
            $subkalimat = "";
            $kata1 = "";
            $kata2 = "";
            $kata3 = "";

            /* untuk ratusan */
            if ($angka[$i + 2] != "0") {
                if ($angka[$i + 2] == "1") {
                    $kata1 = "seratus";
                } else {
                    $kata1 = $kata[$angka[$i + 2]] . " ratus";
                }
            }

            /* untuk puluhan atau belasan */
            if ($angka[$i + 1] != "0") {
                if ($angka[$i + 1] == "1") {
                    if ($angka[$i] == "0") {
                        $kata2 = "sepuluh";
                    } elseif ($angka[$i] == "1") {
                        $kata2 = "sebelas";
                    } else {
                        $kata2 = $kata[$angka[$i]] . " belas";
                    }
                } else {
                    $kata2 = $kata[$angka[$i + 1]] . " puluh";
                }
            }

            /* untuk satuan */
            if ($angka[$i] != "0") {
                if ($angka[$i + 1] != "1") {
                    $kata3 = $kata[$angka[$i]];
                }
            }

            /* pengujian angka apakah tidak nol semua, lalu ditambahkan tingkat */
            if (($angka[$i] != "0") OR ( $angka[$i + 1] != "0") OR ( $angka[$i + 2] != "0")) {
                $subkalimat = "$kata1 $kata2 $kata3 " . $tingkat[$j] . " ";
            }

            /* gabungkan variabel sub kalimat (untuk satu blok 3 angka) ke variabel kalimat */
            $kalimat = $subkalimat . $kalimat;
            $i = $i + 3;
            $j = $j + 1;
        }
        /* mengganti satu ribu jadi seribu jika diperlukan */
        if (($angka[5] == "0") AND ( $angka[6] == "0")) {
            $kalimat = str_replace("satu ribu", "seribu", $kalimat);
        }
        return trim($kalimat);
    }

    function formatTglMysql($tglMysql){
        $arr = explode('-', $tglMysql);
        return $arr[2].' '.$this->bulan[(int)$arr[1]].' '.$arr[0];
    }
    
    function formatCar($CAR){
        //$arr = explode('-', $tglMysql);
        return substr($CAR, 0,6).'-'.substr($CAR, 6,6).'-'.substr($CAR, 12,8).'-'.substr($CAR, 20,6);
    }
    
    
    function fixLen($str, $len, $chr = ' ', $alg = STR_PAD_RIGHT) {
        $hasil = str_pad(substr($str, 0, $len), $len, $chr, $alg);
        return $hasil;
    }
    
    /* ------------------ fungsi pendukung --------------------- */
   /* 
    private function isiRespon(){
        
        $this->rpt['LBLKOP']        = $this->ktr['KOP'];
        $this->rpt['LBLNODAFTAR']   = $this->res['CAR'];
        $this->rpt['LABEL33']       = $this->res['PIBTG'];
        $this->rpt['LBLTGLDAFTAR']  = substr($this->res['CAR'],18,2).'-'.substr($this->res['CAR'],16,2).'-'.substr($this->res['CAR'],12,4);
        $this->rpt['LBLDESKRIPSI']  = $this->res['DESKRIPSI'];
        
        $this->rpt['IMPNPWP']       = $this->formatNPWP($this->hdr['IMPNPWP']);
        $this->rpt['IMPNAMA']       = $this->hdr['IMPNAMA'];
        $this->rpt['IMPALMT']       = $this->hdr['IMPALMT']."\n".$this->hdr['INDALMT'];
        
        $this->rpt['PPJKNPWP']      = $this->formatNPWP($this->hdr['PPJKNPWP']);
        $this->rpt['PPJKNAMA']      = $this->hdr['PPJKNAMA'];
        $this->rpt['PPJKALMT']      = $this->hdr['PPJKALMT'];
        $this->rpt['PPJKNP']        = $this->hdr['PPJKNO'];
        $this->rpt['LBL_13']        = $this->hdr['PIBNO'];
        switch($this->res['RESKD']){
            case'011':case'110':
                if($this->res['RESKD']=='110'){
                    $this->rpt['LBLJUDUL']= 'NOTA PEMBERITAHUAN PENOLAKAN (NPP)' ;
                }
                else{
                    $this->rpt['LBLJUDUL']= 'NOTA PEMBERITAHUAN PENOLAKAN (NPP) - NSW';
                }
                $this->rpt['LABEL28'] = '';
                $this->rpt['LABEL29'] = '';
                $this->rpt['LABEL30'] = '';
                $this->rpt['LABEL31'] = '';
                break;
            case'100':case'010': //Penerimaan PIB
                if($this->res['RESKD']==='100'){
                    $this->rpt['LBLJUDUL']= 'PENERIMAAN PIB' ;
                }
                else{
                    $this->rpt['LBLJUDUL']= 'PENERIMAAN PIB - NSW';
                }
                $this->rpt['LBL_11'] = 'Diterima pada    : ';
                $this->rpt['LBL_13'] = $this->res['RESTG'].' '.$this->res['RESWK'];
                $this->rpt['LBLNARASI'] = 'Catatan      : ';
                break;
            case'200'://Penerimaan PIB 
                $this->rpt['LBLJUDUL']= 'KONFIRMASI JAMINAN' ;
                $this->rpt['LBLNARASI']= 'Sehubungan dengan belum adanya surat tanda jaminan atas PIB Saudara Nomor Pengajuan seperti di atas, dengan ini Saudara diminta agar mengirimkan / menyampaikan dokumen bukti jaminan kepada Pejabat Pengelola Jaminan' ;
                $this->rpt['LBL_11']= 'Waktu Konfirmasi  : ' ;
                $this->rpt['LBL_13'] = $this->res['RESTG'].' '.$this->res['RESWK'];
                break;
            case '205':
                $this->rpt['LBLJUDUL']= 'KONFIRMASI SURAT TANDA TERIMA JAMINAN' ;
                $this->rpt['LBLNARASI']= 'Sehubungan dengan belum adanya surat tanda terima jaminan atas PIB Saudara Nomor Pengajuan seperti di atas, dengan ini Saudara diminta agar mengirimkan / menyampaikan dokumen STTJ kepada Pejabat Pengelola Jaminan.' ;
                $this->rpt['LBL_11']= 'Waktu Konfirmasi  : ' ;
                $this->rpt['LBL_13'] = $this->res['RESTG'].' '.$this->res['RESWK'];
                break;
            case '210':
                $this->rpt['LBLJUDUL']= 'KONFIRMASI PEMBAYARAN' ;
                $this->rpt['LBLNARASI']= 'Sehubungan dengan belum adanya Surat Tanda Pembayaran atas PIB Saudara Nomor Pengajuan seperti di atas, dengan ini Saudara diminta untuk :' ;
                $this->rpt['LBLDESKRIPSI']= $this->res['DESKRIPSI'] ;
                
                $this->rpt['LBL_11']= 'Waktu Konfirmasi  : ' ;
                $this->rpt['LBL_13'] = $this->res['RESTG'].' '.$this->res['RESWK'];
                break;
            case'023':case'230':case'600':
                switch ($this->res['RESKD']){
                    case'230':$this->rpt['LBLJUDUL']= 'NOTA PEMBERITAHUAN BARANG LARANGAN/PEMBATASAN (NPBL)' ;break;
                    case'023':$this->rpt['LBLJUDUL']= 'NOTA PEMBERITAHUAN BARANG LARANGAN/PEMBATASAN (NPBL) - NSW';break;
                    case'600':$this->rpt['LBLJUDUL']= 'NOTA PEMBERITAHUAN' ;break;    
                }
                $this->rpt['LBLNARASI'] = 'Dalam PIB yang Saudara sampaikan terdapat barang yang terkena ketentuan larangan / pembatasan. Untuk itu  diminta  menyerahkan  persetujuan dari  instansi terkait dalam waktu 3 (tiga) hari sejak tanggal Nota Pemberitahuan ini.' ;
                $this->rpt['LABEL2']    = 'Pejabat Peneliti Barang Larangan/Pembatasan';
                $this->rpt['LBL_11']    = 'Nomor Pendaftaran';
                $this->rpt['LABEL33']   = date('d-m-Y', strtotime($this->hdr['PIBTG'])) ;
                break;
            case '240':
                $this->rpt['LBLJUDUL']  = 'KONFIRMASI BC 1.1' ;
                $this->rpt['LBLNARASI'] = "Sehubungan dengan belum adanya dokumen BC 1.1 atas PIB Saudara Nomor Pengajuan seperti di atas, dengan ini Saudara diminta agar mengirimkan / menyampaikan dokumen BC 1.1 kepada Pejabat: \n"
                        . " NIP         : " . $this->res['NIP1']."\n"
                        . " Nama        : " . $this->res['PEJABAT1'];
                $this->rpt['LBLDESKRIPSI'] = "Catatan : \n" . $this->res['DESKRIPSI'];
                $this->rpt['LBL_11']= 'Waktu Konfirmasi  : ' ;
                $this->rpt['LBL_13'] = $this->res['RESTG'].' '.$this->res['RESWK'];
                break;
            case '250':
                $this->rpt['LBLJUDUL']  = 'PEMBERITAHUAN PENERIMAAN PIB PENYELESAIAN' ;
                $this->rpt['LBL_11']    = 'Nomor Pendaftaran : ' ;
                $this->rpt['LABEL33']   = date('d-m-Y', strtotime($this->hdr['PIBTG'])) ;
                break;
            case '900':
                $arrJDL = explode("\n", $this->res['DESKRIPSI']);
                $this->rpt['LBLJUDUL']  = $arrJDL[0] ." \n( RESPON UMUM )";
                
                break;
        }
        $this->rpt['LBLKOTATTD']  = $this->ktr['KOTA'].', '.$this->formatTglMysql($this->res['RESTG']);
        $this->rpt['LBLNMTTD']  = $this->res['PEJABAT1'];
        $this->rpt['LBLNIPTTD']  = $this->res['NIP1'];
        
    }
    
    function drawResponNPP(){
        $this->pdf->SetMargins(5, 5, 5, 5);
        $this->pdf->SetAutoPageBreak(true, 5);
        $this->pdf->setY(5);
        $this->pdf->SetFont('courier', '', '11');
        $this->pdf->multicell(0, 5, $this->rpt['LBLKOP'], 0, 'L');
        $this->pdf->Ln(10);
        $this->pdf->SetFont('courier', 'BU', '13');
        $this->pdf->multicell(0, 6,  str_replace('  ', '', $this->rpt['LBLJUDUL']), 0, 'C');
        $this->pdf->Ln(8);
        $this->pdf->SetFont('courier', '', '10');
        $this->pdf->cell(0, 4.5, $this->rpt['LBLNOTGL'], 0, 0, 'C', 0);
        $this->pdf->Ln(6);
        $this->pdf->cell(45, 4.5, 'Nomor Pengajuan  : ', 0, 0, 'L', 0);
        $this->pdf->cell(85, 4.5, $this->formatCar($this->rpt['LBLNODAFTAR']), 0, 0, 'L', 0);
        $this->pdf->cell(24, 4.5, 'Tanggal : ', 0, 0, 'L', 0);
        $this->pdf->cell(46, 4.5, $this->rpt['LBLTGLDAFTAR'], 0, 1, 'L', 0);
        $this->pdf->cell(45, 4.5, $this->rpt['LBL_11'], 0, 0, 'L', 0);
        $this->pdf->cell(85, 4.5, $this->rpt['LBL_13'], 0, 0, 'L', 0);
        if($this->rpt['LABEL33']!=''){
            $this->pdf->cell(24, 4.5, 'Tanggal : ', 0, 0, 'L', 0);
            $this->pdf->cell(46, 4.5, $this->rpt['LABEL33'], 0, 0, 'L', 0);
        }
        $this->pdf->Ln(10);
        $this->pdf->cell(0, 6,'Kepada       :', 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'IMPORTIR' , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NPWP    : ' . $this->rpt['IMPNPWP'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Nama    : ' . $this->rpt['IMPNAMA'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Alamat  : ' . $this->rpt['IMPALMT'], 0, 1, 'L', 0);
        $this->pdf->Ln(2);
        $this->pdf->cell(0, 4.5, 'PPJK' , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NPWP    : ' . $this->rpt['PPJKNPWP'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Nama    : ' . $this->rpt['PPJKNAMA'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Alamat  : ' . $this->rpt['PPJKALMT'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NP PPJK : ' .$this->rpt['PPJKNP'], 0, 1, 'L', 0);
        $this->pdf->multicell(0, 6,$this->rpt['LBLNARASI'], 0, 'J');
        $this->pdf->Ln(2);
        $this->pdf->multicell(0, 6, str_replace("\n\n", "\n", $this->rpt['LBLDESKRIPSI']), 0, 'J');
        
        #---------------------footer----------------------
        $this->pdf->Ln();
        $this->pdf->cell(70, 5, $this->rpt['LBLKOTATTD'], 0, 0, 'L', 0);
        //$this->pdf->cell(5,  5, ',', 0, 0, 'C', 0);
        //$this->pdf->cell(40, 5, $this->rpt['LBLTGLTTD'], 1, 0, 'C', 0);
        $this->pdf->Ln(15);
        $this->pdf->cell(70, 5, $this->rpt['LABEL2'], 0, 0, 'L', 0);
        $this->pdf->Ln(10);
        $this->pdf->cell(35, 5, 'Tanda tangan', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5,  ':', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(35, 5, 'Nama', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5,  ':', 0, 0, 'L', 0);
        $this->pdf->cell(0, 5, $this->rpt['LBLNMTTD'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(35, 5, 'Nip', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5,  ':', 0, 0, 'L', 0);
        $this->pdf->cell(0, 5,  $this->rpt['LBLNIPTTD'], 0, 0, 'L', 0);
        $this->footerNPP();
    }
        
    function footerNPP(){
        $this->pdf->SetY(-25);
        $this->pdf->SetFont('courier','',10);
        $this->pdf->cell(0, 5, $this->rpt['LABEL28'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 5, $this->rpt['LABEL29'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 5, $this->rpt['LABEL30'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 5, $this->rpt['LABEL31'], 0, 0, 'L', 0);
    }
    
    function footerSPPB(){
        $this->pdf->SetY(-30);
        $this->pdf->line(5, $this->pdf->getY(), 205, $this->pdf->getY());
        $this->pdf->SetFont('courier','',10);
        $this->pdf->cell(0, 5,'Peruntukan :', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 5,'1. Importir.', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 5,'2. Pejabat yang mengawasi pengeluaran barang.', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->multicell(0, 5, 'Formulir ini dicetak secara otomatis oleh sistem komputer dan tidak memerlukan nama, tanda tangan pejabat, dan cap dinas.', 0, 'J');
    }

    function drawResponSPPB() {
        $this->pdf->SetMargins(5, 5, 5, 5);
        $this->pdf->SetAutoPageBreak(0, 10);
        $this->pdf->setY(5);
        $this->pdf->SetFont('courier', '', '11');
        $this->pdf->multicell(0, 5, $this->ktr['KOP'], 0, 'L');
        $this->pdf->Ln(10);
        $this->pdf->SetFont('courier', 'BU', '13');
        $this->pdf->cell(0, 5, 'SURAT PERSETUJUAN PENGELUARAN BARANG (SPPB)', 0, 0, 'C', 0);
        
        $this->pdf->Ln();
        $this->pdf->SetFont('courier', '', '10');
        $this->pdf->multicell(0, 4.5, 'Nomor : ' . $this->res['DOKRESNO'] . '   Tanggal : '. date('d-m-Y',strtotime($this->res['DOKRESTG'])) , 0, 'C');
        $this->pdf->Ln(8);
        $this->pdf->cell(180, 4.5, 'Nomor Pengajuan           : ' . $this->formatCar($this->hdr['CAR']), 0, 1, 'L', 0);
        $this->pdf->cell(180, 4.5, 'Nomor Pendaftaran PIB     : ' . $this->hdr['PIBNO'] . '   Tanggal : ' . date('d-m-Y',strtotime($this->hdr['PIBTG'])) , 0, 0, 'L', 0);
        $this->pdf->Ln(7);
        $this->pdf->cell(0, 4.5,'Kepada       :', 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'IMPORTIR' , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NPWP    : ' . $this->hdr['IMPNPWP'] , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Nama    : ' . $this->hdr['IMPNAMA'] , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Alamat  : ' . $this->hdr['IMPALMT'] , 0, 1, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 4.5, 'PPJK' , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NPWP    : ' . $this->hdr['PPJKNPWP'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Nama    : ' . $this->hdr['PPJKNAMA'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Alamat  : ' . $this->hdr['PPJKALMT'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NP PPJK : ' . $this->hdr['PPJKNO'], 0, 1, 'L', 0);
        $this->pdf->Ln(2);
        $this->pdf->cell(0, 4.5, 'Lokasi Barang             : ' . $this->hdr['TMPTBN'] . ' - ' . $this->main->get_uraian("SELECT URAIAN FROM m_gudang WHERE KDKPBC = '".$this->hdr['KDKPBC']."' AND KDGDG = '".$this->hdr['TMPTBN']."'",'URAIAN') , 0, 1, 'L', 0);
    if($this->hdr['MODA']=='1'){
        $dok = explode('|', $this->main->get_uraian("SELECT CONCAT(DOKNO,'|',DOKTG) AS UR FROM t_bc20dok WHERE CAR = '".$this->hdr['CAR']."' and KODE_TRADER = '".$this->hdr['KODE_TRADER']."' and DOKKD in ('705','704')",'UR'));
        $this->pdf->cell(0, 4.5, 'No. dan Tanggal B/L       : ' . $this->fixLen($dok[0],30) .'  Tanggal : '.date('d-m-Y',strtotime($dok[1])) , 0, 1, 'L', 0);
    }
    elseif($this->hdr['MODA']=='4'){
        $dok = explode('|', $this->main->get_uraian("SELECT CONCAT(DOKNO,'|',DOKTG) AS UR FROM t_bc20dok WHERE CAR = '".$this->hdr['CAR']."' and KODE_TRADER = '".$this->hdr['KODE_TRADER']."' and DOKKD in ('741','740')",'UR'));
        $this->pdf->cell(0, 4.5, 'No. dan Tanggal AWB       : ' . $this->fixLen($dok[0],30) .'  Tanggal : '.date('d-m-Y',strtotime($dok[1])) , 0, 1, 'L', 0);
    }
        $this->pdf->cell(0, 4.5, 'Sarana Pengangkut         : ' . $this->hdr['ANGKUTNAMA'] . '    ' . $this->hdr['ANGKUTNO'] , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'No Voy./Flight            : ' . $this->hdr['ANGKUTNO'] , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'No/Tgl BC 1.1             : ' . $this->fixLen($this->hdr['DOKTUPNO'],6) . '    Tanggal : ' . $this->fixLen(date('d-m-Y', strtotime($this->hdr['doktuptg'])),10) . '  Pos     : ' . $this->fixLen($this->hdr['POSNO'],4) . ' ' . $this->fixLen($this->hdr['POSSUB'],4) . ' ' . $this->fixLen($this->hdr['POSSUBSUB'],4) . ' ' , 0, 1, 'L', 0);
        
        $kms = $this->main->get_uraian("SELECT group_concat(concat(a.JMKEMAS,' ',a.JNKEMAS) SEPARATOR ';') as UR FROM t_bc20kms a WHERE a.KODE_TRADER = '".$this->hdr['KODE_TRADER']."' AND a.CAR = '".$this->hdr['CAR']."'",'UR');
        $this->pdf->cell(0, 4.5, 'Jumlah / Jenis Kemasan    : ' . $this->fixLen($kms, 31) . ' Berat   : ' . number_format($this->hdr['BRUTO'], 2, '.', ',') . ' KGM' , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'Merk Kemasan              : ' , 0, 1, 'L', 0);
        $this->pdf->Ln();
        $jmlConr = count($this->conr);
        $this->pdf->cell(0, 4.5, 'Jumlah Peti Kemas         : ' . $jmlConr , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'Nomor Peti Kemas / Ukuran : ' , 0, 1, 'L', 0);
        $arr = array(1=>array('CAPTION'=>'No.','WEIGHT'=>'10','FIELD'=>''),
                     2=>array('CAPTION'=>'No.Peti Kemas','WEIGHT'=>'30','FIELD'=>'CONTNO'),
                     3=>array('CAPTION'=>'Ukuran','WEIGHT'=>'20','FIELD'=>'CONTUKUR'),
                     4=>array('CAPTION'=>'Penegahan','WEIGHT'=>'25','FIELD'=>'CONTTIPE'),
                     5=>array('CAPTION'=>'Ket','WEIGHT'=>'10','FIELD'=>''));
        
        foreach($arr as $a){
            $this->pdf->cell($a['WEIGHT'], 5, $a['CAPTION'] , 1, 0, 'L', 0);
        }
        if($jmlConr>1){
            $this->pdf->setX($this->pdf->getX()+5);
            foreach($arr as $a){
                $this->pdf->cell($a['WEIGHT'], 5, $a['CAPTION'] , 1, 0, 'L', 0);
            }
        }
        $this->pdf->Ln();
        $akr = ($jmlConr > 14)?14:$jmlConr;
        for($i=1;$i<$akr;$i++){
            foreach($arr as $a){
                if($a['CAPTION']=='No.'){
                    $this->pdf->cell($a['WEIGHT'], 5, $i , 1, 0, 'L', 0);
                }else{
                    $this->pdf->cell($a['WEIGHT'], 5, $this->conr[$i-1][$a['FIELD']] , 1, 0, 'L', 0);
                }
            }
            if($i % 2 == 1){
                $this->pdf->setX($this->pdf->getX()+5);
            }else{
                $this->pdf->Ln();
            }
        }
        $this->pdf->Ln(2);
        $this->pdf->cell(30, 4.5, 'Catatan     : ' , 0, 0, 'L', 0);
        $this->pdf->multicell(0, 4.5, $this->res['DESKRIPSI'] , 0, 'L');
        $this->pdf->Ln(2);
        $this->pdf->cell(100, 4.5, $this->ktr['KOTA'].' , ' . date('d-m-Y',strtotime($this->res['RESTG'])) , 0, 0, 'L', 0);
        $this->pdf->cell(100, 4.5, $this->ktr['KOTA'].' , ' . date('d-m-Y',strtotime($this->res['RESTG'])), 0, 1, 'L', 0);
        $this->pdf->cell(100, 4.5, 'Pejabat Pemeriksa Dokumen', 0, 0, 'L', 0);
        $this->pdf->cell(100, 4.5, 'Pejabat yang mengawasi pengeluaran barang', 0, 0, 'L', 0);
        $this->pdf->Ln(15);
        $this->pdf->cell(100, 4.5, 'Tanda Tangan  :', 0, 0, 'L', 0);
        $this->pdf->cell(100, 4.5, 'Tanda Tangan  :', 0, 1, 'L', 0);
        $this->pdf->cell(100, 4.5, 'Nama          : ' . $this->res['PEJABAT1'], 0, 0, 'L', 0);
        $this->pdf->cell(100, 4.5, 'Nama          : ' . $this->res['NIP1'], 0, 1, 'L', 0);
        $this->pdf->cell(100, 4.5, 'NIP           : ' . $this->res['PEJABAT2'], 0, 0, 'L', 0);
        $this->pdf->cell(100, 4.5, 'NIP           : ' . $this->res['NIP2'], 0, 0, 'L', 0);
        
        $this->pdf->SetY(-30);
        $this->pdf->line(5, $this->pdf->getY(), 205, $this->pdf->getY());
        $this->pdf->SetFont('courier','',10);
        $this->pdf->cell(0, 4.5,'Peruntukan :', 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5,'1. Importir.', 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5,'2. Pejabat yang mengawasi pengeluaran barang.', 0, 1, 'L', 0);
        $this->pdf->multicell(0, 4.5, 'Formulir ini dicetak secara otomatis oleh sistem komputer dan tidak memerlukan nama, tanda tangan pejabat, dan cap dinas.', 0, 'J');
        if($jmlConr>14){
            while($i<=$jmlConr){
                $this->pdf->addPage();
                $this->pdf->SetFont('courier', '', '11');
                $this->pdf->multicell(0, 5, $this->ktr['KOP'], 0, 'L');
                $this->pdf->Ln(10);
                $this->pdf->SetFont('courier', 'BU', '13');
                $this->pdf->cell(0, 5, 'SURAT PERSETUJUAN PENGELUARAN BARANG (SPPB)', 0, 0, 'C', 0);
                $this->pdf->Ln();
                $this->pdf->SetFont('courier', '', '10');
                $this->pdf->multicell(0, 4.5, 'Nomor : ' . $this->res['DOKRESNO'] . '   Tanggal : '. date('d-m-Y',strtotime($this->res['DOKRESTG'])) , 0, 'C');
                $this->pdf->Ln(8);
                $this->pdf->cell(180, 4.5, 'Nomor Pengajuan           : ' . $this->formatCar($this->hdr['CAR']), 0, 1, 'L', 0);
                $this->pdf->cell(180, 4.5, 'Nomor Pendaftaran PIB     : ' . $this->hdr['PIBNO'] . '   Tanggal : ' . date('d-m-Y',strtotime($this->hdr['PIBTG'])) , 0, 1, 'L', 0);
                
                $awl = $i;
                $akr = ($jmlConr > $awl+79)?$awl+79: $jmlConr;
                foreach($arr as $a){
                    $this->pdf->cell($a['WEIGHT'], 5, $a['CAPTION'] , 1, 0, 'L', 0);
                }
                if(($akr - $awl)>0){
                    $this->pdf->setX($this->pdf->getX()+5);
                    foreach($arr as $a){
                        $this->pdf->cell($a['WEIGHT'], 5, $a['CAPTION'] , 1, 0, 'L', 0);
                    }
                }
                $this->pdf->Ln();
                for ($i = $awl; $i <= $akr; $i++) {
                    foreach ($arr as $a) {
                        if ($a['CAPTION'] == 'No.') {
                            $this->pdf->cell($a['WEIGHT'], 5, $i, 1, 0, 'L', 0);
                        } else {
                            $this->pdf->cell($a['WEIGHT'], 5, $this->conr[$i - 1][$a['FIELD']], 1, 0, 'L', 0);
                        }
                    }
                    if ($i % 2 == 1) {
                        $this->pdf->setX($this->pdf->getX() + 5);
                    } else {
                        $this->pdf->Ln();
                    }
                }
                
                $this->pdf->SetY(-30);
                $this->pdf->line(5, $this->pdf->getY(), 205, $this->pdf->getY());
                $this->pdf->SetFont('courier','',10);
                $this->pdf->cell(0, 4.5,'Peruntukan :', 0, 1, 'L', 0);
                $this->pdf->cell(0, 4.5,'1. Importir.', 0, 1, 'L', 0);
                $this->pdf->cell(0, 4.5,'2. Pejabat yang mengawasi pengeluaran barang.', 0, 1, 'L', 0);
                $this->pdf->multicell(0, 4.5, 'Formulir ini dicetak secara otomatis oleh sistem komputer dan tidak memerlukan nama, tanda tangan pejabat, dan cap dinas.', 0, 'J');
            }
        }
        
        
        
        
        
        //$this->footerSPPB();
    }
    
    function drawResponINP(){
        $arr = explode("\n", $this->res['DESKRIPSI']);
        $this->pdf->SetMargins(7, 5, 5);
        $this->pdf->SetAutoPageBreak(2, 10);
        $this->pdf->setY(5);
        $this->pdf->SetFont('courier', '', '11');
        $this->pdf->multicell(0, 5, $this->ktr['KOP'], 0, 'L');
        $this->pdf->Ln(4);
        $this->pdf->SetFont('courier', 'BU', '13');
        $this->pdf->cell(0, 5, 'INFORMASI NILAI PABEAN', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('courier', '', '10');
        $this->pdf->multicell(0, 5, ($this->res['DOKRESNO']=='')?'':'Nomor : '.$this->res['DOKRESNO'] , 0, 'C');
        $this->pdf->Ln(3);
        $this->pdf->cell(0, 6,'Kepada Yth.', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 6, 'PEMBELI' , 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(33, 4.5, '     NPWP    : ' , 0, 0, 'L', 0);$this->pdf->cell(0, 5, $this->hdr['IMPNPWP'] , 0, 1, 'L', 0);
        $this->pdf->cell(33, 4.5, '     Nama    : ' , 0, 0, 'L', 0);$this->pdf->cell(0, 5, $this->hdr['IMPNAMA'] , 0, 1, 'L', 0);
        $this->pdf->cell(33, 4.5, '     Alamat  : ' , 0, 0, 'L', 0);$this->pdf->multicell(0, 5, $this->hdr['IMPALMT'], 0, 'L');
        $this->pdf->cell(0, 4.5, 'PEMBERITAHU' , 0, 1, 'L', 0);
        $this->pdf->cell(33, 4.5, '     NPWP    : ' , 0, 0, 'L', 0);$this->pdf->cell(0, 5, $this->hdr['PPJKNPWP'] , 0, 1, 'L', 0);
        $this->pdf->cell(33, 4.5, '     Nama    : ' , 0, 0, 'L', 0);$this->pdf->cell(0, 5, $this->hdr['PPJKNAMA'] , 0, 1, 'L', 0);
        $this->pdf->cell(33, 4.5, '     Alamat  : ' , 0, 0, 'L', 0);$this->pdf->cell(0, 5, $this->hdr['PPJKALMT'] , 0, 1, 'L', 0);
        $this->pdf->cell(33, 4.5, '     NP PPJK : ' , 0, 0, 'L', 0);$this->pdf->cell(0, 5, $this->hdr['PPJKNO'] , 0, 1, 'L', 0);
        $this->pdf->Ln(2);
        $this->pdf->cell(0, 4.5, 'PIB ' , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '    Nomor Pengajuan    : ' . $this->formatCar($this->hdr['CAR']) , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '    Nomor Pendaftaran  : ' . $this->hdr['PIBNO'] , 0, 1, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->multicell(0, 4.5, "   Berhubung nilai pabean yang Saudara beritahukan di dalam pemberitahuan pabean impor diragukan kebenarannya, yaitu untuk jenis barang nomor urut ".$arr[0]." pada pemberitahuan pabean impor No: ".$this->res['PIBNO'] .' Tanggal : ' . date('d-m-Y', strtotime($this->res['PIBTG'])) . ", maka dengan ini diminta Saudara untuk menyerahkan Deklarasi Nilai Pabean (DNP) yang merupakan deklarasi atas fakta transaksi jual-beli/importasi yang berkaitan dengan barang yang Saudara impor disertai dengan dokumen-dokumen pendukung yang menguatkan deklarasi yang diajukan, misalnya:

a. Kontrak Penjualan (Sales Contract);
b. Kontrak (Agreement) lainnya, misalnya Royalty Agreement, Kontrak Penunjukan
   Keagenan untuk Pembelian Barang, Kontrak Penunjukan sebagai Agen/Distributor;
c. Purchase Order;
d. Letter of Credit (L/C);
e. Bukti pelunasan atas pembelian barang impor (bukti transfer uang);
f. Rekening Koran yang berkaitan dengan transaksi tersebut;
g. Bukti lain yang menyatakan kewajiban importir kepada penjual yang belum
   dipenuhi terkait dengan transaksi impor;
h. Bukti pembayaran atas barang yang sama pada penjual yang sama untuk transaksi
   sebelumnya;
i. Dokumen/bukti negosiasi terbentuknya harga;
j. Dokumen lainnya yang terkait dengan transaksi tersebut.

    DNP dan dokumen-dokumen pendukung tersebut harus diserahkan paling lambat dalam waktu 3 (tiga) hari kerja setelah tanggal pengiriman Informasi Nilai Pabean (INP) kepada Pejabat Bea dan Cukai:" , 0, 'J');
        $this->pdf->cell(0, 4.5, '            Nama                  : ' . $this->res['PEJABAT1'] , 0, 2, 'L', 0);
        $this->pdf->cell(0, 4.5, '            Nip                   : ' . $this->res['NIP1'] , 0, 2, 'L', 0);
        $this->pdf->cell(0, 4.5, '            Jabatan               : ' . $this->res['JABATAN1'] , 0, 2, 'L', 0);
        $this->pdf->cell(0, 4.5, '            Telepon dan Faxsimile : ' . $arr[1] . ' / ' . $arr[2] , 0, 2, 'L', 0);
        
        $this->pdf->multicell(0, 4.5, "   Apabila DNP dan dokumen pendukungnya tidak diserahkan dalam jangka waktu tersebut di atas, nilai pabean ditentukan berdasarkan nilai transaksi barang identik sampai dengan metode pengulangan sesuai hierarki penggunaannya." , 0, 'J');
        $this->pdf->setX($this->pdf->getX()+100);
        $this->pdf->cell(0, 4.5, $this->ktr['KOTA'] . ' , ' . $this->formatTglMysql($this->res['RESTG']) , 0, 2, 'C', 0);
        $this->pdf->cell(0, 4.5, 'Pejabat yang memeriksa dokumen' , 0, 2, 'C', 0);
        $this->pdf->ln(12);
        $this->pdf->setX($this->pdf->getX()+100);
        $this->pdf->cell(0, 4.5, $this->res['PEJABAT1'] , 0, 2, 'C', 0);
        $this->pdf->cell(0, 4.5, $this->res['NIP1'] , 0, 0, 'C', 0);
    }
    
    function drawResponSPTNP() {
        $this->pdf->SetMargins(5, 5, 5);
        $this->pdf->SetAutoPageBreak(0, 0);
        $this->pdf->setY(5);
        
        $this->pdf->SetFont('courier', '', '10');
        $this->pdf->cell(0, 5, 'KEMENTERIAN KEUANGAN REPUBLIK INDONESIA', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 5, 'DIREKTORAT JENDERAL BEA DAN CUKAI', 0, 0, 'L', 0);
        $this->pdf->Ln(15);
        $this->pdf->SetFont('courier', 'UB', '13');
        $this->pdf->cell(0, 5, 'SURAT PENETAPAN  TARIF DAN/ ATAU NILAI PABEAN (SPTNP)', 0, 0, 'C', 0);
        $this->pdf->Ln(5);
        $this->pdf->SetFont('courier', '', '11');
        $this->pdf->setX(48);
        $this->pdf->cell(0, 5, 'Nomor      :'.$this->res['DOKRESNO'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->setX(48);
        $this->pdf->cell(0, 5, 'Tanggal    :'.$this->res['DOKRESTG'], 0, 0, 'L', 0);
        
        $this->pdf->Ln(8);
        $this->pdf->SetFont('courier', '', '10');
        $this->pdf->cell(40, 5, 'Kepada Yth.', 0, 0, 'L', 0);
        $this->pdf->Ln(5);
        $this->pdf->cell(0, 5, 'Nama               : '.$this->hdr['IMPNAMA'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 5, 'Alamat             : '.$this->hdr['IMPALMT'], 0, 0, 'L', 0);
        $this->pdf->Ln(6);
        $this->pdf->cell(0, 5, 'Dengan ini diberitahukan atas Pemberitahuan Pabean Impor (PIB)   :', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(120, 5, 'Nomor Pendaftaran  : '.$this->hdr['PIBNO'], 0, 0, 'L', 0);
        $this->pdf->cell(0, 5, 'Tanggal  : '.$this->res['PIBTG'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(120, 5, 'Importir           : '.$this->hdr['IMPNAMA'], 0, 0, 'L', 0);
        $this->pdf->cell(0, 5, 'NPWP     : '.$this->formatNPWP($this->hdr['IMPNPWP']), 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(120, 5, 'PPJK               : '.$this->hdr['PPJKNAMA'], 0, 0, 'L', 0);
        $this->pdf->cell(0, 5, 'NPWP     : '.$this->formatNPWP($this->hdr['PPJKNPWP']), 0, 0, 'L', 0);
        $this->pdf->Ln(6);
        $this->pdf->multicell(200, 4, 'ditetapkan tarif dan / atau nilai pabean sehingga mengaibatkan kekurangan / kelebihan pembayaran bea masuk, pajak dalam rangka impor, dan/ atau sanksi administrasi berupa denda dengan rincian sebagai berikut :', 0, 'J');
        
        $this->pdf->Ln();
        $this->pdf->cell(40, 5, 'URAIAN', 1, 0, 'C', 0);
        $this->pdf->cell(40, 5, 'DIBERITAHUKAN', 1, 0, 'C', 0);
        $this->pdf->cell(40, 5, 'DITETAPKAN', 1, 0, 'C', 0);
        $this->pdf->cell(40, 5, 'KEKURANGAN', 1, 0, 'C', 0);
        $this->pdf->cell(40, 5, 'KELEBIHAN', 1, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40, 5, '1. Bea Cukai', 'LR', 0, 'L', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['BM_ASAL'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['BMBYR'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['BM_KURANG'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['BM_LEBIH'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40, 5, '2. Cukai', 'LR', 0, 'L', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['CUK_ASAL'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['CUKBYR'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['CUK_KURANG'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['CUK_LEBIH'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40, 5, '3. PPN', 'LR', 0, 'L', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPN_ASAL'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPNBYR'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPN_KURANG'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPN_LEBIH'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40, 5, '4. PPnBM', 'LR', 0, 'L', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPNBM_ASAL'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPNBMBYR'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPNBM_KURANG'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPNBM_LEBIH'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40, 5, '5. PPh Pasal 22', 'LR', 0, 'L', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPH_ASAL'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPHBYR'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPH_KURANG'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPH_LEBIH'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40, 5, '6. Denda', 'LR', 0, 'L', 0);
        $this->pdf->cell(40, 5, '0', 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, '0', 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['DENDA'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, '0', 'LR', 0, 'R', 0);
        $this->pdf->Ln();
        $total = $this->ntp['BM_KURANG'] + $this->ntp['CUK_KURANG'] + $this->ntp['PPN_KURANG'] + $this->ntp['PPNBM_KURANG'] + $this->ntp['PPH_KURANG'] + $this->ntp['DENDA'];
        $totalLbh = $this->ntp['BM_LEBIH'] + $this->ntp['CUK_LEBIH'] + $this->ntp['PPN_LEBIH'] + $this->ntp['PPNBM_LEBIH'] + $this->ntp['PPH_LEBIH'];
        $this->pdf->cell(120, 5, 'JUMLAH KEKURANGAN / KELEBIHAN PEMBAYARAN', 1, 0, 'C', 0);
        $this->pdf->cell(40, 5, number_format($total, 0, '.', ','), 1, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($totalLbh, 0, '.', ','), 1, 0, 'R', 0);
        
        $this->pdf->Ln(6);
        $this->pdf->cell(0, 4, 'Dengan rincian kesalahan sebagai berikut : ', 0, 0, 'L', 0);
        $this->pdf->Ln(6);
        
        $this->pdf->cell(60, 4, 'JENIS KESALAHAN', 1, 0, 'C', 0);
        $this->pdf->cell(0, 4, 'NOMOR URUT BARANG', 1, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(60, 4, '1. Jenis Barang', 'LTR', 0, 'L', 0);$this->pdf->cell(0, 4, $this->ntp['S_JNSBRG'], 'TR', 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(60, 4, '2. Jumlah Barang', 'LR', 0, 'L', 0);$this->pdf->cell(0, 4, $this->ntp['S_JMLBRG'], 'R', 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(60, 4, '3. Tarif', 'LR', 0, 'L', 0);$this->pdf->cell(0, 4, $this->ntp['S_TARIF'], 'R', 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(60, 4, '4. Nilai Pabean', 'LBR', 0, 'L', 0);$this->pdf->cell(0, 4, $this->ntp['S_NILPAB'], 'BR', 0, 'L', 0);
        $this->pdf->Ln(8);
        
        if($total > 0){
            $jthTemp = $this->formatTglMysql($this->res['JATUHTEMPO']);
        }else{
            $jthTemp = ' - ';
        } 
        
        $this->pdf->multicell(200, 4,'     Dalam hal terdapat kekurangan pembayaran, Saudara  wajib melunasi kekurangan pembayaran tersebut paling lambat pada tanggal ' . $jthTemp . ', dan bukti  pelunasan  agar disampaikan kepada Kepala Kantor '. $this->hdr['URKDKPBC'].'.', 0, 'J');
        $this->pdf->Ln();
        $this->pdf->multicell(200, 4, '	    Apabila tagihan tidak dilunasi atau tidak diajukan keberatan smapai dengan tanggal ' . $jthTemp . ', dikenakan bunga sebesar 2% (dua persen) setiap bulan untuk paling lama 24 (dua puluh empat) bulan dari jumlah kekurangan pembayaran, bagian bulan dihitung satu bulan penuh.', 0, 'J');
        $this->pdf->Ln();
        $this->pdf->multicell(200, 4, '     Dalam hal terdapat kelebihan pembayaran, Saudara dapat mengajukan permohonan pengembalian sesuai ketentuan peraturan perundang-undangan.', 0, 'J');
        $this->pdf->Ln();
        $this->pdf->multicell(200, 4, '     Keberatan atas penetapan ini hanya dapat diajukan secara tertulis kepada Direktur Jenderal Bea dan Cukai melalui ' . $this->hdr['URKDKPBC'] . ' sesuai dengan ketentuan tentang keberatan, paling lambat pada tanggal  ' . $jthTemp . ".", 0, 'J');
        $this->pdf->SetFont('courier', '', '11');
        $this->pdf->setXY(100,$this->pdf->getY()+10);
        $this->pdf->cell(100, 5, $this->res['JABATAN1'], 0, 0, 'C', 0);
        $this->pdf->setXY(100,$this->pdf->getY()+20);
        $this->pdf->cell(100, 5, $this->res['PEJABAT1'], 0, 0, 'C', 0);
        $this->pdf->setXY(100,$this->pdf->getY()+5);
        $this->pdf->cell(100, 5, $this->res['NIP1'], 0, 0, 'C', 0);
        $this->pdf->Ln(6);
        $this->pdf->cell(0, 5, 'STNP ini dibuat 3 (tiga) :', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 5, '- Rangkap ke-1 untuk Importir;', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 5, '- Rangkap ke-2 untuk Kepala Kantor;', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 5, '- Rangkap ke-3 untuk arsip Pejabar Bea dan Cukai.', 0, 0, 'L', 0);
    }
    
    function drawResponSPKNP(){
        $arr = explode("\n", $this->res['DESKRIPSI']);
        $this->pdf->SetMargins(7, 5, 5);
        $this->pdf->SetAutoPageBreak(2, 10);
        $this->pdf->setY(5);
        $this->pdf->SetFont('courier', '', '11');
        $this->pdf->multicell(0, 5, $this->ktr['KOP'], 0, 'L');
        $this->pdf->Ln(4);
        $this->pdf->SetFont('courier', 'BU', '13');
        $this->pdf->cell(0, 5, 'SURAT PEMBERITAHUAN KONSULTASI NILAI PABEAN', 0, 1, 'C', 0);
        $this->pdf->SetFont('courier', '', '10');
        $this->pdf->multicell(0, 5, ($this->res['DOKRESNO']=='')?'':'Nomor : '.$this->res['DOKRESNO'] , 0, 'C');
        $this->pdf->Ln(3);
        $this->pdf->cell(0, 6,'Kepada Yth.', 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'Direktur PT  : ' . $this->hdr['IMPNAMA'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'Alamat       : ' . $this->hdr['IMPALMT'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'NPWP         : ' . $this->hdr['IMPNPWP'], 0, 1, 'L', 0);
        $this->pdf->Ln(2);
        $this->pdf->multicell(0, 4.5, '    Sehubungan dengan Deklarasi Nilai Pabean (DNP) atas nilai transaksi yang diberitahukan dalam PIB:', 0, 'J');
        $this->pdf->Ln(4);
        $this->pdf->cell(0, 4.5, 'No. Aju               : ' . $this->hdr['CAR'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'No / Tgl Pendaftaran  : ' . $this->hdr['PIBNO'] . '  Tangga : ' . date('d-m-Y',strtotime($this->hdr['PIBTG'])), 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'Nomor Urut            : ' . $arr[0], 0, 1, 'L', 0);
        $this->pdf->Ln(2);
        $this->pdf->multicell(0, 4.5, '    Saudara diharapkan hadir di '.$this->ktr['URAIAN_KPBC'].' dalam waktu 2 (dua) hari kerja sejak tanggal Surat Pemberitahuan Konsultasi Nilai Pabean untuk melakukan konsultasi tentang nilai transaksi yang Saudara beritahukan dalam pemberitahuan pabean impor di atas dengan membawa data dan/atau informasi tambahan berupa :', 0, 'J');
        $this->pdf->Ln(2);
        $this->pdf->multicell(0, 4.5, str_replace($arr[0] . "\n", '', $this->res['DESKRIPSI']) , 0, 'J');
        $this->pdf->Ln(2);
        $this->pdf->multicell(0, 4.5, '    Apabila Saudara tidak hadir dalam jangka waktu tersebut, nilai pabean ditentukan berdasarkan nilai transaksi barang identik sampai dengan metode pengulangan sesuai hierarki penggunaannya.', 0, 'J');
        
        
        $this->pdf->setXY($this->pdf->getX()+100,-70);
        $this->pdf->cell(0, 4.5, $this->ktr['KOTA'] . ' , ' . $this->formatTglMysql($this->res['RESTG']) , 0, 2, 'C', 0);
        $this->pdf->cell(0, 4.5, 'Pejabat Bea dan Cukai' , 0, 2, 'C', 0);
        $this->pdf->ln(25);
        $this->pdf->setX($this->pdf->getX()+100);
        $this->pdf->cell(0, 4.5, $this->res['PEJABAT1'] , 0, 2, 'C', 0);
        $this->pdf->cell(0, 4.5, $this->res['NIP1'] , 0, 0, 'C', 0);
    }
       
    function drawResponSPJM() {
        $arr = explode("\n", $this->res['DESKRIPSI']."\n");
        $this->pdf->SetMargins(7, 5, 5);
        $this->pdf->SetAutoPageBreak(2, 10);
        $this->pdf->setY(5);
        $this->pdf->SetFont('courier', '', '11');
        $this->pdf->multicell(0, 5, $this->ktr['KOP'], 0, 'L');
        $this->pdf->Ln(4);
        $this->pdf->SetFont('courier', 'BU', '13');
        $this->pdf->cell(0, 5, ($this->res['RESKD']=='400')?'SURAT PEMBERITAHUAN JALUR MERAH (SPJM)':'SURAT PEMBERITAHUAN JALUR KUNING (SPJK)', 0, 1, 'C', 0);
        $this->pdf->SetFont('courier', '', '10');
        $this->pdf->multicell(0, 5, ($this->res['DOKRESNO']=='')?'':'Nomor : ' . $this->res['DOKRESNO'] . '   Tanggal : ' . date('d-m-Y',strtotime($this->res['DOKRESTG'])), 0, 'C');
        $this->pdf->Ln(3);
        $this->pdf->cell(0, 4.5, 'Nomor Pengajuan    : ' . $this->formatCar($this->hdr['CAR']) , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'Nomor Pendaftaran  : ' . $this->hdr['PIBNO'] , 0, 1, 'L', 0);
        $this->pdf->Ln();
        
        $this->pdf->cell(0, 4.5, 'Kepada       :'  , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'IMPORTIR'       , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NPWP    : ' . $this->hdr['IMPNPWP'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Nama    : ' . $this->hdr['IMPNAMA'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Alamat  : ' . $this->hdr['IMPALMT'], 0, 1, 'L', 0);
        $this->pdf->Ln(4);
        $this->pdf->cell(0, 4.5, 'PPJK' , 0, 0, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NPWP    : ' . $this->rpt['PPJKNPWP'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Nama    : ' . $this->rpt['PPJKNAMA'] , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Alamat  : ' . $this->rpt['PPJKALMT'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NP PPJK : ' . $this->rpt['PPJKNP'], 0, 1, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(32, 4.5, 'Lokasi Barang ', 0, 0, 'L', 0);
        $this->pdf->multicell(0, 4.5, $arr[0] , 0, 'L');
        $this->pdf->Ln();
        $this->pdf->multicell(0, 4.5, str_replace($arr[0]."\n", '', $this->res['DESKRIPSI']."\n") , 0, 'L');
        $this->pdf->Ln();
        $this->pdf->multicell(0, 4.5, ($this->res['RESKD']=='400')?"Berdasarkan  hasil penelitian  dokumen, PIB Saudara ditetapkan JALUR MERAH.  Agar Saudara menyerahkan hasil cetak PIB dan dokumen pelengkap pabean serta menyiapkan barang untuk dilakukan pemeriksaan fisik dalam jangka waktu 3 ( tiga ) hari kerja setelah tanggal SPJM ini.":"Berdasarkan  hasil penelitian  dokumen, PIB Saudara ditetapkan JALUR KUNING.  Agar Saudara menyerahkan hasil cetak PIB dan dokumen pelengkap pabean dalam jangka waktu 3 ( tiga ) hari kerja setelah tanggal SPJK ini." , 0, 'L');
        
        $this->pdf->setY(-75);
        $this->pdf->cell(0, 4.5, 'Pejabat yang menangani pelayanan pabean / ' , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'Pejabat Pemeriksa Dokumen' , 0, 1, 'L', 0);
        $this->pdf->ln(15);
        $this->pdf->cell(0, 4.5, 'Tanda Tangan     :' , 0, 1, 'L', 0);
        $this->pdf->ln();
        $this->pdf->cell(0, 4.5, 'Nama             : ' . $this->res['PEJABAT1'] , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'Nip              : ' . $this->res['NIP1'] , 0, 1, 'L', 0);
        $this->pdf->multicell(0, 4.5, "Peruntukan :
1. Importir;
2. Pejabat pemeriksa barang
Formulir ini dicetak secara otomatis oleh sistem komputer dan tidak memerlukan nama, tanda tangan pejabat, dan cap dinas." , 0, 'L');
    }
    
    function drawRespon450(){
        $this->pdf->SetMargins(5, 5, 5, 5);
        $this->pdf->SetAutoPageBreak(0, 10);
        $this->pdf->setY(5);
        $this->pdf->SetFont('courier', '', '11');
        $this->pdf->multicell(0, 5, $this->ktr['KOP'], 0, 'L');
        $this->pdf->Ln(10);
        $this->pdf->SetFont('courier', 'BU', '13');
        $this->pdf->cell(0, 5, 'SURAT PEMBERITAHUAN PEMERIKSAAN FISIK (SPPF)', 0, 0, 'C', 0);
        
        $this->pdf->Ln();
        $this->pdf->SetFont('courier', '', '10');
        $this->pdf->multicell(0, 4.5, 'Nomor : ' . $this->res['DOKRESNO'] . '   Tanggal : '. date('d-m-Y',strtotime($this->res['DOKRESTG'])) , 0, 'C');
        $this->pdf->Ln(8);
        $this->pdf->cell(180, 4.5, 'Nomor Pengajuan           : ' . $this->formatCar($this->hdr['CAR']), 0, 1, 'L', 0);
        $this->pdf->cell(180, 4.5, 'Nomor Pendaftaran PIB     : ' . $this->hdr['PIBNO'] . '   Tanggal : ' . date('d-m-Y',strtotime($this->hdr['PIBTG'])) , 0, 0, 'L', 0);
        $this->pdf->Ln(7);
        $this->pdf->cell(0, 4.5,'Kepada       :', 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'IMPORTIR' , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NPWP    : ' . $this->hdr['IMPNPWP'] , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Nama    : ' . $this->hdr['IMPNAMA'] , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Alamat  : ' . $this->hdr['IMPALMT'] , 0, 1, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 4.5, 'PPJK' , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NPWP    : ' . $this->hdr['PPJKNPWP'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Nama    : ' . $this->hdr['PPJKNAMA'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Alamat  : ' . $this->hdr['PPJKALMT'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NP PPJK : ' . $this->hdr['PPJKNO'], 0, 1, 'L', 0);
        $this->pdf->Ln(2);
        $this->pdf->cell(0, 4.5, 'Lokasi Barang             : ' . $this->hdr['TMPTBN'] . ' - ' . $this->main->get_uraian("SELECT URAIAN FROM m_gudang WHERE KDKPBC = '".$this->hdr['KDKPBC']."' AND KDGDG = '".$this->hdr['TMPTBN']."'",'URAIAN') , 0, 1, 'L', 0);
    if($this->hdr['MODA']=='1'){
        $dok = explode('|', $this->main->get_uraian("SELECT CONCAT(DOKNO,'|',DOKTG) AS UR FROM t_bc20dok WHERE CAR = '".$this->hdr['CAR']."' and KODE_TRADER = '".$this->hdr['KODE_TRADER']."' and DOKKD in ('705','704')",'UR'));
        $this->pdf->cell(0, 4.5, 'No. dan Tanggal B/L       : ' . $this->fixLen($dok[0],30) .'  Tanggal : '.date('d-m-Y',strtotime($dok[1])) , 0, 1, 'L', 0);
    }
    elseif($this->hdr['MODA']=='4'){
        $dok = explode('|', $this->main->get_uraian("SELECT CONCAT(DOKNO,'|',DOKTG) AS UR FROM t_bc20dok WHERE CAR = '".$this->hdr['CAR']."' and KODE_TRADER = '".$this->hdr['KODE_TRADER']."' and DOKKD in ('741','740')",'UR'));
        $this->pdf->cell(0, 4.5, 'No. dan Tanggal AWB       : ' . $this->fixLen($dok[0],30) .'  Tanggal : '.date('d-m-Y',strtotime($dok[1])) , 0, 1, 'L', 0);
    }
        $this->pdf->cell(0, 4.5, 'Sarana Pengangkut         : ' . $this->hdr['ANGKUTNAMA'] . '    ' . $this->hdr['ANGKUTNO'] , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'No Voy./Flight            : ' . $this->hdr['ANGKUTNO'] , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'No/Tgl BC 1.1             : ' . $this->fixLen($this->hdr['DOKTUPNO'],6) . '    Tanggal : ' . $this->fixLen(date('d-m-Y', strtotime($this->hdr['doktuptg'])),10) . '  Pos     : ' . $this->fixLen($this->hdr['POSNO'],4) . ' ' . $this->fixLen($this->hdr['POSSUB'],4) . ' ' . $this->fixLen($this->hdr['POSSUBSUB'],4) . ' ' , 0, 1, 'L', 0);
        
        $kms = $this->main->get_uraian("SELECT group_concat(concat(a.JMKEMAS,' ',a.JNKEMAS) SEPARATOR ';') as UR FROM t_bc20kms a WHERE a.KODE_TRADER = '".$this->hdr['KODE_TRADER']."' AND a.CAR = '".$this->hdr['CAR']."'",'UR');
        $this->pdf->cell(0, 4.5, 'Jumlah / Jenis Kemasan    : ' . $this->fixLen($kms, 31) . ' Berat   : ' . number_format($this->hdr['BRUTO'], 2, '.', ',') . ' KGM' , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'Merk Kemasan              : ' , 0, 1, 'L', 0);
        $this->pdf->Ln();
        $jmlConr = count($this->conr);
        $this->pdf->cell(0, 4.5, 'Jumlah Peti Kemas         : ' . $jmlConr , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'Nomor Peti Kemas / Ukuran : ' , 0, 1, 'L', 0);
        $arr = array(1=>array('CAPTION'=>'No.','WEIGHT'=>'10','FIELD'=>''),
                     2=>array('CAPTION'=>'No.Peti Kemas','WEIGHT'=>'30','FIELD'=>'CONTNO'),
                     3=>array('CAPTION'=>'Ukuran','WEIGHT'=>'20','FIELD'=>'CONTUKUR'),
                     4=>array('CAPTION'=>'Penegahan','WEIGHT'=>'25','FIELD'=>'CONTTIPE'),
                     5=>array('CAPTION'=>'Ket','WEIGHT'=>'10','FIELD'=>''));
        
        foreach($arr as $a){
            $this->pdf->cell($a['WEIGHT'], 5, $a['CAPTION'] , 1, 0, 'L', 0);
        }
        if($jmlConr>1){
            $this->pdf->setX($this->pdf->getX()+5);
            foreach($arr as $a){
                $this->pdf->cell($a['WEIGHT'], 5, $a['CAPTION'] , 1, 0, 'L', 0);
            }
        }
        $this->pdf->Ln();
        $akr = ($jmlConr > 14)?14:$jmlConr;
        for($i=1;$i<$akr;$i++){
            foreach($arr as $a){
                if($a['CAPTION']=='No.'){
                    $this->pdf->cell($a['WEIGHT'], 5, $i , 1, 0, 'L', 0);
                }else{
                    $this->pdf->cell($a['WEIGHT'], 5, $this->conr[$i-1][$a['FIELD']] , 1, 0, 'L', 0);
                }
            }
            if($i % 2 == 1){
                $this->pdf->setX($this->pdf->getX()+5);
            }else{
                $this->pdf->Ln();
            }
        }
        $this->pdf->setY(-93);
        $this->pdf->multicell(0, 4.5, "Petunjuk : 
1. Importir;
2. Pejabat pemeriksa barang;
3. Pejabat yang mengawasi pengeluaran barang.
Formulir ini dicetak secara otomatis  oleh sistem komputer dan tidak memerlukan nama, tanda tangan pejabat, dan cap dinas.

Diberitahukan  bahwa dari hasil penelitian dokumen, terhadap barang dalam PIB dengan nomor pendaftaran  tersebut di atas disetujui untuk  dikeluarkan dengan pemeriksaan fisik di tempat Saudara, dengan ketentuan sebagai berikut:
1. Dilakukan penyegelan dan/atau pengawalan oleh Pejabat Bea dan Cukai;
2. Wajib memberikan bantuan yang layak kepada Pejabat Bea dan Cukai yang melaksanakan.

Pejabat Pemeriksa Dokumen


Tanda Tangan :
Nama        : " . $this->res['PEJABAT1'] . "  
NIP         : " . $this->res['NIP1'], 0, 'L');
        if($jmlConr>14){
            while($i<=$jmlConr){
                $this->pdf->addPage();
                $this->pdf->SetFont('courier', '', '11');
                $this->pdf->multicell(0, 5, $this->ktr['KOP'], 0, 'L');
                $this->pdf->Ln(10);
                $this->pdf->SetFont('courier', 'BU', '13');
                $this->pdf->cell(0, 5, 'SURAT PEMBERITAHUAN PEMERIKSAAN FISIK (SPPF)', 0, 0, 'C', 0);
                $this->pdf->Ln();
                $this->pdf->SetFont('courier', '', '10');
                $this->pdf->multicell(0, 4.5, 'Nomor : ' . $this->res['DOKRESNO'] . '   Tanggal : '. date('d-m-Y',strtotime($this->res['DOKRESTG'])) , 0, 'C');
                $this->pdf->Ln(8);
                $this->pdf->cell(180, 4.5, 'Nomor Pengajuan           : ' . $this->formatCar($this->hdr['CAR']), 0, 1, 'L', 0);
                $this->pdf->cell(180, 4.5, 'Nomor Pendaftaran PIB     : ' . $this->hdr['PIBNO'] . '   Tanggal : ' . date('d-m-Y',strtotime($this->hdr['PIBTG'])) , 0, 1, 'L', 0);
                
                $awl = $i;
                $akr = ($jmlConr > $awl+79)?$awl+79: $jmlConr;
                foreach($arr as $a){
                    $this->pdf->cell($a['WEIGHT'], 5, $a['CAPTION'] , 1, 0, 'L', 0);
                }
                if(($akr - $awl)>0){
                    $this->pdf->setX($this->pdf->getX()+5);
                    foreach($arr as $a){
                        $this->pdf->cell($a['WEIGHT'], 5, $a['CAPTION'] , 1, 0, 'L', 0);
                    }
                }
                $this->pdf->Ln();
                for ($i = $awl; $i <= $akr; $i++) {
                    foreach ($arr as $a) {
                        if ($a['CAPTION'] == 'No.') {
                            $this->pdf->cell($a['WEIGHT'], 5, $i, 1, 0, 'L', 0);
                        } else {
                            $this->pdf->cell($a['WEIGHT'], 5, $this->conr[$i - 1][$a['FIELD']], 1, 0, 'L', 0);
                        }
                    }
                    if ($i % 2 == 1) {
                        $this->pdf->setX($this->pdf->getX() + 5);
                    } else {
                        $this->pdf->Ln();
                    }
                }
                
                $this->pdf->setY(-30);
                $this->pdf->cell(100, 4.5, 'Pejabat yang memeriksa dokumen I/II' , 0, 0, 'L', 0);
                $this->pdf->cell(100, 4.5, 'Pejabat yang melaksanakan pengeluaran barang:' , 0, 1, 'L', 0);
                $this->pdf->cell(100, 4.5, 'Tanda Tangan : .............................' , 0, 0, 'L', 0);
                $this->pdf->cell(100, 4.5, 'Tanda Tangan : .............................' , 0, 1, 'L', 0);
                $this->pdf->cell(100, 4.5, 'Nama         : ' . $this->res['PEJABAT1'] , 0, 0, 'L', 0);
                $this->pdf->cell(100, 4.5, 'Nama         : ' . $this->res['PEJABAT2'] , 0, 1, 'L', 0);
                $this->pdf->cell(100, 4.5, 'NIP          : ' . $this->res['NIP1'], 0, 0, 'L', 0);
                $this->pdf->cell(100, 4.5, 'NIP          : ' . $this->res['NIP2'], 0, 1, 'L', 0);
                $this->pdf->line(5,$this->pdf->getY(),205,$this->pdf->getY());
                $this->pdf->cell(0, 4.5, 'Lembar 1 Untuk DJBC' . $this->res['NIP2'], 0, 1, 'L', 0);
            }
        }
    #=====================================================
    }
    */
}