<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class actManageUser extends CI_Model {

    function lstUser($type = '') {
        $this->load->library('newtable');
        switch ($type) {
            case 'regOnline':
                $status = '00';
                break;
            default :
                $status = '01';
                break;
        }
        $SQL = "SELECT  concat(b.URAIAN,' - ',a.ID) AS Identitas, " .
                "NAMA AS `Nama Perusahaan`, " .
                "ALAMAT AS `Alamat Perusahaan`, " .
                "TELEPON AS `No.Telp Perusahaan`, " .
                "FAX AS `No.Fax Perusahaan`, " .
                "NAMA_PEMILIK AS `Nama Pemilik`, " .
                "KODE_TRADER " .
                "FROM    m_trader a Left Join m_tabel b on b.MODUL = 'BC20' AND b.KDTAB = 'JENIS_IDENTITAS' AND b.KDREC = a.KODE_ID " .
                "WHERE STATUS = '" . $status . "'";
        $this->newtable->clear();
        $this->newtable->keys(array('KODE_TRADER'));
        $this->newtable->hiddens(array('KODE_TRADER'));
        $this->newtable->search(array(array("concat(b.URAIAN,' - ',a.ID)", 'Identitas'),
            array('NAMA', 'Nama Perusahaan'),
            array('ALAMAT', 'Alamat Perusahaan'),
            array('TELEPON', 'No.Telp Perusahaan'),
            array('FAX', 'No.Fax Perusahaan'),
            array('NAMA_PEMILIK', 'Nama Pemilik')));
        $this->newtable->orderby('KODE_TRADER');
        $this->newtable->sortby("DESC");
        $this->newtable->action(site_url('usermanage/regOnline'));
        $process = array('Approve Registrasi' => array('GETAJAX', site_url('usermanage/viwApproval'), '1', '_content_'));
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->rowcount(10);
        $this->newtable->tipe_check('radio');
        $this->newtable->set_formid("f" . $type);
        $this->newtable->set_divid("div" . $type);
        $this->newtable->menu($process);
        $tabel = $this->newtable->generate($SQL);
        if ($this->input->post("ajax"))
            return $tabel;
        else
            return array("tabel" => $tabel);
    }

    function lstUserBC($type = '') {
        $this->load->library('newtable');
        switch ($type) {
            case 'regOnline':
                $status = '00';
                break;
            default :
                $status = '01';
                break;
        }
        $SQL = "SELECT  concat(b.URAIAN,' - ',a.ID) AS Identitas, " .
                "NAMA AS `Nama Perusahaan`, " .
                "ALAMAT AS `Alamat Perusahaan`, " .
                "TELEPON AS `No.Telp Perusahaan`, " .
                "FAX AS `No.Fax Perusahaan`, " .
                "NAMA_PEMILIK AS `Nama Pemilik`, " .
                "KODE_TRADER " .
                "FROM    m_trader a Left Join m_tabel b on b.MODUL = 'BC20' AND b.KDTAB = 'JENIS_IDENTITAS' AND b.KDREC = a.KODE_ID " .
                "WHERE STATUS = '" . $status . "'"; //print_r($SQL);die();
        $this->newtable->clear();
        $this->newtable->keys(array('KODE_TRADER'));
        $this->newtable->hiddens(array('KODE_TRADER'));
        $this->newtable->search(array(array("concat(b.URAIAN,' - ',a.ID)", 'Identitas'),
            array('NAMA', 'Nama Perusahaan'),            
            array('NAMA_PEMILIK', 'Nama Pemilik')));
        $this->newtable->orderby('KODE_TRADER');
        $this->newtable->sortby("DESC");
        $this->newtable->action(site_url('usermanage/regOnline'));
        $process = array('Approve Registrasi' => array('GETAJAX', site_url('usermanage/viwApproval'), '1', '_content_'));
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->rowcount(10);
        $this->newtable->tipe_check('radio');
        $this->newtable->set_formid("f" . $type);
        $this->newtable->set_divid("div" . $type);
        $this->newtable->menu($process);
        $tabel = $this->newtable->generate($SQL);
        if ($this->input->post("ajax"))
            return $tabel;
        else
            return array("tabel" => $tabel);
    }

    function lstPerusahaan($type = '', $kd_trader = '') {
        $this->load->library('newtable');
        switch (strtolower($type)) {
            case'perusahaan':
                $judul = "Daftar Perusahaan.";
                $SQL = "SELECT  concat(b.URAIAN,' - ',a.ID) AS Identitas,
                                NAMA AS `Nama Perusahaan`,
                                ALAMAT AS `Alamat Perusahaan`,
                                TELEPON AS `No.Telp Perusahaan`,
                                FAX AS `No.Fax Perusahaan`,
                                NAMA_PEMILIK AS `Nama Pemilik`,
                                KODE_TRADER
                        FROM    m_trader a Left Join m_tabel b on b.MODUL = 'BC20' AND b.KDTAB = 'JENIS_IDENTITAS' AND b.KDREC = a.KODE_ID 
                        WHERE   STATUS = '02'";
                $this->newtable->keys(array('KODE_TRADER'));
                $this->newtable->hiddens(array('KODE_TRADER'));
                $this->newtable->search(array(
                    array("concat(b.URAIAN,' - ',a.ID)", 'Identitas'),
                    array('NAMA', 'Nama Perusahaan'),
                    array('ALAMAT', 'Alamat Perusahaan'),
                    array('TELEPON', 'No.Telp Perusahaan'),
                    array('FAX', 'No.Fax Perusahaan'),
                    array('NAMA_PEMILIK', 'Nama Pemilik')));
                $this->newtable->orderby('KODE_TRADER');
                $this->newtable->sortby("DESC");
                $this->newtable->action(site_url('usermanage/lstPerusahaan/perusahaan'));
                $this->newtable->detail(site_url('usermanage/lstPerusahaan/userperusahaan/'));
                $this->newtable->detail_tipe('detil_priview_bottom');
                $process = array(
                    'Edit Perusahaan' => array('GETAJAX', site_url('usermanage/mngPerusahaan/edit'), '1', '_content_'),
                    'Nonaktif Perusahaan' => array('DELETEAJAX', site_url('usermanage/nonAktif'), '1', '_content_'),
                    'Reset Passsword Admin Perusahaan' => array('MSGBOX', site_url('usermanage/mngPerusahaan/resetPass'), '1'));
                break;
            case'userperusahaan':
                $this->load->model('actMain');
                $judul = "Daftar User.";
                $SQL = "SELECT  Username,
                                NAMA as `Nama`,
                                ALAMAT as `Alamat`,
                                TELEPON as `Telepon`,
                                JABATAN as `Jabatan`,
                                b.URAIAN as `Tipe User`,
                                c.URAIAN as `Status`,
                                LAST_LOGIN as `Login Terakhir`,
                                EXPIRED_DATE as `Aktif Sampai`,
                                KODE_TRADER,USER_ID
                        FROM    t_user a Left Join m_tabel b on b.MODUL = 'BC20' AND b.KDTAB = 'TIPE_USER' AND b.KDREC = a.TIPE_USER
                                         Left Join m_tabel c on c.MODUL = 'BC20' AND c.KDTAB = 'STATUS_USER' AND c.KDREC = a.STATUS_USER
                        WHERE KODE_TRADER = '" . $kd_trader . "'";
                $this->newtable->keys(array('KODE_TRADER', 'USER_ID'));
                $this->newtable->hiddens(array('KODE_TRADER', 'USER_ID'));
                $this->newtable->search(array(
                    array("Username", 'Username'),
                    array('NAMA', 'Nama'),
                    array('ALAMAT', 'Alamat'),
                    array('TELEPON', 'Telepon'),
                    array('JABATAN', 'Jabatan'),
                    array('b.KDREC', 'Tipe User', 'tag-select', $this->actMain->get_mtabel('TIPE_USER')),
                    array('c.KDREC', 'Status', 'tag-select', $this->actMain->get_mtabel('STATUS_USER'))));
                $this->newtable->orderby(3);
                $this->newtable->sortby("DESC");
                $this->newtable->action(site_url('usermanage/lstPerusahaan/userperusahaan/' . $kd_trader));
                $process = array(
                    'Ubah User' => array('GETAJAX', site_url('usermanage/mngUserOperator/edit'), '1', '_content_'),
                    'Hapus User' => array('GETAJAX', site_url('usermanage/mngUserOperator/hapus'), '1', '_content_'),
                    'Reset Passsword User' => array('GETAJAX', site_url('usermanage/mngPerusahaan/resetPass'), '1', '_content_'));
                break;
        }
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->rowcount(10);
        $this->newtable->clear();
        $this->newtable->set_formid("f" . $type);
        $this->newtable->set_divid("div" . $type);
        $this->newtable->menu($process);
        $tabel = $this->newtable->generate($SQL);
        $arrdata = array('tabel' => $tabel, 'judul' => $judul);
        if ($this->input->post("ajax")) {
            echo $tabel;
            die();
        } else {
            return $arrdata;
        } 
    }

    function getProfile() {
        $this->load->model('actMain');
        $id_user = $this->newsession->userdata('USER_ID');
        $kd_trader = $this->newsession->userdata('KODE_TRADER');
        #perusahaan
        $SQL = "SELECT * FROM m_trader WHERE KODE_TRADER = '" . $kd_trader . "'";
        $hsl['PERUSAHAAN'] = $this->actMain->get_result($SQL);
        #USER
        $SQL = "SELECT * FROM t_user WHERE USER_ID = '" . $id_user . "'";
        $hsl['USER'] = $this->actMain->get_result($SQL);

        $SQL = "SELECT a.*,b.URAIAN_KPBC FROM m_trader_bc20 a Left Join m_kpbc b ON a.KPBC = b.KDKPBC WHERE a.KODE_TRADER = '" . $kd_trader . "'";
        $hsl['PIB'] = $this->actMain->get_result($SQL);
        $hsl['PIB']['MAXLENGTH'] = '<input type="hidden" id="MAXLENGTH" value="' . $this->actMain->get_uraian("SELECT F_MAXLENGTH('m_trader_bc20') as MAXLENGTH", 'MAXLENGTH') . '"/>';

        $SQL = "SELECT a.*,b.URAIAN_KPBC FROM m_trader_bc30 a Left Join m_kpbc b on a.KPBC = b.KDKPBC WHERE a.KODE_TRADER = '" . $kd_trader . "'";
        $hsl['PEB'] = $this->actMain->get_result($SQL);
        $hsl['PEB']['MAXLENGTH'] = '<input type="hidden" id="MAXLENGTH" value="' . $this->actMain->get_uraian("SELECT F_MAXLENGTH('m_trader_bc30') as MAXLENGTH", 'MAXLENGTH') . '"/>';

        $SQL = "SELECT a.*,b.URAIAN_KPBC FROM m_trader_bc23 a Left Join m_kpbc b on a.KPBC = b.KDKPBC WHERE a.KODE_TRADER = '" . $kd_trader . "'";
        $hsl['TPB'] = $this->actMain->get_result($SQL);
        $hsl['TPB']['MAXLENGTH'] = '<input type="hidden" id="MAXLENGTH" value="' . $this->actMain->get_uraian("SELECT F_MAXLENGTH('m_trader_bc23') as MAXLENGTH", 'MAXLENGTH') . '"/>';

        $SQL = "SELECT a.*,b.URAIAN_KPBC FROM m_trader_bc10 a Left Join m_kpbc b on a.KPBC = b.KDKPBC WHERE a.KODE_TRADER = '" . $kd_trader . "'";
        $hsl['MANIFEST'] = $this->actMain->get_result($SQL);
        $hsl['MANIFEST']['MAXLENGTH'] = '<input type="hidden" id="MAXLENGTH" value="' . $this->actMain->get_uraian("SELECT F_MAXLENGTH('m_trader_bc10') as MAXLENGTH", 'MAXLENGTH') . '"/>';

        $hsl['JENIS_IDENTITAS'] = $this->actMain->get_mtabel('JENIS_IDENTITAS', 1, false);
        $hsl['TIPE_TRADER'] = $this->actMain->get_mtabel('TIPE_TRADER', 1, false, '', 'KODEDANUR');
        $hsl['APIKD'] = $this->actMain->get_mtabel('JENIS_API', 1, true, '', 'KODEDANUR');
        $hsl['JNS_PERUSAHAAN'] = $this->actMain->get_mtabel('JNS_PERUSAHAAN', 1, true, '', 'KODEDANUR', 'BC30');
        $hsl['STATUS_EKS'] = $this->actMain->get_mtabel('STATUS_EKS', 1, true, '', 'KODEDANUR', 'BC30');
        $hsl['STATUS_KONS'] = $this->actMain->get_mtabel('STATUS_KONS', 1, true, '', 'KODEDANUR', 'BC30');
        $hsl['STATUS_PERUSAHAAN'] = $this->actMain->get_mtabel('STATUS_PERUSAHAAN', 1, false, '', 'KODEDANUR', 'BC23');
        $hsl['APIKD'] = $this->actMain->get_mtabel('JNS_NOIJIN', 1, false, '', 'KODEDANUR', 'BC23');
        $hsl['KDTPB'] = $this->actMain->get_mtabel('JENIS_TPB', 1, false, '', 'KODEDANUR', 'BC23');
        return $hsl;
    }

    function setProfile() {
        $this->load->model('actMain');
        $profile = $this->input->post('DATA');
        if (is_array($profile['DOKUMEN'])) {
            $profile['DOKUMEN'] = implode(',', $profile['DOKUMEN']);
        }
        $ex = $this->actMain->insertRefernce('m_trader', $profile);
        if ($ex) {
            $hsl = 'MSG|OK||Data Profile Berhasil disimpan.';
        } else {
            $hsl = 'MSG|ER||Data Profile Gagal disimpan.';
        }
        return $hsl;
    }

    function setProfileUser() {
        $this->load->model('actMain');
        $profile = $this->input->post('DATA');
        $profile['USER_ID'] = $this->newsession->userdata('USER_ID');
        $ex = $this->actMain->insertRefernce('t_user', $profile);
        if ($ex) {
            $hsl = 'MSG|OK||Data Profile Berhasil disimpan.';
        } else {
            $hsl = 'MSG|ER||Data Profile Gagal disimpan.';
        }
        return $hsl;
        //redirect('usernamanage', 'refresh');
    }

    function setProfilePIB() {
        $this->load->model('actMain');
        $profile = $this->input->post('DATA');
        $profile['USER_ID'] = $this->newsession->userdata('USER_ID');
        if (is_array($profile['IMPSTATUS'])) {
            $profile['IMPSTATUS'] = implode(',', $profile['IMPSTATUS']);
        }
        //print_r($profile);die();
        $ex = $this->actMain->insertRefernce('m_trader_bc20', $profile);
        if ($ex) {
            $hsl = 'MSG|OK||Data PIB Berhasil disimpan.';
        } else {
            $hsl = 'MSG|ER||Data PIB Gagal disimpan.';
        }
        return $hsl;
        //redirect('usernamanage', 'refresh');
    }

    function setProfilePEB() {
        $this->load->model('actMain');
        $profile = $this->input->post('DATA');
        $profile['USER_ID'] = $this->newsession->userdata('USER_ID');
        $ex = $this->actMain->insertRefernce('m_trader_bc30', $profile);
        if ($ex) {
            $hsl = 'MSG|OK||Data PEB Berhasil disimpan.';
        } else {
            $hsl = 'MSG|ER||Data PEB Gagal disimpan.';
        }
        return $hsl;
        //redirect('usernamanage', 'refresh');
    }

    function setProfileTPB() {
        $this->load->model('actMain');
        $profile = $this->input->post('DATA');
        $profile['USER_ID'] = $this->newsession->userdata('USER_ID');
        $ex = $this->actMain->insertRefernce('m_trader_bc23', $profile);
        if ($ex) {
            $hsl = 'MSG|OK||Data TPB Berhasil disimpan.';
        } else {
            $hsl = 'MSG|ER||Data TPB Gagal disimpan.';
        }
        return $hsl;
    }

    function setProfileManifest() {
        $this->load->model('actMain');
        $profile = $this->input->post('DATA');
        $profile['USER_ID'] = $this->newsession->userdata('USER_ID');
        $ex = $this->actMain->insertRefernce('m_trader_bc10', $profile);
        if ($ex) {
            $hsl = 'MSG|OK||Data Manifest Berhasil disimpan.';
        } else {
            $hsl = 'MSG|ER||Data Manifest Gagal disimpan.';
        }
        return $hsl;
    }

    function approve() {
        $this->load->model('actMain');
        $data = $this->input->post('DATA'); //print_r($data);die();
        if ($data['STATUS'] === '01') {
            foreach ($data['DOKUMEN'] as $a) {
                $ada = $this->actMain->get_uraian("SELECT COUNT(KODE_TRADER) JML FROM M_TRADER_" . $a . " WHERE KODE_TRADER='" . $data['KODE_TRADER'] . "'", 'JML');
                if ((int) $ada <= 0) {
                    $msg .= 'Dokumen ' . $a . ' Tidak ada memiliki nomor registrasi.<br>';
                }
            }
        }
        if ($msg != '') {
            return 'MSG|ERR||' . $msg;
        }

        //Data Trader
        $data['DOKUMEN'] = implode(",", $data['DOKUMEN']);
        $data['ID'] = str_replace(".", "", str_replace("-", "", $data['ID']));
        $data['STATUS'] = '01';
        $exec = $this->actMain->insertRefernce('m_trader', $data);
        return "MSG|OK|" . site_url('usermanage/regOnline') . "|Berhasil.";
    }

    function approveBC() {
        $this->load->model('actMain');
        $data = $this->input->post('DATA');
        $data['DOKUMEN'] = implode(",", $data['DOKUMEN']);
        $data['ID'] = str_replace(".", "", str_replace("-", "", $data['ID']));
        $data['STATUS'] = '02';
        $exe = $this->actMain->insertRefernce('m_trader', $data);
        $whrTBL['KODE_TRADER'] = $data['KODE_TRADER'];
        $whrTBL['TIPE_USER'] = '4';
        $userCek = $this->db->get_where('t_user', $whrTBL);
        if ($userCek->num_rows() == 0) {
            $pass = $this->actMain->genPassword();
            $USER['KODE_TRADER'] = $data['KODE_TRADER'];
            $USER['USERNAME'] = $data['EMAIL_PEMILIK'];
            $USER['PASSWORD'] = md5($pass);
            $USER['NAMA'] = $data['NAMA_PEMILIK'];
            $USER['ALAMAT'] = $data['ALAMAT_PEMILIK'];
            $USER['TIPE_USER'] = '4';
            $USER['STATUS_USER'] = '1';
            $USER['EXPIRED_DATE'] = date("d-m-Y", strtotime(date() . " +2 day"));
            $exe = $this->actMain->insertRefernce('t_user', $USER);
            if ($exe) {
                //send email 
                $isiEmail = "<pre>Anda dapat login ke Sistem Trade to Goverment dengan username dan password sebagai berikut:
                    email   	: " . $USER['USERNAME'] . "
                    Password	: " . $pass . "

                    * User anda akan kadaluarsa pada tanggal " . $USER['EXPIRED_DATE'] . ", untuk memperlama waktu aktif user anda,silahkan segera login.</pre>";
                $kirimEmail = $this->actMain->send_mail($USER['USERNAME'], 'User dan Password Trade to Goverment', $isiEmail);
                if ($kirimEmail) {
                    return "MSG|OK|" . site_url('usermanage/regOnlineBC') . "|Aprove Perusahaan Berhasil.<br>dan Email konfirmasi user password berhasil dikirm.";
                } else {
                    return "MSG|OK|" . site_url('usermanage/regOnlineBC') . "|Aprove Perusahaan Berhasil.<br>dan Email konfirmasi user password gagal dikirim.<br>Kirim Email konfirmasi user password setelah user terdaftar.";
                }
            }
        } else {
            return "MSG|OK|" . site_url('usermanage/regOnlineBC') . "|Aprove Berhasil.";
        }
    }

    function view_by_username_password() {
        $this->load->model('actMain');
        $username = $this->input->post($USERNAME);
        $sql = "SELECT * FROM t_user
						FROM t_user WHERE USERNAME = '" . $USERNAME . "' AND PASSWORD '" . md5($data['']) . "'";
        print_r($sql);
        die();
        return $this->db->query($sql);
    }

    function updatepassword($data) {
        $sql = "UPDATE t_USER 
					SET PASSWORD='" . md5($data['PASSWORDBARU']) . "' WHERE USERNAME='" . $data['USERNAME'] . "'";
        $this->db->query($sql);
    }

    function cekUbahPassword($data) {
        $this->load->model('actMain');
        #username & password benar
        $sql = "SELECT PASSWORD FROM t_user WHERE USERNAME = '" . $data['USERNAME'] . "'"; #AND PASSWORD = '" . md5($data['PASSWORD']) . "'
        $JML = $this->actMain->get_result($sql);
        if ($JML['PASSWORD'] != md5($data['PASSWORD'])) {
            $err .= 'Password lama anda salah<br>';
        }
        #password konfirm = password baru
        if ($data['PASSWORDBARU'] != $data['PASSWORDBARUKONFIRMASI']) {
            $err .= 'Konfirmasi password tidak sama<br>';
        }
        if ($err == '') {
            //update
            $this->actManageUser->updatepassword($data);
            return 'MSG|OK||Ubah Password Berhasil';
        } else {
            return 'MSG|ERR||' . $err;
        }
        echo $this->load->view('frmProfile', $data, true);
        exit();
    }

    function resetPassword($kd_trader, $id_user = '') {
        $this->load->model('actMain');
        if ($id_user == '') {
            //ambil id admin yang ada di kd_trader tersebut
            $sql = "SELECT GROUP_CONCAT(USER_ID) 'UID'
                    FROM t_user 
                    WHERE KODE_TRADER = " . $kd_trader . " AND TIPE_USER = '4'"; //die($sql);
            $arrData = $this->actMain->get_result($sql);
            $id_user = $arrData['UID'];
        }
        if ($id_user != '') {
            $sql = "SELECT USER_ID,USERNAME,NAMA,EXPIRED_DATE
                     FROM   t_user
                     WHERE  USER_ID in (" . $id_user . ")"; //die($sql);
            $data = $this->actMain->get_result($sql, true);
            foreach ($data as $a) {
                $pass = $this->actMain->genPassword();
                $USER['USER_ID'] = $a['USER_ID'];
                $USER['PASSWORD'] = md5($pass);
                $USER['EXPIRED_DATE'] = date("Y-m-d", strtotime(date() . " +2 day"));
                $this->actMain->insertRefernce('t_user', $USER);
                //$this->db->insert('t_user', $USER);
                $isiEmail = "<pre>Password login anda telah direset:
email   	: " . $a['USERNAME'] . "
Password	: " . $pass . "

* User anda akan kadaluarsa pada tanggal " . $USER['EXPIRED_DATE'] . ", untuk memperlama waktu aktif user anda,silahkan segera login.</pre>";
                $kirimEmail = $this->actMain->send_mail($USER['USERNAME'], 'Reset Password Trade to Goverment', $isiEmail);
                if ($kirimEmail) {
                    $hasil[] = 'OK|' . $a['USERNAME'];
                } else {
                    $hasil[] = 'ER|' . $a['USERNAME'];
                }
            }
        } else {
            $hasil[] = '00|';
        }
        return $hasil;
    }

    function getNoRegistrasi($tipe, $kd_trader) {
        $this->load->model('actMain');
        $sql = "SELECT a.KPBC,a.NOREG,b.URAIAN_KPBC as 'URKPBC' " .
                "FROM m_trader_" . $tipe . " a LEFT JOIN m_kpbc b ON a.KPBC = b.KDKPBC " .
                "WHERE KODE_TRADER = '" . $kd_trader . "'";
        $data['DATA'] = $this->actMain->get_result($sql);
        return $data;
    }

    function setNoRegister($tipe, $data) {
        $this->load->model('actMain');
        $ada = $this->actMain->get_uraian("SELECT COUNT(KODE_TRADER) as 'JML' FROM m_trader_" . $tipe . " WHERE KODE_TRADER != " . $data['KODE_TRADER'] . " AND  KPBC = '" . $data['KPBC'] . "' AND NOREG = '" . $data['NOREG'] . "'", 'JML');

        if ($ada > 0) {
            return 'MSG|ERR||Nomor Registrasi : "' . $data['NOREG'] . '  di KPBC : "' . $data['KPBC'] . '" Duplikat';
        }
        $exe = $this->actMain->insertRefernce('m_trader_' . $tipe, $data);
        if ($exe) {
            return 'MSG|OK|msgbox|Proses Nomor Registrasi berhasil';
        } else {
            return 'MSG|ERR||Proses Nomor Registrasi gagal';
        }
    }

    function lstreq($type = '') {
        $this->load->library('newtable');
        $this->load->model("actMain");
        $SQL = "SELECT A.EMAIL, A.TELP as 'TELEPON', A.URAIAN, B.URAIAN as 'STATUS', A.ID_REQUEST
               FROM T_FORMREQUEST A 
                    LEFT JOIN m_tabel B ON B.KDREC = A.STATUS AND B.KDTAB = 'STATUS_REQ' AND B.MODUL = 'TRADER'
               WHERE 1=1";
        $this->newtable->clear();
        $this->newtable->keys(array('ID_REQUEST'));
        $this->newtable->hiddens(array('ID_REQUEST'));
        $this->newtable->search(array(array('EMAIL', 'Email'),
            array('STATUS', 'Status')));
        $this->newtable->orderby('ID_REQUEST');
        $this->newtable->sortby("DESC");
        $this->newtable->action(site_url('usermanage/reqRegistrasi'));
        $process = array('Approve Request' => array('DELETEAJAX', site_url('usermanage/updateStatus'), '1', '_content_'));
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->rowcount(10);
        $this->newtable->tipe_check('radio');
        $this->newtable->set_formid("f" . $type);
        $this->newtable->set_divid("div" . $type);
        $this->newtable->menu($process);
        $tabel .= $this->newtable->generate($SQL);
        if ($this->input->post("ajax"))
            return $tabel;
        else
            return array("tabel" => $tabel);
    }

    function updateStatus($req) {
        $exec = $this->db->simple_query("UPDATE t_formrequest set STATUS='01' WHERE ID_REQUEST= '" . $req . "'");
        if ($exec) {
            $rtn = 'MSG|OK|' . site_url('usermanage/reqRegistrasi') . '| Update Status Berhasil';
        } else {
            $rtn = 'MSG|ER|' . site_url('usermanage/reqRegistrasi') . '|Update Status gagal ';
        }
        return $rtn;
    }

    function getPerusahaan($kd_trader='') {
        $this->load->model('actMain');
        if ($kd_trader != '') { #Gak Dipake
            $query = "SELECT a.* FROM m_trader a WHERE a.KODE_TRADER = '" . $kd_trader . "'";
            $data = $this->actMain->get_result($query);
            $act = site_url('usermanage/editPerusahaan');
        }
        $arrdata = array(
            'DATA' => $data,
            'JENIS_IDENTITAS' => $this->actMain->get_mtabel('JENIS_IDENTITAS', 1, false),
            'MAXLENGTH' => '<input type="hidden" id="MAXLENGTH" value="' . $this->actMain->get_uraian("SELECT F_MAXLENGTH('m_trader') as MAXLENGTH", 'MAXLENGTH') . '"/>',
            'act' => $act);
        return $arrdata;
    }

    function simpanPerusahaan() {
        $this->load->model('actMain');
        $data = $this->input->post('DATA');
        //Data Trader         
        $data['ID'] = str_replace(".", "", str_replace("-", "", $data['ID']));
        $exec = $this->actMain->insertRefernce('m_trader', $data);
        if ($exec) {
            return "MSG|OK|" . site_url('usermanage/lstPerusahaan/perusahaan') . "|Simpan Berhasil.";
        } else {
            return "MSG|ERR|" . site_url('usermanage/mngPerusahaan') . "|Simpan Gagal.";
        }
    }

    function nonAktif($req) {
        $exec = $this->db->simple_query("UPDATE m_trader set STATUS='03' WHERE KODE_TRADER= '" . $req . "'");
        if ($exec) {
            $rtn = 'MSG|OK|' . site_url('usermanage/lstPerusahaan/perusahaan') . '| Non Aktif Berhasil';
        } else {
            $rtn = 'MSG|ER|' . site_url('usermanage/lstPerusahaan/perusahaan') . '|Non Aktif Gagal ';
        }
        return $rtn;
    }

    function daftarUser($kd_trader) {
        $this->load->library('newtable');
        $this->load->model('actMain');
        $type = 'UserOperator';
        $judul = "Daftar User.";
        $SQL = "SELECT  USERNAME,
                        NAMA as `Nama`,
                        ALAMAT as `Alamat`,
                        TELEPON as `Telepon`,
                        JABATAN as `Jabatan`,
                        b.URAIAN as `Tipe User`,
                        c.URAIAN as `Status`,
                        LAST_LOGIN as `Login Terakhir`,
                        EXPIRED_DATE as `Aktif Sampai`,
                        KODE_TRADER,USER_ID
                FROM    t_user a Left Join m_tabel b on b.MODUL = 'BC20' AND b.KDTAB = 'TIPE_USER' AND b.KDREC = a.TIPE_USER
                                 Left Join m_tabel c on c.MODUL = 'BC20' AND c.KDTAB = 'STATUS_USER' AND c.KDREC = a.STATUS_USER
                WHERE KODE_TRADER = '" . $kd_trader . "' AND TIPE_USER='5'";
        $this->newtable->keys(array('USER_ID'));
        $this->newtable->hiddens(array('KODE_TRADER', 'USER_ID'));
        $this->newtable->search(array(
            array("Username", 'Username'),
            array('NAMA', 'Nama'),
            array('ALAMAT', 'Alamat'),
            array('TELEPON', 'Telepon'),
            array('JABATAN', 'Jabatan'),
            array('b.KDREC', 'Tipe User', 'tag-select', $this->actMain->get_mtabel('TIPE_USER')),
            array('c.KDREC', 'Status', 'tag-select', $this->actMain->get_mtabel('STATUS_USER'))));
        $this->newtable->orderby(3);
        $this->newtable->sortby("DESC");
        $this->newtable->action(site_url('usermanage/daftarUser/'));
        $process = array(
            'Tambah User' => array('GETAJAX', site_url('usermanage/mngUserOperator/tambah'), '0', '_content_'),
            'Edit User' => array('GETAJAX', site_url('usermanage/mngUserOperator/edit'), '1', '_content_'),
            'Hapus User' => array('DELETEAJAX', site_url('usermanage/mngUserOperator/hapus'), '1', '_content_'));
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->rowcount(10);
        $this->newtable->tipe_check('radio');
        $this->newtable->set_formid("f" . $type);
        $this->newtable->set_divid("div" . $type);
        $this->newtable->menu($process);
        $tabel = $this->newtable->generate($SQL);
        if ($this->input->post("ajax"))
            return $tabel;
        else
            return array("tabel" => $tabel);
    }

    function getUserOperator($act, $user_id) {
        $this->load->model('actMain');
        switch ($act) {
            case 'edit':
                if ($user_id != '') {
                    $sql = "select * from t_user where user_id='" . $user_id . "'";
                    $hasil['DATA'] = $this->actMain->get_result($sql);
                } else {
                    $hasil = 'error';
                }
                break;
            case'tambah':
                $hasil['act'] = 'save';
                break;
        }
        return $hasil;
    }

    function setUserOperator() {
        $this->load->model('actMain');
        $act = $this->input->post('act');
        switch ($act) {
            case'save':
                $data = $this->input->post('DATA');
                $pass = $this->actMain->genPassword();
                $data['TIPE_USER'] = '5';
                $data['KODE_TRADER'] = $this->newsession->userdata('KODE_TRADER');
                $data['EXPIRED_DATE'] = date("d-m-Y", strtotime(date() . " +2 day"));
                $data['PASSWORD'] = md5($pass);
                $data['STATUS_USER'] = '1';
                $xxx = "SELECT COUNT(USERNAME) as TOTAL FROM t_user WHERE USERNAME = '" . $data['USERNAME'] . "'";
                $cek = $this->actMain->get_uraian($xxx, 'TOTAL');
                if ($cek > 0) {
                    $hasil = 'E-Mail Anda telah Terdaftar. Gunakan E-mail Lainnya';
                } else {
                    $exec = $this->actMain->insertRefernce('t_user', $data);
                    if ($exec) {
                        $isiEmail = "<pre>Anda dapat login ke Sistem Trade to Goverment dengan username dan password sebagai berikut:
                        email   	: " . $data['USERNAME'] . "
                        Password	: " . $pass . "

                        * User anda akan kadaluarsa pada tanggal " . $data['EXPIRED_DATE'] . ", untuk memperlama waktu aktif user anda,silahkan segera login.</pre>";
                        $kirimEmail = $this->actMain->send_mail($data['USERNAME'], 'User dan Password Trade to Goverment', $isiEmail);
                        if ($kirimEmail) {
                            $hasil = "MSG|OK|" . site_url('usermanage/daftarUser') . "|User Berhasil Disimpan.<br>dan Email konfirmasi user password berhasil dikirm.";
                        } else {
                            $hasil = "MSG|OK|" . site_url('usermanage/daftarUser') . "|User Berhasil Disimpan.<br>dan Email konfirmasi user password gagal dikirim.<br>Kirim Email konfirmasi user password setelah user terdaftar.";
                        }
                    } else {
                        $hasil = "MSG|OK|" . site_url('usermanage/daftarUser') . "|Berhasil Disimpan.";
                    }
                }
                break;
            case'update':
                $data = $this->input->post('DATA');
                $xxx = "SELECT COUNT(USERNAME) as TOTAL FROM t_user WHERE USERNAME = '" . $data['USERNAME'] . "'";
                $cek = $this->actMain->get_uraian($xxx, 'TOTAL');
                if ($cek > 0) {
                    $hasil = 'E-Mail Anda telah Terdaftar. Gunakan E-mail Lainnya';
                }
                $exec = $this->actMain->insertRefernce('t_user', $data);
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('usermanage/daftarUser') . '|Update user berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('usermanage/daftarUser') . '|Update user gagal ';
                }
                break;
            case'delete':
                foreach ($this->input->post('tb_chkfUserOperator') as $chkitem) {
                    $arrchk = explode("|", $chkitem);
                    $data['USER_ID'] = $arrchk[0];
                    $this->db->where($data);
                    $exec = $this->db->delete('t_user');
                }
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('usermanage/daftarUser') . '|Delete user berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('usermanage/daftarUser') . '|Delete user gagal';
                }
                break;
        }
        return $hasil;
    }
}