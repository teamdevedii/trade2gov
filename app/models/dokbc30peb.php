<?php
if (!defined('BASEPATH')){exit('No direct script access allowed');}
class dokBC30PEB extends CI_Model {
    var $main = '';      #load actMain
    var $pdf = '';       #load fpdf library
    var $nPungutan = ''; #pungutan
    var $car = '';       #Car
    var $hdr = '';       #data header dalam array 1D
    var $res = '';       #data header dalam array 1D
    var $dok = '';       #data dokumen dalam array 2D
    var $con = '';       #data container dalam array 2D
    var $conr = '';      #data container dalam array 2D
    var $kms = '';       #data kemasan dalam array 2D
    var $dtl = '';       #data detil dalam array 2D
    var $pgt = '';       #data pungutan dalam array 2D
    var $ntb = '';       #data ntb dalam array 1D
    var $npt = '';       #data ntp dalam array 1D
    var $ktr = '';       #data kantor kpbc dalam array 1D
    var $rpt = '';       #data caption respon NPP
    var $kd_trader = ''; #session data kode trader
    var $tp_trader = ''; #session data tipe trader
    var $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
    var $segmentUri = array(); #segment url

    function fpdf($pdf) {
        $this->pdf = $pdf;
    }
    
    function ciMain($main) {
        $this->main = $main;
    }
    
    function segmentUri($arr) {
        $this->segmentUri = $arr;
    }
    
    function showPage($car, $jnPage) {
        //print_r($jnPage);die();
        $this->car = $car;
        $this->kd_trader = $this->newsession->userdata('KODE_TRADER');
        $this->tp_trader = $this->newsession->userdata('TIPE_TRADER');
        $this->pdf->AddPage();
        switch ($jnPage) {
            case'1':
                $this->getHDR();
                $this->getDTL();
                $this->getPJT();
                $this->getKMS();
                $this->getDok();
                $this->getCON();
                $this->drawHeaderPEB();
                break;
            case'2':
                $this->getNTB();
                $this->drawSSPCP();
                break;
            case'4':
                $this->getHDR();
                $this->getDTL();
                $this->drawLembarCatPemerikasaanFisikBrg();
                break;
            case'10':case'15':case'17':case'60':case'70':case'900':  
                $this->getHDR();
                $this->getRESPEB(); 
                $this->isiRespon();
                $this->drawResponNPP();
                break;
            case'20';
                $this->getHDR();
                $this->getRESPEB(); 
                $this->isiRespon();
                $this->drawResponPPB();
                break;
            case'30';
                $this->getHDR();
                $this->getCON();
                $this->getRESPEB(); 
                $this->isiRespon();
                $this->drawResponNPE();
                break;
            case'40';
                $this->getHDR();
                $this->getCON();
                $this->getRESPEB(); 
                $this->isiRespon();
                $this->drawResponLPE();
                break;
            case'65';
                $this->getHDR();
                $this->getRESPEB(); 
                $this->isiRespon();
                $this->drawResponPNPKBE();
                break;
            case'90';
                $this->getHDR();
                $this->getDTL();
                $this->getRESPEB(); 
                $this->isiRespon();
                $this->drawResponSPPBK();
                break;
            case'470';
                $this->getHDR();
                $this->getRES();                
                $this->drawResponPembatalan(); 
                break;
            case'500';
                $this->getHDR();
                $this->getRES();                
                $this->drawResponSPKPBM(); 
                break;
            default :
                $this->pdf->SetFont('times', 'B', '12');
                $this->pdf->cell(131.6, 4, 'Tidak ada jenis laporan ini', 0, 0, 'R', 0);
                break;
        }
        $this->pdf->Output();
    }
    
    function getHDR() {
        $sql = "SELECT A.*, DATE_FORMAT(A.TGPPJK,'%d-%m-%Y') as 'TGPPJK',
                            B.URAIAN AS 'URJNEKS',
                            C.URAIAN AS 'URKATEKS',
                            D.URAIAN AS 'URJNDAG',
                            E.URAIAN AS 'URJNBYR',
                            F.URAIAN AS 'URTMP_PERIKSA',
                            G.URAIAN AS 'URKDASS',
                            H.URAIAN AS 'URJENIS_IDENTITAS',
                            X.URAIAN AS 'URIDQQ',
                            I.URAIAN AS 'URMODA',
                            J.URAIAN AS 'URDELIVERY',
                            K.URAIAN AS 'URKOMODITI',
                            L.URAIAN_NEGARA AS 'URNEGBELI',
                            M.URAIAN_NEGARA AS 'URANGKUTFL',
                            N.URAIAN_NEGARA AS 'URNEGTUJU',
                            O.URAIAN_PELABUHAN AS'URPELMUAT',
                            P.URAIAN_PELABUHAN AS'URPELMUATEKS',
                            Q.URAIAN_PELABUHAN AS'URPELTRANSIT',
                            R.URAIAN_PELABUHAN AS'URPELBONGKAR',
                            S.URAIAN_KPBC AS 'URKDKTR',
                            T.URAIAN_KPBC AS 'URKDKTRPRIKS',
                            U.URDAERAH AS 'URPROPBRG',
                            V.UREDI AS 'URKDVAL',
                            W.URHGR AS 'URKDHANGGAR'
                        FROM t_bc30hdr A
                            LEFT JOIN M_TABEL B ON B.MODUL = 'BC30' AND B.KDTAB = 'JNEKS' 	   AND B.KDREC = A.JNEKS
                            LEFT JOIN M_TABEL C ON C.MODUL = 'BC30' AND C.KDTAB = 'KATEKS' 	   AND C.KDREC = A.KATEKS
                            LEFT JOIN M_TABEL D ON D.MODUL = 'BC30' AND D.KDTAB = 'JNDAG' 	   AND D.KDREC = A.JNDAG
                            LEFT JOIN M_TABEL E ON E.MODUL = 'BC30' AND E.KDTAB = 'JNBYR' 	   AND E.KDREC = A.JNBYR
                            LEFT JOIN M_TABEL F ON F.MODUL = 'BC30' AND F.KDTAB = 'TMP_PERIKSA'    AND F.KDREC = A.KDLOKBRG
                            LEFT JOIN M_TABEL G ON G.MODUL = 'BC30' AND G.KDTAB = 'KDASS' 	   AND G.KDREC = A.KDASS
                            LEFT JOIN M_TABEL H ON H.MODUL = 'BC30' AND H.KDTAB = 'JENIS_IDENTITAS' AND H.KDREC = A.IDEKS
                            LEFT JOIN M_TABEL X ON X.MODUL = 'BC30' AND X.KDTAB = 'JENIS_IDENTITAS' AND X.KDREC = A.IDQQ
                            LEFT JOIN M_TABEL I ON I.MODUL = 'BC30' AND I.KDTAB = 'MODA' 	   AND I.KDREC = A.MODA
                            LEFT JOIN M_TABEL J ON J.MODUL = 'BC30' AND J.KDTAB = 'DELIVERY' 	   AND J.KDREC = A.DELIVERY
                            LEFT JOIN M_TABEL K ON K.MODUL = 'BC30' AND K.KDTAB = 'KOMODITI' 	   AND K.KDREC = A.KOMODITI
                            LEFT JOIN m_negara L ON L.KODE_NEGARA = A.NEGBELI
                            LEFT JOIN m_negara M ON M.KODE_NEGARA = A.ANGKUTFL
                            LEFT JOIN m_negara N ON N.KODE_NEGARA = A.NEGTUJU
                            LEFT JOIN m_pelabuhan O ON O.KODE_PELABUHAN = A.PELMUAT
                            LEFT JOIN m_pelabuhan P ON P.KODE_PELABUHAN = A.PELMUATEKS
                            LEFT JOIN m_pelabuhan Q ON Q.KODE_PELABUHAN = A.PELTRANSIT
                            LEFT JOIN m_pelabuhan R ON R.KODE_PELABUHAN = A.PELBONGKAR
                            LEFT JOIN m_kpbc S ON S.KDKPBC = A.KDKTR
                            LEFT JOIN m_kpbc T ON T.KDKPBC = A.KDKTRPRIKS
                            LEFT JOIN m_daerah U ON U.KDDAERAH = A.PROPBRG
                            LEFT JOIN m_valuta V ON V.KDEDI = A.KDVAL
                            LEFT JOIN m_hanggar W ON W.KDKPBC = A.KDKTR AND W.KDHGR = A.KDHANGGAR
                       WHERE A.CAR = '" . $this->car . "' AND A.KODE_TRADER = '" . $this->kd_trader . "'";
        $this->hdr = $this->main->get_result($sql);
        //print_r($this->hdr);die();
    } 
    
    function getDTL() {
        $sql = "SELECT A.*,
                            B.URAIAN AS 'URKDIZIN',
                            C.KODE_KEMASAN AS 'URJNKOLI',
                            D.URAIAN_NEGARA AS 'URNEGASAL',
                            E.UREDI AS 'URJNSATUAN',
                            F.UREDI AS 'URJNSATPE',
                            G.UREDI AS 'URKDVALPE'
                        FROM t_bc30dtl A
                            LEFT JOIN M_TABEL B ON B.MODUL = 'BC30' AND B.KDTAB = 'KDIZIN' 	   AND B.KDREC = A.KDIZIN
                            LEFT JOIN m_kemasan C ON C.KODE_KEMASAN = A.JNKOLI
                            LEFT JOIN m_negara D ON D.KODE_NEGARA = A.NEGASAL
                            LEFT JOIN m_satuan E ON E.KDEDI = A.JNSATUAN
                            LEFT JOIN m_satuan F ON F.KDEDI = A.JNSATPE
                            LEFT JOIN m_valuta G ON G.KDEDI = A.KDVALPE

                        WHERE CAR = '" . $this->car . "' AND KODE_TRADER = '" . $this->kd_trader . "'";
        $this->dtl = $this->main->get_result($sql, true);        
    }
    
    function getPJT() {
        $sql = "SELECT * FROM t_bc30pjt
                WHERE CAR = '" . $this->car . "' AND KODE_TRADER = '" . $this->kd_trader . "'";
        $this->pjt = $this->main->get_result($sql, true);        
    }      
    
    function getDok() {
        $sql = "SELECT A.*,DATE_FORMAT(A.TGDOK, '%d-%m-%Y') as 'TGDOK' , B.URAIAN AS 'URKDDOK' FROM t_bc30dok A
                LEFT JOIN m_tabel B ON B.MODUL='BC30' AND B.KDTAB='KDDOK' AND B.KDREC = A.KDDOK
                WHERE CAR = '" . $this->car . "' AND KODE_TRADER = '" . $this->kd_trader . "'"; 
        $this->dok = $this->main->get_result($sql, true);   
        //print_r($this->dok);die();
    }     
    
    function getKMS() {
        $sql = "SELECT * FROM t_bc30kms 
                WHERE CAR = '" . $this->car . "' AND KODE_TRADER = '" . $this->kd_trader . "'";
        $this->kms = $this->main->get_result($sql, true);   //die($sql);     
    }  
    
    function getCON() {
        $sql = "SELECT A.*, B.URAIAN AS 'URSTUFF' FROM t_bc30con A
                    LEFT JOIN M_TABEL B ON B.MODUL = 'BC30' AND B.KDTAB = 'STUFF' 	   AND B.KDREC = A.STUFF
                WHERE CAR = '" . $this->car . "' AND KODE_TRADER = '" . $this->kd_trader . "'";
        $this->con = $this->main->get_result($sql, true);   //die($sql);     
    } 
    
    function getRESPEB() {
        $sql = "SELECT *, B.URAIAN_KPBC AS 'URKDKTR'
                FROM   t_bc30respeb A
                LEFT JOIN M_KPBC B ON A.KDKTR = B.KDKPBC                
                WHERE  A.CAR = '" . $this->car . "' AND "
                . "    A.KODE_TRADER = '" . $this->kd_trader . "' AND "
                . "    A.RESKD = '" . $this->segmentUri[4] . "' AND "
                . "    A.RESTG = '" . $this->segmentUri[5] . "' AND "
                . "    A.RESWK = '" . $this->segmentUri[6] . "'"; //print($sql);die();
        $this->res = $this->main->get_result($sql);
        $this->getKTR($this->res['KDKTR']);
        //print_r($this->res);die();
    }
    
    function getNTB() {
        $query = "SELECT a.*,URAIAN_KPBC as 'URKPBC', DATE_FORMAT(TGDAFT, '%d-%m-%Y') AS 'TGDAFT', NODAFT
                  FROM   t_bc30ntb a 
                    Left Join m_kpbc b on a.KDKPBC = b.KDKPBC
                    left join t_bc30hdr c on a.KODE_TRADER = c.KODE_TRADER and a.CAR = c.CAR
                 WHERE  a.CAR = '" . $this->car . "' AND a.KODE_TRADER = " . $this->kd_trader;
        $ntb = $this->main->get_result($query);

        $query = "SELECT AKUN_SSPCP,NILAI FROM t_bc30ntbakun WHERE CAR = '" . $this->car . "' AND KODE_TRADER = " . $this->kd_trader;
        $arrakun = $this->actMain->get_result($query, true);
        foreach ($arrakun as $a) {
            $hasil[$a['AKUN_SSPCP']] = $a['NILAI'];
            $hasil['TOTAL'] += $a['NILAI'];
        }
        $this->ntb = $hasil + $ntb; 
    }    
    
    function getPGT() {
        $sql = "SELECT KDBEBAN,KDFASIL,NILBEBAN
                FROM  t_bc23pgt 
                WHERE t_bc23pgt.CAR = '" . $this->car . "' AND t_bc23pgt.KODE_TRADER = '" . $this->kd_trader . "'";
        $this->pgt = $this->main->get_result($sql, true);
        //print_r($this->pgt);die();
    }     
    
    function getKTR($kdKPBC){
        $SQL = "SELECT URAIAN_KPBC,KOTA,ESELON FROM m_kpbc WHERE KDKPBC = '".$kdKPBC."'";
        $this->ktr = $this->main->get_result($SQL);
        $this->ktr['KWBC'] = $this->main->get_uraian("SELECT URAIAN_KPBC FROM m_kpbc WHERE KDKPBC = '".$this->ktr['ESELON']."'",'URAIAN_KPBC');
        if($kdKPBC==='040300'){
            $this->ktr['KOP'] = "KEMENTERIAN KEUANGAN REPUBLIK INDONESIA\nDIREKTORAT JENDERAL BEA DAN CUKAI\nKANTOR PELAYANAN UTAMA TANJUNG PRIOK";
        }elseif($kdKPBC==='020400'){
            $this->ktr['KOP'] = "KEMENTERIAN KEUANGAN REPUBLIK INDONESIA\nDIREKTORAT JENDERAL BEA DAN CUKAI\nKANTOR PELAYANAN UTAMA BATAM";
        }else{
            $this->ktr['KOP'] = "KEMENTERIAN KEUANGAN REPUBLIK INDONESIA\nDIREKTORAT JENDERAL BEA DAN CUKAI\n" . (($this->ktr['KWBC']=='')?'':$this->ktr['KWBC']."\n"). str_replace('KPPBC', 'KANTOR PENGAWASAN DAN PELAYANAN',str_replace('KPU', 'KANTOR PELAYANAN UTAMA',strtoupper($this->ktr['URAIAN_KPBC'])));
        }
    }    
    
//PEB

    function drawHeaderPEB() {
        $this->pdf->Rect(4.3, 8, 201, 275, 1.5, 'F');
        $this->pdf->SetMargins(5.4, 0, 0);
        $this->pdf->SetAutoPageBreak(0, 0);
        $this->pdf->SetFont('times', 'B', '9');
        $this->pdf->SetXY(4.3, 8);
        
        #tulisan header
        $header = "http://192.168.5.22/t2g/img/header.png";
        $this->pdf->Image($header ,5.5,23,3);
        $this->pdf->cell(5.7, 48, '', 1, 0, 'C', 0);
        #
        #data perdagangan
        $f="http://192.168.5.22/t2g/img/dataperdagangan.png";
        $this->pdf->Image($f ,5.5,100,3);
        $this->pdf->SetXY(4.3, 56);
        $this->pdf->cell(5.7, 184, '', 1, 0, 'C', 0);
        #
        $this->pdf->SetXY(10,8);
        $this->pdf->cell(25, 5, 'BC 3.0', 1, 0, 'C', 0);
        $this->pdf->SetX(35);
        $this->pdf->cell(170.2, 5, 'PEMBERITAHUAN EKSPOR BARANG', 1, 0, 'C', 0);
        
        #ISI
        $this->pdf->Ln();
        $this->pdf->SetFont('times', '', '8');
        $this->pdf->SetX(105);
        $this->pdf->cell(170.2, 5, 'Halaman  '. $this->pdf->PageNo() . " dari {nb}", 0, 0, 'C', 0);
        $this->pdf->AliasNbPages();
        $this->pdf->Ln();
        $this->pdf->SetFont('times', 'B', '9');
        $this->pdf->SetX(10);
        $this->pdf->cell(35, 5, 'A. KANTOR PABEAN', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('times', '', '8');
        $this->pdf->SetX(14);
        $this->pdf->cell(36, 5, '1. Kantor Pabean Pemuatan', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['KDKTR'] .' '. $this->hdr['URKDKTR'], 0, 0, 'L', 0);
        $this->pdf->Ln(8);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->SetX(14);
        $this->pdf->cell(36, 5, '2. Nomor Pengajuan', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, substr($this->hdr['CAR'], 0, 6) . " - " . substr($this->hdr['CAR'], 6, 6) . " - " . substr($this->hdr['CAR'], 12, 8) . " - " . substr($this->hdr['CAR'], 20) , 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('times', 'B', '9');
        $this->pdf->SetX(10);
        $this->pdf->cell(40, 5, 'B. JENIS EKSPOR', 0, 0, 'L', 0);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['URJNEKS'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('times', 'B', '9');
        $this->pdf->SetX(10);
        $this->pdf->cell(40, 5, 'C. KATEGORI EKSPOR', 0, 0, 'L', 0);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['URKATEKS'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('times', 'B', '9');
        $this->pdf->SetX(10);
        $this->pdf->cell(40, 5, 'D. CARA PERDAGANGAN', 0, 0, 'L', 0);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['URJNDAG'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('times', 'B', '9');
        $this->pdf->SetX(10);
        $this->pdf->cell(40, 5, 'E. CARA PEMBAYARAN', 0, 0, 'L', 0);
        $this->pdf->SetFont('times', '', '9');  
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['URJNBYR'], 0, 0, 'L', 0);
        #KOTAK
        $this->pdf->Ln(4.9);
        $this->pdf->SetX(10);
        $this->pdf->SetFont('times', 'B', '9');  
        $this->pdf->cell(105, 3,'EKSPORTIR', 1, 0, 'L', 0);
        #
        $this->pdf->Ln();
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->SetX(10);
        $this->pdf->cell(15, 5, '1. Identitas', 0, 0, 'L', 0);
        $this->pdf->SetFont('times', '', '9');  
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['URJENIS_IDENTITAS'].' '. $this->hdr['NPWPEKS'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetX(27);
        $this->pdf->cell(35, 5, 'QQ '.$this->hdr['URIDQQ'].' '. $this->hdr['NPWPQQ'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->SetX(10);
        $this->pdf->cell(15, 5, '2. Nama', 0, 0, 'L', 0);
        $this->pdf->SetFont('times', '', '9');  
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['NAMAEKS'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetX(27);
        $this->pdf->cell(35, 5, 'QQ '.$this->hdr['NAMAQQ'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->SetX(10);
        $this->pdf->cell(15, 5, '3. Alamat', 0, 0, 'L', 0);
        $this->pdf->SetFont('times', '', '9');  
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['ALMTEKS'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetX(27);
        $this->pdf->cell(35, 5, 'QQ '.$this->hdr['ALMTQQ'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->SetX(10);
        $this->pdf->cell(15, 5, '4. NIPER', 0, 0, 'L', 0);
        $this->pdf->SetFont('times', '', '9');  
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['NIPER'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->SetX(10);
        $this->pdf->cell(28, 5, '5. Status', 0, 0, 'L', 0);
        $this->pdf->SetFont('times', '', '9');  
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['STATUSH'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->SetX(10);
        $this->pdf->cell(28, 5, '6. No. & Tgl. TDP', 0, 0, 'L', 0);
        $this->pdf->SetFont('times', '', '9');  
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['NOTDP'].'     '.$this->hdr['TGTDP'], 0, 0, 'L', 0);
        #KOTAK
        $this->pdf->Ln(4.9);
        $this->pdf->SetX(10);
        $this->pdf->SetFont('times', 'B', '9');  
        $this->pdf->cell(105, 3,'DATA PENGANGKUT', 1, 0, 'L', 0);
        #
        $this->pdf->Ln();
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->SetX(10);
        $this->pdf->cell(42, 5, '13. Cara Pengangkutan', 0, 0, 'L', 0);
        $this->pdf->SetFont('times', '', '9');  
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['URMODA'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->SetX(10);
        $this->pdf->cell(42, 5, '14. Nama Sarana Pengangkut', 0, 0, 'L', 0);
        $this->pdf->SetFont('times', '', '9');  
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['ANGKUTNAMA'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->SetX(10);
        $this->pdf->cell(42, 5, '15. No. PengangkutP', 0, 0, 'L', 0);
        $this->pdf->SetFont('times', '', '9');  
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['ANGKUTNO'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->SetX(10);
        $this->pdf->cell(42, 5, '17. Bendera Sarana Pengangkut', 0, 0, 'L', 0);
        $this->pdf->SetFont('times', '', '9');  
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['URANGKUTFL'], 0, 0, 'L', 0);
        #TULISAN DALAM KOTAK
        $this->pdf->Ln(3.5);
        $this->pdf->Rect(10, 122.5, 105    , 3.5, 1.5, 'F');
        #
        #KOTAK
        $this->pdf->Ln(0.7);
        $this->pdf->SetX(10);
        $this->pdf->SetFont('times', 'B', '9');  
        $this->pdf->cell(105, 3,'DOKUMEN PELENGKAP PABEAN', 1, 0, 'L', 0);
        #
        $this->pdf->Ln();
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->SetX(10);
        $this->pdf->cell(42, 5, '22. Nomor & Tgl Invoice', 0, 0, 'L', 0);
        $this->pdf->SetFont('times', '', '9');  
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['URJNBYR'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->SetX(10);
        $this->pdf->cell(60, 5, '23. Jenis/Nomor/Tgl Dok Pelengkap Pabean', 0, 0, 'L', 0);
        $this->pdf->SetFont('times', '', '9');  
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetX(10);
        $this->pdf->cell(35, 5, $this->dok['URKDDOK'].' / '.$this->dok['NODOK'].' / '.$this->dok['TGDOK'], 0, 0, 'L', 0);
        #TULISAN DALAM KOTAK
        $this->pdf->Ln(3.5);
        $this->pdf->Rect(10, 146.5, 105, 3.5, 1.5, 'F');
        #
        $this->pdf->Ln(3.4);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->SetX(10);
        $this->pdf->cell(42, 5, '27. Negara Tujuan Ekspor', 0, 0, 'L', 0);
        $this->pdf->SetFont('times', '', '9');  
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['URNEGBELI'], 0, 0, 'L', 0);
        #KOTAK
        $this->pdf->Ln(4.1);
        $this->pdf->SetX(10);
        $this->pdf->SetFont('times', 'B', '9');  
        $this->pdf->cell(105, 3,'DATA TRANSAKSI EKSPOR', 1, 0, 'L', 0);
        #
        $this->pdf->Ln(3.4);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->SetX(10);
        $this->pdf->cell(42, 5, '29. Bank Devisa Hasil Ekspor', 0, 0, 'L', 0);
        $this->pdf->SetFont('times', '', '9');  
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['URJNBYR'], 0, 0, 'L', 0);
        $this->pdf->Ln(3.4);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->SetX(10);
        $this->pdf->cell(42, 5, '30. Jenis Valuta Asing', 0, 0, 'L', 0);
        $this->pdf->SetFont('times', '', '9');  
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['URJNBYR'], 0, 0, 'L', 0);
        #KOTAK
        $this->pdf->Ln(9);
        $this->pdf->SetX(10);
        $this->pdf->SetFont('times', 'B', '9');  
        $this->pdf->cell(105, 3,'DATA PETI KEMAS', 1, 0, 'L', 0);
        #
        //PRINT_R($this->con);DIE();
        $this->pdf->Ln();
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->SetX(10);
        $this->pdf->cell(45, 5, '34. Peti Kemas', 0, 0, 'L', 0);
        $this->pdf->SetFont('times', '', '9');  
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        if(count($this->con) != 0){
            $this->pdf->cell(35, 5, 'Ya', 0, 0, 'L', 0);
        }
        $this->pdf->Ln();
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->SetX(10);
        $this->pdf->cell(45, 5, '35. Status Peti Kemas', 0, 0, 'L', 0);
        $this->pdf->SetFont('times', '', '9');  
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->con[0]['URSTUFF'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->SetX(10);
        $this->pdf->cell(45, 5, '36. Jumlah Peti Kemas', 0, 0, 'L', 0);
        $this->pdf->SetFont('times', '', '9');  
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->con[0]['SIZE'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->SetX(10);
        $this->pdf->cell(45, 5, '37. Merek dan Nomor Peti Kemas', 0, 0, 'L', 0);
        $this->pdf->SetFont('times', '', '9');  
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, substr($this->con[0]['NOCONT'], 0,4).' - '.substr($this->con[0]['NOCONT'], 4), 0, 0, 'L', 0);
        
        #KOTAK
        $this->pdf->Ln();
        $this->pdf->SetX(10);
        $this->pdf->SetFont('times', 'B', '9');  
        $this->pdf->cell(195.3, 3,'DATA BARANG EKSPOR', 1, 0, 'L', 0);
        #
        #ini yang tiga nempel
        #KOTAK
        $this->pdf->Ln(3);
        $this->pdf->SetX(10);
        $this->pdf->SetFont('times', '', '9');  
        $this->pdf->cell(66, 3,'41. Volume            ' . $this->hdr['URJNBYR'], 1, 0, 'L', 0);
        $this->pdf->cell(63.3, 3,'41. Berat Kotor(kg)            ' . $this->hdr['URJNBYR'], 1, 0, 'L', 0);
        $this->pdf->cell(66, 3,'41. Berat Bersih(kg)            ' . $this->hdr['URJNBYR'], 1, 0, 'L', 0);
        #
        
        #tabel yang di bawah
        #KOTAK
        $this->pdf->Ln(8);
        $this->pdf->SetXY(10,197);  
        $this->pdf->multicell(10, 7.5, "44. No", 1, 'J');
        $this->pdf->SetXY(20,197);
        $this->pdf->multicell(60, 5, "45. Pos Tarif/HS, uraian jumlah dan barang secara lengkap, merk, tipe ukuran, spesifikasi lain dan kode", 1, 'J');
        $this->pdf->SetXY(80,197);
        $this->pdf->multicell(32, 7.5, "46. HE barang dan Tarif BK pada tanggal", 1, 'J');
        $this->pdf->SetXY(112,197);
        $this->pdf->multicell(35, 5, "47. Jumlah & jenis sat, Berat Bersih Volume (m3)", 1, 'J');
        $this->pdf->SetXY(147,197);
        $this->pdf->multicell(35, 7.5, "48. -Perizinan Ekspor \n -Negara Asal Barang", 1, 'L');
        $this->pdf->SetXY(182,197);
        $this->pdf->multicell(23.2, 15, "49. Jumlah FOB", 1, 'L');
        #
        #KOTAK
        
        if ($this->hdr['JMBRG']) {
            $lampiran['detil'] = '1';
            $this->pdf->SetXY(10,212);
            $this->pdf->SetFont('times', 'B', '9');  
            $this->pdf->cell(195.3, 10,'== '.count($this->dtl) . ' item barang. Lihat Lembar Lanjutan'.' ==', 1, 0, 'C', 0);
        }
        #
        $this->pdf->Ln();
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->SetX(10);
        $this->pdf->cell(42, 5, '22. Nomor & Tgl Invoice', 0, 0, 'L', 0);
        $this->pdf->SetFont('times', '', '9');  
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['URJNBYR'], 0, 0, 'L', 0);
        
        #TULISAN DALAM KOTAK
        $this->pdf->Ln(3.5);
        $this->pdf->Rect(4.3, 240, 110.7    , 43, 1.5, 'F');
        #KOTAK
        $this->pdf->Ln(15);
        $this->pdf->SetFont('times', 'B', '9');
        $this->pdf->SetX(10);
        $this->pdf->cell(45, 5, 'G. TANDA TANGAN EKSPORTIR / PPJK', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->SetX(10);
        $this->pdf->multicell(100, 5, "Dengan ini saya menyatakan bertanggung jawab atas kebenaran yang  diberitahukan dalam Pemberitahuan Ekspor Barang ini", 0, 'L');
        $this->pdf->Ln(2);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->SetX(10);
        $this->pdf->cell(90, 5, 'JAKARTA, ' . date('m-d-Y'), 0, 0, 'C', 0);
        $this->pdf->Ln(3);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->SetX(10);
        $this->pdf->cell(90, 5, 'PPJK', 0, 0, 'C', 0);
        $this->pdf->Ln(17);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->SetX(10);
        $this->pdf->cell(90, 5, '-'.$this->hdr['URJNBYR'].'-', 0, 0, 'C', 0);
        
        
        
        #line
        $this->pdf->Line(205,55.9,4.3,55.9);
        #line
        
        
        #KOTAK SEBELAH KANAN
        $this->pdf->Rect(115, 27, 90.3, 115.8, 1.5, 'F');
        #
        $this->pdf->SetFont('times', 'B', '9');
        $this->pdf->SetXY(115,26.5);
        $this->pdf->cell(58, 5, 'H. KOLOM KHUSUS BEA DAN CUKAI', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->SetX(115);
        $this->pdf->SetFont('times', '', '9'); 
        $this->pdf->cell(30, 5, '1. Nomor Pendaftaran', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['NODAFT'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetX(118);
        $this->pdf->cell(27, 5, 'Tanggal', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['TGDAFT'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetX(115);
        $this->pdf->SetFont('times', '', '9'); 
        $this->pdf->cell(30, 5, '2. Nomor BC 1.1', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['NOBC11'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetX(118);
        $this->pdf->cell(27, 5, 'Tanggal', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['TGBC11'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetX(118);
        $this->pdf->cell(27, 5, 'Pos / Sub Pos', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['POSBC11'], 0, 0, 'L', 0);
        #KOTAK
        $this->pdf->Ln(4.4);
        $this->pdf->SetX(115);
        $this->pdf->SetFont('times', 'B', '9');  
        $this->pdf->cell(90.3, 3,'PENERIMA', 1, 0, 'L', 0);
        #
        $this->pdf->Ln();
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->SetX(115);
        $this->pdf->SetFont('times', '', '9'); 
        $this->pdf->cell(30, 5, '7. Nama', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['NAMABELI'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetX(115);
        $this->pdf->SetFont('times', '', '9'); 
        $this->pdf->cell(30, 5, '8. Alamat', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['URNEGBELI'], 0, 0, 'L', 0);
        $this->pdf->Ln(8);
        $this->pdf->SetX(198);
        $this->pdf->cell(35, 5, $this->hdr['NEGBELI'], 0, 0, 'L', 0);
        #KOTAK
        $this->pdf->Ln(4);
        $this->pdf->SetX(115);
        $this->pdf->SetFont('times', 'B', '9');  
        $this->pdf->cell(90.3, 3,'PPJK', 1, 0, 'L', 0);
        #
        $this->pdf->Ln(5);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->SetX(115);
        $this->pdf->SetFont('times', '', '9'); 
        $this->pdf->cell(30, 5, '9. NPWP', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['NPWPPPJK'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetX(115);
        $this->pdf->SetFont('times', '', '9'); 
        $this->pdf->cell(30, 5, '10. Nama', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['NAMAPPJK'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetX(115);
        $this->pdf->SetFont('times', '', '9'); 
        $this->pdf->cell(30, 5, '11. Alamat', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['ALMTPPJK'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetX(115);
        $this->pdf->SetFont('times', '', '9'); 
        $this->pdf->cell(30, 5, '12. Nomor Poko PPJK', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['NOPPJK'], 0, 0, 'L', 0);
        #KOTAK
        $this->pdf->Ln(7.9);
        $this->pdf->SetX(115);
        $this->pdf->SetFont('times', 'B', '9');  
        $this->pdf->cell(90.3, 3,'DATA PELABUHAN/TEMPAT MUAT EKSPOR', 1, 0, 'L', 0);
        #
        $this->pdf->Ln();
        $this->pdf->SetX(115);
        $this->pdf->SetFont('times', '', '9'); 
        $this->pdf->cell(48, 5, '18. Pelabuhan Muat Asal', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['URPELMUAT'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetX(115);
        $this->pdf->SetFont('times', '', '9'); 
        $this->pdf->cell(48, 5, '19. Pelabuhan/Tempat Muat Ekspor', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['URPELMUATEKS'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetX(115);
        $this->pdf->SetFont('times', '', '9'); 
        $this->pdf->cell(48, 5, '20. Plabuhan Transit LN', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['URPELTRANSIT'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetX(115);
        $this->pdf->SetFont('times', '', '9'); 
        $this->pdf->cell(48, 5, '21. Pelabuhan Bongkar', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['URPELBONGKAR'], 0, 0, 'L', 0);
        #KOTAK
        $this->pdf->Ln();
        $this->pdf->SetX(115);
        $this->pdf->SetFont('times', 'B', '9');  
        $this->pdf->cell(90.3, 3,'DATA TEMPAT PEMERIKSAAN', 1, 0, 'L', 0);
        #
        $this->pdf->Ln();
        $this->pdf->SetX(115);
        $this->pdf->SetFont('times', '', '9'); 
        $this->pdf->cell(48, 5, '24. Lokasi Pemeriksaan', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['URKDLOKBRG'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetX(115);
        $this->pdf->SetFont('times', '', '9'); 
        $this->pdf->cell(48, 5, '25. Kantor Pabean Pemeriksaan', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['URKDKTRPRIKS'], 0, 0, 'L', 0);
        #KOTAK
        $this->pdf->Ln();
        $this->pdf->SetX(115);
        $this->pdf->SetFont('times', 'B', '9');  
        $this->pdf->cell(90.3, 3,'DATA PERDAGANGAN', 1, 0, 'L', 0);
        #
        
        #TULISAN DALAM KOTAK
        $this->pdf->SetFont('times', '', '9');  
        $this->pdf->Ln(2.3);
        $this->pdf->Rect(115, 142.9, 90.3    , 3.5, 1.5, 'F');
        $this->pdf->SetX(115);
        $this->pdf->cell(38, 5, '26. Daerah Asal Barang', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->hdr['URPROPBRG'], 0, 0, 'L', 0);
        #
        #TULISAN DALAM KOTAK
        $this->pdf->Ln(3.5);
        $this->pdf->Rect(115, 146.5, 90.3    , 3.5, 1.5, 'F');
        $this->pdf->SetX(115);
        $this->pdf->cell(38, 5, '28. Cara Penyerahan Barang', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(36, 5, $this->hdr['URDELIVERY'], 0, 0, 'R', 0);
        #
        $this->pdf->Ln();
        $this->pdf->SetX(115);
        $this->pdf->SetFont('times', '', '9'); 
        $this->pdf->cell(30, 5, '31. Freight', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(57.5, 5, $this->hdr['FREIGHT'], 0, 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->SetX(115);
        $this->pdf->SetFont('times', '', '9'); 
        $this->pdf->cell(30, 5, '32. Asuransi(LN/DN)', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(57.5, 5, $this->hdr['ASURANSI'], 0, 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->SetX(115);
        $this->pdf->SetFont('times', '', '9'); 
        $this->pdf->cell(30, 5, '33. FOB', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(57.5, 5, $this->hdr['FOB'], 0, 0, 'R', 0);
        #KOTAK
        $this->pdf->Ln(5.2);
        $this->pdf->SetX(115);
        $this->pdf->SetFont('times', 'B', '9');  
        $this->pdf->cell(90.3, 3,'DATA KEMASAN', 1, 0, 'L', 0);
        #
        $this->pdf->Ln();
        $this->pdf->SetX(115);
        $this->pdf->SetFont('times', '', '9'); 
        $this->pdf->cell(30, 5, '38. Jenis Kemasan', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->kms[0]['JNKEMAS'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetX(115);
        $this->pdf->SetFont('times', '', '9'); 
        $this->pdf->cell(30, 5, '39. Merek Kemasan', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->kms[0]['MERKKEMAS'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetX(115);
        $this->pdf->SetFont('times', '', '9'); 
        $this->pdf->cell(30, 5, '40. Jumlah Kemasan', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, $this->kms[0]['JMKEMAS'], 0, 0, 'L', 0);
        #KOTAK
        $this->pdf->Ln(43.2);
        $this->pdf->SetX(115);
        $this->pdf->SetFont('times', 'B', '9');  
        $this->pdf->cell(90.3, 3,'DATA PENERIMAAN NEGARA', 1, 0, 'L', 0);
        #
         $this->pdf->Ln();
        $this->pdf->SetX(115);
        $this->pdf->SetFont('times', '', '9'); 
        $this->pdf->cell(40, 5, '51. Nilai BK dalam Rupiah', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(48, 5, $this->hdr['NILPE'], 0, 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->SetX(115);
        $this->pdf->SetFont('times', '', '9'); 
        $this->pdf->cell(40, 5, '52. PNBP', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(48, 5, $this->hdr['PNBP'], 0, 0, 'R', 0);
        #TULISAN DALAM KOTAK
        $this->pdf->Ln();
        $this->pdf->Rect(115, 240, 90.3    , 43, 1.5, 'F');
        $this->pdf->SetX(115);
        #
        $this->pdf->Ln(5.2);
        $this->pdf->SetX(115);
        $this->pdf->SetFont('times', 'B', '9');  
        $this->pdf->cell(90.3, 3,'I. BUKTI PEMBAYARAN', 0, 0, 'L', 0);
        $this->pdf->Ln(2);
        $this->pdf->SetFont('times', '', '9');  
        $this->pdf->Rect(115, 240, 90.3    , 43, 1.5, 'F');
        $this->pdf->SetX(115);
        $this->pdf->cell(10, 5, 'SSPCP', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        
        $this->pdf->Ln(2);
        $this->pdf->SetXY(118,247); 
        $this->pdf->cell(13, 8,'Jen. Pen', 1, 0, 'L', 0);
        $this->pdf->SetXY(131,247); 
        $this->pdf->cell(36, 4,'NTB/NTP', 1, 0, 'C', 0);
        $this->pdf->SetXY(131,251); 
        $this->pdf->cell(18, 4,'Nomor', 1, 0, 'C', 0);
        $this->pdf->SetXY(149,251); 
        $this->pdf->cell(18, 4,'Tgl', 1, 0, 'C', 0);
        $this->pdf->SetXY(167,247); 
        $this->pdf->cell(36, 4,'NTPN', 1, 0, 'C', 0);
        $this->pdf->SetXY(167,251); 
        $this->pdf->cell(18, 4,'Nomor', 1, 0, 'C', 0);
        $this->pdf->SetXY(185,251); 
        $this->pdf->cell(18, 4,'Tgl', 1, 0, 'C', 0);
        $this->pdf->SetXY(118,255); 
        $this->pdf->SetFont('times', '', '8');  
        $this->pdf->cell(13, 8,'BK PNBP', 1, 0, 'L', 0);
        $this->pdf->SetXY(131,255);
        $this->pdf->cell(18, 8,'', 1, 0, 'C', 0);
        $this->pdf->SetXY(149,255);
        $this->pdf->cell(18, 8,'', 1, 0, 'C', 0);
        $this->pdf->SetXY(167,255);
        $this->pdf->cell(18, 8,'', 1, 0, 'C', 0);
        $this->pdf->SetXY(185,255);
        $this->pdf->cell(18, 8,'', 1, 0, 'C', 0);
        $this->pdf->Ln(8);
        $this->pdf->SetX(118);
        $this->pdf->SetFont('times', '', '9');  
        $this->pdf->cell(90.3, 3,'Pejabat Penerima', 0, 0, 'L', 0);
        $this->pdf->SetX(167);  
        $this->pdf->cell(90.3, 3,'Nama/Stempel Instansi', 0, 0, 'L', 0);
        $this->pdf->Ln(15);
        $this->pdf->SetX(119);
        $this->pdf->SetFont('times', '', '9');  
        $this->pdf->cell(90.3, 3,'........................', 0, 0, 'L', 0);
        
        #FOOTER
        
        $this->pdf->SetXY(5,285);
        $this->pdf->SetFont('times', '', '9'); 
        $this->pdf->cell(30, 5, 'Sesuai Lampiran I PER-18/BC/2012', 0, 0, 'L', 0);
        $this->pdf->SetXY(98,285);
        $this->pdf->cell(35, 5, date('m/d/Y'), 0, 0, 'L', 0);
        $this->pdf->SetXY(155,285);
        $this->pdf->cell(35, 5, 'Lembar ke-'.'untuk KPBC/BPS/BI', 0, 0, 'L', 0);
        #
        
        
        
        if ($lampiran['detil'] == '1') {
            $this->dataLampiranBarang();
        }
        if (count($this->pjt) > 0) {
            $this->dataLampiranPJT();
        }
        if (count($this->dok) > 0) {
            $this->dataLampiranDok();
        }
    }    
    
    function headerLampiran($tipe, $lamp) {
        
        $this->pdf->AddPage();
        $this->pdf->SetY(278);
        $this->pdf->SetFont('times', 'I', '8');
        $this->pdf->cell(45, 5, 'Tgl. Cetak '. date('d-m-Y'), 0, 0, 'L', 0);
        $this->pdf->SetXY(150,278);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->cell(45, 5, $this->hdr['NAMA_TTD'], 0, 0, 'C', 0);
        
        switch ($tipe) {
            case 'barang':
                $this->pdf->SetMargins(5.4, 0, 0);
                $this->pdf->SetAutoPageBreak(0, 0);
                $this->pdf->SetFont('times', 'B', '9');
                $this->pdf->Ln();
                $this->pdf->Rect(4.3, 8, 201, 10, 1.5, 'F');
                $this->pdf->SetXY(55, 8);
                $this->pdf->multicell(100, 5, "LEMBAR LANJUTAN \n PEMBERITAHUAN EKSPOR BARANG (PEB)", 0, 'C');
                $this->pdf->SetXY(175, 13);
                $this->pdf->SetFont('times', '', '9');
                $this->pdf->cell(20, 5, "Halaman  ". $this->pdf->PageNo() . " dari {nb}", 0, 0, 'C', 0);
                $this->pdf->Ln();
                $this->pdf->SetX(4.3);
                $this->pdf->cell(45, 5, 'Kantor Pelayanan Bea dan Cukai', 0, 0, 'L', 0);
                $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
                $this->pdf->cell(35, 5, $this->hdr['URKDKTRPRIKS'], 0, 0, 'L', 0);
                $this->pdf->SetX(140);
                $this->pdf->cell(15, 5, $this->hdr['KDKTRPRIKS'], 1, 0, 'C', 0);
                $this->pdf->Ln();
                $this->pdf->SetX(4.3);
                $this->pdf->cell(45, 5, 'Nomor Pengajuan', 0, 0, 'L', 0);
                $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
                $this->pdf->cell(35, 5, $this->hdr['CAR'], 0, 0, 'L', 0);
                $this->pdf->Ln();
                $this->pdf->SetX(4.3);
                $this->pdf->cell(45, 5, 'Nomor Pendaftaran', 0, 0, 'L', 0);
                $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
                $this->pdf->cell(35, 5, $this->hdr['URKDKTRPRIKS'], 0, 0, 'L', 0);
                #footer
                $this->pdf->SetXY(150,253);
                $this->pdf->cell(45, 5, 'JAKARTA, '.date('d-m-Y'), 0, 0, 'C', 0);
                $this->pdf->Ln();
                $this->pdf->SetXY(150,258);
                $this->pdf->cell(45, 5, 'P P J K', 0, 0, 'C', 0);
            
                #tabel yang di bawah
                #KOTAK
                $this->pdf->Ln(8);
                $this->pdf->SetXY(10,34);  
                $this->pdf->multicell(10, 7.5, "44. No", 1, 'J');
                $this->pdf->SetXY(20,34);
                $this->pdf->multicell(60, 5, "45. Pos Tarif/HS, uraian jumlah dan barang secara lengkap, merk, tipe ukuran, spesifikasi lain dan kode", 1, 'J');
                $this->pdf->SetXY(80,34);
                $this->pdf->multicell(32, 7.5, "46. HE barang dan Tarif BK pada tanggal", 1, 'J');
                $this->pdf->SetXY(112,34);
                $this->pdf->multicell(35, 5, "47. Jumlah & jenis sat, Berat Bersih Volume (m3)", 1, 'J');
                $this->pdf->SetXY(147,34);
                $this->pdf->multicell(35, 7.5, "48. -Perizinan Ekspor \n -Negara Asal Barang", 1, 'L');
                $this->pdf->SetXY(182,34);
                $this->pdf->multicell(23.2, 15, "49. Jumlah FOB", 1, 'L');
                break;
            case 'pjt':
                $this->pdf->Rect(4.3, 8, 201, 275, 1.5, 'F');
                $this->pdf->SetMargins(5.4, 0, 0);
                $this->pdf->SetAutoPageBreak(0, 0);
                $this->pdf->SetFont('times', 'B', '9');
                $this->pdf->Ln();
                $this->pdf->Rect(4.3, 8, 201, 10, 1.5, 'F');
                $this->pdf->SetXY(30, 8);
                $this->pdf->multicell(150, 5, "LEMBAR LANJUTAN KHUSUS PERUSAHAANJASA TITIPAN (PJT)\n PEMBERITAHUAN EKSPOR BARANG (PEB)", 0, 'C');
                $this->pdf->SetXY(175, 13);
                $this->pdf->SetFont('times', '', '9');
                $this->pdf->cell(20, 5, "Halaman  ". $this->pdf->PageNo() . " dari {nb}", 0, 0, 'C', 0);
                $this->pdf->Ln();
                $this->pdf->SetX(4.3);
                $this->pdf->cell(45, 5, 'Kantor Pelayanan Bea dan Cukai', 0, 0, 'L', 0);
                $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
                $this->pdf->cell(35, 5, $this->hdr['URKDKTRPRIKS'], 0, 0, 'L', 0);
                $this->pdf->SetX(140);
                $this->pdf->cell(15, 5, $this->hdr['KDKTRPRIKS'], 1, 0, 'C', 0);
                $this->pdf->Ln();
                $this->pdf->SetX(4.3);
                $this->pdf->cell(45, 5, 'Nomor Pengajuan', 0, 0, 'L', 0);
                $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
                $this->pdf->cell(35, 5, $this->hdr['CAR'], 0, 0, 'L', 0);
                $this->pdf->Ln();
                $this->pdf->SetX(4.3);
                $this->pdf->cell(45, 5, 'Nomor Pendaftaran', 0, 0, 'L', 0);
                $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
                $this->pdf->cell(35, 5, $this->hdr['URKDKTRPRIKS'], 0, 0, 'L', 0);
                #tabel yang di bawah
                #KOTAK
                $this->pdf->Ln(8);
                $this->pdf->SetXY(10,34);  
                $this->pdf->multicell(10, 35, "No", 1, 'J');
                $this->pdf->SetXY(20,34);
                $this->pdf->multicell(40, 5, "Pengirim \n -identitas \n -nama \n -alamat \n Penerima \n -nama \n -alamat", 1, 'J');
                $this->pdf->SetXY(60,34);
                $this->pdf->multicell(48, 7, "45. Pos Tarif/HS, uraian jumlah dan barang secara lengkap, merk, tipe ukuran, spesifikasi lain dan kode \n 46. HE barang dan Tarif BK pada tanggal", 1, 'J');
                $this->pdf->SetXY(108,34);
                $this->pdf->multicell(40, 7, "47. Jumlah & jenis sat, Berat Bersih Volume (m3) \n Kemasan \n -jumlah \n -jenis", 1, 'J');
                $this->pdf->SetXY(148,34);
                $this->pdf->multicell(34, 17.5, "48. -Perizinan Ekspor \n -Negara Asal Barang", 1, 'T');
                $this->pdf->SetXY(182,34);
                $this->pdf->multicell(23.2, 35, "49. Jumlah FOB", 1, 'L');
                
                #footer
                $this->pdf->SetXY(150,253);
                $this->pdf->cell(45, 5, 'JAKARTA, '.date('d-m-Y'),0, 0, 'C', 0);
                $this->pdf->Ln();
                $this->pdf->SetXY(150,258);
                $this->pdf->cell(45, 5, 'P P J K', 0, 0, 'C', 0);
                $this->pdf->SetY(69);
                break;
            case 'dokumen':
                #footer
                $this->pdf->SetXY(150,253);
                $this->pdf->cell(45, 5, 'JAKARTA, '.date('d-m-Y'),0, 0, 'C', 0);
                $this->pdf->SetMargins(5.4, 0, 0);
                $this->pdf->SetAutoPageBreak(0, 0);
                $this->pdf->SetFont('times', 'B', '9');
                $this->pdf->Ln();
                $this->pdf->Rect(4.3, 8, 201, 10, 1.5, 'F');
                $this->pdf->SetXY(30, 8);
                $this->pdf->multicell(150, 5, "LEMBAR LANJUTAN DOKUMEN PELENGKAP PABEAN\n PEMBERITAHUAN EKSPOR BARANG (PEB)", 0, 'C');
                $this->pdf->SetXY(175, 13);
                $this->pdf->SetFont('times', '', '9');
                $this->pdf->cell(20, 5, "Halaman  ". $this->pdf->PageNo() . " dari {nb}", 0, 0, 'C', 0);
                $this->pdf->Ln();
                $this->pdf->SetX(4.3);
                $this->pdf->cell(45, 5, 'Kantor Pelayanan Bea dan Cukai', 0, 0, 'L', 0);
                $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
                $this->pdf->cell(35, 5, $this->hdr['URKDKTRPRIKS'], 0, 0, 'L', 0);
                $this->pdf->SetX(140);
                $this->pdf->cell(15, 5, $this->hdr['KDKTRPRIKS'], 1, 0, 'C', 0);
                $this->pdf->Ln();
                $this->pdf->SetX(4.3);
                $this->pdf->cell(45, 5, 'Nomor Pengajuan', 0, 0, 'L', 0);
                $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
                $this->pdf->cell(35, 5, $this->hdr['CAR'], 0, 0, 'L', 0);;
                #tabel yang di bawah
                #KOTAK
                $this->pdf->Ln(8);
                $this->pdf->SetXY(4.3,34);  
                $this->pdf->multicell(20, 10, '', 1, 'J');
                $this->pdf->SetXY(24.3,34);
                $this->pdf->multicell(15, 10, "No", 1, 'C');
                $this->pdf->SetXY(39.3,34);
                $this->pdf->multicell(50, 10, 'Jenis Dokumen', 1, 'L');
                $this->pdf->SetXY(89.4,34);
                $this->pdf->multicell(55, 10, 'Nomor Dokumen', 1, 'C');
                $this->pdf->SetXY(144.4,34);
                $this->pdf->multicell(60.9, 10, 'Tanggal Dokumen', 1, 'C');
                
                break;
            
        }
    }    
    
    function dataLampiranBarang($idPrint = 0) {
        $this->headerLampiran('barang', 'LANJUTAN BARANG');
        $this->pdf->SetFont('times', '', '8');
        $Yawl = $this->pdf->getY();
        $Y = $this->pdf->getY(); //print_r($this->dtl);die();
        $totData = count($this->dtl);
        #ISI TABEL
        for ($i = $idPrint; $i < $totData; $i++) {
            $h = '';
            $this->pdf->setY($Yawl);
            #no 
            $this->pdf->setXY($this->pdf->getX() + 4.3, $Yawl);
            $this->pdf->cell(10, 4, ($i + 1), 0, 0, 'C', 0);
            #45
            $this->pdf->multicell(60, 4, $this->dtl[$i]['HS']."\n".$this->dtl[$i]['URBRG1']."\n".$this->dtl[$i]['DMERK'].'/'.$this->dtl[$i]['SIZE'].'/'.$this->dtl[$i]['TYPE'].'/'.$this->dtl[$i]['KDBRG'], 0, 'L');
            $h[] = $this->pdf->getY();
            #46
            $this->pdf->setXY($this->pdf->getX() + 74.6, $Yawl);
            $this->pdf->multicell(32, 4, '', 0, 'L');
            $h[] = $this->pdf->getY();
            #47
            $this->pdf->setXY($this->pdf->getX() + 106.6, $Yawl);
            $this->pdf->multicell(35, 4, $this->dtl[$i]['JMSATUAN'].$this->dtl[$i]['JNSATUAN'].'/'.$this->dtl[$i]['URJNSATUAN']."\n".$this->dtl[$i]['NETDET'].' Kg', 0, 'L');
            $h[] = $this->pdf->getY();
            #48
            $this->pdf->setXY($this->pdf->getX() + 141.6, $Yawl);
            $this->pdf->multicell(35, 4, '', 0, 'L');
            $h[] = $this->pdf->getY();
            #49
            $this->pdf->setXY($this->pdf->getX() + 176.5, $Yawl);
            $this->pdf->multicell(23.3, 4, $this->dtl[$i]['DNILINV'], 0, 'R');
            $h[] = $this->pdf->getY();
            
            $Yawl = max($h) + 2;
            if ($Yawl > 230) {
                $i++;
                break;
            }
        }
        
        $this->pdf->Rect(10, $Y, 10, ($Yawl - $Y));
        $this->pdf->Rect(20, $Y, 60, ($Yawl - $Y));
        $this->pdf->Rect(80, $Y, 32, ($Yawl - $Y));
        $this->pdf->Rect(112, $Y, 35, ($Yawl - $Y));
        $this->pdf->Rect(147, $Y, 35, ($Yawl - $Y));
        $this->pdf->Rect(182, $Y, 23.2, ($Yawl - $Y));
        if ($i < $totData) {
            $this->dataLampiranBarang($i);
        }
    }
    
    function dataLampiranPJT($idPrint = 0){
        $this->headerLampiran('pjt', 'LANJUTAN PJT');
        $this->pdf->SetFont('times', '', '8');
        $Yawl = $this->pdf->getY();
        $Y = $this->pdf->getY(); 
        $totData = count($this->pjt);
        for ($i = $idPrint; $i < $totData; $i++) {
            #no 
            //print_r($this->pjt);die();
            $this->pdf->setXY($this->pdf->getX() + 4.6, $Yawl);
            $this->pdf->cell(10, 4, ($i + 1), 0, 0, 'C', 0);
            #45-46
            $this->pdf->setXY($this->pdf->getX() + 0, $Yawl);
            $this->pdf->multicell(39, 4, $this->pjt[$i]['NPWPEKST']."\n".$this->pjt[$i]['NAMAEKST']."\n".$this->pjt[$i]['ALMTEKST']."\n"."\n".$this->pjt[$i]['NAMABELIT']."\n".$this->pjt[$i]['ALMTBELIT'], 0, 'L');
            $h[] = $this->pdf->getY();
            #45-46
            $this->pdf->setXY($this->pdf->getX() + 54.6, $Yawl);
            $this->pdf->multicell(47, 4, $this->dtl[$i]['HS']."\n".$this->dtl[$i]['URBRG1']."\n".$this->dtl[$i]['DMERK'].'/'.$this->dtl[$i]['SIZE'].'/'.$this->dtl[$i]['TYPE'].'/'.$this->dtl[$i]['KDBRG']."\n".'gak tau dari mana', 0, 'L');
            $h[] = $this->pdf->getY();
            #45-46
            $this->pdf->setXY($this->pdf->getX() + 102.6, $Yawl);
            $this->pdf->multicell(39, 4, $this->dtl[$i]['JMSATUAN'].$this->dtl[$i]['JNSATUAN'].'/'.$this->dtl[$i]['URJNSATUAN']."\n".$this->dtl[$i]['NETDET'].' Kg', 0, 'L');
            $h[] = $this->pdf->getY();
            #45-46
            $this->pdf->setXY($this->pdf->getX() + 142.6, $Yawl);
            $this->pdf->multicell(33, 4, '', 0, 'L');
            $h[] = $this->pdf->getY();
            #45-46
            $this->pdf->setXY($this->pdf->getX() + 176.6, $Yawl);
            $this->pdf->multicell(22.5, 4, $this->dtl[$i]['DNILINV'], 0, 'L');
            $h[] = $this->pdf->getY();
            
            $Yawl = max($h) + 2;
            if ($Yawl > 230) {
                $i++;
                break;
            }
        }
        
        $this->pdf->Rect(10, $Y, 10, ($Yawl - $Y)); 
        $this->pdf->Rect(20, $Y, 40, ($Yawl - $Y)); 
        $this->pdf->Rect(60, $Y, 48, ($Yawl - $Y)); 
        $this->pdf->Rect(108, $Y, 40, ($Yawl - $Y)); 
        $this->pdf->Rect(148, $Y, 34, ($Yawl - $Y));
        $this->pdf->Rect(182, $Y, 23.2, ($Yawl - $Y));
        if ($i < $totData) {
            $this->dataLampiranBarang($i);
        }
                
    }    
    
    function dataLampiranDok($idPrint = 0){
        $this->headerLampiran('dokumen', 'LANJUTAN DOKUMEN');
        $this->pdf->SetFont('times', '', '8');
        $Yawl = $this->pdf->getY();
        $Y = $this->pdf->getY(); //print_r($this->dtl);die();
        $totData = count($this->dok);
        for ($i = $idPrint; $i < $totData; $i++) {
            #no 
            $this->pdf->setXY($this->pdf->getX() + 19, $Yawl);
            $this->pdf->cell(14, 4, ($i + 1), 0, 0, 'C', 0);
            
            #45-46
            $this->pdf->setXY($this->pdf->getX() + 1, $Yawl);
            $this->pdf->multicell(49, 4, $this->dok[$i]['URKDDOK'], 0, 'L');
            $h[] = $this->pdf->getY();
            #47
            $this->pdf->setXY($this->pdf->getX() + 84, $Yawl);
            $this->pdf->multicell(54, 4, $this->dok[$i]['NODOK'], 0, 'L');
            $h[] = $this->pdf->getY();
            #48
            $this->pdf->setXY($this->pdf->getX() + 139, $Yawl);
            $this->pdf->multicell(60, 4, $this->dok[$i]['TGDOK'], 0, 'L');
            $h[] = $this->pdf->getY();
            
            $Yawl = max($h) + 2;
            if ($Yawl > 230) {
                $i++;
                break;
            }
        }
        $this->pdf->Rect(4.3, $Y, 201, ($Yawl - $Y));
        $this->pdf->Rect(4.3, $Y, 20, ($Yawl - $Y)); 
        $this->pdf->Rect(24.3, $Y, 15,($Yawl - $Y)); 
        $this->pdf->Rect(39.3, $Y, 50, ($Yawl - $Y)); 
        $this->pdf->Rect(89.4, $Y, 55, ($Yawl - $Y)); 
        $this->pdf->Rect(144.5, $Y, 60.7, ($Yawl - $Y)); 
    }   
    
//SSPCP

    function drawSSPCP() {
        $this->pdf->SetMargins(5, 0, 0);
        $this->pdf->SetAutoPageBreak(0, 0);
        $this->pdf->SetY(5);
        $this->pdf->SetFont('times', 'B', '8.5');
        $Y = $this->pdf->getY();
        $this->pdf->multicell(65, 4, "KEMENTERIAN KEUANGAN R.I.\nDIREKTORAT JENDERAL BEA DAN CUKAI", 0, 'L');
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->multicell(65, 4, $this->ntb['URKPBC'] . ' / ' . $this->ntb['KDKPBC'], 0, 'L');
        $arrY[] = $this->pdf->getY() - 5;
        $this->pdf->setXY(70, $Y);
        $this->pdf->SetFont('times', 'B', '12');
        $this->pdf->multicell(70, 6, "SURAT SETORAN\nPABEAN, CUKAI DAN PAJAK\n( SSPCP )", 0, 'C');
        $this->pdf->setXY(140, $Y);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->multicell(65, 4, "Lembar ke-1  :  Wajib Bayar\nLembar ke-2  :  KPPN\nLembar ke-3  :  Kantor Bea dan Cukai\nLembar ke-4  :  Bank Devisa Persepsi / Bank\n                          Persepsi / Pos Persepsi", 0, 'L');
        $arrY[] = $this->pdf->getY() - 5;
        $Y = max($arrY);
        $this->pdf->Rect(5, 5, 65, $Y);
        $this->pdf->Rect(70, 5, 70, $Y);
        $this->pdf->Rect(140, 5, 65, $Y);
        $this->pdf->SetY($Y + 5);
        $this->pdf->SetFont('times', 'B', '9');
        $this->pdf->cell(65, 6, 'A. JENIS PENERIMAAN NEGARA', 1, 0, 'L', 0);
        $this->pdf->cell(5, 6, 'X', 1, 0, 'C', 0);
        $this->pdf->cell(20, 6, 'IMPOR', 1, 0, 'L', 0);
        $this->pdf->cell(5, 6, ' ', 1, 0, 'C', 0);
        $this->pdf->cell(20, 6, 'EXSPOR', 1, 0, 'L', 0);
        $this->pdf->cell(5, 6, ' ', 1, 0, 'L', 0);
        $this->pdf->cell(20, 6, 'CUKAI', 1, 0, 'L', 0);
        $this->pdf->cell(5, 6, ' ', 1, 0, 'C', 0);
        $this->pdf->cell(55, 6, 'BARANG TERTENTU', 1, 0, 'L', 0);
        $this->pdf->ln();
        $this->pdf->cell(65, 6, 'B. JENIS IDENTITAS', 1, 0, 'L', 0);
        $this->pdf->cell(5, 6, 'X', 1, 0, 'C', 0);
        $this->pdf->cell(20, 6, 'NPWP', 1, 0, 'L', 0);
        $this->pdf->cell(5, 6, ' ', 1, 0, 'C', 0);
        $this->pdf->cell(20, 6, 'PASPOR', 1, 0, 'L', 0);
        $this->pdf->cell(5, 6, ' ', 1, 0, 'L', 0);
        $this->pdf->cell(80, 6, 'KTP', 1, 0, 'L', 0);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->ln();
        $Y = $this->pdf->getY();
        $this->pdf->SetX(10);
        $npwp = str_split($this->ntb['IMPNPWP']);
        $i = 0;
        $this->pdf->cell(20, 5, 'NOMOR', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(9, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->ln();
        $this->pdf->SetX(10);
        $this->pdf->cell(20, 5, 'NAMA', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->cell(170, 5, $this->ntb['NAMA'], 0, 0, 'L', 0);

        $this->pdf->ln();
        $this->pdf->SetX(10);
        $this->pdf->cell(20, 5, 'ALAMAT', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->cell(170, 5, $this->ntb['ALAMAT'], 0, 0, 'L', 0);
        $this->pdf->ln();
        $this->pdf->SetX(150);
        $this->pdf->cell(55, 5, 'Kode Pos  :  ' . $this->ntb['KDPOS'], 0, 0, 'L', 0);
        $this->pdf->ln();
        $Yawal = $Y;
        $Y = $this->pdf->getY() + 2;
        $this->pdf->Rect(5, $Yawal, 200, $Y - $Yawal);
        $this->pdf->setY($Y);
        $this->pdf->SetFont('times', 'B', '9');
        $this->pdf->cell(65, 5, 'C. DOKUMEN DASAR PEMBAYARAN  :', 0, 0, 'L', 0);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->cell(135, 5, 'Pemberitahuan Impor Barang (BC30)', 0, 0, 'L', 0);
        $this->pdf->ln();
        $car = substr($this->ntb['CAR'], 0, 6) . " - " . substr($this->ntb['CAR'], 6, 6) . " - " . substr($this->ntb['CAR'], 12, 8) . " - " . substr($this->ntb['CAR'], 20);
        $tgl = substr($this->ntb['CAR'], 12, 8);
        $tgl = substr($tgl, 0, 4) . '-' . substr($tgl, 4, 2) . '-' . substr($tgl, 6, 2);
        $this->pdf->cell(15, 5, 'Nomor  : ', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, 'Nomor Pengajuan PEB', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ': ', 0, 0, 'L', 0);
        $this->pdf->cell(90, 5, $car, 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, 'Tanggal : ' . $tgl, 0, 0, 'L', 0);
        $this->pdf->ln();
        $this->pdf->cell(15, 5, '', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, 'Nomor Pendaftaran PEB', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ': ', 0, 0, 'L', 0);
        $this->pdf->cell(90, 5, $this->ntb['KDKPBC'].'/'.$this->ntb['NODAFT'], 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, 'Tanggal : ' . $tgl, 0, 0, 'L', 0);
        $this->pdf->ln();
        $Yawal = $Y;
        $Y = $this->pdf->getY() + 2;
        $this->pdf->Rect(5, $Yawal, 200, $Y - $Yawal);
        $this->pdf->setY($Y);
        $this->pdf->SetFont('times', 'B', '9');
        $this->pdf->cell(200, 5, 'D. PEMBAYARAN PENERIMAAN NEGARA  :', 1, 0, 'L', 0);
        $this->pdf->ln();
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->cell(120, 5, 'AKUN', 1, 0, 'C', 0);
        $this->pdf->cell(24, 5, 'KODE AKUN', 1, 0, 'C', 0);
        $this->pdf->cell(56, 5, 'JUMLAH PEMBAYARAN', 1, 0, 'C', 0);
        $this->pdf->ln();


        $this->pdf->cell(120, 5, '     Bea Masuk', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '412111', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['412111'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();


        $this->pdf->cell(120, 5, '     Bea Masuk Ditanggung Pemerintah atas Hibah (SPM) Nihil', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '412112', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['412112'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(120, 5, '     Bea Masuk Dalam Rangka Kemudahan Impor Tujuan Elspor (KITE)', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '412114', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['412114'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(120, 5, '     Denda Administrasi Pabean', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '412113', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['412113'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(120, 5, '     Denda Administrasi Atas Pengangkutan Barang Tertentu', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '412115', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['412115'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(120, 5, '     Pendapatan Pabean lainnya', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '412119', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['412119'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(120, 5, '     Bea Keluar', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '412211', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['412211'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(120, 5, '     Denda Administrasi Bea Keluar', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '412212', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['412212'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(120, 5, '     Bunga Bea Keluar', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '412213', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['412213'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(120, 5, '     Cukai Hasil Tembakau', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '411511', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['411511'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(120, 5, '     Cukai Etil Alkohol', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '411512', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['411512'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(120, 5, '     Cukai Minuman Mengandung Etil Alkohol', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '411513', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['411513'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(120, 5, '     Pendapatan Cukai lainnya', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '411519', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['411519'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(120, 5, '     Denda Administrasi Cukai', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '411514', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['411514'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(120, 5, '     PNBP/Pendapatan DJBC', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '423216', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['423216'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(70, 5, '     PPN IMPOR', 0, 0, 'L', 0);
        $this->pdf->cell(15, 5, 'NPWP : ', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, ($this->ntb['411212'] != '') ? $this->formatNPWP($this->ntb['IMPNPWP']) : '', 'B', 0, 'L', 0);
        $this->pdf->cell(24, 5, '411212', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['411212'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(120, 5, '     PPN Hasil Tembakau / PPN Dalam Negeri', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '411211', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['411211'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(70, 5, '     PPnBM Impor', 0, 0, 'L', 0);
        $this->pdf->cell(15, 5, 'NPWP : ', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, ($this->ntb['411222'] != '') ? $this->formatNPWP($this->ntb['IMPNPWP']) : '', 'B', 0, 'L', 0);
        $this->pdf->cell(24, 5, '411222', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['411222'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(70, 5, '     PPh Pasal 22 Impor ', 0, 0, 'L', 0);
        $this->pdf->cell(15, 5, 'NPWP : ', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, ($this->ntb['411123'] != '') ? $this->formatNPWP($this->ntb['IMPNPWP']) : '', 'B', 0, 'L', 0);
        $this->pdf->cell(24, 5, '411123', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['411123'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(120, 5, '     Bunga Penagihan PPN', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '411622', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['411622'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();
        $Yawal = $Y;
        $Y = $this->pdf->getY() + 2;
        $this->pdf->Rect(5, $Yawal, 200, $Y - $Yawal);
        $this->pdf->Rect(125, $Yawal, 24, $Y - $Yawal);
        $this->pdf->setY($Y);
        $this->pdf->cell(144, 5, 'Masa Pajak', 1, 0, 'C', 0);
        $this->pdf->cell(56, 5, 'Tahun Pajak', 0, 0, 'C', 0);
        $this->pdf->ln();
        $bln = array('Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nop', 'Des');
        foreach ($bln as $a) {
            $x[] = $this->pdf->getX();
            $this->pdf->cell(12, 6, $a, 1, 0, 'C', 0);
        }
        $this->pdf->setX($x[$this->ntb['MASAPAJAK'] - 1]);
        $this->pdf->SetFont('times', 'B', '15');
        $this->pdf->cell(12, 6, 'X', 0, 0, 'C', 0);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->setX($x[11] + 22);
        $thn = str_split($this->ntb['TAHUNPAJAK']);
        foreach ($thn as $a) {
            $this->pdf->cell(9, 6, $a, 1, 0, 'C', 0);
        }
        $this->pdf->ln();
        $Yawal = $Y;
        $Y = $this->pdf->getY();
        $this->pdf->Rect(5, $Yawal, 200, $Y - $Yawal);
        $this->pdf->SetFont('times', 'B', '9');
        $this->pdf->cell(200, 6, 'E. JUMLAH PEMBAYARAN PENERIMAAN NEGARA   : Rp.  ' . number_format($this->ntb['TOTAL'], 2, '.', ','), 0, 0, 'L', 0);
        $this->pdf->ln();
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->cell(30, 5, '     Dengan Huruf    :', 0, 0, 'L', 0);
        $this->pdf->multicell(170, 5, strtoupper($this->terbilang($this->ntb['TOTAL'])), 0, 'L');
        $Yawal = $Y;
        $Y = $this->pdf->getY() + 2;
        $this->pdf->Rect(5, $Yawal, 200, $Y - $Yawal);
        $this->pdf->setY($Y);
        $this->pdf->cell(25, 6, 'Diterima Oleh', 0, 0, 'L', 0);
        $this->pdf->cell(6, 6, '', 1, 0, 'L', 0);
        $this->pdf->cell(35, 6, 'Kantor Bea dan Cukai', 0, 0, 'L', 0);
        $this->pdf->cell(6, 6, '', 1, 0, 'L', 0);
        $this->pdf->cell(32, 6, 'Kantor Pos', 0, 0, 'L', 0);
        $this->pdf->cell(6, 6, '', 1, 0, 'L', 0);
        $this->pdf->cell(32, 6, 'Bank Devisa Persepsi', 0, 0, 'L', 0);
        $this->pdf->cell(6, 6, '', 1, 0, 'L', 0);
        $this->pdf->cell(23, 6, 'Bank Persepsi', 0, 0, 'L', 0);
        $this->pdf->cell(6, 6, '', 1, 0, 'L', 0);
        $this->pdf->cell(23, 6, 'Pos Persepsi', 0, 0, 'L', 0);
        $this->pdf->ln(7);
        $Y2 = $this->pdf->getY();
        $this->pdf->setX(10);
        $this->pdf->cell(25, 5, 'NPWP', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->cell(65, 5, $this->formatNPWP($this->ntb['NPWP']), 0, 0, 'L', 0);
        $this->pdf->ln();
        $this->pdf->setX(10);
        $this->pdf->cell(25, 5, 'Nama Kantor', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->multicell(65, 5, $this->ntb['URKPBC'], 0, 'L');
        $this->pdf->setX(10);
        $this->pdf->cell(25, 5, 'Kode Kantor', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->cell(65, 5, $this->ntb['KDKPBC'], 0, 0, 'L', 0);
        $this->pdf->ln();
        $this->pdf->setX(10);
        $this->pdf->cell(25, 5, 'Nomor SSPCP', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->cell(65, 5, '........................................................................', 0, 0, 'L', 0);
        $this->pdf->ln();
        $this->pdf->setX(10);
        $this->pdf->cell(25, 5, 'Tanggal', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->cell(65, 5, '........................................................................', 0, 0, 'L', 0);
        $this->pdf->ln(7);
        $this->pdf->SetFont('times', 'i', '9');
        $this->pdf->cell(100, 8, 'Cap dan tanda tangan', 0, 0, 'C', 0);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->ln(7);
        $this->pdf->setX(25);
        $this->pdf->cell(15, 5, 'Nama', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->multicell(60, 5, "..............................................................\n..............................................................", 0, 'L');
        $arY[] = $this->pdf->getY();
        $this->pdf->setY($Y2);
        $this->pdf->setX(110);
        $this->pdf->cell(25, 5, 'Nama Bank/Pos', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->cell(65, 5, '........................................................................', 0, 0, 'L', 0);
        $this->pdf->ln();
        $this->pdf->setX(110);
        $this->pdf->cell(25, 5, 'Kode Bank/Pos', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->cell(65, 5, '........................................................................', 0, 0, 'L', 0);
        $this->pdf->ln();
        $this->pdf->setX(110);
        $this->pdf->cell(25, 5, 'Nomor SSPCP', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->cell(65, 5, '........................................................................', 0, 0, 'L', 0);
        $this->pdf->ln();
        $this->pdf->setX(110);
        $this->pdf->cell(25, 5, 'Unit KPPN', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->cell(65, 5, '........................................................................', 0, 0, 'L', 0);
        $this->pdf->ln();
        $this->pdf->setX(110);
        $this->pdf->cell(25, 5, 'Tanggal', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->cell(65, 5, '........................................................................', 0, 0, 'L', 0);
        $this->pdf->ln();
        $this->pdf->ln(7);
        $this->pdf->SetFont('times', 'i', '9');
        $this->pdf->setX(110);
        $this->pdf->cell(100, 8, 'Cap dan tanda tangan', 0, 0, 'C', 0);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->ln(7);
        $this->pdf->setX(125);
        $this->pdf->cell(15, 5, 'Nama', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->multicell(60, 5, "..............................................................\n..............................................................", 0, 'L');
        $arY[] = $this->pdf->getY();
        $Yawal = $Y;
        $Y = max($arY) + 2;
        $this->pdf->Rect(5, $Yawal, 100, $Y - $Yawal);
        $this->pdf->Rect(105, $Yawal, 100, $Y - $Yawal);
        $this->pdf->setY($Y);
        $this->pdf->SetFont('times', 'B', '9');
        $this->pdf->cell(100, 5, 'NTB / NTP  : ' . $this->ntb['NTB'], 1, 0, 'L', 0);
        $this->pdf->cell(100, 5, 'NTPN  : ' . $this->ntb['NTPN'], 1, 0, 'L', 0);
    }
    
//RESPON

    private function isiRespon(){
        $this->rpt['KOP']           = $this->ktr['KOP'];
        $this->rpt['USAHANPWP']     = $this->formatNPWP($this->hdr['NPWPEKS']);
        $this->rpt['USAHANAMA']     = $this->hdr['NAMAEKS'];
        $this->rpt['USAHAALMT']     = $this->hdr['ALMTEKS'];
        $this->rpt['PPJKNPWP']      = $this->formatNPWP($this->hdr['NPWPPPJK']);
        $this->rpt['PPJKNAMA']      = $this->hdr['NAMAPPJK'];
        $this->rpt['NPWPEKS']       = $this->formatNPWP($this->hdr['NPWPEKS']);
        $this->rpt['PPJKALMT']      = $this->hdr['ALMTPPJK'];
        $this->rpt['PPJKNP']        = $this->hdr['NOPPJK'].'  Tanggal  '.$this->hdr['TGPPJK']; 
        $this->rpt['KAKI'] = 'Wk.Respon:'.$this->res['RESTG'].substr($this->res['RESWK'],0,2).':'.substr($this->res['RESWK'],2,2).':'.substr($this->res['RESWK'],4).'Kode:'.'Komp: '.$this->res['NMKOMP'];
        switch($this->res['RESKD']){            
            case'10':                
                $this->rpt['LBLJUDUL']= 'NOTA PEMBERITAHUAN PENOLAKAN (NPP)' ;
                $this->rpt['LBL11'] = 'Terhadap PEB dengan nomor pengajuan: '.$this->formatCar($this->res['CAR']);
                $this->rpt['LBL12'] = 'DITOLAK/REJECTED karena';
                $this->rpt['LBL13'] = 'Jakarta, '.$this->res['RESTG'];
                $this->rpt['LBL14'] = 'Pejabat Pemeriksa Dokumen Ekspor/';
                $this->rpt['LBL15'] = 'Pejabat Penerima Dokumen';
                break;
            case'15':                 
                $this->rpt['LBLJUDUL']= 'NOTA PEMBERITAHUAN PENOLAKAN (NPP)' ;
                $this->rpt['LBL11'] = 'Terhadap PEB dengan nomor pengajuan: '.$this->formatCar($this->res['CAR']);
                $this->rpt['LBL12'] = 'sedang dalam proses pelayanan ekspor.';
                $this->rpt['LBL13'] = 'Jakarta, '.$this->res['RESTG'];
                $this->rpt['LBL14'] = 'Pejabat Pemeriksa Dokumen Ekspor';
                break;
            case'17':                
                $this->rpt['LBLJUDUL']= 'NOTA PEMBERITAHUAN PERSYARATAN DOKUMEN (NPPD)' ;
                $this->rpt['LBL11'] = 'Terhadap PEB Perbaikan dengan nomor pengajuan: '.$this->formatCar($this->res['CAR']);
                $this->rpt['LBL12'] = 'Kekurangan dokumen yang dipersyaratkan oleh instansi terkait berupa:';
                $this->rpt['LBL13'] = 'Jakarta, '.$this->res['RESTG'];
                $this->rpt['LBL14'] = 'Pejabat Bea dan Cukai';
                break;            
            case'60':                  
                $this->rpt['LBLJUDUL']= "NOTA PEMBERITAHUAN PENOLAKAN (NPP) \n (PEB PERBAIKAN)" ;
                $this->rpt['LBL11'] = 'Terhadap PEB Perbaikan dengan nomor pengajuan: '.$this->formatCar($this->res['CAR']);
                $this->rpt['LBL12'] = 'DITOLAK/REJECTED karena';
                $this->rpt['LBL13'] = 'Jakarta, '.$this->res['RESTG'];
                $this->rpt['LBL14'] = 'Pejabat Pemeriksa Dokumen Ekspor';
                break; 
            case'70':                  
                $this->rpt['LBLJUDUL']= "NOTA PENERIMAAN \n (PEB PERBAIKAN)" ;
                $this->rpt['LBL11'] = 'Terhadap PEB Perbaikan dengan nomor pengajuan: '.$this->formatCar($this->res['CAR']);
                $this->rpt['LBL12'] = 'Perbaikan telah memenuhi syarat untuk diproses lebih lanjut.';
                $this->rpt['LBL13'] = 'Jakarta, '.$this->res['RESTG'];
                $this->rpt['LBL14'] = 'Pejabat Pemeriksa Dokumen Ekspor/';
                $this->rpt['LBL15'] = 'Pejabat Penerima Dokumen';
                break;
            case'20':                  
                $this->rpt['LBLJUDUL']= "PEMBERITAHUAN PEMERIKSAAN BARANG (PPB)" ;
                $this->rpt['LBLJUDUL2']= 'Nomor                    '.'Tanggal      ' ;
                $this->rpt['LBL11'] = 'Terhadap Barang Ekspor yang diberitahukan dengan PEB nomor: '.$this->res['CAR'].' Tanggal '.$this->res['CAR'];
                $this->rpt['LBL12'] = 'harus dilakukan pemeriksaan fisik dan pengawasan stuffing pada:';
                $this->rpt['NOAJU'] = 'No.Pengajuan: '.$this->formatCar($this->res['CAR']);
                $this->rpt['KAKI20'] = 'Wk.Respon:'.$this->res['RESTG'].substr($this->res['RESWK'],0,2).':'.substr($this->res['RESWK'],2,2).':'.substr($this->res['RESWK'],4).'Kode:'.'Komp: '.$this->res['NMKOMP'].'                               Peruntukan:';
                break;
            case'30':                  
                $this->rpt['LBLJUDUL']= "NOTA PELAYANAN EKSPOR (NPE)" ;
                $this->rpt['LBLJUDUL2']= 'Nomor                    '.'Tanggal      ' ;
                $this->rpt['LBL11'] = 'Terhadap Barang Ekspor yang diberitahukan dengan PEB nomor: '.$this->res['CAR'].' Tanggal '.$this->res['CAR'];
                $this->rpt['LBL12'] = 'harus dilakukan pemeriksaan fisik dan pengawasan stuffing pada:';
                $this->rpt['NOAJU'] = 'No.Pengajuan: '.$this->formatCar($this->res['CAR']);
                $this->rpt['HDRCAR'] = 'KEMENTERIAN KEUANGAN REPUBLIK INDONESIA'.'          No.Pengajuan: '.$this->formatCar($this->res['CAR']);
                $this->rpt['KAKI30'] = 'Wk.Respon:'.$this->res['RESTG'].substr($this->res['RESWK'],0,2).':'.substr($this->res['RESWK'],2,2).':'.substr($this->res['RESWK'],4).'Kode:'.'Komp: '.$this->res['NMKOMP'].'                               Peruntukan:';
                break;
            case'40':                  
                $this->rpt['LBLJUDUL']= "LAPORAN PEMERIKSAAN EKSPOR (LPE)" ;
                $this->rpt['LBLJUDUL2']= 'Nomor:';
                $this->rpt['KAKI40'] = 'Wk.Respon:'.$this->res['RESTG'].substr($this->res['RESWK'],0,2).':'.substr($this->res['RESWK'],2,2).':'.substr($this->res['RESWK'],4).'Kode:'.'Komp: '.$this->res['NMKOMP'].'                               Peruntukan:';
                break;
            case'65':                
                $this->rpt['LBLJUDUL']= 'PENOLAKAN MOTUL PKBE' ;
                $this->rpt['LBL11'] = 'Terhadap PEB dengan nomor pengajuan: '.$this->formatCar($this->res['CAR']);
                $this->rpt['LBL12'] = 'DITOLAK/REJECTED karena';
                $this->rpt['LBL13'] = 'Jakarta, '.$this->res['RESTG'];
                $this->rpt['LBL14'] = 'Pejabat Pemeriksa Dokumen Ekspor/';
                $this->rpt['LBL15'] = 'Pejabat Penerima Dokumen';
                break;
            case'900':                
                $this->rpt['LBLJUDUL']= 'JADUL' ;
                $this->rpt['LBL11'] = 'Terhadap PEB dengan nomor pengajuan: '.$this->formatCar($this->res['CAR']);
                $this->rpt['LBL12'] = 'DITOLAK/REJECTED karena';
                $this->rpt['LBL13'] = 'Jakarta, '.$this->res['RESTG'];
                $this->rpt['LBL14'] = 'Pejabat Pemeriksa Dokumen Ekspor/';
                $this->rpt['LBL15'] = 'Pejabat Penerima Dokumen';
                break;
        }
        $this->rpt['LBLKOTATTD']  = $this->res['KOTA'].', '.$this->formatTglMysql($this->res['RESTG']);
        
    }    
    
    function drawResponNPP(){
        $this->pdf->SetMargins(5, 5, 5, 5);
        $this->pdf->SetAutoPageBreak(true, 5);
        $this->pdf->SetFont('courier', '', '9');
        $this->pdf->Rect(5.4, 10, 185, 188, 1.5, 'F');
        $this->pdf->Ln();
        $this->pdf->multicell(176, 6,$this->rpt['KOP'], 0, 'L');
        $this->pdf->Ln(10);
        $this->pdf->SetFont('courier', 'B', '10');
        $this->pdf->multicell(185, 5, $this->rpt['LBLJUDUL'], 0, 'C');
        $this->pdf->SetFont('courier', '', '9');
        $this->pdf->Ln(10);
        $this->pdf->multicell(35, 6,'Kepada Saudara:', 0, 'L');
        $this->pdf->multicell(20, 4.5, 'EKSPORTIR' , 0, 'L');
        $this->pdf->setX(13.5);
        $this->pdf->multicell(176, 4.5, '-NPWP    : ' . $this->rpt['USAHANPWP'], 0, 'L');
        $this->pdf->setX(13.5);
        $this->pdf->multicell(176, 4.5, '-Nama    : ' . $this->rpt['USAHANAMA'], 0, 'L');
        $this->pdf->setX(13.5);
        $this->pdf->multicell(176, 4.5, '-Alamat  : ' . $this->rpt['USAHAALMT'], 0, 'L');
        $this->pdf->Ln(2);
        
        $this->pdf->multicell(20, 4.5, 'PPJK' , 0, 'L');
        $this->pdf->setX(13.5);
        $this->pdf->multicell(176, 4.5, '-NPWP    : ' . $this->rpt['PPJKNPWP'], 0, 'L');
        $this->pdf->setX(13.5);
        $this->pdf->multicell(176, 4.5, '-Nama    : ' . $this->rpt['PPJKNAMA'], 0, 'L');
        $this->pdf->setX(13.5);
        $this->pdf->multicell(176, 4.5, '-Alamat  : ' . $this->rpt['PPJKALMT'], 0, 'L');
        $this->pdf->setX(13.5);
        $this->pdf->multicell(176, 4.5, '-NP PPJK : ' .$this->rpt['PPJKNP'], 0, 'L'); 
        $this->pdf->Ln(10);
        $this->pdf->multicell(176, 4.5, $this->rpt['LBL11'], 0, 'L');
        $this->pdf->multicell(176, 4.5, $this->rpt['LBL12'], 0, 'L');
        
        $this->pdf->Ln(20);
        $this->pdf->setX(100);
        $this->pdf->multicell(80, 4.5, $this->rpt['LBL13'], 0, 'L');
        $this->pdf->setX(100);
        $this->pdf->multicell(80, 4.5, $this->rpt['LBL14'], 0, 'L');
        $this->pdf->setX(100);
        $this->pdf->multicell(80, 4.5, $this->rpt['LBL15'], 0, 'L');
        $this->pdf->Ln(20);
        $this->pdf->setX(100);
        $this->pdf->multicell(100, 4.5, 'NAMA    : ' . $this->rpt['PPJKNPWP'], 0, 'L');
        $this->pdf->setX(100);
        $this->pdf->multicell(100, 4.5, 'NIP     : ' . $this->rpt['PPJKNPWP'], 0, 'L');
        
        $this->pdf->Ln(10);
        $this->pdf->multicell(185, 4.5,     $this->rpt['KAKI'], 0, 'L');
    }   
    
    function drawResponPPB(){
        $this->pdf->SetMargins(5, 5, 5, 5);
        $this->pdf->SetAutoPageBreak(true, 5);
        $this->pdf->SetFont('courier', '', '8');
        $this->pdf->Rect(5.4, 10, 190, 222, 1.5, 'F');
        $this->pdf->setXY(180,5);
        $this->pdf->cell(3, 4, 'BCF 3.05', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->multicell(176, 6,$this->rpt['KOP'], 0, 'L');
        $this->pdf->Ln(-17);
        $this->pdf->setX(110);
        $this->pdf->cell(3, 4, $this->rpt['NOAJU'], 0, 0, 'L', 0);
        $this->pdf->Ln(20);
        $this->pdf->SetFont('courier', 'B', '9');
        $this->pdf->multicell(185, 5, $this->rpt['LBLJUDUL'], 0, 'C');
        $this->pdf->multicell(185, 5, $this->rpt['LBLJUDUL2'], 0, 'C');
        $this->pdf->SetFont('courier', '', '8');
        $this->pdf->Ln(5);
        $this->pdf->multicell(35, 6,'Kepada Saudara:', 0, 'L');
        $this->pdf->multicell(20, 4.5, 'EKSPORTIR' , 0, 'L');
        $this->pdf->setX(13.5);
        $this->pdf->multicell(176, 4.5, '-NPWP    : ' . $this->rpt['USAHANPWP'], 0, 'L');
        $this->pdf->setX(13.5);
        $this->pdf->multicell(176, 4.5, '-Nama    : ' . $this->rpt['USAHANAMA'], 0, 'L');
        $this->pdf->setX(13.5);
        $this->pdf->multicell(176, 4.5, '-Alamat  : ' . $this->rpt['USAHAALMT'], 0, 'L');
        $this->pdf->Ln(2);
        
        $this->pdf->multicell(20, 4.5, 'PPJK' , 0, 'L');
        $this->pdf->setX(13.5);
        $this->pdf->multicell(176, 4.5, '-NPWP    : ' . $this->rpt['PPJKNPWP'], 0, 'L');
        $this->pdf->setX(13.5);
        $this->pdf->multicell(176, 4.5, '-Nama    : ' . $this->rpt['PPJKNAMA'], 0, 'L');
        $this->pdf->setX(13.5);
        $this->pdf->multicell(176, 4.5, '-Alamat  : ' . $this->rpt['PPJKALMT'], 0, 'L');
        $this->pdf->setX(13.5);
        $this->pdf->multicell(176, 4.5, '-NP PPJK : ' .$this->rpt['PPJKNP'], 0, 'L'); 
        $this->pdf->Ln(5);
        $this->pdf->multicell(176, 4.5, $this->rpt['LBL11'], 0, 'L');
        $this->pdf->multicell(176, 4.5, $this->rpt['LBL12'], 0, 'L');
        
        $this->pdf->Ln(3);
        $this->pdf->SetFont('courier', '', '8');
        $this->pdf->setX(13.5);
        $this->pdf->multicell(176, 4.5, 'a. Tanggal                   : ' . $this->rpt['PPJKNPWP'], 0, 'L');
        $this->pdf->setX(13.5);
        $this->pdf->multicell(176, 4.5, 'b. Kantor Pabean Pemeriksa   : ' . $this->rpt['PPJKNPWP'], 0, 'L');
        $this->pdf->setX(13.5);
        $this->pdf->multicell(176, 4.5, 'c. Lokasi dan Nomor Telepon  : ' . $this->rpt['PPJKNPWP'], 0, 'L');
        $this->pdf->setX(73);
        $this->pdf->multicell(80, 6, 'Phone:      ' . $this->rpt[''].'Fax:      '.$this->rpt[''], 0, 'L');
        $this->pdf->setX(13.5);
        $this->pdf->multicell(176, 4.5, 'd. Nama Petugas Eksportir    : ' . $this->rpt['PPJKNPWP'], 0, 'L');
        $this->pdf->setX(13.5);
        $this->pdf->multicell(176, 4.5, 'e. Tanggal & Tempat Stuffing : ' . $this->rpt['PPJKNPWP'], 0, 'L');
        $this->pdf->setX(13.5);
        $this->pdf->multicell(176, 4.5, 'f. Jml peti Kemas/Kemasan(*) : ' . $this->rpt['PPJKNPWP'], 0, 'L');
        $this->pdf->Ln(3);
        $this->pdf->multicell(176, 4.5, 'Untuk pemeriksaan fisik wajib menyiapkan barang ekspor sesuai PEB dan menyerahkan:', 0, 'L');
        $this->pdf->setX(13.5);
        $this->pdf->multicell(176, 4.5, 'a. PEB', 0, 'L');
        $this->pdf->setX(13.5);
        $this->pdf->multicell(176, 4.5, 'b. PEB Pembetulan, apabila dilakukan pembetulan PEB; dan', 0, 'L');
        $this->pdf->setX(13.5);
        $this->pdf->multicell(176, 4.5, 'c. Fotokopi invoice dan fotokopi packing list.', 0, 'L');

        $this->pdf->Ln();
        $this->pdf->setX(100);
        $this->pdf->multicell(80, 4.5, 'Pejabat Pemeriksa Dokumen Ekspor', 0, 'L');
        $this->pdf->Ln(15);
        $this->pdf->setX(100);
        $this->pdf->multicell(100, 4.5, 'NAMA    : .................................', 0, 'L');
        $this->pdf->setX(100);
        $this->pdf->multicell(100, 4.5, 'NIP     : .................................', 0, 'L');
        
        $Y = $this->pdf->getY();
        $Yawal = $Y;
        $this->pdf->Rect(7, $Yawal, 70, 20);
        #$this->pdf->Rect(7, $Yawal, 70, 20, 1.5);
        $this->pdf->setX(8);
         $this->pdf->SetFont('courier', 'B', '8');
        $this->pdf->multicell(176, 4.5, 'Pemeriksa', 0, 'L');
         $this->pdf->SetFont('courier', '', '8');
         $this->pdf->setX(8);
        $this->pdf->multicell(176, 4.5, 'Nama      : ..........................', 0, 'L');
        $this->pdf->setX(8);
        $this->pdf->multicell(176, 4.5, 'NIP       : ..........................', 0, 'L');
        $this->pdf->setX(8);
        $this->pdf->multicell(176, 4.5, 'Tingkat Pemeriksaan : ................', 0, 'L');
        
        $this->footerResponPPB();
       }   
    
    function footerResponPPB(){
        $this->pdf->Ln(15);
        $this->pdf->multicell(185, 4.5,$this->rpt['KAKI20'], 0, 'L');
        $this->pdf->multicell(185, 4.5, 'Fromulir ini dicetak secara otomatis oleh sistem komputer dan tidak memerlukan nama, tanda tangan pejabar dan cap dinas', 0, 'L');
    
    }
    
    function drawResponPNPKBE(){
        $this->pdf->SetMargins(5, 5, 5, 5);
        $this->pdf->SetAutoPageBreak(true, 5);
        $this->pdf->SetFont('courier', '', '9');
        $this->pdf->setXY(174,5);
        $this->pdf->cell(3, 4, 'BCF 3.05', 0, 0, 'L', 0);
        $this->pdf->Rect(5.4, 10, 185, 188, 1.5, 'F');
        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->multicell(176, 6,$this->rpt['KOP'], 0, 'L');
        $this->pdf->Ln(10);
        $this->pdf->SetFont('courier', 'B', '10');
        $this->pdf->multicell(185, 5, $this->rpt['LBLJUDUL'], 0, 'C');
        $this->pdf->SetFont('courier', '', '9');
        $this->pdf->Ln(10);
        $this->pdf->multicell(35, 6,'Kepada Saudara:', 0, 'L');
        $this->pdf->multicell(20, 4.5, 'EKSPORTIR' , 0, 'L');
        $this->pdf->setX(13.5);
        $this->pdf->multicell(176, 4.5, '-NPWP    : ' . $this->rpt['USAHANPWP'], 0, 'L');
        $this->pdf->setX(13.5);
        $this->pdf->multicell(176, 4.5, '-Nama    : ' . $this->rpt['USAHANAMA'], 0, 'L');
        $this->pdf->setX(13.5);
        $this->pdf->multicell(176, 4.5, '-Alamat  : ' . $this->rpt['USAHAALMT'], 0, 'L');
        
        $this->pdf->Ln(35);
        $this->pdf->multicell(176, 4.5, $this->rpt['LBL11'], 0, 'L');
        $this->pdf->multicell(176, 4.5, $this->rpt['LBL12'], 0, 'L');
        
        $this->pdf->Ln(20);
        $this->pdf->setX(100);
        $this->pdf->multicell(80, 4.5, $this->rpt['LBL13'], 0, 'L');
        $this->pdf->setX(100);
        $this->pdf->multicell(80, 4.5, $this->rpt['LBL14'], 0, 'L');
        $this->pdf->setX(100);
        $this->pdf->multicell(80, 4.5, $this->rpt['LBL15'], 0, 'L');
        $this->pdf->Ln(20);
        $this->pdf->setX(100);
        $this->pdf->multicell(100, 4.5, 'NAMA    : ' . $this->rpt['PPJKNPWP'], 0, 'L');
        $this->pdf->setX(100);
        $this->pdf->multicell(100, 4.5, 'NIP     : ' . $this->rpt['PPJKNPWP'], 0, 'L');
        
        $this->ResponPNPKBE();
        
    }   
    
    function ResponPNPKBE(){
        $this->pdf->Ln(12);
        $this->pdf->multicell(185, 4.5,     $this->rpt['KAKI'], 0, 'L');
    }
    
    function drawResponNPE(){
        $this->pdf->SetMargins(10, 5, 5, 5);
        $this->pdf->SetAutoPageBreak(true, 5);
        $this->pdf->SetFont('courier', '', '9');
        $this->pdf->setXY(184,0);
        $this->pdf->cell(3, 4, 'BCF 3.03', 0, 0, 'L', 0);
        $Y = $this->pdf->getY();
        $Yawal = $Y;
        $this->pdf->Rect(10, $Yawal+5, 190, 275, 1.5, 'F');
        $this->pdf->Ln();
        $this->pdf->multicell(176, 6,$this->rpt['KOP'], 0, 'L');
        $this->pdf->Ln(-17);
        $this->pdf->setX(110);
        $this->pdf->cell(3, 4, $this->rpt['NOAJU'], 0, 0, 'L', 0);
        $this->pdf->Ln(20);
        $this->pdf->SetFont('courier', 'B', '11');
        $this->pdf->multicell(185, 5, $this->rpt['LBLJUDUL'], 0, 'C');
        $this->pdf->Ln(2);
        $this->pdf->SetFont('courier', 'B', '9');
        $this->pdf->setX(50);
        $this->pdf->multicell(176, 4.5, 'Nomor              :', 0, 'L');
        $this->pdf->setX(50);
        $this->pdf->multicell(176, 4.5, 'No.Pendaftaran PEB :'.'Tanggal :', 0, 'L');
        $this->pdf->Ln(0.5);
        $this->pdf->setX(155);
        $this->pdf->SetFont('courier', '', '7');
        
        $this->pdf->multicell(176, 3, 'Halaman  '. $this->pdf->PageNo() . ' dari {nb}', 0, 'L');
        $this->pdf->SetFont('courier', '', '9');
        $this->pdf->Ln(0.2);
        $this->pdf->multicell(190, 4.5, '1. KANTOR PABEAN PEMUATAN  : ' . $this->rpt['USAHANPWP'], 1, 'L');
        $this->pdf->multicell(190, 4.5, '2. NPWP / NAMA EKSPORTIR   : ' . $this->rpt['USAHANPWP'], 1, 'L');
        $this->pdf->multicell(190, 4.5, '3. NPWP / NAMA PPJK        : ' . $this->rpt['USAHANPWP'], 1, 'L');
        $this->pdf->multicell(190, 4.5, '4. SARANA ANGKUT', 0, 'L');
        $this->pdf->Ln(-1);
        $this->pdf->setX(20);
        $this->pdf->cell(41.5, 4.5, 'a. Nama', 0, 0, 'L', 0);
        $this->pdf->cell(3, 4.5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(50, 4.5, $this->res['NIP2'], 0, 0, 'L', 0);
        $this->pdf->cell(32, 4.5, 'b. Voyage/Flight', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4.5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(50, 4.5, $this->res['NIP2'], 0, 0, 'L', 0);
        $this->pdf->Ln(5);
        $this->pdf->multicell(190, 4.5, '5. TANGGAL PERKIRAAN EKSPOR: ' . $this->rpt['USAHANPWP'], 1, 'L');
        $this->pdf->multicell(190, 4.5, '6. PELABUHAN MUAT', 0, 'L');
        $this->pdf->Ln(-1);
        $this->pdf->setX(20);
        $this->pdf->cell(41.5, 4.5, 'a. Pelabuhan Muat Asal', 0, 0, 'L', 0);
        $this->pdf->cell(3, 4.5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(50, 4.5, $this->res['NIP2'], 0, 0, 'L', 0);
        $this->pdf->cell(48, 4.5, 'b. Pelabuhan Muat Ekspor', 0, 0, 'L', 0);    
        $this->pdf->cell(2, 4.5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(50, 4.5, $this->res['NIP2'], 0, 0, 'L', 0);
        $this->pdf->Ln(5);
        $this->pdf->multicell(190, 4.5, '7. BERAT KOTOR             : '.$this->hdr['BRUTO'], 1, 'L');
        
        $this->pdf->multicell(190, 4.5, '8. KEMASAN                 : ' . $this->rpt['USAHANPWP'], 0, 'L');
        $this->pdf->setX(15.5);
        $this->pdf->cell(46, 4.5, 'PETI KEMAS', 0, 0, 'L', 0);
        $this->pdf->cell(3, 4.5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(50, 4.5, $this->res['NIP2'], 0, 0, 'L', 0);
        $this->pdf->cell(32, 4.5, 'NON PETI KEMAS', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4.5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(50, 4.5, $this->res['NIP2'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->setX(20);
        $this->pdf->cell(41.5, 4.5, 'a. Merek/nomor', 0, 0, 'L', 0);
        $this->pdf->cell(3, 4.5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(50, 4.5, $this->res['NIP2'], 0, 0, 'L', 0);
        $this->pdf->cell(42, 4.5, 'a. Jenis/Merek Kemasan', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4.5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(50, 4.5, $this->res['NIP2'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->setX(20);
        $this->pdf->cell(41.5, 4.5, 'b. Ukuran', 0, 0, 'L', 0);
        $this->pdf->cell(3, 4.5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(50, 4.5, $this->res['NIP2'], 0, 0, 'L', 0);
        $this->pdf->cell(42, 4.5, 'b. Jumlah', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4.5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(50, 4.5, $this->res['NIP2'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('courier', 'B', '9');
        $this->pdf->multicell(190, 4.5, 'UNTUK KANTOR PABEAN PEMUATAN DI PELABUHAN MUAT EKSPOR', 1, 'C');
        $this->pdf->SetFont('courier', '', '9');
        $Y = $this->pdf->getY();
        $Yawal = $Y;
        $this->pdf->Rect(10, $Yawal, 95, 22, 1.5, 'F');
        $this->pdf->Rect(105, $Yawal, 95, 22, 1.5, 'F');
        $this->pdf->multicell(190, 4.5, 'A. CATATAN PEMERIKSAAN DOKUMEN EKSPOR', 0, 'L');
        $this->pdf->multicell(190, 4.5, 'Pejabar Pemeriksa Dokumen Ekspor', 0, 'L');
        $this->pdf->Ln(8);
        $this->pdf->cell(31.6, 4.5, '...........', 0, 0, 'C', 0);
        $this->pdf->cell(31.6, 4.5, '...........', 0, 0, 'C', 0);
        $this->pdf->cell(31.6, 4.5, '...........', 0, 0, 'C', 0);
        $this->pdf->Ln(-17);
        $this->pdf->setX(105);
        $this->pdf->multicell(190, 4.5, 'B. CATATAN PEMERIKSAAN FISIK BARANG', 0, 'L');
        $this->pdf->Ln(0.1);
        $this->pdf->setX(105);
        $this->pdf->multicell(190, 4.5, 'Pemeriksa', 0, 'L');
        $this->pdf->Ln(8);
        $this->pdf->setX(105);
        $this->pdf->cell(31.6, 4.5, '...........', 0, 0, 'C', 0);
        $this->pdf->cell(31.6, 4.5, '...........', 0, 0, 'C', 0);
        $this->pdf->cell(31.6, 4.5, '...........', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $Y = $this->pdf->getY();
        $Yawal = $Y;
        $this->pdf->Rect(10, $Yawal+0.5, 95, 33, 1.5, 'F');
        $this->pdf->Rect(105, $Yawal+0.5, 95, 33, 1.5, 'F');
        $this->pdf->multicell(190, 4.5, 'C. CATATAN PENGAWASAN STUFFING', 0, 'L');
        $this->pdf->setX(15);
        $this->pdf->multicell(190, 4.5, 'Merek / Nomor Peti Kemas :'.$this->con['NOCONT'], 0, 'L');
        $this->pdf->setX(15);
        $this->pdf->multicell(190, 4.5, 'Ukuran Peti Kemas        :'.$this->con['SIZE'], 0, 'L');
        $this->pdf->setX(15);
        $this->pdf->cell(47.5, 4.5, 'Jenis Segel:.........', 0, 0, 'L', 0);
        $this->pdf->cell(47.5, 4.5, 'Nomor Segel:.........', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->setX(15);
        $this->pdf->multicell(190, 4.5, 'Petugas Pengawas Stuffing', 0, 'L');
        $this->pdf->Ln(5);
        $this->pdf->setX(15);
        $this->pdf->cell(28, 4.5, '...........', 0, 0, 'C', 0);
        $this->pdf->cell(28, 4.5, '...........', 0, 0, 'C', 0);
        $this->pdf->cell(28, 4.5, '...........', 0, 0, 'C', 0);
        $this->pdf->Ln(-27.5);
        $this->pdf->setX(105);
        $this->pdf->multicell(190, 4.5, 'D. CATATAN PENGELUARAN BARANG EKSPOR DARI TPB', 0, 'L');
        $this->pdf->setX(105);
        $this->pdf->cell(47.5, 4.5, 'Jenis Segel:.........', 0, 0, 'L', 0);
        $this->pdf->cell(47.5, 4.5, 'Nomor Segel:.........', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->setX(105);
        $this->pdf->cell(63, 4.5, 'Selesai Muat Tanggal :.........', 0, 0, 'L', 0);
        $this->pdf->cell(47.5, 4.5, 'Pukul:.........', 0, 0, 'L', 0);
        $this->pdf->Ln(9);
        $this->pdf->setX(105);
        $this->pdf->multicell(190, 4.5, 'Petugas Dinas Luar', 0, 'L');
        $this->pdf->Ln(5);
        $this->pdf->setX(105);
        $this->pdf->cell(28, 4.5, '...........', 0, 0, 'C', 0);
        $this->pdf->cell(28, 4.5, '...........', 0, 0, 'C', 0);
        $this->pdf->cell(28, 4.5, '...........', 0, 0, 'C', 0);
        $Y = $this->pdf->getY();
        $Yawal = $Y;
        $this->pdf->Rect(10, $Yawal+6, 95, 28.5, 1.5, 'F');
        $this->pdf->Rect(105,$Yawal+6, 95, 28.5, 1.5, 'F');
        $this->pdf->Ln(5.5);
        $this->pdf->multicell(190, 4.5, 'E. CATATAN PEMASUKAN BRG.EKSPOR KE KAWASAN PABEAN', 0, 'L');
        
        $this->pdf->cell(16, 4.5, 'SEGEL:' ,0, 0, 'L', 0);
        $this->pdf->cell(5, 4.5, '', 1, 0, 'C', 0);
        $this->pdf->cell(16, 4.5, 'Baik', 0, 0, 'L', 0);
        $this->pdf->cell(5, 4.5, '', 1, 0, 'C', 0);
        $this->pdf->cell(16, 4.5, 'Rusak', 0, 0, 'C', 0);
        $this->pdf->cell(5, 4.5, '', 1, 0, 'C', 0); 
        $this->pdf->cell(20, 4.5, 'Tdk Sesuai', 0, 0, 'L', 0);
        $this->pdf->Ln(5);
        $this->pdf->cell(58, 4.5, 'Selesai Masuk Tgl :..........', 0, 0, 'L', 0);
        $this->pdf->cell(30, 4.5, 'Pukul :..........', 0, 0, 'L', 0);
        $this->pdf->Ln(5);
        $this->pdf->multicell(190, 4.5, 'Petugas Dinas Luar', 0, 'L');
        $this->pdf->Ln(5);
        $this->pdf->setX(15);
        $this->pdf->cell(28, 4.5, '...........', 0, 0, 'C', 0);
        $this->pdf->cell(28, 4.5, '...........', 0, 0, 'C', 0);
        $this->pdf->cell(28, 4.5, '...........', 0, 0, 'C', 0);
        $this->pdf->Ln(-24);
        $this->pdf->setX(105);
        $this->pdf->multicell(190, 4.5, 'F. CATATAN PENGELUARAN BARANG EKSPOR DARI TPB', 0, 'L');
        $this->pdf->setX(105);
        $this->pdf->cell(47.5, 4.5, 'Jenis Segel:.........', 0, 0, 'L', 0);
        $this->pdf->cell(47.5, 4.5, 'Nomor Segel:.........', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->setX(105);
        $this->pdf->cell(63, 4.5, 'Selesai Muat Tanggal :.........', 0, 0, 'L', 0);
        $this->pdf->cell(47.5, 4.5, 'Pukul:.........', 0, 0, 'L', 0);
        $this->pdf->Ln(5);
        $this->pdf->setX(105);
        $this->pdf->multicell(190, 4.5, 'Petugas Dinas Luar', 0, 'L');
        $this->pdf->Ln(5.5);
        $this->pdf->setX(105);
        $this->pdf->cell(28, 4.5, '...........', 0, 0, 'C', 0);
        $this->pdf->cell(28, 4.5, '...........', 0, 0, 'C', 0);
        $this->pdf->cell(28, 4.5, '...........', 0, 0, 'C', 0); 
         $this->pdf->Ln(5);
        $this->pdf->multicell(190, 4.5, 'UNTUK KANTOR PABEAN PEMUATAN DI PELABUHAN MUAT ASAL', 1, 'C');
        $Y = $this->pdf->getY();
        $Yawal = $Y;
        $this->pdf->Rect(10, $Yawal, 95, 21, 1.5, 'F');
        $this->pdf->Rect(105, $Yawal, 95, 21, 1.5, 'F');
        $this->pdf->multicell(190, 4.5, 'G. CATATAN PEMERIKSAAN DOKUMEN EKSPOR', 0, 'L');
        $this->pdf->multicell(190, 4.5, 'Pejabar Pemeriksa Dokumen Ekspor', 0, 'L');
        $this->pdf->Ln(6);
        $this->pdf->cell(31.6, 4.5, '...........', 0, 0, 'C', 0);
        $this->pdf->cell(31.6, 4.5, '...........', 0, 0, 'C', 0);
        $this->pdf->cell(31.6, 4.5, '...........', 0, 0, 'C', 0);
        $this->pdf->Ln(-15.5);
        $this->pdf->setX(105);
        $this->pdf->multicell(190, 4.5, 'H. CATATAN PEMERIKSAAN FISIK BARANG', 0, 'L');
        $this->pdf->Ln(0.1);
        $this->pdf->setX(105);
        $this->pdf->multicell(190, 4.5, 'Pemeriksa', 0, 'L');
        $this->pdf->Ln(6);
        $this->pdf->setX(105);
        $this->pdf->cell(31.6, 4.5, '...........', 0, 0, 'C', 0);
        $this->pdf->cell(31.6, 4.5, '...........', 0, 0, 'C', 0);
        $this->pdf->cell(31.6, 4.5, '...........', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->Ln();
        $Y = $this->pdf->getY();
        $Yawal = $Y;
        $this->pdf->Rect(10, $Yawal-2.5, 95, 35, 1.5, 'F');
        $this->pdf->Rect(105, $Yawal-2.5, 95, 35, 1.5, 'F');
        $this->pdf->multicell(190, 4.5, 'I. CATATAN PENGAWASAN STUFFING', 0, 'L');
        $this->pdf->setX(15);
        $this->pdf->multicell(190, 4.5, 'Merek / Nomor Peti Kemas :'.$this->con['NOCONT'], 0, 'L');
        $this->pdf->setX(15);
        $this->pdf->multicell(190, 4.5, 'Ukuran Peti Kemas        :'.$this->con['SIZE'], 0, 'L');
        $this->pdf->setX(15);
        $this->pdf->cell(47.5, 4.5, 'Jenis Segel:.........', 0, 0, 'L', 0);
        $this->pdf->cell(47.5, 4.5, 'Nomor Segel:.........', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->setX(15);
        $this->pdf->multicell(190, 4.5, 'Petugas Pengawas Stuffing', 0, 'L');
        $this->pdf->Ln(5);
        $this->pdf->setX(15);
        $this->pdf->cell(28, 4.5, '...........', 0, 0, 'C', 0);
        $this->pdf->cell(28, 4.5, '...........', 0, 0, 'C', 0);
        $this->pdf->cell(28, 4.5, '...........', 0, 0, 'C', 0);
        $this->pdf->Ln(-27.5);
        $this->pdf->setX(105);
        $this->pdf->multicell(190, 4.5, 'J. CATATAN PENGELUARAN BARANG EKSPOR DARI TPB', 0, 'L');
        $this->pdf->setX(105);
        $this->pdf->cell(47.5, 4.5, 'Jenis Segel:.........', 0, 0, 'L', 0);
        $this->pdf->cell(47.5, 4.5, 'Nomor Segel:.........', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->setX(105);
        $this->pdf->cell(63, 4.5, 'Selesai Muat Tanggal :.........', 0, 0, 'L', 0);
        $this->pdf->cell(47.5, 4.5, 'Pukul:.........', 0, 0, 'L', 0);
        $this->pdf->Ln(9);
        $this->pdf->setX(105);
        $this->pdf->multicell(190, 4.5, 'Petugas Dinas Luar', 0, 'L');
        $this->pdf->Ln(5);
        $this->pdf->setX(105);
        $this->pdf->cell(28, 4.5, '...........', 0, 0, 'C', 0);
        $this->pdf->cell(28, 4.5, '...........', 0, 0, 'C', 0);
        $this->pdf->cell(28, 4.5, '...........', 0, 0, 'C', 0);
        $Y = $this->pdf->getY();
        $Yawal = $Y;
        $this->pdf->Rect(10, $Yawal+5, 95, 29, 1.5, 'F');
        $this->pdf->Rect(105,$Yawal+5, 95, 29, 1.5, 'F');
        $this->pdf->Ln(5.5);
        $this->pdf->multicell(190, 4.5, 'K. CATATAN PEMASUKAN BRG.EKSPOR KE KAWASAN PABEAN', 0, 'L');
        
        $this->pdf->cell(16, 4.5, 'SEGEL:' ,0, 0, 'L', 0);
        $this->pdf->cell(5, 4.5, '', 1, 0, 'C', 0);
        $this->pdf->cell(16, 4.5, 'Baik', 0, 0, 'L', 0);
        $this->pdf->cell(5, 4.5, '', 1, 0, 'C', 0);
        $this->pdf->cell(16, 4.5, 'Rusak', 0, 0, 'C', 0);
        $this->pdf->cell(5, 4.5, '', 1, 0, 'C', 0); 
        $this->pdf->cell(20, 4.5, 'Tdk Sesuai', 0, 0, 'L', 0);
        $this->pdf->Ln(5);
        $this->pdf->cell(58, 4.5, 'Selesai Masuk Tgl :..........', 0, 0, 'L', 0);
        $this->pdf->cell(30, 4.5, 'Pukul :..........', 0, 0, 'L', 0);
        $this->pdf->Ln(5);
        $this->pdf->multicell(190, 4.5, 'Petugas Dinas Luar', 0, 'L');
        $this->pdf->Ln(5);
        $this->pdf->setX(15);
        $this->pdf->cell(28, 4.5, '...........', 0, 0, 'C', 0);
        $this->pdf->cell(28, 4.5, '...........', 0, 0, 'C', 0);
        $this->pdf->cell(28, 4.5, '...........', 0, 0, 'C', 0);
        $this->pdf->Ln(-24);
        $this->pdf->setX(105);
        $this->pdf->multicell(190, 4.5, 'L. CATATAN PENGELUARAN BARANG EKSPOR DARI TPB', 0, 'L');
        $this->pdf->setX(105);
        $this->pdf->cell(47.5, 4.5, 'Jenis Segel:.........', 0, 0, 'L', 0);
        $this->pdf->cell(47.5, 4.5, 'Nomor Segel:.........', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->setX(105);
        $this->pdf->cell(63, 4.5, 'Selesai Muat Tanggal :.........', 0, 0, 'L', 0);
        $this->pdf->cell(47.5, 4.5, 'Pukul:.........', 0, 0, 'L', 0);
        $this->pdf->Ln(5);
        $this->pdf->setX(105);
        $this->pdf->multicell(190, 4.5, 'Petugas Dinas Luar', 0, 'L');
        $this->pdf->Ln(5.5);
        $this->pdf->setX(105);
        $this->pdf->cell(28, 4.5, '...........', 0, 0, 'C', 0);
        $this->pdf->cell(28, 4.5, '...........', 0, 0, 'C', 0);
        $this->pdf->cell(28, 4.5, '...........', 0, 0, 'C', 0); 
        $this->pdf->Ln();
        $this->pdf->SetFont('courier', '', '7');
        $this->pdf->multicell(190, 4.5, $this->rpt['KAKI30'], 0, 'L');
        $this->pdf->cell(28, 4.5, 'Formulir ini dicetak secara otomatis oleh sistem komputer dan tidak memerlukan nama, tanda tangan pejabat dan cap dinas', 0, 0, 'L', 0); 
        
        
        
    }   
    
    function drawResponLPE(){
        $this->pdf->SetMargins(10, 5, 5, 5);
        $this->pdf->SetAutoPageBreak(true, 5);
        $this->pdf->SetFont('courier', '', '9');
        $this->pdf->setXY(184,0);
        $this->pdf->cell(3, 4, 'BCF 3.03', 0, 0, 'L', 0);
        $Y = $this->pdf->getY();
        $Yawal = $Y;
        $this->pdf->Rect(10, 4, 190, 275, 1.5, 'F');
        $this->pdf->Ln();
        $this->pdf->multicell(176, 6,$this->rpt['KOP'], 0, 'L');
        $this->pdf->Ln(2);
        $this->pdf->SetFont('courier', 'B', '11');
        $this->pdf->multicell(185, 5, $this->rpt['LBLJUDUL'], 0, 'C');
        $this->pdf->multicell(185, 5, $this->rpt['LBLJUDUL2'], 0, 'C');
        $this->pdf->Ln(2);
        $this->pdf->SetFont('courier', 'B', '9');
        $this->pdf->multicell(190, 4.5,'A. KANTOR PABEAN PENERBIT : '.$this->res['URKDKTR'], 1, 'L');
        $this->pdf->SetFont('courier', '', '9');
        $this->pdf->multicell(190, 4.5,'B. PERNYATAAN EKSPORTIR'.$this->res['URKDKTR'], 0, 'L');
        $this->pdf->Rect(10, 45, 95, 27, 1.5, 'F');#
        $this->pdf->Rect(105, 45, 95, 27, 1.5, 'F');#
        $this->pdf->multicell(95, 4.5,'EKSPORTIR(NPWP, Nama, Alamat)', 0, 'L');
        $this->pdf->multicell(95, 4.5,'a. NPWP   : '.$this->hdr['NPWPEKS'], 0, 'L');
        $this->pdf->multicell(95, 4.5,'b. NIPER  : '.$this->hdr['NIPER'], 0, 'L');
        $this->pdf->multicell(95, 4.5,'c. Nama   : '.$this->hdr['NAMAEKS'], 0, 'L');
        $this->pdf->multicell(95, 4.5,'d. Alamat : '.$this->hdr['ALMTEKS'], 0, 'L');
        $this->pdf->Ln(-27);
        $this->pdf->setX(105);
        $this->pdf->multicell(95, 4.5,'No. PEB :'.$this->res['NOPEB'].'Tgl.'.$this->res['NOPEB'], 1, 'L');
        $this->pdf->setX(105);
        $this->pdf->multicell(190, 4.5,'Tanggal Pemeriksaan :'.$this->res['NOPEB'].'Tgl.'.$this->res['NOPEB'], 0, 'L');
        $this->pdf->setX(105);
        $this->pdf->multicell(190, 4.5,'Lokasi Pemeriksaan  :', 0, 'L');
        $this->pdf->Ln(13);
        $this->pdf->Rect(10, 72, 95, 25, 1.5, 'F');#
        $this->pdf->Rect(105,72, 95, 25, 1.5, 'F');#
        $this->pdf->multicell(95, 4.5,'PENERIMA', 0, 'L');
        $this->pdf->multicell(95, 4.5,'a. Nama   : '.$this->hdr['NAMABELI'], 0, 'L');
        $this->pdf->multicell(95, 4.5,'b. Alamat : '.$this->hdr['ALMTBELI'], 0, 'L');
        $this->pdf->multicell(95, 4.5,'c. Negara : '.$this->hdr['URNEGBELI'], 0, 'L');
        $this->pdf->Ln(-22);
        $this->pdf->setX(105);
        $this->pdf->multicell(95, 4.5, 'FASILITAS YANG DIMINTA :', 0, 'L');
        $this->pdf->setX(105);
        $this->pdf->multicell(190, 4.5,'PELABUHAN MUAT ASAL    :'.$this->res['NOPEB'], 0, 'L');
        $this->pdf->setX(105);
        $this->pdf->multicell(190, 4.5,'PELABUHAN MUAT EKSPOR  :', 0, 'L');
        $this->pdf->setX(105);
        $this->pdf->multicell(190, 4.5,'PELABUHAN TUJUAN       :', 0, 'L');
        $this->pdf->Ln(7);
        $this->pdf->Rect(10, 97, 95, 27, 1.5, 'F');#
        $this->pdf->Rect(105,97, 95, 27, 1.5, 'F');#
        $this->pdf->multicell(95, 4.5,'URAIAN BARANG', 0, 'L');
        $this->pdf->multicell(95, 4.5,'== sesuai PEB ==', 0, 'C');
        $this->pdf->Ln(-9);
        $this->pdf->setX(105);
        $this->pdf->multicell(95, 4.5, 'PACKING LIST -', 0, 'L');
        $this->pdf->setX(105);
        $this->pdf->multicell(95, 4.5, 'INVOICE      -', 0, 'L');
        $this->pdf->Rect(105,106, 95, 18, 1.5, 'F');#
        $this->pdf->setX(105);
        $this->pdf->multicell(31.6, 4.5, 'VALUTA   :', 0, 'L');
        $this->pdf->setX(105);
        $this->pdf->multicell(31.6, 4.5, $this->hdr['URKDVAL'], 0, 'C');
        $this->pdf->Ln(-9);
        $this->pdf->setX(136.6);
        $this->pdf->multicell(31.6, 4.5, 'NILAI FOB:', 0, 'L');
        $this->pdf->setX(136.6);
        $this->pdf->multicell(31.6, 4.5, number_format($this->hdr['FOB'], 2, '.', ','), 0, 'C');
        $this->pdf->Ln(-9);
        $this->pdf->setX(168.2);
        $this->pdf->multicell(31.6, 4.5, 'NILAI FOB:',0, 'L');
        $this->pdf->setX(168.2);
        $this->pdf->multicell(31.6, 4.5, number_format($this->hdr['FOB'], 2, '.', ','), 0, 'C');
        $this->pdf->Ln(10);
        $this->pdf->SetFont('courier', 'B', '9');
        $this->pdf->multicell(190, 4.5,'C. HASIL PEMERIKSAAN', 0, 'L');
        $this->pdf->Ln(1.5);
        $this->pdf->SetFont('courier', '', '9');
        $this->pdf->Rect(10, 130, 95, 15, 1.5, 'F');#
        $this->pdf->multicell(190, 4.5,'JUMLAH DAN JENIS KEMASAN', 0, 'L');
        $this->pdf->Rect(10, 145, 95, 25, 1.5, 'F');#
        $this->pdf->Ln(10.5);
        $this->pdf->multicell(190, 4.5,'JUMLAH DAN JENIS KEMASAN', 0, 'L');
        $this->pdf->Rect(105,130, 95, 40, 1.5, 'F');#
        $this->pdf->Ln(-19.5);
        $this->pdf->setX(105);
        $this->pdf->multicell(190, 4.5,'JUMLAH DAN JENIS KEMASAN', 0, 'L');
        $this->pdf->Ln(35.5);
        $this->pdf->cell(10, 8, 'No', 1, 0, 'C', 0);
        $this->pdf->cell(33, 8, 'POS TARIF', 1, 0, 'C', 0); 
        $this->pdf->cell(74, 8, 'URAIAN BARANG', 1, 0, 'C', 0); 
        $this->pdf->cell(40, 8, 'SATUAN', 1, 0, 'C', 0); 
        $this->pdf->cell(33, 8, 'JUMLAH', 1, 0, 'C', 0);
        $this->pdf->Rect(10, 178, 10, 40, 1.5, 'F');#NO
        $this->pdf->Rect(20, 178, 33, 40, 1.5, 'F');#POS
        $this->pdf->Rect(53, 178, 74, 40, 1.5, 'F');#URAIAN
        $this->pdf->Rect(127, 178, 40, 40, 1.5, 'F');#SATUAN
        $this->pdf->Rect(167, 178, 33, 40, 1.5, 'F');#JUMLAH
        $this->pdf->Ln(48);
        $this->pdf->Rect(10, 218, 190, 10, 1.5, 'F');#CATATAN
        $this->pdf->multicell(190, 4.5,'CATATAN PEMERIKSAAN     :', 0, 'L');
        $this->pdf->Ln(5.6);
        $this->pdf->multicell(190, 4.5,'NO. SK MENTERI KEUANGAN :', 0, 'L');
        $this->pdf->Rect(10, 228, 190, 5, 1.5, 'F');#SK
        $this->pdf->Ln(5.6);
        $this->pdf->setX(105);
        $this->pdf->multicell(70, 4.5,'Jakarta, '.date('d-m-Y'), 0, 'L');
        $this->pdf->setX(105);
        $this->pdf->multicell(70, 4.5,'Pejabat Pemeriksa Dokumen Ekspor', 0, 'L');
        $this->pdf->Ln(15);
        $this->pdf->setX(105);
        $this->pdf->multicell(70, 4.5,'Nama :', 0, 'L');
        $this->pdf->setX(105);
        $this->pdf->Ln(15);
        $this->pdf->multicell(190, 4.5, $this->rpt['KAKI40'], 0, 'L');
        
    }
    
    function drawResponSPPBK(){
        $this->pdf->SetMargins(10, 5, 5, 5);
        $this->pdf->SetAutoPageBreak(true, 5);
        $this->pdf->SetFont('courier', 'B', '9');
        $this->pdf->Ln();
        $this->pdf->multicell(176, 6,$this->rpt['KOP'], 0, 'L');
        $this->pdf->Ln(8);
        $this->pdf->multicell(190, 4.5,'SURAT PENETAPAN PERHITUNGAN BEA KELUAR', 0, 'C');
        $this->pdf->Ln(2);
        $this->pdf->SetFont('courier', '', '9');
        $this->pdf->setX(80);
        $this->pdf->multicell(100, 4.5,'Nomor   : SPPBK-'.$this->sppbk['NOSPPBK'], 0, 'L');
        $this->pdf->setX(80);
        $this->pdf->multicell(100, 4.5,'Tanggal :'.$this->sppbk['TGSPPBK'], 0, 'L');
        $this->pdf->Ln();
        $this->pdf->multicell(100, 4.5,'Kepada Yth.', 0, 'L');
        $this->pdf->multicell(100, 4.5,'Nama Eksportir :'.$this->hdr['NAMAEKS'], 0, 'L');
        $this->pdf->multicell(150, 4.5,'Alamat         :'.$this->hdr['ALMTEKS'], 0, 'L');
        $this->pdf->Ln();
        $this->pdf->multicell(190, 6,'Dengan ini diberitahukan bahwa perhitungan Bea Keluar atas Pemberitahuan Pabean Ekspor (PEB) :', 0, 'L');
        $this->pdf->multicell(90, 4.5,'Nomor Pendaftaran :'.$this->hdr['NODAFT'], 0, 'L');
        $this->pdf->multicell(90, 4.5,'Eksportir         :'.$this->hdr['NAMAEKS'], 0, 'L');
        $this->pdf->multicell(90, 4.5,'PPJK              :'.$this->hdr['NAMAPPJK'], 0, 'L');
        $this->pdf->Ln(-13.5);
        $this->pdf->setX(100);
        $this->pdf->multicell(90, 4.5,'Tanggal :'.$this->hdr['TGDAFT'], 0, 'L');
        $this->pdf->setX(100);
        $this->pdf->multicell(90, 4.5,'NPWP    :'.$this->formatNPWP($this->hdr['NPWPEKS']), 0, 'L');
        $this->pdf->setX(100);
        $this->pdf->multicell(90, 4.5,'NPWP    :'.$this->formatNPWP($this->hdr['NPWPPPJK']), 0, 'L');
        $this->pdf->Ln();
        $this->pdf->SetFont('courier', 'B', '9');
        $this->pdf->multicell(190, 4.5,'DITETAPKAN:', 0, 'C');
        $this->pdf->Ln();
        $this->pdf->cell(45, 4.5, 'URAIAN', 1, 0, 'C', 0);
        $this->pdf->cell(44, 4.5, 'DIBERITAHUKAN', 1, 0, 'C', 0);
        $this->pdf->cell(44, 4.5, 'DITETAPKAN', 1, 0, 'C', 0);
        $this->pdf->cell(46, 4.5, 'KEKURANGAN / KELEBIHAN', 1, 0, 'C', 0);
        $this->pdf->SetFont('courier', '', '8');
        $this->pdf->Ln(6);
        $this->pdf->multicell(50, 4.5,'1. Jenis Barang', 0, 'L');
        $this->pdf->multicell(50, 4.5,'2. Satuan Barang', 0, 'L');
        $this->pdf->multicell(50, 4.5,'3. Jumlah Barang', 0, 'L');
        $this->pdf->multicell(50, 4.5,'4. Pos Tarif', 0, 'L');
        $this->pdf->multicell(50, 4.5,'5. Tarif Bea Keluar', 0, 'L');
        $this->pdf->multicell(50, 4.5,'6. Harga Ekspor', 0, 'L');
        $this->pdf->multicell(50, 4.5,'7. Nilai Tukar Mata Uang', 0, 'L');
        $this->pdf->Rect(10, 111.5, 45, 35, 1.5, 'F');#POS
        $this->pdf->Rect(55, 111.5, 44, 35, 1.5, 'F');#URAIAN
        $this->pdf->Rect(99, 111.5, 44, 35, 1.5, 'F');#SATUAN
        $this->pdf->Rect(143, 111.5, 46, 35, 1.5, 'F');#JUMLAH
        if (count($this->dtl) > 0) {
            $lampiran['barang'] = '1';
            $this->pdf->Ln(-30);
            $this->pdf->setX(80);
            $this->pdf->multicell(70, 4, "\n\n=== Lihat lampiran ===", 0, 'C');
        }
        
        $this->pdf->Ln(22);
        $this->pdf->multicell(163, 4.5,'Sehingga menyebabkan kekurangan/kelebihan pembayaran dan/atau denda administrasi, dengan perhitungan sebagai berikut :', 0, 'L');
       
        $this->pdf->SetFont('courier', 'B', '9');
        $this->pdf->Ln();
        $this->pdf->cell(45, 4.5, 'URAIAN', 1, 0, 'C', 0);
        $this->pdf->cell(44, 4.5, 'DIBERITAHUKAN', 1, 0, 'C', 0);
        $this->pdf->cell(44, 4.5, 'DITETAPKAN', 1, 0, 'C', 0);
        $this->pdf->cell(46, 4.5, 'KEKURANGAN / KELEBIHAN', 1, 0, 'C', 0);
        $this->pdf->Rect(10, 166.5, 45, 11, 1.5, 'F');#POS
        $this->pdf->Rect(55, 166.5, 44, 11, 1.5, 'F');#URAIAN
        $this->pdf->Rect(99, 166.5, 44, 11, 1.5, 'F');#SATUAN
        $this->pdf->Rect(143, 166.5, 46, 11, 1.5, 'F');#JUMLAH
        $this->pdf->SetFont('courier', '', '8');
        $this->pdf->Ln(5);
        $this->pdf->multicell(45, 4.5,'1. Bea Keluar', 0, 'L');
        $this->pdf->multicell(45, 4.5,'2. Sanksi Administrasi', 0, 'L');
        $this->pdf->Ln(-9);
        $this->pdf->setX(56);
        $this->pdf->multicell(43, 4.5,number_format($this->hdr['FOB'], 2, '.', ','), 0, 'L');
        $this->pdf->setX(56);
        $this->pdf->multicell(43, 4.5,number_format($this->hdr['FOB'], 2, '.', ','), 0, 'L');
        
        $this->pdf->Ln(-9);
        $this->pdf->setX(100);
        $this->pdf->multicell(43, 4.5,number_format($this->hdr['FOB'], 2, '.', ','), 0, 'L');
        $this->pdf->setX(100);
        $this->pdf->multicell(43, 4.5,number_format($this->hdr['FOB'], 2, '.', ','), 0, 'L');
        
        $this->pdf->Ln(-9);
        $this->pdf->setX(144);
        $this->pdf->multicell(43, 4.5,number_format($this->hdr['FOB'], 2, '.', ','), 0, 'L');
        $this->pdf->setX(144);
        $this->pdf->multicell(43, 4.5,number_format($this->hdr['FOB'], 2, '.', ','), 0, 'L');
        
        $this->pdf->Ln();
        
        
        $this->pdf->multicell(50, 8,'Alasan penetapan :', 0, 'L');
        $this->pdf->setX(25);
        $this->pdf->cell(163, 4.5, 'Dalam hal terdapak kekurangan pembayaran, saudara wajib melunasi kekurangan pembayaran tersebut', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->setX(10);
        $this->pdf->cell(163, 4.5, 'paling lama 60 (enam puluh) hari terhitung sejak tanggal penetapan ini pada '.$this->hdr['URKDKTR'].'.', 0, 0, 'L', 0);
        $this->pdf->Ln();  
        $this->pdf->setX(25);
        $this->pdf->cell(163, 4.5, 'Kekurangan pembayaran yang tidak dilunasi dan tidak diajukan keberatan sampai dengan tanggal ', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->setX(10);
        $this->pdf->cell(163, 4.5, 'jatuh tempo dikenakan bunga sebesar 2% (dua persen) setiap bulan untuk paling lama 24 (dua puluh empat)', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->setX(10);
        $this->pdf->cell(163, 4.5, 'bulan dari jumlah kekurangan pembayaran, bagian bulan dihitung satu bulan penuh.', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->setX(25);
        $this->pdf->cell(163, 4.5, 'Dalam hal terdapat kelebihan pembayaran, saudara dapat m    engajukan permohonan pengembalian sesuai', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->setX(10);
        $this->pdf->cell(163, 4.5, ' ketentuan yang berlaku.', 0, 0, 'L', 0);
        $this->pdf->Ln();  
        $this->pdf->setX(25);
        $this->pdf->cell(163, 4.5, 'Keberatan atas SPPBK ini hanya dapat diajukan secara tertulis kepada Direktorat Jendral Bed dan ', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->setX(10);
        $this->pdf->cell(163, 4.5, 'Cukai paling lama pada tanggal jatuh tempo dengan menyerahkan jaminan sebesar tagihan yang harus dibayar', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->setX(10);
        $this->pdf->cell(163, 4.5, 'atau bukti pelunasan tagihan.', 0, 0, 'L', 0);
        
        $this->pdf->Ln(25);
        $this->pdf->cell(90, 4.5, 'SPPBK ini dibuat rangkap 3(tiga):', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(90, 4.5, '- Lembar ke-1 untuk Ekspor;', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(90, 4.5, '- Lembar ke-2 untuk Kepala Kantor Pabean;', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(90, 4.5, '- Lembar ke-3 untuk arsip Pejabat Bea dan Cukai.', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('courier', '', '5');
        $this->pdf->cell(90, 4.5, '*)dalam hal terdiri dari satu jenis barang, pengisian dilakukan pada lampiran', 0, 0, 'L', 0);
        
        
        $this->pdf->Ln(-30);
         $this->pdf->SetFont('courier', '', '9');
        $this->pdf->setX(150);
        $this->pdf->cell(163, 4.5, 'Pejabar Bea dan Cukai', 0, 0, 'L', 0);
        $this->pdf->Ln(22);
        $this->pdf->setX(150);
        $this->pdf->cell(163, 4.5, 'NIP', 0, 0, 'L', 0);
        
        
        
        
        
    }
    
    function footerResponUmum(){
        $this->pdf->SetY(-17);
        $this->pdf->SetFont('courier','I',8);
        $this->pdf->cell(150, 5, $this->rpt['LBL40'], 0, 0, 'L', 0);         
    }
    
//RESPON  

    function dataLampiranBarangg($idPrint = 0) {
        $this->headerLampiran('barang', 'LANJUTAN BARANG');
        $this->pdf->SetFont('times', '', '8');
        $Yawl = $this->pdf->getY();
        $Y = $this->pdf->getY();
        $totData = count($this->dtl);
        for ($i = $idPrint; $i < $totData; $i++) {
            $h = '';
            $this->pdf->setY($Yawl);
            $this->pdf->multicell(7, 3.5, ($i + 1), 0, 'L');
            $h[] = $this->pdf->getY();
            //Uraiana
            $this->pdf->setXY($this->pdf->getX() + 7, $Yawl);
            $fas = ($this->dtl[$i]['KDFASDTL'] != '') ? "\nFas : " . $this->dtl[$i]['KDFASDTL'] . '/' . $this->dtl[$i]['URKDFASDTL'] : '';
            $urai = $this->formaths($this->dtl[$i]['NOHS']) . "\n" . $this->dtl[$i]['BRGURAI'] . "\n" . $this->dtl[$i]['MERK'] . " - " . $this->dtl[$i]['TIPE'] . " - " . $this->dtl[$i]['SPFLAIN'] . $fas;
            $this->pdf->multicell(72, 3.5, $urai, 0, 'L');
            $h[] = $this->pdf->getY();
            //Barang asal
            $this->pdf->setXY($this->pdf->getX() + 79, $Yawl);
            $this->pdf->multicell(30, 3.5, $this->dtl[$i]['BRGASAL'] . ' / ' . $this->dtl[$i]['URBRGASAL'], 0, 'L');
            $h[] = $this->pdf->getY();
            //Perhitungan
            $this->pdf->setXY($this->pdf->getX() + 109, $Yawl);
            $FASBM = ($this->dtl[$i]['FASBM'] > 0) ? $this->getfas($this->dtl[$i]['KDFASBM']) . ' : ' . ($this->dtl[$i]['FASBM'] / 100) . ' %' : '';
            $BM = $this->strip($this->dtl[$i]['TRPBM']) . ' ' . $FASBM;
            $FASCUK = ($this->dtl[$i]['FASCUK'] > 0) ? $this->getfas($this->dtl[$i]['KDFASCUK']) . ' : ' . ($this->dtl[$i]['FASCUK'] / 100) . ' %' : '';
            $CUK = $this->strip($this->dtl[$i]['TRPCUK']) . ' ' . $FASCUK;
            $FASPPN = ($this->dtl[$i]['FASPPN'] > 0) ? $this->getfas($this->dtl[$i]['KDFASPPN']) . ' : ' . ($this->dtl[$i]['FASPPN'] / 100) . ' %' : '';
            $PPN = $this->strip($this->dtl[$i]['TRPPPN']) . ' ' . $FASPPN;
            $FASPBM = ($this->dtl[$i]['FASPBM'] > 0) ? $this->getfas($this->dtl[$i]['KDFASPBM']) . ' : ' . ($this->dtl[$i]['FASPBM'] / 100) . ' %' : '';
            $PBM = $this->strip($this->dtl[$i]['TRPPBM']) . ' ' . $FASPBM;
            $FASPPH = ($this->dtl[$i]['FASPPH'] > 0) ? $this->getfas($this->dtl[$i]['KDFASPPH']) . ' : ' . ($this->dtl[$i]['FASPPH'] / 100) . ' %' : '';
            $PPH = $this->strip($this->dtl[$i]['TRPPPH']) . ' ' . $FASPPH;
            $this->pdf->multicell(30, 3.5, 'BM : ' . $BM . "\nCukai : " . $CUK . "\nPPN : " . $PPN . "\nPBM : " . $PBM . "\nPPh : " . $PPH, 0, 'L');
            $h[] = $this->pdf->getY();
            //kemasan
            $this->pdf->setXY($this->pdf->getX() + 139, $Yawl);
            $satuan = ($this->dtl[$i]['KDSAT'] != '') ? ' ' . $this->dtl[$i]['KDSAT'] . ' / ' . $this->dtl[$i]['URKDSAT'] . ',' . ' BB: ' . number_format($this->dtl[$i]['NETTODTL'], 4, '.', ',') . ' Kg' : '';
            $kms = ((int) $this->dtl[$i]['KEMASJM'] > 0) ? "\n" . $this->dtl[$i]['KEMASJM'] . ' ' . $this->dtl[$i]['KEMASJN'] . ' / ' . $this->dtl[$i]['URKEMASJN'] : '';
            $this->pdf->multicell(30, 3.5, number_format($this->dtl[$i]['JMLSAT'], 4, '.', ',') . $satuan . $kms, 0, 'L');
            $h[] = $this->pdf->getY();
            //Jumlah Nilain CIF
            $this->pdf->setXY($this->pdf->getX() + 169, $Yawl);
            $this->pdf->multicell(30.2, 3.5, number_format($this->dtl[$i]['DCIF'], 4, '.', ','), 0, 'R');
            $Yawl = max($h) + 2;
            if ($Yawl > 230) {
                $i++;
                break;
            }
        }
        //Rect($x, $y, $w, $h, $style='')
        $this->pdf->Rect(5.4, $Y, 7, ($Yawl - $Y));
        $this->pdf->Rect(12.4, $Y, 72, ($Yawl - $Y));
        $this->pdf->Rect(84.4, $Y, 30, ($Yawl - $Y));
        $this->pdf->Rect(114.4, $Y, 30, ($Yawl - $Y));
        $this->pdf->Rect(144.4, $Y, 30, ($Yawl - $Y));
        $this->pdf->Rect(174.6, $Y, 30.2, ($Yawl - $Y));
        $this->footerLampiran();
        if ($i < $totData) {
            $this->dataLampiranBarang($i);
        }
    }
    
    function dataLampiranDokumen($noPrint, $idPrint = 0) {
        $this->headerLampiran('dokumen', 'LANJUTAN DOKUMEN');
        $this->pdf->SetFont('times', '', '8');
        $Yawl = $this->pdf->getY();
        $Y = $this->pdf->getY();
        $totData = count($this->dok);
        for ($i = $idPrint; $i < $totData; $i++) {
            if (!strstr($noPrint, $this->dok[$i]['DOKKD'])) {
                $h = '';
                $this->pdf->setY($Yawl);
                //Uraian Kode Dokumen
                $this->pdf->cell(10, 4, '', 0, 0, 'L', 0);
                $this->pdf->multicell(65, 4, $this->dok[$i]['URDOKKD'], 0, 'L');
                $h[] = $this->pdf->getY();
                //Nomor Dokumen
                $this->pdf->setXY($this->pdf->getX() + 75, $Yawl);
                $this->pdf->multicell(64.2, 4, $this->dok[$i]['DOKNO'], 0, 'L');
                $h[] = $this->pdf->getY();
                //Tanggal Dokumen
                $this->pdf->setXY($this->pdf->getX() + 139.2, $Yawl);
                $this->pdf->multicell(60, 4, $this->dok[$i]['DOKTG'], 0, 'L');
                $h[] = $this->pdf->getY();

                $Yawl = max($h);
                if ($Yawl > 230) {
                    $i++;
                    break;
                }
            }
        }
        $this->pdf->Rect(5.4, $Y, 199.3, ($Yawl - $Y));
        $this->footerLampiran();
        if ($i < $totData) {
            $this->dataLampiranDokumen($noPrint, $i);
        }
    }
    
    function dataLampiranKemasan($idPrint = 0) {
        $this->headerLampiran('kemasan', 'LANJUTAN KEMASAN');
        $this->pdf->SetFont('times', '', '8');
        $Yawl = $this->pdf->getY();
        $Y = $this->pdf->getY(); //print_r($this->kms);die();
        $totData = count($this->kms);
        for ($i = $idPrint; $i < $totData; $i++) {
            $h = '';
            $this->pdf->setY($Yawl);
            //Uraian Kode Dokumen
            $this->pdf->cell(10, 4, '', 0, 0, 'L', 0);
            $this->pdf->multicell(35, 4, $this->kms[$i]['JMKEMAS'], 0, 'R');
            $h[] = $this->pdf->getY();
            //Nomor Dokumen
            $this->pdf->setXY($this->pdf->getX() + 45, $Yawl);
            $this->pdf->multicell(79.2, 4, $this->kms[$i]['JNKEMAS'] . ' / ' . $this->kms[$i]['URJNKEMAS'], 0, 'L');    
            $h[] = $this->pdf->getY();
            //Tanggal Dokumen
            $this->pdf->setXY($this->pdf->getX() + 124.2, $Yawl);
            $this->pdf->multicell(75, 4, $this->kms[$i]['MERKKEMAS'], 0, 'L');
            $h[] = $this->pdf->getY();
            $Yawl = max($h);
            if ($Yawl > 230) {
                $i++;
                break;
            }
        }
        $this->pdf->Rect(5.4, $Y, 199.3, ($Yawl - $Y));
        $this->footerLampiran();
        if ($i < $totData) {
            $this->dataLampiranKemasan($i);
        }
    }
    
    function dataLampiranKontainer($idPrint = 0) {
        $this->headerLampiran('kontainer', 'LANJUTAN KEMASAN');
        $this->pdf->SetFont('times', '', '8');
        $totData = count($this->con);
        $Y = $this->pdf->getY();
        for ($i = $idPrint; $i < $totData; $i++) {
            $this->pdf->Ln();
            $this->pdf->cell(12.6, 4, ($i + 1), 0, 0, 'C', 0);
            $this->pdf->cell(37, 4, $this->setnocont($this->con[$i]['CONTNO']), 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, $this->con[$i]['CONTUKUR'], 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, $this->con[$i]['CONTTIPE'], 0, 0, 'L', 0);
            $i++;
            $this->pdf->cell(12.6, 4, ($this->con[$i]['CONTNO'] != '') ? ($i + 1) : '', 'L', 0, 'C', 0);
            $this->pdf->cell(37, 4, $this->setnocont($this->con[$i]['CONTNO']), 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, $this->con[$i]['CONTUKUR'], 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, $this->con[$i]['CONTTIPE'], 0, 0, 'L', 0);
        }
        $this->pdf->Ln();
        $this->pdf->Rect(5.4, $Y, 199.3, ( $this->pdf->getY() - $Y));
        $this->footerLampiran();
        if ($i < $totData) {
            $this->dataLampiranKontainer($i);
        }
    }
    
    function footerLampiran() {
        $this->pdf->setXY(150, 255);
        $this->pdf->SetFont('times', '', '9');
        $imp = ($this->tp_trader == '1') ? 'Importir' : 'PPJK';
        $this->pdf->multicell(50, 4, "Jakarta, " . $this->hdr['TANGGAL_TTD'] . "\n" . $imp . "\n\n\n\n\n\n" . $this->hdr['NAMA_TTD'], 0, 'C');
        $this->pdf->SetFont('times', 'I', '9');
        $this->pdf->multicell(99.6, 4, "Tgl.Cetak " . date('d-m-Y'), 0, 'L');
    }    
    
    function drawLembarCatPencocokan(){        
        $noPrinLampiranDok = '';
        $this->pdf->SetMargins(5.4, 0, 0);
        $this->pdf->SetAutoPageBreak(0, 0);
        $this->pdf->SetY(5.4);
        $this->pdf->SetFont('times', 'B', '12');        
        $this->pdf->cell(200, 4, 'LEMBAR LAMPIRAN III', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(200, 4, 'PEMBERITAHUAN IMPOR BARANG UNTUK DITIMBUN ', 0, 0, 'C', 0);
        $this->pdf->Ln(); 
        $this->pdf->cell(200, 4, 'DI TEMPAT PENIMBUNAN BERIKAT', 0, 0, 'C', 0);              
        $this->pdf->Ln(10);         
        $this->pdf->cell(200, 4, 'UNTUK CATATAN PENCOCOKAN', 0, 0, 'C', 0);
        $this->pdf->SetX(137);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->cell(67.6, 4, 'BC 2.3', 10, 0, 'R', 0);
        
        //KPBC
        $this->pdf->Ln(2);                 
        $this->pdf->Rect(5.4, 29.4, 199, 14, 1.5, 'F');
        $this->pdf->Ln();
        $this->pdf->cell(45, 4, 'Kantor Pabean', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(103, 4, $this->hdr['URKDKPBC'], 0, 0, 'L', 0);
        $this->pdf->cell(15, 4, $this->hdr['KDKPBC'], 1, 0, 'C', 0);                
        $this->pdf->Ln(); 
        $this->pdf->cell(45, 4, 'Nomor Pengajuan', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(80, 4, substr($this->hdr['CAR'], 0, 6) . " - " . substr($this->hdr['CAR'], 6, 6) . " - " . substr($this->hdr['CAR'], 12, 8) . " - " . substr($this->hdr['CAR'], 20), 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(45, 4, 'Nomor Pendaftaran', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(103, 4, $this->hdr[''], 0, 0, 'L', 0);
        $this->pdf->setX(73);
        $this->pdf->cell(15, 4, 'Tgl.', 0, 0, 'C', 0);               
        
        $this->pdf->Ln(7);
        $this->pdf->cell(45, 4, 'DIISI DALAM HAL DILAKUKAN  PENCOCOKAN JUMLAH & JENIS KEMASAN/PETI KEMAS', 0, 0, 'L', 0);
        $this->pdf->Ln(10);
        $this->pdf->cell(20, 4, 'PETUGAS', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(20, 4, 'NAMA', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(20, 4, 'NIP', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        
        $this->pdf->Ln();
        $this->pdf->setX(110);
        $yAWL = $this->pdf->getY();
        $this->pdf->multicell(49.3, 4, "...................................", 0, 'C');
        $this->pdf->setY($yAWL);
        $this->pdf->setX(141);
        $this->pdf->multicell(49.3, 4, "Tgl ...................................", 0, 'C');        
        $this->pdf->setX(120);        
        $this->pdf->cell(49.3, 4, 'Pejabat', 0, 0, 'L', 0);
        $this->pdf->Ln(8);
        $this->pdf->setX(120);        
        $this->pdf->cell(49.3, 4, 'ttd', 0, 0, 'L', 0);
        
        $this->pdf->Ln(10);
        $this->pdf->setX(120);
        $this->pdf->cell(20, 4, 'NAMA', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->setX(120);
        $this->pdf->cell(20, 4, 'NIP.', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        
        $this->pdf->Ln(10);
        $this->pdf->cell(45, 4, 'TEMPAT PENCOCOKAN', 0, 0, 'L', 0);              
        $this->pdf->setX(120);
        $this->pdf->cell(45, 4, 'TGL. PENCOCOKAN', 0, 0, 'L', 0);
        
        $this->pdf->Rect(5.4, 43.5, 199, 235, 1.5, 'F');
        
        $this->pdf->Ln(120);
        $this->pdf->setX(110);
        $yAWL = $this->pdf->getY();
        $this->pdf->multicell(49.3, 4, "...................................", 0, 'C');
        $this->pdf->setY($yAWL);
        $this->pdf->setX(141);
        $this->pdf->multicell(49.3, 4, "Tgl ...................................", 0, 'C');        
        $this->pdf->setX(120);        
        $this->pdf->cell(49.3, 4, 'Pemeriksa Bea dan Cukai', 0, 0, 'L', 0);
        $this->pdf->Ln(8);
        $this->pdf->setX(120);        
        $this->pdf->cell(49.3, 4, 'ttd', 0, 0, 'L', 0);
        
        $this->pdf->Ln(10);
        $this->pdf->setX(120);
        $this->pdf->cell(20, 4, 'NAMA', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->setX(120);
        $this->pdf->cell(20, 4, 'NIP', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
    }
    
    function drawLembarCatPemerikasaanFisikBrg(){
        $noPrinLampiranDok = '';
        $this->pdf->SetMargins(5.4, 0, 0);
        $this->pdf->SetAutoPageBreak(0, 0);
        $this->pdf->SetY(5.4);
        $this->pdf->SetFont('times', 'B', '12');        
        $this->pdf->cell(200, 4, 'LEMBAR LAMPIRAN IV', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(200, 4, 'PEMBERITAHUAN IMPOR BARANG UNTUK DITIMBUN ', 0, 0, 'C', 0);
        $this->pdf->Ln(); 
        $this->pdf->cell(200, 4, 'DI TEMPAT PENIMBUNAN BERIKAT', 0, 0, 'C', 0);              
        $this->pdf->Ln(10);         
        $this->pdf->cell(200, 4, 'UNTUK CATATAN PEMERIKSAAN FISIK BARANG', 0, 0, 'C', 0);
        
        $this->pdf->SetX(137);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->cell(67.6, 4, 'BC 2.3', 10, 0, 'R', 0);
        //KPBC
        $this->pdf->Ln(2);                 
        $this->pdf->Rect(5.4, 29.4, 199, 14, 1.5, 'F');
        $this->pdf->Ln();
        $this->pdf->cell(45, 4, 'Kantor Pabean', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(103, 4, $this->hdr['URKDKPBC'], 0, 0, 'L', 0);
        $this->pdf->cell(15, 4, $this->hdr['KDKPBC'], 1, 0, 'C', 0);                
        $this->pdf->Ln();
        $this->pdf->cell(45, 4, 'Nomor Pengajuan', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(80, 4, substr($this->hdr['CAR'], 0, 6) . " - " . substr($this->hdr['CAR'], 6, 6) . " - " . substr($this->hdr['CAR'], 12, 8) . " - " . substr($this->hdr['CAR'], 20), 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(45, 4, 'Nomor Pendaftaran', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(103, 4, $this->hdr[''], 0, 0, 'L', 0);
        $this->pdf->setX(73);
        $this->pdf->cell(15, 4, 'Tgl.', 0, 0, 'C', 0);               
        
        $this->pdf->Ln(7);
        $this->pdf->cell(45, 4, 'DIISI DALAM HAL DILAKUKAN  PEMERIKSAAN FISIK BARANG', 0, 0, 'L', 0);
        $this->pdf->Ln(10);
        $this->pdf->cell(20, 4, 'PETUGAS', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(20, 4, 'NAMA', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(20, 4, 'NIP', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(20, 4, 'TINGKAT PEMERIKSAAN', 0, 0, 'L', 0);        
        
        $this->pdf->Ln();
        $this->pdf->setX(110); 
        $yAWL = $this->pdf->getY();
        $this->pdf->multicell(49.3, 4, "...................................", 0, 'C');
        $this->pdf->setY($yAWL);
        $this->pdf->setX(141);
        $this->pdf->multicell(49.3, 4, "Tgl ...................................", 0, 'C');        
        $this->pdf->setX(120);        
        $this->pdf->cell(49.3, 4, 'Pejabat', 0, 0, 'L', 0);
        $this->pdf->Ln(8);
        $this->pdf->setX(120);        
        $this->pdf->cell(49.3, 4, 'ttd', 0, 0, 'L', 0);
        
        $this->pdf->Ln(10);
        $this->pdf->setX(120);
        $this->pdf->cell(20, 4, 'NAMA', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->setX(120);
        $this->pdf->cell(20, 4, 'NIP', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        
        $this->pdf->Ln(10);
        $this->pdf->cell(45, 4, 'TEMPAT PEMERIKSAAN FISIK', 0, 0, 'L', 0);              
        $this->pdf->setX(120);
        $this->pdf->cell(45, 4, 'TGL. DILAKUKAN PEMERIKSAAN FISIK BARANG', 0, 0, 'L', 0);
        $this->pdf->Ln(10   );
        $this->pdf->cell(45, 4, 'IKHTISAR PEMERIKSAAN', 0, 0, 'L', 0);
        
        $this->pdf->Rect(5.4, 43.5, 199, 235, 1.5, 'F');
        
        $this->pdf->Ln(120);
        $this->pdf->setX(110);
        $yAWL = $this->pdf->getY();
        $this->pdf->multicell(49.3, 4, "...................................", 0, 'C');
        $this->pdf->setY($yAWL);
        $this->pdf->setX(141);
        $this->pdf->multicell(49.3, 4, "Tgl ...................................", 0, 'C');        
        $this->pdf->setX(120);        
        $this->pdf->cell(49.3, 4, 'Pemeriksa Bea dan Cukai', 0, 0, 'L', 0);
        $this->pdf->Ln(8);
        $this->pdf->setX(120);        
        $this->pdf->cell(49.3, 4, 'ttd', 0, 0, 'L', 0);
        
        $this->pdf->Ln(10);
        $this->pdf->setX(120);
        $this->pdf->cell(20, 4, 'NAMA', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->setX(120);
        $this->pdf->cell(20, 4, 'NIP', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        
    }
    
    function trimstr($strpotong, $panjang) {
        $hsl = '';
        $strpotong = trim($strpotong);
        $len = strlen($strpotong);
        $str = 0;
        while ($str < $len) {
            $hsl[] = substr($strpotong, $str, $panjang);
            $str += $panjang;
        }
        return $hsl;
    }
    
    function formatNPWP($npwp) {
        $strlen = strlen($npwp);
        if ($strlen == 15) {
            $npwpnya = substr($npwp, 0, 2) . "." . substr($npwp, 2, 3) . "." . substr($npwp, 5, 3) . "." . substr($npwp, 8, 1) . "-" . substr($npwp, 9, 3) . "." . substr($npwp, 12, 3);
        } else if ($strlen == 12) {
            $npwpnya = substr($npwp, 0, 2) . "." . substr($npwp, 2, 3) . "." . substr($npwp, 5, 3) . "." . substr($npwp, 8, 1) . "-" . substr($npwp, 9, 3);
        } else {
            $npwpnya = $npwp;
        }
        return $npwpnya;
    }
    
    function setnocont($nocont) {
        return substr($nocont, 0, 4) . '-' . substr($nocont, 4, 11);
    }
    
    function formaths($hs) {
        $formaths = substr($hs, 0, 4) . '.' . substr($hs, 4, 2) . '.' . substr($hs, 6, 4);
        return $formaths;
    }
    
    function strip($strstrip) {
        if (trim($strstrip) != 0) {
            $hasile = $strstrip . '%';
        } else {
            $hasile = ' - ';
        }
        return $hasile;
    }
    
    function showDok($jnDok, $arrDok) {//'380|365|861'
        $hasil = array();
        foreach ($arrDok as $a) {
            if (strstr($jnDok, $a['DOKKD'])) {
                $hasil['NO'][] = $a['DOKNO'];
                $hasil['TG'][] = $a['DOKTG'];
            }
        }
        return $hasil;
    }
    
    function formatDokArr($arr) {
        $hasil = '';
        foreach ($arr as $a) {
            $hasil[$a['DOKKD']]['NO'][] = $a['DOKNO'];
            $hasil[$a['DOKKD']]['TG'][] = $a['DOKTG'];
        }
        return $hasil;
    }
    
    function getfas($kodefas) {
        switch ($kodefas) {
            case 1:
                $kdfaspbm_nama = 'DTP';
                break;
            case 2:
                $kdfaspbm_nama = 'DTG';
                break;
            case 3:
                $kdfaspbm_nama = 'BKL';
                break;
            case 4:
                $kdfaspbm_nama = 'BBS';
                break;
        }
        return $kdfaspbm_nama;
    }
    
    function terbilang($bilangan) {
        $angka = array('0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
        $kata = array('', 'satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 'tujuh', 'delapan', 'sembilan');
        $tingkat = array('', 'ribu', 'juta', 'milyar', 'triliun');
        $panjang_bilangan = strlen($bilangan);

        if ($panjang_bilangan > 15) {
            $kalimat = "Diluar Batas";
            return $kalimat;
        }

        /* mengambil angka-angka yang ada dalam bilangan, dimasukkan ke dalam array */
        for ($i = 1; $i <= $panjang_bilangan; $i++) { 
            $angka[$i] = substr($bilangan, -($i), 1);
        }

        $i = 1;
        $j = 0;
        $kalimat = "";

        /* mulai proses iterasi terhadap array angka */
        while ($i <= $panjang_bilangan) {
            $subkalimat = "";
            $kata1 = "";
            $kata2 = "";
            $kata3 = "";

            /* untuk ratusan */
            if ($angka[$i + 2] != "0") {
                if ($angka[$i + 2] == "1") {
                    $kata1 = "seratus";
                } else {
                    $kata1 = $kata[$angka[$i + 2]] . " ratus";
                }
            }

            /* untuk puluhan atau belasan */
            if ($angka[$i + 1] != "0") {
                if ($angka[$i + 1] == "1") {
                    if ($angka[$i] == "0") {
                        $kata2 = "sepuluh";
                    } elseif ($angka[$i] == "1") {
                        $kata2 = "sebelas";
                    } else {
                        $kata2 = $kata[$angka[$i]] . " belas";
                    }
                } else {
                    $kata2 = $kata[$angka[$i + 1]] . " puluh";
                }
            }

            /* untuk satuan */
            if ($angka[$i] != "0") {
                if ($angka[$i + 1] != "1") {
                    $kata3 = $kata[$angka[$i]];
                }
            }

            /* pengujian angka apakah tidak nol semua, lalu ditambahkan tingkat */
            if (($angka[$i] != "0") OR ( $angka[$i + 1] != "0") OR ( $angka[$i + 2] != "0")) {
                $subkalimat = "$kata1 $kata2 $kata3 " . $tingkat[$j] . " ";
            }

            /* gabungkan variabel sub kalimat (untuk satu blok 3 angka) ke variabel kalimat */
            $kalimat = $subkalimat . $kalimat;
            $i = $i + 3;
            $j = $j + 1;
        }
        /* mengganti satu ribu jadi seribu jika diperlukan */
        if (($angka[5] == "0") AND ( $angka[6] == "0")) {
            $kalimat = str_replace("satu ribu", "seribu", $kalimat);
        }
        return trim($kalimat);
    }
    
    function formatTglMysql($tglMysql){
        $arr = explode('-', $tglMysql);
        return $arr[2].' '.$this->bulan[(int)$arr[1]].' '.$arr[0];
    }
    
    function formatCar($CAR){
        //$arr = explode('-', $tglMysql);
        return substr($CAR, 0,6).'-'.substr($CAR, 6,6).'-'.substr($CAR, 12,8).'-'.substr($CAR, 20,6);
    }
    
    function fixLen($str, $len, $chr = ' ', $alg = STR_PAD_RIGHT) {
        $hasil = str_pad(substr($str, 0, $len), $len, $chr, $alg);
        return $hasil;
    }

    
}
