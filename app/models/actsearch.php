<?php
if (!defined('BASEPATH')){exit('No direct script access allowed');}
class actSearch extends CI_Model {
    function search($tipe = "", $indexField = "", $formName = "", $getdata = "") {
        $this->load->library('newtable');
        $KODETRADER = $this->newsession->userdata('KODE_TRADER');
        switch ($tipe) {
            case'barang':
                if ($getdata != "") {
                    $getdata = explode(";", $getdata);
                    $getdata = " AND NOHS='".str_replace(".", "", $getdata[0])."' AND SERITRP='".str_replace('.','', $getdata[1])."' ";
                }
                $judul = "Pencarian Data Barang";
                $SQL = "SELECT  KODE_TRADER 'Kode Trader',
                                BRGURAI 'Uraian&nbsp;Barang',
                                MERK 'Merk',
                                TIPE 'Tipe',
                                SPFLAIN 'SPF&nbsp;Lain',
                                f_formaths(NOHS) 'Kode HS',
                                SERITRP 'Seri HS'
			FROM m_trader_barang 
			WHERE KODE_TRADER='" . $KODETRADER . "' " . $getdata." ";
                $order = "2";
                $hidden = array("Kode Trader", "Kode HS", "Seri HS");
                $sort = "ASC";
                $key = array("Uraian&nbsp;Barang", "Merk", "Tipe", "SPF&nbsp;Lain", "Kode HS", "Seri HS");
                $search = array(array('KODE_BARANG', 'Kode Barang'), array('URAIAN_BARANG', 'Uraian Barang'));
                $field = explode(";", $indexField);
                $show_search = true;
				//die($SQL);
                break;
            case'satuan':
                if ($getdata != "") {
                    $data = str_replace(";", "','", $getdata);
                    $data = substr($data, 0, strlen($data) - 3);
                    $SQL = "SELECT KDEDI 'Kode Satuan',UREDI 'Uraian Satuan' FROM m_satuan WHERE KDEDI IN('" . $data . "')";
                    $show_search = false;
                } else {
                    $SQL = "SELECT KDEDI 'Kode Satuan',UREDI 'Uraian Satuan' FROM m_satuan ";
                    $show_search = true;
                }
                $judul = "Pencarian Data Satuan";
                $order = "1";
                $hidden = array();
                $sort = "ASC";
                $key = array("Kode Satuan", "Uraian Satuan");
                $search = array(array('KDEDI', 'Kode Satuan'), array('UREDI', 'Uraian Satuan'));
                $field = explode(";", $indexField);
                break;
            case'pemasok':
                $judul = "Pencarian Data Pemasok";
                $SQL = "SELECT CONCAT('&nbsp;',f_ref('KODE_ID',KODE_ID_PARTNER),'<br>',ID_PARTNER) 'Identitas',NAMA_PARTNER 'Nama Pemasok', 
						   ALAMAT_PARTNER 'Alamat Pemasok', KODE_ID_PARTNER,ID_PARTNER FROM m_trader_pemasok 
						   WHERE KODE_TRADER='" . $KODETRADER . "'";
                $order = "2";
                $sort = "ASC";
                $hidden = array("KODE_ID_PARTNER", "ID_PARTNER");
                $key = array("KODE_ID_PARTNER", "ID_PARTNER", "Nama Pemasok", "Alamat Pemasok");
                $search = array(array('ID_PARTNER', 'No. Identitas'),
                    array('NAMA_PARTNER', 'Nama Pemasok'),
                    array('ALAMAT_PARTNER', 'Alamat Pemasok'));
                $field = explode(";", $indexField);
                $show_search = true;
                break;
            case'partner':
                $judul = "Pencarian Data Pemasok";
                $SQL = "SELECT KODE_PARTNER 'Kode Perusahan',NAMA_PARTNER 'Nama Perusahaan' FROM m_trader_pemasok 
						   WHERE KODE_TRADER='" . $KODETRADER . "'";
                $order = "2";
                $sort = "ASC";
                $hidden = array();
                $key = array("Kode Perusahan", "Nama Perusahaan");
                $search = array(array('KODE_PARTNER', 'Kode Perusahan'),
                    array('NAMA_PARTNER', 'Nama Perusahaan'));
                $field = explode(";", $indexField);
                $show_search = true;
                break;
            case'pembeli':
                $judul = "Pencarian Data Pembeli";
                $SQL = "SELECT NAMA_PARTNER 'Nama Pemasok',ALAMAT_PARTNER 'Alamat Pemasok',
				           f_negara(NEGARA_PARTNER) 'Negara Pemasok', NEGARA_PARTNER 'Negara' 
						   FROM m_trader_pemasok WHERE KODE_TRADER='" . $KODETRADER . "'";
                $order = "1";
                $hidden = array("Negara");
                $sort = "ASC";
                $key = array("Nama Pemasok", "Alamat Pemasok", "Negara");
                $search = array(array('NAMA_PARTNER', 'Nama Pemasok'), array('ALAMAT_PARTNER', 'Alamat Pemasok'));
                $field = explode(";", $indexField);
                $show_search = true;
                break;
            case'kemasan':
                $judul = "Pencarian Data Kemasan";
                $SQL = "SELECT KODE_KEMASAN 'Kode Kemasan',URAIAN_KEMASAN 'Uraian Kemasan' FROM m_kemasan ";
                $order = "1";
                $hidden = array();
                $sort = "ASC";
                $key = array("Kode Kemasan", "Uraian Kemasan");
                $search = array(array('KODE_KEMASAN', 'Kode Kemasan'), array('URAIAN_KEMASAN', 'Uraian Kemasan'));
                $field = explode(";", $indexField);
                $show_search = true;
                break;
            case'negara':
                $judul = "Pencarian Data Negara";
                $SQL = "SELECT KODE_NEGARA 'Kode Negara',URAIAN_NEGARA 'Uraian Negara' FROM m_negara ";
                $order = "1";
                $hidden = array();
                $sort = "ASC";
                $key = array("Kode Negara", "Uraian Negara");
                $search = array(array('KODE_NEGARA', 'Kode Negara'), array('URAIAN_NEGARA', 'Uraian Negara'));
                $field = explode(";", $indexField);
                $show_search = true;
                break;
            case'fasilitas':
                $judul = "Pencarian Data Fasilitas";
                $SQL = "SELECT KDREC 'Kode Fasilitas',URAIAN 'Uraian Fasilitas' FROM m_tabel WHERE MODUL = 'BC20' AND KDTAB='SKEP_FASILITAS'";
                $order = "1";
                $hidden = array();
                $sort = "ASC";
                $key = array("Kode Fasilitas", "Uraian Fasilitas");
                $search = array(array('KDREC', 'Kode Fasilitas'), array('URAIAN', 'Uraian Fasilitas'));
                $field = explode(";", $indexField);
                $show_search = true;
                break;
            case'pelabuhan':
                $judul = "Pencarian Data Pelabuhan";
                $SQL = "SELECT KODE_PELABUHAN 'Kode Pelabuhan',URAIAN_PELABUHAN 'Uraian Pelabuhan' FROM m_pelabuhan ";
                $order = "1";
                $hidden = array();
                $sort = "ASC";
                $key = array("Kode Pelabuhan", "Uraian Pelabuhan");
                $search = array(array('KODE_PELABUHAN', 'Kode Pelabuhan'), array('URAIAN_PELABUHAN', 'Uraian Pelabuhan'));
                $field = explode(";", $indexField);
                $show_search = true;
                break;
            case'valuta':
                $judul = "Pencarian Data Valuta";
                $SQL = "SELECT KDEDI 'Kode Valuta',UREDI 'Uraian Valuta' FROM m_valuta ";
                $order = "1";
                $hidden = array();
                $sort = "ASC";
                $key = array("Kode Valuta", "Uraian Valuta");
                $search = array(array('KDEDI', 'Kode Valuta'), array('UREDI', 'Uraian Valuta'));
                $field = explode(";", $indexField);
                $show_search = true;
                break;
            case'valuta2':
                $judul = "Pencarian Data Valuta";
                $SQL = "SELECT KDEDI 'Kode Valuta',UREDI 'Uraian Valuta', KDEDI FROM m_valuta ";
                $order = "1";
                $hidden = array('KDEDI');
                $sort = "ASC";
                $key = array("Kode Valuta", "KDEDI");
                $search = array(array('KDEDI', 'Kode Valuta'), array('UREDI', 'Uraian Valuta'));
                $field = explode(";", $indexField);
                $show_search = true;
                break;
            case'timbun':
                $judul = "Pencarian Data Penimbunan";
                $SQL = "SELECT KDGDG 'Kode Timbun',URAIAN 'Uraian Timbun' FROM m_gudang WHERE KDKPBC='" . substr($getdata, 0, -1) . "'";
                $order = "1";
                $hidden = array();
                $sort = "ASC";
                $key = array("Kode Timbun", "Uraian Timbun");
                $search = array(array('KDGDG', 'Kode Timbun'), array('URAIAN', 'Uraian Timbun'));
                $field = explode(";", $indexField);
                $show_search = true;
                break;
            case'kpbc':
                $judul = "Pencarian Data KPBC";
                $SQL = "SELECT KDKPBC 'Kode Kpbc',URAIAN_KPBC 'Uraian Kpbc' FROM m_kpbc ";
                $order = "1";
                $hidden = array();
                $sort = "ASC";
                $key = array("Kode Kpbc", "Uraian Kpbc");
                $search = array(array('KDKPBC', 'Kode Kpbc'), array('URAIAN_KPBC', 'Uraian Kpbc'));
                $field = explode(";", $indexField);
                $show_search = true;
                break;
            case'hanggar':
                $judul = "Pencarian Data Hanggar";
                $SQL = "SELECT KDHGR 'Kode Hanggar',URHGR 'Uraian Hanggar' FROM m_hanggar WHERE KDKPBC='" . substr($getdata, 0, -1) . "' ";
                $order = "1";
                $hidden = array();
                $sort = "ASC";
                $key = array("Kode Hanggar", "Uraian Hanggar");
                $search = array(array('KDHGR', 'Kode Hanggar'), array('URHGR', 'Uraian Hanggar'));
                $field = explode(";", $indexField);
                $show_search = true;
                break;
            case'skep':
                $judul = "Pencarian Data Skep";
                $SQL = "SELECT KDREC 'Kode', URAIAN 'Uraian Skep' FROM m_tabel WHERE MODUL = 'BC20' AND KDTAB='SKEP_FASILITAS'";
                $order = "1";
                $hidden = array();
                $sort = "ASC";
                $key = array("Kode", "Uraian Skep");
                $search = array(array('KDREC', 'Kode Skep'), array('URAIAN', 'Uraian Skep'));
                $field = explode(";", $indexField);
                $show_search = true;
                break;
            case'hs':
                $judul = "Data HS";
                $SQL = "SELECT NOHS ,SERITRP,TRPBM,TRPPPN,TRPPBM,TRPCUK,TRPPPH FROM M_TRADER_TARIF WHERE KODE_TRADER = '".$KODETRADER."'";
                $order = "1";
                $hidden = array();
                $sort = "ASC";
                $key = array('NOHS','SERITRP','TRPBM','TRPPPN','TRPPBM','TRPCUK','TRPPPH');
                $search = array(array('NOHS', 'NOHS'));
                $field = explode(";", $indexField);
                $show_search = true;
                break;
            case'propinsi':
                $judul = "Propinsi Asal Barang";
                $SQL = "SELECT KDDAERAH as 'Kode', URDAERAH as 'Daerah', IBUKOTA as 'Ibu Kota' FROM M_DAERAH";
                $order = "1";
                $hidden = array();
                $sort = "ASC";
                $key = array('Kode','Daerah');
                $search = array(array('KDDAERAH','Kode'), array('URDAERAH','Daerah'),array('IBUKOTA','Ibu Kota'));
                $field = explode(";", $indexField);
                $show_search = true;
                break;
            case'bank':
                $judul = "Bank DHE";
                $SQL = "SELECT KDBANK as 'Kode', NMBANK as 'Nama Bank', concat(KDBANK,'-',NMBANK) as 'txtVal' FROM M_BANK";
                $order = "1";
                $hidden = array('txtVal');
                $sort = "ASC";
                $key = array('txtVal');
                $search = array(array('KDBANK','Kode'), array('NMBANK','Nama Bank'));
                $field = explode(";", $indexField);
                $show_search = true;
                break;
            case'kade':
                $judul  = "Kode KADE";
                $SQL    = "SELECT  KD_TAMBAT AS 'Kode KADE', NM_TAMBAT AS 'Nama KADE', NM_PEMILIK AS 'Pemilik' FROM M_TAMBAT";
                $order  = "1";
                $sort   = "ASC";
                $key    = array('Kode KADE','Nama KADE');
                $search = array(array('KD_TAMBAT', 'Kode KADE'),array('NM_TAMBAT', 'Nama KADE'),array('NM_PEMILIK', 'Pemilik'));
                $field  = explode(";", $indexField);
                $show_search = true;
                break;
            case'trader_eksportir':
                $judul = "Pencarian Data Eksportir";
                $SQL = "SELECT NPWPEKS AS 'NPWP Eksportir', NAMAEKS AS 'Nama Eksportir',ALMTEKS FROM m_trader_eksportir ";
                $order = "1";
                $hidden = array("ALMTEKS");
                $sort = "ASC";
                $key = array("NPWP Eksportir", "Nama Eksportir");
                $search = array(array('NPWPEKS', 'NPWP Eksportir'), array('NAMAEKS', 'Nama Eksportir'));
                $field = explode(";", $indexField);
                $show_search = true;
                break;
            case'partiesPengirim':
                $judul = "Pengirim Barang";
                $SQL = "SELECT NAMAPARTIES AS 'Nama Pengirim', ALMTPARTIES AS 'Alamat Pengirim' "
                     . "FROM m_trader_parties "
                     . "WHERE KODEPARTIES = '0' AND KODE_TRADER = '" . $KODETRADER . "'";
                $order = "1";
                $sort = "ASC";
                $key = array("Nama Pengirim", "Alamat Pengirim");
                $search = array(array('NAMAPARTIES', 'Nama Pengirim'), array('ALMTPARTIES', 'Alamat Pengirim'));
                $field = explode(";", $indexField);
                $show_search = true;
                break;
            case'partiesPenerima':
                $judul = "Penerima Barang";
                $SQL = "SELECT NAMAPARTIES AS 'Nama Penerima', ALMTPARTIES AS 'Alamat Penerima' "
                     . "FROM m_trader_parties "
                     . "WHERE KODEPARTIES = '1' AND KODE_TRADER = '" . $KODETRADER . "'";
                $order = "1";
                $sort = "ASC";
                $key = array("Nama Penerima", "Alamat Penerima");
                $search = array(array('NAMAPARTIES', 'Nama Penerima'), array('ALMTPARTIES', 'Alamat Penerima'));
                $field = explode(";", $indexField);
                $show_search = true;
                break;
            case'partiesPemberitahu':
                $judul = "Pemberitahu Barang";
                $SQL = "SELECT NAMAPARTIES AS 'Nama Pemberitahu', ALMTPARTIES AS 'Alamat Pemberitahu' "
                     . "FROM m_trader_parties "
                     . "WHERE KODEPARTIES = '2' AND KODE_TRADER = '" . $KODETRADER . "'";
                $order = "1";
                $sort = "ASC";
                $key = array("Nama Pemberitahu", "Alamat Pemberitahu");
                $search = array(array('NAMAPARTIES', 'Nama Pemberitahu'), array('ALMTPARTIES', 'Alamat Pemberitahu'));
                $field = explode(";", $indexField);
                $show_search = true;
                break;
            case'kodehs':
                $judul = "Kode HS";
                $SQL = "SELECT HS AS 'Kode HS', DESCRIPTION AS 'Keterangan' "
                     . "FROM m_hs ";
                $order = "1";
                $sort = "ASC";
                $key = array("Kode HS", "Keterangan");
                $search = array(array('HS', 'Kode HS'), array('DESCRIPTION', 'Alamat Keterangan'));
                $field = explode(";", $indexField);
                $show_search = true;
                break;
            case'eksportir':
                $judul = "Pencarian Data Eksportir";
                $SQL = "SELECT IDEKS,NPWPEKS,NAMAEKS, ALMTEKS , NPWPEKS AS 'NPWP Eksportir', NAMAEKS AS 'Nama Eksportir', ALMTEKS AS 'Alamat', NIPER,  STATUSH, NOTDP, TGTDP FROM m_trader_eksportir";
                $order = "1";
                $hidden = array("IDEKS","NIPER","STATUSH","NOTDP","TGTDP","NPWPEKS","NAMAEKS","ALMTEKS");
                $sort = "ASC";
                $key = array("IDEKS","NPWPEKS","NAMAEKS","ALMTEKS","NIPER","STATUSH","NOTDP","TGTDP");
                $search = array(array('NPWPEKS', 'NPWP Eksportir'), array('NAMAEKS', 'Nama Eksportir'));
                $field = explode(";", $indexField);
                $show_search = true;
                break;
            case'carBC10':
                #CAR;NMANGKUT;REGKAPAL;URVOYFLAG;NOVOY;TGTIBABERANGKAT;JAMTIBABERANGKAT
                $judul = "Nomor aju yang dapat digabungkan.";
                $SQL = "SELECT  A.CAR as 'No. Aju', A.KPBC, A.IDPGUNA, A.NPWPPGUNA, A.KODEAGEN, A.NAMAPGUNA, A.ALMTGUNA, A.NMANGKUT as 'Nama Moda', A.REGKAPAL, A.VOYFLAG,
                                A.NOVOY, DATE_FORMAT(A.TGTIBABERANGKAT,'%d-%m-%Y') as 'Tgl', A.JAMTIBABERANGKAT as 'Jam', A.PELMUAT, A.PELTRANSIT, A.PELBKR, A.PELNEXT,
                                D.URAIAN_NEGARA as 'URVOYFLAG', 
                                E.URAIAN_PELABUHAN as 'URPELMUAT', 
                                F.URAIAN_PELABUHAN as 'URPELTRANSIT', 
                                G.URAIAN_PELABUHAN as 'URPELBKR', 
                                H.URAIAN_PELABUHAN as 'URPELNEXT', 
                                I.URAIAN_KPBC as 'URKPBC',
                                A.CAR
                        FROM t_bc11hdr A LEFT JOIN m_tabel B ON A.JNSMANIFEST = B.KDREC AND B.MODUL = 'BC11' 
                                         LEFT JOIN m_tabel C ON C.KDREC = A.STATUS AND C.KDTAB = 'STATUS' AND C.MODUL = 'BC11' 
                                         LEFT JOIN m_negara D ON D.KODE_NEGARA = A.VOYFLAG 
                                         LEFT JOIN m_pelabuhan E ON E.KODE_PELABUHAN = A.PELMUAT 
                                         LEFT JOIN m_pelabuhan F ON F.KODE_PELABUHAN = A.PELTRANSIT 
                                         LEFT JOIN m_pelabuhan G ON G.KODE_PELABUHAN = A.PELBKR 
                                         LEFT JOIN m_pelabuhan H ON H.KODE_PELABUHAN = A.PELNEXT 
                                         LEFT JOIN m_kpbc I ON I.KDKPBC = A.KPBC 
                                         LEFT JOIN m_tambat J ON J.KD_TAMBAT = A.KADE 
                        WHERE A.STATUS IN ('00','01') AND A.KODE_TRADER = '" . $KODETRADER . "'";
                $order = "1";
                $comp = 'No. Aju;KPBC;URKPBC;IDPGUNA;NPWPPGUNA;KODEAGEN;NAMAPGUNA;ALMTGUNA;Nama Moda;REGKAPAL;VOYFLAG;URVOYFLAG;NOVOY;Tgl;Jam;PELMUAT;URPELMUAT;PELTRANSIT;URPELTRANSIT;PELBKR;URPELBKR;PELNEXT;URPELNEXT;CAR';
                $key = explode(';', $comp);
                $sort = "ASC";
                $comp = 'KPBC;URKPBC;IDPGUNA;NPWPPGUNA;KODEAGEN;NAMAPGUNA;ALMTGUNA;URVOYFLAG;PELMUAT;URPELMUAT;PELTRANSIT;URPELTRANSIT;PELBKR;URPELBKR;PELNEXT;URPELNEXT;CAR';
                $hidden = explode(';', $comp);
                $search = array(array('A.CAR', 'CAR'), array('A.NMANGKUT', 'Nama Moda'));
                $field = explode(";", $indexField);
                $show_search = true;
                break;
                break;
            default:
                $judul = "Failed";
                break;
        }
        $ciuri = $this->input->post("uri");
        //die(site_url("search/getsearch/".$tipe."/".$indexField."/".$formName."/". $getdata));
        $this->newtable->action(site_url("search/getsearch/".$tipe."/".$indexField."/".$formName."/". $getdata));
        $this->newtable->hiddens($hidden);
        $this->newtable->keys($key);
        $this->newtable->indexField($field);
        $this->newtable->formField($formName);
        $this->newtable->orderby($order);
        $this->newtable->sortby($sort);
        $this->newtable->detail_tipe('pilih');
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->show_search($show_search);
        $this->newtable->search($search);
        $this->newtable->show_chk(false);
        $this->newtable->field_order(false);
        $this->newtable->set_formid($formName);
        $this->newtable->set_divid("div".$formName);
        $this->newtable->rowcount(8);
        $this->newtable->clear();
        $tabel = $this->newtable->generate($SQL);
        $arrdata = array("judul" => $judul, "tabel" => $tabel);
        if ($this->input->post("ajax"))
            return $tabel;
        else
            return $arrdata;
    }

}