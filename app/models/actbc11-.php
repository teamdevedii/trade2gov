<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class actBC11 extends CI_Model {

    var $kd_trader;

    function __construct() {
        parent::__construct();
        $this->kd_trader = $this->newsession->userdata('KODE_TRADER');
        return true;
    }

    //agung
    function setHeaderMANIFESTin() {
        $this->load->model("actMain");
        $data = $this->input->post();
        if ($data['act'] == 'save') {
            $data['HEADER']['CAR'] = $this->actMain->get_car('BC10');
        }
        $data['HEADER']['NPWPPGUNA'] = str_replace('.', '', str_replace('-', '', $data['HEADER']['NPWPPGUNA']));
        $exe = $this->actMain->insertRefernce('T_BC11HDR', $data['HEADER']);
        $msg = $this->validasiHDRMANIFEST($data['HEADER']['CAR']);
        // print_r($data['HEADER']);die();
        if ($exe) {
            $this->actMain->set_car('BC10');
            $rtn = "MSG|OK|" . site_url('bc11/daftarMANIFEST/' . $data['HEADER']['CAR']) . "|" . $msg;
        } else {
            $rtn = "MSG|ERR||Data header gagal diproses.";
        }
        return $rtn;
    }

    function validasiHDRMANIFEST($car) {
        $this->load->model('actMain');
        $tipe_trader = $this->newsession->userdata('TIPE_TRADER');
        $i = 0;
        //Validasi Header
        $query = "SELECT * FROM m_trader A  WHERE A.KODE_TRADER = " . $this->kd_trader;
        $strMy = $this->actMain->get_result($query);

        $query = "SELECT A.* FROM t_bc11hdr A WHERE A.CAR = '" . $car . "' AND A.KODE_TRADER = " . $this->kd_trader;
        $aH = $this->actMain->get_result($query);
        //print_r($aH);die();
        if (count($aH) > 0) {
            $judul = 'Hasil validasi Jadwal Kapal Nomor Aju : ' . substr($car, 20) . ' tanggal ' . date('d-m-Y', strtotime(substr($car, 12, 8)));
            $query = "SELECT FLDNAME,MESSAGE from m_validasi WHERE SCRNAME = 'T_BC11HDR' AND MANDATORY = 1 ";
            $arrdataV = $this->actMain->get_result($query, true);
            foreach ($arrdataV as $aV) {
                if (trim($aH[$aV['FLDNAME']]) == '') {
                    $hasil .= substr('   ' . ++$i, -3) . '. ' . $aV['MESSAGE'] . ' Harus diisi<br>';
                }
            }

            //update status [exist coy]
            if ($aH['STATUS'] == '')
                $aH['STATUS'] = '00';
            #tidak ada perubahan jika status diliar 010 dan 020
            if (strpos('|00|01|', $aH['STATUS']))
                $aH['STATUS'] = ($i == 0) ? '01' : '00';
            //apply ke database
            $upd = $aH;
            unset($upd['URSTATUS']);
            $this->db->where(array('CAR' => $car, 'KODE_TRADER' => $this->kd_trader));
            $exec = $this->db->update('t_bc11hdr', $upd);
            unset($upd);
            //tulis informasi
            if ($i == 0) {
                $hasil .= '<br>------------------ Data Lengkap ------------------<br>';
            } else {
                $hasil .= '<br>--------------- Data Tidak Lengkap ---------------<br>';
            }
        }

        $view['data'] = $hasil;
        $view['judul'] = $judul;
        $msg = $this->load->view('notify', $view, true);
        unset($aH);
        return $msg;
    }

    function lsvHeaderMANIFEST($tipe = 'MANIFEST') {
        $this->load->library('newtable');
        $this->load->model("actMain");
        $SQL = "SELECT  A.CAR AS 'CAR', 
                A.NOBC11 AS 'No BC 1.0', 
                A.TGBC11 AS 'Tanggal BC 1.0',
                A.NOBC11 AS 'No BC 1.1', 
                A.TGBC11 AS 'Tanggal BC 1.1',
                B.URAIAN AS 'Jenis Manifest',
                C.URAIAN as 'Status',
                A.KODE_TRADER
                FROM T_BC11HDR A 
                LEFT JOIN m_tabel B ON A.JNSMANIFEST = B.KDREC AND B.MODUL = 'BC11'
                LEFT JOIN m_tabel C ON C.KDREC = A.STATUS AND C.KDTAB = 'STATUS' AND C.MODUL = 'BC11'
                WHERE KODE_TRADER= '" . $this->kd_trader . "' ";
        $prosesnya = array(
            'Create' => array('ADDAJAX', site_url('bc11/createMANIFEST'), '0', '_content_'),
            'View' => array('GETAJAX', site_url('bc11/createMANIFEST'), '1', '_content_'),
            'Print Dokumen' => array('GETPOP', site_url('bc11/beforecetak'), '1', ' CETAK DOKUMEN MANIFEST |400|200'),
            'Hapus' => array('DELETEAJAX', site_url('bc11/delMANIFESThdr'), 'N', '_content_'));
        $this->newtable->menu($prosesnya);
        $this->newtable->keys(array('CAR'));
        $this->newtable->hiddens(array('KODE_TRADER'));
        $this->newtable->search(array(
            array('CAR', 'No. Aju'),
            array('a.JNSMANIFEST', 'Jenis Manifest', 'tag-select', $this->actMain->get_mtabel('JNSMANIFEST', 1, false, '', 'KODEUR', 'BC11'))));           
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->action(site_url("bc11/daftarMANIFEST"));
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->orderby(1, 2);
        $this->newtable->sortby("DESC");
        $this->newtable->tipe_check('radio');
        $this->newtable->set_formid("f" . $tipe);
        $this->newtable->set_divid("div" . $tipe);
        $this->newtable->rowcount(20);
        $this->newtable->tipe_proses('button');
        $this->newtable->clear();
        $tabel .= $this->newtable->generate($SQL);
        $arrdata = array('tabel' => $tabel, 'tipe' => $tipe);
        if ($this->input->post("ajax")) {
            return $tabel;
        } else {
            return $arrdata;
        }
    }
    /*
            $this->load->model('actMain');
            $data = $this->input->post('DETIL');                     
            if($data['KATEKS'] == '33'){
                $exec = $this->actMain->insertRefernce('t_bc30pjt',$data);
            }           
            $exec = $this->actMain->insertRefernce('t_bc30dtl',$data);
            
            $msg = $this->validasiDetil($data['CAR']);
            if ($exec) {
                //$rtn = "MSG|OK|".site_url('bc30/daftarDetil/barang/'.$data['CAR'])."|Data barang berhasil disimpan."|".$msg;
                $rtn = "MSG|OK|".site_url('bc30/daftarDetil/barang/'.$data['CAR'])."|".$msg;
            }
            else {
                $rtn = "MSG|ERR||Proses data barang gagal.";
            }
            
            $exe = $this->actMain->insertRefernce('T_BC11HDR', $data['HEADER']);
        $msg = $this->validasiHDRMANIFEST($data['HEADER']['CAR']);
        // print_r($data['HEADER']);die();
        if ($exe) {
            $this->actMain->set_car('BC11');
            $rtn = "MSG|OK|" . site_url('bc11/daftarMANIFEST/' . $data['HEADER']['CAR']) . "|" . $msg;
        } else {
            $rtn = "MSG|ERR||Data header gagal diproses.";
        }
    */
    function setDetailMANIFin($act,$car,$kdgrup,$jumpos,$nopos,$nopossub,$nopossubsub) {
        //print_r($act.'|'.$car.'|'.$kdgrup.'|'.$jumpos.'|'.$nopos);
        $this->load->model('actMain');
        $data = $this->input->post('DETIL');//print_r($data);die();
        //$car = $data['DETIL']['CAR'];
        $exe = $this->actMain->insertRefernce('t_bc11dtl', $data, true);
        $msg = $this->validasiManifDTL($data['CAR'],$data['KDGRUP'],$data['NOPOS']);//print_r($msg);die();
        //print($msg);die();
        if ($exe) {//print('bangke');die();
            $this->actMain->set_car('BC11');
            $rtn = 'MSG|OK|' . site_url('bc11/daftarGroup/' . $jns, $ftz, $car)."|".$msg;
        } else {
            $rtn = "MSG|ERR||Data detil gagal diproses.";
        }
        return $rtn;
    }

    function delMANIFESThdr($key) {
        //print_r('sini');die();
        $arr = explode('|', $key[0]);
        $car = $arr[0];
        $this->db->where_in("CAR", $key);
        $exec = $this->db->delete('t_bc11hdr'); //print_r($exec);die();
        if ($exec) {
            $rtn = 'MSG|OK|' . site_url('bc11/daftarMANIFEST/' . $car) . '|Dalete data detil berhasil.';
        } else {
            $rtn = 'MSG|ER|' . site_url('bc11/daftarMANIFEST/' . $car) . '|Dalete data detil gagal.';
        }
        return $rtn;
    }

    function getHeaderMANIFEST($car = "") {
        $this->load->model("actMain");
        if ($car) {
            $SQL = "SELECT A.*,
                DATE_FORMAT(A.TGBC10,'%Y-%m-%d') as TGBC10,
                DATE_FORMAT(A.TGBC11,'%Y-%m-%d') as TGBC11,
                B.URAIAN AS 'URJNSMANIFEST',
                C.URAIAN as 'URSTATUS'
                FROM T_BC11HDR A 
                LEFT JOIN m_tabel B ON A.JNSMANIFEST = B.KDREC AND B.MODUL = 'BC11'
                LEFT JOIN m_tabel C ON C.KDREC = A.STATUS AND C.KDTAB = 'STATUS' AND C.MODUL = 'BC11'
                WHERE CAR='" . $car . "' AND KODE_TRADER='" . $this->kd_trader . "'";

            $data ['HEADER'] = $this->actMain->get_result($SQL);
            $data['act'] = 'update'; // array('act' => 'update');
        } else {
            $sql = "SELECT 	a.KODE_ID as 'IDPGUNA', 
			a.ID as 'NPWPPGUNA', 
			a.NAMA as 'NAMAPGUNA', 
			a.ALAMAT as 'ALMTGUNA'
                    FROM m_trader a left join m_trader_bc10 b on a.KODE_TRADER = b.KODE_TRADER
                    WHERE a.KODE_TRADER = '" . $this->kd_trader . "'"; //die($sql);
            $data['HEADER'] = $this->actMain->get_result($sql);
            $data['act'] = 'save';
        }
        //print_r($data);die();
        $data ['DETIL'] = $this->daftarGroup($data['HEADER']['JNSMANIFEST'], $data['HEADER']['MANIFEST'], $car);
        $data ['IDAGEN'] = $this->actMain->get_mtabel('JENIS_IDENTITAS', 1, false, '', 'KODEDANUR', 'BC10');
        $data['MODA'] = $this->actMain->get_mtabel('MODA', 1, false, '', 'KODEDANUR', 'BC11');
        //print_r($data);die();
        return $data;
    }

    function viewdaftarGroup($car, $jns, $ftz, $kdgrup) {
        //print_r('bangke');die();
        $this->load->model("actMain");
        $data['DETIL']['CAR'] = $car;
        $data['DETIL']['KDGRUP'] = $kdgrup;

        $SQL = "SELECT *
                FROM T_BC11DTL 
                WHERE CAR='" . $car . "' AND KODE_TRADER='" . $this->kd_trader . "' 
                AND KDGRUP = '" . $kdgrup . "' ORDER BY NOPOS DESC";
        //print_r($SQL);die();
        $data['DETIL'] = $this->actMain->get_result($SQL);
        if (count($data['DETIL']) > 0) {
            $data['act'] = 'update'; // array('act' => 'update'); 

            $data['DETIL']['NOPOS'] = substr('0000' . ($data['DETIL']['NOPOS'] + 1), -4);
        } else {
            $data['DETIL']['CAR'] = $car;
            $data['DETIL']['NOPOS'] = '0001';
            $data['act'] = 'save'; // array('act' => 'update'); 
        }
        $data['DETIL']['KDGRUP'] = $kdgrup;
        //$data['KDGRUP'] = $this->actMain->get_combobox("SELECT KDREC,CONCAT(KDREC,' - ',URAIAN) as KODEDANUR,URAIAN AS KODEUR FROM M_TABEL WHERE MODUL = '".$modul."' AND KDTAB='".$jenis."'  ".$where." ORDER BY $by ", "KDREC", $uraian, $empty);
        $jns = ($jns == 'I') ? 'INWARD' : 'OUTWARD';
        switch ($ftz) {
            case'0':$addSQL = ' AND FTZ = 0';
                break;
            case'1':$addSQL = ' AND FTZ = 1';
                break;
            default :$addSQL = '';
                break;
        }
        $SQL = "SELECT KDGRUP,CONCAT(kdgrup, ' - ' ,urkdgrup) AS DETIL FROM m_manifestgrup WHERE " . $jns . " = true " . $addSQL;
        $data['KDGRUP'] = $this->actMain->get_combobox($SQL, "KDGRUP", 'DETIL', false);
        //print_r($data['KDGRUP']);die();
        return $data;
    }

    function lsvDataDrillDownManifest($type, $car, $kdgrup) {//print_r('aaa');die();
        //print_r($type.'|'.$car.'|'.$kdgrup);die();
        $this->load->library('newtable');
        $this->load->model('actMain');
        $SQL = "SELECT A.CAR,A.KDGRUP, 
                SUBSTR(CONCAT('00000',A.NOPOS),-4) AS NOPOS,
                A.NOPOSSUB,A.NOPOSSUBSUB,A.KODE_TRADER, A.NOBL, 
                DATE_FORMAT(A.TGBL,'%Y-%m-%d') as TGBL, 
                A.NMPENGIRIM, A.NMPENERIMA, 
                (SELECT COUNT(*) FROM T_BC11CON B WHERE B.CAR='" . $car . "' "
                . "AND B.KODE_TRADER='" . $this->kd_trader . "' "
                . "AND B.KDGRUP = '" . $kdgrup . "' AND B.NOPOS=A.NOPOS) AS JMLCONT
                FROM T_BC11DTL A
                WHERE A.CAR='" . $car . "'
                AND A.KODE_TRADER='" . $this->kd_trader . "'
                AND A.KDGRUP = '" . $kdgrup . "'";
        // print_r($SQL);die();
        $this->newtable->keys(array('CAR', 'KDGRUP', 'KODE_TRADER', 'NOPOS', 'NOPOSSUB', 'NOPOSSUBSUB'));
        $this->newtable->hiddens(array('CAR', 'KDGRUP', 'KODE_TRADER', 'NOPOSSUB', 'NOPOSSUBSUB'));
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->action(site_url('bc11/dataDrillDownManifest/' . $type . '/' . $car));
        //$this->newtable->search(array(array('NOPOS', 'No. Pos'), array('NOBL', 'No. BL')));
        $this->newtable->show_search(false);
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->rowcount(10);
        $this->newtable->tipe_check('radio');
        $this->newtable->clear();
        $this->newtable->set_formid("f" . $type);
        $this->newtable->set_divid("div" . $type);
        if (!isset($process)) {
            $process['Tambah '] = array('GETPOP', site_url('bc11/createPOPDetailMANIFEST/viewform/' . $car . '/' . $kdgrup), '0', 'Tambah |900|500');
            $process['View '] = array('GETPOP', site_url('bc11/createPOPDetailMANIFEST/viewform'), '1', 'View |900|500'); //print_r($kdgrup);die();
            $process['Hapus '] = array('DELETEAJAX', site_url('bc11/createPOPDetailMANIFEST/delete'), 'N', '_content_');
        }
        $this->newtable->menu($process);
        $tabel = $this->newtable->generate($SQL);
        $arrdata = array("tabel" => $tabel);
        if ($this->input->post("ajax"))
            return $tabel;
        else
            return $arrdata;
    }

    function daftarGroup($jns, $ftz, $car) {//print_r($ftz);die();
        $tipe = 'gruppos';
        $this->load->library('newtable');
        $this->load->model("actMain");
        $jns = ($jns == 'I') ? 'INWARD' : 'OUTWARD';
        switch ($ftz) {
            case'0':$addSQL = ' AND FTZ = 0';
                break;
            case'1':$addSQL = ' AND FTZ = 1';
                break;
            case'2':$addSQL = '';
                break;
            default :$addSQL = '';
                break;
        }
        $SQL = "SELECT kdgrup,CONCAT(kdgrup, ' - ' ,urkdgrup) AS detil FROM m_manifestgrup WHERE " . $jns . " = true " . $addSQL;
        $this->newtable->keys(array('kdgrup'));
        $this->newtable->hiddens(array('kdgrup'));
        $this->newtable->detail(site_url('bc11/dataDrillDownManifest/popDetilManifest/' . $car));
        //  print_r($car);die();
        $this->newtable->detail_tipe('detil_priview_bottom');
        $this->newtable->show_search(false);
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->action(site_url("bc11/daftarMANIFEST"));
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->orderby(1, 2);
        $this->newtable->sortby("DESC");
        $this->newtable->show_chk(false);
        $this->newtable->set_formid("f" . $tipe);
        $this->newtable->set_divid("div" . $tipe);
        $this->newtable->rowcount(100);
        $this->newtable->tipe_proses('button');
        $this->newtable->clear();
        $tabel .= $this->newtable->generate($SQL);
        $arrdata = array('tabel' => $tabel, 'tipe' => $tipe);
        if ($this->input->post('ajax')) {
            return $tabel;
        } else {
            return $this->load->view('daftar', $arrdata, true);
        }
    }

    //KODINGAN INI SEHARUSNYA PAKE POP UP
    function getTabelDetailPOPMANIFEST($car, $kdgrup, $jumpos = '', $nopos = '', $nopossub = '', $nopossubsub = '') {
        //print_r($car.'|'.$kdgrup.'|'.$nopos);die();       
        $this->load->model("actMain");        
        $SQL = "SELECT A.*, A.NOPOS ,B.URAIAN AS 'UDTLOK' , 
                (SELECT GROUP_CONCAT(DESCRIPTION) FROM T_BC11BRG
                WHERE CAR='" . $car . "' AND "
                . "   KODE_TRADER='" . $this->kd_trader . "' AND "
                . "   KDGRUP = '" . $kdgrup . "' AND"
                . "   NOPOS ='" . $nopos . "' AND "
                . "   NOPOSSUB ='" . $nopossub . "' AND "
                . "   NOPOSSUBSUB ='" . $nopossubsub . "') AS DESCRIPTION
                FROM T_BC11DTL A
                LEFT JOIN  m_tabel B ON B.MODUL  = 'BC11' AND B.KDTAB = 'STATUS_DETIL' AND B.KDREC = A.DTLOK
                WHERE A.CAR='" . $car . "' AND "
                . "   A.KODE_TRADER='" . $this->kd_trader . "' AND "
                . "   A.KDGRUP = '" . $kdgrup . "' AND"
                . "   A.NOPOS ='" . $nopos . "' AND "
                . "   A.NOPOSSUB ='" . $nopossub . "' AND "
                . "   A.NOPOSSUBSUB ='" . $nopossubsub . "'"; //die($SQL);
        $data ['DETIL'] = $this->actMain->get_result($SQL); //print_r($SQL);die();        
        if (count($data['DETIL']) <= 0) { //Baru
            $data['DETIL']['CAR'] = $car;
            $data['DETIL']['KDGRUP'] = $kdgrup;
            $SQL = "SELECT (NOPOS + 1) as 'NOPOS' "
                    . "FROM T_BC11DTL "
                    . "WHERE CAR='" . $car . "' AND "
                    . "   KODE_TRADER='" . $this->kd_trader . "' AND "
                    . "   KDGRUP = '" . $kdgrup . "' "
                    . "ORDER BY NOPOS DESC LIMIT 1 ";
            //die($SQL);
            // $seri = (int) $this->actMain->get_uraian("SELECT MAX(SERI) AS MAXSERI FROM t_bc11con WHERE CAR='" . $car . "'", "MAXSERI") + 1;
            $rs = $this->actMain->get_result($SQL);
            if ((int) $rs['NOPOS'] <= 0) {
                $rs['NOPOS'] = '1';
            }
            $data['DETIL']['NOPOS'] = $rs['NOPOS'];
            $data['act'] = 'save';
            $data ['TBLKONTAINER'] = $this->get_tblKontainer($car, $kdgrup, $nopos, $nopossub, $nopossubsub);
            $data ['TBLURBARANG'] = $this->get_tblUrBarang($car, $kdgrup, $nopos, $nopossub, $nopossubsub);
            $data ['TBLDOKUMEN'] = $this->get_tblUrDokumen($car, $kdgrup, $nopos, $nopossub, $nopossubsub);
        } else {
            $data['act'] = 'update';
            $data ['TBLKONTAINER'] = $this->get_tblKontainer($car, $kdgrup, $nopos, $nopossub, $nopossubsub);
            $data ['TBLURBARANG'] = $this->get_tblUrBarang($car, $kdgrup, $nopos, $nopossub, $nopossubsub);
            $data ['TBLDOKUMEN'] = $this->get_tblUrDokumen($car, $kdgrup, $nopos, $nopossub, $nopossubsub);
        }
        $data['DETIL']['KDGRUP'] = $kdgrup;
        $jns = ($jns == 'I') ? 'INWARD' : 'OUTWARD';
        switch ($ftz) {
            case'0':$addSQL = ' AND FTZ = 0';
                break;
            case'1':$addSQL = ' AND FTZ = 1';
                break;
            default :$addSQL = '';
                break;
        }

        $SQL = "SELECT KDGRUP,CONCAT(kdgrup, ' - ' ,urkdgrup) AS DETIL FROM m_manifestgrup WHERE " . $jns . " = true " . $addSQL;
        $data['KDGRUP'] = $this->actMain->get_combobox($SQL, "KDGRUP", 'DETIL', false);
        $sqlpos = "SELECT COUNT(*) NOPOS FROM T_BC11DTL WHERE CAR='" . $car . "' AND KDGRUP = '" . $kdgrup . "' AND KODE_TRADER = '" . $this->kd_trader . "'";
        $data['JMNOPOS'] = $this->actMain->get_uraian($sqlpos, 'NOPOS');

        //print_r($data);die();
        return $data;
    }

    private function get_tblKontainer($aju = '', $kdgrup = '', $nopos = '', $nopossub = '', $nopossubsub = '') {
        //$data = $this->lstKontainer($car,$kdgrup,$nopos,$nopossub,$nopossubsub);
        $arrdata = $this->lstDetil('kontainer', $aju, $kdgrup, $nopos, $nopossub, $nopossubsub);
        $data['list'] = $this->load->view('list', $arrdata, true);
        $data['type'] = 'kontainer';
        //print_r($data);
        //die();
        return $this->load->view("bc11/lst", $data, true);
    }

    private function get_tblUrBarang($aju = '', $kdgrup = '', $nopos = '', $nopossub = '', $nopossubsub = '') {
        //$data = $this->lstUrBarang($car);
        $arrdata = $this->lstDetil('barang', $aju, $kdgrup, $nopos, $nopossub, $nopossubsub);
        $data['list'] = $this->load->view('list', $arrdata, true);
        $data['type'] = 'barang';
        return $this->load->view("bc11/lst", $data, true);
    }

    private function get_tblUrDokumen($aju = '', $kdgrup = '', $nopos = '', $nopossub = '', $nopossubsub = '') {
        //$data = $this->lstDokumen($car);
        $arrdata = $this->lstDetil('dokumen', $aju, $kdgrup, $nopos, $nopossub, $nopossubsub);
        $data['list'] = $this->load->view('list', $arrdata, true);
        $data['type'] = 'dokumen';
        return $this->load->view("bc11/lst", $data, true);
    }

//$car, $seri, $kdgrup, $nopos
    function lstKontainer($car, $kdgrup = '', $nopos = '', $nopossub = '', $nopossubsub = '', $seri = '') {
        $data = array();
        $this->load->model("actMain");
        if ($car && $kdgrup && $nopos && $seri) {//print_r($car.'|'.$kdgrup.'|'.$nopos.'|'.$nopossub.'|'.$nopossubsub.'|'.$seri);die();
            $query = "SELECT * FROM t_bc11con WHERE CAR = '$car' AND SERI = '$seri' AND KDGRUP = '$kdgrup' AND NOPOS = '$nopos' AND NOPOSSUB= '$nopossub' AND NOPOSSUBSUB = '$nopossubsub' AND KODE_TRADER = '$this->kd_trader'";

            $hasil = $this->actMain->get_result($query);
            $data['KONTAINER'] = array_change_key_case($hasil, CASE_UPPER);
            $data['act'] = 'update';
        } else {
            $SQL = "SELECT ifnull(max(SERI)+1,1) as 'SERI' FROM T_BC11CON WHERE CAR = '$car' AND KDGRUP = '$kdgrup' AND NOPOS = '$nopos' AND NOPOSSUB= '$nopossub' AND NOPOSSUBSUB = '$nopossubsub' AND KODE_TRADER = '$this->kd_trader'";
            //print_r($SQL);die();
            $data['KONTAINER']['SERI'] = $this->actMain->get_uraian($SQL, 'SERI');
            $data['act'] = 'save';
        }
        $data['UKURAN_CONTAINER'] = $this->actMain->get_mtabel('UKURAN_CONTAINER');
        $data['JENIS_CONTAINER'] = $this->actMain->get_mtabel('JENIS_CONTAINER');

        $data['KONTAINER']['CAR'] = $car;
        $data['KONTAINER']['KDGRUP'] = $kdgrup;
        $data['KONTAINER']['NOPOS'] = $nopos;
        $data['KONTAINER']['NOPOSSUB'] = $nopossub;
        $data['KONTAINER']['NOPOSSUBSUB'] = $nopossubsub; //print_r($nopossubsub);die();
        return $data;
    }

    function lstUrBarang($car, $kdgrup = '', $nopos = '', $nopossub = '', $nopossubsub = '', $seri = '') {
        
        $data = array();
        $this->load->model("actMain");
        if ($car && $kdgrup && $nopos && $seri) {//print_r('jancuk');die();
            $query = "SELECT * FROM t_bc11brg WHERE CAR = '$car' AND SERI = '$seri' AND KDGRUP = '$kdgrup' AND NOPOS = '$nopos' AND NOPOSSUB= '$nopossub' AND NOPOSSUBSUB = '$nopossubsub' AND KODE_TRADER = '$this->kd_trader'";
            //print_r($query);die();
            $hasil = $this->actMain->get_result($query);
            $data['URBARANG'] = array_change_key_case($hasil, CASE_UPPER);
            $data['act'] = 'update';
        } else {
            $SQL = "SELECT ifnull(max(SERI)+1,1) as 'SERI' FROM T_BC11BRG WHERE CAR = '$car' AND KDGRUP = '$kdgrup' AND NOPOS = '$nopos' AND NOPOSSUB= '$nopossub' AND NOPOSSUBSUB = '$nopossubsub' AND KODE_TRADER = '$this->kd_trader'";
            $data['URBARANG']['SERI'] = $this->actMain->get_uraian($SQL, 'SERI');
            $data['act'] = 'save';
        }
        $data['URBARANG']['CAR'] = $car;
        $data['URBARANG']['KDGRUP'] = $kdgrup;
        $data['URBARANG']['NOPOS'] = $nopos;
        $data['URBARANG']['NOPOSSUB'] = $nopossub;
        $data['URBARANG']['NOPOSSUBSUB'] = $nopossubsub;
        return $data;
    }

    function lstDokumen($car, $kdgrup = '', $nopos = '', $nopossub = '', $nopossubsub = '', $seri = '') {
        $data = array();
        $this->load->model("actMain");
        if ($car && $kdgrup && $nopos && $seri) {
            $query = "SELECT A.*, A.DOKKODE, B.URAIAN FROM t_bc11dok A "
                    . "LEFT JOIN M_TABEL B ON B.KDREC=A.DOKKODE AND B.MODUL= 'BC11' AND B.KDTAB= 'KDDOK'"
                    . "WHERE A.CAR = '$car' AND A.SERI = '$seri' AND A.KDGRUP = '$kdgrup' AND A.NOPOS = '$nopos' AND A.NOPOSSUB= '$nopossub' AND A.NOPOSSUBSUB = '$nopossubsub' AND A.KODE_TRADER = '$this->kd_trader'";

            $hasil = $this->actMain->get_result($query);
            $data['DOKUMEN'] = array_change_key_case($hasil, CASE_UPPER);
            $data['act'] = 'update';
        } else {
            $SQL = "SELECT ifnull(max(SERI)+1,1) as 'SERI' FROM T_BC11DOK WHERE CAR = '$car' AND KDGRUP = '$kdgrup' AND NOPOS = '$nopos' AND NOPOSSUB= '$nopossub' AND NOPOSSUBSUB = '$nopossubsub' AND KODE_TRADER = '$this->kd_trader'";
            $data['DOKUMEN']['SERI'] = $this->actMain->get_uraian($SQL, 'SERI');
            $data['act'] = 'save';
        }
        $data['DOKUMEN']['CAR'] = $car;
        $data['DOKUMEN']['KDGRUP'] = $kdgrup;
        $data['DOKUMEN']['NOPOS'] = $nopos;
        $data['DOKUMEN']['NOPOSSUB'] = $nopossub;
        $data['DOKUMEN']['NOPOSSUBSUB'] = $nopossubsub;
        $data['DOKKODE'] = $this->actMain->get_combobox("SELECT KDREC,
                                                     CONCAT(KDREC,' - ',URAIAN) as KODEDANUR
                                             FROM M_TABEL WHERE MODUL = 'BC11' AND KDTAB = 'KDDOK' ORDER BY KDTAB,URUT ", 'KDREC', 'KODEDANUR', true);
        return $data;
    }

    function lstDetil($type, $aju, $kdgrup, $nopos, $nopossub, $nopossubsub) {
        $this->load->library('newtable');
        $this->newtable->keys = '';
        if ($type == "barang") {
            $this->load->model('actMain');
            $SQL = "SELECT HS as 'HS Kode', DESCRIPTION as 'Deskripsi', CAR,
                    SERI, KDGRUP, NOPOS, NOPOSSUB, NOPOSSUBSUB
                     FROM t_bc11brg 
                     WHERE CAR= '$aju' AND KDGRUP = '$kdgrup' AND NOPOS = '$nopos' AND NOPOSSUB = '$nopossub' AND NOPOSSUBSUB = '$nopossubsub' AND KODE_TRADER = '" . $this->kd_trader . "'"; //disesuaiakn $aju, $kdgrup, $nopos='', $nopossub='', $nopossubsub
            $this->newtable->keys(array("CAR", "SERI", "KDGRUP", "NOPOS", "NOPOSSUB", "NOPOSSUBSUB"));
            $this->newtable->hiddens(array("CAR", "SERI", "KDGRUP", "NOPOS", "NOPOSSUB", "NOPOSSUBSUB"));
            //$this->newtable->search(array(
            //    array('BRGURAI', 'Uraian Barang'),
            //    array('NOHS', 'Kode HS'),
             //   array('DTLOK', 'Status', 'tag-select', $this->actMain->get_mtabel('STATUS_DETIL'))));
            $this->newtable->orderby("CAR");
            $this->newtable->sortby("DESC");
        } else if ($type == "kontainer") {
            $SQL = "SELECT A.NOCONT AS 'Nomor', 
                            C.URAIAN AS 'Ukuran', 
                            B.URAIAN AS 'Tipe',
                            A.CAR, A.SERI, A.KDGRUP, A.NOPOS, A.NOPOSSUB, A.NOPOSSUBSUB
                    FROM t_bc11con A
                    INNER JOIN m_tabel B ON A.TIPECONT = B.KDREC AND B.MODUL = 'BC11' AND B.KDTAB = 'JENIS_CONTAINER' 
                    INNER JOIN m_tabel C ON A.UKCONT = C.KDREC AND C.MODUL = 'BC11' AND C.KDTAB = 'UKURAN_CONTAINER' 
                    WHERE A.CAR= '$aju' AND A.KDGRUP = '$kdgrup' AND A.NOPOS = '$nopos' AND A.NOPOSSUB = '$nopossub' AND A.NOPOSSUBSUB = '$nopossubsub' AND A.KODE_TRADER = '" . $this->kd_trader . "'"; //die($SQL);
            $this->newtable->keys(array("CAR", "SERI", "KDGRUP", "NOPOS", "NOPOSSUB", "NOPOSSUBSUB")); //die($SQL);
            $this->newtable->hiddens(array("CAR", "SERI", "KDGRUP", "NOPOS", "NOPOSSUB", "NOPOSSUBSUB"));
            $this->newtable->show_search(false);
            //$this->newtable->search(array(array('A.CONTNO', 'Nomor'), array('A.UKURAN_CONTAINER', 'Ukuran'), array('A.JENIS_CONTAINER', 'Tupe')));
            $this->newtable->orderby(4);
            $this->newtable->sortby("DESC");
        } else if ($type == "dokumen") {
            $SQL = "SELECT B.URAIAN AS 'Jenis Dokumen', A.DOKNO AS 'No Dokumen', 
                    DATE_FORMAT(A.DOKTG,'%Y-%m-%d') AS 'Tgl Dokumen',
                    A.DOKKPBC AS 'Kode Kantor' ,  
                             A.CAR, A.SERI, A.KDGRUP, A.NOPOS, A.NOPOSSUB, A.NOPOSSUBSUB
                    FROM t_bc11dok A
                    LEFT JOIN M_TABEL B ON B.KDREC=A.DOKKODE AND B.MODUL= 'BC11' AND B.KDTAB= 'KDDOK'
                    WHERE A.CAR= '$aju' AND A.KDGRUP = '$kdgrup' AND A.NOPOS = '$nopos' AND A.NOPOSSUB = '$nopossub' AND A.NOPOSSUBSUB = '$nopossubsub' AND A.KODE_TRADER = '" . $this->kd_trader . "'";
            $this->newtable->keys(array("CAR", "SERI", "KDGRUP", "NOPOS", "NOPOSSUB", "NOPOSSUBSUB"));
            $this->newtable->hiddens(array("CAR", "SERI", "KDGRUP", "NOPOS", "NOPOSSUB", "NOPOSSUBSUB"));
            $this->newtable->search(array(
                array('DOKKD', 'Kode'),
                array("f_ref('KODE_DOKUMEN',DOKKD)", 'Jenis'),
                array('DOKNO', 'Nomor'),
                array("DATE_FORMAT(DOKTG,'%Y-%m-%d')", 'Tanggal', 'tag-tanggal')));
            $this->newtable->orderby("CAR");
            $this->newtable->sortby("DESC");
        } else {
            return "Failed";
            exit();
        }
       $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->action(site_url('bc11/daftarDetil/' . $type . '/' . $aju . '/' . $kdgrup . '/' . $nopos . '/' . $nopossub . '/' . $nopossubsub));
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->rowcount(5);
        $this->newtable->clear();
        $this->newtable->set_formid("f" . $type);
        $this->newtable->set_divid("div" . $type);
        $process = array(
            'Tambah ' . $type => array('ADDAJAX', site_url('bc11/' . $type . '/' . $aju . '/' . $kdgrup . '/' . $nopos . '/' . $nopossub . '/' . $nopossubsub), '0', 'f' . $type . '_form'),
            'Ubah ' . $type => array('EDITAJAX', site_url('bc11/' . $type . '/' . $aju . '/' . $kdgrup . '/' . $nopos . '/' . $nopossub . '/' . $nopossubsub), '1', 'f' . $type . '_form'),
            'Hapus ' . $type => array('DELETEAJAX', site_url('bc11/' . $type . '/' . $aju . '/' . $kdgrup . '/' . $nopos . '/' . $nopossub . '/' . $nopossubsub), 'N', 'f' . $type . '_list'));
        $this->newtable->menu($process);
        $tabel = $this->newtable->generate($SQL);
        $arrdata = array("tabel" => $tabel);
        if ($this->input->post("ajax"))
            return $tabel;
        else
            return $arrdata;
    }

    function delManifdtl($key) {
        //print_r($key);die();
        //die();
        $arr = explode('|',$key[0]);
        //$arr            = explode('|', $key);
        $car            = $arr[0];
        $kdgrup         = $arr[1]; 
        $kdtrader       = $arr[2]; 
        $nopos          = intval($arr[3]);//print_r($nopos);die();
        $nopossub       = intval($arr[4]);
        $nopossubsub    = intval($arr[5]);

        $tables = array('t_bc11dtl', 't_bc11dok', 't_bc11con', 't_bc11brg');
        //$this->db->where_in("CONCAT(CAR,'|',KDGRUP,'|',KODE_TRADER,'|',NOPOS,'|',NOPOSSUB,'|',NOPOSSUBSUB)", $key);
        $this->db->where(array('CAR' => $car, 'KDGRUP' => $kdgrup, 'KODE_TRADER' => $kdtrader, 'NOPOS' => $nopos, 'NOPOSSUB' => $nopossub, 'NOPOSSUBSUB' => $nopossubsub, 'KODE_TRADER' => $this->kd_trader));
        $this->db->delete($tables);//echo $this->db->last_query();
        //$this->db->delete('t_bc11dtl'); //echo $this->db->last_query();
        foreach ($key as $v) {
            $arr = explode('|', $v);
            $car = $arr[0];            
            $nopos = $arr[3];
            $sql = "call bc11UrutkanSerial(" . $this->kd_trader . ",'" . $car . "',$nopos)";
            $exec = $this->db->query($sql);
        }
        if ($exec) {
            $rtn = 'MSG|OK|' . site_url('bc11/refreshHeader/' . $car,$kdgrup,$nopos,$nopossub,$nopossubsub) . '|Dalete data detil berhasil.';
        } else {
            $rtn = 'MSG|ER|' . site_url('bc11/refreshHeader/' . $car,$kdgrup,$nopos,$nopossub,$nopossubsub) . '|Dalete data detil gagal.';
        }
        return $rtn;
    }

    function setKontainer($type, $car, $kdgrup, $nopos, $nopossub, $nopossubsub) {
        if ($type == 'save' || $type == 'update') {
            $this->load->model("actMain");
            $data = $this->input->post('KONTAINER');
            if (!$data['CAR']) {
                return "MSG|ERR||Simpan data kontainer Gagal.<br>Data header belum tersimpan.";
            }
            $data['KODE_TRADER'] = $this->kd_trader;            
            $exec = $this->actMain->insertRefernce('t_bc11con', $data);
            if ($exec) {//print_r($car.'|'.$nopos.'|'.$kdgrup.'|'.$nopossub.'|'.$nopossubsub);die();
                $sql = "call P_BC11UPDCON('" . $data['CAR'] . "'," . $this->kd_trader . ")";
                $this->db->query($sql);
                return "MSG|OK|" . site_url('bc11/daftarDetil/kontainer/' . $data['CAR'],$data['KDGRUP'],$data['NOPOS'],$data['NOPOSSUB'],$data['NOPOSSUBSUB']) . "|Data kontainer berhasil disimpan.";               
             
            } else {
                return "MSG|ERR||Proses data kontainer gagal.";
            }
        } else if ($type == 'delete') {
            $this->input->post('tb_chkfkontainer');
            foreach ($this->input->post('tb_chkfkontainer') as $chkitem) {
                $arrchk = explode("|", $chkitem);
            }
            $car = $arrchk[0];
            $seri = $arrchk[1];
            $kdgrup = $arrchk[2];
            $nopos = $arrchk[3];
            $nopossub = $arrchk[4];
            $nopossubsub = $arrchk[5];
            $sql = "DELETE FROM T_BC11CON WHERE CAR='" . $car . "' AND NOPOS = '" . $nopos . "' AND NOPOSSUB = '" . $nopossub . "' AND NOPOSSUBSUB = '" . $nopossubsub . "' AND KDGRUP = '" . $kdgrup . "' AND SERI = '" . $seri . "' AND KODE_TRADER = '" . $this->kd_trader . "' ";
            //print_r($sql);//die()
            $exec = $this->db->query($sql);
            if ($exec) {//print_r($car.'|'.$nopos.'|'.$kdgrup.'|'.$nopossub.'|'.$nopossubsub);die();
                $sql = "call P_BC11UPDCON('" . $car . "'," . $this->kd_trader . ')';
                $this->db->query($sql);
                return "MSG|OK|" . site_url('bc11/daftarDetil/kontainer/' . $car,$kdgrup,$nopos,$nopossub,$nopossubsub) . "|Data kontainer berhasil didelete.";
            } else {
                return "MSG|ERR||Data kontainer gagal didelete.";
            }
        }
    }

    function setDokumen($type, $car, $kdgrup, $nopos, $nopossub, $nopossubsub) {
        if ($type == 'save' || $type == 'update') {
            $this->load->model('actMain');
            $data = $this->input->post('DOKUMEN');
            if (!$data['CAR']) {
                return "MSG|ERR||Simpan data dokumen Gagal.<br>Data header belum tersimpan.";
            }
            $data['KODE_TRADER'] = $this->kd_trader;
            $exec = $this->actMain->insertRefernce('t_bc11dok', $data);
            if ($exec) {
                return "MSG|OK|" . site_url('bc11/daftarDetil/dokumen/' . $data['CAR']) . "|Data dokumen berhasil disimpan.";
            } else {
                return "MSG|ERR||Proses data dokumen gagal.";
            }
        } else if ($type == 'delete') {
            foreach ($this->input->post('tb_chkfdokumen') as $chkitem) {
                $arrchk = explode("|", $chkitem);
            }
            $car = $arrchk[0];
            $seri = $arrchk[1];
            $kdgrup = $arrchk[2];
            $nopos = $arrchk[3];
            $nopossub = $arrchk[4];
            $nopossubsub = $arrchk[5];
            $sql = "DELETE FROM T_BC11DOK WHERE CAR='" . $car . "' AND NOPOS = '" . $nopos . "' AND NOPOSSUB = '" . $nopossub . "' AND NOPOSSUBSUB = '" . $nopossubsub . "' AND KDGRUP = '" . $kdgrup . "' AND SERI = '" . $seri . "' AND KODE_TRADER = '" . $this->kd_trader . "' ";
            //print_r($sql);//die()
            $exec = $this->db->query($sql);
            if ($exec) {
                return 'MSG|OK|' . site_url('bc11/daftarDetil/dokumen/' . $car) . '|Data dokumen berhasil didelete.';
            } else {
                return 'MSG|ERR||Data dokumen gagal didelete.';
            }
        }
    }

    function setBarang($type, $car, $kdgrup, $nopos, $nopossub, $nopossubsub) {
        if ($type == 'save' || $type == 'update') {
            $this->load->model("actMain");
            $data = $this->input->post('URBARANG');
            if (!$data['CAR']) {
                return "MSG|ERR||Simpan data barang Gagal.<br>Data header belum tersimpan.";
            }
            $data['KODE_TRADER'] = $this->kd_trader;
            $exec = $this->actMain->insertRefernce('t_bc11brg', $data);
            if ($exec) {
                return "MSG|OK|" . site_url('bc11/daftarDetil/barang/' . $data['CAR']) . "|Data barang berhasil disimpan.";
            } else {
                return "MSG|ERR||Proses data barang gagal.";
            }
        } else if ($type == 'delete') {
            $this->input->post('tb_chkfbarang');
            foreach ($this->input->post('tb_chkfbarang') as $chkitem) {
                $arrchk = explode("|", $chkitem);
            }
            $car = $arrchk[0];
            $seri = $arrchk[1];
            $kdgrup = $arrchk[2];
            $nopos = $arrchk[3];
            $nopossub = $arrchk[4];
            $nopossubsub = $arrchk[5];
            ;
            $sql = "DELETE FROM T_BC11BRG WHERE CAR='" . $car . "' AND NOPOS = '" . $nopos . "' AND NOPOSSUB = '" . $nopossub . "' AND NOPOSSUBSUB = '" . $nopossubsub . "' AND KDGRUP = '" . $kdgrup . "' AND SERI = '" . $seri . "' AND KODE_TRADER = '" . $this->kd_trader . "' ";

            $exec = $this->db->query($sql);
            if ($exec) {
                return "MSG|OK|" . site_url('bc11/daftarDetil/barang/' . $car) . "|Data barang berhasil didelete.";
            } else {
                return "MSG|ERR||Data barang gagal didelete.";
            }
        }
    }
    
    function validasiManifDTL($car, $kdgrup='', $nopos='', $nopossub='', $nopossubsub='') {
        //die('validasi gabungan');
        //print_r($car.'|'.$kdgrup.'|'.$nopos.'|'.$jmnopos);die();
        $this->load->model('actMain');
        $tipe_trader = $this->newsession->userdata('TIPE_TRADER');
        $i = 0;
        //Validasi Header
        $query = "SELECT * FROM m_trader A  WHERE A.KODE_TRADER = " . $this->kd_trader;
        $strMy = $this->actMain->get_result($query);
        
        $query = "SELECT A.* FROM t_bc11dtl A  WHERE A.CAR = '" . $car . "' AND A.KDGRUP = '" . $kdgrup . "' AND A.NOPOS = '" . $nopos . "' AND A.NOPOSSUB = '" . $nopossub . "' AND A.NOPOSSUBSUB = '" . $nopossubsub . "' AND A.KODE_TRADER = " . $this->kd_trader;
        $aManifDtl = $this->actMain->get_result($query);
        
        //print_r($aManifDtl);die();
        if (count($aManifDtl) > 0) {
            $judul = 'Hasil validasi Detil Manifest Nomor Aju : ' . substr($car, 20) . ' tanggal ' . date('d-m-Y',strtotime(substr($car, 12, 8)));
            $query = "SELECT FLDNAME,MESSAGE from m_validasi WHERE SCRNAME = 'T_BC11DTL' AND MANDATORY = 1";
            $arrdataV = $this->actMain->get_result($query, true);
            foreach ($arrdataV as $aV) {
                if (trim($aManifDtl[$aV['FLDNAME']]) == '') {
                    $hasil .= substr('   ' . ++$i, -3) . '. ' . $aV['MESSAGE'] . ' Harus diisi<br>';
                }
            }
            //######====validasi kusus=====#######
            /*if($jmbrggab != $aManifDtl['SERIEKS']){
                $hasil .= substr('   ' . ++$i, -3) .'. Pengisian Detil Gabungan belum lengkap.<br>';
            }*/
            //update DTLOK [exist coy]
            if ($aH['DTLOK'] == '')
                $aH['DTLOK'] = '0';
            #tidak ada perubahan jika DTLOK diluar 010 dan 020
            if (strpos('|0|1|', $aH['DTLOK']))
                $aH['DTLOK'] = ($i == 0) ? '1' : '0';
            //print_r($i);die();
            //apply ke database
            $upd = $aH;
            unset($upd['UDTLOK']);
            $this->db->where(array('CAR' => $car, 'KDGRUP'=> $kdgrup, 'NOPOS'=> $nopos, 'NOPOSSUB'=> $nopossub, 'NOPOSSUBSUB'=> $nopossubsub, 'KODE_TRADER' => $this->kd_trader));
            $exec = $this->db->update('t_bc11dtl', $upd);
            //echo $this->db->last_query();
            unset($upd);
            //tulis informasi
            if ($i == 0) {
                $hasil .= '<br>------------------ Data Lengkap ------------------<br>';
            } else {
                $hasil .= '<br>--------------- Data Tidak Lengkap ---------------<br>';
            }
        }
        $view['data']   = $hasil;
        $view['judul']  = $judul;
        $msg = $this->load->view('notify', $view, true);
        unset($aH);
        //return $msg;
        return $msg;
    }
    
    function genFlatfile($car) {
        function fixLen($str, $len, $chr = ' ', $alg = STR_PAD_RIGHT) {
            $hasil = str_pad(substr($str, 0, $len), $len, $chr, $alg);
            return $hasil;
        }
        $this->load->model('actMain');
        $EDINUMBER = $this->newsession->userdata('EDINUMBER');
        $SRP = $this->newsession->userdata('SRP');
        $SQL = "SELECT a.*, c.NOMOREDI,"
                . "       DATE_FORMAT(a.TGBC10,'%Y%m%d') as TGBC10, "
                . "       DATE_FORMAT(a.TGBC11,'%Y%m%d') as TGBC11, "
                . "       DATE_FORMAT(a.TGVOY,'%Y%m%d%H%i') as TGVOY "                             
                . "FROM T_BC11HDR a "
                . "LEFT JOIN m_trader_bc10 b ON a.KODE_TRADER = b.KODE_TRADER AND b.KPBC = a.KPBC "
                . "LEFT JOIN M_TRADER_PARTNER c ON a.KODE_TRADER = c.KODE_TRADER and c.KDKPBC = a.KPBC "
                . "WHERE a.CAR = '" . $car . "' AND a.KODE_TRADER = " . $this->kd_trader;//print_r($SQL);die();
        //return $SQL;
        $hdr = $this->actMain->get_result($SQL);//print_r($hdr['KDKPBC']);die();
        $hdr = str_replace("\n", '', str_replace("\r", '', $hdr));
        if ($hdr['STATUS'] != '01') {
            if ($hdr['STATUS'] == '02') {
                $whHDR['KODE_TRADER'] = $this->kd_trader;
                $whHDR['CAR'] = $car;
                $this->db->where($whHDR);
                $upHDR['SNRF'] = '';
                $upHDR['DIRFLT'] = '';
                $upHDR['DIREDI'] = '';
                $upHDR['STATUS'] = '010';
                $this->db->update('t_bc11hdr', $upHDR);
                unset($upHDR);
                #delete dokoutbound
                $whHDR['KDKPBC'] = $hdr['KDKPBC'];
                $whHDR['JNSDOK'] = 'BC11';
                #$this->db->delete('m_trader_dokoutbound', $whHDR);
                unset($whHDR);
                unlink($hdr['DIRFLT']);
                unlink($hdr['DIREDI']);
                $hsl = '1|';
            } else {
                $hsl = '0|';
            }
            unset($hdr);
            return $hsl;
        }
        if ($hdr['NOMOREDI'] == '') {
            return 'Edinumber Kantor Pelayanan belum terekam.';
        }
        /*
        $KDKPBC = $hdr['KDKTR'];
        $SQL = "SELECT NOMOREDI FROM M_TRADER_PARTNER WHERE KDKPBC = '" . $hdr['KDKTR'] . "' AND KODE_TRADER = " . $this->kd_trader;
        $partner = $this->actMain->get_uraian($SQL, 'NOMOREDI');
        if ($partner == '') {
            return 'Edinumber Kantor Pelayanan belum terekam.';
        }
        */
        //$DOKUMEN = ($hdr['JNSMANIFEST']=='R')?'DOKRKSP':'DOKJKSP';
        //$KDDOK = ($hdr['JNSMANIFEST']=='R')?'781':'957';
        $spasi = str_repeat(" ",30);
        $spasijmlbl = str_repeat(" ",5);
        
        #SEPARATOR UNTUK HEADER
        $FF.= "ENV00101" . fixLen($partner, 20) . "DOKMANIF  " . "2\n";

        #RECORD TYPE HCAR0101
        $FF.= "HCAR0101" . fixLen($hdr['CAR'], 26) . " 9" . fixLen($hdr['JNSMANIFEST'], 8) . "242" . fixLen($hdr['TANGGAL DOKUMEN'], 12) . "203\n";

        #RECORD TYPE HCAR0201
        $FF.= "HCAR020110" . fixLen($hdr['KPBC'], 6) . fixLen($hdr['NOBC10'], 6) . "182" . fixLen($hdr['TGBC10'], 8) . "102\n";
        $FF.= "HCAR020111" . fixLen($hdr['KPBC'], 6) . fixLen($hdr['NOBC11'], 6) . "182" . fixLen($hdr['TGBC11'], 8) . "102\n";

        #RECORD TYPE HCAR0301
        $FF.= "HCAR0301CG " . fixLen($hdr['IDPGUNA'], 1) . fixLen($hdr['NPWPPGUNA'], 15) . fixLen($hdr['NAMAPGUNA'], 40) . fixLen($hdr['ALMTGUNA'], 50) . fixLen($hdr['PEMBERITAHU'], 30) . fixlen($spasi) . "\n";
        $FF.= "HCAR0301CG " . fixLen($hdr['IDPGUNA'], 1) . fixLen($hdr['NPWPPGUNA'], 15) . fixLen($hdr['NAMAPGUNA'], 40) . fixLen($hdr['ALMTGUNA'], 50) . fixLen($hdr['PEMBERITAHU'], 30) . fixlen($spasi) . "\n";
        
        #HCAR3A01
        $len = strlen($hdr['MEMO']);
        $start = 0;
        while($len>0){
            $FF.= "HCAR3A01ADU" . fixLen(substr($hdr['MEMO'],$start,$len), 280) . "\n";
            $start += 280;
            $len -= 280;
        }
        
        #RECORD TYPE HCAR0401
        $FF.= "HCAR040111 " . fixLen($hdr['MODA'], 2)
                            . fixLen($hdr['REGKAPAL'], 9) 
                            . fixLen($hdr['NMANGKUT'], 35) 
                            . fixLen($hdr['NOVOY'], 10) 
                            . fixLen($hdr['VOYFLAG'], 2)  
                            . fixLen($hdr['KODEAGEN'], 3) . "\n";       

        #RECORD TYPE HCAR0501
        $FF.= "HCAR050141 " . fixLen($hdr['KPBC'], 6) . "\n";
        $FF.= "HCAR05019  " . fixLen($hdr['PELMUAT'], 6) . "\n";
        $FF.= "HCAR0501127" . fixLen($hdr['PELAKHIR'], 6) . "\n";
        $FF.= "HCAR050111 " . fixLen($hdr['PELBKR'], 6) . fixLen($hdr['KADE'], 6) . "\n";
        $FF.= "HCAR050161 " . fixLen($hdr['PELNEXT'], 6) . "\n";
        $FF.= "HCAR050176 " . fixLen($hdr['PELMUAT'], 6) . "\n";
        
        #RECORD TYPE HCAR0601 
        $FF.= "HCAR0601132" . fixLen($hdr['TGVOY'], 12) . "203\n";
        $FF.= "HCAR0601133" . fixLen($hdr['TGVOY'], 12) . "203\n";
        
        #RECORD TYPE HCAR0701
        $FF.= "HCAR070110 000000000" . fixLen($hdr['HJMLBL'], 5) . "0000\n";
        $FF.= "HCAR070116 000000000" . fixLen($hdr['HJMLCONT'], 5) . "0000\n";
        $FF.= "HCAR070111 000000" . fixLen($hdr['HJMLKEMAS'], 8) . "0000\n";
        $FF.= "HCAR07017  " . fixLen($hdr['HBRUTO'], 17) . "\n";
        $FF.= "HCAR070190 " . fixLen($hdr['HVOLUME'], 17) . "\n";
        
        #DATA DETIL
        $SQL = "SELECT *, SUBSTR(CONCAT('00000',NOPOS),-4) AS NOPOS, SUBSTR(CONCAT('00000',NOPOSSUB),-4) AS NOPOSSUB,"
                . " DATE_FORMAT(TGBL,'%Y%m%d') as TGBL "
                . " FROM T_BC11DTL "
                . " WHERE CAR = '" . $car . "' AND KODE_TRADER = " . $this->kd_trader;                
        $dtl = $this->actMain->get_result($SQL, true);
        $dtl = str_replace("\n", '', str_replace("\r", '', $dtl));
        
        #DATA BL
        $SQL = "SELECT COUNT(NOPOS) AS JMLBL FROM T_BC11DTL "
                . "WHERE CAR = '" . $car . "' AND KODE_TRADER = " . $this->kd_trader;                
        $bl = $this->actMain->get_result($SQL, true);
        //print_r($bl);die();
       
        #DATA CONT
        $SQL = "SELECT *, KDGRUP, NOPOS, NOPOSSUB, NOPOSSUBSUB, SERI, NOCONT, UKCONT, TIPECONT"
                . " FROM T_BC11CON "
                . " WHERE CAR = '" . $car . "' AND KODE_TRADER = " . $this->kd_trader ;                
        $cont = $this->actMain->get_result($SQL, true);
        //print_r($cont);die();
        foreach ($cont as $c){
            $arrCont[$c['KDGRUP']][$c['NOPOS']][$c['NOPOSSUB']][$c['NOPOSSUBSUB']][$c['SERI']]['NOCONT'] = $c['NOCONT'];
            $arrCont[$c['KDGRUP']][$c['NOPOS']][$c['NOPOSSUB']][$c['NOPOSSUBSUB']][$c['SERI']]['UKCONT'] = $c['UKCONT'];
            $arrCont[$c['KDGRUP']][$c['NOPOS']][$c['NOPOSSUB']][$c['NOPOSSUBSUB']][$c['SERI']]['TIPECONT'] = $c['TIPECONT'];
           //print_r($a);
        }//print_r($arrCont);die();
        
        #DATA BRG
        $SQL = "SELECT *, KDGRUP, NOPOS, NOPOSSUB, NOPOSSUBSUB, SERI, HS, DESCRIPTION FROM T_BC11BRG "
                . " WHERE CAR = '" . $car . "' AND KODE_TRADER = " . $this->kd_trader;                
        $brg = $this->actMain->get_result($SQL, true);
        foreach ($brg as $a){
            $arrbrg[$a['KDGRUP']][$a['NOPOS']][$a['NOPOSSUB']][$a['NOPOSSUBSUB']][$a['SERI']]['HS']  = $a['HS'];
            $arrbrg[$a['KDGRUP']][$a['NOPOS']][$a['NOPOSSUB']][$a['NOPOSSUBSUB']][$a['SERI']]['DESCRIPTION']  = $a['DESCRIPTION'];                 
        }
        
        #DATA DOK
        $SQL = "SELECT *, KDGRUP, NOPOS, NOPOSSUB, NOPOSSUBSUB, SERI, DOKKODE, DOKNO, "
                . " DATE_FORMAT(DOKTG,'%Y%m%d') as DOKTG, DOKKPBC FROM T_BC11DOK "               
                . " WHERE CAR = '" . $car . "' AND KODE_TRADER = " . $this->kd_trader;                
        $dok = $this->actMain->get_result($SQL, true);
        foreach ($dok as $a){
            $arrdok[$a['KDGRUP']][$a['NOPOS']][$a['NOPOSSUB']][$a['NOPOSSUBSUB']][$a['SERI']]['DOKKODE']     = $a['DOKKODE'];
            $arrdok[$a['KDGRUP']][$a['NOPOS']][$a['NOPOSSUB']][$a['NOPOSSUBSUB']][$a['SERI']]['DOKNO']     = $a['DOKNO'];
            $arrdok[$a['KDGRUP']][$a['NOPOS']][$a['NOPOSSUB']][$a['NOPOSSUBSUB']][$a['SERI']]['DOKTG']     = $a['DOKTG'];
            $arrdok[$a['KDGRUP']][$a['NOPOS']][$a['NOPOSSUB']][$a['NOPOSSUBSUB']][$a['SERI']]['DOKKPBC']     = $a['DOKKPBC'];            
        }
        
        //SAMPAI DISINI
        
        foreach ($dtl as $a) {
            
            
            #RECORD TYPE DCAR0101
            
            
            
                    
            $FF.= "DCAR010101" . "\n";

            #RECORD TYPE DCAR0201
            foreach($bl as $pp){
            $FF.= "DCAR020110 000000000" . fixLen($pp['JMLBL'], 5) . "0000\n";
            }
            unset($pp);
            $FF.= "DCAR020116 000000000" . fixLen($a['JMLCONT'], 5) . "0000\n";
            $FF.= "DCAR020111 000000" . fixLen($a['JMLKEMAS'], 8) . "0000\n";
            $FF.= "DCAR02017  " . fixLen($a['BRUTO'], 17) . "\n";
            $FF.= "DCAR020190 " . fixLen($a['VOLUME'], 17) . "\n"; 
            
            #RECORD TYPE DCAR0301
            $FF.= "DCAR0301" . fixLen($a['NOPOS'], 4) . fixLen($a['NOPOSSUB'], 4) . "MB " . fixLen($a['NOBL'], 30) . fixLen($a['TGBL'], 8) . "\n";
            $FF.= "DCAR0301" . fixLen($a['NOPOS'], 4) . fixLen($a['NOPOSSUB'], 4) . "MWB" . fixLen($a['NOBL'], 30) . fixLen($a['TGBL'], 8) . "\n";
            $FF.= "DCAR0301" . fixLen($a['NOPOS'], 4) . fixLen($a['NOPOSSUB'], 4) . "BH " . fixLen($a['NOBL'], 30) . fixLen($a['TGBL'], 8) . "\n";
            $FF.= "DCAR0301" . fixLen($a['NOPOS'], 4) . fixLen($a['NOPOSSUB'], 4) . "AWB" . fixLen($a['NOBL'], 30) . fixLen($a['TGBL'], 8) . "\n";
            $FF.= "DCAR0301" . fixLen($a['NOPOS'], 4) . fixLen($a['NOPOSSUB'], 4) . "BM " . fixLen($a['NOBL'], 30) . fixLen($a['TGBL'], 8) . "\n";
        
            #RECORD TYPE DCAR3A01
            $FF.= "DCAR3A0116 " . fixLen($a['JMLCONT'], 5) . "\n";
            $FF.= "DCAR3A0150 " . fixLen($a['MJMLCONT'], 5) . "\n";
            $FF.= "DCAR3A0152 " . fixLen($a['PARTIALJMLCONT'], 5) . "\n";
            
            #RECORD TYPE DCAR0401
            $FF.= "DCAR04019  " . fixLen($a['DPELASAL'], 5) . fixLen($hdr['KPBC'], 6) . "\n";
            $FF.= "DCAR040111 " . fixLen($a['DPELBONGKAR'], 5) . fixLen($hdr['KPBC'], 6) . "\n";
            $FF.= "DCAR04018  " . fixLen($a['DPELLANJUT'], 5) . fixLen($hdr['KPBC'], 6) . "\n";
            
            #RECORD TYPE DCAR0501
            $FF.= "DCAR050122 21 " . fixLen($hdr['KDMOANG'], 2) . fixLen($a['MOTHERVESSEL'], 35) . fixLen($a['NOVOY'], 17) . "\n";
            $FF.= "DCAR050123 21 " . fixLen($hdr['KDMOANG'], 2) . fixLen($a['MOTHERVESSEL'], 35) . fixLen($a['NOVOY'], 17) . "\n";
            $FF.= "DCAR050128 21 " . fixLen($hdr['KDMOANG'], 2) . fixLen($a['MOTHERVESSEL'], 35) . fixLen($a['NOVOY'], 17) . "\n";
            
            #RECORD TYPE DCAR0601
            $FF.= "DCAR0601CZ NAM" . fixLen($a['NMPENGIRIM'], 172) . "  \n";
            $FF.= "DCAR0601CZ ADD" . fixLen($a['ALPENGIRIM'], 172) . "  \n";
            $FF.= "DCAR0601CN NAM" . fixLen($a['NMPENERIMA'], 172) . "  \n";
            $FF.= "DCAR0601CN ADD" . fixLen($a['ALPENERIMA'], 172) . "  \n";
            $FF.= "DCAR0601NI NAM" . fixLen($a['NMPEMBERITAU'], 172) . "  \n";
            $FF.= "DCAR0601NI ADD" . fixLen($a['ALPEMBERITAU'], 172) . "  \n";
            
            #RECORD TYPE DCAR0701
            $FF.= "DCAR0701" . fixLen($a['JNSKEMAS'], 2) . fixLen($a['JMLKEMAS'], 8) . fixLen($a['MJNSKEMAS'], 2) . fixLen($a['MJMLKEMAS'], 8) . fixLen($a['JNSKEMAS'], 2) . fixLen($a['PARTIALJMKKEMAS'], 8) . "\n";
            
            foreach($arrbrg as $kk){
                foreach($kk as $ll)
                {
                    foreach($ll as $mm)
                    {
                        foreach($mm as $nn)
                        {
                            foreach($nn as $oo)
                            {
                                $len = strlen($oo['HS']);
                                $start = 0;
                                while($len>0){
                                    $FF.= "DCAR7A01AAH" . fixLen(substr($oo['HS'],$start,$len), 350) . "\n";
                                    $start += 350;
                                    $len -= 350;
                                }

                                $len = strlen($oo['DESCRIPTION']);
                                $start = 0;
                                while($len>0){
                                    $FF.= "DCAR7A01AAA" . fixLen(substr($oo['DESCRIPTION'],$start,$len), 350) . "\n";
                                    $start += 350;
                                    $len -= 350;
                                }
                            }
                        }
                    }
                }                              
            }            
            unset($kk);
            /*
            foreach($arrbrg[$a['KDGRUP']][$a['NOPOS']][$a['NOPOSSUB']][$a['NOPOSSUBSUB']][$a['SERI']] as $aa){            
            #RECORD TYPE DCAR7A01
            $len = strlen($aa['HS']);
            $start = 0;
            while($len>0){
                $FF.= "DCAR7A01AAH" . fixLen(substr($aa['HS'],$start,$len), 350) . "\n";
                $start += 350;
                $len -= 350;
            }
            
            $len = strlen($aa['DESCRIPTION']);
            $start = 0;
            while($len>0){
                $FF.= "DCAR7A01AAA" . fixLen(substr($aa['DESCRIPTION'],$start,$len), 350) . "\n";
                $start += 350;
                $len -= 350;
            }
            }
            unset($aa); 
            */
            
            #RECORD TYPE DCAR0801
            $FF.= "DCAR0801EGW" . fixLen($a['BRUTO'], 17) . "KGM\n";
            $FF.= "DCAR0801VOL" . fixLen($a['VOLUME'], 17) . "M3 \n";  
            foreach($bl as $qq){
            $FF.= "DCAR0801POS000000000" . fixLen($qq['JMLBL'], 5) . "0000POS\n";
            }
            unset($qq);
            #RECORD TYPE DCAR0901
            foreach($arrCont as $aa){
                foreach($aa as $bb)
                {
                    foreach($bb as $cc)
                    {
                        foreach($cc as $dd)
                        {
                            foreach($dd as $ee)
                            {
                                $FF.= "DCAR0901CN " . fixLen($ee['NOCONT'], 30) . fixLen($ee['UKCONT'], 2) . fixLen($ee['TIPECONT'], 1) . fixLen($ee['SEALNUM'], 15) . "  \n";
                                $FF.= "DCAR0901PBP" . fixLen($ee['NOCONT'], 30) . fixLen($ee['UKCONT'], 2) . fixLen($ee['TIPECONT'], 1) . fixLen($ee['SEALNUM'], 15) . "  \n";
                            }
                        }
                    }
                }
                //die();
                
            //print_r($arrCont[$aCont['KDGRUP']][$aCont['NOPOS']][$aCont['NOPOSSUB']][$aCont['NOPOSSUBSUB']]);                
            }            
            unset($aa);             
            
            #RECORD TYPE DCAR1001            
            $len = strlen($a['MERKKEMAS']);
            $start = 0;
            while($len>0){
                $FF.= "DCAR100128 " . fixLen(substr($a['MERKKEMAS'],$start,$len), 240) . "\n";
                $start += 240;
                $len -= 240;
            }
            /*
            foreach($arrdok[$a['KDGRUP']][$a['NOPOS']][$a['NOPOSSUB']][$a['NOPOSSUBSUB']][$a['SERI']] as $aa){
            #RECORD TYPE DCAR1101    
            $FF.= "DCAR1101ZZZ" . fixLen($aa['DOKKODE'], 3) . fixLen($aa['DOKKPBC'], 6) . fixLen($aa['DOKNO'], 6) . fixLen($aa['DOKTG'], 8) . "\n";
            }
            unset($aa);
            */
            foreach($arrdok as $ff){
                foreach($ff as $gg)
                {
                    foreach($gg as $hh)
                    {
                        foreach($hh as $ii)
                        {
                            foreach($ii as $jj)
                            {
                                $FF.= "DCAR1101ZZZ" . fixLen($jj['DOKKODE'], 3) . fixLen($jj['DOKKPBC'], 6) . fixLen($jj['DOKNO'], 6) . fixLen($jj['DOKTG'], 8) . "\n";
                            }
                        }
                    }
                }                              
            }            
            unset($ff);
            
        }
        unset($dtl);  
        unset($a);
        
        $fullPaht = 'Flat/' . $EDINUMBER;
        if (!is_dir($fullPaht)) {
            mkdir($fullPaht, 0777, true);
        }
        $fullPaht = $fullPaht . '/' . $car . '.FLT';
        if (file_exists($fullPaht)) {
            unlink($fullPaht);
        }
        $handle = fopen($fullPaht, 'w');
        fwrite($handle, $FF);
        fclose($handle);
        if (file_exists($fullPaht)) {
            $hsl = '2|' . $hdr['KDKPBC'];
        } else {
            $hsl = '0|';
        }
        return $hsl;
    }
    
    function crtQueue($car = '') {
        $this->load->library("nuSoap_lib");
        $nuSoap_lib = new nusoap_client('http://192.168.5.22:8080/EDITranslator/Services?wsdl');
        if ($nuSoap_lib->fault) {
            $text = 'Error: ' . $nuSoap_lib->fault;
        }
        else {
            if ($nuSoap_lib->getError()) {
                $text = 'Error: ' . $nuSoap_lib->getError();
            }
            else {
                $snrf       = date('ymdHis') . rand(10, 99);
                $EDINUMBER  = $this->newsession->userdata('EDINUMBER');
                $EDIPaht    = 'EDI/' . $EDINUMBER;
                if (!is_dir($EDIPaht)){
                    mkdir($EDIPaht,0777,true);
                    chmod($EDIPaht,0777);
                }
                $FLTPaht    = 'Flat/' . $EDINUMBER;
                if (!is_dir($FLTPaht)){
                    mkdir($FLTPaht,0777,true);
                    chmod($FLTPaht,0777);
                }
                $row = $nuSoap_lib->call('generateEDIFACT', array(  'EDINumber' => $EDINUMBER,
                                                                    'fileName'  => $car.'.FLT',
                                                                    'APRF'      => 'DOKPIB09',
                                                                    'SNRF'      => $snrf), 'http://services.translator.edii/');
                
                if ($row == 'true') {
                    if (file_exists($EDIPaht.'/'. $car .'.EDI') && file_exists($FLTPaht.'/'. $car .'.FLT')) {
                        //return print_r($row,true).'oke';
                        $dirEdiNumber = 'Transaction/' . $EDINUMBER . '/' . date('Ymd');
                        if (!is_dir($dirEdiNumber)){mkdir($dirEdiNumber,0777,true);}
                        rename($FLTPaht.'/'. $car .'.FLT', $dirEdiNumber . '/' . $car . '.FLT');
                        rename($EDIPaht.'/'. $car .'.EDI', $dirEdiNumber . '/' . $car . '.EDI');
                        $rtn = $snrf . '|' . $dirEdiNumber . '/' . $car . '.EDI' . '|' . $dirEdiNumber . '/' . $car . '.FLT';
                    }
                    else {
                        //return print_r($row,true).'not oke';
                        $rtn = 'ERROR';
                    }
                } 
                else {
                    $rtn = 'ERROR';
                }
            }
        }
        return $rtn;
    }
    
    function updateSNRF($car, $data) {
        #update t_bc20hdr
        $this->load->model('actMain');
        $arrData = explode('|', $data);
        $dataHdr['SNRF'] = $arrData[0];
        $dataHdr['DIRFLT'] = $arrData[2];
        $dataHdr['DIREDI'] = $arrData[1];
        $dataHdr['STATUS'] = '020';
        $dataHdr['CAR'] = $car;
        $dataHdr['KODE_TRADER'] = $this->kd_trader;
        $this->actMain->insertRefernce('t_bc20hdr', $dataHdr);

        #insert m_trader_dokoutbound
        $dataDOK['KODE_TRADER'] = $this->kd_trader;
        $dataDOK['KDKPBC'] = $arrData[3];
        $dataDOK['JNSDOK'] = 'BC20';
        $dataDOK['APRF'] = 'DOKPIB09';
        $dataDOK['CAR'] = $car;
        $dataDOK['STATUS'] = '00';
        #$this->actMain->insertRefernce('m_trader_dokoutbound', $dataDOK);
        return '0';
    }
    
    function cetakDokumen($car, $jnPage, $arr ) {
        //die($car.'|snv'.$jnPage);
        $this->load->library('fpdf');
        $this->load->model('actMain');
        $this->load->model('dokBC11');
        $this->dokBC11->ciMain($this->actMain);
        $this->dokBC11->fpdf($this->fpdf);
	$this->dokBC11->segmentUri($arr);				
        $this->dokBC11->showPage($car, $jnPage);
    }
}
