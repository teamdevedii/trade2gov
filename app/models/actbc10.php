<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class actBC10 extends CI_Model {
    var $kd_trader;
    function __construct() {
        parent::__construct();
        $this->kd_trader = $this->newsession->userdata('KODE_TRADER');
        return true;
    }
    
    function lstBC10() {
        $this->load->library('newtable');
        $tipe = 'lstBC10';
        $this->load->model("actMain");
        $SQL = "SELECT  CONCAT(SUBSTRING(A.CAR,1,6),'-',SUBSTRING(A.CAR,7,6),'-',SUBSTRING(A.CAR,13,8),'-',SUBSTRING(A.CAR,21,8)) AS 'No Aju', "
                    . " CONCAT(A.REGMODA,' - ',A.NMMODA) AS 'Nama Angkut', "
                    . " B.URAIAN AS 'Jenis Manifest', "
                    . " C.URAIAN AS 'Status', "
                    . "CAR "
             . "FROM t_bc10hdr A LEFT JOIN m_tabel B ON A.JNSMANIFEST = B.KDREC AND B.MODUL = 'BC10' "
                             . " LEFT JOIN m_tabel C ON C.KDREC = A.STATUS AND C.KDTAB = 'STATUS' AND C.MODUL = 'BC10' "
             . "WHERE KODE_TRADER= '" . $this->kd_trader . "'";
        $prosesnya = array(
            'Create'    => array('ADDAJAX', site_url('bc10/create'), '0', '_content_'),
            'View'      => array('GETAJAX', site_url('bc10/create'), '1', '_content_'),
            'Print'     => array('GETPOP', site_url('bc10/beforeCetak'), '1', ' CETAK RKSP / JKSP |400|200'),
            'Hapus '    => array('DELETEAJAX', site_url('bc10/deleteRKSP'), 'N', '_content_'));
        $this->newtable->menu($prosesnya);
        $this->newtable->keys(array('CAR'));
        $this->newtable->hiddens(array('CAR'));
        $this->newtable->search(array(
            array('CAR', 'No. Aju'),
            array('a.KATEKS', 'Katagori Ekspor', 'tag-select', $this->actMain->get_mtabel('KATEKS', 1, false, '', 'KODEUR', 'BC30')),
            array('a.NAMABELI', 'Nama Pembeli'),
            array('a.ANGKUTNAMA', 'Nama Moda'),
            array('a.STATUS', 'Status', 'tag-select', $this->actMain->get_mtabel('STATUS', 1, false, '', 'KODEUR', 'BC10'))));
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->action(site_url("bc10/lstBC10"));
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->orderby(1, 2);
        $this->newtable->sortby("DESC");
        $this->newtable->tipe_check('radio');
        $this->newtable->set_formid("f" . $tipe);
        $this->newtable->set_divid("div" . $tipe);
        $this->newtable->rowcount(20);
        $this->newtable->tipe_proses('button');
        $this->newtable->clear();
        $tabel .= $this->newtable->generate($SQL);
        $arrdata = array('tabel' => $tabel, 'tipe' => $tipe);
        if ($this->input->post("ajax")) {
            return $tabel;
        } else {
            return $arrdata;
        }
    }
    
    function getHeader($car=''){
        $this->load->model('actMain');
        if ($car) {
            $SQL = "SELECT A.*, "
                        . "DATE_FORMAT(A.TGBC10,'%Y-%m-%d') as TGBC10, " 
                        . "B.URAIAN AS 'URJNSMANIFEST', C.URAIAN as 'URSTATUS', " 
                        . "D.URAIAN_KPBC as 'URKDKPBC', "
                        . "E.URAIAN_NEGARA as 'URFLAGMODA', "
                        . "F.NM_TAMBAT as 'URKADE', "
                        . "G.URAIAN_PELABUHAN as 'URPELMUAT', "
                        . "H.URAIAN_PELABUHAN as 'URPELTRANSIT', "
                        . "I.URAIAN_PELABUHAN as 'URPELBERIKUT', "
                        . "J.URAIAN_PELABUHAN as 'URPELBONGKAR', "
                        . "K.URAIAN_KEMASAN as 'URJNSKMS' "
                 . "FROM t_bc10hdr A LEFT JOIN m_tabel B ON A.JNSMANIFEST = B.KDREC AND B.MODUL = 'BC10' "
                                  . "LEFT JOIN m_tabel C ON C.KDREC = A.STATUS AND C.KDTAB = 'STATUS' AND C.MODUL = 'BC10' "
                                  . "LEFT JOIN m_kpbc D ON D.KDKPBC = A.KDKPBC  " 
                                  . "LEFT JOIN m_negara E ON E.KODE_NEGARA = A.FLAGMODA  " 
                                  . "LEFT JOIN m_tambat F ON F.KD_TAMBAT = A.KADE " 
                                  . "LEFT JOIN m_pelabuhan G ON G.KODE_PELABUHAN = A.PELMUAT " 
                                  . "LEFT JOIN m_pelabuhan H ON H.KODE_PELABUHAN = A.PELTRANSIT " 
                                  . "LEFT JOIN m_pelabuhan I ON I.KODE_PELABUHAN = A.PELBERIKUT " 
                                  . "LEFT JOIN m_pelabuhan J ON J.KODE_PELABUHAN = A.PELBONGKAR " 
                                  . "LEFT JOIN m_kemasan K ON K.KODE_KEMASAN = A.JNSKMS " 
                 . "WHERE CAR='" . $car . "' AND KODE_TRADER='" . $this->kd_trader . "'";
            $data['HEADER'] = $this->actMain->get_result($SQL);
            $data['act']    = 'update';
        } else {
            $sql = "SELECT a.KODE_ID as 'KDIDAGEN', "
                       . " a.ID as 'NOIDAGEN', "
                       . " a.NAMA as 'NMAGEN',"
                       . " a.ALAMAT as 'ALAGEN',"
                       . " b.KODEAGEN as 'KDAGEN' "
                 . "FROM m_trader a LEFT JOIN m_trader_bc10 b on a.KODE_TRADER = b.KODE_TRADER "
                 . "WHERE a.KODE_TRADER = '" . $this->kd_trader . "'";
            $data['HEADER'] = $this->actMain->get_result($sql);
            $data['act'] = 'save';
        }
        $data ['KDIDAGEN']  = $this->actMain->get_mtabel('JENIS_IDENTITAS', 1, false, '', 'KODEDANUR', 'BC10');
        $data['MODA']       = $this->actMain->get_mtabel('MODA', 1, false, '', 'KODEDANUR', 'BC10');
        $data['MAXLENGTH']  = '<input type="hidden" id="MAXLENGTH" value="' . $this->actMain->get_uraian("SELECT F_MAXLENGTH('t_bc10hdr') as MAXLENGTH", 'MAXLENGTH') . '"/>';
        return $data;
    }
    
    function setHeader($data){
        $this->load->model('actMain');
        $updateCar = false ;
        $data['HEADER']['NOIDAGEN'] = str_replace('.', '', str_replace('-', '', $data['HEADER']['NOIDAGEN']));
        if($data['HEADER']['CAR'] == ''){
            $data['HEADER']['CAR'] = $this->actMain->get_car('BC10');
            $updateCar = true ;
        }
        $this->actMain->insertRefernce('m_trader_kapalmanifest',$data['HEADER']);
        $int = $this->actMain->insertRefernce('t_bc10hdr',$data['HEADER']);
        if($int && $updateCar ){$this->actMain->set_car('BC10');}
        $rtn = $this->validasi($data['HEADER']['CAR']);
        return $rtn;
    }
    
    function validasi($car){
        $this->load->model('actMain');
        $i = 0;
        $query = "SELECT A.* FROM t_bc10hdr A WHERE A.CAR = '" . $car . "' AND A.KODE_TRADER = " . $this->kd_trader;
        $aH = $this->actMain->get_result($query);
        if (count($aH) > 0) {
            $judul = 'Hasil validasi Jadwal Kapal Nomor Aju : ' . substr($car, 20) . ' tanggal ' . date('d-m-Y', strtotime(substr($car, 12, 8)));
            $query = "SELECT FLDNAME,MESSAGE from m_validasi WHERE SCRNAME = 'T_BC10HDR' AND MANDATORY = 1 ";
            $arrdataV = $this->actMain->get_result($query, true);
            foreach ($arrdataV as $aV) {
                if (trim($aH[$aV['FLDNAME']]) == '') {
                    $hasil .= substr('   ' . ++$i, -3) . '. ' . $aV['MESSAGE'] . ' Harus diisi<br>';
                }
            }
            #khusus
            if ($aH['KADE'] != '') {
                $sql = "SELECT COUNT(KD_TAMBAT) as jml FROM m_tambat WHERE KD_TAMBAT ='" . $aH['KADE'] . "'";
                $jmlGdg = $this->actMain->get_uraian($sql, 'jml');
                if ($jmlGdg <= 0) {
                    $hasil .= substr('   ' . ++$i, -3) . '. Kode Kade ' . $aH['TMPTBN'] . ' tidak tersedia';
                }
            }
            
            if (strpos('|00|01|05|', $aH['STATUS'])){
                $aH['STATUS'] = ($i == 0) ? '01' : '00';
            }
            $this->db->where(array('CAR' => $car, 'KODE_TRADER' => $this->kd_trader));
            $this->db->update('t_bc10hdr', $aH);
            unset($aH);
            //tulis informasi
            if ($i == 0) {
                $hasil .= '<br>------------------ Data Lengkap ------------------<br>';
            } else {
                $hasil .= '<br>--------------- Data Tidak Lengkap ---------------<br>';
            }
        }
        $view['data'] = $hasil;
        $view['judul'] = $judul;
        $msg = $this->load->view('notify', $view, true);
        return $msg.'|'.$car;
    }
    
    function genFlatfile($car) {
        function fixLen($str, $len, $chr = ' ', $alg = STR_PAD_RIGHT){
            $hasil = str_pad(substr($str, 0, $len), $len, $chr, $alg);
            return $hasil;
        }
        $this->load->model('actMain');
        $EDINUMBER = $this->newsession->userdata('EDINUMBER');
        $SQL = "SELECT 	a.*, b.NOMOREDI, c.`STATUS` as 'STATUS_APRF', "
                     . "DATE_FORMAT(a.ETATGVOY,'%Y%m%d') as 'ETATGVOY', "
                     . "DATE_FORMAT(a.ETDTGVOY,'%Y%m%d') as 'ETDTGVOY', "
                     . "DATE_FORMAT(NOW(),'%Y%m%d') as 'TGL_AJU', "
                     . "DATE_FORMAT(a.WKDOK ,'%Y%m%d%H%i') as 'WKDOK', "
                     . "DATE_FORMAT(a.TGBC10 ,'%Y%m%d%H%i') as 'TGBC10' "
                . "FROM t_bc10hdr a LEFT JOIN m_trader_partner b ON a.KODE_TRADER = b.KODE_TRADER and b.KDKPBC = a.KDKPBC "
                . "LEFT JOIN m_trader_partner_detil c ON a.KODE_TRADER = c.KODE_TRADER AND c.KDKPBC = a.KDKPBC AND c.APRF = 'RKSP' "
                . "WHERE a.CAR = '" . $car ."' AND a.KODE_TRADER = '" . $this->kd_trader . "'";
        $hdr = $this->actMain->get_result($SQL);
        $hdr = str_replace("\n", '', str_replace("\r", '', $hdr));
        if ($hdr['STATUS'] != '01') {
            if ($hdr['STATUS'] == '02') {
                $whHDR['KODE_TRADER'] = $this->kd_trader;
                $whHDR['CAR'] = $car;
                $this->db->where($whHDR);
                $upHDR['SNRF'] = '';
                $upHDR['DIRFLT'] = '';
                $upHDR['DIREDI'] = '';
                $upHDR['STATUS'] = '010';
                $this->db->update('t_bc10hdr', $upHDR);
                unset($upHDR);
                #delete dokoutbound
                $whHDR['KDKPBC'] = $hdr['KDKPBC'];
                $whHDR['JNSDOK'] = 'BC10';
                $this->db->delete('m_trader_dokoutbound', $whHDR);
                unset($whHDR);
                unlink($hdr['DIRFLT']);
                unlink($hdr['DIREDI']);
                $hsl = '1|';
            } else {
                $hsl = '0|';
            }
            unset($hdr);
            return $hsl;
        }
        if ($hdr['NOMOREDI'] == '') {
            return 'Edinumber Kantor Pelayanan belum terekam.';
        }
        $DOKUMEN    = ($hdr['JNSMANIFEST'] == 'R') ? 'DOKRKSP' : 'DOKJKSP';
        $KDDOK      = ($hdr['JNSMANIFEST'] == 'R') ? '781' : '957';
        #SEPARATOR UNTUK HEADER
        $FF.= 'ENV00101' . fixLen($EDINUMBER, 20) . fixLen($hdr['NOMOREDI'], 20) . "DOKRKSP   2\n";
        $FF.= 'HREP0101' . fixLen($hdr['CAR'], 26) . ' 9' . fixLen($KDDOK, 3) . '242' . fixLen($hdr['WKDOK'] , 12) . "203\n";
        $FF.= 'HREP0201AANAAMTNE' . fixLen(($hdr['GT'] * 100), 8, '0', STR_PAD_LEFT) . "\n";
        $FF.= 'HREP0201LAOLM MTR' . fixLen(($hdr['LOA'] * 100), 8, '0', STR_PAD_LEFT) . "\n";
        $FF.= 'HREP0301CG ' . fixLen($hdr['KDIDAGEN'], 1) . fixLen($hdr['NOIDAGEN'], 15) . fixLen($hdr['NMAGEN'], 40) . fixLen($hdr['ALAGEN'], 50) . fixLen($hdr['PEMBERITAHU'], 30) . "\n";
        $FF.= 'HREP040111 ' . fixLen(substr($hdr['CAR'], -6),6)   
                            . fixLen($hdr['ETANOVOY'], 7)
                            . fixLen($hdr['MODA'], 2)
                            . fixLen($hdr['KDAGEN'], 3)
                            . fixLen($hdr['REGMODA'], 9)
                            . fixLen($hdr['NMMODA'], 35)
                            . fixLen($hdr['FLAGMODA'], 2)
                            . fixLen($hdr['NOBC10'], 6)
                            . fixLen($hdr['TGBC10'], 8). "\n";
        
        $FF.= 'HREP040112 ' . fixLen(substr($hdr['CAR'], -6),6) 
                            . fixLen($hdr['ETDNOVOY'], 7)
                            . fixLen($hdr['MODA'], 2)
                            . fixLen($hdr['KDAGEN'], 3)
                            . fixLen($hdr['REGMODA'], 9)
                            . fixLen($hdr['NMMODA'], 35)
                            . fixLen($hdr['FLAGMODA'], 2)
                            . fixLen($hdr['NOBC10'], 6)
                            . fixLen($hdr['TGBC10'], 8). "\n";
        
        $FF.= 'HREP0501132' . fixLen($hdr['ETATGVOY'], 8) . fixLen(str_replace(':', '', $hdr['ETAWKVOY']), 4) . "203\n";
        $FF.= 'HREP0501133' . fixLen($hdr['ETDTGVOY'], 8) . fixLen(str_replace(':', '', $hdr['ETDWKVOY']), 4) . "203\n";
        $FF.= 'HREP0501253' . fixLen(substr($hdr['CAR'], 13, 8), 8) . "    203\n";
        $FF.= 'HREP0501182' . fixLen($hdr['TGL_AJU'], 8) . "    203\n";
        
        #RECORD TYPE HREP0601 
        $FF.= 'HREP060141 ' . fixLen($hdr['KDKPBC'], 6) . "      \n";
        $FF.= 'HREP060180 ' . fixLen($hdr['PELMUAT'], 6) . "      \n";
        $FF.= 'HREP06019  ' . fixLen($hdr['PELMUAT'], 6) . "      \n";
        $FF.= 'HREP0601127' . fixLen($hdr['PELTRANSIT'], 6) . "      \n";
        $FF.= 'HREP060161 ' . fixLen($hdr['PELBERIKUT'], 6) . "      \n";
        $FF.= 'HREP060111 ' . fixLen($hdr['PELBONGKAR'], 6) . fixLen($hdr['KADE'], 6) . "\n";
        
        #RECORD TYPE HDEC0701
        $FF.= 'HREP0701AAEAEQMTR' . fixLen($hdr['DRAFTDEPAN'] * 100, 8) . "\n";
        $FF.= 'HREP0701AAEAERMTR' . fixLen($hdr['DRAFTBELAKANG'] * 100, 8) . "\n";
        
        #RECORD TYPE HDEC0801
        $FF.= 'HREP0801KMS' . fixLen($hdr['JMLKMS'], 8, '0', STR_PAD_LEFT) . fixLen($hdr['JNSKMS'], 2) . "\n";
        $len = strlen($hdr['MEMO']);
        $start = 0;
        while ($len > 0) {
            $FF.= 'HREP0801AAI' . fixLen(substr($hdr['MEMO'], $start, $len), 350) . "\n";
            $start  += 350;
            $len    -= 350;
        }
        $FF.= 'UNZ00101';
        $fullPaht = 'FLAT/' . $EDINUMBER;
        if (!is_dir($fullPaht)) {
            mkdir($fullPaht, 0777, true);
        }
        $fullPaht = $fullPaht . '/' . $car . '.FLT';
        if (file_exists($fullPaht)) {
            unlink($fullPaht);
        }
        $handle = fopen($fullPaht, 'w');
        fwrite($handle, $FF);
        fclose($handle);
        if (file_exists($fullPaht)) {
            $hsl = '2|' . $hdr['KDKPBC'];
        } else {
            $hsl = '0|';
        }
        return $hsl;
    }
    
    function crtQueue($car) {
        $this->load->model('actTranslator');
        $SNRF       = date('ymdHis') . rand(10, 99);
        $EDINUMBER  = $this->newsession->userdata('EDINUMBER');
        $EDIPaht    = 'EDI/' . $EDINUMBER;
        if (!is_dir($EDIPaht)){
            mkdir($EDIPaht,0777,true);
        }else{
            chmod($EDIPaht,0777);
        }
        $FLTPaht    = 'FLAT/' . $EDINUMBER;
        if (!is_dir($FLTPaht)){
            mkdir($FLTPaht,0777,true);
        }else{
            chmod($FLTPaht,0777);
        }
        $this->actTranslator->getEDIFILE($EDINUMBER,'DOKRKSP',$SNRF,$car.'.FLT');
        if (file_exists($EDIPaht.'/'. $car .'.EDI') && file_exists($FLTPaht.'/'. $car .'.FLT')){
            $dirEdiNumber = 'TRANSACTION/' . $EDINUMBER . '/' . date('Ymd');
            if (!is_dir($dirEdiNumber)){mkdir($dirEdiNumber,0777,true);}
            rename($FLTPaht.'/'. $car .'.FLT', $dirEdiNumber . '/' . $car . '.FLT');
            rename($EDIPaht.'/'. $car .'.EDI', $dirEdiNumber . '/' . $car . '.EDI');
            $rtn = $SNRF . '|' . $dirEdiNumber . '/' . $car . '.EDI' . '|' . $dirEdiNumber . '/' . $car . '.FLT';
        }
        else {
            $rtn = 'ERROR';
        }
        return $rtn;
    }
    
    function updateSNRF($car, $data) {
        $this->load->model('actMain');
        $arrData = explode('|', $data);
        $dataHdr['SNRF']    = $arrData[0];
        $dataHdr['DIRFLT']  = $arrData[2];
        $dataHdr['DIREDI']  = $arrData[1];
        $dataHdr['STATUS']  = '02';
        $dataHdr['CAR']     = $car;
        $dataHdr['KODE_TRADER'] = $this->kd_trader;
        $this->actMain->insertRefernce('t_bc10hdr', $dataHdr);

        #insert m_trader_dokoutbound
        $dataDOK['KODE_TRADER'] = $this->kd_trader;
        $dataDOK['KDKPBC']      = $arrData[3];
        $dataDOK['JNSDOK']      = 'BC10';
        $dataDOK['APRF']        = 'DOKRKSP';
        $dataDOK['CAR']         = $car;
        $dataDOK['STATUS']      = '00';
        $this->actMain->insertRefernce('m_trader_dokoutbound', $dataDOK);
        return '0';
    }
    
    function deleteRKSP($data){
        $arrWhr['CAR']        = $data[0];
        $arrWhr['KODE_TRADER']= $this->kd_trader;
        $this->db->where($arrWhr);
        $exec = $this->db->delete(array('t_bc10hdr','t_bc10res'));
        return 'MSG|OK|'.site_url('bc10/lstBC10').'|Data RKSP berhasil dihapus.';
    }
    
    #yang dipakai ==============================================================
    
    function cetakDokumen($car, $jnPage, $arr) {
        //die($car.'|snv'.$jnPage);
        $this->load->library('fpdf');
        $this->load->model('actMain');
        $this->load->model('dokBC10');
        $this->dokBC10->ciMain($this->actMain);
        $this->dokBC10->fpdf($this->fpdf);
        $this->dokBC10->segmentUri($arr);
        $this->dokBC10->showPage($car, $jnPage);
    }
    
    function delRKSPhdr($key) {
        //print_r('sini');die();
        $arr = explode('|', $key[0]);
        $car = $arr[0];
        $this->db->where_in("CAR", $key);
        $exec = $this->db->delete('t_bc10hdr'); //print_r($exec);die();
        if ($exec) {
            $rtn = 'MSG|OK|' . site_url('bc10/daftarRKSP/' . $car) . '|Dalete data detil berhasil.';
        } else {
            $rtn = 'MSG|ER|' . site_url('bc10/daftarRKSP/' . $car) . '|Dalete data detil gagal.';
        }
        return $rtn;
    }
}
