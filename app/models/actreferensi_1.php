<?php
if (!defined('BASEPATH')){exit('No direct script access allowed');}
class actReferensi_1 extends CI_Model {
    var $kd_trader = '';
    public function __construct() {
        parent::__construct();
        if (!$this->newsession->userdata('LOGGED')) {
            redirect();
        }
        $this->kd_trader = $this->newsession->userdata('KODE_TRADER');
        return true;
    }

    function lsvDataPendukung($type, $key = '') {
        $this->load->library('newtable');
        $this->newtable->keys = '';
        switch (strtolower($type)) {
            case'indentoreks':
                $this->load->model('actMain');
                $SQL = "select 	a.KODE_TRADER, 
                                a.IDQQ, 
                                a.NPWPQQ ,
                                b.URAIAN as 'Jns Identitas' , 
                                a.NPWPQQ as 'No. Identitas', 
                                a.NAMAQQ as 'Nama Indentor', 
                                a.ALMTQQ as 'Almt Indentor' 
                        from m_trader_indentoreks a
                        LEFT JOIN  m_tabel       b ON b.MODUL = 'BC30' AND b.KDTAB = 'JENIS_IDENTITAS' AND b.KDREC = a.IDQQ
                        WHERE   a.KODE_TRADER = '" . $this->kd_trader . "'";
                
                $this->newtable->keys(array('KODE_TRADER','IDQQ','NPWPQQ'));
                $this->newtable->hiddens(array('KODE_TRADER','IDQQ','NPWPQQ'));
                $this->newtable->detail(site_url('referensi_1/dataPendukung/indentoreks'));
                $this->newtable->search(array(
                    array('a.IDQQ', 'Identitas', 'tag-select', $this->actMain->get_mtabel('JENIS_IDENTITAS', 1, true, '', 'KODEUR', 'BC30')),
                    array('a.NPWPQQ', 'NPWP indentor'),
                    array('a.NAMAQQ', 'Nama Indentor'),
                    array('a.ALMTQQ', 'Alamat Indentor')));
                $process['Ubah ' . $type]   =  array('EDITAJAX', site_url('referensi_1/' . $type), '1', '_content_'); 
                $process['Hapus ' . $type]  =  array('DELETEAJAX', site_url('referensi_1/' . $type), 'N', '_content_'); 
                break;
            case'pembeli':
                $this->load->model('actMain');
                $SQL = "select 	a.KODE_TRADER, 
                                a.NAMABELI, 
                                a.NAMABELI as 'Nama', 
                                a.ALMTBELI as 'Alamat', 
                                a.NEGBELI as 'Negara Asal' 
                        from m_trader_pembeli a
                        WHERE   a.KODE_TRADER = '" . $this->kd_trader . "'";
                $this->newtable->keys(array('KODE_TRADER','NAMABELI'));
                $this->newtable->hiddens(array('KODE_TRADER','NAMABELI'));
                $this->newtable->detail(site_url('referensi_1/dataPendukung/pembeli'));
                $this->newtable->search(array(
                    array('a.NAMABELI', 'Nama Beli'),
                    array('a.ALMTBELI', 'Alamat Beli'),
                    array('a.NEGBELI', 'Negara Beli')));
                $process['Ubah ' . $type]   =  array('EDITAJAX', site_url('referensi_1/' . $type), '1', '_content_'); 
                $process['Hapus ' . $type]  =  array('DELETEAJAX', site_url('referensi_1/' . $type), 'N', '_content_'); 
                break;
            case'parties':
                $this->load->model('actMain');
                $SQL = "select 	a.KODE_TRADER, 
                                a.KODEPARTIES, 
                                a.NAMAPARTIES,
                                a.NAMAPARTIES as 'Nama Parties',
                                b.URAIAN as 'Kode Parties',
                                a.ALMTPARTIES as 'Alamat' 
                        from m_trader_parties a
                        LEFT JOIN  m_tabel       b ON b.MODUL = 'REF' AND b.KDTAB = 'KD_PARTIES' AND b.KDREC = a.KODEPARTIES
                        WHERE   a.KODE_TRADER = '" . $this->kd_trader . "'";
                $this->newtable->keys(array('KODE_TRADER','KODEPARTIES','NAMAPARTIES'));
                $this->newtable->hiddens(array('KODE_TRADER','KODEPARTIES','NAMAPARTIES'));
                $this->newtable->detail(site_url('referensi_1/dataPendukung/parties'));
                $this->newtable->search(array(
                    array('a.NAMAPARTIES', 'Nama Parties'),
                    array('a.KODEPARTIES', 'Kode Parties','tag-select', $this->actMain->get_mtabel('KD_PARTIES', 1,false,'','KODEUR','REF'))));
                $process['Tambah '.$type]   =  array('ADDAJAX', site_url('referensi_1/'.$type), '0', '_content_');
                $process['Ubah ' . $type]   =  array('EDITAJAX', site_url('referensi_1/' . $type), '1', '_content_'); 
                $process['Hapus ' . $type]  =  array('DELETEAJAX', site_url('referensi_1/' . $type), 'N', '_content_'); 
                break;
            default :
                return "Failed";
                exit();
        }
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->action(site_url('referensi_1/datapendukung/' . $type));
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->rowcount(10);
        $this->newtable->clear();
        $this->newtable->set_formid("f" . $type);
        $this->newtable->set_divid("div" . $type);
        if (!isset($process)) {
            $process['Ubah ' . $type]   =  array('EDITAJAX', site_url('referensi_1/' . $type), '1', '_content_');
            $process['Hapus ' . $type]  =  array('DELETEAJAX', site_url('referensi_1/' . $type), 'N', '_content_'); 
        }
        $this->newtable->menu($process);
        $tabel = $this->newtable->generate($SQL);
        $arrdata = array("tabel" => $tabel);
        if ($this->input->post("ajax"))
            return $tabel;
        else
            return $arrdata;
    }
    
    function getParties($act, $kd_trader = '', $kd_parties = '', $nama_paries = '') {
        $this->load->model('actMain');
        switch ($act) {
            case 'edit':
                if ($kd_trader != '' && $kd_parties != '' && $nama_paries != '') {
                    $sql = "select * from m_trader_parties
                            where kode_trader = '".$kd_trader."' and kodeparties = '".$kd_parties."' and namaparties = '".$nama_paries."'";
                    $hasil['PARTIES'] = $this->actMain->get_result($sql);
                }
                else {
                    $hasil = 'error';
                }
                break;
            case'add':
                $hasil['act']     = 'insert';
                $hasil['PARTIES']['KODE_TRADER']= $this->kd_trader;
                break;
        }
        $hasil['KD_PARTIES']      = $this->actMain->get_mtabel('KD_PARTIES', 1, false,'','KODEDANUR','REF');
        return $hasil;
    }

    function setParties($act) {
        $this->load->model('actMain');
        if ($act == 'insert' || $act == 'update'){
            $data = $this->input->post('PARTIES');
            $exec = $this->actMain->insertRefernce('m_trader_parties',$data);
            if ($exec) {
                $rtn = 'MSG|OK|' . site_url('referensi_1/dataPendukung/parties') . '| Proses ' .$act. ' data parties berhasil';
            }
            else {
                $rtn = "MSG|ERR||Proses data barang gagal.";
            }
        }
        else if ($act == 'delete') {
            foreach ($this->input->post('tb_chkfparties') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $data['KODE_TRADER']    = $arrchk[0];
                $data['KODEPARTIES']    = $arrchk[1];
                $data['NAMAPARTIES']    = $arrchk[2];
                $this->db->where($data);
                $exec = $this->db->delete('m_trader_parties');
            }
            if ($exec) {
                $rtn = 'MSG|OK|'.site_url('referensi_1/dataPendukung/parties'.$data['CAR']) . '|Dalete data detil berhasil.';
            } else {
                $rtn = 'MSG|ER|'.site_url('referensi_1/dataPendukung/parties'.$data['CAR']) . '|Dalete data detil gagal.';
            }
        }
        return $rtn;
    }
    
    function getPembeli($act, $kd_trader = '', $nm_beli = '') {
        $this->load->model('actMain');
        switch ($act) {
            case 'edit':
                if ($kd_trader != '' && $nm_beli != '') {
                    $sql = "select * from m_trader_pembeli
                                where kode_trader ='".$kd_trader."' and namabeli ='".$nm_beli."'";
                    $hasil['PEMBELI'] = $this->actMain->get_result($sql);
                }
                else {
                    $hasil = 'error';
                }
                break;
            case'add':
                break;
        }
        return $hasil;
    }
    
    function setPembeli($act) {
        $this->load->model('actMain');
        switch ($act) {
            case'update':
                $pembeli = $this->input->post('PEMBELI');
                $exec = $this->actMain->insertRefernce('m_trader_pembeli', $pembeli);
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi_1/dataPendukung/pembeli') . '|Update data pembeli berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi_1/dataPendukung/pembeli') . '|Update data gagal';
                }
                break;
            case'delete':
                $this->input->post('tb_chkfpembeli');
                foreach ($this->input->post('tb_chkfpembeli') as $chkitem){
                    $arrchk = explode("|", $chkitem);
                    $data['KODE_TRADER'] = $arrchk[0];
                    $data['NAMABELI']    = $arrchk[1];
                $this->db->where($data); 
                $exec = $this->db->delete('m_trader_pembeli');
                }
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi_1/dataPendukung/pembeli') . '|Delete data pembeli berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi_1/dataPendukung/pembeli') . '|Delete data pembeli gagal';
                }
                break;
        }
        return $hasil;
    }
        
    function getIndentorEks($act, $kd_trader = '', $id_qq = '', $npwp_qq = '') {
        $this->load->model('actMain');
        switch ($act) {
            case 'edit':
                if ($kd_trader != '' && $id_qq != '' && $npwp_qq != '') {
                    $sql = "select a.*, b.URAIAN as 'URIDQQ' from m_trader_indentoreks a
                            LEFT JOIN  m_tabel       b ON b.MODUL = 'BC30' AND b.KDTAB = 'JENIS_IDENTITAS' AND b.KDREC = a.IDQQ
                            where a.KODE_TRADER ='".$kd_trader."' and a.IDQQ='".$id_qq."' and a.NPWPQQ ='".$npwp_qq."'";
                    $hasil['INDENTOREKS'] = $this->actMain->get_result($sql);
                }
                else {
                    $hasil = 'error';
                }
                break;
            case'add':
                break;
        }
        return $hasil;
    }
    
    function setIndentorEks($act) {
        $this->load->model('actMain');
        switch ($act) {
            case'update':
                $indentoreks = $this->input->post('INDENTOREKS');
                $exec = $this->actMain->insertRefernce('m_trader_indentoreks', $indentoreks);
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi_1/dataPendukung/indentoreks') . '|Update data pembeli berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi_1/dataPendukung/indentoreks') . '|Update data gagal';
                }
                break;
            case'delete':
                $this->input->post('tb_chkfindentoreks');
                foreach ($this->input->post('tb_chkfindentoreks') as $chkitem){
                    $arrchk = explode("|", $chkitem);
                    $data['KODE_TRADER'] = $arrchk[0];
                    $data['IDQQ']        = $arrchk[1];
                    $data['NPWPQQ']      = $arrchk[2];
                $this->db->where($data); 
                $exec = $this->db->delete('m_trader_indentoreks');
                }
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi_1/dataPendukung/indentoreks') . '|Delete data pembeli berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi_1/dataPendukung/indentoreks') . '|Delete data pembeli gagal';
                }
                break;
        }
        return $hasil;
    }
}