<?php
if (!defined('BASEPATH')){exit('No direct script access allowed');}
class actReferensi extends CI_Model {
    var $kd_trader = '';
    public function __construct() {
        parent::__construct();
        if (!$this->newsession->userdata('LOGGED')) {
            redirect();
        }
        $this->kd_trader = $this->newsession->userdata('KODE_TRADER');
        return true;
    }

    function lsvDataPendukung($type, $key = '') {
        $this->load->library('newtable');
        $this->newtable->keys = '';
        switch (strtolower($type)) {
            case'importir':
                $this->load->model('actMain');
                $SQL = "SELECT  b.URAIAN AS 'Jns. Identitas',
                                a.IMPNPWP AS 'No. Identitas',
                                a.IMPNAMA AS 'Nama Importir',
                                c.URAIAN  AS 'Jns. API',
                                a.APINO AS 'No. API',
                                a.IMPALMT AS 'Almt Importir',
                                a.KODE_TRADER,
                                a.IMPID,
                                a.IMPNPWP
                        FROM    M_TRADER_IMPORTIR a left join M_TABEL b on b.MODUL = 'BC20' AND b.KDTAB = 'JENIS_IDENTITAS'  AND b.KDREC = a.IMPID
                                                    left join M_TABEL c on c.MODUL = 'BC20' AND c.KDTAB = 'JENIS_API' AND c.KDREC = a.APIKD
                        WHERE   a.KODE_TRADER = '" . $this->kd_trader . "'";
                $this->newtable->keys(array('KODE_TRADER', 'IMPID', 'IMPNPWP'));
                $this->newtable->hiddens(array('KODE_TRADER', 'IMPID', 'IMPNPWP'));
                $this->newtable->search(array(array('a.IMPID', 'Jns. Identitas', 'tag-select', $this->actMain->get_mtabel('JENIS_IDENTITAS')),
                    array('a.IMPNPWP', 'No. Identitas'),
                    array('a.IMPNAMA', 'Nama Importir'),
                    array('a.APIKD', 'Jns. API', 'tag-select', $this->actMain->get_mtabel('JENIS_API')),
                    array('a.APINO', 'No. API'),
                    array('a.IMPALMT', 'Almt Importir')));
                break;
            case'indentor':
                $this->load->model('actMain');
                $SQL = "SELECT  b.URAIAN 'Jns. Identitas',
                                a.INDNPWP AS 'No. Identitas',
                                a.INDNAMA AS 'Nama Indentor',
                                a.INDALMT AS 'Almt Indentor',
                                a.KODE_TRADER,
                                a.INDID,
                                a.INDNPWP
                        FROM    M_TRADER_INDENTOR a left join M_TABEL b on b.MODUL = 'BC20' AND b.KDTAB = 'JENIS_IDENTITAS' AND b.KDREC = a.INDID
                        WHERE   a.KODE_TRADER = '" . $this->kd_trader . "'";
                $this->newtable->keys(array('KODE_TRADER', 'INDID', 'INDNPWP'));
                $this->newtable->hiddens(array('KODE_TRADER', 'INDID', 'INDNPWP'));
                $this->newtable->search(array(array('a.INDID', 'Jns. Identitas', 'tag-select', $this->actMain->get_mtabel('JENIS_IDENTITAS')),
                    array('a.INDNPWP', 'No. Identitas'),
                    array('a.INDNAMA', 'Nama Indentor'),
                    array('a.INDALMT', 'Almt Indentor')));
                break;
            case'pemasok':
                $SQL = "SELECT  a.PASOKNAMA AS 'Nama Pemasok',
                                a.PASOKALMT AS 'Alamat Pemasok',
                                b.URAIAN_NEGARA  AS 'Negara Pemasok',
                                a.KODE_TRADER,
                                a.PASOKNAMA
                        FROM    M_TRADER_SUPPLIER a left join M_NEGARA b on b.KODE_NEGARA = a.PASOKNEG
                        WHERE   a.KODE_TRADER = '" . $this->kd_trader . "'";
                $this->newtable->keys(array('KODE_TRADER', 'PASOKNAMA'));
                $this->newtable->hiddens(array('KODE_TRADER', 'PASOKNAMA'));
                $this->newtable->search(array(array('a.PASOKNAMA', 'Nama Pemasok'),
                    array('a.PASOKALMT', 'Alamat Pemasok'),
                    array('b.URAIAN_NEGARA', 'Negara Pemasok')));
                break;
            case'moda':
                $this->load->model('actMain');
                $SQL = "SELECT  b.URAIAN 'Jns. Moda',
                                a.ANGKUTNAMA AS 'Nama Moda',
                                c.URAIAN_NEGARA  AS 'Bendera',
                                a.KODE_TRADER,
                                a.MODA,
                                a.ANGKUTNAMA
                        FROM    M_TRADER_KAPAL a left join M_TABEL b on b.MODUL = 'BC20' AND b.KDTAB = 'MODA' AND b.KDREC = a.MODA
                                                 left join M_NEGARA c on c.KODE_NEGARA = a.ANGKUTFL
                        WHERE   a.KODE_TRADER = '" . $this->kd_trader . "'";
                $this->newtable->keys(array('KODE_TRADER', 'MODA', 'ANGKUTNAMA'));
                $this->newtable->hiddens(array('KODE_TRADER', 'MODA', 'ANGKUTNAMA'));
                $this->newtable->search(array(array('a.MODA', 'Jns. Moda', 'tag-select', $this->actMain->get_mtabel('MODA')),
                    array('a.ANGKUTNAMA', 'Nama Moda'),
                    array('c.URAIAN_NEGARA', 'Bendera')));
                break;
            case'tod':
                $SQL = "select A.REGION, B.URAIAN, A.TGLBERLAKU, A.NFREIGHT, A.NASURANSI, A.JNSMODA 
                        from m_tod A
                         LEFT JOIN M_TABEL B ON B.MODUL = 'BC30' AND B.KDTAB = 'MODA' AND B.KDREC = A.JNSMODA";
                //die($SQL);
                $this->newtable->keys(array('REGION','JNSMODA','TGLBERLAKU'));
                $this->newtable->hiddens(array('JNSMODA'));
                $this->newtable->search(array(array('A.REGION', 'Kode Region'),
                    array('B.URAIAN', 'Jenis Moda'),
                    array('A.TGLBERLAKU', 'Tgl Berlaku')));
                $this->newtable->show_chk(false);
                $this->newtable->show_menu(false);
                break;
            case'barang':
                $SQL = "SELECT  a.BRGURAI AS 'Uraian Barang',
                                CONCAT(a.NOHS,' - ',a.SERITRP) AS 'HS - Seri',
                                a.MERK  AS 'Merk',
                                a.TIPE  AS 'Tipe',
                                a.SPFLAIN  AS 'Spesifikasi Lain',
                                a.KODE_TRADER,
                                a.NOHS,
                                a.SERITRP,
                                a.BRGURAI
                        FROM    M_TRADER_BARANG a
                        WHERE   a.KODE_TRADER = '" . $this->kd_trader . "'";
                $this->newtable->keys(array('KODE_TRADER', 'NOHS', 'SERITRP', 'BRGURAI'));
                $this->newtable->hiddens(array('KODE_TRADER', 'NOHS', 'SERITRP', 'BRGURAI'));
                $this->newtable->search(array(array('a.BRGURAI', 'Uraian Barang'),
                    array("CONCAT(a.NOHS,' - ',a.SERITRP)", 'HS - Seri'),
                    array('a.MERK', 'Merk'),
                    array('a.TIPE', 'Tipe'),
                    array('a.SPFLAIN', 'Spesifikasi Lain')));
                break;
            case'tarif':
                $this->load->model('actMain');
                $cmb = $this->actMain->get_mtabel('JENIS_TARIF');
                $SQL = "SELECT  a.KODE_TRADER,
                                a.NOHS,
                                a.SERITRP,
                                CONCAT(a.NOHS,' - ',a.SERITRP) as 'Nomor HS',
                                b.Uraian    as 'Jns Tarif BM',
                                a.TRPBM     as 'Tarif BM',
                                c.Uraian    as 'Jns Tarif Cukai',
                                a.TRPCUK    as 'Tarif Cukai',
                                a.TRPPPN    as 'Tarif PPn',
                                a.TRPPBM    as 'Tarif PPnBM',
                                a.TRPPPH    as 'Tarif PPh'
                        FROM    M_TRADER_TARIF a Left Join M_TABEL b ON b.MODUL = 'BC20' AND b.KDTAB = 'JENIS_TARIF' AND b.KDREC = a.KDTRPBM
                                                 Left Join M_TABEL c ON c.MODUL = 'BC20' AND c.KDTAB = 'JENIS_TARIF' AND c.KDREC = a.KDTRPCUK
                        WHERE   a.KODE_TRADER = '" . $this->kd_trader . "'";
                $this->newtable->keys(array('KODE_TRADER', 'NOHS', 'SERITRP'));
                $this->newtable->hiddens(array('KODE_TRADER', 'NOHS', 'SERITRP', 'BRGURAI'));
                $this->newtable->detail(site_url('referensi/dataPendukung/tarifdetil'));
                $this->newtable->detail_tipe('detil_priview_bottom');
                $this->newtable->search(array(array('a.NOHS', 'Nomor HS'),
                    array('a.KDTRPBM', 'Jns Tarif BM', 'tag-select', $cmb),
                    array('a.KDTRPCUK', 'Jns Tarif Cukai', 'tag-select', $cmb)));
                break;
            case'tarifdetil':
                $arrKey = explode('|', $key);
                $SQL = "SELECT  a.BRGURAI AS 'Uraian Barang',
                                CONCAT(a.NOHS,' - ',a.SERITRP) AS 'HS - Seri',
                                a.MERK  AS 'Merk',
                                a.TIPE  AS 'Tipe',
                                a.SPFLAIN  AS 'Spesifikasi Lain',
                                a.KODE_TRADER,
                                a.NOHS,
                                a.SERITRP,
                                a.BRGURAI
                        FROM    M_TRADER_BARANG a
                        WHERE   a.KODE_TRADER = '" . $this->kd_trader . "' AND NOHS = '" . $arrKey[1] . "' AND SERITRP = '" . $arrKey[2] . "'";
                $this->newtable->keys(array('KODE_TRADER', 'NOHS', 'SERITRP', 'BRGURAI'));
                $this->newtable->hiddens(array('KODE_TRADER', 'NOHS', 'SERITRP', 'BRGURAI'));
                $this->newtable->search(array(array('a.BRGURAI', 'Uraian Barang'),
                    array("CONCAT(a.NOHS,' - ',a.SERITRP)", 'HS - Seri'),
                    array('a.MERK', 'Merk'),
                    array('a.TIPE', 'Tipe'),
                    array('a.SPFLAIN', 'Spesifikasi Lain')));
                break;
            case'bank':
                $SQL = "SELECT  KDBANK AS 'Kode Bank', NMBANK AS 'Nama Bank' FROM    M_BANK";
                $this->newtable->keys(array('Kode Bank', 'Nama Bank'));
                $this->newtable->search(array(
                    array('KDBANK', 'Kode Bank'),
                    array('NMBANK', 'Nama Bank')));
                $process['Tambah ' . $type] = array('ADDAJAX', site_url('referensi/' . $type), '0', '_content_');
                $this->newtable->menu($process);
                break;
            case'daerah':
                $SQL = "SELECT  KDDAERAH AS 'Kode Daerah', 
                                URDAERAH AS 'Uraian Daerah',  
                                IBUKOTA AS 'Ibukota', 
                                KDDAERAH,
                                URDAERAH
                        FROM    M_DAERAH";
                $this->newtable->keys(array('KDDAERAH'));
                $this->newtable->hiddens(array('KDDAERAH', 'URDAERAH'));
                $this->newtable->search(array(array('KDDAERAH', 'Kode Daerah'),
                    array('NMDAERAH', 'Nama Daerah'),
                    array('IBUKOTA', 'Ibukota')));
                $process['Tambah ' . $type] = array('ADDAJAX', site_url('referensi/' . $type), '0', '_content_');
                $this->newtable->menu($process);
                break;
            case'gudang':
                $SQL = "SELECT  KDKPBC AS 'Kode KPBC', 
                                KDGDG AS 'Kode Gudang',
                                URAIAN AS 'Uraian',
                                KDKPBC,
                                KDGDG
                        FROM    M_GUDANG";
                $this->newtable->keys(array('KDKPBC', 'KDGDG'));
                $this->newtable->hiddens(array('KDKPBC', 'KDGDG', 'URAIAN'));
                $this->newtable->search(array(array('KDKPBC', 'Kode KPBC'),
                    array('KDGDG', 'Kode Gudang'),
                    array('URAIAN', 'Uraian')));
                $process = array(
                    'Tambah ' . $type => array('ADDAJAX', site_url('referensi/' . $type), '0', '_content_'),
                    'Ubah ' . $type => array('EDITAJAX', site_url('referensi/' . $type), '1', '_content_'),
                    'Hapus ' . $type => array('DELETEAJAX', site_url('referensi/' . $type), 'N', '_content_'));
                $this->newtable->menu($process);
                break;
            case'hanggar':
                $SQL = "SELECT  KDKPBC AS 'Kode KPBC', ".
                               "KDHGR AS 'Kode Hanggar',".
                               "URHGR AS 'Uraian Hanggar',".
                               "KDKPBC,".
                               "KDHGR ".
                    "FROM    M_HANGGAR";
                $this->newtable->keys(array('KDKPBC', 'KDHGR'));
                $this->newtable->hiddens(array('KDKPBC', 'KDHGR', 'URHGR'));
                $this->newtable->search(array(array('KDKPBC', 'Kode KPBC'),
                    array('KDHGR', 'Kode Hanggar'),
                    array('URHGR', 'Uraian Hanggar')));
                $process = array(
                    'Tambah ' . $type => array('ADDAJAX', site_url('referensi/' . $type), '0', '_content_'),
                    'Ubah ' . $type => array('EDITAJAX', site_url('referensi/' . $type), '1', '_content_'),
                    'Hapus ' . $type => array('DELETEAJAX', site_url('referensi/' . $type), 'N', '_content_'));
                $this->newtable->menu($process);
                break;
            case'manifest_grup':
                $SQL = "SELECT  KDGRUP AS 'Kode Grup', ".
                               "URKDGRUP AS 'Uraian Hanggar',".
                               "ADADETIL AS 'Ada Detil',".
                               "INWARD AS 'Inward',".
                               "OUTWARD AS 'Outward',".
                               "FTZ AS 'FTZ'," .
                               "EKSPOR AS 'Ekspor',".
                               "KDGRUP ".                               
                    "FROM    M_MANIFESTGRUP"; //print_r($SQL);die();
                $this->newtable->keys(array('KDGRUP'));
                $this->newtable->hiddens(array('KDGRUP'));
                $this->newtable->search(array(array('KDGRUP', 'Kode Grup'),
                    array('URKDGRUP', 'Uraian Hanggar'),
                    array('ADADETIL', 'Ada Detil'),
                    array('INWARD', 'Inward'),
                    array('OUTWARD', 'Outward'),
                    array('FTZ', 'FTZ'),
                    array('EKSPOR', 'Ekspor')));
                $process = array(
                    'Tambah ' . $type => array('ADDAJAX', site_url('referensi/' . $type), '0', '_content_'),
                    'Ubah ' . $type => array('EDITAJAX', site_url('referensi/' . $type), '1', '_content_'),
                    'Hapus ' . $type => array('DELETEAJAX', site_url('referensi/' . $type), 'N', '_content_'));
                $this->newtable->menu($process);
                break;
            case'hs':
                $SQL = "SELECT  HS AS 'No HS',  
                                DESCRIPTION AS 'Deskripsi',                                  
                                HS
                        FROM    M_HS";
                $this->newtable->keys(array('HS'));
                $this->newtable->hiddens(array('HS'));
                $this->newtable->search(array(array('HS', 'HS'),
                    array('DESCRIPTION', 'Deskripsi')
                    ));
                $process = array(
                    'Tambah ' . $type => array('ADDAJAX', site_url('referensi/' . $type), '0', '_content_'),
                    'Ubah ' . $type => array('EDITAJAX', site_url('referensi/' . $type), '1', '_content_'),
                    'Hapus ' . $type => array('DELETEAJAX', site_url('referensi/' . $type), 'N', '_content_'));
                $this->newtable->menu($process);
                break;
            case'tod2':
                $this->load->model('actMain');
                $cmb = $this->actMain->get_mtabel('MODA');
                $SQL = "SELECT  a.REGION AS 'Data Region', 
                                b.URAIAN AS 'Jenis Moda',                                  
                                a.TGLBERLAKU AS 'Tanggal Berlaku',
                                a.NFREIGHT AS 'Nilai Freight',
                                a.NASURANSI AS 'Nilai Asuransi',
                                a.REGION,
                                a.JNSMODA,
                                a.TGLBERLAKU                                
                        FROM    M_TOD a
                        LEFT JOIN M_TABEL b ON b.MODUL = 'BC23' AND b.KDTAB = 'MODA' AND b.KDREC = a.JNSMODA";
                $this->newtable->keys(array('REGION','JNSMODA','TGLBERLAKU'));
                $this->newtable->hiddens(array('REGION','JNSMODA','TGLBERLAKU'));
                $this->newtable->search(array(array('REGION', 'Data Region'),
                    array('a.JNSMODA', 'Jenis Moda', 'tag-select', $cmb),
                    array('TGLBERLAKU', 'Tanggal Berlaku')                    
                    ));
                $process = array(
                    'Tambah ' . $type => array('ADDAJAX', site_url('referensi/' . $type), '0', '_content_'),
                    'Ubah ' . $type => array('EDITAJAX', site_url('referensi/' . $type), '1', '_content_'),
                    'Hapus ' . $type => array('DELETEAJAX', site_url('referensi/' . $type), 'N', '_content_'));
                $this->newtable->menu($process);
                break;
            case'izin':
                $SQL = "SELECT  KDIZIN AS 'Kode Izin',
                                URIZIN AS 'Uraian Izin',
                                KDIZIN
                        FROM    M_IZIN";
                $this->newtable->keys(array('KDIZIN'));
                $this->newtable->hiddens(array('KDIZIN'));
                $this->newtable->search(array(array('KDIZIN', 'Kode Izin'),
                    array('URIZIN', 'Uraian Izin')));
                $process = array(
                    'Tambah ' . $type => array('ADDAJAX', site_url('referensi/' . $type), '0', '_content_'),
                    'Ubah ' . $type => array('EDITAJAX', site_url('referensi/' . $type), '1', '_content_'),
                    'Hapus ' . $type => array('DELETEAJAX', site_url('referensi/' . $type), 'N', '_content_'));
                $this->newtable->menu($process);
                break;
            case'kemasan':
                $SQL = "SELECT  KODE_KEMASAN AS 'Kode Kemasan',
                                URAIAN_KEMASAN AS 'Uraian Kemasan',
                                KODE_KEMASAN,
                                URAIAN_KEMASAN
                        FROM    M_KEMASAN";
                $this->newtable->keys(array('KODE_KEMASAN'));
                $this->newtable->hiddens(array('KODE_KEMASAN', 'URAIAN_KEMASAN'));
                $this->newtable->search(array(array('KODE_KEMASAN', 'Kode Kemasan'),
                    array('URAIAN_KEMASAN', 'Uraian Kemasan')));
                $process = array(
                    'Tambah ' . $type => array('ADDAJAX', site_url('referensi/' . $type), '0', '_content_'),
                    'Ubah ' . $type => array('EDITAJAX', site_url('referensi/' . $type), '1', '_content_'),
                    'Hapus ' . $type => array('DELETEAJAX', site_url('referensi/' . $type), 'N', '_content_'));
                $this->newtable->menu($process);
                break;
            case'tambat':
                $SQL = "SELECT  KD_TAMBAT AS 'Kode Tambat',
                                NM_TAMBAT AS 'NAMA TAMBAT',
                                NM_PEMILIK AS 'NAMA PEMILIK',                               
                                KD_TAMBAT                                
                        FROM    M_TAMBAT";
                $this->newtable->keys(array('KD_TAMBAT'));
                $this->newtable->hiddens(array('KD_TAMBAT'));
                $this->newtable->search(array(array('KD_TAMBAT', 'Kode Tambat'),
                    array('NM_TAMBAT', 'Nama Tambat'),
                    array('NM_PEMILIK', 'Nama Pemilik')
                    ));
                $process = array(
                    'Tambah ' . $type => array('ADDAJAX', site_url('referensi/' . $type), '0', '_content_'),
                    'Ubah ' . $type => array('EDITAJAX', site_url('referensi/' . $type), '1', '_content_'),
                    'Hapus ' . $type => array('DELETEAJAX', site_url('referensi/' . $type), 'N', '_content_'));
                $this->newtable->menu($process);
                break;
            case'kpbc':
                $SQL = "SELECT  KDKPBC AS 'Kode KPBC',
                                URAIAN_KPBC AS 'Uraian KPBC',
                                LAST_NOREG AS 'Last No Reg',
                                ESELON AS 'Eselon',
                                KOTA AS 'Kota',
                                NPWP AS 'NPWP',
                                KDKPBC,
                                URAIAN_KPBC
                        FROM    m_kpbc";
                $this->newtable->keys(array('KDKPBC'));
                $this->newtable->hiddens(array('KDKPBC', 'URAIAN_KPBC'));
                $this->newtable->search(array(array('KDKPBC', 'Kode KPBC'),
                    array('URAIAN_KPBC', 'Uraian KPBC')));
                $process = array(
                    'Tambah ' . $type => array('ADDAJAX', site_url('referensi/' . $type), '0', '_content_'),
                    'Ubah ' . $type => array('EDITAJAX', site_url('referensi/' . $type), '1', '_content_'),
                    'Hapus ' . $type => array('DELETEAJAX', site_url('referensi/' . $type), 'N', '_content_'));
                $this->newtable->menu($process);
                break;
            case'kpker':
                $SQL = "SELECT  KDKPKER AS 'Kode KPKER',
                                URAIAN AS 'Uraian KPKER',
                                KWBC AS 'KWBC',
                                KDKPKER,
                                URAIAN
                        FROM    M_KPKER";
                $this->newtable->keys(array('KDKPKER'));
                $this->newtable->hiddens(array('KDKPKER', 'URAIAN'));
                $this->newtable->search(array(array('KDKPKER', 'Kode KPKER'),
                    array('URAIAN', 'Uraian KPKER')));
                $process = array(
                    'Tambah ' . $type => array('ADDAJAX', site_url('referensi/' . $type), '0', '_content_'),
                    'Ubah ' . $type => array('EDITAJAX', site_url('referensi/' . $type), '1', '_content_'),
                    'Hapus ' . $type => array('DELETEAJAX', site_url('referensi/' . $type), 'N', '_content_'));
                $this->newtable->menu($process);
                break;
            case'negara':
                $SQL = "SELECT  KODE_NEGARA AS 'Kode Negara',
                                URAIAN_NEGARA AS 'Uraian Negara',
                                KODE_NEGARA,
                                URAIAN_NEGARA
                        FROM    M_NEGARA";
                $this->newtable->keys(array('KODE_NEGARA'));
                $this->newtable->hiddens(array('KODE_NEGARA', 'URAIAN_NEGARA'));
                $this->newtable->search(array(array('KODE_NEGARA', 'Kode Negara'),
                    array('URAIAN_NEGARA', 'Uraian Negara')));
                $process = array(
                    'Tambah ' . $type => array('ADDAJAX', site_url('referensi/' . $type), '0', '_content_'),
                    'Ubah ' . $type => array('EDITAJAX', site_url('referensi/' . $type), '1', '_content_'),
                    'Hapus ' . $type => array('DELETEAJAX', site_url('referensi/' . $type), 'N', '_content_'));
                $this->newtable->menu($process);
                break;
            case'pelabuhan':
                $SQL = "SELECT  KODE_PELABUHAN AS 'Kode Pelabuhan' ,
                                URAIAN_PELABUHAN AS 'Uraian Pelabuhan',
                                KODE_NEGARA AS 'Kode Negara',
                                KODE_PELABUHAN,
                                URAIAN_PELABUHAN
                        FROM    M_PELABUHAN";
                $this->newtable->keys(array('KODE_PELABUHAN'));
                $this->newtable->hiddens(array('KODE_PELABUHAN', 'URAIAN_PELABUHAN'));
                $this->newtable->search(array(array('KODE_PELABUHAN', 'Kode Pelabuhan'),
                    array('URAIAN_PELABUHAN', 'Uraian Pelabuhan'),
                    array('NEG_PELABUHAN', 'Negara Pelabuhan')));
                $process = array(
                    'Tambah ' . $type => array('ADDAJAX', site_url('referensi/' . $type), '0', '_content_'),
                    'Ubah ' . $type => array('EDITAJAX', site_url('referensi/' . $type), '1', '_content_'),
                    'Hapus ' . $type => array('DELETEAJAX', site_url('referensi/' . $type), 'N', '_content_'));
                $this->newtable->menu($process);
                break;
            case'satuan':
                $SQL = "SELECT  KDEDI AS 'Kode Satuan',
                                UREDI AS 'Uraian Satuan',
                                KDEDI,
                                UREDI
                        FROM    M_SATUAN";
                $this->newtable->keys(array('KDEDI'));
                $this->newtable->hiddens(array('KDEDI', 'UREDI'));
                $this->newtable->search(array(array('KDEDI', 'Kode Satuan'),
                    array('UREDI', 'Uraian Satuan')));
                $process = array(
                    'Tambah ' . $type => array('ADDAJAX', site_url('referensi/' . $type), '0', '_content_'),
                    'Ubah ' . $type => array('EDITAJAX', site_url('referensi/' . $type), '1', '_content_'),
                    'Hapus ' . $type => array('DELETEAJAX', site_url('referensi/' . $type), 'N', '_content_'));
                $this->newtable->menu($process);
                break;
            case'setting':
                $SQL = "SELECT  MODUL AS 'Kode Modul',
                                VARIABLE AS 'Variabel',
                                VALUE AS 'Value',
                                MODUL,
                                VARIABLE
                        FROM    M_SETTING";
                $this->newtable->keys(array('MODUL', 'VARIABLE'));
                $this->newtable->hiddens(array('MODUL', 'VARIABLE'));
                $this->newtable->search(array(array('MODUL', 'Kode Modul'),
                    array('VARIABLE', 'Variabel')));
                $process = array(
                    'Tambah ' . $type => array('ADDAJAX', site_url('referensi/' . $type), '0', '_content_'),
                    'Ubah ' . $type => array('EDITAJAX', site_url('referensi/' . $type), '1', '_content_'),
                    'Hapus ' . $type => array('DELETEAJAX', site_url('referensi/' . $type), 'N', '_content_'));
                $this->newtable->menu($process);
                break;
            case'tambat':
                $SQL = "SELECT  KD_TAMBAT AS 'Kode KADE', "
                             . "NM_TAMBAT AS 'Nama KADE', "
                             . "NM_PEMILIK AS 'Pemilik', "
                             . "KD_TAMBAT "
                    . "FROM     M_TAMBAT";
                $this->newtable->keys(array('KD_TAMBAT'));
                $this->newtable->hiddens(array('KD_TAMBAT'));
                $this->newtable->search(array(array('KD_TAMBAT', 'Kode KADE'),array('NM_TAMBAT', 'Nama KADE'),array('NM_PEMILIK', 'Pemilik')));
                break;
            case'partner':
                $this->load->model('actMain');
                $SQL = "SELECT  a.KODE_TRADER,
                                a.KDKPBC,
                                a.NOMOREDI,
                                a.KDKPBC 'Kode KPBC',
                                b.URAIAN_KPBC AS 'Uraian KPBC',
                                a.NAMALOKAL AS 'Nama Local',
                                a.NOMOREDI AS 'Edi Number',
                                a.Keterangan
                        FROM    M_TRADER_PARTNER a left join M_KPBC b on b.KDKPBC = a.KDKPBC
                        WHERE   a.KODE_TRADER = '" . $this->kd_trader . "'";
                $this->newtable->keys(array('KODE_TRADER','KDKPBC','NOMOREDI'));
                $this->newtable->hiddens(array('KODE_TRADER','KDKPBC','NOMOREDI'));
                $this->newtable->detail(site_url('referensi/dataPendukung/partnerdetil'));
                $this->newtable->detail_tipe('detil_priview_bottom');
                $this->newtable->search(array(
                    array('a.KDKPBC', 'Kode KPBC'),
                    array('b.URAIAN_KPBC', 'Uraian KPBC'),
                    array('a.NAMALOKAL', 'Nama Local'),
                    array('a.NOMOREDI', 'Edi Number'),
                    array('a.Keterangan', 'Keterangan')));
                $process['Tambah '.$type]   =  array('ADDAJAX', site_url('referensi/'.$type), '0', '_content_');
                $process['Ubah ' . $type]   =  array('EDITAJAX', site_url('referensi/' . $type), '1', '_content_'); 
                $process['Hapus ' . $type]  =  array('DELETEAJAX', site_url('referensi/' . $type), 'N', '_content_'); 
                break;
            case'partnerdetil':
                $arrKey = explode('|', urldecode($key));
                $this->load->model('actMain');
                $SQL = "SELECT  a.KODE_TRADER, 
                                a.NOMOREDI, 
                                a.APRF,
                                a.KDKPBC,
                                d.URAIAN as 'Jns Dokumen',
                                c.URAIAN as 'Direction',
                                b.URAIAN as 'Status'
                        FROM    M_TRADER_PARTNER_DETIL a left join M_TABEL b on b.MODUL = 'PARTNER' AND b.KDTAB = 'STATUS' AND b.KDREC = a.STATUS
                                                         left join M_TABEL c on c.MODUL = 'PARTNER' AND c.KDTAB = 'DIRECTION' AND c.KDREC = a.DIRECTION
                                                         left join M_TABEL d on d.MODUL = 'PARTNER' AND d.KDTAB = 'APRF' AND d.KDREC = a.APRF
                        WHERE   a.KODE_TRADER = '" . $this->kd_trader . "' AND a.NOMOREDI = '".$arrKey[2]."'";
                $this->newtable->keys(array('KODE_TRADER','KDKPBC','NOMOREDI','APRF'));
                $this->newtable->hiddens(array('KODE_TRADER','KDKPBC','NOMOREDI'));
                $this->newtable->search(array(array('a.APRF', 'APRF'),
                                              array('d.URAIAN', 'Jns Dokumen'),
                                              array('c.URAIAN', 'Direction')));
                $process['Tambah '] =  array('ADDAJAX', site_url('referensi/'.$type.'/'.$key), '0', 'divpartnerdetil');
                //$process['Ubah ']   =  array('EDITAJAX', site_url('referensi/'.$type.'/'.$key), '1', 'divpartnerdetil');
                $process['Hapus ']  =  array('DELETEAJAX', site_url('referensi/'.$type.'/'.$key), 'N', 'filltd');
                break;
            default :
                return "Failed";
                exit();
        }
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->action(site_url('referensi/datapendukung/' . $type));
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->rowcount(10);
        $this->newtable->clear();
        $this->newtable->set_formid("f" . $type);
        $this->newtable->set_divid("div" . $type);
        if (!isset($process)) {
            $process['Ubah ' . $type]   =  array('EDITAJAX', site_url('referensi/' . $type), '1', '_content_');
            $process['Hapus ' . $type]  =  array('DELETEAJAX', site_url('referensi/' . $type), 'N', '_content_'); 
        }
        $this->newtable->menu($process);
        $tabel = $this->newtable->generate($SQL);
        $arrdata = array("tabel" => $tabel);
        if ($this->input->post("ajax"))
            return $tabel;
        else
            return $arrdata;
    }
    
    function getBarang($act, $hs = '', $seri = '', $brg = '') {
        $this->load->model('actMain');
        switch ($act) {
            case 'edit':
                if ($hs != '' && $seri != '' && $brg != '') {
                    $sql = "SELECT * 
                            FROM m_trader_barang a Left Join m_trader_tarif b ON a.KODE_TRADER = b.KODE_TRADER AND a.NOHS = b.NOHS AND a.SERITRP = b.SERITRP 
                            WHERE a.KODE_TRADER='" . $this->kd_trader . "' AND a.NOHS = '" . $hs . "' AND a.SERITRP = '" . $seri . "' AND a.BRGURAI = '" . $brg . "'";
                    $hasil['BARANG'] = $this->actMain->get_result($sql);
                }
                else {
                    $hasil = 'error';
                }
                break;
            case'add':
                break;
        }
        return $hasil;
    }

    function setBarang($act) {
        $this->load->model('actMain');
        switch ($act) {
            case'update':
                $barang = $this->input->post('BARANG');
                $barang['NOHS'] = str_replace('.', '', $barang['NOHS']);
                $exec = $this->actMain->insertRefernce('m_trader_barang', $barang);
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/barang') . '|Update data barang berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/barang') . '|Update data gagal berhasil';
                }
                break;
            case'delete':
                $where = $this->input->post('tb_chkfbarang');
                $this->db->where_in("CONCAT(KODE_TRADER,'|',NOHS,'|',SERITRP,'|',BRGURAI)", $where);
                $exec = $this->db->delete('m_trader_barang');
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/barang') . '|Delete data barang berhasil';
                }
                else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/barang') . '|Delete data barang gagal';
                }
                break;
        }
        return $hasil;
    }

    function getImportir($act, $kode = '', $impid = '', $npwp = '') {
        $this->load->model('actMain');
        switch ($act) {
            case 'edit':
                if ($kode != '' && $impid != '' && $npwp != '') {
                    $sql = "SELECT * 
                            FROM m_trader_importir where impnpwp = '" . $npwp . "'";
                    $hasil['IMPORTIR'] = $this->actMain->get_result($sql);
                    $hasil['JENIS_IDENTITAS'] = $this->actMain->get_mtabel('JENIS_IDENTITAS', 1, FALSE);
                    $hasil['JENIS_API'] = $this->actMain->get_mtabel('JENIS_API', 1, FALSE);
                } else {
                    $hasil = 'error';
                }
                break;
            case'add':

                break;
        }
        return $hasil;
    }

    function setImportir($act) {
        $this->load->model('actMain');
        switch ($act) {
            case'update':
                $importir = $this->input->post('IMPORTIR');
                $importir['IMPNPWP'] = str_replace('.', '', str_replace('-', '', $importir['IMPNPWP']));
                $importir['IMPSTATUS'] = implode(',', $importir['IMPSTATUS']);
                $exec = $this->actMain->insertRefernce('m_trader_importir', $importir);
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/importir') . '|Update data importir berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/importir') . '|Update data gagal';
                }
                break;
            case'delete':
                $where = $this->input->post('tb_chkfimportir');
                $this->db->where_in("CONCAT(KODE_TRADER,'|',IMPID,'|',IMPNPWP)", $where);
                $exec = $this->db->delete('m_trader_importir');
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/importir') . '|Delete data importir berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/importir') . '|Delete data importir gagal';
                }
                break;
        }
        return $hasil;
    }

    function getIndentor($act, $kode = '', $indid = '', $npwp = '') {
        $this->load->model('actMain');
        switch ($act) {
            case 'edit':
                if ($kode != '' && $indid != '' && $npwp != '') {
                    $sql = "SELECT * 
                            FROM m_trader_indentor where indnpwp = '" . $npwp . "'";
                    $hasil['INDENTOR'] = $this->actMain->get_result($sql);
                } else {
                    $hasil = 'error';
                }
                break;
            case'add':

                break;
        }
        return $hasil;
    }

    function setIndentor($act) {
        $this->load->model('actMain');
        switch ($act) {
            case'update':
                $indentor = $this->input->post('INDENTOR');
                $indentor['INDNPWP'] = str_replace('.', '', str_replace('-', '', $indentor['INDNPWP']));
                $exec = $this->actMain->insertRefernce('m_trader_indentor', $indentor);
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/indentor') . '|Update data indentor berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/indentor') . '|Update data gagal';
                }
                break;
            case'delete':
                $where = $this->input->post('tb_chkfindentor');
                $this->db->where_in("CONCAT(KODE_TRADER,'|',INDID,'|',INDNPWP)", $where);
                $exec = $this->db->delete('m_trader_indentor');
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/indentor') . '|Delete data indentor berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/indentor') . '|Delete data indentor gagal';
                }
                break;
        }
        return $hasil;
    }

    function getPemasok($act, $kode = '', $nama = '') {
        $this->load->model('actMain');
        switch ($act) {
            case 'edit':
                if ($kode != '' && $nama != '') {
                    $sql = "SELECT * 
                            FROM m_trader_supplier where kode_trader = '" . $kode . "' and pasoknama = '" . $nama . "'";
                    $hasil['PEMASOK'] = $this->actMain->get_result($sql);
                } else {
                    $hasil = 'error';
                }
                break;
            case'add':

                break;
        }
        return $hasil;
    }

    function setPemasok($act) {
        $this->load->model('actMain');
        switch ($act) {
            case'insert':
            case'update':
                $pemasok = $this->input->post('PEMASOK');
                $exec = $this->actMain->insertRefernce('m_trader_supplier', $pemasok);
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/pemasok') . '|Update data Pemasok berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/pemasok') . '|Update data gagal ';
                }
                break;
            case'delete':
                $where = $this->input->post('tb_chkfpemasok');
                $this->db->where_in("CONCAT(KODE_TRADER,'|',PASOKNAMA)", $where);
                $exec = $this->db->delete('m_trader_supplier');
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/pemasok') . '|Delete data Pemasok berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/pemasok') . '|Delete data supplier gagal';
                }
                break;
        }
        return $hasil;
    }

    function getTarif($act, $kode = '', $nohs = '', $seritrp = '') {
        $this->load->model('actMain');
        switch ($act) {
            case 'edit':
                if ($kode != '' && $nohs != '' && $seritrp != '') {
                    $sql = "SELECT * 
                            FROM m_trader_tarif where kode_trader = '" . $kode . "' and nohs = '" . $nohs . "' and seritrp = '" . $seritrp . "'";
                    $hasil['TARIF'] = $this->actMain->get_result($sql);
                } else {
                    $hasil = 'error';
                }
                break;
            case'add':

                break;
        }
        return $hasil;
    }

    function setTarif($act) {
        $this->load->model('actMain');
        switch ($act) {
            case'update':
                $tarif = $this->input->post('TARIF');
                $tarif['NOHS'] = str_replace('.', '', $tarif['NOHS']);
                $exec = $this->actMain->insertRefernce('m_trader_tarif', $tarif);
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/tarif') . '|Update data tarif berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/tarif') . '|Update data tarif gagal ';
                }
                break;
            case'delete':
                $where = $this->input->post('tb_chkftarif');
                $this->db->where_in("CONCAT(KODE_TRADER,'|',NOHS,'|',SERITRP)", $where);
                $exec = $this->db->delete('m_trader_tarif');
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/tarif') . '|Delete data tarif berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/tarif') . '|Delete data tarif gagal';
                }
                break;
        }
        return $hasil;
    }

    function getModa($act, $kode = '', $moda = '', $nama = '') {
        $this->load->model('actMain');
        switch ($act) {
            case 'edit':
                if ($kode != '' && $moda != '' && $nama != '') {
                    $sql = "SELECT * 
                            FROM m_trader_kapal where kode_trader = '" . $kode . "' and moda = '" . $moda . "' and angkutnama = '" . $nama . "'";
                    $hasil['MODA'] = $this->actMain->get_result($sql);
                } else {
                    $hasil = 'error';
                }
                break;
            case'add':

                break;
        }
        return $hasil;
    }

    function setModa($act) {
        $this->load->model('actMain');
        switch ($act) {
            case'update':
                $moda = $this->input->post('MODA');
                $exec = $this->actMain->insertRefernce('m_trader_kapal', $moda);
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/moda') . '|Update data moda berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/moda') . '|Update data moda gagal ';
                }
                break;
            case'delete':
                $where = $this->input->post('tb_chkfmoda');
                $this->db->where_in("CONCAT(KODE_TRADER,'|',MODA,'|',ANGKUTNAMA)", $where);
                $exec = $this->db->delete('m_trader_kapal');
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/moda') . '|Delete data moda berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/moda') . '|Delete data moda gagal';
                }
                break;
        }
        return $hasil;
    }

    function getBank($act, $kode = '', $nama = '') {
        $this->load->model('actMain');
        switch ($act) {
            case 'edit':
                if ($kode != '' && $nama != '') {
                    $sql = "SELECT * 
                           FROM m_bank where 
						   KDBANK = '" . $kode . "' and NMBANK = '" . $nama . "'";
                    $hasil['BANK'] = $this->actMain->get_result($sql);
                } else {
                    $hasil = 'error';
                }
                break;
            case'add':

                break;
        }
        return $hasil;
    }

    function setBank($act) {
        $this->load->model('actMain');
        switch ($act) {
            case'save':
                $bank = $this->input->post('BANK');
                $id = $bank['KDBANK'];
                $sql = "SELECT count(*) JML FROM m_bank where KDBANK = '" . $id . "'";
                $hasil['JML'] = $this->actMain->get_uraian($sql, 'JML');
                if ($hasil['JML'] > 0) {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/bank') . '|Insert data bank gagal. NO ID Bank [' . $id . '] sudah ada';
                } else {
                    $exec = $this->actMain->insertRefernce('m_bank', $bank);
                    if ($exec) {
                        $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/bank') . '|Insert data bank berhasil';
                    } else {
                        $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/bank') . '|Insert data bank gagal ';
                    }
                }
                break;
            case'update':
                $bank = $this->input->post('BANK');
                //$tarif['NOHS']= str_replace('.','',$tarif['NOHS']);
                $exec = $this->actMain->insertRefernce('m_bank', $bank);
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/bank') . '|Update data bank berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/bank') . '|Update data bank gagal ';
                }
                break;
            case'delete':
                $where = $this->input->post('tb_chkfbank');
                $this->db->where_in("CONCAT(KDBANK,'|',NMBANK)", $where);
                $exec = $this->db->delete('m_bank');
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/bank') . '|Delete data bank berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/bank') . '|Delete data bank gagal';
                }
                break;
        }
        return $hasil;
    }

    function getDaerah($act, $kode = '') {
        $this->load->model('actMain');
        switch ($act) {
            case 'edit':
                if ($kode != '') {
                    $sql = "SELECT * 
                            FROM m_daerah where KDDAERAH = '" . $kode . "'";
                    $hasil['DAERAH'] = $this->actMain->get_result($sql);
                } else {
                    $hasil = 'error';
                }
                break;
            case'add':

                break;
        }
        return $hasil;
    }

    function setDaerah($act) {
        $this->load->model('actMain');
        switch ($act) {
            case'save':
                $daerah = $this->input->post('DAERAH');
                $id = $daerah['KDDAERAH'];
                $sql = "SELECT count(*) JML FROM m_daerah where KDDAERAH = '" . $id . "'";
                $hasil['JML'] = $this->actMain->get_uraian($sql, 'JML');
                if ($hasil['JML'] > 0) {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/daerah') . '|Insert data daerah gagal. NO ID DAERAH [' . $id . '] sudah ada';
                } else {
                    $exec = $this->actMain->insertRefernce('m_daerah', $daerah);
                    if ($exec) {
                        $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/daerah') . '|Insert data daerah berhasil';
                    } else {
                        $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/daerah') . '|Insert data daerah gagal ';
                    }
                }
                break;
            case'update':
                $daerah = $this->input->post('DAERAH');
                //$tarif['NOHS']= str_replace('.','',$tarif['NOHS']);
                $exec = $this->actMain->insertRefernce('m_daerah', $daerah);
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/daerah') . '|Update data daerah berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/daerah') . '|Update data daerah gagal ';
                }
                break;
            case'delete':
                $where = $this->input->post('tb_chkfdaerah');
                $this->db->where_in("KDDAERAH", $where);
                $exec = $this->db->delete('m_daerah');
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/daerah') . '|Delete data daerah berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/daerah') . '|Delete data daerah gagal';
                }
                break;
        }
        return $hasil;
    }

    function getGudang($act, $kode = '', $gudang = '') {
        $this->load->model('actMain');
        switch ($act) {
            case 'edit':
                if ($kode != '' && $gudang != '') {
                    $sql = "SELECT * 
                            FROM m_gudang where KDKPBC = '" . $kode . "' AND KDGDG = '" . $gudang . "'";
                    $hasil['GUDANG'] = $this->actMain->get_result($sql);
                } else {
                    $hasil = 'error';
                }
                break;
            case'add':

                break;
        }
        return $hasil;
    }

    function setGudang($act) {
        $this->load->model('actMain');
        switch ($act) {
            case'save':
                $gudang = $this->input->post('GUDANG');
                $id = $gudang['KDKPBC'];
                $kode = $gudang['KDGDG'];
                $sql = "SELECT count(*) JML FROM m_gudang where KDKPBC = '" . $id . "' AND KDGDG = '" . $kode . "'";
                $hasil['JML'] = $this->actMain->get_uraian($sql, 'JML');
                if ($hasil['JML'] > 0) {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/gudang') . '|Insert data gudang gagal. KODE KPBC [' . $id . ']  dan KODE GUDANG [' . $kode . '] sudah ada';
                } else {
                    $exec = $this->actMain->insertRefernce('m_gudang', $gudang);
                    if ($exec) {
                        $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/gudang') . '|Insert data gudang berhasil';
                    } else {
                        $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/gudang') . '|Insert data gudang gagal ';
                    }
                }
                break;
            case'update':
                $gudang = $this->input->post('GUDANG');
                $exec = $this->actMain->insertRefernce('m_gudang', $gudang);
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/gudang') . '|Update data gudang berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/gudang') . '|Update data gudang gagal ';
                }
                break;
            case'delete':
                $where = $this->input->post('tb_chkfgudang');
                $this->db->where_in("CONCAT(KDKPBC,'|',KDGDG)", $where);
                $exec = $this->db->delete('m_gudang');
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/gudang') . '|Delete data gudang berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/gudang') . '|Delete data gudang gagal';
                }
                break;
        }
        return $hasil;
    }

    function getHanggar($act, $kode = '', $hanggar = '') {
        $this->load->model('actMain');
        switch ($act) {
            case 'edit':
                if ($kode != '' && $hanggar != '') {
                    $sql = "SELECT * 
                            FROM m_hanggar where KDKPBC = '" . $kode . "' AND KDHGR = '" . $hanggar . "'";
                    $hasil['HANGGAR'] = $this->actMain->get_result($sql);
                } else {
                    $hasil = 'error';
                }
                break;
            case'add':

                break;
        }
        return $hasil;
    }

    function setHanggar($act) {
        $this->load->model('actMain');
        switch ($act) {
            case'save':
                $hanggar = $this->input->post('HANGGAR');
                $id = $hanggar['KDKPBC'];
                $kode = $hanggar['KDHGR'];
                $sql = "SELECT count(*) JML FROM m_hanggar where KDKPBC = '" . $id . "' AND KDHGR = '" . $kode . "'";
                $hasil['JML'] = $this->actMain->get_uraian($sql, 'JML');
                if ($hasil['JML'] > 0) {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/hanggar') . '|Insert data hanggar gagal. KODE KPBC [' . $id . ']  dan KODE HANGGAR [' . $kode . '] sudah ada';
                } else {
                    $exec = $this->actMain->insertRefernce('m_hanggar', $hanggar);
                    if ($exec) {
                        $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/hanggar') . '|Insert data hanggar berhasil';
                    } else {
                        $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/hanggar') . '|Insert data hanggar gagal ';
                    }
                }
                break;
            case'update':
                $hanggar = $this->input->post('HANGGAR');
                $exec = $this->actMain->insertRefernce('m_hanggar', $hanggar);
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/hanggar') . '|Update data hanggar berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/hanggar') . '|Update data hanggar gagal ';
                }
                break;
            case'delete':
                $where = $this->input->post('tb_chkfhanggar');
                $this->db->where_in("CONCAT(KDKPBC,'|',KDHGR)", $where);
                $exec = $this->db->delete('m_hanggar');
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/hanggar') . '|Delete data hanggar berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/hanggar') . '|Delete data hanggar gagal';
                }
                break;
        }
        return $hasil;
    }

    function getIzin($act, $kode = '') {
        $this->load->model('actMain');
        switch ($act) {
            case 'edit':
                if ($kode != '') {
                    $sql = "SELECT * FROM m_izin where KDIZIN = '" . $kode . "'";
                    $hasil['IZIN'] = $this->actMain->get_result($sql);
                } else {
                    $hasil = 'error';
                }
                break;
            case'add':

                break;
        }
        return $hasil;
    }

    function setIzin($act) {
        $this->load->model('actMain');
        switch ($act) {
            case'save':
                $izin = $this->input->post('IZIN');
                $id = $izin['KDIZIN'];
                $sql = "SELECT count(*) JML FROM m_izin where KDIZIN = '" . $id . "'";
                $hasil['JML'] = $this->actMain->get_uraian($sql, 'JML');
                if ($hasil['JML'] > 0) {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/izin') . '|Insert data izin gagal. IZIN ' . $urizin . ' sudah ada';
                } else {
                    $exec = $this->actMain->insertRefernce('m_izin', $izin);
                    if ($exec) {
                        $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/izin') . '|Insert data izin berhasil';
                    } else {
                        $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/izin') . '|Insert data izin gagal ';
                    }
                }
                break;
            case'update':
                $izin = $this->input->post('IZIN');
                $exec = $this->actMain->insertRefernce('m_izin', $izin);
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/izin') . '|Update data izin berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/izin') . '|Update data izin gagal ';
                }
                break;
            case'delete':
                $where = $this->input->post('tb_chkfizin');
                $this->db->where_in("KDIZIN", $where);
                $exec = $this->db->delete('m_izin');
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/izin') . '|Delete data izin berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/izin') . '|Delete data izin gagal';
                }
                break;
        }
        return $hasil;
    }

    function getKemasan($act, $kode = '') {
        $this->load->model('actMain');
        switch ($act) {
            case 'edit':
                if ($kode != '') {
                    $sql = "SELECT * 
                            FROM m_kemasan where KODE_KEMASAN = '" . $kode . "'";
                    $hasil['KEMASAN'] = $this->actMain->get_result($sql);
                } else {
                    $hasil = 'error';
                }
                break;
            case'add':

                break;
        }
        return $hasil;
    }

    function setKemasan($act) {
        $this->load->model('actMain');
        switch ($act) {
            case'save':
                $kemasan = $this->input->post('KEMASAN');
                $id = $KEMASAN['KODE_KEMASAN'];
                $sql = "SELECT count(*) JML FROM m_kemasan where KODE_KEMASAN = '" . $id . "'";
                $hasil['JML'] = $this->actMain->get_uraian($sql, 'JML');
                if ($hasil['JML'] > 0) {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/kemasan') . '|Insert data kemasan gagal. KODE KEMASAN [' . $id . '] sudah ada';
                } else {
                    $exec = $this->actMain->insertRefernce('m_kemasan', $kemasan);
                    if ($exec) {
                        $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/kemasan') . '|Insert data kemasan berhasil';
                    } else {
                        $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/kemasan') . '|Insert data kemasan gagal ';
                    }
                }
                break;
            case'update':
                $kemasan = $this->input->post('KEMASAN');
                $exec = $this->actMain->insertRefernce('m_kemasan', $kemasan);
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/kemasan') . '|Update data kemasan berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/kemasan') . '|Update data kemasan gagal ';
                }
                break;
            case'delete':
                $where = $this->input->post('tb_chkfkemasan');
                $this->db->where_in("KODE_KEMASAN", $where);
                $exec = $this->db->delete('m_kemasan');
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/kemasan') . '|Delete data kemasan berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/kemasan') . '|Delete data kemasan gagal';
                }
                break;
        }
        return $hasil;
    }

    function getKpbc($act, $kode = '') {
        $this->load->model('actMain');
        switch ($act) {
            case 'edit':
                if ($kode != '') {
                    $sql = "SELECT * 
                            FROM m_kpbc where KDKPBC = '" . $kode . "'";
                    $hasil['KPBC'] = $this->actMain->get_result($sql);
                    $hasil['KOTA'] = $this->actMain->get_mtabel('JENIS_IDENTITAS', 1, FALSE);
                } else {
                    $hasil = 'error';
                }
                break;
            case'add':

                break;
        }
        return $hasil;
    }

    function setKpbc($act) {
        $this->load->model('actMain');
        switch ($act) {
            case'save':
                $kpbc = $this->input->post('KPBC');
                $id = $KPBC['KDKPBC'];
                $sql = "SELECT count(*) JML FROM m_kpbc where KDKPBC = '" . $id . "'";
                $hasil['JML'] = $this->actMain->get_uraian($sql, 'JML');
                if ($hasil['JML'] > 0) {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/kpbc') . '|Insert data kpbc gagal. KODE KPBC [' . $id . '] sudah ada';
                } else {
                    $exec = $this->actMain->insertRefernce('m_kpbc', $kpbc);
                    if ($exec) {
                        $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/kpbc') . '|Insert data kpbc berhasil';
                    } else {
                        $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/kpbc') . '|Insert data kpbc gagal ';
                    }
                }
                break;
            case'update':
                $kpbc = $this->input->post('KPBC');
                $exec = $this->actMain->insertRefernce('m_kpbc', $kpbc);
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/kpbc') . '|Update data kpbc berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/kpbc') . '|Update data kpbc gagal ';
                }
                break;
            case'delete':
                $where = $this->input->post('tb_chkfkpbc');
                $this->db->where_in("KDKPBC", $where);
                $exec = $this->db->delete('m_kpbc');
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/kpbc') . '|Delete data kpbc berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/kpbc') . '|Delete data kpbc gagal';
                }
                break;
        }
        return $hasil;
    }

    function getKpker($act, $kode = '') {
        $this->load->model('actMain');
        switch ($act) {
            case 'edit':
                if ($kode != '') {
                    $sql = "SELECT * 
                            FROM m_kpker where KDKPKER = '" . $kode . "'";
                    $hasil['KPKER'] = $this->actMain->get_result($sql);
                } else {
                    $hasil = 'error';
                }
                break;
            case'add':

                break;
        }
        return $hasil;
    }

    function setKpker($act) {
        //echo $act; die();
        $this->load->model('actMain');
        switch ($act) {
            case'save':
                $kpker = $this->input->post('KPKER');
                $id = $KPKER['KDKPKER'];
//              die($id);
                $sql = "SELECT count(*) JML FROM m_kpker where KDKPKER = '" . $id . "'";
                $hasil['JML'] = $this->actMain->get_uraian($sql, 'JML');
//              die($hasil['JML']);
                if ($hasil['JML'] > 0) {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/kpker') . '|Insert data kpker gagal. KODE KPKER [' . $id . '] sudah ada';
                } else {
                    $exec = $this->actMain->insertRefernce('m_kpker', $kpker);
                    if ($exec) {
                        $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/kpker') . '|Insert data kpker berhasil';
                    } else {
                        $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/kpker') . '|Insert data kpker gagal ';
                    }
                }
                break;
            case'update':
                $kpker = $this->input->post('KPKER');
                $exec = $this->actMain->insertRefernce('m_kpker', $kpker);
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/kpker') . '|Update data kpker berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/kpker') . '|Update data kpker gagal ';
                }
                break;
            case'delete':
                $where = $this->input->post('tb_chkfkpker');
                $this->db->where_in("KDKPKER", $where);
                $exec = $this->db->delete('m_kpker');
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/kpker') . '|Delete data kpker berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/kpker') . '|Delete data kpker gagal';
                }
                break;
        }
        return $hasil;
    }

    function getNegara($act, $kode = '') {
        $this->load->model('actMain');
        switch ($act) {
            case 'edit':
                if ($kode != '') {
                    echo $sql = "SELECT * 
                            FROM m_negara where KODE_NEGARA = '" . $kode . "'";
                    $hasil['NEGARA'] = $this->actMain->get_result($sql);
                } else {
                    $hasil = 'error';
                }
                break;
            case'add':

                break;
        }
        return $hasil;
    }

    function setNegara($act) {
        $this->load->model('actMain');
        switch ($act) {
            case'save':
                $NEGARA = $this->input->post('NEGARA');
                $id = $NEGARA['KODE_NEGARA'];
                $sql = "SELECT count(*) JML FROM m_negara where KODE_NEGARA = '" . $id . "'";
                $hasil['JML'] = $this->actMain->get_uraian($sql, 'JML');
                if ($hasil['JML'] > 0) {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/negara') . '|Insert data negara gagal. KODE NEGARA [' . $id . '] sudah ada';
                } else {
                    $exec = $this->actMain->insertRefernce('m_negara', $NEGARA);
                    if ($exec) {
                        $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/negara') . '|Insert data negara berhasil';
                    } else {
                        $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/negara') . '|Insert data negara gagal ';
                    }
                }
                break;
            case'update':
                $negara = $this->input->post('NEGARA');
                $exec = $this->actMain->insertRefernce('m_negara', $negara);
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/negara') . '|Update data negara berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/negara') . '|Update data negara gagal ';
                }
                break;
            case'delete':
                $where = $this->input->post('tb_chkfnegara');
                $this->db->where_in("KODE_NEGARA", $where);
                $exec = $this->db->delete('m_negara');
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/negara') . '|Delete data negara berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/negara') . '|Delete data negara gagal';
                }
                break;
        }
        return $hasil;
    }

    function getPelabuhan($act, $kode = '') {
        $this->load->model('actMain');
        switch ($act) {
            case 'edit':
                if ($kode != '') {
                    $sql = "SELECT * 
                            FROM m_pelabuhan where KODE_PELABUHAN = '" . $kode . "'";
                    $hasil['PELABUHAN'] = $this->actMain->get_result($sql);
                } else {
                    $hasil = 'error';
                }
                break;
            case'add':

                break;
        }
        return $hasil;
    }

    function setPelabuhan($act) {
        $this->load->model('actMain');
        switch ($act) {
            case'save':
                $pelabuhan = $this->input->post('PELABUHAN');
                $id = $pelabuhan['KODE_PELABUHAN'];
                $sql = "SELECT count(*) JML FROM m_pelabuhan where KODE_PELABUHAN = '" . $id . "'";
                $hasil['JML'] = $this->actMain->get_uraian($sql, 'JML');
                if ($hasil['JML'] > 0) {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/pelabuhan') . '|Insert data pelabuhan gagal. KODE PELABUHAN [' . $id . '] sudah ada';
                } else {
                    $exec = $this->actMain->insertRefernce('m_pelabuhan', $pelabuhan);
                    if ($exec) {
                        $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/pelabuhan') . '|Insert data pelabuhan berhasil';
                    } else {
                        $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/pelabuhan') . '|Insert data pelabuhan gagal ';
                    }
                }
                break;
            case'update':
                $pelabuhan = $this->input->post('PELABUHAN');
                $exec = $this->actMain->insertRefernce('m_pelabuhan', $pelabuhan);
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/pelabuhan') . '|Update data pelabuhan berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/pelabuhan') . '|Update data pelabuhan gagal ';
                }
                break;
            case'delete':
                $where = $this->input->post('tb_chkfpelabuhan');
                $this->db->where_in("KODE_PELABUHAN", $where);
                $exec = $this->db->delete('m_pelabuhan');
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/pelabuhan') . '|Delete data pelabuhan berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/pelabuhan') . '|Delete data pelabuhan gagal';
                }
                break;
        }
        return $hasil;
    }

    function getSatuan($act, $kode = '') {
        $this->load->model('actMain');
        switch ($act) {
            case 'edit':
                if ($kode != '') {
                    $sql = "SELECT * 
                            FROM m_satuan where KDEDI = '" . $kode . "'";
                    $hasil['SATUAN'] = $this->actMain->get_result($sql);
                } else {
                    $hasil = 'error';
                }
                break;
            case'add':

                break;
        }
        return $hasil;
    }

    function setSatuan($act) {
        $this->load->model('actMain');
        switch ($act) {
            case'save':
                $satuan = $this->input->post('SATUAN');
                $id = $satuan['KDEDI'];
                $sql = "SELECT count(*) JML FROM m_satuan where KDEDI = '" . $id . "'";
                $hasil['JML'] = $this->actMain->get_uraian($sql, 'JML');
                if ($hasil['JML'] > 0) {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/satuan') . '|Insert data satuan gagal. KODE EDI [' . $id . '] sudah ada';
                } else {
                    $exec = $this->actMain->insertRefernce('m_satuan', $satuan);
                    if ($exec) {
                        $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/satuan') . '|Insert data satuan berhasil';
                    } else {
                        $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/satuan') . '|Insert data satuan gagal ';
                    }
                }
                break;
            case'update':
                $satuan = $this->input->post('SATUAN');
                $exec = $this->actMain->insertRefernce('m_satuan', $satuan);
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/satuan') . '|Update data satuan berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/satuan') . '|Update data satuan gagal ';
                }
                break;
            case'delete':
                $where = $this->input->post('tb_chkfsatuan');
                $this->db->where_in("KDEDI", $where);
                $exec = $this->db->delete('m_satuan');
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/satuan') . '|Delete data satuan berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/satuan') . '|Delete data satuan gagal';
                }
                break;
        }
        return $hasil;
    }

    function getSetting($act, $kode = '', $var = '') {
        $this->load->model('actMain');
        switch ($act) {
            case 'edit':
                if ($kode != '') {
                    $sql = "SELECT * 
                            FROM m_setting where MODUL = '" . $kode . "' AND VARIABLE = '" . $var . "'";
                    $hasil['SETTING'] = $this->actMain->get_result($sql);
                } else {
                    $hasil = 'error';
                }
                break;
            case'add':

                break;
        }
        return $hasil;
    }

    function setSetting($act) {
        $this->load->model('actMain');
        switch ($act) {
            case'save':
                $setting = $this->input->post('SETTING');
                $id = $setting['MODUL'];
                $var = $setting['VARIABLE'];
                $sql = "SELECT count(*) JML FROM m_setting where MODUL = '" . $id . "' AND VARIABLE = '" . $var . "'";
                $hasil['JML'] = $this->actMain->get_uraian($sql, 'JML');
                if ($hasil['JML'] > 0) {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/setting') . '|Insert data setting gagal. MODUL [' . $id . '] sudah ada';
                } else {
                    $exec = $this->actMain->insertRefernce('m_setting', $setting);
                    if ($exec) {
                        $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/setting') . '|Insert data setting berhasil';
                    } else {
                        $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/setting') . '|Insert data setting gagal ';
                    }
                }
                break;
            case'update':
                $setting = $this->input->post('SETTING');
                $exec = $this->actMain->insertRefernce('m_setting', $setting);
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/setting') . '|Update data setting berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/setting') . '|Update data setting gagal ';
                }
                break;
            case'delete':
                $where = $this->input->post('tb_chkfsetting');
                $this->db->where_in("CONCAT(MODUL,'|',VARIABLE)", $where);
                $exec = $this->db->delete('m_setting');
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/setting') . '|Delete data setting berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/setting') . '|Delete data setting gagal';
                }
                break;
        }
        return $hasil;
    }
    
    function getPartner($act, $kode_trader = '', $kdkpbc = '', $edinumber = '') {
        $this->load->model('actMain');
        switch ($act) {
            case 'edit':
                if ($kode_trader != '' && $kdkpbc != '' && $edinumber != '') {
                    $sql = "SELECT a.* ,b.URAIAN_KPBC as 'URKDKPBC' ".
                           "FROM m_trader_partner a INNER JOIN m_kpbc b on a.KDKPBC = b.KDKPBC ".
                           "WHERE a.KODE_TRADER = '".$kode_trader."' AND " .
                                 "a.KDKPBC      = '".$kdkpbc."' AND " . 
                                 "a.NOMOREDI    = '".$edinumber."'";
                    $hasil['PARTNER'] = $this->actMain->get_result($sql);
                    $hasil['act']     = 'update';
                } 
                else {
                    $hasil = 'error';
                }
                break;
            case'add':
                $hasil['act']     = 'insert';
                break;
        }
        
        return $hasil;
    }

    function setPartner($act) {
        $this->load->model('actMain');
        switch ($act) {
            case'insert':
            case'update':
                $partner = $this->input->post('PARTNER');
                $partner['KODE_TRADER'] = $this->kd_trader;
                $exec = $this->actMain->insertRefernce('m_trader_partner', $partner);
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/partner') . '|Update data partner berhasil';
                }
                else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/partner') . '|Update data partner gagal';
                }
                break;
            case'delete':
                $where = $this->input->post('tb_chkfpartner');
                $whrTBL = $this->db->where_in("CONCAT(KODE_TRADER,'|',KDKPBC,'|',NOMOREDI)", $where);
                $datCEK = $this->db->get_where('m_trader_partner_detil', $whrTBL);
                if($datCEK->num_rows()<=0){
                    $this->db->where_in("CONCAT(KODE_TRADER,'|',KDKPBC,'|',NOMOREDI)", $where);
                    $exec = $this->db->delete('m_trader_partner');
                    if ($exec) {
                        $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/partner') . '|Delete data partner berhasil';
                    } 
                    else {
                        $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/partner') . '|Delete data partner gagal';
                    }
                }
                else{
                    $hasil = 'Hapus terlebih dahulu detil dokumen / APRF.';
                }
                break;
        }
        return $hasil;
    }
    
    function getPartnerDetil($act, $kdkpbc='', $edinumber='', $aprf=''){
        $this->load->model('actMain');
        switch ($act) {
            case 'edit':
                if ($kdkpbc!='' && $edinumber!='' && $aprf!='') {
                    $sql = "SELECT a.*,b.URAIAN_KPBC "
                         . "FROM M_TRADER_PARTNER_DETIL a LEFT JOIN M_KPBC b ON a.KDKPBC = b.KDKPBC "
                         . "WHERE a.KODE_TRADER= '".$this->kd_trader ."' AND "
                         .        "a.KDKPBC     = '".$kdkpbc."' AND "
                         .        "a.NOMOREDI   = '".$edinumber."' AND "
                         .        "a.APRF       = '".$aprf."'";
                    $hasil['PARTNERDETIL'] = $this->actMain->get_result($sql);
                    $hasil['APRF']         = $this->actMain->get_mtabel('APRF', 1, FALSE,'','KODEDANUR','PARTNER');
                    $hasil['act']          = $act;
                }
                else {
                    $hasil = 'error getPartnerDetil data parameter tidak lengkap.';
                }
                break;
            case'add':
                $hasil['act']              = 'insert';
                break;
        }
        return $hasil;
    }
    
    function getUrKpbc($kode=''){
        $this->load->model('actMain');
        $sql = "SELECT KDKPBC,URAIAN_KPBC FROM m_kpbc where KDKPBC = '" . $kode . "'";
        $hasil['PARTNERDETIL'] = $this->actMain->get_result($sql);
        $hasil['APRF'] = $this->actMain->get_mtabel('APRF', 1, FALSE,'','KODEDANUR','PARTNER');
        return $hasil;
    }
    
    function setPartnerDetil($act,$key=''){
        switch ($act) {
            case 'update':
            case 'insert':
                $this->load->model('actMain');
                $key = $this->input->post('key');
                $data = $this->input->post('PARTNERDETIL');
                $exec = $this->actMain->insertRefernce('m_trader_partner_detil', $data);
                if ($exec)
                    $hasil = 'MSG|OK|'.site_url('referensi/dataPendukung/partnerdetil/'.str_replace('|','`',$key)).'|Proses data Dokumen Partner berhasil';
                else 
                    $hasil = 'MSG|ER|'.site_url('referensi/dataPendukung/partnerdetil/'.str_replace('|','`',$key)).'|Proses data Dokumen Partner gagal';
                break;
            case 'delete':
                $where = $this->input->post('tb_chkfpartnerdetil');
                $this->db->where_in("CONCAT(KODE_TRADER,'|',KDKPBC,'|',NOMOREDI,'|',APRF)", $where);
                $exec = $this->db->delete('m_trader_partner_detil');
                if ($exec)
                    $hasil = 'MSG|OK|'.site_url('referensi/dataPendukung/partnerdetil/'.str_replace('|','`',$key)).'|Delete data Dokumen Partner berhasil';
                else 
                    $hasil = 'Delete data Dokumen Partner gagal';
                break;
        }
        return $hasil;
    }
    
    function getTod($act, $kode_trader ='', $jnsmoda='', $region='') {
        $this->load->model('actMain');
        switch ($act) {
            case 'edit':
                if ($kode_trader != '' && $jnsmoda != '' && $region != '') {
                    $sql = "select A.*, B.URAIAN_NEGARA AS 'REGION', C.URAIAN AS 'JENIS MODA'
                                from m_tod A
                                LEFT JOIN m_negara B ON B.REGION = A.REGION
                                LEFT JOIN M_TABEL C ON C.MODUL = 'BC30' AND C.KDTAB = 'MODA' AND C.KDREC = A.JNSMODA
                           WHERE A.KODE_TRADER = '".$kode_trader."' AND
                                 A.REGION      = '".$region."' AND 
                                 A.JNSMODA    = '".$jnsmoda."'";
                    $hasil['TOD'] = $this->actMain->get_result($sql);
                    $hasil['act']     = 'update';
                } 
                else {
                    $hasil = 'error';
                }
                break;
            case'add':
                $hasil['act']     = 'insert';
                break;
        }
        
        return $hasil;
    }
    
    function setTod($act) {
        $this->load->model('actMain');
        switch ($act) {
            case'insert':
            case'update':
                $partner = $this->input->post('TOD');
                $partner['KODE_TRADER'] = $this->kd_trader;
                $exec = $this->actMain->insertRefernce('m_tod', $partner);
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/tod') . '|Update data partner berhasil';
                }
                else {
                    $hasil = 'MSG|ER|' . site_url('referensi/tod') . '|Update data partner gagal';
                }
                break;
            case'delete':
                $where = $this->input->post('tb_chkfpartner');
                $whrTBL = $this->db->where_in("CONCAT(KODE_TRADER,'|',KDKPBC,'|',NOMOREDI)", $where);
                $datCEK = $this->db->get_where('m_trader_partner_detil', $whrTBL);
                if($datCEK->num_rows()<=0){
                    $this->db->where_in("CONCAT(KODE_TRADER,'|',KDKPBC,'|',NOMOREDI)", $where);
                    $exec = $this->db->delete('m_trader_partner');
                    if ($exec) {
                        $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/partner') . '|Delete data partner berhasil';
                    } 
                    else {
                        $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/partner') . '|Delete data partner gagal';
                    }
                }
                else{
                    $hasil = 'Hapus terlebih dahulu detil dokumen / APRF.';
                }
                break;
        }
        return $hasil;
    }
    
    function getHs($act, $kode = '') {
        $this->load->model('actMain');
        switch ($act) {
            case 'edit':
                if ($kode != '') {
                    $sql = "SELECT * 
                            FROM m_hs where HS = '" . $kode . "'";
                    $hasil['HS'] = $this->actMain->get_result($sql);                    
                } else {
                    $hasil = 'error';
                }
                break;
            case'add':
                break;
        }
        return $hasil;
    }
    
    function setHs($act) {
        $this->load->model('actMain');
        switch ($act) {
            case'save':
                $hs = $this->input->post('HS');
                $id = $HS['HS'];
                $sql = "SELECT count(*) JML FROM m_hs where HS = '" . $id . "'";
                $hasil['JML'] = $this->actMain->get_uraian($sql, 'JML');
                if ($hasil['JML'] > 0) {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/hs') . '|Insert data hs gagal. NO HS [' . $id . '] sudah ada';
                } else {
                    $exec = $this->actMain->insertRefernce('m_hs', $hs);
                    if ($exec) {
                        $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/hs') . '|Insert data hs berhasil';
                    } else {
                        $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/hs') . '|Insert data hs gagal ';
                    }
                }
                break;
            case'update':
                $hs = $this->input->post('HS');
                $exec = $this->actMain->insertRefernce('m_hs', $hs);
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/hs') . '|Update data hs berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/hs') . '|Update data hs gagal ';
                }
                break;
            case'delete':
                $where = $this->input->post('tb_chkfhs');
                $this->db->where_in("HS", $where);
                $exec = $this->db->delete('m_hs');
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/hs') . '|Delete data hs berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/hs') . '|Delete data hs gagal';
                }
                break;
        }
        return $hasil;
    }
    
    function getManifestGrup($act, $kode = '') {
        $this->load->model('actMain');
        switch ($act) {
            case 'edit':
                if ($kode != '') {
                    $sql = "SELECT * 
                            FROM m_manifestgrup where KDGRUP = '" . $kode . "'";
                    $hasil['MANIFESTGRUP'] = $this->actMain->get_result($sql);                                                            
                } else {
                    $hasil = 'error';
                }
                break;
            case'add':
                break;
        }        
        $hasil['ADADETIL']  = $this->actMain->get_mtabel('YN', 1, false, '', 'KODEDANUR','MANGRUP');
        $hasil['INWARD']  = $this->actMain->get_mtabel('YN', 1, false, '', 'KODEDANUR','MANGRUP');
        $hasil['OUTWARD']  = $this->actMain->get_mtabel('YN', 1, false, '', 'KODEDANUR','MANGRUP');
        $hasil['FTZ']  = $this->actMain->get_mtabel('YN', 1, false, '', 'KODEDANUR','MANGRUP');
        $hasil['EKSPOR']  = $this->actMain->get_mtabel('YN', 1, false, '', 'KODEDANUR','MANGRUP');       
        return $hasil;
    }
    
    function setManifestGrup($act) {
        $this->load->model('actMain');
        switch ($act) {
            case'save':
                $manifestgrup = $this->input->post('MANIFESTGRUP');
                $id = $MANIFESTGRUP['KDGRUP'];
                $sql = "SELECT count(*) JML FROM m_manifestgrup where KDGRUP = '" . $id . "'";
                $hasil['JML'] = $this->actMain->get_uraian($sql, 'JML');
                if ($hasil['JML'] > 0) {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/manifest_grup') . '|Insert data Manifest Grup gagal. KODE GRUP [' . $id . '] sudah ada';
                } else {
                    $exec = $this->actMain->insertRefernce('m_manifestgrup', $manifestgrup);
                    if ($exec) {
                        $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/manifest_grup') . '|Insert data manifest grup berhasil';
                    } else {
                        $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/manifest_grup') . '|Insert data manifest grup gagal ';
                    }
                }
                break;
            case'update':
                $manifestgrup = $this->input->post('MANIFESTGRUP');
                $exec = $this->actMain->insertRefernce('m_manifestgrup', $manifestgrup);
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/manifest_grup') . '|Update data manifest grup berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/manifest_grup') . '|Update data manifest grup gagal ';
                }
                break;
            case'delete':
                $where = $this->input->post('tb_chkfmanifest_grup');
                $this->db->where_in("KDGRUP", $where);
                $exec = $this->db->delete('m_manifestgrup');
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/manifest_grup') . '|Delete data manifest grup berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/manifest_grup') . '|Delete data manifest grup gagal';
                }
                break;
        }
        return $hasil;
    }
    
    function getTambat($act, $kode = '') {
        $this->load->model('actMain');
        switch ($act) {
            case 'edit':
                if ($kode != '') {
                    $sql = "SELECT * 
                            FROM m_tambat where KD_TAMBAT = '" . $kode . "'";
                    $hasil['TAMBAT'] = $this->actMain->get_result($sql);                    
                } else {
                    $hasil = 'error';
                }
                break;
            case'add':
                break;
        }
        return $hasil;
    }
    
    function setTambat($act) {
        $this->load->model('actMain');
        switch ($act) {
            case'save':
                $tambat = $this->input->post('TAMBAT');
                $id = $TAMBAT['KD_TAMBAT'];
                $sql = "SELECT count(*) JML FROM m_tambat where KD_TAMBAT = '" . $id . "'";
                $hasil['JML'] = $this->actMain->get_uraian($sql, 'JML');
                if ($hasil['JML'] > 0) {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/tambat') . '|Insert data tambat gagal. Kode Tambat [' . $id . '] sudah ada';
                } else {
                    $exec = $this->actMain->insertRefernce('m_tambat', $tambat);
                    if ($exec) {
                        $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/tambat') . '|Insert data tambat berhasil';
                    } else {
                        $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/tambat') . '|Insert data tambat gagal ';
                    }
                }
                break;
            case'update':
                $tambat = $this->input->post('TAMBAT');
                $exec = $this->actMain->insertRefernce('m_tambat', $tambat);
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/tambat') . '|Update data tambat berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/tambat') . '|Update data tambat gagal ';
                }
                break;
            case'delete':
                $where = $this->input->post('tb_chkftambat');
                $this->db->where_in("KD_TAMBAT", $where);
                $exec = $this->db->delete('m_tambat');
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/tambat') . '|Delete data tambat berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/tambat') . '|Delete data tambat gagal';
                }
                break;
        }
        return $hasil;
    }
    
    function getTod2($act, $kode = '') {
        $this->load->model('actMain');
        switch ($act) {
            case 'edit':
                if ($kode != '') {
                    $sql = "SELECT * 
                            FROM m_tod where REGION = '" . $kode . "'";
                    $hasil['TOD'] = $this->actMain->get_result($sql);                    
                } else {
                    $hasil = 'error';
                }
                break;
            case'add':
                break;
        }
        $hasil['MODA']  = $this->actMain->get_mtabel('MODA', 1, false, '', 'KODEDANUR','BC23');
        return $hasil;
    }
    
    function setTod2($act) {
        $this->load->model('actMain');
        switch ($act) {
            case'save':
                $tod = $this->input->post('TOD');
                $id = $TOD['REGION'];
                $sql = "SELECT count(*) JML FROM m_tod where REGION = '" . $id . "'";
                $hasil['JML'] = $this->actMain->get_uraian($sql, 'JML');
                if ($hasil['JML'] > 0) {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/tod2') . '|Insert data tod gagal. Kode Tod [' . $id . '] sudah ada';
                } else {
                    $exec = $this->actMain->insertRefernce('m_tod', $tod);
                    if ($exec) {
                        $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/tod2') . '|Insert data tod berhasil';
                    } else {
                        $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/tod2') . '|Insert data tod gagal ';
                    }
                }
                break;
            case'update':
                $tod = $this->input->post('TOD');
                $exec = $this->actMain->insertRefernce('m_tod', $tod);
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/tod2') . '|Update data tod berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/tod2') . '|Update data tod gagal ';
                }
                break;
            case'delete':
                $where = $this->input->post('tb_chkftod2');
                
                $arrchk = explode("|", $where[0]);
                $kode = $arrchk[0];
                //die($kode);
                $this->db->where_in("REGION", $kode);
                $exec = $this->db->delete('m_tod');
                if ($exec) {
                    $hasil = 'MSG|OK|' . site_url('referensi/dataPendukung/tod2') . '|Delete data tod berhasil';
                } else {
                    $hasil = 'MSG|ER|' . site_url('referensi/dataPendukung/tod2') . '|Delete data tod gagal';
                }
                break;
        }
        return $hasil;
    }
    
}