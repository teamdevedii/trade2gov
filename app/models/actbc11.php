<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class actBC11 extends CI_Model {
    var $kd_trader;
    function __construct() {
        parent::__construct();
        $this->kd_trader = $this->newsession->userdata('KODE_TRADER');
        return true;
    }
    
    function lstBC11() {
        $this->load->library('newtable');
        $tipe = 'lstBC11';
        $this->load->model("actMain");
        $SQL = "SELECT  CONCAT(SUBSTRING(A.CAR,1,6),'-',SUBSTRING(A.CAR,7,6),'-',SUBSTRING(A.CAR,13,8),'-',SUBSTRING(A.CAR,21,8)) AS 'No. Aju', "
                    . " A.NMANGKUT AS 'Nm Angkut', "
                    . " A.NOVOY AS 'No Voy/Flight', "
                    . " CONCAT(A.NOBC10,' / ',A.TGBC10) AS 'No / Tg BC 1.0', "
                    . " B.URAIAN AS 'Jenis Manifest', "
                    . " A.HJMLBL AS 'Jml BL/AWB', "
                    . " C.URAIAN as 'Status', "
                    . " A.CAR,A.STATUS as 'STATUS_KD' "
             . "FROM T_BC11HDR A LEFT JOIN m_tabel B ON A.JNSMANIFEST = B.KDREC AND B.MODUL = 'BC11' LEFT JOIN m_tabel C ON C.KDREC = A.STATUS AND C.KDTAB = 'STATUS' AND C.MODUL = 'BC11' WHERE KODE_TRADER = '" . $this->kd_trader . "'" ;
        $prosesnya = array(
            'Create'    => array('ADDAJAX', site_url('bc11/create'), '0', '_content_'),
            'View'      => array('GETAJAX', site_url('bc11/create'), '1', '_content_'),
            'Upload FLT'=> array('GETPOP',  site_url('bc11/uploadFLT'), '0', ' UPLOAD FILE FLT |800|530'),
            'Print'     => array('GETPOP',  site_url('bc11/beforecetak'), '1', ' CETAK DOKUMEN MANIFEST |400|200'),
            'Hapus '    => array('DELETEAJAX', site_url('bc11/delHeader'), 'N', '_content_'));
        $this->newtable->menu($prosesnya);
        $this->newtable->keys(array('CAR','STATUS_KD'));
        $this->newtable->hiddens(array('CAR','STATUS_KD'));
        $this->newtable->search(array(
            array('CONCAT(SUBSTRING(A.CAR,1,6),'-',SUBSTRING(A.CAR,7,6),'-',SUBSTRING(A.CAR,13,8),'-',SUBSTRING(A.CAR,21,8))', 'No. Aju'),
            array('a.KATEKS', 'Katagori Ekspor', 'tag-select', $this->actMain->get_mtabel('KATEKS', 1, false, '', 'KODEUR', 'BC30')),
            array('a.NAMABELI', 'Nama Pembeli'),
            array('a.ANGKUTNAMA', 'Nama Moda'),
            array('a.STATUS', 'Status', 'tag-select', $this->actMain->get_mtabel('STATUS', 1, false, '', 'KODEUR', 'BC10'))));
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->action(site_url("bc11/lstBC11"));
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->orderby('A.CAR');
        $this->newtable->sortby("DESC");
        $this->newtable->tipe_check('radio');
        $this->newtable->set_formid("f" . $tipe);
        $this->newtable->set_divid("div" . $tipe);
        $this->newtable->rowcount(20);
        $this->newtable->tipe_proses('button');
        $this->newtable->clear();
        $tabel .= $this->newtable->generate($SQL);
        $arrdata = array('tabel' => $tabel, 'tipe' => $tipe);
        if ($this->input->post("ajax")) {
            return $tabel;
        } else {
            return $arrdata;
        }
    }
    
    function lstGrup($car, $jns, $tipeM, $status='', $showMenu = false){
        $this->load->library('newtable');
        switch ($jns){
            case'I':$JNSManifest = 'a.INWARD';break;
            case'O':$JNSManifest = 'a.OUTWARD';break;
            default :$JNSManifest = 'a.INWARD';break;
        }
        switch ($tipeM){
            case'0':$TIPEManifest = " and a.FTZ = 'N' ";break;
            case'1':$TIPEManifest = " and a.FTZ = 'Y' ";break;
            case'2':$TIPEManifest = '';break;
            default :$TIPEManifest = " and a.FTZ = 'N' ";break;
        }
        
        $tipe = 'lstPost';
        $this->load->model("actMain");
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        //$car, $kdgrup, $ekspor
        if($showMenu){
            $prosesnya = array(
            'Print Respon' => array('NEWPOPCETAK', site_url('bc11/cetak'), '1', ' CETAK RESPON |900|550'));
            $this->newtable->menu($prosesnya);
        }
        $this->newtable->keys(array('KUNCI'));
        $this->newtable->group('a.KDGRUP');
        $this->newtable->show_menu($showMenu);
        $this->newtable->show_search(false);
        $this->newtable->show_chk($showMenu);
        if(!$showMenu){
            $this->newtable->detail(site_url('bc11/lstPost'));
            $this->newtable->detail_tipe('detil_priview_bottom');
            $keyQuery = "CONCAT('" . $car . "', '/', a.KDGRUP,'/', a.EKSPOR, '/" . $status . "') AS KUNCI ";
        }else{
            $this->newtable->having(" HAVING COUNT(b.NOPOS) > 0 ");
            $keyQuery = "CONCAT('" . $car . "', '/2/', a.KDGRUP) AS KUNCI ";
        }
        $this->newtable->hiddens(array('KUNCI'));
        $this->newtable->action(site_url("bc11/lstGrup"));
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->tipe_check('radio');
        $this->newtable->set_formid("f" . $tipe);
        $this->newtable->set_divid("div" . $tipe);
        $this->newtable->rowcount(30);
        $this->newtable->tipe_proses('button');
        $this->newtable->clear();
        $SQL = "SELECT CONCAT(a.KDGRUP, ' - ',a.URKDGRUP) AS 'Uraian Pos', COUNT(b.NOPOS) as 'Jumlah BL', " . $keyQuery
             . "FROM m_manifestgrup a left join t_bc11dtl b on b.KDGRUP = a.KDGRUP AND b.CAR = '" . $car . "' "
             . "WHERE " . $JNSManifest . " = 'Y' " . $TIPEManifest . $addQuery;
        // die($SQL);
        $tabel .= $this->newtable->generate($SQL);
        $arrdata = array('tabel' => $tabel, 'tipe' => $tipe);
        if ($this->input->post("ajax")) {
            return $tabel;
        } else {
            return $arrdata;
        }        
    }
    
    function lstDetil($car, $kdgrup, $export, $status){
        $this->load->library('newtable');
        $tipe = 'lstDetil';
        $this->load->model("actMain");
        $SQL = "SELECT NOPOS as 'No Pos', "
                   . " a.NOBL as 'No BL', "
                   . "a.TGBL as 'Tgl BL', "
                   . "a.NMPENGIRIM as 'Nm Pengirim', "
                   . "a.JMLCONT as 'Kontainer', "
                   . "Concat(a.JMLKEMAS,' ',a.JNSKEMAS) as 'Kemasan', "
                   . "a.CAR, a.KDGRUP, a.NOPOS, a.NOPOSSUB, a.NOPOSSUBSUB,'".$export."' as EKSPOR, '".$status."' as STATUS, DTLOK as 'Lengkap' "
             . "FROM t_bc11dtl a WHERE a.CAR = '" . $car . "' AND a.KDGRUP = '" . $kdgrup . "' AND a.KODE_TRADER = '" . $this->kd_trader . "'" ;
        $prosesnya = array(
            'Tambah'    => array('ADDAJAX', site_url('bc11/formDetil/' . $car . '/' . $kdgrup . '/0/0/0/' . $export), '0', 'f' . $tipe . '_form'),
            'Edit'      => array('GETAJAX', site_url('bc11/formDetil'), '1', 'f' . $tipe . '_form'),
            'Hapus '    => array('DELETEAJAX', site_url('bc11/delDetil'), 'N', 'filltd'));
        $this->newtable->menu($prosesnya);
        $this->newtable->keys(array('CAR','KDGRUP','NOPOS','NOPOSSUB','NOPOSSUBSUB','EKSPOR','STATUS'));
        $this->newtable->hiddens(array('CAR','KDGRUP','NOPOS','NOPOSSUB','NOPOSSUBSUB','EKSPOR','STATUS'));
        $this->newtable->search(array(
            array("a.NOPOS", 'No. Pos'),
            array('a.NOBL', 'No. BL'),
            array('a.JMLCONT', 'Jml Kontainer'),
            array("Concat(a.JMLKEMAS,' ',a.JNSKEMAS)", 'Kemasan')));
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->action(site_url('bc11/lstPost/' . $car . '/' . $kdgrup . '/' . $export .'/' . $status));
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->show_no(false);
        $this->newtable->orderby('a.NOPOS');
        $this->newtable->sortby("ASC");
        $this->newtable->tipe_check('radio');
        $this->newtable->set_formid("f" . $tipe);
        $this->newtable->set_divid("div" . $tipe);
        $this->newtable->rowcount(10);
        $this->newtable->tipe_proses('button');
        $this->newtable->clear();
        // die($SQL);
        $tabel .= $this->newtable->generate($SQL);
        $arrdata = array('list' => $tabel, 'type' => $tipe);
        if ($this->input->post("ajax")) {
            return $tabel;
        } else {
            return $arrdata;
        }
    }
    
    function lstRespon($car){
        $this->load->library('newtable');
        #$tipe = 'lstRespon';
        $this->load->model("actMain");
        $SQL = "SELECT 	a.CAR, "
                    . " a.RESKD, "
                    . " a.RESTG, "
                    . " a.RESWK, "
                    . " b.URAIAN as 'Jns. Respon', "
                    . " CONCAT(a.RESTG, ' ',a.RESWK) as 'Wkt Respon', "
                    . " a.NOBC11 as 'No. BC 1.1', DATE_FORMAT(a.TGBC11,'%d-%m-%Y') as 'Tgl BC 1.1', LEFT(a.DESKRIPSI,25) AS 'Deskripdi' "
             . "FROM t_bc11res a LEFT JOIN m_tabel b ON a.RESKD = b.KDREC AND b.KDTAB = 'STATUS' AND b.MODUL = 'BC11' "
             . "WHERE  a.KODE_TRADER = '" . $this->kd_trader . "' AND "
                    . "a.CAR = '" . $car . "'";
        $this->newtable->keys(array('CAR','RESKD','RESTG','RESWK'));
        $this->newtable->hiddens(array('CAR','RESKD','RESTG','RESWK'));
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->action(site_url('bc11/beforecetakrespon/'.$car));
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $prosesnya = array(
            'Print Respon' => array('NEWPOPCETAK', site_url('bc11/cetak'), '1', ' CETAK RESPON |900|550'));
        
        $this->newtable->orderby(1, 2);
        $this->newtable->sortby("DESC");
        $this->newtable->tipe_check('radio');
        $this->newtable->set_formid("f" . $tipe);
        $this->newtable->set_divid("div" . $tipe);
        $this->newtable->rowcount(10);
        $this->newtable->clear();
        $this->newtable->menu($prosesnya);
        $this->newtable->show_search(false);
        $this->newtable->tipe_proses('button');
        $tabel .= $this->newtable->generate($SQL);
        $arrdata['CAR'] = $car;
        $arrdata['tabel'] = $tabel;
        $arrdata['tipe'] = $tipe;
        if ($this->input->post("ajax")){return $tabel;}
        else{return $arrdata;}
    }
    
    public function daftarPos($car) {
        $this->load->model("actMain");
        #Query ambil data $jns, $tipeM actMain->get_result
        $sql="select CONCAT(JNSMANIFEST, '|', TIPEMANIFEST, '|' ,STATUS) as 'KEYS' from t_bc11hdr 
                where car = '".$car."' and kode_trader ='".$this->kd_trader."'";
        $key = $this->actMain->get_uraian ($sql, 'KEYS');
        $arrKey = explode ('|', $key);
        $jns     = $arrKey[0];
        $tipeM   = $arrKey[1];
        $status  = $arrKey[2];
        return $this->lstGrup($car, $jns, $tipeM, $status, true);
    }
            
    function getHeader($car=''){
        $this->load->model('actMain');
        if ($car) {
            $SQL = "SELECT A.*, "
                       . " DATE_FORMAT(A.TGBC10,'%Y-%m-%d') as TGBC10, "
                       . " B.URAIAN AS 'URJNSMANIFEST', "
                       . " C.URAIAN as 'URSTATUS', "
                       . " D.URAIAN_NEGARA as 'URVOYFLAG', "
                       . " E.URAIAN_PELABUHAN as 'URPELMUAT', "
                       . " F.URAIAN_PELABUHAN as 'URPELTRANSIT', "
                       . " G.URAIAN_PELABUHAN as 'URPELBKR', "
                       . " H.URAIAN_PELABUHAN as 'URPELNEXT', "
                       . " I.URAIAN_KPBC as 'URKPBC', "
                       . " J.NM_TAMBAT as 'URKADE' "
                 . "FROM T_BC11HDR A LEFT JOIN m_tabel B ON A.JNSMANIFEST = B.KDREC AND B.MODUL = 'BC11' "
                                  . "LEFT JOIN m_tabel C ON C.KDREC = A.STATUS AND C.KDTAB = 'STATUS' AND C.MODUL = 'BC11' "
                                  . "LEFT JOIN m_negara D ON D.KODE_NEGARA = A.VOYFLAG "
                                  . "LEFT JOIN m_pelabuhan E ON E.KODE_PELABUHAN = A.PELMUAT "
                                  . "LEFT JOIN m_pelabuhan F ON F.KODE_PELABUHAN = A.PELTRANSIT "
                                  . "LEFT JOIN m_pelabuhan G ON G.KODE_PELABUHAN = A.PELBKR "
                                  . "LEFT JOIN m_pelabuhan H ON H.KODE_PELABUHAN = A.PELNEXT "
                                  . "LEFT JOIN m_kpbc I ON I.KDKPBC = A.KPBC "
                                  . "LEFT JOIN m_tambat J ON J.KD_TAMBAT = A.KADE "
                 . "WHERE CAR='" . $car . "' AND KODE_TRADER='" . $this->kd_trader . "'";
            $data['HEADER'] = $this->actMain->get_result($SQL);
            
            $SQL = "SELECT COUNT(b.NOPOS) as 'HJMLBL', SUM(b.JMLKEMAS) as 'HJMLKEMAS', SUM(b.BRUTO) as 'HBRUTO', SUM(b.VOLUME) as 'HVOLUME', (SELECT COUNT(DISTINCT c.NOCONT) FROM t_bc11con c WHERE c.KODE_TRADER = a.KODE_TRADER AND c.CAR = a.CAR) as 'HJMLCONT' FROM t_bc11hdr a left join t_bc11dtl b on a.KODE_TRADER = b.KODE_TRADER AND a.CAR = b.CAR WHERE a.KODE_TRADER = '" . $this->kd_trader . "' AND b.CAR = '" . $car . "' GROUP BY a.KODE_TRADER , b.CAR";      
            $resume = $this->actMain->get_result($SQL);
            $data['HEADER']['HJMLBL']       = $resume['HJMLBL'];
            $data['HEADER']['HJMLKEMAS']    = $resume['HJMLKEMAS'];
            $data['HEADER']['HBRUTO']       = $resume['HBRUTO'];
            $data['HEADER']['HVOLUME']      = $resume['HVOLUME'];
            $data['HEADER']['HJMLCONT']     = $resume['HJMLCONT'];
            $data['act']    = 'update';
        } else {
            $sql = "SELECT a.KODE_ID as 'IDPGUNA', "
                       . " a.ID as 'NPWPPGUNA', "
                       . " a.NAMA as 'NAMAPGUNA',"
                       . " a.ALAMAT as 'ALMTGUNA',"
                       . " b.KODEAGEN as 'KODEAGEN' "
                 . "FROM m_trader a LEFT JOIN m_trader_bc10 b on a.KODE_TRADER = b.KODE_TRADER "
                 . "WHERE a.KODE_TRADER = '" . $this->kd_trader . "'";
            $data['HEADER'] = $this->actMain->get_result($sql);
            $data['HEADER']['JNSMANIFEST']  = 'I';
            $data['HEADER']['TIPEMANIFEST'] = '0';
            $data['act']    = 'save';
        }
        $data['IDPGUNA']    = $this->actMain->get_mtabel('JENIS_IDENTITAS', 1, false, '', 'KODEDANUR', 'BC10');
        $data['MODA']       = $this->actMain->get_mtabel('MODA', 1, false, '', 'KODEDANUR', 'BC10');
        $data['MAXLENGTH']  = '<input type="hidden" id="MAXLENGTH" value="' . $this->actMain->get_uraian("SELECT F_MAXLENGTH('t_bc11hdr') as MAXLENGTH", 'MAXLENGTH') . '"/>';
        return $data;
    }
    
    function getDetil($car, $kdgrup, $nopos, $nopossub, $nopossubsub, $export, $status){
        $this->load->model('actMain');
        if($car != '' && $kdgrup != ''){
            if($nopos > 0 ){
                $SQL = "SELECT a.*, "
                            . "b.URAIAN_KEMASAN as URMJNSKEMAS, "
                            . "c.URAIAN_PELABUHAN as URDPELASAL, "
                            . "d.URAIAN_PELABUHAN as URDPELSEBELUM, "
                            . "e.URAIAN_PELABUHAN as URDPELBONGKAR, "
                            . "f.URAIAN_PELABUHAN as URDPELLANJUT, "
                            . "g.URAIAN_KEMASAN as URJNSKEMAS, "
                            . "h.URAIAN as URDTLOK "
                     . "FROM t_bc11dtl a LEFT JOIN m_kemasan b ON a.MJNSKEMAS       = b.KODE_KEMASAN  "
                                      . "LEFT JOIN m_pelabuhan c ON a.DPELASAL      = c.KODE_PELABUHAN "
                                      . "LEFT JOIN m_pelabuhan d ON a.DPELSEBELUM   = d.KODE_PELABUHAN "
                                      . "LEFT JOIN m_pelabuhan e ON a.DPELBONGKAR   = e.KODE_PELABUHAN "
                                      . "LEFT JOIN m_pelabuhan f ON a.DPELLANJUT    = f.KODE_PELABUHAN "
                                      . "LEFT JOIN m_kemasan g ON a.JNSKEMAS        = g.KODE_KEMASAN "
                                      . "LEFT JOIN m_tabel h ON a.DTLOK = h.KDREC AND h.MODUL = 'BC11' AND h.KDTAB = 'DTLOK' "
                     . "WHERE a.KODE_TRADER = '" . $this->kd_trader . "' AND "
                           . "a.CAR = '" . $car . "' AND "
                           . "a.KDGRUP = '" . $kdgrup . "' AND "
                           . "a.NOPOS = '" . $nopos . "' AND "
                           . "a.NOPOSSUB = '" . $nopossub . "' AND "
                           . "a.NOPOSSUBSUB = '" . $nopossubsub . "'";
                $data['DETIL'] = $this->actMain->get_result($SQL);
                $data['DATABARANG']     = $this->lstPendukung('barang', $car, $kdgrup, $nopos, $nopossub, $nopossubsub, $status);
                $data['DATAKONTAINER']  = $this->lstPendukung('kontainer', $car, $kdgrup, $nopos, $nopossub, $nopossubsub, $status);
                if(strtoupper($export) == 'Y'){
                    $data['DATADOKUMEN']    = $this->lstPendukung('dokumen', $car, $kdgrup, $nopos, $nopossub, $nopossubsub, $status);
                }
            }else{
                $data['DETIL']['CAR']           = $car;
                $data['DETIL']['KDGRUP']        = $kdgrup;
                $SQL = "SELECT IF(MAX(a.NOPOS)>0, MAX(a.NOPOS) + 1, 1) AS 'NOPOS' FROM t_bc11dtl a WHERE a.KODE_TRADER = '" . $this->kd_trader . "' AND a.CAR = '" . $car . "' AND a.KDGRUP = '" . $kdgrup . "'";
                $data['DETIL']['NOPOS']         = $this->actMain->get_uraian($SQL,'NOPOS');
                $data['DETIL']['NOPOSSUB']      = '0';
                $data['DETIL']['NOPOSSUBSUB']   = '0';
                $data['DETIL']['DTLOK']         = 'N';
                $data['DETIL']['URDTLOK']       = 'Tidak Lengkap';
            }
        }
        $data['MAXLENGTH']  = '<input type="hidden" id="MAXLENGTH" value="' . $this->actMain->get_uraian("SELECT F_MAXLENGTH('t_bc11dtl') as MAXLENGTH", 'MAXLENGTH') . '"/>';
        $data['EXPORT']     = $export;
        $data['STATUSHDR']  = $status;
        return $data;
    }
    
    function getBarang($car, $kdgrup, $nopos, $nopossub, $nopossubsub, $seri, $status){
        $this->load->model('actMain');
        if($car !='' && $kdgrup !='' && $nopos !='' && $nopossub !='' && $nopossubsub !=''){
            if($seri !=''){
                $SQL = "SELECT a.* FROM t_bc11brg a "
                     . "WHERE a.KODE_TRADER = '" . $this->kd_trader . "' AND "
                           . "a.CAR = '" . $car . "' AND "
                           . "a.KDGRUP = '" . $kdgrup . "' AND "
                           . "a.NOPOS = '" . $nopos . "' AND "
                           . "a.NOPOSSUB = '" . $nopossub . "' AND "
                           . "a.NOPOSSUBSUB = '" . $nopossubsub . "' AND "
                           . "a.SERI = '" . $seri . "'";
                $data['BARANG'] = $this->actMain->get_result($SQL);
            }else{
                $data['BARANG']['CAR']          = $car;
                $data['BARANG']['KDGRUP']       = $kdgrup;
                $data['BARANG']['NOPOS']        = $nopos;
                $data['BARANG']['NOPOSSUB']     = $nopossub;
                $data['BARANG']['NOPOSSUBSUB']  = $nopossubsub;
                $SQL = "SELECT IF(MAX(a.SERI)>0, MAX(a.SERI) + 1, 1) AS 'SERI' FROM t_bc11brg a WHERE a.KODE_TRADER = '" . $this->kd_trader . "' AND a.CAR = '" . $car . "' AND a.KDGRUP = '" . $kdgrup . "' AND a.NOPOS = '" . $nopos . "' AND a.NOPOSSUB = '" . $nopossub . "' AND a.NOPOSSUBSUB = '" . $nopossubsub . "'";
                $data['BARANG']['SERI']         = $this->actMain->get_uraian($SQL, 'SERI');
            }
            $data['STATUSHDR'] = $status;
        }
        return $data;
    }
    
    function getKontainer($car, $kdgrup, $nopos, $nopossub, $nopossubsub, $seri, $status){
        $this->load->model('actMain');
        if($car !='' && $kdgrup !='' && $nopos !='' && $nopossub !='' && $nopossubsub !=''){
            if($seri !=''){
                $SQL = "SELECT a.* FROM t_bc11con a "
                     . "WHERE a.KODE_TRADER = '" . $this->kd_trader . "' AND "
                           . "a.CAR = '" . $car . "' AND "
                           . "a.KDGRUP = '" . $kdgrup . "' AND "
                           . "a.NOPOS = '" . $nopos . "' AND "
                           . "a.NOPOSSUB = '" . $nopossub . "' AND "
                           . "a.NOPOSSUBSUB = '" . $nopossubsub . "' AND "
                           . "a.SERI = '" . $seri . "'";
                $data['KONTAINER'] = $this->actMain->get_result($SQL);
            }else{
                $data['KONTAINER']['CAR']          = $car;
                $data['KONTAINER']['KDGRUP']       = $kdgrup;
                $data['KONTAINER']['NOPOS']        = $nopos;
                $data['KONTAINER']['NOPOSSUB']     = $nopossub;
                $data['KONTAINER']['NOPOSSUBSUB']  = $nopossubsub;
                $SQL = "SELECT IF(MAX(a.SERI)>0, MAX(a.SERI) + 1, 1) AS 'SERI' FROM t_bc11con a WHERE a.KODE_TRADER = '" . $this->kd_trader . "' AND a.CAR = '" . $car . "' AND a.KDGRUP = '" . $kdgrup . "' AND a.NOPOS = '" . $nopos . "' AND a.NOPOSSUB = '" . $nopossub . "' AND a.NOPOSSUBSUB = '" . $nopossubsub . "'";
                $data['KONTAINER']['SERI']         = $this->actMain->get_uraian($SQL, 'SERI');
            }
            $data['UKURAN_CONTAINER'] = $this->actMain->get_mtabel('UKURAN_CONTAINER', 1, false, '', 'KODEUR', 'BC11');
            $data['JENIS_CONTAINER']  = $this->actMain->get_mtabel('JENIS_CONTAINER', 1, false, '', 'KODEUR', 'BC11');
            $data['STATUSHDR'] = $status;
        }
        return $data;
    }
    
    function getDokumen($car, $kdgrup, $nopos, $nopossub, $nopossubsub, $seri, $status){
        $this->load->model('actMain');
        if($car !='' && $kdgrup !='' && $nopos !='' && $nopossub !='' && $nopossubsub !=''){
            if($seri !=''){
                $SQL = "SELECT a.*, b.URAIAN_KPBC as 'URDOKKPBC' "
                     . "FROM t_bc11dok a LEFT JOIN m_kpbc b ON a.DOKKPBC = b.KDKPBC "
                     . "WHERE a.KODE_TRADER = '" . $this->kd_trader . "' AND "
                           . "a.CAR = '" . $car . "' AND "
                           . "a.KDGRUP = '" . $kdgrup . "' AND "
                           . "a.NOPOS = '" . $nopos . "' AND "
                           . "a.NOPOSSUB = '" . $nopossub . "' AND "
                           . "a.NOPOSSUBSUB = '" . $nopossubsub . "' AND "
                           . "a.SERI = '" . $seri . "'";
                $data['DOKUMEN'] = $this->actMain->get_result($SQL);
            }else{
                $data['DOKUMEN']['CAR']          = $car;
                $data['DOKUMEN']['KDGRUP']       = $kdgrup;
                $data['DOKUMEN']['NOPOS']        = $nopos;
                $data['DOKUMEN']['NOPOSSUB']     = $nopossub;
                $data['DOKUMEN']['NOPOSSUBSUB']  = $nopossubsub;
                $SQL = "SELECT IF(MAX(a.SERI)>0, MAX(a.SERI) + 1, 1) AS 'SERI' FROM t_bc11dok a WHERE a.KODE_TRADER = '" . $this->kd_trader . "' AND a.CAR = '" . $car . "' AND a.KDGRUP = '" . $kdgrup . "' AND a.NOPOS = '" . $nopos . "' AND a.NOPOSSUB = '" . $nopossub . "' AND a.NOPOSSUBSUB = '" . $nopossubsub . "'";
                $data['DOKUMEN']['SERI']         = $this->actMain->get_uraian($SQL, 'SERI');
            }
            $data['DOKKODE'] = $this->actMain->get_mtabel('KDDOK', 1, false, '', 'KODEUR', 'BC11');
            $data['STATUSHDR'] = $status;
        }
        return $data;
    }
    
    private function lstPendukung($type, $car, $kdgrup, $nopos, $nopossub, $nopossubsub, $status){
        $arrdata        = $this->lstDataPendukung($type, $car, $kdgrup, $nopos, $nopossub, $nopossubsub, $status);
        $data['list']   = $this->load->view('list', $arrdata, true);
        $data['type']   = $type;
        return $this->load->view("bc11/lst", $data, true);
    }
            
    function lstDataPendukung($type, $car, $kdgrup, $nopos, $nopossub, $nopossubsub, $status){
        $this->load->library('newtable');
        $this->load->model('actMain');
        $this->newtable->keys = '';
        switch ($type){
            case'barang':
                $SQL = "SELECT  a.SERI ,a.HS as 'Kode HS', a.Description as 'Deskripsi' "
                     . "FROM t_bc11brg a "
                     . "WHERE a.KODE_TRADER = '" . $this->kd_trader . "' AND "
                           . "a.CAR = '" . $car . "' AND "
                           . "a.KDGRUP = '" . $kdgrup . "' AND "
                           . "a.NOPOS = '" . $nopos . "' AND "
                           . "a.NOPOSSUB = '" . $nopossub . "' AND "
                           . "a.NOPOSSUBSUB = '" . $nopossubsub . "' ";
                $this->newtable->keys(array('SERI'));
                $this->newtable->hiddens(array('SERI'));
                $this->newtable->show_search(false);
                $this->newtable->orderby("SERI");
                $this->newtable->sortby("ASC");
            break;
            case'kontainer':
                $SQL = "SELECT a.SERI, a.NOCONT as 'Nomor', a.UKCONT as 'Ukuran', a.TIPECONT as 'Tipe', a.SEALNUM as 'Seal Number', a.PARTIAL as 'Parsial' "
                     . "FROM t_bc11con a "
                     . "WHERE a.KODE_TRADER = '" . $this->kd_trader . "' AND "
                           . "a.CAR = '" . $car . "' AND "
                           . "a.KDGRUP = '" . $kdgrup . "' AND "
                           . "a.NOPOS = '" . $nopos . "' AND "
                           . "a.NOPOSSUB = '" . $nopossub . "' AND "
                           . "a.NOPOSSUBSUB = '" . $nopossubsub . "' ";
                $this->newtable->keys(array('SERI'));
                $this->newtable->hiddens(array('SERI'));
                $this->newtable->show_search(false);
                $this->newtable->orderby("SERI");
                $this->newtable->sortby("ASC");
            break;
            case'dokumen':
                $SQL = "SELECT a.SERI, b.URAIAN as 'Jenis' , a.DOKNO as 'Nomor', a.DOKTG as 'Tanggal', a.DOKKPBC as 'KPBC' "
                     . "FROM t_bc11dok a LEFT JOIN m_tabel b ON a.DOKKODE = b.KDREC AND b.MODUL = 'BC11' AND b.KDTAB = 'KDDOK' "
                     . "WHERE a.KODE_TRADER = '" . $this->kd_trader . "' AND "
                           . "a.CAR = '" . $car . "' AND "
                           . "a.KDGRUP = '" . $kdgrup . "' AND "
                           . "a.NOPOS = '" . $nopos . "' AND "
                           . "a.NOPOSSUB = '" . $nopossub . "' AND "
                           . "a.NOPOSSUBSUB = '" . $nopossubsub . "' ";
                $this->newtable->keys(array('SERI'));
                $this->newtable->hiddens(array('SERI'));
                $this->newtable->show_search(false);
                $this->newtable->orderby("SERI");
                $this->newtable->sortby("ASC");
                break;
            default :
                return "Failed"; exit();
            break;
        }
		$status = $this->actMain->get_uraian("SELECT STATUS FROM t_bc11hdr WHERE CAR = '" . $car . "'", 'STATUS');
        $key    = $car . '/' . $kdgrup . '/' . $nopos . '/' . $nopossub . '/' . $nopossubsub . '/' .$status;
        $ciuri  = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->action(site_url('bc11/lstDataPendukung/' . $type . '/' . $key));
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->rowcount(5);
        $this->newtable->clear();
        $this->newtable->set_formid("f" . $type);
        $this->newtable->set_divid("div" . $type);
        $process = array(
            'Tambah '   . $type => array('ADDAJAX', site_url('bc11/frm' . $type. '/' . $key), '0', 'f' . $type . '_form'),
            'Ubah '     . $type => array('EDITAJAX', site_url('bc11/frm' . $type . '/' . $key), '1', 'f' . $type . '_form'),
            'Hapus '    . $type => array('DELETEAJAX', site_url('bc11/frm' . $type . '/' . $key), '1', 'f' . $type . '_list'));
        $this->newtable->menu($process);
        $tabel = $this->newtable->generate($SQL);
        $arrdata = array("tabel" => $tabel);
        if ($this->input->post("ajax")){
            return $tabel;
        }else{
            return $arrdata;
        }
    }
    
    function setHeader($data){
        $this->load->model('actMain');
        $updateCar = false ;
        $data['HEADER']['NPWPPGUNA'] = str_replace('.', '', str_replace('-', '', $data['HEADER']['NPWPPGUNA']));
        if($data['HEADER']['CAR'] == ''){
            $data['HEADER']['CAR'] = $this->actMain->get_car('BC10');
            $updateCar = true ;
        }
        #mapping untuk data referensi kapal
        $data['KAPAL']['MODA']      = $data['HEADER']['MODA'];
        $data['KAPAL']['REGMODA']   = $data['HEADER']['REGKAPAL'];
        $data['KAPAL']['NMMODA']    = $data['HEADER']['NMANGKUT'];
        $data['KAPAL']['FLAGMODA']  = $data['HEADER']['VOYFLAG'];
        $this->actMain->insertRefernce('m_trader_kapalmanifest',$data['KAPAL']);
        if($data['HEADER']['KPPT']!='1'){$data['HEADER']['KPPT'] = '0';}
        $int = $this->actMain->insertRefernce('t_bc11hdr',$data['HEADER']);
        if($int && $updateCar){$this->actMain->set_car('BC10');}
        $rtn = $this->validasiHeader($data['HEADER']['CAR']);
        return $rtn;
    }
    
    function setDetil($data){
        $this->load->model('actMain');
        $DETIL  = $data['DETIL'];
        #mapping data parties
        #-Pengirim
        $data['PARTIES']['KODEPARTIES'] = '0';
        $data['PARTIES']['NAMAPARTIES'] = $DETIL['NMPENGIRIM'];
        $data['PARTIES']['ALMTPARTIES'] = $DETIL['ALPENGIRIM'];
        $this->actMain->insertRefernce('m_trader_parties',$data['PARTIES']);
        #-Penerima
        $data['PARTIES']['KODEPARTIES'] = '1';
        $data['PARTIES']['NAMAPARTIES'] = $DETIL['NMPENERIMA'];
        $data['PARTIES']['ALMTPARTIES'] = $DETIL['ALPENERIMA'];
        $this->actMain->insertRefernce('m_trader_parties',$data['PARTIES']);
        #-Pemberitahu
        $data['PARTIES']['KODEPARTIES'] = '2';
        $data['PARTIES']['NAMAPARTIES'] = $DETIL['NMPEMBERITAU'];
        $data['PARTIES']['ALMTPARTIES'] = $DETIL['ALPEMBERITAU'];
        $this->actMain->insertRefernce('m_trader_parties',$data['PARTIES']);
        if($DETIL['MBL'] != 'Y'){
            $DETIL['MNOBL'] = '';
            $DETIL['MTGBL'] = '';
            $DETIL['MJMLKEMAS'] = '';
            $DETIL['MJNSKEMAS'] = '';
            $DETIL['MJMLCONT']  = '';
        }
        $this->actMain->insertRefernce('t_bc11Dtl',$DETIL);
        #UPDATE STATUS HEADER
        $dataH['CAR']   = $DETIL['CAR'];
        $dataH['STATUS']= '00';
        $this->actMain->insertRefernce('t_bc11Hdr',$dataH);
        
        $query = "SELECT FLDNAME,MESSAGE from m_validasi WHERE SCRNAME = 'T_BC11DTL' AND MANDATORY = 1 ";
        $roleD = $this->actMain->get_result($query, true);

        $query = "SELECT * FROM t_bc11dtl a WHERE a.KODE_TRADER = " . $this->kd_trader . " AND a.CAR = '" . $DETIL['CAR'] . "' AND a.KDGRUP = '" . $DETIL['KDGRUP'] . "' AND a.NOPOS = '" . $DETIL['NOPOS'] . "' AND NOPOSSUB = '" . $DETIL['NOPOSSUB'] . "' AND a.NOPOSSUBSUB = '" . $DETIL['NOPOSSUBSUB'] . "' ORDER BY a.KDGRUP,a.NOPOS ASC";
        $aD = $this->actMain->get_result($query);
        $msg = $this->validasiAllDetil($roleD,$aD,false);
        return "MSG|OK|".site_url('bc11/lstPost/' . $DETIL['CAR'] . '/' . $DETIL['KDGRUP'] . '/' . $DETIL['EXPORT']) . '|' . $msg;
    }
    
    function setBarang($data){
        $this->load->model('actMain');
        $BARANG = $data['BARANG'];
        $exec = $this->actMain->insertRefernce('t_bc11Brg',$BARANG);
        if($exec){
            $rtn = "MSG|OK|".site_url('bc11/lstDataPendukung/barang/' . $BARANG['CAR'] . '/' . $BARANG['KDGRUP'] . '/' . $BARANG['NOPOS'] . '/' . $BARANG['NOPOSSUB'] . '/' . $BARANG['NOPOSSUBSUB']) . '|Proses data barang berhasil.';
        }else{
            $rtn = 'MSG|ERR||Proses data barang gagal.';
        }
        return $rtn;
    }
    
    function delBarang($car, $kdgrup, $nopos, $nopossub, $nopossubsub, $seri){
        $where = array('CAR'=>$car, 'KDGRUP'=>$kdgrup, 'NOPOS'=>$nopos, 'NOPOSSUB'=>$nopossub, 'NOPOSSUBSUB'=>$nopossubsub, 'SERI'=>$seri);
        $this->db->where($where);
        $exec = $this->db->delete('t_bc11brg');
        if ($exec) {
            $rtn = "MSG|OK|".site_url('bc11/lstDataPendukung/barang/' . $car . '/' . $kdgrup . '/' . $nopos . '/' . $nopossub . '/' . $nopossubsub) . '|Proses data barang berhasil.';
        } else {
            $rtn = 'MSG|ER||Proses data barang gagal.';
        }
        return $rtn;
    }
    
    function setKontainer($data){
        $this->load->model('actMain');
        $KONTAINER = $data['KONTAINER'];
        $exec = $this->actMain->insertRefernce('t_bc11Con',$KONTAINER);
        if($exec){
            $rtn = "MSG|OK|".site_url('bc11/lstDataPendukung/kontainer/' . $KONTAINER['CAR'] . '/' . $KONTAINER['KDGRUP'] . '/' . $KONTAINER['NOPOS'] . '/' . $KONTAINER['NOPOSSUB'] . '/' . $KONTAINER['NOPOSSUBSUB']) . '|Proses data kontainer berhasil.';
        } else {
            $rtn = 'MSG|ERR||Proses data kontainer gagal.';
        }
        return $rtn;
    }
    
    function delKontainer($car, $kdgrup, $nopos, $nopossub, $nopossubsub, $seri){
        $where = array('CAR'=>$car, 'KDGRUP'=>$kdgrup, 'NOPOS'=>$nopos, 'NOPOSSUB'=>$nopossub, 'NOPOSSUBSUB'=>$nopossubsub, 'SERI'=>$seri);
        $this->db->where($where);
        $exec = $this->db->delete('t_bc11con');
        if ($exec) {
            $rtn = "MSG|OK|".site_url('bc11/lstDataPendukung/kontainer/' . $car . '/' . $kdgrup . '/' . $nopos . '/' . $nopossub . '/' . $nopossubsub) . '|Proses data kontainer berhasil.';
        } else {
            $rtn = 'MSG|ER||Proses data kontainer gagal.';
        }
        return $rtn;
    }
    
    function setDokumen($data){
        $this->load->model('actMain');
        $DOKUMEN    = $data['DOKUMEN'];
        $exec       = $this->actMain->insertRefernce('t_bc11Dok',$DOKUMEN);
        if($exec){
            $rtn = "MSG|OK|".site_url('bc11/lstDataPendukung/dokumen/' . $DOKUMEN['CAR'] . '/' . $DOKUMEN['KDGRUP'] . '/' . $DOKUMEN['NOPOS'] . '/' . $DOKUMEN['NOPOSSUB'] . '/' . $DOKUMEN['NOPOSSUBSUB']) . '|Proses data dokumen berhasil.';
        }else{
            $rtn = 'MSG|ERR||Proses data dokumen gagal.';
        }
        return $rtn;
    }
    
    function delDokumen($car, $kdgrup, $nopos, $nopossub, $nopossubsub, $seri){
        $where = array('CAR'=>$car, 'KDGRUP'=>$kdgrup, 'NOPOS'=>$nopos, 'NOPOSSUB'=>$nopossub, 'NOPOSSUBSUB'=>$nopossubsub, 'SERI'=>$seri);
        $this->db->where($where);
        $exec = $this->db->delete('t_bc11dok');
        if ($exec) {
            $rtn = "MSG|OK|".site_url('bc11/lstDataPendukung/dokumen/' . $car . '/' . $kdgrup . '/' . $nopos . '/' . $nopossub . '/' . $nopossubsub) . '|Proses data dokumen berhasil.';
        } else {
            $rtn = 'MSG|ER||Proses data dokumen gagal.';
        }
        return $rtn;
    }
    
    function validasiHeader($car){
        $this->load->model('actMain');
        $i = 0;
        $query = "SELECT A.* FROM t_bc11hdr A WHERE A.CAR = '" . $car . "' AND A.KODE_TRADER = " . $this->kd_trader;
        $aH = $this->actMain->get_result($query);
        if (count($aH) > 0) {
            $judul = 'Hasil validasi Nomor Aju : ' . substr($car, 20) . ' tanggal ' . date('d-m-Y', strtotime(substr($car, 12, 8)));
            $query = "SELECT FLDNAME,MESSAGE from m_validasi WHERE SCRNAME = 'T_BC11HDR' AND MANDATORY = 1 ";
            $arrdataV = $this->actMain->get_result($query, true);
            foreach ($arrdataV as $aV) {
                if (trim($aH[$aV['FLDNAME']]) == '') {
                    $hasil .= substr('   ' . ++$i, -3) . '. ' . $aV['MESSAGE'] . ' Harus diisi<br>';
                }
            }
            #khusus
            #Moda Laut Dan Udara Saja
            if(!strpos('|1|4|', $aH['MODA'])){
                $hasil .= substr('   ' . ++$i, -3) . '. Jenis Moda yang diijinkan hanya Laut atau Udara<br>';#pengaruh di flt HCAR0101
            }
            
            #check detil
            $query = "SELECT FLDNAME,MESSAGE from m_validasi WHERE SCRNAME = 'T_BC11DTL' AND MANDATORY = 1 ";
            $roleD = $this->actMain->get_result($query, true);
            
            $query = "SELECT * FROM t_bc11dtl a WHERE a.KODE_TRADER = " . $this->kd_trader . " AND a.CAR = '" . $car . "' ORDER BY a.KDGRUP,a.NOPOS ASC";
            $aD = $this->actMain->get_result($query, true);
            foreach ($aD as $a){
                if($this->validasiAllDetil($roleD,$a,true)==false){
                    $hasil .= substr('   ' . ++$i, -3) . '. ada kesalahan di group : ' . $a['KDGRUP'] . ' Nomor Pos : ' . $a['NOPOS'] . '<br>';
                }
            }
            
            if (strpos('|00|01|05|', trim($aH['STATUS']))){
                $exec['STATUS'] = ($i > 0) ? '00' : '01';
            }
            $exec['CAR'] = $aH['CAR'];
            $this->actMain->insertRefernce('t_bc11hdr',$exec);
            unset($aH);
            //tulis informasi
            if ($i == 0) {
                $hasil .= '<br>------------------ Data Lengkap ------------------<br>';
            } else {
                $hasil .= '<br>--------------- Data Tidak Lengkap ---------------<br>';
            }
        }
        $view['data'] = $hasil;
        $view['judul'] = $judul;
        $msg = $this->load->view('notify', $view, true);
        return $msg . '|' . $car;
    }
    
    function validasiAllDetil($role, $dataDetil, $bacth){
        $this->load->model('actMain');
        $i = 0;
        foreach ($role as $vld){
            if (($dataDetil[$vld['FLDNAME']] == '' && $vld['TIPE']=='') || ($dataDetil[$vld['FLDNAME']] <= 0 && $vld['TIPE']=='NUMBER') || (($dataDetil[$vld['FLDNAME']] == '' || $dataDetil[$vld['FLDNAME']]=='0000-00-00') && $vld['TIPE']=='DATE') || (($dataDetil[$vld['FLDNAME']] == '' || $dataDetil[$vld['FLDNAME']]=='00:00:00') && $vld['TIPE']=='TIME') || (($dataDetil[$vld['FLDNAME']] == '' || $dataDetil[$vld['FLDNAME']]=='0000-00-00 00:00:00') && $vld['TIPE']=='DATETIME')) {
                $hasil .= substr('   ' . ++$i, -3) . '. ' . ucwords(strtolower($vld['MESSAGE'])) . ' Harus diisi<br>';
            }
        }
        ################ validasi kusus ##################
        $data['CAR']        = $dataDetil['CAR'];
        $data['KDGRUP']     = $dataDetil['KDGRUP'];
        $data['NOPOS']      = $dataDetil['NOPOS'];
        $data['NOPOSSUB']   = $dataDetil['NOPOSSUB'];
        $data['NOPOSSUBSUB']= $dataDetil['NOPOSSUBSUB'];
        if ($i == 0) {
            $data['DTLOK']  = 'Y';
            $hasil .= '<br>------------------ Data Lengkap ------------------<br>';
        } else {
            $data['DTLOK']  = 'N';
            $hasil .= '<br>--------------- Data Tidak Lengkap ---------------<br>';
        }
        $this->actMain->insertRefernce('t_bc11dtl',$data);
        if($bacth){
            if($i==0){
                $rtn = true;
            }else{
                $rtn = false;
            }
        }else{
            $rtn = $hasil;
        }
        return $rtn;
    }
    
    function genFlatfile($car){
        function fixLen($str, $len, $chr = ' ', $alg = STR_PAD_RIGHT) {
            $hasil = str_pad(substr($str, 0, $len), $len, $chr, $alg);
            return $hasil;
        }
        $this->load->model('actMain');
        $EDINUMBER = $this->newsession->userdata('EDINUMBER');
        $SQL = "SELECT a.*, c.NOMOREDI,"
             . "       DATE_FORMAT(a.TGBC10,'%Y%m%d') as TGBC10, "
             . "       DATE_FORMAT(a.TGBC11,'%Y%m%d') as TGBC11, "
             . "       DATE_FORMAT(a.TGBC11ASAL,'%Y%m%d') as TGBC11ASAL, "
             . "       DATE_FORMAT(a.TGTIBABERANGKAT,'%Y%m%d') as TGTIBABERANGKAT, "
             . "       DATE_FORMAT(NOW(),'%Y%m%d%H%i') as TGLDOKUMEN "
             . "FROM T_BC11HDR a LEFT JOIN m_trader_bc10 b ON a.KODE_TRADER = b.KODE_TRADER AND b.KPBC = a.KPBC "
                              . "LEFT JOIN m_trader_partner c ON a.KODE_TRADER = c.KODE_TRADER and c.KDKPBC = a.KPBC "
             . "WHERE a.CAR = '" . $car . "' AND a.KODE_TRADER = " . $this->kd_trader;
        $hdr = $this->actMain->get_result($SQL);
        $hdr = str_replace("\n", '', str_replace("\r", '', $hdr));
        if ($hdr['STATUS'] != '01') {
            if ($hdr['STATUS'] == '02') {
                $whHDR['KODE_TRADER'] = $this->kd_trader;
                $whHDR['CAR'] = $car;
                $this->db->where($whHDR);
                $upHDR['SNRF'] = '';
                $upHDR['DIRFLT'] = '';
                $upHDR['DIREDI'] = '';
                $upHDR['STATUS'] = '01';
                $this->db->update('t_bc11hdr', $upHDR);
                unset($upHDR);
                #delete dokoutbound
                $whHDR['KDKPBC'] = $hdr['KPBC'];
                $whHDR['JNSDOK'] = 'BC11';
                $whHDR['CAR']    = $hdr['CAR'];
                $this->db->delete('m_trader_dokoutbound', $whHDR);
                unset($whHDR);
                unlink($hdr['DIRFLT']);
                unlink($hdr['DIREDI']);
                $hsl = '1|';#berhasil unEdifact
            } else {
                $hsl = '0|';#Status tidak bisa di edifact atau unEdifact
            }
            unset($hdr);
            return $hsl;
        }
        if ($hdr['NOMOREDI'] == '') {
            return '0|Edinumber Partner Kantor Pelayanan Bea Cukai ' . $hdr['NOMOREDI'] . ' belum terekam.';
        }
        
        #SEPARATOR UNTUK HEADER
        $FF.= 'ENV00101' . fixLen($EDINUMBER, 20) . fixLen($hdr['NOMOREDI'], 13) . 'DOKMANIF  ' . "2\n"; #berpatokan dari flt yang digenerate oleh ai

        #RECORD TYPE HCAR0101
        #mapping Moda
        switch ($hdr['MODA']){
            case'1':$jnModa = 'S';break;
            case'4':$jnModa = 'A';break;
        }
        $FF.= 'HCAR0101' . fixLen($hdr['CAR'], 26) . '9 ' . fixLen($hdr['JNSMANIFEST'].$jnModa , 10) . '242' . fixLen($hdr['TGLDOKUMEN'], 12) . "203\n";

        #RECORD TYPE HCAR0201
        $FF .= ($hdr['NOBC10']!='')?'HCAR020110' . fixLen($hdr['KPBC'], 6) . fixLen($hdr['NOBC10'], 6) . '182' . fixLen($hdr['TGBC10'], 8) . "102\n":'';
        $FF .= ($hdr['NOBC11ASAL']!='')?'HCAR020111' . fixLen($hdr['KPBC'], 6) . fixLen($hdr['NOBC11ASAL'], 6) . '182' . fixLen($hdr['TGBC11ASAL'], 8) . "102\n":'';
        
        #RECORD TYPE HCAR0301
        $FF.= 'HCAR0301CG ' . fixLen($hdr['IDPGUNA'], 1) . fixLen($hdr['NPWPPGUNA'], 15) . fixLen($hdr['NAMAPGUNA'], 40) . fixLen($hdr['ALMTGUNA'], 50) . fixLen($hdr['NAMANAHKODA'], 30) . fixlen($spasi) . "\n";
        
        
        
        #HCAR3A01
        $len = strlen($hdr['NOTE']);
        $start = 0;
        while($len>0){
            $FF.= 'HCAR3A01ADU' . fixLen(substr($hdr['NOTE'],$start,$len), 280) . "\n";
            $start += 280;
            $len -= 280;
        }
        
        #RECORD TYPE HCAR0401
        $FF.= 'HCAR040111 ' . fixLen($hdr['MODA'], 2) . fixLen($hdr['REGKAPAL'], 9) . fixLen($hdr['NMANGKUT'], 35) . fixLen($hdr['NOVOY'], 10) . fixLen($hdr['VOYFLAG'], 2)  . fixLen($hdr['KODEAGEN'], 3) . "\n";
        

        #RECORD TYPE HCAR0501
        $FF.= 'HCAR050141 ' . fixLen($hdr['KPBC'], 6) . "\n";
        $FF.= 'HCAR05019  ' . fixLen($hdr['PELMUAT'], 6) . "\n";
        $FF.= 'HCAR0501127' . fixLen($hdr['PELTRANSIT'], 6) . "\n";
        $FF.= 'HCAR050111 ' . fixLen($hdr['PELBKR'], 6) . fixLen($hdr['KADE'], 6) . "\n";
        $FF.= 'HCAR050161 ' . fixLen($hdr['PELNEXT'], 6) . "\n";
        $FF.= 'HCAR050176 ' . fixLen($hdr['PELMUAT'], 6) . "\n";
        
        #RECORD TYPE HCAR0601 
        if($hdr['JNSMANIFEST']=='O'){
            $FF.= 'HCAR0601133' . fixLen($hdr['TGTIBABERANGKAT'] . str_replace(':', '', $hdr['JAMTIBABERANGKAT']), 12) . '' . "203\n";
        }elseif($hdr['JNSMANIFEST']=='I'){
            $FF.= 'HCAR0601132' . fixLen($hdr['TGTIBABERANGKAT'] . str_replace(':', '', $hdr['JAMTIBABERANGKAT']), 12) . '' . "203\n";
        }
        
        
        #RECORD TYPE HCAR0701
        $FF.= 'HCAR070110 ' . fixLen($hdr['HJMLBL'], 14, '0', STR_PAD_LEFT) . "0000\n";
        $FF.= 'HCAR070116 ' . fixLen($hdr['HJMLCONT'], 14, '0', STR_PAD_LEFT) . "0000\n";
        $FF.= 'HCAR070111 ' . fixLen($hdr['HJMLKEMAS'], 14, '0', STR_PAD_LEFT) . "0000\n";
        $FF.= 'HCAR07017  ' . fixLen($hdr['HBRUTO'] * 10000, 18, '0', STR_PAD_LEFT) . "\n";
        $FF.= 'HCAR070190 ' . fixLen($hdr['HVOLUME'] * 10000, 18, '0', STR_PAD_LEFT) . "\n";
        
        #DATA DETIL
        switch ($hdr['JNSMANIFEST']){
            case'I':$JNSManifest = 'a.INWARD';break;
            case'O':$JNSManifest = 'a.OUTWARD';break;
            default :$JNSManifest = 'a.INWARD';break;
        }
        switch ($hdr['TIPEMANIFEST']){
            case'0':$TIPEManifest = " and a.FTZ = 'N' ";break;
            case'1':$TIPEManifest = " and a.FTZ = 'Y' ";break;
            case'2':$TIPEManifest = '';break;
            default :$TIPEManifest = " and a.FTZ = 'N' ";break;
        }
        
        switch ($hdr['MODA']){
            case '1':$JNSBL = 'BH';$JNSMBL= 'MB';break;
            case '4':$JNSBL = 'AWB';$JNSMBL= 'MWB';break;
        }
        
        #array detil
        $SQL = "SELECT a.*, DATE_FORMAT(a.MTGBL,'%Y%m%d') as MTGBL FROM t_bc11dtl a WHERE a.KODE_TRADER = '" .  $this->kd_trader. "' AND a.CAR = '" . $car . "'";
        $dtl = $this->actMain->get_result($SQL, true);
        foreach ($dtl as $a){
            $arrDtl[$a['KDGRUP']][] = $a;
        }
        
        #array Barang
        $SQL = "SELECT a.* FROM t_bc11brg a WHERE a.KODE_TRADER = '" .  $this->kd_trader. "' AND a.CAR = '" . $car . "'";
        $con = $this->actMain->get_result($SQL, true);
        foreach ($con as $a){
            $arrBrg[$a['KDGRUP']][$a['NOPOS']][$a['NOPOSSUB']][$a['NOPOSSUBSUB']][$a['SERI']]['HS']         = $a['HS'];
            $arrBrg[$a['KDGRUP']][$a['NOPOS']][$a['NOPOSSUB']][$a['NOPOSSUBSUB']][$a['SERI']]['DESCRIPTION']= $a['DESCRIPTION'];                 
        }
        
        #array container
        $SQL = "SELECT a.* FROM t_bc11con a WHERE a.KODE_TRADER = '" .  $this->kd_trader. "' AND a.CAR = '" . $car . "'";
        $con = $this->actMain->get_result($SQL, true);
        foreach ($con as $a){
            $arrCon[$a['KDGRUP']][$a['NOPOS']][$a['NOPOSSUB']][$a['NOPOSSUBSUB']][$a['SERI']]['NOCONT']     = $a['NOCONT'];
            $arrCon[$a['KDGRUP']][$a['NOPOS']][$a['NOPOSSUB']][$a['NOPOSSUBSUB']][$a['SERI']]['UKCONT']     = $a['UKCONT'];
            $arrCon[$a['KDGRUP']][$a['NOPOS']][$a['NOPOSSUB']][$a['NOPOSSUBSUB']][$a['SERI']]['TIPECONT']   = $a['TIPECONT'];
            $arrCon[$a['KDGRUP']][$a['NOPOS']][$a['NOPOSSUB']][$a['NOPOSSUBSUB']][$a['SERI']]['SEALNUM']   = $a['SEALNUM'];
            $arrCon[$a['KDGRUP']][$a['NOPOS']][$a['NOPOSSUB']][$a['NOPOSSUBSUB']][$a['SERI']]['PARTIAL']    = $a['PARTIAL'];
        }
        
        #array Dokumen
        $SQL = "SELECT a.*, DATE_FORMAT(DOKTG,'%Y%m%d') as DOKTG FROM t_bc11dok a WHERE a.KODE_TRADER = '" .  $this->kd_trader. "' AND a.CAR = '" . $car . "'";
        $dok = $this->actMain->get_result($SQL, true);
        foreach ($dok as $a){
            $arrDok[$a['KDGRUP']][$a['NOPOS']][$a['NOPOSSUB']][$a['NOPOSSUBSUB']][$a['SERI']]['DOKKODE']    = $a['DOKKODE'];
            $arrDok[$a['KDGRUP']][$a['NOPOS']][$a['NOPOSSUB']][$a['NOPOSSUBSUB']][$a['SERI']]['DOKNO']      = $a['DOKNO'];
            $arrDok[$a['KDGRUP']][$a['NOPOS']][$a['NOPOSSUB']][$a['NOPOSSUBSUB']][$a['SERI']]['DOKTG']      = $a['DOKTG'];
            $arrDok[$a['KDGRUP']][$a['NOPOS']][$a['NOPOSSUB']][$a['NOPOSSUBSUB']][$a['SERI']]['DOKKPBC']    = $a['DOKKPBC'];
        }
        
        $SQL = "SELECT 	a.KDGRUP, count(b.NOPOS) as 'GJMLPOS', (SELECT COUNT(DISTINCT c.NOCONT) FROM t_bc11con c WHERE c.KODE_TRADER = b.KODE_TRADER AND c.CAR = b.CAR AND c.KDGRUP = b.KDGRUP) as 'GJMLCON', SUM(b.JMLKEMAS) as 'GJMLKMS', SUM(b.BRUTO) as 'GJMLBRT', SUM(b.VOLUME) as 'GJMLVOL' FROM m_manifestgrup a RIGHT JOIN t_bc11dtl b on b.KODE_TRADER = '" . $this->kd_trader . "' AND b.CAR = '" . $car . "' AND a.KDGRUP = b.KDGRUP WHERE " . $JNSManifest . " = 'Y' " . $TIPEManifest . " GROUP BY b.KDGRUP";
        $grp = $this->actMain->get_result($SQL, true);
        $flt = false;
        foreach ($grp as $a){
            $flt = true;
            $FF.= 'DCAR0101' . fixLen($a['KDGRUP'], 2) . "\n";
            $FF.= 'DCAR020110 ' . fixLen($a['GJMLPOS'], 14, '0', STR_PAD_LEFT) . "0000\n";
            $FF.= 'DCAR020116 ' . fixLen($a['GJMLCON'], 14, '0', STR_PAD_LEFT) . "0000\n";
            $FF.= 'DCAR020111 ' . fixLen($a['GJMLKMS'], 14, '0', STR_PAD_LEFT) . "0000\n";
            $FF.= 'DCAR02017  ' . fixLen($a['GJMLBRT'] * 10000, 18, '0', STR_PAD_LEFT) . "\n";
            $FF.= 'DCAR020190 ' . fixLen($a['GJMLVOL'] * 10000, 18, '0', STR_PAD_LEFT) . "\n";
            foreach ($arrDtl[$a['KDGRUP']] as $a){
                $a = str_replace("\n", '', str_replace("\r", '', $a));
                if($a['MBL'] == 'Y'){
                    $FF.= 'DCAR0301' . fixLen($a['NOPOS'], 4, '0', STR_PAD_LEFT) . '0000' . fixLen($JNSMBL, 3) . fixLen($a['MNOBL'], 30) . fixLen($dtl['MTGBL'], 8) . fixLen($a['CONSOLIDATE'], 1) . fixLen($a['PARTIAL'], 1) .  "\n";
                }
                $FF.= 'DCAR0301' . fixLen($a['NOPOS'], 4, '0', STR_PAD_LEFT) . '0000' . fixLen($JNSBL, 3) . fixLen($a['NOBL'], 30) . fixLen($dtl['TGBL'], 8) . fixLen($a['CONSOLIDATE'], 1) . fixLen($a['PARTIAL'], 1) .  "\n";
                $FF.= 'DCAR3A0116 ' . fixLen($a['JMLCONT'], 5, '0', STR_PAD_LEFT) .  "\n";
                if($a['MBL'] == 'Y'){
                    $FF.= 'DCAR3A0150 ' . fixLen($a['MJMLCONT'], 5, '0', STR_PAD_LEFT) .  "\n";
                }
                if($a['PARTIAL'] == 'E'){
                    $FF.= 'DCAR3A0152 ' . fixLen($a['PARTIALJMLCONT'], 5, '0', STR_PAD_LEFT) .  "\n";
                }
                $FF.= 'DCAR04019  ' . fixLen($a['DPELASAL'], 5) .  fixLen($a['DPELSEBELUM'], 5) . "\n";
                $FF.= 'DCAR040110 ' . fixLen($a['DPELBONGKAR'], 5) .  fixLen($a['DPELLANJUT'], 5) . "\n";
                $FF.= 'DCAR040111 ' . fixLen($a['DPELBONGKAR'], 5) .  fixLen($a['DPELLANJUT'], 5) . "\n";
                $FF.= 'DCAR04018  ' . fixLen($a['DPELBONGKAR'], 5) . "\n";
                
                #IIf(Right(rsd!KdGrup, 1) = "E", "22" & Space(1) & "21", "23" & Space(1) & "21") & Space(1) & rsh!kdmoang & " " & vvesselname & vnovoy
                $FF.= 'DCAR0501' . fixLen((substr($a['KDGRUP'],-1)=='E')?'22':'23', 3) . '21 ' . fixLen($hdr['MODA'], 2) . fixLen($hdr['MOTHERVESSEL'], 35) . '                 ' . "\n";
                #pengirim
                $len = strlen($a['NMPENGIRIM']);
                $start = 0;
                while($len>0){
                    $FF.= 'DCAR0601CZ NAM' . fixLen(substr($a['NMPENGIRIM'],$start,$len), 172) . "\n";
                    $start += 172;
                    $len -= 172;
                }
                $len = strlen($a['ALPENGIRIM']);
                $start = 0;
                while($len>0){
                    $FF.= 'DCAR0601CZ ADD' . fixLen(substr($a['ALPENGIRIM'],$start,$len), 172) . "\n";
                    $start += 172;
                    $len -= 172;
                }
                #penerima
                $len = strlen($a['NMPENERIMA']);
                $start = 0;
                while($len>0){
                    $FF.= 'DCAR0601CN NAM' . fixLen(substr($a['NMPENERIMA'],$start,$len), 172) . "\n";
                    $start += 172;
                    $len -= 172;
                }
                $len = strlen($a['ALPENERIMA']);
                $start = 0;
                while($len>0){
                    $FF.= 'DCAR0601CN ADD' . fixLen(substr($a['ALPENERIMA'],$start,$len), 172) . "\n";
                    $start += 172;
                    $len -= 172;
                }
                #Pemberitahu
                $len = strlen($a['NMPEMBERITAU']);
                $start = 0;
                while($len>0){
                    $FF.= 'DCAR0601NI NAM' . fixLen(substr($a['NMPEMBERITAU'],$start,$len), 172) . "\n";
                    $start += 172;
                    $len -= 172;
                }
                $len = strlen($a['ALPEMBERITAU']);
                $start = 0;
                while($len>0){
                    $FF.= 'DCAR0601NI ADD' . fixLen(substr($a['ALPEMBERITAU'],$start,$len), 172) . "\n";
                    $start += 172;
                    $len -= 172;
                }
                
                $FF.= 'DCAR0701' . fixLen($a['JNSKEMAS'],2) . fixLen($a['JMLKEMAS'], 8, '0', STR_PAD_LEFT) . fixLen($a['MJNSKEMAS'],2) . fixLen($a['MJMLKEMAS'], 8, '0', STR_PAD_LEFT) . fixLen($a['PARTIALJNSKEMAS'],2) . fixLen($a['PARTIALJNSKEMAS'], 8, '0', STR_PAD_LEFT) . "\n";
                
                foreach ($arrBrg[$a['KDGRUP']][$a['NOPOS']][$a['NOPOSSUB']][$a['NOPOSSUBSUB']] as $brg){
                    $FF.= 'DCAR7A01AAH' . fixLen($brg['HS'], 350) . "\n";
                    $len = strlen($brg['DESCRIPTION']);
                    $start = 0;
                    while($len>0){
                        $FF.= 'DCAR7A01AAA' . fixLen(substr($brg['DESCRIPTION'],$start,$len), 350) . "\n";
                        $start  += 350;
                        $len    -= 350;
                    }
                }
                $FF.= 'DCAR0801EGW' . fixLen($a['BRUTO'] * 10000, 18, '0', STR_PAD_LEFT) . "KGM\n";
                $FF.= 'DCAR0801VOL' . fixLen($a['VOLUME'] * 10000, 18, '0', STR_PAD_LEFT) . "M3\n";
                foreach ($arrCon[$a['KDGRUP']][$a['NOPOS']][$a['NOPOSSUB']][$a['NOPOSSUBSUB']] as $con){
                    switch ($con['TIPECONT']){
                        case'F':$tipeCon = '8';break;
                        case'L':$tipeCon = '7';break;
                        case'E':$tipeCon = '4';break;
                        default:$tipeCon = '7';break;
                    }
                    $FF.= 'DCAR0901' . (($hdr['MODA']=='1')?'CN ':'PBP') . fixLen($con['NOCONT'], 30) . fixLen($con['UKCONT'], 2) . fixLen($tipeCon, 1) . fixLen($con['SEALNUM'], 15) . fixLen($con['PARTIAL'], 2) . "\n";
                }
                $len = strlen($a['MERKKEMAS']);
                $start = 0;
                while($len>0){
                    $FF.= 'DCAR100128 ' . fixLen(substr($a['MERKKEMAS'],$start,$len), 240) . "\n";
                    $start  += 240;
                    $len    -= 240;
                }
                foreach ($arrDok[$a['KDGRUP']][$a['NOPOS']][$a['NOPOSSUB']][$a['NOPOSSUBSUB']] as $dok){
                    switch ($dok['DOKKODE']){
                        case'1':$unEDI = '913';break;   #BC30
                        #case'2':$unEDI = '';break;     #SAD tidak ada di PIA dimasukkan ke dokuem lain lain
                        case'3':$unEDI = '831';break;   #PEB Berkala
                        #case'4':$unEDI = '999';break;  #Dokumen Lain2
                        case'5':$unEDI = '785';break;   #BC11
                        case'6':$unEDI = '950';break;   #BC12
                        case'7':$unEDI = '830';break;   #PKBE
                        default:$unEDI = '999';break;
                    }
                    $FF.= 'DCAR1101ZZZ' . fixLen($unEDI, 3) . fixLen($dok['DOKKPBC'], 6) . fixLen($dok['DOKNO'], 6) . fixLen($dok['DOKTG'], 8) . "182\n";
                }
            }
            $FF.= "DCAR1101END\n";
        }
        if($flt == true){$FF.= "UNZ00101\n";}
        $fullPaht = 'FLAT/' . $EDINUMBER;
        if (!is_dir($fullPaht)) {
            mkdir($fullPaht, 0777, true);
        }
        $fullPaht = $fullPaht . '/' . $car . '.FLT';
        if (file_exists($fullPaht)) {
            unlink($fullPaht);
        }
        $handle = fopen($fullPaht, 'w');
        fwrite($handle, $FF);
        fclose($handle);
        if (file_exists($fullPaht)) {
            $hsl = '2|' . $hdr['KPBC'];
        } else {
            $hsl = '0|';
        }
        return $hsl;
    }
    
    function crtQueue($car) {
        $this->load->model('actTranslator');
        $SNRF       = date('ymdHis') . rand(10, 99);
        $EDINUMBER  = $this->newsession->userdata('EDINUMBER');
        $EDIPaht    = '/home/dev/t2g/EDI/' . $EDINUMBER;
        if (!is_dir($EDIPaht)){
            mkdir($EDIPaht,0777,true);
        }else{
            chmod($EDIPaht,0777);
        }
        $FLTPaht    = 'FLAT/' . $EDINUMBER;
        if (!is_dir($FLTPaht)){
            mkdir($FLTPaht,0777,true);
        }else{
            chmod($FLTPaht,0777);
        }
        $this->actTranslator->getEDIFILE($EDINUMBER,'DOKMANIF',$SNRF,$car.'.FLT');
        if (file_exists($EDIPaht.'/'. $car .'.EDI') && file_exists($FLTPaht.'/'. $car .'.FLT')){
            $dirEdiNumber = 'TRANSACTION/' . $EDINUMBER . '/' . date('Ymd');
            if (!is_dir($dirEdiNumber)){mkdir($dirEdiNumber,0777,true);}
            rename($FLTPaht.'/'. $car .'.FLT', $dirEdiNumber . '/' . $car . '.FLT');
            rename($EDIPaht.'/'. $car .'.EDI', $dirEdiNumber . '/' . $car . '.EDI');
            $rtn = $SNRF . '|' . $dirEdiNumber . '/' . $car . '.EDI' . '|' . $dirEdiNumber . '/' . $car . '.FLT';
        }
        else {
            $rtn = 'ERROR';
        }
        return $rtn;
    }
    
    function updateSNRF($car, $data) {
        $this->load->model('actMain');
        $arrData = explode('|', $data);
        $dataHdr['SNRF'] = $arrData[0];
        $dataHdr['DIRFLT'] = $arrData[2];
        $dataHdr['DIREDI'] = $arrData[1];
        $dataHdr['STATUS'] = '02';
        $dataHdr['CAR'] = $car;
        $dataHdr['KODE_TRADER'] = $this->kd_trader;
        $this->actMain->insertRefernce('t_bc11hdr', $dataHdr);

        #insert m_trader_dokoutbound
        $dataDOK['KODE_TRADER'] = $this->kd_trader;
        $dataDOK['KDKPBC']      = $arrData[3];
        $dataDOK['JNSDOK']      = 'BC11';
        $dataDOK['APRF']        = 'DOKMANIF';
        $dataDOK['CAR']         = $car;
        $dataDOK['STATUS']      = '00';
        $this->actMain->insertRefernce('m_trader_dokoutbound', $dataDOK);
        return '0';
    }
    
    function getDataLoad(){
        $this->load->model('actMain');
        $sql = "SELECT a.KODE_ID as 'IDPGUNA', "
                       . " a.ID as 'NPWPPGUNA', "
                       . " a.NAMA as 'NAMAPGUNA',"
                       . " a.ALAMAT as 'ALMTGUNA',"
                       . " b.KODEAGEN as 'KODEAGEN' "
                 . "FROM m_trader a LEFT JOIN m_trader_bc10 b on a.KODE_TRADER = b.KODE_TRADER "
                 . "WHERE a.KODE_TRADER = '" . $this->kd_trader . "'";
        $rtn['DATA'] = $this->actMain->get_result($sql);
        $rtn['DATA']['JNSMANIFEST']  = 'I';
        $rtn['DATA']['TIPEMANIFEST'] = '0';
        $rtn['IDPGUNA']     = $this->actMain->get_mtabel('JENIS_IDENTITAS', 1, false, '', 'KODEDANUR', 'BC10');
        $rtn['MODA']        = $this->actMain->get_mtabel('MODA', 1, false, '', 'KODEDANUR', 'BC11');
        
        return $rtn;
    }
    
    function uploadFLT(){
        $element = 'fileFLT';
        if ($element != "") {
            $path = $_FILES[$element]['name'];
            $ftype = pathinfo($path, PATHINFO_EXTENSION);
            if (!empty($_FILES[$element]['error'])){
                switch ($_FILES[$element]['error']){
                    case'1':$error = "ERROR~|~Ukuran File Terlalu Besar.";break;
                    case'3':$error = "ERROR~|~File Yang Ter-Upload Tidak Sempurna.";break;
                    case'4':$error = "ERROR~|~File Kosong Atau Belum Dipilih.";break;
                    case'6':$error = "ERROR~|~Direktori Penyimpanan Sementara Tidak Ditemukan.";break;
                    case'7':$error = "ERROR~|~File Gagal Ter-Upload.";break;
                    case'8':$error = "ERROR~|~Proses Upload File Dibatalkan.";break;
                    default :$error = "ERROR~|~Pesan Error Tidak Ditemukan.";break;
                }
            }else if (empty($_FILES[$element]['tmp_name']) || ($_FILES[$element]['tmp_name'] == 'none')) {
                $error = "ERROR~|~File Gagal Ter-Upload.";
            }else if(strtolower($ftype) != 'flt') {
                $error = "ERROR~|~Tipe File Salah.<br>Tipe File Yang Diterima : *.FLT";
            }else {
                $filename = 'uploads/' . $this->kd_trader . '.FLT';
                chmod($filename,0777);
                @unlink($filename);
                if(move_uploaded_file($_FILES[$element]['tmp_name'], $filename)){
                    $error = 'OKE~|~'.$this->readHeaderFLT($filename);
                }else{
                    $error = "ERROR~|~File Gagal Ter-Upload.";
                }
            }
        }else{
            $error = "ERROR~|~Parameter Tidak Ditemukan.";
        }
        @unlink($_FILES[$element]);
        return $error;
    }
    
    private function readHeaderFLT($ditFLT){
        $arrFILE    = file($ditFLT);
        $data       = $arrFILE[0];
        if(substr($data,0,5)=='HDR01'){
            $rtn['NMANGKUT']        = trim(substr($data, 9, 40));
            $rtn['REGKAPAL']        = trim(substr($data, 49, 10));
            $rtn['VOYFLAG']         = trim(substr($data, 59, 2));
            $rtn['NOVOY']           = trim(substr($data, 61, 10));
            $rtn['PELMUAT']         = trim(substr($data, 74, 5));
            $rtn['PELTRANSIT']      = trim(substr($data, 79, 5));
            $rtn['PELBKR']          = trim(substr($data, 84, 5));
            $rtn['PELNEXT']         = trim(substr($data, 89, 5));
            $rtn['TGTIBABERANGKAT'] = trim(substr($data, 100, 2)) . '-' . trim(substr($data, 98, 2)) . '-' . trim(substr($data, 94, 4)); 
            $rtn['JAMTIBABERANGKAT']= trim(substr($data, 102, 2)) . ':' . trim(substr($data, 104, 2)); 
            foreach($rtn as $k=>$v){
                $msg .= '|-|' . $k .'|;|' . $v;
            }
            $msg = substr($msg,3);
        }
        return $msg;
    }
    
    function insertFLT($data){
        $this->load->model('actMain');
        $filename = 'uploads/' . $this->kd_trader . '.FLT';
        if(is_readable($filename)){
            $lines  = file($filename);
            $car = $data['car'];
            foreach($lines as $line){
                $lbl = substr($line, 0, 5);
                switch ($lbl){
                    case'HDR01':
                        if($data['jnsImport'] == 'baru'){
                            #create car
                            $car = $this->actMain->get_car('BC10');
                            $updateCar = true ;
                            $sql = "SELECT a.KODE_ID as 'IDPGUNA', a.ID as 'NPWPPGUNA', a.NAMA as 'NAMAPGUNA', a.ALAMAT as 'ALMTGUNA',"
                                       . " b.KODEAGEN as 'KODEAGEN' "
                                  . "FROM m_trader a LEFT JOIN m_trader_bc10 b on a.KODE_TRADER = b.KODE_TRADER "
                                  . "WHERE a.KODE_TRADER = '" . $this->kd_trader . "'";
                            $hdr                    = $this->actMain->get_result($sql);
                            $hdr['KODE_TRADER']     = $this->kd_trader;
                            $hdr['CAR']             = $car;
                            $hdr['NMANGKUT']        = trim(substr($line, 9, 40));
                            $hdr['REGKAPAL']        = trim(substr($line, 49, 10));
                            $hdr['VOYFLAG']         = trim(substr($line, 59, 2));
                            $hdr['NOVOY']           = trim(substr($line, 61, 10));
                            $hdr['PELMUAT']         = trim(substr($line, 74, 5));
                            $hdr['PELTRANSIT']      = trim(substr($line, 79, 5));
                            $hdr['PELBKR']          = trim(substr($line, 84, 5));
                            $hdr['PELNEXT']         = trim(substr($line, 89, 5));
                            $hdr['TGTIBABERANGKAT'] = trim(substr($line, 100, 2)) . '-' . trim(substr($data, 98, 2)) . '-' . trim(substr($data, 94, 4)); 
                            $hdr['JAMTIBABERANGKAT']= trim(substr($line, 102, 4));
                            $exec = $this->actMain->insertRefernce('t_bc11hdr', $hdr);
                            if($exec && $updateCar){$this->actMain->set_car('BC10');}
                        }else{
                            $sql = "SELECT a.KDGRUP, MAX(a.NOPOS) as 'NOPOS' FROM t_bc11dtl a WHERE a.KODE_TRADER = '" . $this->kd_trader . "' AND a.CAR = '" . $car . "' GROUP BY a.KDGRUP";
                            $arrData = $this->actMain->get_result($sql,true);
                            foreach ($arrData as $a){
                                $maxNOPOS[$a['KDGRUP']] = $a['NOPOS'];
                            }
                        }
                        #insert hdr
                        $adaDetil = false;
                        break;
                    case'DTL01':
                        $i++;
                        $adaDetil               = true;
                        $kdpos                  = substr($line, 5, 3);
                        $dtl[$i]['KODE_TRADER'] = $this->kd_trader;
                        $dtl[$i]['CAR']         = $car;
                        $dtl[$i]['KDGRUP']      = $kdpos;
                        $dtl[$i]['NOPOS']       = ++$maxNOPOS[$kdpos];
                        $dtl[$i]['NOPOSSUB']    = '0';
                        $dtl[$i]['NOPOSSUBSUB'] = '0';
                        $dtl[$i]['DPELASAL']    = substr($line, 19, 5);
                        $dtl[$i]['DPELBONGKAR'] = substr($line, 29, 5);
                        $dtl[$i]['DPELSEBELUM'] = substr($line, 24, 5);
                        $dtl[$i]['DPELLANJUT']  = substr($line, 34, 5);
                        $dtl[$i]['MOTHERVESSEL']= substr($line, 39, 40);
                        $dtl[$i]['NOBL']        = substr($line, 89, 30);
                        $dtl[$i]['TGBL']        = substr($line, 119, 4) . '-' . substr($line, 123, 2) . '-' . substr($line, 125, 2);
                        $MNoBL                  = trim(substr($line, 127, 30));
                        $MTgBL                  = substr($line, 157, 4) . '-' . substr($line, 161, 2) . '-' . substr($line, 163, 2);
                        If($MNoBL != ''){
                            $dtl[$i]['MNOBL']   = $MNoBL;
                            $dtl[$i]['MTGBL']   = $MTgBL;
                            $dtl[$i]['MJMLCONT']= substr($line, 216, 5)+0;
                            $dtl[$i]['MJMLKEMAS']=substr($line, 221, 5)+0;
                        }
                        $dtl[$i]['BRUTO']       = substr($line, 167, 14) . '.' . substr($line, 181, 4)+0;
                        $dtl[$i]['VOLUME']      = substr($line, 185, 14) . '.' . substr($line, 199, 4)+0;
                        $dtl[$i]['JMLCONT']     = substr($line, 203, 5)+0;
                        $dtl[$i]['JMLKEMAS']    = substr($line, 208, 8)+0;
                        $dtl[$i]['JNSKEMAS']    = substr($line, 242, 2);
                        break;
                    case'DTL02':
                        $jnsDtl = substr($line, 5, 3);
                        switch ($jnsDtl){
                            case'SNM':$dtl[$i]['NMPENGIRIM']    .= trim(substr($line, 10, 200));break;
                            case'SNA':$dtl[$i]['ALPENGIRIM']    .= trim(substr($line, 10, 200));break;
                            case'CNM':$dtl[$i]['NMPENERIMA']    .= trim(substr($line, 10, 200));break;
                            case'CNA':$dtl[$i]['ALPENERIMA']    .= trim(substr($line, 10, 200));break;
                            case'NNM':$dtl[$i]['NMPEMBERITAU']  .= trim(substr($line, 10, 200));break;
                            case'NNA':$dtl[$i]['ALPEMBERITAU']  .= trim(substr($line, 10, 200));break;
                            case'SMR':$dtl[$i]['MERKKEMAS']     .= trim(substr($line, 10, 200));break;
                            case'HSC':
                                $ii++;
                                $brg[$ii]['KODE_TRADER']    = $this->kd_trader;
                                $brg[$ii]['CAR']            = $car;
                                $brg[$ii]['KDGRUP']         = $kdpos;
                                $brg[$ii]['NOPOS']          = $maxNOPOS[$kdpos];
                                $brg[$ii]['NOPOSSUB']       = '0';
                                $brg[$ii]['NOPOSSUBSUB']    = '0';
                                $brg[$ii]['SERI']           = ++$seri['BRG'][$brg[$ii]['NOPOS']];
                                $brg[$ii]['HS']             = substr($line, 10, 12);
                            break;
                            case'DES':$brg[$ii]['DESCRIPTION']  .= trim(substr($line, 10, 200));break;
                        }
                        break;
                    case'CNT01':
                        $iii++;
                        $con[$iii]['KODE_TRADER']   = $this->kd_trader;
                        $con[$iii]['CAR']           = $car;
                        $con[$iii]['KDGRUP']        = $kdpos;
                        $con[$iii]['NOPOS']         = $maxNOPOS[$kdpos];
                        $con[$iii]['NOPOSSUB']      = '0';
                        $con[$iii]['NOPOSSUBSUB']   = '0';
                        $con[$iii]['SERI']          = ++$seri['CON'][$con[$iii]['NOPOS']];
                        $con[$iii]['NOCONT']        = substr($line, 9, 20);
                        $con[$iii]['UKCONT']        = substr($line, 34, 2);
                        $con[$iii]['TIPECONT']      = substr($line, 36, 1);
                        $con[$iii]['SEALNUM']       = substr($line, 39, 15);
                        break;
                    case'DOK01':
                        $iv++;
                        $dok[$iv]['KODE_TRADER']    = $this->kd_trader;
                        $dok[$iv]['CAR']            = $car;
                        $dok[$iv]['KDGRUP']         = $kdpos;
                        $dok[$iv]['NOPOS']          = $maxNOPOS[$kdpos];
                        $dok[$iv]['NOPOSSUB']       = '0';
                        $dok[$iv]['NOPOSSUBSUB']    = '0';
                        $dok[$iv]['SERI']           = ++$seri['DOK'][$dok[$iv]['NOPOS']];
                        $DOKKODE = substr($line, 5,3);
                        switch ($DOKKODE){
                            case'PEB':$dok[$iv]['DOKKODE']='1';break;
                            case'SAD':$dok[$iv]['DOKKODE']='2';break;
                            case'PBB':$dok[$iv]['DOKKODE']='3';break;
                            default:  $dok[$iv]['DOKKODE']='4';break;
                        }
                        $dok[$iv]['DOKNO']          = substr('000000' . substr($line, 17,6),-6);
                        $dok[$iv]['DOKTG']          = substr($line, 23,4) . '-' . substr($line, 27,2) . '-' . substr($line, 29,2);
                        $dok[$iv]['DOKKPBC']        = substr($line, 11,6);
                        break;
                }
            }
            if(count($dtl)>0){
                $key        = array_keys($dtl[1]);
                $keyGabung  = implode(',',$key);
                $sql        = 'INSERT INTO t_bc11dtl (' . $keyGabung . ') VALUES ';
                $value      = '';
                foreach($dtl as $a){
                    $a = array_map('trim', $a);
                    $a = str_replace("'", "''", $a);
                    $field = '';
                    foreach($key as $k){
                        $field .= ",'" .  $a[$k] . "'";
                    }
                    $value .= ', (' . substr($field,1) . ')';
                }
                $sql        = $sql . substr($value,1);
                $this->db->simple_query($sql);
            }
            if(count($brg)>0){
                $key        = array_keys($brg[1]);
                $keyGabung  = implode(',',$key);
                $sql        = 'INSERT INTO t_bc11brg (' . $keyGabung . ') VALUES ';
                $value      = '';
                foreach($brg as $a){
                    $a = array_map('trim', $a);
                    $a = str_replace("'", "''", $a);
                    $field = '';
                    foreach($key as $k){
                        $field .= ",'" .  $a[$k] . "'";
                    }
                    $value .= ', (' . substr($field,1) . ')';
                }
                $sql        = $sql . substr($value,1);
                $this->db->simple_query($sql);
            }
            if(count($con)>0){
                $key        = array_keys($con[1]);
                $keyGabung  = implode(',',$key);
                $sql        = 'INSERT INTO t_bc11con (' . $keyGabung . ') VALUES ';
                $value      = '';
                foreach($con as $a){
                    $a = array_map('trim', $a);
                    $a = str_replace("'", "''", $a);
                    $field = '';
                    foreach($key as $k){
                        $field .= ",'" .  $a[$k] . "'";
                    }
                    $value .= ', (' . substr($field,1) . ')';
                }
                $sql        = $sql . substr($value,1);
                $this->db->simple_query($sql);
            }
            if(count($dok)>0){
                $key        = array_keys($dok[1]);
                $keyGabung  = implode(',',$key);
                $sql        = 'INSERT INTO t_bc11dok (' . $keyGabung . ') VALUES ';
                $value      = '';
                foreach($dok as $a){
                    $a = array_map('trim', $a);
                    $a = str_replace("'", "''", $a);
                    $field = '';
                    foreach($key as $k){
                        $field .= ",'" .  $a[$k] . "'";
                    }
                    $value .= ', (' . substr($field,1) . ')';
                }
                $sql        = $sql . substr($value,1);
                $this->db->simple_query($sql);
            }
            #resume
            
            $sql = "SELECT COUNT(b.NOPOS) as 'HJMLBL', SUM(b.JMLKEMAS) as 'HJMLKEMAS', SUM(b.BRUTO) as 'HBRUTO', SUM(b.VOLUME) as 'HVOLUME', (SELECT COUNT(DISTINCT c.NOCONT) FROM t_bc11con c WHERE c.KODE_TRADER = a.KODE_TRADER AND c.CAR = a.CAR) as 'HJMLCONT' FROM t_bc11hdr a left join t_bc11dtl b on a.KODE_TRADER = b.KODE_TRADER AND a.CAR = b.CAR WHERE a.KODE_TRADER = '" . $this->kd_trader . "' AND b.CAR = '" . $car . "' GROUP BY a.KODE_TRADER , b.CAR";
            $hdr = $this->actMain->get_result($sql);
            $hdr['KODE_TRADER']     = $this->kd_trader;
            $hdr['CAR']             = $car;
            $hdr['STATUS']          = '00';
            $exec = $this->actMain->insertRefernce('t_bc11hdr', $hdr);
            if($exec && $updateCar){$this->actMain->set_car('BC10');}
            
        }
        return 'oke';
    }
    
    function delDetil($data){
        $arr = explode('|', $data[0]);
        $car            = $arr[0];
        $kdgrup         = $arr[1];
        $nopos          = $arr[2];
        $nopossub       = $arr[3];
        $nopossubsub    = $arr[4];
        $ekspor         = $arr[5];
        $status         = $arr[6];
        if($status == '00' || $status == '01'){
             $this->db->where(array( 'CAR' => $car, 
                                'KDGRUP' => $kdgrup, 
                                'KODE_TRADER' => $this->kd_trader, 
                                'NOPOS' => $nopos, 
                                'NOPOSSUB' => $nopossub, 
                                'NOPOSSUBSUB' => $nopossubsub));
            $tables = array('t_bc11dok', 't_bc11con', 't_bc11brg', 't_bc11dtl');
            $this->db->delete($tables);
            $sql    = "call bc11UrutkanSerial(" . $this->kd_trader . ",'" . $car . "','"  . $kdgrup . "'," . $nopos . ")";
            $exec   = $this->db->query($sql);#$car, $kdgrup, $ekspor
            return 'MSG|OK|' . site_url('bc11/lstPost/' . $car . '/' . $kdgrup . '/' . $ekspor) . '|Dalete data detil berhasil.';
        }else{
            return 'MSG|ER||Dalete data detil gagal.<br>Proses delete hanya pada status Edit atau Ready.';
        }
       
    }
    
    function delHeader($data){
        $arr = explode('|', $data[0]);
        if($arr[1] != '02' && $arr[1] != '03' && $arr[1] != '04' && $arr[1] != '07'){
            $this->db->where(array( 'CAR' => $arr[0], 'KODE_TRADER' => $this->kd_trader));
            $tables = array('t_bc11dok', 't_bc11con', 't_bc11brg', 't_bc11dtl', 't_bc11hdr');
            $this->db->delete($tables);
            return 'MSG|OK|' . site_url('bc11/lstBC11') . '|Dokumen berhasil dihapus.';
        }else{
            return 'MSG|ER||Dokumen tidak dapat dihapus.<br>Status dokumen sedang transaksi.';
        }
    }
}
