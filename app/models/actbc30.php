<?php
if (!defined('BASEPATH')){exit('No direct script access allowed');}
class actBC30 extends CI_Model {
    var $kd_trader;
    function __construct(){
        parent::__construct();
        $this->kd_trader = $this->newsession->userdata('KODE_TRADER');
        return true;
    }
    
    function lsvHeaderPKBE($tipe='PKBE'){
        $this->load->library('newtable');
        $tipe = 'hdrPKBE';
        $this->load->model("actMain");
        $SQL = "SELECT  concat(SUBSTRING(A.CAR,1,6),'-',SUBSTRING(A.CAR,7,6),'-',SUBSTRING(A.CAR,13,8),'-',SUBSTRING(A.CAR,21,8))  AS 'C A R',
                        A.CAR,
                        A.KDNEGTUJU AS 'Nama Kapal', 
                        A.CARRIER as 'Tujuan', 
                        B.URAIAN as 'Status',A.KODE_TRADER FROM T_BC30PKBEHDR A
                LEFT JOIN m_tabel B ON B.KDREC = A.STATUS AND B.KDTAB = 'STATUS' AND B.MODUL = 'BC30'
                WHERE KODE_TRADER= '".  $this->kd_trader ."'";
        $prosesnya = array(
            'Create'        => array('ADDAJAX', site_url('bc30/createPKBE'), '0', '_content_'),
            'View'          => array('GETAJAX', site_url('bc30/createPKBE'), '1', '_content_'),
            'Print Dokumen' => array('GETPOP', site_url('bc30/beforecetakPKBE'), '1', ' CETAK DOKUMEN PKBE |400|200'),
            'Upload BUE PKBE'    => array('GETPOP', site_url('bc30/uploadbuepkbe/viewform'), '0', ' UPLOAD FILE .BUE |400|200'),  
            'Download BUE PKBE'  => array('DOWNLOADAJAX', site_url('bc30/downloadBUEpkbe/download'), '1', ' DOWNLOAD FILE .BUE |400|200'),
            'Hapus '        => array('DELETEAJAX', site_url('bc30/delPKBEhdr'), 'N', '_content_'));
        $this->newtable->menu($prosesnya);
        $this->newtable->keys(array('CAR'));
        $this->newtable->hiddens(array('KODE_TRADER','CAR'));
        $this->newtable->search(array(
            array('CAR',        'No. Aju'),
            array('a.KATEKS',   'Katagori Ekspor', 'tag-select', $this->actMain->get_mtabel('KATEKS',1,false,'','KODEUR','BC30')),
            array('a.NAMABELI', 'Nama Pembeli'),
            array('a.ANGKUTNAMA','Nama Moda'),
            array('a.STATUS', 'Status', 'tag-select', $this->actMain->get_mtabel('STATUS',1,false,'','KODEUR','BC30'))));
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->action(site_url("bc30/daftarPKBE"));
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->orderby(1, 2);
        $this->newtable->sortby("DESC");
        $this->newtable->tipe_check('radio');
        $this->newtable->set_formid("f".$tipe);
        $this->newtable->set_divid("div".$tipe);
        $this->newtable->rowcount(20);
        $this->newtable->tipe_proses('button');
        $this->newtable->clear();
        $tabel .= $this->newtable->generate($SQL);
        $arrdata = array('tabel' => $tabel,'tipe' => $tipe);
        if ($this->input->post("ajax")){
            return $tabel;
        }
        else{
            return $arrdata;
        }
    }        
    
    function detailPKBE($car="", $KDKTR=""){
        //print_r($KDKTR);die('tset');
        $this->load->library('newtable');
        $tipe = 'dtlPKBE';
        $this->load->model("actMain");
        $SQL = "SELECT  KATEKS AS 'Ket. Eks', 
                        KDKTRPEB AS 'KPBC', 
                        NOPEB AS 'No. PEB', 
                        TGPEB AS 'Tgl. PEB', 
                        JNDOKEKSPOR AS 'Dok', 
                        NOPE AS 'Nomor PE/PPB', 
                        TGPE AS 'TgL. PE/PPB', 
                        EXMERAH AS 'Trans', 
                        KETERANGAN AS 'Keterangan',
                        CAR, 
                        SERICONT, 
                        SERIPE,
                        KODE_TRADER
                        FROM T_BC30PKBEDTL
                        WHERE CAR='" . $car . "' AND KODE_TRADER='" . $this->kd_trader . "'";
        $prosesnya = array(
            'Tambah' => array('GETPOP', site_url('bc30/insert_detail_pkbe/viewform/'.$car.'/-/-/'.$KDKTR), '0', ' Tambah |900|500'),
            'View' => array('GETPOP', site_url('bc30/insert_detail_pkbe/viewform'), '1', ' Tambah |900|700'),
            'Hapus '    => array('DELETEAJAX', site_url('bc30/insert_detail_pkbe/delete'), 'N', 'detilPKBE')
            //'Create'  => array('ADDAJAX', site_url('bc30/create'), '0', '_content_'),
            //'View'  => array('GETAJAX', site_url('bc30/createPKBE'), '1', '_content_')
            );
        $this->newtable->menu($prosesnya);
        $this->newtable->keys(array('CAR','SERICONT','SERIPE','KODE_TRADER'));
        $this->newtable->hiddens(array('CAR','SERICONT','SERIPE','KODE_TRADER'));
        $this->newtable->search(array(
            array('NOPEB',        'No. PEB'),
            array('KDKTRPEB',   'Kode Kantor')));
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->action(site_url('bc30/lstDetilPKBE/' . $car));
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->orderby(1, 2);
        $this->newtable->sortby("DESC");
        $this->newtable->set_formid("f".$tipe);
        $this->newtable->set_divid("div".$tipe);
        $this->newtable->rowcount(20);
        $this->newtable->tipe_proses('button');
        $this->newtable->clear();
        $tabel .= $this->newtable->generate($SQL);
        $arrdata = array('tabel' => $tabel,'tipe' => $tipe);
        if ($this->input->post("ajax")){
            return $tabel;
        }
        else{
            return $arrdata;
        }
    }    
    
    function getHeaderPKBE($car=""){
        $this->load->model("actMain");
        
        if ($car) {
            $SQL = "SELECT  A.*, 
                            B.NOCONT, 
                            B.SIZE, 
                            C.URAIAN as 'URSTATUS', 
                            D.URAIAN_KPBC AS 'UKDKTR',
                            E.URAIAN_KPBC AS 'UKDKTRMUAT',
                            F.URAIAN_KPBC AS 'UKDKTRPERIKSA',
                            G.URAIAN_NEGARA AS 'UKDNEGTUJU',
                            H.URAIAN_NEGARA AS 'UBENDERA',
                            I.URHGR AS 'URHGR'
                            FROM t_bc30PKBEhdr A 
                    LEFT JOIN t_bc30pkbecon B ON A.CAR = B.CAR
                    LEFT JOIN m_tabel C ON C.KDREC = A.STATUS AND C.KDTAB = 'STATUS' AND C.MODUL = 'BC30'
                    LEFT JOIN m_kpbc D ON A.KDKTR = D.KDKPBC
                    LEFT JOIN m_kpbc E ON A.KDKTRMUAT = E.KDKPBC
                    LEFT JOIN m_kpbc F ON A.KDKTRPERIKSA = F.KDKPBC
                    LEFT JOIN m_negara G ON A.KDNEGTUJU = G.KODE_NEGARA
                    LEFT JOIN m_negara H ON A.BENDERA = H.KODE_NEGARA
                    LEFT JOIN m_hanggar I ON A.KDHANGGAR = I.KDHGR	and I.KDHGR = A.KDHANGGAR		
                    WHERE A.CAR='" . $car . "' AND A.KODE_TRADER='" . $this->kd_trader . "'";
            $data['HEADER'] = $this->actMain->get_result($SQL);
            $data['act']    = 'update';// array('act' => 'update');
        }
        else{
            $sql = "SELECT 	a.KODE_ID as 'IDEKS', 
			a.ID as 'NPWPEKS', 
			a.NAMA as 'NAMAEKS', 
			a.ALAMAT as 'ALMTEKS', 
			b.NIPER, 
			b.STATUSH
                    FROM m_trader a left join m_trader_bc30 b on a.KODE_TRADER = b.KODE_TRADER
                    WHERE a.KODE_TRADER = '".$this->kd_trader."'";
            $data['HEADER'] = $this->actMain->get_result($sql);
            $data['act']    = 'save';
        }
        $data ['DETIL']        = $this->actBC30->getDetailPKBE($car,$data['HEADER']['KDKTR']);
        $data ['IDEKS']        = $this->actMain->get_mtabel('JENIS_IDENTITAS', 1, false, '', 'KODEDANUR','BC30');
        $data ['MODA']         = $this->actMain->get_mtabel('MODA', 1, false,'','KODEDANUR','BC30');
        $data ['SIZE_CONT']    = $this->actMain->get_mtabel('SIZE_CONT', 1, false, '', 'KODEDANUR','BC30');
        //print_r($data ['SIZE_CONT']);die();
        $data ['STATUS_KONS']  = $this->actMain->get_mtabel('STATUS_KONS', 1, false, '', 'KODEDANUR','BC30');
        $data ['MAXLENGTH']    = '<input type="hidden" id="MAXLENGTH" value="' . $this->actMain->get_uraian("SELECT F_MAXLENGTH('t_bc30PKBEhdr') as MAXLENGTH", 'MAXLENGTH') . '"/>';
////COMBO DI CONTROLER, AMBIL JENIS TAB DI M_TABLE DAN KODE BC30
        //print_r($data);die();
        return $data;
    }
    
    function getDetailPKBE($car, $KDKTR) {
        $arrdata = $this->detailPKBE($car, $KDKTR);
        $data = $this->load->view('list', $arrdata, true);
        return $data;
    }
    
    function setDetailPKBEin($act) {
        $this->load->model('actMain');
        $data = $this->input->post('DETPKBE');
        $car  = $data['CAR'];
        $KDKTR  = $data['KDKTR'];
        
        $data['SERICONT'] = '1';
        if($data['SERIPE']==''){
            $sql = "SELECT ifnull(max(a.SERIPE)+1,1) as JML 
                    FROM t_bc30pkbedtl a 
                    WHERE a.KODE_TRADER = '".$this->kd_trader."' AND a.CAR = '".$car."' AND a.SERICONT = '1'";
            $data['SERIPE'] = $this->actMain->get_uraian($sql,'JML');
        }
        
        if($data['KDKTRPEB'] != $data['KDKTR']){
             $data['NPWPEKS']      = str_replace('.', '', str_replace('-', '', $data['NPWPEKS']));
            $exec = $this->actMain->insertRefernce('m_trader_eksportir', $data);
            //die($exec);
        }
       
        
        if($this->actMain->insertRefernce('t_bc30pkbedtl',$data)){
            $rtn = 'MSG|OK|'.site_url('bc30/lstDetilPKBE/'.$data['CAR']) . '|Insert data detil berhasil.';
        }
        return $rtn;
    }
    
    function getFormPKBEDtl($car,$seriCont='',$seriPE='',$KDKTR=''){
        //die($car.'|'.$seriCont.'|'.$seriPE.'|'.$KDKTR);
        $this->load->model("actMain");
        $data ['DOK']    = $this->actMain->get_mtabel('JNS_DOK', 1, false, '', 'KODEDANUR','BC30');
        $data ['JNSRES'] = $this->actMain->get_mtabel('JNS_RESPON', 1, false, '', 'KODEDANUR','BC30');
        $data ['JNSPER'] = $this->actMain->get_mtabel('JNS_PERIKSA', 1, false, '', 'KODEDANUR','BC30');
        $data ['KATEKS'] = $this->actMain->get_mtabel('KATEKS', 1, false, '', 'KODEDANUR','BC30');
        $data ['IDEKS'] = $this->actMain->get_mtabel('JENIS_IDENTITAS', 1, false, '', 'KODEDANUR','BC30');
        $data ['MAXLENGTH']    = '<input type="hidden" id="MAXLENGTH" value="' . $this->actMain->get_uraian("SELECT F_MAXLENGTH('t_bc30PKBEdtl') as MAXLENGTH", 'MAXLENGTH') . '"/>';  
        if($seriCont!='' && $seriCont!='-' && $seriPE!='' && $seriPE!='-'){
            //QueriData yang diambil
            $sql = "SELECT  A.*, 
                            B.URAIAN_KPBC AS 'UKDKTRPEB',
                            C.URAIAN_KEMASAN AS 'UJNKEMAS'
                    FROM t_bc30pkbedtl A 
                    LEFT JOIN m_kpbc B ON  B.KDKPBC = A.KDKTRPEB
                    LEFT JOIN m_kemasan C ON C.KODE_KEMASAN = A.JNKEMAS
                    WHERE A.SERICONT = '" . $seriCont . "' AND A.SERIPE = '" . $seriPE . "' AND A.CAR = '".$car."' AND A.KODE_TRADER ='".$this->kd_trader."'" ;
            $data['DETPKBE'] = $this->actMain->get_result($sql);
            //print_r($data['DETPKBE']);die();
            $data['act'] = 'edit';
        }else{
            $sql = "SELECT count(a.SERIPE)+1 as  SERIPE
                    FROM t_bc30pkbedtl a 
                    WHERE a.KODE_TRADER = '".$this->kd_trader."' AND a.CAR = '".$car."' AND a.SERICONT = '1'";
            $data['DETPKBE'] = $this->actMain->get_result($sql);
            //print_r($sql);die();
            $data['DETPKBE']['CAR'] = $car;
            $data['DETPKBE']['KDKTR'] = $KDKTR;
            $data['act'] = 'add';
        }
        
        return $data;
        
    }    
    
    function delPKBEdtl($key){
        $arr = explode('|',$key[0]);
        $car = $arr[0];
        $this->db->where_in("CONCAT(CAR,'|',SERICONT,'|',SERIPE,'|',KODE_TRADER)", $key);
        $exec = $this->db->delete('t_bc30pkbedtl');
        if ($exec) {
            $rtn = 'MSG|OK|'.site_url('bc30/lstDetilPKBE/'.$car) . '|Dalete data detil berhasil.';
        } else {
            $rtn = 'MSG|ER|'.site_url('bc30/lstDetilPKBE/'.$car) . '|Dalete data detil gagal.';
        }
        return $rtn;
    }
    
    function delPKBEhdr($key){
        $this->load->model("actMain");
        $arr = explode('|',$key[0]);
        $car = $arr[0];
        $sql = "SELECT CONCAT(a.`STATUS`,'|',b.URAIAN) as STATUS 
                FROM t_bc30pkbehdr a left join m_tabel b on b.MODUL = 'BC30' AND 
                                                   b.KDTAB = 'STATUS' AND 
                                                   b.KDREC =  a.STATUS 
                WHERE a.KODE_TRADER = '".$this->kd_trader."' AND 
                      a.CAR = '".$car."'";
        $sts = $this->actMain->get_uraian($sql,'STATUS');
        $arrSts = explode('|', $sts);
        if(strpos('|20|30|40|50|60|',$arrSts[0])){
            $rtn = 'MSG|ER|' . site_url('bc30/daftarPKBE/'.$car). '|Dokumen tidak dapat dihapus karena berstatus '.$arrSts[1].'.';
        }else{
            $keys    = array('KODE_TRADER' =>  $this->kd_trader, 'CAR' => $car);
            $this->db->where($keys);
            $tbl    = explode(',','t_bc30pkbehdr,t_bc30pkbedtl,t_bc30pkbecon');
            $exec = $this->db->delete($tbl);
            $rtn = 'MSG|OK|' . site_url('bc30/daftarPKBE/'.$car) . '|Delete data dokumen PKBE Berhasil.';            
        }
        return $rtn;
        
        /*$arr = explode('|',$key[0]);
        $car = $arr[0];
        $this->db->where_in("CONCAT(CAR,'|',KODE_TRADER)", $key);
        $exec = $this->db->delete('t_bc30pkbehdr','t_bc30pkbedtl','t_bc30pkbecon');
        if ($exec) {
            $rtn = 'MSG|OK|'.site_url('bc30/daftarPKBE/'.$car) . '|Dalete data detil berhasil.';
        } else {
            $rtn = 'MSG|ER|'.site_url('bc30/daftarPKBE/'.$car) . '|Dalete data detil gagal.';
        }
        return $rtn;*/
        
    }
    
    function setHeaderPKBEin(){
        $this->load->model("actMain");
        $data = $this->input->post();
        if($data['act']=='save'){
            $data['HEADER']['CAR'] = $this->actMain->get_car('BC30');
        }
        $data['HEADER']['NPWPEKS']      = str_replace('.', '', str_replace('-', '', $data['HEADER']['NPWPEKS']));
        $exe = $this->actMain->insertRefernce('T_BC30PKBEHDR', $data['HEADER']);
        $data['HEADER']['SERICONT'] = 1;
        $data['HEADER']['NOCONT']   = $data['HEADER']['NOCONT1'].$data['HEADER']['NOCONT2'];
        $exe = $this->actMain->insertRefernce('T_BC30PKBECON', $data['HEADER']);
        $msg = $this->validasiHDRPKBE($data['HEADER']['CAR']);
        if($exe){
            $this->actMain->set_car('BC30');
            $rtn =  "MSG|OK|".site_url('bc30/createPKBE/'.$data['HEADER']['CAR'])."|".$msg;
        }
        else {
            $rtn = "MSG|ERR||Data header gagal diproses.";
        }
        return $rtn;
    }
    
    function validasiHDRPKBE($car) {
        $this->load->model('actMain');
        $tipe_trader = $this->newsession->userdata('TIPE_TRADER');
        $i = 0;
        //Validasi Header
        $query = "SELECT * FROM m_trader A  WHERE A.KODE_TRADER = " . $this->kd_trader;
        $strMy = $this->actMain->get_result($query);
        
        $query = "SELECT A.* FROM t_bc30pkbehdr A  WHERE A.CAR = '" . $car . "' AND A.KODE_TRADER = " . $this->kd_trader;
        $aH = $this->actMain->get_result($query);
        
        //print_r($aH);die();
        if (count($aH) > 0) {
            $judul = 'Hasil validasi PKBE Nomor Aju : ' . substr($car, 20) . ' tanggal ' . date('d-m-Y',strtotime(substr($car, 12, 8)));
            $query = "SELECT FLDNAME,MESSAGE from m_validasi WHERE SCRNAME = 'T_BC30PKBEHDR' AND MANDATORY = 1 ";
            $arrdataV = $this->actMain->get_result($query, true);
            foreach ($arrdataV as $aV) {
                if (trim($aH[$aV['FLDNAME']]) == '') {
                    $hasil .= substr('   ' . ++$i, -3) . '. ' . $aV['MESSAGE'] . ' Harus diisi<br>';
                }
            }
            unset($arrdataV);
            /*START VALIDASI KUSUS*/
            //Konsolidator
            switch ($this->newsession->userdata('TIPE_TRADER')){
                case '3': //CNF
                    if ($aH['JNKONS'] == '3' || $aH['JNKONS']=='4') {
                        $hasil .= substr('   ' . ++$i, -3) . '. Untuk Eksportir, pilihan jenis konsolidator hanya 1 atau 2 <br>';
                    }
                    break;
                default :
                    //query masih salah
                    $queryKons = "Select NOKONSOLIDATOR from m_trader_eksportir WHERE IDEKS = '" . $car . "' AND IDEKS = '" . $car . "' AND KODE_TRADER = " . $this->kd_trader;
                    $kons = $this->actMain->get_uraian($queryKons, 'NOKONSOLIDATOR');
                    switch ($aH['JNKONS']){
                        case '3':
                            if ($strMy['NO_PPJK'] == '') {
                                $hasil .= substr('   ' . ++$i, -3) . '. Belum ada izin PPJK. (Lihat Form PPJK) <br>';
                            }
                            if ($kons == '') {
                                $hasil .= substr('   ' . ++$i, -3) . '. Belum ada izin Konsolidator (Lihat form Data Perusahaan).<br>';
                            }
                        case '4':
                            if ($kons == '') {
                                $hasil .= substr('   ' . ++$i, -3) . '. Belum ada izin Konsolidator (Lihat form Data Perusahaan).<br>';
                            }
                    }
                    break;
            }
            
            //Pengisian Container
            $queryCont = "SELECT * FROM t_bc30pkbecon WHERE CAR = '" . $car . "' AND KODE_TRADER = " . $this->kd_trader;
            $aC = $this->actMain->get_result($queryCont, true);
            if (count($aC) <= 0) {
                $hasil .= substr('   ' . ++$i, -3) . '. Data kontainer masih kosong.<br>';
            }
            
            //Cek User
            if (!strstr ('|02|03|04|55|',$aH['STATUS']))
                        {
                        if ($this->newsession->userdata('TIPE_TRADER') == '2')
                        {
                            $aH['IDEKS']   = $strMy['KODE_ID'];
                            $aH['NPWPEKS'] = $strMy['ID'];
                            $aH['NAMAEKS'] = $strMy['NAMA'];
                            $aH['ALMTEKS'] = $strMy['ALAMAT'];                                  
                        }
                    }
            //Cek Pengisian Detil Kontainer
            if (count($aC) > 0) {
            $query = "SELECT FLDNAME,MESSAGE from m_validasi WHERE SCRNAME = 'T_BC30PKBECON' AND MANDATORY = 1 ";
            $arrdataV = $this->actMain->get_result($query, true);
            foreach ($aC as $key => $val) {
                //namaFungsi($val);
                foreach ($arrdataV as $aV) {
                    if (trim($val[$aV['FLDNAME']]) == '') {
                        $hasil .= substr('   ' . ++$i, -3) . '. Data Kontainer '.$key. ' ' . $aV['MESSAGE'] . ' Harus diisi<br>';
                        }
                    }
                }
            }
            
            //Cek Pengisian Detil
            $query = "SELECT A.* FROM t_bc30pkbedtl A  WHERE A.CAR = '" . $car . "' AND A.KODE_TRADER = " . $this->kd_trader;
            $aD = $this->actMain->get_result($query);
            if (count($aD) > 0) {
                //print_r('sini coyy');die();
            $query = "SELECT FLDNAME,MESSAGE from m_validasi WHERE SCRNAME = 'T_BC30PKBEDTL' AND MANDATORY = 1 ";
            $arrdataV = $this->actMain->get_result($query, true);
            foreach ($arrdataV as $aV) {
                if (trim($aD[$aV['FLDNAME']]) == '') {
                    $hasil .= substr('   ' . ++$i, -3) . '. ' . $aV['MESSAGE'] . ' Harus diisi<br>';
                    }
                }
            }else{
                if ($aD['NOPEB'] == '') {
                    $hasil .= substr('   ' . ++$i, -3) . '. Data PEB pada Kontainer ke-1 belum direkam <br>';
                }
            }
            //update status [exist coy]
            if ($aH['STATUS'] == '')
                $aH['STATUS'] = '00';
            #tidak ada perubahan jika status diliar 010 dan 020
            if (strpos('|00|01|', $aH['STATUS']))
                $aH['STATUS'] = ($i == 0) ? '01' : '00';
            //apply ke database
            $upd = $aH;
            unset($upd['URSTATUS']);
            $this->db->where(array('CAR' => $car, 'KODE_TRADER' => $this->kd_trader));
            $exec = $this->db->update('t_bc30PKBEhdr', $upd);
            unset($upd);
            //tulis informasi
            if ($i == 0) {
                $hasil .= '<br>------------------ Data Lengkap ------------------<br>';
            } else {
                $hasil .= '<br>--------------- Data Tidak Lengkap ---------------<br>';
            }
        }
        $view['data'] = $hasil;
        $view['judul'] = $judul;
        $msg = $this->load->view('notify', $view, true);
        unset($aH);
        //return $msg;
        return $msg;
    }
    
    Private function fixLen($str, $len, $chr = ' ', $alg = STR_PAD_RIGHT) {
            $hasil = str_pad(substr($str, 0, $len), $len, $chr, $alg);
            return $hasil;
        }
    
    function genFlatfile($car) {
        $this->load->model('actMain');
        $EDINUMBER = $this->newsession->userdata('EDINUMBER');
        $SRP = $this->newsession->userdata('SRP');
        $SQL = "SELECT  A.*, B.NOMOREDI, C.NO_PPJK, C.TANGGAL_PPJK, D.NOKONSOLIDATOR,
                        DATE_FORMAT(A.TGSIUP,'%Y%m%d') as TGSIUP,
                        DATE_FORMAT(A.TGTDP,'%Y%m%d') as TGTDP,
                        DATE_FORMAT(C.TANGGAL_PPJK,'%Y%m%d') as TANGGAL_PPJK,
                        DATE_FORMAT(A.TGSURAT,'%Y%m%d') as TGSURAT,
                        DATE_FORMAT(A.TGSTUFFING,'%Y%m%d') as TGSTUFFING,
                        DATE_FORMAT(D.TGKONSOLIDATOR,'%Y%m%d') as TGKONSOLIDATOR
                        FROM t_bc30pkbehdr A
                        LEFT JOIN M_TRADER_PARTNER B ON A.KODE_TRADER = B.KODE_TRADER and B.KDKPBC = A.KDKTR
                        LEFT JOIN m_trader C ON A.KODE_TRADER = C.KODE_TRADER
                        LEFT JOIN m_trader_bc30 D ON A.KODE_TRADER = D.KODE_TRADER AND D.KPBC = A.KDKTR
                       WHERE a.CAR = '" . $car . "' AND a.KODE_TRADER = " . $this->kd_trader;
        #return $SQL;
        //print_r($SQL);die();
        $hdr = $this->actMain->get_result($SQL);
        $hdr = str_replace("\n", '', str_replace("\r", '', $hdr));
        if ($hdr['STATUS'] != '01') {
            if ($hdr['STATUS'] == '02') {
                $whHDR['KODE_TRADER'] = $this->kd_trader;
                $whHDR['CAR'] = $car;
                $this->db->where($whHDR);
                $upHDR['SNRF']   = '';
                $upHDR['DIRFLT'] = '';
                $upHDR['DIREDI'] = '';
                $upHDR['STATUS'] = '01';
                $this->db->update('t_bc30pkbehdr', $upHDR);
                unset($upHDR);
                #delete dokoutbound
                $whHDR['KDKPBC'] = $hdr['KDKPBC'];
                $whHDR['JNSDOK'] = 'BC30'; #pertanyaan
                #$this->db->delete('m_trader_dokoutbound', $whHDR);
                unset($whHDR);
                unlink($hdr['DIRFLT']);
                unlink($hdr['DIREDI']);
                $hsl = '1|';
            }
            else {
                $hsl = '0|';
            }
            unset($hdr);
            return $hsl;
        }
        if ($hdr['NOMOREDI'] == '') {
            return 'Edinumber Kantor Pelayanan belum terekam.';
        }
        #SEPARATOR UNTUK HEADER
        $FF.= 'ENV00101' . $this->fixLen($hdr['NOMOREDI'], 20) . $this->fixLen($hdr['KDKPBC'], 20) . "DOKPKB    2\n";
        
        #RECORD TYPE HCND0101 PKBE
        $FF.= 'HCND0101' . $this->fixLen($hdr['ORIGINAL'], 1) . "BCPKB 830DOKPKB9\n";
        
        #RECORD TYPE HCND0201
        ($hdr['NOSIUP'] != '') ?    $FF.= 'HCND0201ADZ' . $this->fixLen($hdr['NOSIUP'], 30) . $this->fixLen($hdr['TGSIUP'], 8)        . "102\n" : '';
        ($hdr['NOTDP'] != '')  ?    $FF.= 'HCND0201ADJ' . $this->fixLen($hdr['NOTDP'],  30) . $this->fixLen($hdr['TGTDP'], 8)         . "102\n" : '';
        //($hdr['JNKONS'] != '') ?    $FF.= "HCND0201XA2" . fixLen($hdr['JNKONS'], 30) . fixLen($hdr['JNKONS'], 8)        . "102\n" : '';
        //($hdr['NO_PPJK'] != '') ?   $FF.= "HCND0201XA2" . fixLen($hdr['NO_PPJK'], 30). fixLen($hdr['TANGGAL_PPJK'], 8)  . "102\n" : '';
        
        if ($hdr['JNKONS'] != '') {
            switch ($hdr['JNKONS']) {
                case'3':
                    $FF.= 'HCND0201XA2' . $this->fixLen($hdr['JNKONS'], 30) . $this->fixLen($hdr['JNKONS'], 8). "102\n";
                    ($hdr['NO_PPJK']!='')?$FF.= "HCND0201XA3" . $this->fixLen($hdr['NO_PPJK'], 30) . $this->fixLen($hdr['TANGGAL_PPJK'], 8). "102\n":'';
                    break;
                case'4':
                    $FF.= 'HCND0201XA2' . $this->fixLen($hdr['JNKONS'], 30) . $this->fixLen($hdr['JNKONS'], 8). "102\n";
                    break;
                default :
                 'do nothing';
            }   
        }
        
        ($hdr['NIPER'] != '')  ?    $FF.= 'HCND0201XA1' . $this->fixLen($hdr['NIPER'],  30) . $this->fixLen($hdr['NIPER'], 8)         . "102\n" : '';
        
        #RECORD TYPE HCND0301 [PEMUAT]
        $FF.= 'HCND0301142 ' . $this->fixLen($hdr['KDKTRMUAT'], 6) . $this->fixLen($hdr['KDNEGTUJU'], 3) . "\n";
        
        #RECORD TYPE HCND0301 [PEMERIKSA]
        $FF.= 'HCND0301143 ' . $this->fixLen($hdr['KDKTRPERIKSA'], 6) . "\n";
         
        #RECORD TYPE HCND0301 [PENDAFTARAN]
        $FF.= 'HCND0301122 ' . $this->fixLen($hdr['KDKTR'], 6)        . "\n";
        
        #RECORD TYPE HCND0301 [LOKASI GUDANG] PERBAIKAN HANGGAR
        $FF.= 'HCND03011100'. $this->fixLen($hdr['KDHANGGAR'], 6) . $this->fixLen($hdr['KDHANGGAR'], 3) . "\n";
        
        #RECORD TYPE HCND0401
        $FF.= 'HCND0401CS ' .$this->fixLen($hdr['IDEKS'], 1) . $this->fixLen($hdr['NPWPEKS'], 15) . $this->fixLen($hdr['NAMAEKS'], 50) . $this->fixLen($hdr['ALMTEKS'], 70) 
                            .$this->fixLen($hdr['JNKONS'], 2) . "\n";
        
        #RECORD TYPE HCND0401
        /*if($hdr['NPWPEKS']!= $hdr['NPWPUSER']){
            $FF.='HCND0401CB '.$this->fixLen($hdr['NPWPUSER'], 15).$this->fixLen($hdr['NAMAEKS'], 15).$this->fixLen($hdr['vNamaUser'], 50).$this->fixLen($hdr['vAlmtUser'], 70)."\n";
        }*/
        
        #RECORD TYPE HCND0401 [PENANDATANGAN]
        ($hdr['NAMATTD'] != '') ? $FF.= 'HCND0401AM                 '. $this->fixLen($hdr['NAMATTD'], 50) . "\n" : '';
        
        #RECORD TYPE HCND0411
        ($hdr['NOSURAT'] != '') ?    $FF.= 'HCND0411DE' . $this->fixLen($hdr['PETUGAS'], 17) . 'TE' . $this->fixLen($hdr['NOPHONE'], 50)."\n" : '';
        
        #RECORD TYPE HCND0421 [PERMOHONAN]
        ($hdr['NOSURAT'] != '') ? $FF.= 'HCND0421MRN'. $this->fixLen($hdr['NOSURAT'], 30) . '182' .$this->fixLen($hdr['TGSURAT'], 8) ."\n" : '';
        
        #RECORD TYPE HCND0421
        $FF.= 'HCND0421'. 'AOZ' . '0550' . "\n";
        
        #RECORD TYPE HCND0501 [ANGKUT]
        ($hdr['CARRIER'] != '') ?  $FF.= 'HCND050120'. $this->fixLen($hdr['CARRIER'], 20) . $this->fixLen($hdr['VOYFLIGHT'], 7) . "\n":'';
        
        #RECORD TYPE CCND0101 SEGEL GAK TAU DI AMBIL DARI MANA
        $FF.= 'CCND0101CN' . $this->fixLen($hdr['SIZE'], 5) . $this->fixLen($hdr['NOCONT'], 20) . '   ' . $this->fixLen($hdr['SIZE'], 5) . $this->fixLen($hdr['SEGEL'], 1) . 'ZZZ' 
                           . $this->fixLen($hdr['TGSTUFFING'], 12) . '104' . 'FO' . $this->fixLen($hdr['NIPSTUFFING'], 5) ."\n";
        
        
        
        #DATA CONTAINER
         $SQL = "SELECT A.*,
                        DATE_FORMAT(A.TGSTUFFING,'%Y%m%d') as TGSTUFFING,
                        DATE_FORMAT(A.JAMSTUFFING,'%H%i') as JAMSTUFFING
                        FROM t_bc30pkbecon A
                       WHERE A.CAR = '".$car."' AND A.KODE_TRADER ='".$this->kd_trader."'";
        $cont = $this->actMain->get_result($SQL, true);
        $cont = str_replace("\n", '', str_replace("\r", '', $cont));
        #RECORD TYPE CCND0101
        if(count($cont)>0){
            foreach ($cont as $a){
                $FF.= 'CCND0101CN '.$this->fixLen($a['SERICONT'], 5).$this->fixLen($a['NOCONT'], 20).$this->fixLen($a['SIZE'], 2)
                           .' '.'ZZZ'.$this->fixLen($a['TGSTUFFING'], 12).$this->fixLen($a['JAMSTUFFING'], 4).'104'.$this->fixLen($a['ALMTSTUFFING'], 70)
                           .'FO'.$this->fixLen($a['NIPSTUFFING'], 9)."\n";
            }
        }
        unset($cont);
        
        
        #DATA DETAIL
         $SQL = "SELECT A.*,
                        DATE_FORMAT(A.TGPEB,'%Y%m%d') as TGPEB,
                        DATE_FORMAT(A.TGPE,'%Y%m%d') as TGPE
                        FROM t_bc30pkbedtl A   
                       WHERE A.CAR = '".$car."' AND A.KODE_TRADER ='".$this->kd_trader."'";
        $dtl = $this->actMain->get_result($SQL, true);
        $dtl = str_replace("\n", '', str_replace("\r", '', $dtl));
        
        if(count($dtl)>0){
            foreach ($dtl as $d){
                #RECORD TYPE PCND0101
                $FF.= 'PCND0101'.$this->fixLen($d['SERICONT'], 5).$this->fixLen($d['SERIPE'], 5).$this->fixLen($d['SERIPE'], 6)."\n";

                #RECORD TYPE PCND0201 
                $FF .='PCND0201XXXJNB'.$this->fixLen($d['JNEKS'], 30).'        '.$this->fixLen($d['KETERANGAN'], 70)."\n";
                $FF .='PCND0201XXXPEB'.$this->fixLen($d['NOPEB'], 30).$this->fixLen($d['TGPEB'], 8)."\n";
                $FF .='PCND0201XXXPM '.$this->fixLen($d['NOPE'], 30).$this->fixLen($d['TGPE'], 8)."\n";

                #RECORD TYPE PCND0301
                ($d['NPWPEKS'] != '')? $FF.='PCND0301EX '.$this->fixLen($d['IDEKS'], 1).$this->fixLen($d['NPWPEKS'], 15)
                                                           .$this->fixLen($d['NAMAEKS'], 50).$this->fixLen($d['ALMTEKS'], 70)."\n":'';

                #RECORD TYPE PCND0401
                ($d['JMKEMAS'] > 0)? $FF.='PCND0401ZZZ'.$this->fixLen($d['JNKEMAS'], 3).$this->fixLen($d['JMKEMAS'], 8). "\n":'';
            }
            
        }
        unset($dtl);
        
        #RECORD TYPE UNZ00101
        $FF.='UNZ00101'."\n";
        
        $fullPaht = 'FLAT/' . $EDINUMBER;
        if (!is_dir($fullPaht)){mkdir($fullPaht,0777,true);}
        $fullPaht = $fullPaht . '/' . $car . '.FLT';
        if (file_exists($fullPaht)){unlink($fullPaht);}
        $handle = fopen($fullPaht, 'w');
        fwrite($handle, $FF);
        fclose($handle);
        if (file_exists($fullPaht)) {$hsl = '2|' . $hdr['KDKTR'];}
        else {$hsl = '0|';}
        unset($hdr);
        return $hsl;
    }    
    
    function crtQueue($car = '') {
        $this->load->model('actTranslator');
        $SNRF       = date('ymdHis') . rand(10, 99);
        $EDINUMBER  = $this->newsession->userdata('EDINUMBER');
        $EDIPaht    = 'EDI/' . $EDINUMBER;
        //print_r($SNRF .'|'.$EDINUMBER.'|'.$EDIPaht);die();
        if (!is_dir($EDIPaht)){
            mkdir($EDIPaht,0777,true);
        }else{
            chmod($EDIPaht,0777);
        }
        $FLTPaht    = 'FLAT/' . $EDINUMBER;
        if (!is_dir($FLTPaht)){
            mkdir($FLTPaht,0777,true);
        }else{
            chmod($FLTPaht,0777);
        }
        $this->actTranslator->getEDIFILE($EDINUMBER,'DOKPKB',$SNRF,$car.'.FLT');
		if (file_exists($EDIPaht.'/'. $car .'.EDI') && file_exists($FLTPaht.'/'. $car .'.FLT')){
            $dirEdiNumber = 'TRANSACTION/' . $EDINUMBER . '/' . date('Ymd');
            if (!is_dir($dirEdiNumber)){mkdir($dirEdiNumber,0777,true);}
            rename($FLTPaht.'/'. $car .'.FLT', $dirEdiNumber . '/' . $car . '.FLT');
            rename($EDIPaht.'/'. $car .'.EDI', $dirEdiNumber . '/' . $car . '.EDI');
            $rtn = $SNRF . '|' . $dirEdiNumber . '/' . $car . '.EDI' . '|' . $dirEdiNumber . '/' . $car . '.FLT';
        }
        else {
            $rtn = 'ERROR';
        }
        //print_r($rtn);
        return $rtn;
    }
    
    function updateSNRF($car, $data) {
        #update t_bc30hdr
        $this->load->model('actMain');
        $arrData = explode('|', $data);
        $dataHdr['SNRF'] = $arrData[0];
        $dataHdr['DIRFLT'] = $arrData[2];
        $dataHdr['DIREDI'] = $arrData[1];
        $dataHdr['STATUS'] = '02';
        $dataHdr['CAR'] = $car;
        $dataHdr['KODE_TRADER'] = $this->kd_trader;
        $this->actMain->insertRefernce('t_bc30pkbehdr', $dataHdr);

        #insert m_trader_dokoutbound
        $dataDOK['KODE_TRADER'] = $this->kd_trader;
        $dataDOK['KDKPBC'] = $arrData[3];
        $dataDOK['JNSDOK'] = 'BC30PKBE';
        $dataDOK['APRF'] = 'DOKPKB';
        $dataDOK['CAR'] = $car;
        $dataDOK['STATUS'] = '00';
        $this->actMain->insertRefernce('m_trader_dokoutbound', $dataDOK);
        return '0';
    }
    
    function cetakDokumenPKBE($car, $jnPage, $arr ) {
        //print_r('sini');die();
        $this->load->library('fpdf');
        $this->load->model('actMain');
        $this->load->model('dokBC30PKBE');
        $this->dokBC30PKBE->ciMain($this->actMain);
        $this->dokBC30PKBE->fpdf($this->fpdf);
		$this->dokBC30PKBE->segmentUri($arr);		
		//$this->dokBC30PKBE->data($data);
        $this->dokBC30PKBE->showPage($car, $jnPage);
    }
    
    function getFormPKB($car){
        $this->load->model("actMain");
        $data ['CAR']           = $car; //print_r($data ['CAR']);die();
        $data ['JNUSAHA']       = $this->actMain->get_mtabel('JNS_PERUSAHAAN', 1, false, '', 'KODEDANUR','BC30');
        $data ['KDKITE']        = $this->actMain->get_mtabel('84', 1, false, '', 'KODEDANUR','BC30');
        $data ['ZONING_KITE']   = $this->actMain->get_mtabel('ZONING_KITE', 1, false, '', 'KODEDANUR','BC30');
        $data ['JNBRGGAB']      = $this->actMain->get_mtabel('JNBRGGAB', 1, false, '', 'KODEDANUR','BC30');
        $data ['GUDANG']        = $this->actMain->get_mtabel('GUDANG', 1, false, '', 'KODEDANUR','BC30');
        $data ['STUFF']         = $this->actMain->get_mtabel('STUFF', 1, false, '', 'KODEDANUR','BC30');
        $data ['JNPARTOF']      = $this->actMain->get_mtabel('JNPARTOF', 1, false, '', 'KODEDANUR','BC30');
        $data ['MAXLENGTH']     = '<input type="hidden" id="MAXLENGTH" value="' . $this->actMain->get_uraian("SELECT F_MAXLENGTH('t_bc30pkb') as MAXLENGTH", 'MAXLENGTH') . '"/>';  
        if($car){
            $sql = "SELECT  A.*, 
                            B.NPWPEKS, 
                            B.NAMAEKS, 
                            B.NIPER, 
                            B.ALMTEKS, 
                            B.JNEKS,
                            B.TGSIAP,
                            B.WKSIAP,
                            C.URAIAN as 'ULENGKAP'
                    FROM  t_bc30pkb A
                    LEFT JOIN  t_bc30hdr B ON A.KODE_TRADER=B.KODE_TRADER AND A.CAR=B.CAR
                    LEFT JOIN  m_tabel   C ON C.MODUL = 'BC30' AND C.KDTAB = 'STATUS_DETIL' AND C.KDREC = A.LENGKAP
                    WHERE A.CAR = '".$car."' AND A.KODE_TRADER ='".$this->kd_trader."'" ;
            $data['DTLPKB'] = $this->actMain->get_result($sql);
            $data['act'] = 'edit';
        }else{
            $data['act'] = 'save';
        }
        $sql = "SELECT A.NPWPEKS, A.ALMTEKS, A.NIPER, A.NAMAEKS 
                FROM t_bc30hdr A 
                    WHERE A.CAR = '".$car."' AND A.KODE_TRADER ='".$this->kd_trader."'" ;
        $data['PEMOHON']= $this->actMain->get_result($sql);
        //print_r($data['PEMOHON']);die();
        return $data;   
    }
    
    function setHeaderPKBin(){
        //print_r($car);die();
        $this->load->model("actMain");
        $data = $this->input->post();
        $data['DTLPKB']['INSTFAS']  = '2';
        $exe = $this->actMain->insertRefernce('t_bc30pkb', $data['DTLPKB']);
        //echo $this->db->last_query();
        $exe = $this->actMain->insertRefernce('t_bc30hdr', $data['DTLPKB']);
        $msg = $this->validasiPKB($data['DTLPKB']['CAR']);
        if($exe){
            $rtn =  "MSG|OK|".site_url('bc30/frmPKB/'.$data['DTLPKB']['CAR'])."|".$msg;
        }
        else {
            $rtn = "MSG|ERR||Data PKB gagal diproses.";
        }
        return $rtn;
    }
    
    function getFormPEBGab($car='', $jmbrggab='', $seri='1'){
        //print_r($car .'|'. $jmbrggab.'|'.$seri);die();
        $this->load->model("actMain");
        $data['CAR']            = $car;
        $data['JENIS_IDENTITAS'] = $this->actMain->get_mtabel('JENIS_IDENTITAS', 1, true,'','KODEDANUR','BC30');
        $data['MAXLENGTH']      = '<input type="hidden" id="MAXLENGTH" value="' . $this->actMain->get_uraian("SELECT F_MAXLENGTH('t_bc30gab') as MAXLENGTH", 'MAXLENGTH') . '"/>';  
        $data['ZONING_KITE']    = $this->actMain->get_mtabel('ZONING_KITE', 1, false, '', 'KODEDANUR','BC30');
        
        if($car && $seri){
            $sql = "SELECT A.*, B.UREDI AS 'UKDVALG', C.URAIAN AS 'UGABOK', D.JMBRGGAB
                    FROM t_bc30gab A LEFT JOIN m_valuta B ON A.KDVALG = B.KDEDI 
                                     LEFT JOIN  m_tabel C ON C.MODUL  = 'BC30' AND C.KDTAB = 'STATUS_DETIL' AND C.KDREC = A.GABOK
                                     LEFT JOIN  t_bc30hdr D ON A.CAR  = D.CAR AND A.KODE_TRADER = D.KODE_TRADER
                    WHERE A.CAR = '".$car."' AND A.SERIEKS = '".$seri."' AND A.KODE_TRADER ='".$this->kd_trader."'" ;
            #die($sql);
            $hasil = $this->actMain->get_result($sql);
            $data['PEBGAB']     = array_change_key_case($hasil, CASE_UPPER);
            $data['act']        = 'edit';
        }
        else{
            $data['act']        = 'add';
        }
        $sql  = "SELECT count(A.SERIEKS) AS 'JMLGAB' 
                 FROM t_bc30gab A
                 WHERE A.CAR = '".$car."' AND A.KODE_TRADER ='".$this->kd_trader."'";
        $data['JMLGAB'] = $this->actMain->get_uraian($sql,'JMLGAB');
        return $data;
    }
    
    function setHeaderPEBGabin(){
        $data = $this->input->post('PEBGAB');
        $this->load->model("actMain");
        $data['NPWPEKSG'] = str_replace('.', '', str_replace('-', '', $data['NPWPEKSG']));
        $exe = $this->actMain->insertRefernce('t_bc30gab', $data);
        $msg = $this->validasiPEBgab($data['CAR'],$data['JMBRGGAB'],$data['SERIEKS']);
        if($exe){
            $rtn = "MSG|OK|".site_url('bc30/RefFrmPEBGab/'.$data['CAR'].','.$data['SERIEKS']).','.$data['JMBRGGAB']."|".$msg;
        }
        else {
            $rtn = "MSG|ERR||Data PEB gagal diproses.";
        }
        return $rtn;
    }
    
    function getFormPEBdtlGab($car, $jmbrgg='', $serieks='', $seri='1'){
        //print_r($car.'|'.$jmbrgg.'|'.$serieks.'|'.$seri);die();
        $this->load->model("actMain");
        $data['CAR']           = $car;
        $data['SERIEKS']       = $serieks;
        $data['JMBRGG']        = $jmbrgg;
        $data['SERI']          = $seri;
        $data['MAXLENGTH']     = '<input type="hidden" id="MAXLENGTH" value="' . $this->actMain->get_uraian("SELECT F_MAXLENGTH('t_bc30gabdtl') as MAXLENGTH", 'MAXLENGTH') . '"/>';  
        if($car && $seri){
              $sql = "SELECT A.*,B.UREDI AS 'UJNSATUAN', C.URAIAN_KEMASAN AS 'UJNKOLIG', D.URAIAN AS 'UDTLGOK' 
                    FROM t_bc30gabdtl A
                    LEFT JOIN m_satuan  B ON A.JNSATUANG =B.KDEDI
                    LEFT JOIN m_kemasan C ON A.JNKOLIG   =C.KODE_KEMASAN
                    LEFT JOIN m_tabel   D ON D.MODUL     ='BC30' AND D.KDTAB = 'STATUS_DETIL' AND D.KDREC = A.DTLGOK
                  WHERE A.CAR = '".$car."' AND A.SERIEKS = '".$serieks."' AND A.SERIBRGG = '".$seri."' AND A.KODE_TRADER ='".$this->kd_trader."'" ;
            $hasil = $this->actMain->get_result($sql);
            $data['PEBDTLGAB']  = array_change_key_case($hasil, CASE_UPPER);
            $data['act'] = 'edit';
            $data['DOKGAB']        = $this->getTblDokGab($car, $data['PEBDTLGAB']['SERIEKS'], $data['PEBDTLGAB']['SERIBRGG']);
        }else{
            $data['act'] = 'add';
            $data ['DOKGAB']        = $this->getTblDokGab($car,$serieks,$jmbrgg );
        }
        $sql  = "select count(A.SERIBRGG) AS 'SERIBRG', B.JMBRGG AS 'JMBRGG_C' FROM t_bc30gabdtl A 
                    LEFT JOIN t_bc30gab B ON A.KODE_TRADER=B.KODE_TRADER AND A.CAR=B.CAR AND A.SERIEKS=B.SERIEKS 
                WHERE A.CAR = '".$car."' AND A.SERIEKS = '".$serieks."' AND A.KODE_TRADER ='".$this->kd_trader."'" ;
        $data['SERI'] = $this->actMain->get_result($sql);
        return $data;
    }
    
    function setHeaderPEBdtlGabin(){
        $this->load->model("actMain");
        $data = $this->input->post();
        $data['PEBDTLGAB']['URBRGG']   = $data['PEBDTLGAB']['URBRGG1'].$data['PEBDTLGAB']['URBRGG2'].$data['PEBDTLGAB']['URBRGG3'].$data['PEBDTLGAB']['URBRGG4'];
        $exe = $this->actMain->insertRefernce('t_bc30gabdtl', $data['PEBDTLGAB']);
        $msg = $this->validasiPEBgabDTL($data['PEBDTLGAB']['CAR'],$data['PEBDTLGAB']['SERIEKS'],$data['PEBDTLGAB']['SERIBRGG']);
        if($exe){
            return "MSG|OK|".site_url('bc30/RefFrmPEBGabDtl/'.$data['PEBDTLGAB']['CAR'].'/'.$data['PEBDTLGAB']['SERIBRGG'].'/'.$data['PEBDTLGAB']['SERIEKS'].'/'.$data['PEBDTLGAB']['SERIBRGG'])."|".$msg;
        }
        else {
            $rtn = "MSG|ERR||Data PEB Gabungan gagal diproses.";
        }
        return $rtn;
    }
    
    function getTblDokGab($car, $serieks, $seribrgg) {
        $data['serieks']= $serieks;
        $data['seribrgg']= $seribrgg;
        $arrdata = $this->lsvDokGab($car, $serieks, $seribrgg);
        $data['list'] = $this->load->view('list', $arrdata, true);
        $data['type'] = 'dokgab';
        return $this->load->view("bc30/lst", $data, true);
    }
    
    function lsvDokGab($car, $serieks, $seribrgg){
        //print_r($serieks.'|'.$seribrgg);die();
        $this->load->library('newtable');
        $tipe = 'dokgab';
        $this->load->model("actMain");
        $SQL = "SELECT  KTRSSTBG AS KPBC, 
                        NODOKG AS NOMOR, 
                        TGDOKG AS TANGGAL, CAR, KODE_TRADER, SERIEKS, SERIBRGG, KTRSSTBG, KDDOKG, NODOKG
                        FROM t_bc30gabdok
                    WHERE CAR='" . $car . "' AND SERIEKS='".$serieks."' AND SERIBRGG='".$seribrgg."' AND KODE_TRADER='" . $this->kd_trader . "'";
        //print_r($SQL);die();
        $prosesnya = array(
            'Tambah'    => array('ADDAJAX', site_url('bc30/DokGab/add/'.$car.'/'.$serieks.'/'.$seribrgg), '0','fdokgab_form'),
            'View'      => array('EDITAJAX', site_url('bc30/DokGab/edit/'.$car), '1', 'fdokgab_form'),
            'Hapus '    => array('DELETEAJAX', site_url('bc30/DokGab/delete/'.$car), 'N', 'fdokgab_list')
            );
        $this->newtable->menu($prosesnya);
        $this->newtable->keys(array('CAR','SERIEKS','SERIBRGG','KTRSSTBG','KDDOKG','NODOKG'));
        $this->newtable->hiddens(array('CAR','KODE_TRADER','SERIEKS','SERIBRGG','KTRSSTBG','KDDOKG','NODOKG'));
        $this->newtable->search(array(
            array('KTRSSTBG',        'KPBC'),
            array('NODOKG',   'No. Dok Gab')));
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->action(site_url("bc30/cariDokGab"));
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->orderby(1, 2);
        $this->newtable->sortby("DESC");
        $this->newtable->set_formid("f".$tipe);
        $this->newtable->set_divid("div".$tipe);
        $this->newtable->rowcount(20);
        $this->newtable->tipe_proses('combobox');
        $this->newtable->clear();
        $tabel .= $this->newtable->generate($SQL);
        $arrdata = array('tabel' => $tabel,'tipe' => $tipe);
        if ($this->input->post("ajax")){
            return $tabel;
        }
        else{
            return $arrdata;
        }
    }
    
    function getDokGab($car, $serieks, $seribrgg, $ktrsstbg, $kddokg, $nodokg) {
        //print_r($car.'|'.$serieks.'|'.$seribrgg.'|'.$ktrsstbg.'|'.$kddokg.'|'.$nodokg);die();
        $this->load->model("actMain");
        $data ['CAR']       = $car;
        $data ['SERIEKS']   = $serieks;
        $data ['SERIBRGG']  = $seribrgg;
        if ($car && $ktrsstbg && $kddokg && $nodokg ) {
            $sql = "SELECT A.*,B.URAIAN_KPBC  FROM t_bc30gabdok A
                        left join m_kpbc B on A.KTRSSTBG=B.KDKPBC
                    WHERE CAR='" . $car . "' AND SERIEKS='".$serieks."' AND SERIBRGG='".$seribrgg."' AND KTRSSTBG='".$ktrsstbg."' AND KDDOKG='".$kddokg."' AND NODOKG='".$nodokg."' AND KODE_TRADER='" . $this->kd_trader . "'";
            $data['DOKGAB'] = $this->actMain->get_result($sql);
            $data['act'] = 'update';
        }
        else {
            $data['act'] = 'save';
        }
        $data['KDDOK'] = $this->actMain->get_mtabel('KDDOK', 1, true, '', 'KODEDANUR','BC30');
        return $data;
    }
    
    function setDokGab($type,$car, $serieks, $seribrgg){
        //PRINR_R($car.'|'.$serieks.'|'.$seribrgg);DIE();
        if ($type == 'save' || $type == 'update') {
            $this->load->model("actMain");
            $data = $this->input->post('DOKGAB');
            $exec = $this->actMain->insertRefernce('t_bc30gabdok',$data);
            //echo $this->db->last_query();
            if ($exec) {
               
                return "MSG|OK|".site_url('bc30/lstDokGab/'.$data['CAR'].','.$data['SERIEKS'].','.$data['SERIBRGG'])."|".$msg;
            } else {
                return "MSG|ERR||Proses data kontainer gagal.";
            }
        }
        else if ($type == 'delete'){
            $this->input->post('tb_chkfdokgab');
            foreach ($this->input->post('tb_chkfdokgab') as $chkitem){
                $arrchk = explode("|", $chkitem);
                $data['CAR']        = $arrchk[0];
                $data['SERIEKS']    = $arrchk[1];
                $data['SERIBRGG']   = $arrchk[2];
                $data['KTRSSTBG']   = $arrchk[3];
                $data['KDDOKG']     = $arrchk[4];
                $data['NODOKG']     = $arrchk[5];
                $data['KODE_TRADER']= $this->kd_trader;
            $this->db->where($data); 
            $exec = $this->db->delete('t_bc30gabdok');
            }
            if ($exec){
                return "MSG|OK|".site_url('bc30/lstDokGab/'.$data['CAR'])."|Data Dokumen Gabungan berhasil didelete.";
            }
            else {
                return "MSG|ERR||Data Dokumen gabungan gagal didelete.";
            }
        }
    }
    
    function lsvHeader($tipe='BC30'){
        $this->load->library('newtable');
        $this->load->model("actMain");
        $SQL = "SELECT  concat(SUBSTRING(a.CAR,1,6),'-',SUBSTRING(a.CAR,7,6),'-',SUBSTRING(a.CAR,13,8),'-',SUBSTRING(a.CAR,21,8))  AS 'C A R', 
            a.CAR as 'No Aju',
                        b.URAIAN as 'Katagori Ekspor',
                        a.NAMABELI as 'Pembeli',
                        a.ANGKUTNAMA as 'Nama Moda',
                        a.JMBRG as 'Jml Barang',
                        a.JMCONT as 'Jml Kontainer',
                        c.Uraian as 'Staus'
                FROM T_BC30HDR a Left Join M_TABEL b ON b.MODUL='BC30' AND b.KDTAB='KATEKS' AND b.KDREC = a.KATEKS
                                 Left Join M_TABEL c ON c.MODUL='BC30' AND c.KDTAB='STATUS' AND c.KDREC = a.STATUS
                WHERE KODE_TRADER= '".  $this->kd_trader ."'";
        $prosesnya = array(
            'Create'    => array('ADDAJAX', site_url('bc30/create'), '0', '_content_'),
            'View'      => array('GETAJAX', site_url('bc30/create'), '1', '_content_'),
            'Print Dokumen' => array('GETPOP', site_url('bc30/beforecetakPEB'), '1', ' CETAK DOKUMEN PEB |400|200'),
            'Upload B30'    => array('GETPOP', site_url('bc30/uploadbue/viewform'), '0', ' UPLOAD FILE B30 |400|200'),  
            'Download B30'  => array('DOWNLOADAJAX', site_url('bc30/downloadBUE/download'), '1', ' DOWNLOAD FILE BUE |400|200'),
            'Hapus '    => array('DELETEAJAX', site_url('bc30/deleteHdr'), 'N', '_content_'));
        $this->newtable->menu($prosesnya);
        $this->newtable->keys(array('No Aju'));
        $this->newtable->hiddens(array('No Aju'));
        $this->newtable->search(array(
            array('CAR',        'No. Aju'),
            array('a.KATEKS',   'Katagori Ekspor', 'tag-select', $this->actMain->get_mtabel('KATEKS',1,false,'','KODEUR','BC30')),
            array('a.NAMABELI', 'Nama Pembeli'),
            array('a.ANGKUTNAMA','Nama Moda'),
            array('a.STATUS', 'Status', 'tag-select', $this->actMain->get_mtabel('STATUS',1,false,'','KODEUR','BC30'))));
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->action(site_url("bc30/daftar"));
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->orderby(1, 2);
        $this->newtable->sortby("DESC");
        $this->newtable->tipe_check('radio');
        $this->newtable->set_formid("f".$tipe);
        $this->newtable->set_divid("div".$tipe);
        $this->newtable->rowcount(20);
        $this->newtable->tipe_proses('button');
        $this->newtable->clear();
        $tabel .= $this->newtable->generate($SQL);
        $arrdata = array('tabel' => $tabel,'tipe' => $tipe);
        if ($this->input->post("ajax")){
            return $tabel;
        }
        else{
            return $arrdata;
        }
    }
    
    function get_header($aju = "") {//print_r('sini');die();
        $data = array();
        $this->load->model("actMain");
        if ($aju) {
            $SQL    = "SELECT	a.*,
                                l.TIPE_TRADER,
                                b.URAIAN_KPBC as 'URKDKTR',
                                c.UREDI as 'URKDVAL',
                                d.URAIAN_PELABUHAN as URPELMUAT,
                                e.URAIAN_PELABUHAN as URPELMUATEKS,
                                f.URAIAN_PELABUHAN as URPELTRANSIT,
                                g.URAIAN_PELABUHAN as URPELBONGKAR,
                                h.URAIAN_KPBC as 'URKDKTRPRIKS',
                                i.URDAERAH as 'URPROPBRG',
                                j.URAIAN_NEGARA as 'URANGKUTFL',
                                k.URAIAN_NEGARA as 'URNEGTUJU',
                                m.URHGR as 'URKDHANGGAR',
                                n.URAIAN as 'URSTATUS',
                                o.URAIAN_NEGARA as 'URNEGBELI'
                       FROM     t_BC30hdr a LEFT JOIN m_kpbc b      ON a.KDKTR = b.KDKPBC
                                            LEFT JOIN m_valuta c    ON a.KDVAL = c.KDEDI
                                            LEFT JOIN m_pelabuhan d ON a.PELMUAT = d.KODE_PELABUHAN
                                            LEFT JOIN m_pelabuhan e ON a.PELMUATEKS = e.KODE_PELABUHAN
                                            LEFT JOIN m_pelabuhan f ON a.PELTRANSIT = f.KODE_PELABUHAN
                                            LEFT JOIN m_pelabuhan g ON a.PELBONGKAR = g.KODE_PELABUHAN
                                            LEFT JOIN m_kpbc h      ON a.KDKTRPRIKS = h.KDKPBC
                                            LEFT JOIN m_daerah i    ON a.PROPBRG = i.KDDAERAH
                                            LEFT JOIN m_negara j    ON a.ANGKUTFL = j.KODE_NEGARA
                                            LEFT JOIN m_negara k    ON a.NEGTUJU = k.KODE_NEGARA
                                            LEFT JOIN m_trader l    ON l.KODE_TRADER = a.KODE_TRADER
                                            LEFT JOIN m_hanggar m   ON m.KDKPBC = a.KDKTR AND m.KDHGR = a.KDHANGGAR
                                            LEFT JOIN m_tabel n     ON n.MODUL = 'BC30' AND n.KDTAB = 'STATUS' AND n.KDREC = a.STATUS
                                            LEFT JOIN m_negara o    ON a.NEGBELI = o.KODE_NEGARA
                       WHERE 	a.CAR = '" . $aju . "' AND a.KODE_TRADER = " . $this->kd_trader ;
                // die($SQL);
            $data['HEADER'] = $this->actMain->get_result($SQL);
            
            $SQL2="select b.NFREIGHT, b.NASURANSI, b.REGION from m_negara a LEFT JOIN m_tod b on b.REGION = a.REGION and b.JNSMODA = '".$data['HEADER']['MODA']."' where a.KODE_NEGARA ='".$data['HEADER']['NEGTUJU']."'";
            $data['HITUNG'] = $this->actMain->get_result($SQL2);
            $data['act']    = 'update'; 
        }
        else {
            $SQL    = "SELECT 	a.KODE_ID 'IDEKS', 
                                a.ID 'NPWPEKS', 
                                a.NAMA 'NAMAEKS', 
                                a.ALAMAT 'ALMTEKS', 
                                b.NIPER 'NIPER', 
                                b.STATUSH 'STATUSH', 
                                c.URAIAN 'URSTATUSH', 
                                b.NOTDP 'NOTDP', 
                                b.TGTDP 'TGTDP', 
                                b.NAMA_TTD,
                                b.KOTA_TTD,
                                a.KODE_ID 'IDPPJK', 
                                a.ID 	'NPWPPPJK', 
                                a.NAMA 	'NAMAPPJK', 
                                a.ALAMAT 'ALMTPPJK', 
                                a.NO_PPJK 'NOPPJK', 
                                a.TANGGAL_PPJK 'TGPPJK',
                                a.TIPE_TRADER
                FROM m_trader a INNER JOIN m_trader_bc30 b ON a.KODE_TRADER = b.KODE_TRADER
                                LEFT JOIN  m_tabel       c ON c.MODUL = 'BC30' AND c.KDTAB = 'STATUS_EKS' AND c.KDREC = b.STATUSH 
                WHERE a.KODE_TRADER = " . $this->kd_trader;
            $data['HEADER'] = $this->actMain->get_result($SQL);
            $data['act']    = 'save';
        }
        $data['JNEKS']      =   $this->actMain->get_mtabel('JNEKS', 1,true,'','KODEDANUR','BC30');
        $data['KATEKS']     =   $this->actMain->get_mtabel('KATEKS',1,true,'','KODEDANUR','BC30');
        $data['JNDAG']      =   $this->actMain->get_mtabel('JNDAG', 1,true,'','KODEDANUR','BC30');
        $data['JNBYR']      =   $this->actMain->get_mtabel('JNBYR', 1,true,'','KODEDANUR','BC30');
        $data['TMP_PERIKSA'] = $this->actMain->get_mtabel('TMP_PERIKSA', 1,true,'','KODEDANUR','BC30');
        $data['TIPE_TRADER'] = $this->newsession->userdata('TIPE_TRADER');
        $data['JENIS_IDENTITAS'] = $this->actMain->get_mtabel('JENIS_IDENTITAS', 1, true,'','KODEDANUR','BC30');
        $data['MODA']       = $this->actMain->get_mtabel('MODA', 1, false,'','KODEDANUR','BC30');
        $data['DELIVERY']   = $this->actMain->get_mtabel('DELIVERY', 1, true,'','KODEDANUR','BC30');
        $data['KDASS']      = $this->actMain->get_mtabel('KDASS', 1, false,'','KODEDANUR','BC30');
        $data['KOMODITI']   = $this->actMain->get_mtabel('KOMODITI', 1, true,'','KODEDANUR','BC30');
        $data['STATUSH']    = $this->actMain->get_mtabel('STATUS_EKS', 1, true,'','KODEDANUR','BC30');
        $data['MAXLENGTH']  = '<input type="hidden" id="MAXLENGTH" value="'.$this->actMain->get_uraian("SELECT F_MAXLENGTH('t_bc30hdr') as MAXLENGTH",'MAXLENGTH').'"/>';
        if($aju != ''){
            $data['DATA_DOKUMEN']   = $this->get_tblDokumen($aju);
            $data['DATA_CONTAINER'] = $this->get_tblKontainer($aju);
            $data['DATA_KEMASAN']   = $this->get_tblKemasan($aju);
        }
        return $data;
    }
    
    function setHeader(){
        $this->load->model("actMain");
        $data = $this->input->post();
        if($data['act']=='save'){
            $data['HEADER']['CAR'] = $this->actMain->get_car('BC30');
        }
        $data['HEADER']['KODE_TRADER']  = $this->kd_trader;
        $data['HEADER']['NPWPEKS']      = str_replace('.', '', str_replace('-', '', $data['HEADER']['NPWPEKS']));
        $data['HEADER']['NPWPQQ']       = str_replace('.', '', str_replace('-', '', $data['HEADER']['NPWPQQ']));
        $data['HEADER']['NPWPPPJK']     = str_replace('.', '', str_replace('-', '', $data['HEADER']['NPWPPPJK']));
        
        $this->actMain->insertRefernce('M_TRADER_EKSPORTIR', $data['HEADER']);
        $this->actMain->insertRefernce('M_TRADER_INDENTOREKS', $data['HEADER']);
        $this->actMain->insertRefernce('M_TRADER_PEMBELI', $data['HEADER']);
        $this->actMain->insertRefernce('M_TRADER_KAPAL', $data['HEADER']);
        switch ($data['HEADER']['DELIVERY']){
            case 'CIF':$data['HEADER']['KDHRG']='2';break;
            case 'CFR':$data['HEADER']['KDHRG']='3';break;
            default :$data['HEADER']['KDHRG']='1';break;
        }
        $data['HEADER']['NILINV'] = $data['HEADER']['FOB'];
        $exe = $this->actMain->insertRefernce('T_BC30HDR', $data['HEADER']);
        $msg = $this->validasiBC30HDR($data['HEADER']['CAR']);
        if($exe){
            $this->actMain->set_car('BC30');
            return "MSG|OK|".site_url('bc30/create/'.$data['HEADER']['CAR'])."|".$msg;
        }
        else {
            return "MSG|ERR||Data header gagal diproses.";
        }
        
    }
    
    function delHDR($car){
        $this->load->model("actMain");
        $sql = "SELECT CONCAT(a.`STATUS`,'|',b.URAIAN) as STATUS 
                FROM t_bc30hdr a left join m_tabel b on b.MODUL = 'BC30' AND 
                                                   b.KDTAB = 'STATUS' AND 
                                                   b.KDREC =  a.STATUS 
                WHERE a.KODE_TRADER = '".$this->kd_trader."' AND 
                      a.CAR = '".$car."'";
        $sts = $this->actMain->get_uraian($sql,'STATUS');
        $arrSts = explode('|', $sts);
        if(strpos('|20|30|40|50|60|',$arrSts[0])){
            $rtn = 'MSG|ER|' . site_url('bc30/daftar'). '|Dokumen tidak dapat dihapus karena berstatus '.$arrSts[1].'.';
        }else{
            $key    = array('KODE_TRADER' =>  $this->kd_trader, 'CAR' => $car);
            $this->db->where($key);
            $tbl    = explode(',','t_bc30hdr,t_bc30kms,t_bc30dok,t_bc30con,t_bc30dtl,t_bc30pkb,t_bc30gab,t_bc30gabdtl,t_bc30gabdok');
            $exec = $this->db->delete($tbl);
            $rtn = 'MSG|OK|' . site_url('bc30/daftar') . '|Delete data dokumen PEB Berhasil.';            
        }
        return $rtn;
    }
    
    function validasiBC30HDR($car) { 
        $this->load->model('actMain');
        $tipe_trader = $this->newsession->userdata('TIPE_TRADER');
        $i = 0;
        $query = "SELECT * FROM m_trader A  WHERE A.KODE_TRADER = " . $this->kd_trader;
        $strMy = $this->actMain->get_result($query);
        
        $query = "SELECT A.* FROM t_bc30hdr A WHERE A.CAR = '" . $car . "' AND A.KODE_TRADER = " . $this->kd_trader;
        $aH = $this->actMain->get_result($query);
        if (count($aH) > 0) {
            $judul = 'Hasil validasi PEB Nomor Aju : ' . substr($car, 20) . ' tanggal ' . date('d-m-Y',strtotime(substr($car, 12, 8)));
            $query = "SELECT FLDNAME,MESSAGE from m_validasi WHERE SCRNAME = 'T_BC30HDR' AND MANDATORY = 1 ";
            $arrdataV = $this->actMain->get_result($query, true);
            foreach ($arrdataV as $aV) {
                if (trim($aH[$aV['FLDNAME']]) == '') {
                    $hasil .= substr('   ' . ++$i, -3) . '. ' . $aV['MESSAGE'] . ' Harus diisi<br>';
                }
            }
            unset($arrdataV);
            //#########Validasi Khusus########
            
            //==========jika pelabuhan muat ekspor kosong========
            if($aH['PELMUATEKS'] == ""){
                $aH['PELMUATEKS'] = $aH['PELMUAT'];
            }
            
            //===========Cek jika jumlah barang < 0===========
            if ((int) $aH['JMBRG'] < 0) {
                $hasil .= substr('   ' . ++$i, -3) . '. Jumlah Barang Tidak Boleh Negatif<br>';
                $aH['JMBRG'] = 0;
            }
            
            //============Cek Jika Jenis Ekspor==============
            if (strpos("|21|22|23|", $aH['KATEKS'])) {
                if($aH['NIPER']==''){
                    $hasil .= substr('   ' . ++$i, -3) . '. [Ekspor KITE] NIPER harus diisi<br>';
                }
                $queryPKB = "SELECT COUNT(CAR) as jmlPKB FROM t_bc30pkb WHERE CAR = '" . $car . "' AND KODE_TRADER = " . $this->kd_trader;
                $jmlPKB = $this->actMain->get_uraian($queryPKB, 'jmlPKB');
                if ($jmlPKB <= 0) {
                    $hasil .= substr('   ' . ++$i, -3) . '. [Ekspor KITE] KPB harus diisi<br>';
                }
            }
            elseif (strpos("|41|42|43|44|45|46|", $aH['KATEKS']) && $aH['LOKTPB']=='') {
                $hasil .= substr('   ' . ++$i, -3) . '. Lokasi TPB harus diisi<br>';
            }
            
            //==========Unsur Harga=========
            if ($aH['NILINV'] < 0 || $aH['ASURANSI'] < 0 || $aH['FREIGHT'] < 0 || $aH['DISCOUNT'] < 0) {
                $hasil .= substr('   ' . ++$i, -3) . '. Unsur harga tidak boleh kurang dari 0<br>';
            }
            
            //=========Validasi Berat===========
            if ($aH['BRUTO'] < $aH['NETTO']) {
                $hasil .= substr('   ' . ++$i, -3) . '. Brutto tidak boleh lebih kecil dari netto<br>';
            }
            
            //========cek isian kemasan==========
            $queryKms = "SELECT COUNT(CAR) as jmlKMS FROM t_bc30kms WHERE CAR = '" . $car . "' AND KODE_TRADER = " . $this->kd_trader;
            $jmlKms = $this->actMain->get_uraian($queryKms, 'jmlKMS');
            if ($jmlKms <= 0) {
                $hasil .= substr('   ' . ++$i, -3) . '. Data kemasan masih kosong.<br>';
            }
            
            #jika jenis perusahaan ppjk identitas ppjk harus diisi
            if($tipe_trader == '2'){
                $hasil .= ($aH['IDPPJK']=='')?substr('   ' . ++$i, -3) . '. Jenis Identitas Perusahaan PPJK Harus diisi.<br>':'';
                $hasil .= ($aH['NPWPPPJK']=='')?substr('   ' . ++$i, -3) . '. Nomor Identitas Perusahaan PPJK Harus diisi.<br>':'';
                $hasil .= ($aH['NAMAPPJK']=='')?substr('   ' . ++$i, -3) . '. Nama Perusahaan PPJK Harus diisi.<br>':'';
                $hasil .= ($aH['ALMTPPJK']=='')?substr('   ' . ++$i, -3) . '. Alamat Perusahaan PPJK Harus diisi.<br>':'';
                $hasil .= ($aH['NOPPJK']=='')?substr('   ' . ++$i, -3) . '. Nomor Ijin Perusahaan PPJK Harus diisi.<br>':'';
                $hasil .= ($aH['TGPPJK']=='')?substr('   ' . ++$i, -3) . '. Tanggal Ijin Perusahaan PPJK Harus diisi.<br>':'';
            }
            
            #jika Katagori ekspor 41 = KB,42 = GB,43 = ETP,44 = TBB,45 = TLB,46 = KDU
            switch($aH['KATEKS']){
                case'41':$aH['JNTPB']='KB';break;
                case'42':$aH['JNTPB']='GB';break;
                case'43':$aH['JNTPB']='ETP';break;
                case'44':$aH['JNTPB']='TBB';break;
                case'45':$aH['JNTPB']='TLB';break;
                case'46':$aH['JNTPB']='KDU';break;
                default :$aH['JNTPB']=null;break;
            }
            
            $aH['KTRGATE'] = $aH['KDKTR'];
            
            //==========Cek Pengisian Harga===========
            if($aH['FOB'] < '0' && $aH['JNEKS'] != '4'){
                $hasil .= substr('   ' . ++$i, -3) . '. FOB tidak boleh kurang dari 0.<br>';
            }
            
           
            //===========validasi tanggal============= ada yang mau ditanya
            if($aH['TGEKS'] != '0000-00-00' && $aH['TGEKS'] != '' && str_replace('-', '', $aH['TGEKS']) < substr($aH['CAR'],12,8) ){
                $hasil .= substr('   ' . ++$i, -3) . '. Tanggal Ekspor tidak boleh < tanggal aju.<br>';
            }
             if($aH['TGEKS'] != '0000-00-00' && $aH['TGEKS'] != '' && (str_replace('-', '', $aH['TGEKS']) - 7) < substr($aH['CAR'],12,8) ){
                $hasil .= substr('   ' . ++$i, -3) . '. Tanggal Ekspor tidak boleh > 7 hari dari tanggal aju.<br>';
            }
            if($aH['TGSIAP'] != '0000-00-00' && $aH['TGSIAP'] != '' && str_replace('-', '', $aH['TGSIAP']) < substr($aH['CAR'],12,8) ){
                $hasil .= substr('   ' . ++$i, -3) . '. Tanggal pemeriksaan tidak boleh < tanggal aju.<br>';
            }
            
            if (!$dok['COO'] && strstr('|06|54|55|56|57|58|', $aH['KDFAS'])) {
                $hasil .= substr('   ' . ++$i, -3) . '. Ada fasilitas ' . $aH['KDFAS'] . ' tetapi belum mengisi Dokumen SKA/COO [kode 861]<br>';
            }
            
            //==========Cek Pengisian Dokumen==========
            $queryDok = "SELECT GROUP_CONCAT(KDDOK separator '|') as ADOK FROM t_bc30dok WHERE CAR = '" . $car . "' AND KODE_TRADER = " . $this->kd_trader;
            $strDok = $this->actMain->get_uraian($queryDok, 'ADOK');
            $dok['ADAINVOICE'] = $this->actMain->findArr2Str($strDok, array('380'));
            $dok['ADAPL']      = $this->actMain->findArr2Str($strDok, array('217'));
            $dok['ADAMOU']     = $this->actMain->findArr2Str($strDok, array('500'));
            $dok['ADABANK']    = $this->actMain->findArr2Str($strDok, array('111'));
            if (!$dok['ADABANK']) {
                $hasil .= substr('   ' . ++$i, -3) . '. Nama Bank DHE belum diisi<br>';
            }
            if (!$dok['ADAINVOICE']) {
                $hasil .= substr('   ' . ++$i, -3) . '. Dokumen Invoice belum diisi<br>';
            }
            if (!$dok['ADAPL']) {
                $hasil .= substr('   ' . ++$i, -3) . '. Dokumen Packing List belum diisi<br>';
            }
            
            if (!strstr('|02|03|04|15|20|30|40|17|70|90|50|60|',$aH['STATUS'])){
                if ($tipe_trader == '2')
                {
                    $aH['IDPPJK']   = $strMy['KODE_ID'];
                    $aH['NPWPPPJK'] = $strMy['ID'];
                    $aH['NAMAPPJK'] = $strMy['NAMA'];
                    $aH['ALMTPPJK'] = $strMy['ALAMAT'];                                  
                }else{
                    $aH['IDEKS']   = $strMy['KODE_ID'];
                    $aH['NPWPEKS'] = $strMy['ID'];
                    $aH['NAMAEKS'] = $strMy['NAMA'];
                    $aH['ALMTEKS'] = $strMy['ALAMAT'];  
                    
                    $aH['IDPPJK']   = "";
                    $aH['NPWPPPJK'] = "";
                    $aH['NAMAPPJK'] = "";
                    $aH['ALMTPPJK'] = "";  
                    $aH['NOPPJK']   = "";
                    $aH['TGPPJK']   = NULL;
                }
            }
            
            switch ($aH['IDEKS']) {
                case '0': 
                    if (strlen($aH['NPWPEKS'])!= '12') {
                        $hasil .= substr('   ' . ++$i, -3) . '. Pengisian Identitas + NPWP Eksportir salah<br>';
                    }
                    break;
                case '1':
                    if (strlen($aH['NPWPEKS'])!= '10') {
                        $hasil .= substr('   ' . ++$i, -3) . '. Pengisian Identitas + NPWP Eksportir salah<br>';
                    }
                    break;
                case '6':
                    if (strlen($aH['NPWPEKS'])!= '15') {
                        $hasil .= substr('   ' . ++$i, -3) . '. Pengisian Identitas + NPWP Eksportir salah<br>';
                    }
                    break;
            }
            
            $query = "SELECT A.* FROM t_bc30dtl A WHERE A.CAR = '" . $car . "' AND A.KODE_TRADER = " . $this->kd_trader;
            $aDtl  = $this->actMain->get_result($query, true);
            $query = "SELECT FLDNAME,MESSAGE,TIPE from m_validasi WHERE SCRNAME = 'T_BC30DTL' AND MANDATORY = 1 ";
            $aDtlV = $this->actMain->get_result($query, true);
            
            foreach ($aDtl as $key => $val) {
                $rtnDtl = $this->validasiDetil($val,$aDtlV,$aH,true);
                if(!$rtnDtl){
                    $hasil .= substr('   ' . ++$i, -3) . '. Data Barang dengan seri '. $val['SERIBRG'] .' belum lengkap <br>';
                }
            }
            
            //======================== Cek Pengisian PEB Gabungan =================
            $query = "SELECT A.* FROM t_bc30gab A  WHERE A.CAR = '" . $car . "' AND A.KODE_TRADER = " . $this->kd_trader;
            $aPebGab = $this->actMain->get_result($query, true);
            
            $query = "SELECT FLDNAME,MESSAGE from m_validasi WHERE SCRNAME = 'T_BC30GAB' AND MANDATORY = 1";
            $aPebGabV = $this->actMain->get_result($query, true);
            
            $query = "SELECT FLDNAME,MESSAGE from m_validasi WHERE SCRNAME = 'T_BC30GABDTL' AND MANDATORY = 1";
            $aPebGabDtlV = $this->actMain->get_result($query, true);
            
            foreach ($aPebGab as $key => $val) {
                $rtnPebGab = $this->validasiPebGabBacth($val,$aPebGabV, $aPebGabDtlV ,true);
                if(!$rtnPebGab){
                    $hasil .= substr('   ' . ++$i, -3) . '. Data PEB Gabungan dengan seri '. $val['SERIEKS'] .' belum lengkap <br>';
                }
            }
            
            //================ Cek Pengisian PKB ================
            $query = "SELECT A.* FROM t_bc30pkb A  WHERE A.CAR = '" . $car . "' AND A.KODE_TRADER = " . $this->kd_trader;
            $aPKB = $this->actMain->get_result($query, true);
            
            $query = "SELECT FLDNAME,MESSAGE from m_validasi WHERE SCRNAME = 'T_BC30PKB' AND MANDATORY = 1";
            $aPkbV = $this->actMain->get_result($query, true);
            foreach ($aPKB as $key => $val) {
                $rtnPkbDtl = $this->validasiPkbBacth($val,$aPkbV,true);
                if(!$rtnPkbDtl){
                    $hasil .= substr('   ' . ++$i, -3) . '. Data Detail PKB belum lengkap <br>';
                }
            }
            
            //Resume detil error
            if (count($arrSerial) > 0) {
                $hasil .= substr('   ' . ++$i, -3) . '. Ada ' . count($arrSerial) . ' data barang yang belum benar.<br>';
            }
            
           
            // VALIDASI JUMLAH PENGISIAN PEB GABUNGAN
            $query = "SELECT COUNT(B.SERIEKS) AS 'SERI', A.JMBRGGAB AS 'JUMLAH'
                           FROM t_bc30hdr A
                      LEFT JOIN t_bc30gab B ON A.KODE_TRADER = B.KODE_TRADER AND A.CAR = B.CAR
                      WHERE A.CAR = '" . $car . "' AND A.KODE_TRADER = " . $this->kd_trader;
            $aSERI = $this->actMain->get_result($query);
            if($aSERI['SERI'] < $aSERI['JUMLAH']){
                $hasil .= substr('   ' . ++$i, -3) . '. Jumlah PEB Gabungan Belum Sesuai.<br>';
            }
            unset($aSERI);
            
            //update status [exist coy]
            if ($aH['STATUS'] == ''){$aH['STATUS'] = '00';}
            
            #tidak ada perubahan jika status diluar 010 dan 020
            if (strpos('|00|01|', $aH['STATUS'])){
                $aH['STATUS'] = ($i == 0) ? '01' : '00';
            }
            
            #validasi lartas
            $query = "SELECT b.HS_CODE, D.NMGA, GROUP_CONCAT(DISTINCT '- ',C.UR_IJIN,' ','(Kode Dokumen:',C.KD_MODUL,')', ' ', D.NMGA separator '<br>') as 'Ijin'
                        FROM m_ter_lartas_eksportir as b inner join t_bc30dtl as a on left(a.HS,10) = b.HS_CODE
                        LEFT JOIN m_ter_lic_ijin_eksportir C ON C.KD_IJIN=B.KD_IJIN
                        LEFT JOIN m_ga D ON D.ID_GA=C.KD_GA
                        WHERE CAR = '".$car."'
                        GROUP BY b.HS_CODE";
            //die($query);
            $HS_Lartas  = $this->actMain->get_result($query, true);
            if($HS_Lartas['HS'] == $HS_Lartas['HS_CODE'] && $aH['STATUS'] == '01'){
                $hasil .= substr('   ' . '', 4) .'#==============================================PERINGATAN==============================================#'.'<br>';
                foreach($HS_Lartas as $hs_l){
                $no++;
                $hasil .= substr('   ' . $no, -3) . '. Detail barang dengan HS:'.$hs_l['HS_CODE'].' Terkena Larangan dan Pembatasan. <br>';
                $hasil .= substr('   ' . '', 4) .$hs_l['Ijin'].'<br><br>';
                }
            }
            unset($HS_Lartas);
            
            
            
            //apply ke database
            $upd = $aH;
            unset($upd['URSTATUS']);
            $this->db->where(array('CAR' => $car, 'KODE_TRADER' => $this->kd_trader));
            $exec = $this->db->update('t_bc30hdr', $upd);
            unset($upd);
            //tulis informasi
            if ($i == 0) {
                $hasil .= '<br>------------------------- Data Lengkap -------------------------<br>';
            } else {
                $hasil .= '<br>---------------------- Data Tidak Lengkap ----------------------<br>';
            }
            
        }
        $view['data'] = $hasil;
        $view['judul'] = $judul;
        $msg = $this->load->view('notify', $view, true);
        unset($aH);
        return $msg;
    }
    
    private function validasiDetil($arrDtl,$arrValidasi,$aH,$bacth=false){
        $i = 0;
        foreach ($arrValidasi as $vld){
            if (($arrDtl[$vld['FLDNAME']] == '' && $vld['TIPE']=='') || ($arrDtl[$vld['FLDNAME']] <= 0 && $vld['TIPE']=='NUMBER') || (($arrDtl[$vld['FLDNAME']] == '' || $arrDtl[$vld['FLDNAME']]=='0000-00-00') && $vld['TIPE']=='DATE') || (($arrDtl[$vld['FLDNAME']] == '' || $arrDtl[$vld['FLDNAME']]=='00:00:00') && $vld['TIPE']=='TIME') || (($arrDtl[$vld['FLDNAME']] == '' || $arrDtl[$vld['FLDNAME']]=='0000-00-00 00:00:00') && $vld['TIPE']=='DATETIME')) {
                $hasil .= substr('   ' . ++$i, -3) . '. ' . ucwords(strtolower($vld['MESSAGE'])) . ' Harus diisi<br>';
            }
        }
        if(!$arrDtl['NEGASAL']){
            $hasil .= substr('   ' . ++$i, -3) . '. Negara Asal Barang Harus diisi.<br>';
        }
        if($arrDtl['JMSATUAN'] > '0'){
            $arrDtl['FOBPERSAT']= round($arrDtl['FOBPERBRG'] / $arrDtl['JMSATUAN'], 4);
        }else{
            $arrDtl['JMSATUAN']='0';
        }
        
        //Bea Keluar (11-05-2009)
        if($arrDtl['KDPE'] == '1'){
            $arrDtl['PEPERBRG']=$arrDtl['HPATOK']*$arrDtl['JMSATPE']*$arrDtl['TARIPPE']*$arrDtl['NILVALPE']/100;
           
        }elseif($arrDtl['KDPE'] == '2'){
            $arrDtl['PEPERBRG']=$arrDtl['JMSATPE']*$arrDtl['TARIPPE']*$arrDtl['NILVALPE'];
            
        }else{
            $arrDtl['PEPERBRG']='0';
        }
        
        //Jika nilai barang negatif
        if($arrDtl['FOBPERBRG'] < '0'){
            $hasil .= substr('   ' . ++$i, -3) . '. Harga tidak boleh < 0.<br>';
        }
        //validasi PJT
        /*
        if($aH['KATEKS'] == '33'){
            $hasil .= substr('   ' . ++$i, -3) . '. PEB PJT, belum mengisi data pengirim/penerima barang.<br>';
            //if(!$arrDtl['NAMAEKST'] && !$arrDtl['NAMABELIT']){
            //    $hasil .= substr('   ' . ++$i, -3) . '. PEB PJT, belum mengisi data pengirim/penerima barang.<br>';
           // }
        }*/
        
        //Cek panjang isian hs
        if(strlen($arrDtl['HS'])!= '12'){
            $hasil .= substr('   ' . ++$i, -3) . '. Pengisian HS salah.<br>';
        }
        $arrDtl['DTLOK'] = ($i == 0) ? '1' : '0';
        $this->db->where(array('CAR' => $arrDtl['CAR'], 'SERIBRG'=> $arrDtl['SERIBRG'],'KODE_TRADER' => $this->kd_trader));
        $this->db->update('t_bc30dtl', $arrDtl);

        //tulis informasi
        if ($i == 0) {
            $hasil .= '<br>------------------ Data Lengkap ------------------<br>';
        } else {
            $hasil .= '<br>--------------- Data Tidak Lengkap ---------------<br>';
        }
        
        if($bacth){
            if($i==0){
                $rtn = true;
            }else{
                $rtn = false;
            }
        }else{
            $view['data'] = $hasil;
            $view['judul'] = $judul;
            $rtn = $this->load->view('notify', $view, true);
        }
        return $rtn;
    }
    
    private function validasiPebGabBacth($arrPebGab,$arrValidasi,$aPebGabDtlV,$bacth=false){
        $i = 0;
        foreach ($arrValidasi as $vld){
            
            if (($arrPebGab[$vld['FLDNAME']] == '' && $vld['TIPE']=='') || 
                    ($arrPebGab[$vld['FLDNAME']] <= 0 && $vld['TIPE']=='NUMBER') || 
                    (($arrPebGab[$vld['FLDNAME']] == '' || $arrPebGab[$vld['FLDNAME']]=='0000-00-00') && $vld['TIPE']=='DATE') || 
                    (($arrPebGab[$vld['FLDNAME']] == '' || $arrPebGab[$vld['FLDNAME']]=='00:00:00') && $vld['TIPE']=='TIME') || 
                    (($arrPebGab[$vld['FLDNAME']] == '' || $arrPebGab[$vld['FLDNAME']]=='0000-00-00 00:00:00') && $vld['TIPE']=='DATETIME')) {
                $hasil .= substr('   ' . ++$i, -3) . '. ' . ucwords(strtolower($vld['MESSAGE'])) . ' Harus diisi<br>';
            }
        }
        
        //======================== Cek Pengisian PEB Gabungan Detail =================
        $query = "SELECT A.* FROM t_bc30gabdtl A  WHERE A.KODE_TRADER = " . $this->kd_trader." AND A.CAR = '" . $arrPebGab['CAR'] . "'  AND A.SERIEKS=".$arrPebGab['SERIEKS'] ;
        $aPebGabDtl = $this->actMain->get_result($query, true);
        
        foreach ($aPebGabDtl as $key => $val) {
            $rtnPebGabDtl = $this->validasiPebGabDtlBacth($val,$aPebGabDtlV,true);
            if(!$rtnPebGabDtl){
                $hasil .= substr('   ' . ++$i, -3) . '. Data Detail PEB Gabungan dengan seri '. $val['SERIEKS'] .' belum lengkap <br>';
            }
        }
        //validasi kusus masih belum dipindahkan

        $arrPebGab['GABOK'] = ($i == 0) ? '1' : '0';
        $this->db->where(array('CAR' => $arrPebGab['CAR'], 'SERIEKS'=> $arrPebGab['SERIEKS'],'KODE_TRADER' => $this->kd_trader));
        $this->db->update('t_bc30gab', $arrPebGab);
        
        if ($i == 0) {
            $hasil .= '<br>------------------ Data Lengkap ------------------<br>';
        } else {
            $hasil .= '<br>--------------- Data Tidak Lengkap ---------------<br>';
        }
            
        if($bacth){
            if($i==0){
                $rtn = true;
            }else{
                $rtn = false;
            }
        }else{
            $view['data'] = $hasil;
            $view['judul'] = $judul;
            $rtn = $this->load->view('notify', $view, true);
        }
        return $rtn;
    }
    
    private function validasiPebGabDtlBacth($arrPebGabDtl,$arrValidasi,$bacth=false){;
        $i = 0;
        foreach ($arrValidasi as $vld){
            if (($arrPebGabDtl[$vld['FLDNAME']] == '' && $vld['TIPE']=='') || ($arrPebGabDtl[$vld['FLDNAME']] <= 0 && $vld['TIPE']=='NUMBER') || (($arrPebGabDtl[$vld['FLDNAME']] == '' || $arrPebGabDtl[$vld['FLDNAME']]=='0000-00-00') && $vld['TIPE']=='DATE') || (($arrPebGabDtl[$vld['FLDNAME']] == '' || $arrPebGabDtl[$vld['FLDNAME']]=='00:00:00') && $vld['TIPE']=='TIME') || (($arrPebGabDtl[$vld['FLDNAME']] == '' || $arrPebGabDtl[$vld['FLDNAME']]=='0000-00-00 00:00:00') && $vld['TIPE']=='DATETIME')) {
                $hasil .= substr('   ' . ++$i, -3) . '. ' . ucwords(strtolower($vld['MESSAGE'])) . ' Harus diisi<br>';
            }
        }
        //validasi kusus masih belum dipindahkan
        
        $arrPebGabDtl['DTLGOK'] = ($i == 0) ? '1' : '0';
        $this->db->where(array('CAR' => $arrPebGabDtl['CAR'], 'SERIEKS'=> $arrPebGabDtl['SERIEKS'], 'SERIBRGG'=> $arrPebGabDtl[SERIBRGG],'KODE_TRADER' => $this->kd_trader));
        $this->db->update('t_bc30gabdtl', $arrPebGabDtl);
        //tulis informasi
        if ($i == 0) {
            $hasil .= '<br>------------------ Data Lengkap ------------------<br>';
        } else {
            $hasil .= '<br>--------------- Data Tidak Lengkap ---------------<br>';
        }
            
        if($bacth){
            if($i==0){
                $rtn = true;
            }else{
                $rtn = false;
            }
        }else{
            $view['data'] = $hasil;
            $view['judul'] = $judul;
            $rtn = $this->load->view('notify', $view, true);
        }
        return $rtn;
    }
    
    private function validasiPkbBacth($arrPKB,$arrValidasi,$bacth=false){;
        $i = 0;
        foreach ($arrValidasi as $vld){
            if ( ($arrPKB[$vld['FLDNAME']] == '' && $vld['TIPE']=='') || 
                 ($arrPKB[$vld['FLDNAME']] <= 0 && $vld['TIPE']=='NUMBER') || 
                (($arrPKB[$vld['FLDNAME']] == '' || $arrPKB[$vld['FLDNAME']]=='0000-00-00') && $vld['TIPE']=='DATE') || 
                (($arrPKB[$vld['FLDNAME']] == '' || $arrPKB[$vld['FLDNAME']]=='00:00:00') && $vld['TIPE']=='TIME') || 
                (($arrPKB[$vld['FLDNAME']] == '' || $arrPKB[$vld['FLDNAME']]=='0000-00-00 00:00:00') && $vld['TIPE']=='DATETIME')) {
                $hasil .= substr('   ' . ++$i, -3) . '. ' . ucwords(strtolower($vld['MESSAGE'])) . ' Harus diisi<br>';
            }
        }
        if($aDtl['tgstuff'] < $arrPKB['TGSIAP']){
            $hasil .= substr('   ' . ++$i, -3) .'. Tanggal stuffing tidak boleh < tanggal pemeriksaan.<br>';
        }
        if($arrPKB['TGSTUFF'] != '0000-00-00' && $arrPKB['TGSTUFF'] != '' && str_replace('-', '', $arrPKB['TGSTUFF']) < substr($arrPKB['CAR'],12,8) ){
            $hasil .= substr('   ' . ++$i, -3) . '. Tanggal Ekspor tidak boleh < tanggal aju.<br>';
        }
        $arrPKB['LENGKAP'] = ($i == 0) ? '1' : '0';
        $this->db->where(array('CAR' => $arrPKB['CAR'], 'KODE_TRADER' => $this->kd_trader));
        $this->db->update('t_bc30pkb', $arrPKB);
        
        if ($i == 0) {
            $hasil .= '<br>------------------ Data Lengkap ------------------<br>';
        } else {
            $hasil .= '<br>--------------- Data Tidak Lengkap ---------------<br>';
        }
        if($bacth){
            if($i==0){
                $rtn = true;
            }else{
                $rtn = false;
            }
        }else{
            $view['data'] = $hasil;
            $view['judul'] = $judul;
            $rtn = $this->load->view('notify', $view, true);
        }
        return $rtn;
    }
    
    function recordValidasiDetil($car,$seri){
        //print_r($car.'|'.$seri.'|'.'asas');die();
        $this->load->model('actMain');
        $query = "SELECT A.* FROM t_bc30dtl A WHERE A.KODE_TRADER = " . $this->kd_trader . " AND A.CAR = '" . $car . "' AND A.SERIBRG = ".$seri;
        $aDtl  = $this->actMain->get_result($query);
        
        $query = "SELECT FLDNAME,MESSAGE,TIPE from m_validasi WHERE SCRNAME = 'T_BC30DTL' AND MANDATORY = 1 ";
        $aDtlV = $this->actMain->get_result($query, true);
        $rtn = $this->validasiDetil($aDtl,$aDtlV);
        return $rtn;
    }
    
    function validasiPEBgab($car, $jmbrggab, $serieks ) {
        //print_r($car.'|'.$jmbrggab.'|'.$serieks);die();
        $this->load->model('actMain');
        $query = "SELECT A.* FROM t_bc30gab A  WHERE A.KODE_TRADER = " . $this->kd_trader . " AND A.CAR = '" . $car . "' AND A.SERIEKS =" . $serieks;
        $aPEBgab  = $this->actMain->get_result($query);
        
        $query = "SELECT FLDNAME,MESSAGE,TIPE from m_validasi WHERE SCRNAME = 'T_BC30GAB' AND MANDATORY = 1 ";
        $aDtlV = $this->actMain->get_result($query, true);
        $rtn = $this->validasiPebGabBacth($aPEBgab,$aDtlV);
        return $rtn;
    }
    
    function validasiPEBgabDTL($car, $serieks, $seribrgg) {
        //print_r($car.'|'.$serieks.'|'.$seribrgg);die();
        $this->load->model('actMain');
        $query = "SELECT A.* FROM t_bc30gabdtl A  WHERE A.KODE_TRADER = " . $this->kd_trader . " AND A.CAR = '" . $car . "' AND A.SERIEKS ='" . $serieks . "' AND A.SERIBRGG=". $seribrgg;
        $aPEBgabdTL  = $this->actMain->get_result($query);
        
        $query = "SELECT FLDNAME,MESSAGE,TIPE from m_validasi WHERE SCRNAME = 'T_BC30GABDTL' AND MANDATORY = 1 ";
        $aDtlV = $this->actMain->get_result($query, true);
        $rtn = $this->validasiPebGabDtlBacth($aPEBgabdTL,$aDtlV);
        return $rtn;
    }
    
    function validasiPKB($car) {
        $this->load->model('actMain');
        $query = "SELECT A.* FROM t_bc30pkb A  WHERE A.CAR = '" . $car . "' AND A.KODE_TRADER = " . $this->kd_trader;
        $aPKB  = $this->actMain->get_result($query);
        
        $query = "SELECT FLDNAME,MESSAGE,TIPE from m_validasi WHERE SCRNAME = 'T_BC30PKB' AND MANDATORY = 1 ";
        $aDtlV = $this->actMain->get_result($query, true);
        $rtn = $this->validasiPkbBacth($aPKB,$aDtlV);
        return $rtn;
    }
    
    private function informasi($arrH) {
        $this->load->model('actMain');
        $arti = explode('|', $this->actMain->get_uraian("SELECT URAIAN FROM M_TABEL WHERE MODUL = 'BC30' AND KDTAB = 'ARTI_STATUS' AND KDREC = '" . $arrH['STATUS'] . "'", 'URAIAN'));
        $hasil = 'Informasi Data PIB<br>';
        $hasil .= '--------------------------------------------------<br>';
        $hasil .= 'Nomor Aju              : ' . $arrH['CAR'] . '<br>';
        $hasil .= 'Status                 : ' . $arrH['URSTATUS'] . ' ( ' . $arti[0] . ' ) <br>';
        $hasil .= 'Keterangan Status      : ' . $arti[1] . ' <br>';
        $hasil .= 'Jumlah Barang          : ' . str_pad(number_format($arrH['JMBRG']), 20, ' ', STR_PAD_LEFT) . ' Barang<br>';
        $hasil .= 'Jumlah Kontainer       : ' . str_pad(number_format($arrH['JMCONT']), 20, ' ', STR_PAD_LEFT) . ' Kontainer<br>';
        $hasil .= 'Bruto                  : ' . str_pad(number_format($arrH['BRUTO'], 4), 20, ' ', STR_PAD_LEFT) . ' Kgm<br>';
        $hasil .= 'Netto                  : ' . str_pad(number_format($arrH['NETTO'], 4), 20, ' ', STR_PAD_LEFT) . ' Kgm<br>';
        $hasil .= 'CIF (' . $arrH['KDVAL'] . ')              : ' . str_pad(number_format($arrH['CIF'], 2), 20, ' ', STR_PAD_LEFT) . ' <br>';
        $hasil .= 'SNRF                   :' . $arrH['SNRF'] . ' <br>';
        $hasil .= 'Respons                :<br>';

        $SQL = "SELECT CONCAT(A.RESKD,' - ', B.URAIAN) AS RESPON "
                . "FROM T_BC30RES A LEFT JOIN M_TABEL B ON B.MODUL = 'BC30' AND B.KDTAB = 'JENIS_RESPON' AND B.KDREC = A.RESKD "
                . 'WHERE A.KODE_TRADER = ' . $this->kd_trader . " AND A.CAR = '" . $arrH['CAR'] . "'";
        $res = $this->actMain->get_result($SQL, true);
        if (isset($res)) {
            foreach ($res as $a) {
                $hasil .= '                       : ' . $a['RESPON'].'<br>';
            }
        }
        $hasil .= 'Pemberitahu            : ' . $arrH['NAMA_TTD'];
        unset($arrH);
        return $hasil;
    }
    
    function lsvDetil($type, $aju) {
        $this->load->library('newtable');
        $this->newtable->keys = '';
        if ($type == "barang") {
            $this->load->model('actMain');
            $SQL = "SELECT  f_formaths(a.HS) as 'Kode HS', 
                            a.URBRG1 'Uraian Barang', 
                            a.DMERK as 'Merk', 
                            a.SIZE as 'Ukuran', 
                            a.`TYPE` as 'Tipe', 
                            a.KDBRG as 'Kode', 
                            format(a.DNILINV,4) as 'Harga', 
                            format(a.JMSATUAN,0) as 'Jml Satuan',
                            a.JNSATUAN as 'Jns Satuan',
                            format(a.NETDET ,0) as 'Netto',
                            b.URAIAN as 'Status',
                            a.CAR,
                            a.SERIBRG
                    FROM t_bc30dtl a LEFT JOIN m_tabel b on b.MODUL = 'BC30' AND b.KDTAB = 'STATUS_DETIL' AND b.KDREC = a.DTLOK 
                    WHERE a.CAR= '$aju' AND a.KODE_TRADER = '".$this->kd_trader."'";
            $this->newtable->keys(array('CAR', 'SERIBRG'));
            $this->newtable->hiddens(array('CAR','SERIBRG'));
            $this->newtable->search(array(array('a.HS', 'Kode HS'),
                                          array('a.URBRG1','Uraian Barang'),
                                          array('a.DMERK','Merk'),
                                          array('a.`TYPE`','Uraian Barang'),
                                          array('a.KDBRG','Uraian Barang'),
                                         array('a.DTLOK', 'Status', 'tag-select', $this->actMain->get_mtabel('STATUS_DETIL',1,true,'','KODEUR','BC30'))));
            $this->newtable->orderby("SERIBRG");
            $this->newtable->sortby("ASC");
        }
        else if ($type == "kemasan") {
            $SQL = "SELECT  CONCAT(JNKEMAS,' - ',b.URAIAN_KEMASAN) AS 'Kemasan', 
                            MERKKEMAS AS 'Merek', 
                            JMKEMAS AS 'Jumlah', 
                            JNKEMAS,
                            CAR 
                    FROM t_bc30kms a Left Join m_kemasan b ON a.JNKEMAS = b.KODE_KEMASAN
                    WHERE CAR= '$aju' AND KODE_TRADER = '".$this->kd_trader."'";
            $this->newtable->keys(array('CAR', 'JNKEMAS', 'Merek'));
            $this->newtable->hiddens(array('CAR', 'JNKEMAS'));
            $this->newtable->search(array(
                array("CONCAT(JNKEMAS,' - ',b.URAIAN_KEMASAN)", 'Kemasan'), 
                array("MERKKEMAS", 'Merek')));
            $this->newtable->orderby(1);
            $this->newtable->sortby("DESC");
        } 
        else if ($type == "kontainer") {
            $SQL = "SELECT  a.NOCONT as 'Nomor', 
                            a.SIZE   as 'Ukuran',
                            c.URAIAN as 'Stuff',
                            b.URAIAN as 'Tipe',
                            d.URAIAN as 'Part Of',
                            CAR
                    FROM t_bc30con a Left Join m_tabel b ON b.MODUL = 'BC30' AND b.KDTAB = 'TYPE_CONT' AND b.KDREC = a.TYPE
                                     Left Join m_tabel c ON c.MODUL = 'BC30' AND c.KDTAB = 'STUFF' AND c.KDREC = a.STUFF
                                     Left Join m_tabel d ON d.MODUL = 'BC30' AND d.KDTAB = 'JNPARTOF' AND d.KDREC = a.JNPARTOF
                    WHERE CAR= '$aju' AND KODE_TRADER = '".$this->kd_trader."'";
            $this->newtable->keys(array("CAR", "Nomor"));
            $this->newtable->hiddens(array("CAR"));
            $this->newtable->search(array(array('NOCONT', 'Nomor'), array('UKURAN_CONTAINER', 'Ukuran'), array('JENIS_CONTAINER', 'Tupe')));
            $this->newtable->orderby(4);
            $this->newtable->sortby("DESC");
        }
        else if ($type == "dokumen") {
            $SQL = "SELECT  KDDOK,KDDOK AS 'Kode',
                            b.URAIAN AS 'Jenis Dokumen', 
                            NODOK AS 'Nomor',
                            DATE_FORMAT(TGDOK,'%Y-%m-%d') AS 'Tanggal',
                            CAR
                    FROM t_bc30dok a Left Join m_tabel b on b.Modul = 'BC30' AND b.KDTAB = 'KDDOK' AND b.KDREC = a.KDDOK
                    WHERE CAR= '$aju' AND KODE_TRADER = '".$this->kd_trader."'";
            $this->newtable->keys(array('CAR', 'KDDOK', 'Nomor'));
            $this->newtable->hiddens(array('CAR','KDDOK'));
            $this->newtable->search(array(
                array('KDDOK', 'Kode'),
                array('b.URAIAN', 'Jenis'),
                array('NODOK', 'Nomor'),
                array("DATE_FORMAT(TGDOK,'%Y-%m-%d')", 'Tanggal', 'tag-tanggal')));
            $this->newtable->orderby(4);
            $this->newtable->sortby("DESC");
        }
        else {
            return "Failed";
            exit();
        }
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->action(site_url('bc30/daftarDetil/'.$type.'/'.$aju));
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->rowcount(10);
        $this->newtable->clear();
        $this->newtable->set_formid("f".$type);
        $this->newtable->set_divid("div".$type);
        $process = array(
            'Tambah '   . $type => array('ADDAJAX', site_url('bc30/' . $type . '/' . $aju), '0', 'f' . $type . '_form'),
            'Ubah '     . $type => array('EDITAJAX', site_url('bc30/' . $type . '/' . $aju), '1', 'f' . $type . '_form'),
            'Hapus '    . $type => array('DELETEAJAX', site_url('bc30/' . $type . '/' . $aju), 'N', 'f' . $type . '_list'));
        $this->newtable->menu($process);
        $tabel = $this->newtable->generate($SQL);
        $arrdata = array('tabel' => $tabel);
        if ($this->input->post('ajax')){
            return $tabel;
        }else{
            return $arrdata;
        }
    }
    
    private function get_tblDokumen($car) {
        $arrdata = $this->lsvDetil('dokumen', $car);
        $data['list'] = $this->load->view('list', $arrdata, true);
        $data['type'] = 'dokumen';
        return $this->load->view("bc30/lst", $data, true);
    }
    
    function getDokumen($car = "", $DOKKD = "", $DOKNO = "") {
        $this->load->model("actMain");
        if ($car && $DOKKD && $DOKNO) {
            $query = "SELECT * FROM t_bc30dok WHERE KODE_TRADER='" . $this->kd_trader . "' AND CAR = '$car' AND KDDOK = '$DOKKD' AND NODOK = '$DOKNO'";
            $hasil = $this->actMain->get_result($query);
            $data['DOKUMEN'] = array_change_key_case($hasil, CASE_UPPER);
            $data['act'] = 'update';
        }
        else {
            $data['act'] = 'save';
        }
        $data['DOKUMEN']['CAR'] = $car;
        $data['KDDOK'] = $this->actMain->get_mtabel('KDDOK', 1, true, '', 'KODEDANUR','BC30');
        return $data;
    }
    
    function setDokumen($type,$car) {
        if ($type == 'save' || $type == 'update') {
            $this->load->model('actMain');
            $data = $this->input->post('DOKUMEN');
            if (!$data['CAR']) {
                return "MSG|ERR||Simpan data dokumen Gagal.<br>Data header belum tersimpan.";
            }
            $data['KODE_TRADER'] = $this->kd_trader;
            $exec = $this->actMain->insertRefernce('t_bc30dok',$data);
            if ($exec) {
                return "MSG|OK|".site_url('bc30/daftarDetil/dokumen/'.$data['CAR'])."|Data dokumen berhasil disimpan.";
            } else {
                return "MSG|ERR||Proses data dokumen gagal.";
            }
        }
        else if ($type == 'delete') {
            foreach ($this->input->post('tb_chkfdokumen') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $data['CAR']        = $arrchk[0];
                $data['KDDOK']      = $arrchk[1];
                $data['NODOK']      = $arrchk[2];
                $data['KODE_TRADER']= $this->kd_trader;
                $this->db->where($data);
                $exec = $this->db->delete('t_bc30dok');
            }
            if ($exec) {
                return 'MSG|OK|'.site_url('bc30/daftarDetil/dokumen/'.$car).'|Data dokumen berhasil didelete.';
            } else {
                return 'MSG|ERR||Data dokumen gagal didelete.';
            }
        }
    }
    
    private function get_tblKontainer($car) {
        $arrdata = $this->lsvDetil('kontainer', $car);
        $data['list'] = $this->load->view('list', $arrdata, true);
        $data['type'] = 'kontainer';
        return $this->load->view("bc30/lst", $data, true);
    }
    
    function getKontainer($car, $contno = '') {
        $this->load->model("actMain");
        if ($car && $contno) {
            $query = "SELECT * FROM t_bc30con WHERE CAR = '$car' AND NOCONT = '$contno' AND KODE_TRADER = '$this->kd_trader'";
            $hasil = $this->actMain->get_result($query);
            $data['KONTAINER'] = array_change_key_case($hasil, CASE_UPPER);
            $data['act'] = 'update';
        }
        else {
            $data['act'] = 'save';
        }
        $data['TIPE']   = $this->actMain->get_mtabel('TYPE_CONT', 1, true, '', 'KODEDANUR','BC30');
        $data['UKURAN'] = $this->actMain->get_mtabel('SIZE_CONT', 1, true, '', 'KODEUR','BC30');
        $data['STUFF']  = $this->actMain->get_mtabel('STUFF', 1, true, '', 'KODEUR','BC30');
        $data['PARTOF'] = $this->actMain->get_mtabel('JNPARTOF', 1, true, '', 'KODEDANUR','BC30');
        $data['KONTAINER']['CAR'] = $car;
        return $data;
    }
    
    function setKontainer($type,$car){
        if ($type == 'save' || $type == 'update') {
            $this->load->model("actMain");
            $data = $this->input->post('KONTAINER');
            if (!$data['CAR']) {
                return "MSG|ERR||Simpan data kontainer Gagal.<br>Data header belum tersimpan.";
            }
            $data['KODE_TRADER'] = $this->kd_trader;
            $exec = $this->actMain->insertRefernce('t_bc30con',$data);
            if ($exec) {
                $sql = "call P_BC30UPDCON('" . $data['CAR'] . "'," . $this->kd_trader . ")";
                $this->db->query($sql);
                return "MSG|OK|".site_url('bc30/daftarDetil/kontainer/'.$data['CAR'])."|Data kontainer berhasil disimpan.";
            } else {
                return "MSG|ERR||Proses data kontainer gagal.";
            }
        }
        else if ($type == 'delete'){
            $this->input->post('tb_chkfkontainer') ;
            foreach ($this->input->post('tb_chkfkontainer') as $chkitem){
                $arrchk = explode("|", $chkitem);
                $car    = $arrchk[0];
                $nocont .= ",'" . $arrchk[1] . "'";
            }
            $nocont = substr($nocont, 1);
            $sql = "DELETE FROM T_BC30CON WHERE CAR='" . $car . "' AND KODE_TRADER = '" . $this->kd_trader . "' AND NOCONT in (" . $nocont . ')';
            $exec = $this->db->query($sql);
            if ($exec){
                $sql = "call P_BC30UPDCON('" . $car . "'," . $this->kd_trader . ')';
                $this->db->query($sql);
                return "MSG|OK|".site_url('bc30/daftarDetil/kontainer/'.$car)."|Data kontainer berhasil didelete.";
            }
            else {
                return "MSG|ERR||Data kontainer gagal didelete.";
            }
        }
    }
    
    
    private function get_tblKemasan($car) {
        $arrdata = $this->lsvDetil('kemasan', $car);
        $data['list'] = $this->load->view('list', $arrdata, true);
        $data['type'] = 'kemasan';
        return $this->load->view("bc30/lst", $data, true);
    }
    
    function getKemasan($car, $jns = '', $merk = '') {
        $this->load->model("actMain");
        if ($car && $jns && $merk) {
            $query = "SELECT CAR, JNKEMAS, JMKEMAS, MERKKEMAS, f_kemas(JNKEMAS) URAIAN 
                      FROM t_bc30kms 
		      WHERE KODE_TRADER=" . $this->kd_trader . " AND 
                            CAR = '" . $car . "' AND 
                            JNKEMAS = '" . $jns . "' AND 
                            MERKKEMAS = '" . $merk . "'";//die($query);
            $hasil = $this->actMain->get_result($query);
            $data['KEMASAN'] = array_change_key_case($hasil, CASE_UPPER);
            $data['act'] = 'update';
        }
        else {
            $data['act'] = 'save';
        }
        $data['KEMASAN']['CAR'] = $car;
        return $data;
    }
    
    function setKemasan($type,$car){
        if ($type == 'save' || $type == 'update'){
            $this->load->model('actMain');
            $data = $this->input->post('KEMASAN');
            if (!$data['CAR']) {
                return "MSG|ERR||Simpan data kemasan Gagal.<br>Data header belum tersimpan.";
            }
            $data['KODE_TRADER'] = $this->kd_trader;
            $exec = $this->actMain->insertRefernce('t_bc30kms',$data);
            if ($exec) {
                return "MSG|OK|".site_url('bc30/daftarDetil/kemasan/'.$data['CAR'])."|Data kemasan berhasil disimpan.";
            }
            else {
                return "MSG|ERR||Proses data kemasan gagal.";
            }
        }
        else if ($type == 'delete') {
            foreach ($this->input->post('tb_chkfkemasan') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $data['CAR']        = $arrchk[0];
                $data['JNKEMAS']    = $arrchk[1];
                $data['MERKKEMAS']  = $arrchk[2];
                $data['KODE_TRADER']= $this->kd_trader;
                $this->db->where($data);
                $exec = $this->db->delete('t_bc30kms');
            }
            if ($exec) {
                return 'MSG|OK|'.site_url('bc30/daftarDetil/kemasan/'.$car).'|Data kemasan berhasil didelete.';
            }
            else {
                return 'MSG|ERR||Data kemasan gagal didelete.';
            }
        }
    }
    
    function getBarang($car, $seribrg = '') {
        //print_r($car .'|'. $seribrg); die();
        $this->load->model("actMain");
        $data ['JENIS_TARIF']       = $this->actMain->get_mtabel('JENIS_TARIF', 1, true, '', 'KODEDANUR','BC30');
        $data ['KDIZIN']            = $this->actMain->get_mtabel('KDIZIN', 1, false, '', 'KODEDANUR','BC30');
        $data['JENIS_IDENTITAS']    = $this->actMain->get_mtabel('JENIS_IDENTITAS', 1, true,'','KODEDANUR','BC30');
        $data ['MAXLENGTH']         = '<input type="hidden" id="MAXLENGTH" value="' . $this->actMain->get_uraian("SELECT F_MAXLENGTH('t_bc30dtl') as MAXLENGTH", 'MAXLENGTH') . '"/>';
        if ($car && $seribrg) {
            //print_r('sini');die();
            $query = "SELECT A.*, 
		B.URAIAN_KEMASAN AS 'URJNKOLI', 
		C.URAIAN_NEGARA AS 'URNEGASAL', 
		D.UREDI AS 'URJNSATUAN', 
		E.UREDI AS 'UJNSATPE', 
		F.KATEKS, 
		G.IDEKST,G.NPWPEKST,G.NAMAEKST,G.ALMTEKST, G.NEGBELIT,G.NAMABELIT,G.ALMTBELIT,
		H.URAIAN AS 'UDTLOK'
                    FROM t_bc30dtl A LEFT JOIN m_kemasan B ON A.JNKOLI =B.KODE_KEMASAN 
                        LEFT JOIN m_negara  C ON A.NEGASAL =C.KODE_NEGARA 
                        LEFT JOIN m_satuan  D ON A.JNSATUAN =D.KDEDI 
                        LEFT JOIN m_satuan  E ON A.JNSATPE =E.KDEDI 
                        LEFT JOIN t_bc30hdr F ON A.KODE_TRADER = F.KODE_TRADER AND A.CAR = F.CAR 
                        LEFT JOIN t_bc30pjt G ON A.KODE_TRADER = G.KODE_TRADER AND A.CAR = G.CAR AND A.SERIBRG = G.SERIBRG
                        LEFT JOIN m_tabel   H ON H.MODUL  = 'BC30' AND H.KDTAB = 'STATUS_DETIL' AND H.KDREC = A.DTLOK
                    WHERE A.KODE_TRADER = ".$this->kd_trader." AND A.CAR = '".$car."' AND A.SERIBRG = " . $seribrg;
            //die($query);
            $hasil = $this->actMain->get_result($query);
            $data['DETIL'] = array_change_key_case($hasil, CASE_UPPER);
            $data['act'] = 'update';
        }
        else {
            $sql = "SELECT ifnull(max(A.SERIBRG)+1,1) as SERIBRG, B.KATEKS 
                    FROM t_bc30dtl A 
                    LEFT JOIN t_bc30hdr B ON A.CAR=B.CAR
                    WHERE A.KODE_TRADER = '".$this->kd_trader."' AND A.CAR = '".$car."'";
            $data['DETIL']['SERIBRG'] = $this->actMain->get_uraian($sql,'SERIBRG');
            $data['DETIL']['KATEKS'] = $this->actMain->get_uraian($sql,'KATEKS');
            
            $data['act'] = 'save';
        }
        $data['DETIL']['TOTALBRG']  = $this->actMain->get_uraian("SELECT COUNT(*) TOTALBRG FROM T_BC30DTL WHERE CAR='" . $car . "' AND KODE_TRADER = " . $this->kd_trader, 'TOTALBRG');
        $data['DETIL']['CAR'] = $car;
        //print_r($data);die();
        return $data;
    }
    
    function setBarang($type,$car){
        if ($type == 'save' || $type == 'update'){
            $this->load->model('actMain');
            $data = $this->input->post('DETIL');
            $data['NPWPEKST']      = str_replace('.', '', str_replace('-', '', $data['NPWPEKST']));
            if (!$data['CAR']) {
                return "MSG|ERR||Simpan data barang Gagal.<br>Data header belum tersimpan.";
            }
            $tp = ($data['KDPE']=='1')?'A':'S';
            if($data['KATEKS'] == '33'){
                $exec = $this->actMain->insertRefernce('t_bc30pjt',$data);
            }
             
            $data['HS']        = str_replace('.', '', str_replace('-', '', $data['HS']));
            $data['KDVALPE']   = $data['KDVALPE_'.$tp];
            $data['JNSATPE']   = $data['JNSATPE_'.$tp];
            $data['TARIPPE']   = $data['TARIPPE_'.$tp];
            $data['NILVALPE']  = $data['NILVALPE_'.$tp];
            $data['PEPERBRG']  = $data['PEPERBRG_'.$tp];
            $data['JMSATPE']   = $data['JMSATPE_'.$tp];
            
            $data['DNILINV'] = $data['FOBPERBRG'];
            
            /*$queryHDR = "SELECT CONCAT(KDHRG, '|' ,ASURANSI, '|' ,FREIGHT,'|',NILINV) AS 'value' FROM t_bc30hdr where CAR='".$data['CAR']."' AND KODE_TRADER=".$this->kd_trader;
            $value = $this->actMain->get_uraian($queryHDR,'value');
            $value = explode("|", $value);
            $KDHRG     = $value[0];
            $ASURANSI  = $value[1];
            $FREIGHT   = $value[2];
            $NILINV    = $value[3];
            if($KDHRG == '1'){
                $data['FOBPERBRG']=$data['DNILINV'];
            }else{
                if($NILINV > 0){
                    $DFREIGHT  = ($FREIGHT  * $data['DNILINV']) / $NILINV;
                    $DASURANSI = ($ASURANSI * $data['DNILINV']) / $NILINV;
                }else{
                    $DFREIGHT  = 0;
                    $DASURANSI = 0;
                }
                $data['FOBPERBRG']= $data['DNILINV'] - $DFREIGHT - $DASURANSI;
                //print_r('FOBPERBRG ='.$data['DNILINV'].'-'.$DFREIGHT.'-'.$DASURANSI.'='.$data['FOBPERBRG']);die();
            }*/
            
            //print_r($data['PEPERBRG'].'|'.$data['FOBPERSAT']);die();
            $exec = $this->actMain->insertRefernce('t_bc30dtl',$data);
            //echo $this->db->last_query(); die();
                    
            $msg = $this->recordValidasiDetil($data['CAR'],$data['SERIBRG']);
            if ($exec) {
                $rtn = "MSG|OK|".site_url('bc30/daftarDetil/barang/'.$data['CAR'])."|".$msg;
            }
            else {
                $rtn = "MSG|ERR||Proses data barang gagal.";
            }
        }
        else if ($type == 'delete') {
            foreach ($this->input->post('tb_chkfbarang') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $data['CAR']        = $arrchk[0];
                $data['SERIBRG']    = $arrchk[1];
                $data['KODE_TRADER']= $this->kd_trader;
                $this->db->where($data);
                $exec = $this->db->delete('t_bc30dtl');
            }
            if ($exec) {
                $rtn = 'MSG|OK|'.site_url('bc30/daftarDetil/barang/'.$data['CAR']) . '|Dalete data detil berhasil.';
            } else {
                $rtn = 'MSG|ER|'.site_url('bc30/daftarDetil/barang/'.$data['CAR']) . '|Dalete data detil gagal.';
            }
        }
        return $rtn;
    }
    
    function setBaranGg($type,$car) {
        $this->load->model('actMain');
        if ($type == 'save' || $type == 'update'){
            $post = $this->input->post();
            $car = $this->input->post('CAR');
            if ($type == 'save') {
                $seri = (int) $this->actMain->get_uraian("SELECT MAX(SERIAL) AS MAXSERI FROM t_bc30dtl WHERE CAR='" . $car . "'", "MAXSERI") + 1;
            }
            else {
                $seri = $post['SERIAL'];
            }
            $BARANG                 = $post['DETIL'];
            $BARANG["KODE_TRADER"]  = $this->kd_trader;
            $BARANG["CAR"]          = $car;
            $BARANG["NOHS"]         = str_replace(".", "", $BARANG["NOHS"]);
            $BARANG["SERIAL"]       = $seri;
            unset($BARANG["INVOICE"]);
            unset($BARANG["CIFRP"]);
            $exec['barang']         = $this->actMain->insertRefernce('t_bc30dtl', $BARANG, true);
            $exec['trader_barang']  = $this->actMain->insertRefernce('m_trader_barang', $BARANG, true);

            $FASILITAS                  = $post['FASILITAS'];
            $FASILITAS["KODE_TRADER"]   = $this->kd_trader;
            $FASILITAS["CAR"]           = $car;
            $FASILITAS["SERIAL"]        = $seri;
            $exec['fasilitas']          = $this->actMain->insertRefernce('t_bc30fas', $FASILITAS, true);

            $TARIF                  = $post['TARIF'];
            $TARIF["KODE_TRADER"]   = $this->kd_trader;
            $TARIF["CAR"]           = $car;
            $TARIF["SERIAL"]        = $seri;
            $TARIF["SERITRP"]       = $BARANG["SERITRP"];
            $TARIF["NOHS"]          = $BARANG["NOHS"];
            $exec['tarif']          = $this->actMain->insertRefernce('t_bc0trf', $TARIF, true);
            $exec['trader_tarif']   = $this->actMain->insertRefernce('m_trader_tarif', $TARIF, true);
            
            
            
            $sql = "call P_BC30UPDDTL('" . $car . "'," . $this->kd_trader . ")";
            $this->db->query($sql);
            
            
            //list($msg,$status) = $this->validasiDetil($car,$seri);
            if ($exec['barang'] && $exec['trader_barang'] && $exec['fasilitas'] && $exec['tarif']) {
                return "MSG|OK|".site_url('bc30/daftarDetil/barang/'.$car).'/|<pre style="float: left;">'.$msg.'</pre>';
            } else {
                return "MSG|ERR||Proses data barang gagal.";
            }
        }
        else if ($type == "delete") {
            foreach ($this->input->post('tb_chkfbarang') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $car = $arrchk[0];
                $seri = $arrchk[1];
                $key = array('KODE_TRADER' => $this->kd_trader, 'CAR' => $car, 'SERIAL' => $seri);
                $tables = array('t_bc30dtl', 't_bc30fas', 't_bc30trf');
                $this->db->where($key);
                $this->db->delete($tables);
                //update urutkan serial
                $sql = "call bc30UrutkanSerial(" . $this->kd_trader . ",'" . $car . "'," . $seri . ")";
                $exec = $this->db->query($sql);
            }
            
            if ($exec) {
                return "MSG|OK|".site_url('bc30/daftarDetil/barang/'.$car).'/|Delete data barang berhasil.';
            } else {
                return "MSG|ERR||Delete data barang gagal.";
            }
            die();
        }
    }
    
    function lstGoToPEBg($car, $jmbrggab){
        $this->load->library('newtable');
        $tipe = 'gotoPEBg';
        $this->load->model("actMain");
        $SQL = "SELECT CAR, SERIEKS, '".$jmbrggab."' as 'Jumlah Gabungan' ,CONCAT('". site_url('bc30/pagePEBgabungan') . "/',CAR,'/',SERIEKS,'/','".$jmbrggab."') as URL "
             . "FROM t_bc30gab WHERE CAR='".$car."' AND KODE_TRADER= '".  $this->kd_trader ."'"; //site_url + '/bc30/pagePEBgabungan/' + arr[0] + '/' + arr[1] + '/10'
        //die($SQL);
        $this->newtable->keys(array('URL'));
        $this->newtable->hiddens(array('URL','CAR'));
        $this->newtable->search(array(array('SERIEKS', 'No. Seri')));
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->action(site_url("bc30/GoToPagePEBg"));
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->detail_tipe('pilih_page');
        $this->newtable->orderby(1, 2);
        $this->newtable->sortby("ASC");
        $this->newtable->set_formid("f".$tipe);
        $this->newtable->set_divid('div'.$tipe);
        $this->newtable->divChange('navPebGab');
        $this->newtable->rowcount(20);
        $this->newtable->tipe_proses('button');
        $this->newtable->clear();
        $tabel .= $this->newtable->generate($SQL);
        $arrdata = array('tabel' => $tabel,'tipe' => $tipe);
        if ($this->input->post("ajax")){
            return $tabel;
        }
        else{
            return $arrdata;
        }
    }
    
    function lstGoToPEBgDtl($car, $serieks, $seribrgg,$jmbrgg){
        //print_r($car.'|'.$seribrgg);die();
        $this->load->library('newtable');
        $tipe = 'gotoPEBgDtl';
        $this->load->model("actMain");
        $SQL = "SELECT CAR, SERIEKS, SERIBRGG, '".$jmbrgg."' as 'Jumlah Barang',CONCAT('". site_url('bc30/frmPEBGabDtl') . "/',CAR,'/','".$jmbrgg."' ,'/',SERIEKS,'/',SERIBRGG) as URL "
             . "FROM t_bc30gabdtl WHERE CAR='".$car."' AND SERIEKS='".$serieks."' AND KODE_TRADER= '".  $this->kd_trader ."'"; //site_url + '/bc30/pagePEBgabungan/' + arr[0] + '/' + arr[1] + '/10'
        //die($SQL);
        $this->newtable->keys(array('URL'));
        $this->newtable->hiddens(array('URL','CAR'));
        $this->newtable->search(array(array('SERIEKS', 'No. Seri')));
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->action(site_url("bc30/GoToPagePEBgDtl"));
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->detail_tipe('pilih_page');
        $this->newtable->orderby(1, 2);
        $this->newtable->sortby("ASC");
        $this->newtable->set_formid("f".$tipe);
        $this->newtable->set_divid('div'.$tipe);
        $this->newtable->divChange('navPebGabDtl');
        $this->newtable->rowcount(20);
        $this->newtable->tipe_proses('button');
        $this->newtable->clear();
        $tabel .= $this->newtable->generate($SQL);
        $arrdata = array('tabel' => $tabel,'tipe' => $tipe);
        if ($this->input->post("ajax")){
            return $tabel;
        }
        else{
            return $arrdata;
        }
    }
    
    function genFlatfilePEB($car,$srpAwal='') {
        function fixLen($str, $len, $chr = ' ', $alg = STR_PAD_RIGHT) {
            $hasil = str_pad(substr($str, 0, $len), $len, $chr, $alg);
            return $hasil;
        }
        $vNoreg = substr($car,6,6);
        $this->load->model('actMain');
        $EDINUMBER = $this->newsession->userdata('EDINUMBER');
        $SQL = "SELECT A.* ,
                        DATE_FORMAT(A.TGDAFT,'%Y%m%d') as TGDAFT,
                        DATE_FORMAT(A.TGBCF ,'%Y%m%d') as TGBCF,
                        DATE_FORMAT(A.TGEKS ,'%Y%m%d') as TGEKS,
                        DATE_FORMAT(A.TGSIUP ,'%Y%m%d') as TGSIUP,
                        DATE_FORMAT(A.TGTDP ,'%Y%m%d') as TGTDP,
                        DATE_FORMAT(A.TGPPJK ,'%Y%m%d') as TGPPJK,
                        DATE_FORMAT(A.TGSIAP ,'%Y%m%d') as TGSIAP
                    FROM t_bc30hdr A
                LEFT JOIN m_trader_bc30 B ON A.KODE_TRADER = B.KODE_TRADER AND A.KDKTR = B.KPBC
                WHERE A.CAR = '" . $car . "' AND A.KODE_TRADER = " . $this->kd_trader;
        $hdr = $this->actMain->get_result($SQL);
        
        $hdr = str_replace("\n", '', str_replace("\r", '', $hdr));
        if($this->newsession->userdata('TIPE_TRADER')=='1'){
            $SRP = $hdr['SRP'];
        }else{
            $SRP = $srpAwal;
        }//print_r($hdr['STATUS']);die();
        if ($hdr['STATUS'] != '01') {
            if ($hdr['STATUS'] == '02') {
                $whHDR['KODE_TRADER'] = $this->kd_trader;
                $whHDR['CAR'] = $car;
                $this->db->where($whHDR);
                $upHDR['SNRF'] = '';
                //$upHDR['DIRFLT'] = '';
                //$upHDR['DIREDI'] = '';
                $upHDR['STATUS'] = '01';
                $this->db->update('t_bc30hdr', $upHDR);
                unset($upHDR);
                #delete dokoutbound
                $whHDR['KDKPBC'] = $hdr['KDKTR'];
                $whHDR['JNSDOK'] = 'BC30';
                $this->db->delete('m_trader_dokoutbound', $whHDR);
                unset($whHDR);
                unlink($hdr['DIRFLT']);
                unlink($hdr['DIREDI']);
                $hsl = '1|';
            }
            else {
                $hsl = '0|';
            }
            unset($hdr);
            return $hsl;
        }
        
        //$vdokumen=(strpos('|BCF305|PEB|', !$hdr['NOBCF']));
        $vdokumen= ($hdr['NOBCF']!='')?'BCF305':'PEB';
        #Konversi header dulu...
        switch ($hdr['KATEKS']) {
            Case "10":    #Umum
                $strJnEks = "1";
                $strJnLain = "9";
                break;
            Case "21":   #KITE Pembebasan
                $strJnEks = "3";
                $strJnLain = "";
                break;
            Case "22":   #KITE Pengembalian
                $strJnEks = "3";
                $strJnLain = "";
                break;
            Case "23":   #KITE Gabungan
                $strJnEks = "3";
                $strJnLain = "";
                break;
            Case "31":   #Diplomatik
                $strJnEks = "4";
                $strJnLain = "C";
                break;
            Case "32":   #Badan Intl
                $strJnEks = "4";
                $strJnLain = "J";
                break;
            Case "33":   #Kiriman
                $strJnEks = "4";
                $strJnLain = "A";
                break;
            Case "34":   #Pindahan
                $strJnEks = "4";
                $strJnLain = "B";
                break;
            Case "35":   #Kepentingan Sosial
                $strJnEks = "4";
                $strJnLain = "D";
                break;
            Case "36":   #Cinderamata
                $strJnEks = "4";
                $strJnLain = "G";
                break;
            Case "37":   #Contoh
                $strJnEks = "4";
                $strJnLain = "H";
                break;
            Case "38":   #Penelitian
                $strJnEks = "4";
                $strJnLain = "I";
                break;
            Case "41":   #TPB KB
                $strJnEks = "5";
                $strJnLain = "";
                $strJnTPB = "KB ";
                break;
            Case "42":   #TPB GB
                $strJnEks = "5";
                $strJnLain = "";
                $strJnTPB = "GB ";
                break;
            Case "43":   #TPB ETP
                $strJnEks = "5";
                $strJnLain = "";
                $strJnTPB = "ETP";
                break;
            Case "44":   #TPB TBB
                $strJnEks = "5";
                $strJnLain = "";
                $strJnTPB = "TBB";
                break;
            Case "45":   #TPB TLB
                $strJnEks = "5";
                $strJnLain = "";
                $strJnTPB = "TLB";
                break;
            Case "46":   #TPB KDU
                $strJnEks = "5";
                $strJnLain = "";
                $strJnTPB = "KDU";
                break;
            Default:
                $strJnEks = "1";
                $strJnLain = "9";
                  break;
        }
        
        switch ($hdr['KATEKS']) {
           case "1":    #Umum
           case "2":   #Reimpor
               $strJnPEB = "4";
               $strJnLain = "F";
               break;
           case "3":   #Reekpor
               $strJnLain = "E";
               $strJnPEB = "5";
               break;
        }
        
        $KDKPBC = $hdr['KDKTR'];
        $SQL = "SELECT NOMOREDI FROM M_TRADER_PARTNER WHERE KDKPBC = '" . $hdr['KDKTR'] . "' AND KODE_TRADER = " . $this->kd_trader;
        $partner = $this->actMain->get_uraian($SQL, 'NOMOREDI');
        if ($partner == '') {
            return 'Edinumber Kantor Pelayanan belum terekam.';
        }
        
        $sql    = "SELECT * FROM t_bc30pkb
                WHERE CAR = '" . $car . "' AND KODE_TRADER = " . $this->kd_trader;
        $arrPKB = $this->actMain->get_result($sql, true);
        //print_r($arrPKB);die(); 
        if(count($arrPKB)>0){
            $adaPKB = true;
        }else{
            $adaPKB = false;
        }
        //print_r($arrPKB);die();
        
        #ENV00101
        $vDok = ($vdokumen=='BCF305')?'DOKBCF':'DOKPEB';
        $FF.= 'ENV00101' . $this->fixLen($EDINUMBER, 20) . $this->fixLen($partner, 20) . $this->fixLen($vDok, 10) . "2\n";
        
        #HDEC0101
        if($vdokumen == 'BCF305'){
            $FF.= 'HDEC0101' . $hdr['CAR'] . 'BCBCF830DOKBCF9 ' . $this->fixLen($hdr['SERIBCF'], 5) . $this->fixLen($hdr['NOBCF'], 10) . "\n";
        }elseif($vdokumen == 'PEB'){
            $FF.= 'HDEC0101' . $hdr['CAR'] . 'BCPEB830DOKPEB9 ' . $vNoreg . $this->fixLen($hdr['CAR'], 6) . "\n";
        }
        
        #HDEC0201
        $FF.= 'HDEC020122 ' . $hdr['KDKTR'] . $hdr['KTRGATE'] . $hdr['KDKTRPRIKS'] . ($hdr['KDHANGGAR']) . "       7" . "\n"; //Mid("" & rspebConst!KdHanggar & Space(7), 7)
        $FF.= 'HDEC02019  ' . $hdr['PELMUAT'] . "\n";
        $FF.= 'HDEC020111 ' . $hdr['PELBONGKAR'] . "\n";
        ($hdr['PELTRANSIT'] != '') ? $FF.= 'HDEC020162 ' . $hdr['PELTRANSIT'] . "\n" : '';
        ($hdr['PELMUATEKS'] != '') ? $FF.= 'HDEC020123 ' . $hdr['PELMUATEKS'] . "\n" : '';
        
        $FF.= "HDEC0201" . "28 " . $hdr['NEGTUJU'] . "\n";
        $FF.= "HDEC0201" . "106" . $hdr['PROPBRG'] . "\n";
        
        if($adaPKB == true){
            $FF.= 'HDEC020114 ' . $this->fixLen($hdr['KDLOKBRG'], 5) . $this->fixLen($arrPKB[0]['GUDANG'], 1) . "        " . $this->fixLen($arrPKB[0]['ALMTSIAP'], 70) . $this->fixLen($arrPKB[0]['ALMTSTUFF'], 70) . "\n";
        }else{
            ($hdr['KDLOKBRG'] != '') ? $FF.= 'HDEC020114 ' . $this->fixLen($hdr['KDLOKBRG'], 5) . "                                                                                                                                                                " . "\n" : '';
        } //di PIA gak ada if-if-an
        
        
        #HDEC0301
        if($vdokumen == 'BCF305'){
            $FF.= 'HDEC0301148' . $hdr['TGDAFT'] . "102" . "\n";
            $FF.= 'HDEC0301184' . $hdr['TGBCF'] . "102" . "\n";
        }
        
        $FF.= 'HDEC0301150' . substr($hdr['CAR'], 12, 8) . "102\n";
        $FF.= 'HDEC0301129' . $hdr['TGEKS'] . "102" . "\n";
        
        if($hdr['TGSIAP'] != '' || $hdr['TGSIAP'] != '0000-00-00'){
            $vAdaTgSiap = true ;
        }else{
            $vAdaTgSiap = false ;
        }
        if($hdr['TGPPJK'] != '' ||$hdr['TGPPJK'] != '0000-00-00'){
            $vAdaTgPpjk = true ;
        }else{
            $vAdaTgPpjk = false ;
        }
         
        ($vAdaTgPpjk == true) ? $FF.= 'HDEC0301313' . $hdr['TGSIAP'] . "102\n" : ''; #Tanggal Kesiapan Barang .. harusnya ada JAM-MENIT
        ($vAdaTgPpjk == true) ? $FF.= 'HDEC0301255' . $hdr['TGSIAP'] . "102\n" : ''; #Tanggal Pemeriksaan diisi=TgSiap
        
         if($adaPKB == true){
            $FF.= 'HDEC0301199' . $arrPKB[0]['TGSTUFF'] . "102\n";
        }
        ($hdr['TGSIUP'] != '' || $hdr['TGSIUP'] != '0000-00-00') ? $FF.= 'HDEC0301182' . $hdr['TGSIUP'] . "102\n" : ''; 
        ($hdr['TGTDP']  != '' || $hdr['TGTDP'] != '0000-00-00') ?  $FF.= 'HDEC0301183' . $hdr['TGTDP'] . "102\n" : '';
        ($vAdaTgPpjk == true) ? $FF.= 'HDEC0301125' . $hdr['TGPPJK'] . "102\n" : '';
        
        #HDEC0401
        $FF.= 'HDEC040190 ' . $strJnPEB . "\n";
        $FF.= 'HDEC040191 ' . $this->fixLen($strJnEks, 2) . $strJnLain . "\n";
        $FF.= 'HDEC040192 ' . $this->fixLen($hdr['JNDAG'], 1) . "\n";
        $FF.= 'HDEC040193 ' . $this->fixLen($hdr['JNBYR'], 1) . "\n";
        ($hdr['JMBRGGAB'] > 0) ? $FF.= 'HDEC040194 ' . $hdr['JMBRGGAB'] . "\n": '' ;
        
        foreach ($arrPKB  as $val ){
            ($val['STUFF']!='') ? $FF.= 'HDEC040195 ' . $val['STUFF'] . "\n": '' ;
        }
        ($hdr['JNPARTOF']!='') ? $FF.= 'HDEC040196 ' . $hdr['JNPARTOF'] . "\n": '' ;
        
        
        $FF.= 'HDEC0401100' . $hdr['KMDT'] . "\n";//KMDT ada di mana ? komodity
        ($hdr['KONS']!='') ? $FF.= "HDEC0401" . "101" . $hdr['KONS'] . "\n" : '' ; 
        $FF.= 'HDEC0401102' .  $vAktivasi . "\n"; //aktivasi sama kota apa ?
        $FF.= 'HDEC0401103' . $kota . "\n";
        
        ($hdr['JNTPB']!='') ? $FF.= 'HDEC040197 ' . $this->fixLen($hdr['JNTPB'], 3) . $this->fixLen($hdr['LOKTPB'], 10) . "\n" :'';
        foreach ($arrPKB  as $val ){
            ($val['STUFF']!='') ? $FF.= 'HDEC040198 ' . $this->fixLen($val['INSTFAS'], 1) . $this->fixLen($val['KDKTRFAS'], 6) . $this->fixLen($val['FASBC'], 1) . $val['KPKER'] . $val['KWBC'] . $val['FASILITAS'] . "\n": '' ;
        }
        $vVersion = $this->actMain->get_Setting('BC30','VERSION');
        $FF.= 'HDEC040199 ' . $vVersion; //gak ngerti, vVersion dapet dari mana ?
        
        #HDEC0601
        $FF.= 'HDEC0601WT AAD' . $hdr['BRUTO'] . "KGM\n";
        $FF.= 'HDEC0601WT AAC' . $hdr['NETTO'] . "KGM\n";
        $FF.= 'HDEC0601VOLABJ' . $hdr['VOLUME'] . "M3\n";
        
        #HDEC0701-HDEC0901  -- Cont
        
        $SQL = "SELECT * FROM t_bc30con WHERE CAR = '" . $car . "' AND KODE_TRADER = " . $this->kd_trader;
        $con = $this->actMain->get_result($SQL, true);
        foreach ($con as $a) { 
            $iCon++;//00001,00002
            $FF.= 'HDEC0701CN ' . fixLen($iCon, 5, '0', STR_PAD_LEFT) . $this->fixLen($a['NOSEGEL'], 15) . $this->fixLen($a['NOCONT'], 17) . $this->fixLen($a['SIZE'], 2) . $this->fixLen($a['TYPE'], 1) . $this->fixLen($a['STUFF'], 1) . $this->fixLen($a['JNPARTOF'], 1) . "\n";
        }
        ($iCon == 0) ? $FF.= 'HDEC0701PBP' . "\n" : '' ;
        ($vdokumen = 'BCF305') ? $FF.= 'HDEC0901ABT' . $this->fixLen($hdr['NODAFT'], 5) . "BCF305\n" : ''; /// gak ada di PIA
        unset($con);
        
        
        #HDEC0901-HDEC1001 KemasAN
        $SQL = "SELECT * FROM t_bc30kms WHERE CAR = '" . $car . "' AND KODE_TRADER = " . $this->kd_trader;
        $kms = $this->actMain->get_result($SQL, true);
        foreach ($kms as $a) {
            $iKms++;
            $FF.= 'HDEC0901ACH' . fixLen($iKms, 5, '0', STR_PAD_LEFT) . $this->fixLen($a['NOSEGEL'], 15) . $this->fixLen($a['JMLKEMAS'], 8) . $this->fixLen($a['JNSKEMAS'], 2) . "\n";
            //$FF.= 'HDEC0901' . 'ACH' . fixLen($iKms, 5, '0', STR_PAD_LEFT) . '               ' . fixLen($a['JMKEMAS'], 8, '0', STR_PAD_LEFT) . fixLen($a['JNKEMAS'], 2) . "\n";
        }
        $FF.= 'HDEC0901CW                             ZZ' . $hdr['MERK'] . "\n"; //MEREK DAPET DARI MANA 
        $FF.= 'HDEC100120 ' . $hdr['MODA'] . $this->fixLen($hdr['ANGKUTNAMA'], 20) . $this->fixLen($hdr['ANGKUTNO'], 7) . $hdr['ANGKUTFL'] . "\n";
        unset($kms);
        
        #HDEC1101 -- DOKUMEN
        $SQL = "SELECT *, DATE_FORMAT(TGDOK,'%Y%m%d') as 'TGDOK' FROM t_bc30dok WHERE CAR = '" . $car . "' AND KODE_TRADER = " . $this->kd_trader;
        $dok = $this->actMain->get_result($SQL, true);
        foreach ($dok as $a) {  
            $iDok++;
            $FF.= 'HDEC1101' . $this->fixLen($iDok, 5, '0', STR_PAD_LEFT) . $a['KDDOK'] . $this->fixLen($a['NODOK'], 30) . '     182' . $this->fixLen($a['TGDOK'], 8) . '102' . "\n";                
        }
        
        #HDEC1201 - HDEC1401
        $text = 'HDEC1201EX ' . $hdr['IDEKS'] . $this->fixLen($hdr['NPWPEKS'], 15) . $this->fixLen($hdr['NAMAEKS'], 50) . $this->fixLen($hdr['ALMTEKS'], 70) . 
                $this->fixLen($hdr['STATUSH'], 2) . $this->fixLen($hdr['JNPERUS'], 2) . '     XA' . $this->fixLen($hdr['NIPER'], 4) . 
                $this->fixLen($hdr['NOSIUP'], 30) . '      ' . $this->fixLen($hdr['NOTDP'], 30);
        if($adaPKB == true){
            $text = $this->fixLen($arrPKB['PETUGAS'], 40) . $this->fixLen($arrPKB['NOPHONE'], 20) . $arrPKB['NOFAX'];
        }
        $FF.= text;
        ($hdr['NPWPPPJK']!='') ? $FF.= 'HDEC1201CB ' . $hdr['IDPPJK'] . $this->fixLen($hdr['NPWPPPJK'], 15). $this->fixLen($hdr['NAMAPPJK'], 50) . $this->fixLen($hdr['ALMTPPJK'], 70) . '         XA' . $hdr['KDKTR'] . '                                  ' . $hdr['NOPPJK'] . "\n" : '' ;
        (!$hdr['NPWPQQ'])   ? $FF.= 'HDEC1201GO ' . $hdr['IDQQ'] . $this->fixLen($hdr['NPWPQQ'], 15). $this->fixLen($hdr['NAMAQQ'], 50) . $this->fixLen($hdr['ALMTQQ'], 70) . "         " . "XA" . $hdr['NIPERQQ'] . "\n" : '' ;
        $FF.= 'HDEC1201BY                 ' . fixLen($hdr['NAMABELI'], 50) . fixLen($hdr['ALMTBELI'], 70) . '  ' . $hdr['NEGBELI'] . "\n";
        ($hdr['TTDPEB']!='') ? $FF.= 'HDEC1201AM                 ' . $this->fixLen($hdr['NAMA_TTD'], 30) ."\n": '' ; //field TTDPEB dapet dari mana
        $FF.= 'HDEC13016  ' . $hdr['DELIVERY'] . "\n";
        $FF.= 'HDEC140163 ' . $hdr['FOB'] . $hdr['KDVAL'] ."\n";
        $FF.= 'HDEC140164 ' . $hdr['FREIGHT'] ."\n";
        $FF.= 'HDEC140170 0000' . $this->fixLen($hdr['KDASS'], 1, '0', STR_PAD_LEFT) . $hdr['ASURANSI'] ."\n";
        
        $FF.= 'UNS00101D' . "\n";
        
        ($hdr['NILKURS'] > 0) ? $FF.= 'HDEC140110 000000000' . $hdr['NILKURS'] . $hdr['KDVAL'] ."\n" :'';
        ($hdr['PNBP']    > 0) ? $FF.= 'HDEC140158 ' . $hdr['PNBP'] ."\n" : '' ;
        
        $FF.= "UNS00101" . "D" . "\n";
        
        unset($dok);
        #HDEC1201 - HDEC1401
        
        $sql    = "SELECT *, 
                    DATE_FORMAT(TGDAFTINVK,'%Y%m%d') as 'TGDAFTINVK',
                    DATE_FORMAT(TGINVK,'%Y%m%d') as 'TGINVK'
                    FROM t_bc30berkala
                WHERE CAR = '" . $car . "' AND KODE_TRADER = " . $this->kd_trader;
        $arrBerkala = $this->actMain->get_result($sql, true);
        if(count($arrBerkala)>0){
            $adaBerkala = true;
        }else{
            $adaBerkala = false;
        }
        
        $sql    = "SELECT * FROM t_bc30pjt
                WHERE CAR = '" . $car . "' AND KODE_TRADER = " . $this->kd_trader;
        $arrPjt = $this->actMain->get_result($sql, true);
        if(count($arrPjt)>0){
            $adaPjt = true;
        }else{
            $adaPjt = false;
        }
        
        $SQL = "SELECT *,
                        DATE_FORMAT(A.TGIZIN,'%Y%m%d') as 'TGIZIN'
                    FROM t_bc30dtl A
                WHERE A.CAR = '" . $car . "' AND A.KODE_TRADER = " . $this->kd_trader;
        $brg = $this->actMain->get_result($SQL);
        
        #DDEC0101- 
        $vNcv = ($brg['FOBPERBRG'] > 0)?'1':'2';
        $FF.= 'DDEC0101' . $this->fixLen($brg['SERIBRG'], 30) . $this->fixLen($brg['HS'], 30) . 'AAA' . $vNcv . $this->fixLen($brg['URBRG1'] . $brg['URBRG2'] . $brg['URBRG3'] . $hdr['URBRG4'], 160) . "\n";
        #DDEC0201
        (!$brg['NEGASAL']) ? $FF.= 'DDEC020127 ' . $brg['NEGASAL'] . "\n" : '';
        if($adaBerkala == true){
            ($arrBerkala['NEGTUJUK']!='') ? $FF.= 'DDEC020128 ' . $arrBerkala['NEGTUJUK'] . '    129' . $arrBerkala['TGEKSK'] . "102\n": '';
        }
        
        #DDEC0301
        $FF.= 'DDEC0301AAE0000' . $brg['JMSATUAN'] . $brg['JNSATUAN'] . "\n";
        ($brg['JNSATPE']!='') ? $FF.= 'DDEC0301DX 0000' . $brg['JMSATPE'] . $brg['JNSATPE'] . "\n": '';
        $FF.= 'DDEC0301WT ' . $brg['NETDET'] . "KGM\n";
        ($brg['DVOLUME'] > 0) ? $FF.= 'DDEC0301VOL' . $brg['DVOLUME'] . 'M3 ' . "\n": ''; //kondisi ini perlu ditnya
        #DDEC0401         
        if($adaPjt == true){
            ($arrPjt['NPWPEKST']!='')  ? $FF.= 'DDEC0401EX ' . $arrPjt['IDEKST'] . $this->fixLen($arrPjt['NPWPEKST'], 15) . $this->fixLen($arrPjt['NAMAEKST'], 50) . $this->fixLen($arrPjt['ALMTEKST'], 70) . "\n": '';
            ($arrPjt['NAMABELIT']!='') ? $FF.= 'DDEC0401BY                 ' . $this->fixLen($arrPjt['NAMABELIT'], 50) . $this->fixLen($arrPjt['ALMTBELIT'], 70) . '  ' . $arrPjt['NEGBELIT'] . "\n":'';
        }
        #DDEC0501
        //$FF.= "HDEC1001" . "20 " . $hdr['MODA'] . fixLen($hdr['ANGKUTNAMA'], 20) . fixLen($hdr['ANGKUTNO'], 7) . $hdr['ANGKUTFL'] . "\n";
        if($adaBerkala == true){
            $FF.= 'DDEC050120 ' . $this->fixLen($arrBerkala['MODAK'], 1) . $this->fixLen($arrBerkala['CARRIERK'], 20) . $this->fixLen($arrBerkala['VOYK'], 7) . $this->fixLen($arrBerkala['BENDERAK'], 2) . "\n";
        }
        #DDEC0601
        $FF.= 'DDEC0601' . $this->fixLen($brg['JMKOLI'], 8) . $this->fixLen($brg['JNKOLI'], 2) . $this->fixLen($brg['SIZE'], 15) . $this->fixLen($brg['TYPE'], 15) . $this->fixLen($brg['KDBRG'], 15) . $brg['DMERK'] . "\n";
        #DDEC0701
        $FF.= 'DDEC070163 ' . $brg['FOBPERSAT'] . "\n"; 
        $FF.= 'DDEC070165 ' . $brg['FOBPERBRG'] . "\n";
        ($brg['HPATOK'] > 0) ? $FF.= 'DDEC0701172' . $brg['HPATOK'] . "\n" : '';
        ($brg['KDPE']!='') ? $FF.= 'DDEC070110 000000000' . $brg['NILVALPE'] . $brg['KDVALPE'] . "\n": '';
        #DDEC0801
        ($brg['KDIZIN']!='') ? $FF.= 'DDEC0801' . $brg['KDIZIN'] . $this->fixLen($brg['NOIZIN'], 30) . '      182' . $brg['TGIZIN'] . "102\n" : '';
        
        if($adaBerkala == true){
            ($arrBerkala['NODAFTINVK']!='') ? $FF.= 'DDEC0801393' . $this->fixLen($arrBerkala['NODAFTINVK'], 6) . '      182' . $arrBerkala['TGDAFTINVK'] . "102\n": '';
            ($arrBerkala['NOINVK']!='') ?     $FF.= 'DDEC0801380' . $this->fixLen($arrBerkala['NOINVK'], 30) .    '      182' . $arrBerkala['TGINVK'] . "102\n": '';
            
        }
        #DDEC0901
        //$KDPE = ($vdokumen=='BCF305')?'DOKBCF':'DOKPEB';
        ($brg['KDPE']!='') ? $FF.= 'DDEC09011  CST' . strpos('|SP |SA |', $brg['KDPE']) . $brg['TARIPPE'] . $brg['PEPERBRG'] . "\n":''; //ini perlu di tanya
        unset($brg);
        unset($arrBerkala);
        $FF.= "UNS00201" . "S" . "\n";
        
        #SDEC0101 - SUMMARY
        $FF.= 'SDEC01015  ' . $this->fixLen($hdr['JMBRG'], 5) . "\n";
        
        if($iKms > 0){//ini jadi pertanyaan
            $FF.= 'SDEC010111 ' . $this->fixLen(5, '0', STR_PAD_LEFT) . "\n";
        }
        ($hdr['JMCONT'] > 0) ? $FF.= 'SDEC010116 ' . $this->fixLen($hdr['JMCONT'], 5) . "\n":'';

        
        if($adaPKB == true){
            ($arrPKB['JMCONT20'] > 0) ? $FF.= 'SDEC010190 ' . $this->fixLen($arrPKB['JMCONT20'], 5) . "\n":'';
            ($arrPKB['JMCONT40'] > 0) ? $FF.= 'SDEC010191 ' . $this->fixLen($arrPKB['JMCONT40'], 5) . "\n":'';
        }
        //($hdr['JMBRGCTH'] > 0) ? $FF.= 'SDEC010192 ' . fixLen($hdr['JMBRGCTH'], 5) . "\n":'';
        ($hdr['JMBRGGAB'] > 0) ? $FF.= 'SDEC010193 ' . $this->fixLen($hdr['JMBRGGAB'], 5) . "\n":'';
        //($hdr['JMBRGPJT'] > 0) ? $FF.= "SDEC0101" . "94 " . fixLen($hdr['JMBRGPJT'], 5) . "\n":'';
        
        #SDEC0201
        ($hdr['NILPE']!='') ? $FF.= 'SDEC02012  24 ' . $hdr['NILPE'] . "\n":'';
        
        $FF.= "UNT00101";
                
        #Header barang gabungan
        $SQL = "SELECT A.* 
                    FROM t_bc30gab A
                WHERE A.CAR = '" . $car . "' AND A.KODE_TRADER = " . $this->kd_trader;
        $PebGab = $this->actMain->get_result($SQL);
        
        $SQL = "SELECT A.* 
                    FROM t_bc30gabdtl A
                WHERE A.CAR = '" . $car . "' AND A.KODE_TRADER = " . $this->kd_trader;
        $GabDtl = $this->actMain->get_result($SQL);
        $SQL = "SELECT A.*, DATE_FORMAT(TGDOKG,'%Y%m%d') as 'TGDOKG'
                    FROM t_bc30gabdok A
                WHERE A.CAR = '" . $car . "' AND A.KODE_TRADER = " . $this->kd_trader;
        $GabDok = $this->actMain->get_result($SQL);
        
        if(count($PebGab) > 0){
            $nJmGab = "nJmGab" . "1";
            #HDEC0101
            $FF.= 'HDEC0101' . 'BCPEB 830PEBGAB' . $hdr['NOSERIBCF'] . $hdr['NOBCF'] . "\n";
            //$FF.= 'HDEC0101' . $hdr['CAR'] . 'BCPEB830PEBGAB9 ' . fixLen($hdr['vNoreg'], 6) . fixLen($hdr['CAR'], 6) . "\n";
            #HDEC0301
            $FF.= 'HDEC0301150' . substr($hdr['CAR'], 13 , 8) . "102\n";
            #HDEC0401
            $FF.= 'HDEC040190 ' . $PebGab['JNPEBG'] . "\n";
            $FF.= 'HDEC040191 ' . $PebGab['JNEKSG'] . "\n";
            $FF.= 'HDEC040198 ' . $this->fixLen($PebGab['INSTFASG'], 1) . $this->fixLen($PebGab['KDKTRFASG'], 6) . $this->fixLen($PebGab['FASBCG'], 1) . $this->fixLen($PebGab['KPKERG'], 2) . $this->fixLen($PebGab['KWBCG'], 6) . $PebGab['FASILITASG'] . "\n";
            
            #HDEC1201 fixLen($hdr['NAMAEKS'], 50)
            $FF.= 'HDEC1201EX ' . $this->fixLen($hdr['IDEKS'], 1) . $this->fixLen($hdr['NPWPEKS'], 15) . $this->fixLen($hdr['NAMAEKS'], 50) . $this->fixLen($hdr['ALMTEKS'], 70) . '    ' . $this->fixLen($hdr['SERIEKS'], 5) . 'XA' . $this->fixLen($hdr['NIPER'], 4);
            #HDEC1401
            $FF.= 'HDEC140163 ' . $hdr['FOB'] . $hdr['KDVAL'] . "\n";
            #UNS00101
            $FF.= 'UNS00101D' . "\n";
            
            
            if(count($GabDtl) > 0){
                $vNcvG = (strpos('|1|2|', $GabDtl['FOBPERBRGG']) > 0);
                #DDEC0101
                $FF.= 'DDEC0101' . $this->fixLen($GabDtl['SERIBRGG'], 5) . $this->fixLen($GabDtl['HSG'], 12) . 'AAA' . $vNcvG . $this->fixLen($GabDtl['URBRGG'], 160) . $GabDtl['EXSERIBRGG'] . "\n";
                #DDEC0301
                $FF.= 'DDEC0301AAE' . $GabDtl['JMSATUANG'] . $GabDtl['JNSATUANG'] . "\n";
                #DDEC0601  fixLen($GabDtl['HSG'], 8) . $GabDtl['JNKOLIG'] . 
                $FF.= 'DDEC0601          ' . $this->fixLen($GabDtl['SIZEG'], 15) . $this->fixLen($GabDtl['TYPEG'], 15) . $this->fixLen($GabDtl['KDBRGG'], 15) . $this->fixLen($GabDtl['DMERKG'], 15) . "\n";
                #DDEC0701
                $FF.= 'DDEC070163 ' . $GabDtl['FOBPERSATG'] . "\n";
                $FF.= 'DDEC070165 ' . $GabDtl['FOBPERBRGG'] . "\n";
                
                #DDEC0801
                if(count($GabDok) > 0){
                    if($GabDok['SERIEKS'] == $PebGab['SERIEKS'] && $GabDok['SERIBRGG'] == $GabDtl['SERIBRGG']){
                        $FF.= 'DDEC0801383' . $this->fixLen($GabDok['NODOKG'], 30) . $GabDok['KTRSSTBG'] . '182' . $GabDok['TGDOKG'] . "102\n";
                    }
                }
            }
        $FF.= 'UNS00201S' . "\n";
        $FF.= 'SDEC01015  ' . $this->fixLen($PebGab['JMBRGG'], 5, '0', STR_PAD_LEFT) . "\n";
        ($nJmGab > 0) ? $FF.= 'UNT00101' . "\n" : '';
        }
        $FF.= 'UNZ00101' . "\n";
        
        unset($PebGab);
        unset($GabDtl);
        unset($GabDok);
        unset($arrPjt);
        
        
        $fullPaht = 'FLAT/' . $EDINUMBER;
        if (!is_dir($fullPaht)){mkdir($fullPaht,0777,true);}
        $fullPaht = $fullPaht . '/' . $car . '.FLT';
        if (file_exists($fullPaht)){unlink($fullPaht);}
        $handle = fopen($fullPaht, 'w');
        fwrite($handle, $FF);
        fclose($handle);
        if (file_exists($fullPaht)) {$hsl = '2|' . $KDKPBC;}
        else {$hsl = '0|';}
        return $hsl;
    }
    
    function crtQueuePEB($car){
        $this->load->model('actTranslator');
        $SNRF       = date('ymdHis') . rand(10, 99);
        $EDINUMBER  = $this->newsession->userdata('EDINUMBER');
        $EDIPaht    = 'EDI/' . $EDINUMBER;
        //print_r($SNRF .'|'.$EDINUMBER.'|'.$EDIPaht);die();
        if (!is_dir($EDIPaht)){
            mkdir($EDIPaht,0777,true);
        }else{
            chmod($EDIPaht,0777);
        }
        $FLTPaht    = 'FLAT/' . $EDINUMBER;
        if (!is_dir($FLTPaht)){
            mkdir($FLTPaht,0777,true);
        }else{
            chmod($FLTPaht,0777);
        }
        $this->actTranslator->getEDIFILE($EDINUMBER,'DOKPEB',$SNRF,$car.'.FLT');
		if (file_exists($EDIPaht.'/'. $car .'.EDI') && file_exists($FLTPaht.'/'. $car .'.FLT')){
            $dirEdiNumber = 'TRANSACTION/' . $EDINUMBER . '/' . date('Ymd');
            if (!is_dir($dirEdiNumber)){mkdir($dirEdiNumber,0777,true);}
            rename($FLTPaht.'/'. $car .'.FLT', $dirEdiNumber . '/' . $car . '.FLT');
            rename($EDIPaht.'/'. $car .'.EDI', $dirEdiNumber . '/' . $car . '.EDI');
            $rtn = $SNRF . '|' . $dirEdiNumber . '/' . $car . '.EDI' . '|' . $dirEdiNumber . '/' . $car . '.FLT';
        }
        else {
            $rtn = 'ERROR';
        }
        //print_r($rtn);
        return $rtn;
    }
    
    function updateSNRFpeb($car, $data) {
        #update t_bc30hdr
        $this->load->model('actMain');
        $arrData = explode('|', $data);
        $dataHdr['SNRF'] = $arrData[0];
        $dataHdr['DIRFLT'] = $arrData[2];
        $dataHdr['DIREDI'] = $arrData[1];
        $dataHdr['STATUS'] = '02';
        $dataHdr['CAR'] = $car;
        $dataHdr['KODE_TRADER'] = $this->kd_trader;
        $this->actMain->insertRefernce('t_bc30hdr', $dataHdr);

        #insert m_trader_dokoutbound
        $dataDOK['KODE_TRADER'] = $this->kd_trader;
        $dataDOK['KDKPBC'] = $arrData[3];
        $dataDOK['JNSDOK'] = 'BC30';
        $dataDOK['APRF'] = 'DOKPEB';
        $dataDOK['CAR'] = $car;
        $dataDOK['STATUS'] = '00';
        $this->actMain->insertRefernce('m_trader_dokoutbound', $dataDOK);
        return '0';
    }
    
    function cetakDokumenPEB($car, $jnPage, $arr ) {
        //print_r('sini oy');die();
        $this->load->library('fpdf');
        $this->load->model('actMain');
        $this->load->model('dokBC30PEB');
        $this->dokBC30PEB->ciMain($this->actMain);
        $this->dokBC30PEB->fpdf($this->fpdf);
		$this->dokBC30PEB->segmentUri($arr);		
		//$this->dokBC30PKBE->data($data);
        $this->dokBC30PEB->showPage($car, $jnPage);
    }
    
    function getSSPCP($car) {
        $this->load->model('actMain');
        $query = "SELECT a.*,URAIAN_KPBC as 'URKPBC', KDKTR
                  FROM   t_bc30ntb a 
						Left Join m_kpbc b on a.KDKPBC = b.KDKPBC
						left join t_bc30hdr c on a.KODE_TRADER = c.KODE_TRADER and a.CAR = c.CAR
                  WHERE  a.CAR = '" . $car . "' AND a.KODE_TRADER = " . $this->kd_trader;
        $aNtb = $this->actMain->get_result($query);
        if (count($aNtb) > 0) {
            $query = "SELECT AKUN_SSPCP,NILAI
                      FROM   t_bc30ntbakun
                      WHERE CAR = '" . $car . "' AND 
                            KODE_TRADER = " . $this->kd_trader;
            $arrakun = $this->actMain->get_result($query, true);
            foreach ($arrakun as $a) {
                $hasil[$a['AKUN_SSPCP']] = $a['NILAI'];
            }
            $hasil = $hasil + $aNtb;
            return $hasil;
        } else {
            $arrSet = $this->actMain->get_Setting('BC30', 'PNBP');
            $hasil['423216'] = $arrSet['PNBP'];
            $query = "SELECT a.KDKTR ,URAIAN_KPBC as 'URKPBC', NPWP, a.NAMAEKS as 'NAMA',a.NPWPEKS as 'NPWPEKS'
                      FROM   t_bc30hdr a Left Join m_kpbc b on a.KDKTR = b.KDKPBC
                      WHERE CAR = '" . $car . "' AND 
                            KODE_TRADER = " . $this->kd_trader;
            $arrHdr = $this->actMain->get_result($query);
            $hasil = $hasil + $arrHdr;
            return $hasil;
        }
    }
    
    function setSSPCP() {
        $this->load->model('actMain');
        $SSPCP = $this->input->post('SSPCP');
        
        $SSPCP['KODE_TRADER'] = $this->kd_trader;
        $this->actMain->insertRefernce('t_bc30ntb', $SSPCP);
        //insert batch
        //detelete
        $this->db->delete('t_bc30ntbakun', array('KODE_TRADER' => $SSPCP['KODE_TRADER'], 'CAR' => $SSPCP['CAR']));
        $SSPCPAKUN = $this->input->post('SSPCPAKUN');
        foreach ($SSPCPAKUN as $key => $val) {
            if ((int) $val > 0) {
                $dataAkun[] = array('KODE_TRADER' => $SSPCP['KODE_TRADER'], 'CAR' => $SSPCP['CAR'], 'AKUN_SSPCP' => $key, 'NILAI' => $val);
            }
        }
        $this->db->insert_batch('t_bc30ntbakun', $dataAkun);
        return true;
    }
    
    public function daftarRespon($car,$tipe) {
        $this->load->library('newtable');
        $this->load->model("actMain");
        $SQL = " SELECT CAR, a.KDKTR as 'Kpbc', concat(RESKD,' - ',b.URAIAN) as 'Respon',  a.RESTG as 'Tgl Respon', a.RESWK as 'Wkt Respon',
                a.RESKD, a.RESTG, a.RESWK
                FROM t_bc30respeb a LEFT JOIN M_TABEL b ON a.RESKD = b.KDREC and b.MODUL = 'BC30' AND b.KDTAB = 'STATUS' 
                WHERE a.CAR='" . $car . "' AND a.KODE_TRADER = '" . $this->kd_trader . "'";      
        $this->newtable->keys(array('CAR','RESKD','RESTG','RESWK'));
        $this->newtable->hiddens(array('CAR','RESKD','RESTG','RESWK'));
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->action(site_url('bc30/beforecetakrespon/'.$car));
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $prosesnya = array(
            'Print Respon' => array('NEWPOPCETAK', site_url('bc30/cetakPEB'), '1', ' CETAK RESPON |900|550'));
        
        $this->newtable->orderby(RESTG, RESWK, RESKD);
        $this->newtable->sortby("DESC");
        $this->newtable->tipe_check('radio');
        $this->newtable->set_formid("f" . $tipe);
        $this->newtable->set_divid("div" . $tipe);
        $this->newtable->rowcount(10);
        $this->newtable->clear();
        $this->newtable->menu($prosesnya);
        $this->newtable->show_search(false);
        $this->newtable->tipe_proses('button');
        $tabel .= $this->newtable->generate($SQL);
        $arrdata['CAR'] = $car;
        $arrdata['tabel'] = $tabel;
        $arrdata['tipe'] = $tipe;
        if ($this->input->post("ajax")){return $tabel;}
        else{return $arrdata;}
    }
    
    function downloadBUE(){
        $arrCar = $this->input->post('tb_chkfBC30');
        $car    = implode(',', $arrCar);
        //print_r($car);die();
        $EDINUMBER = $this->newsession->userdata('EDINUMBER');
        $this->load->library("nuSoap_lib");
        $nuSoap_lib = new nusoap_client('http://10.1.5.78/wsT2g/index.php?wsdl'); 
        //print_r($nuSoap_lib);die();
        if ($nuSoap_lib->fault) {
            $error = 'Error: ' . $nuSoap_lib->fault;
        }
        else {
            if ($nuSoap_lib->getError()) {
                $error = 'error ws : ' . $nuSoap_lib->getError();
            }
            else {
                $genBUE = $nuSoap_lib->call('backupBUE', array( 'car'=> $car ,'kode_trader'  => $this->kd_trader));
                //print_r($genBUE.'|');die();
                if($genBUE){
                    $ftp_conn = ftp_connect('10.1.5.78') or die('Could not connect to 10.1.5.78');
                    if(ftp_login($ftp_conn, 'Administrator', 'Bismillah')){
                        //move to ftp
                        ftp_pasv($ftp_conn, true);
                        $file = $this->kd_trader . '.BUE';
                        $paht_local = getcwd() . '/TRANSACTION/' . $EDINUMBER . '/'.$file;
                        $paht_remot = '/wsT2G/files/' . $file ;
                        chmod($paht_local,0777);
                        //print_r($genBUE.'|'.$ftp_conn.'|'.$paht_local.'|'.$paht_remot);die();
                        if (ftp_get($ftp_conn, $paht_local, $paht_remot, FTP_BINARY)){
                            $error = 'MSG|OK|'. base_url('TRANSACTION/'.$EDINUMBER.'/'.$this->kd_trader.'.BUE').'|Generate File Berhasil.<br>Silahkan didownload.';
                        }
                        else{
                            $error = 'MSG|ERR||Generate File Gagal';
                        }
                    }else{
                        $error = "Login ftp gagal.";
                    }
                    ftp_close($ftp_conn);
                }
            }
        }
        return $error;
        
    }
    
    function downloadBUEpkbe(){
        $arrCar = $this->input->post('tb_chkfhdrPKBE');
        $car    = implode(',', $arrCar);
        $EDINUMBER = $this->newsession->userdata('EDINUMBER');
        $this->load->library("nuSoap_lib");
        $nuSoap_lib = new nusoap_client('http://10.1.5.78/wsT2g/index.php?wsdl'); 
        //print_r($nuSoap_lib);die();
        if ($nuSoap_lib->fault) {
            $error = 'Error: ' . $nuSoap_lib->fault;
        }
        else {
            if ($nuSoap_lib->getError()) {
                $error = 'error ws : ' . $nuSoap_lib->getError();
            }
            else {
                $genBUE = $nuSoap_lib->call('backupBUEpkbe', array( 'car'=> $car ,'kode_trader'  => $this->kd_trader));
                //print_r($genBUE);die();
                if($genBUE){
                    $ftp_conn = ftp_connect('10.1.5.78') or die('Could not connect to 10.1.5.78'); 
                    if(ftp_login($ftp_conn, 'Administrator', 'Bismillah')){
                        //move to ftp
                        ftp_pasv($ftp_conn, true);
                        $file = $this->kd_trader . '.BUE';
                        $paht_local = getcwd() . '/TRANSACTION/' . $EDINUMBER . '/'.$file;
                        $paht_remot = '/wsT2G/files/' . $file ;
                        chmod($paht_local,0777);
                        //print_r($file.'|'.$paht_local.'|'.$paht_remot);die();
                        if (ftp_get($ftp_conn, $paht_local, $paht_remot, FTP_BINARY)){
                            $error = 'MSG|OK|'. base_url('TRANSACTION/'.$EDINUMBER.'/'.$this->kd_trader.'.BUE').'|Generate File Berhasil.<br>Silahkan didownload.';
                        }
                        else{
                            $error = 'MSG|ERR||Generate File Gagal';
                        }
                    }else{
                        $error = "Login ftp gagal.";
                    }
                    ftp_close($ftp_conn);
                }
            }
        }
        return $error;
        
    }
    
    function uploadBUE(){
        $element = 'fileBUE';
        $EDINUMBER = $this->newsession->userdata('EDINUMBER');
        if ($element != "") {
            $path = $_FILES[$element]['name'];
            $ftype = pathinfo($path, PATHINFO_EXTENSION);
            //die($ftype.' = '.$arrtype);
            if (!empty($_FILES[$element]['error'])) {
                switch ($_FILES[$element]['error']){
                    case'1':$error = "Ukuran File Terlalu Besar.";break;
                    case'3':$error = "File Yang Ter-Upload Tidak Sempurna.";break;
                    case'4':$error = "File Kosong Atau Belum Dipilih.";break;
                    case'6':$error = "Direktori Penyimpanan Sementara Tidak Ditemukan.";break;
                    case'7':$error = "File Gagal Ter-Upload.";break;
                    case'8':$error = "Proses Upload File Dibatalkan.";break;
                    default :$error = "Pesan Error Tidak Ditemukan.";break;
                }
            }
            else if (empty($_FILES[$element]['tmp_name']) || ($_FILES[$element]['tmp_name'] == 'none')) {
                $error = "File Gagal Ter-Upload.";
            } 
            else if($ftype != 'BUE') {
                $error = "Tipe File Salah.<br>Tipe File Yang Diterima : *.BUE";
            }
            else {
                $filename = 'TRANSACTION/' . $EDINUMBER . '/' . $this->kd_trader . '.' .$ftype;
                mkdir('TRANSACTION/' . $EDINUMBER . '/',0777,true);
                chmod('TRANSACTION/' . $EDINUMBER . '/',0777);
                @unlink($filename);
                if(move_uploaded_file($_FILES[$element]['tmp_name'], $filename)){
                    $ftp_conn = ftp_connect('10.1.5.78') or die('Could not connect to 10.1.5.78');
                    if(ftp_login($ftp_conn, 'Administrator', 'Bismillah')){
                        //move to ftp
                        ftp_pasv($ftp_conn, true);
                        $file = $this->kd_trader . '.' .$ftype;
                        $paht_local = getcwd() . '/TRANSACTION/' . $EDINUMBER . '/'.$file;
                        $paht_remot = '/wsT2G/files/' . $file ;
                        if (ftp_put($ftp_conn, $paht_remot , $paht_local, FTP_BINARY)){
                            $this->load->library("nuSoap_lib");
                            $nuSoap_lib = new nusoap_client('http://10.1.5.78/wsT2g/index.php?wsdl');
                            if ($nuSoap_lib->fault) {
                                $error = 'Error: ' . $nuSoap_lib->fault;
                            }
                            else {
                                if ($nuSoap_lib->getError()) {
                                    $error = 'error ws : ' . $nuSoap_lib->getError();
                                }
                                else {
                                    $error = 'return ws : '.$nuSoap_lib->call('restoreBUE', array( 'namaFile'=> $file ,
                                                                              'kode_trader'  => $this->kd_trader));
                                }
                            }
                            //$error = "Put file from ftp berhasil.";
                        }
                        else{ 
                            $error = "Put file from ftp gagal.";
                        }
                    }else{
                        $error = "Login ftp gagal.";
                    }
                    ftp_close($ftp_conn);
                }
                else {
                    $error = "File Gagal Ter-Upload.";
                }
            }
        }
        else {
            $error = "Parameter Tidak Ditemukan.";
        }
        @unlink($_FILES[$element]);
        return $error;
    }
    
    function uploadBUEpkbe(){
        $element = 'fileBUEpkbe';
        $EDINUMBER = $this->newsession->userdata('EDINUMBER');
        if ($element != "") {
            $path = $_FILES[$element]['name'];
            $ftype = pathinfo($path, PATHINFO_EXTENSION);
            //die($ftype.' = '.$arrtype);
            if (!empty($_FILES[$element]['error'])) {
                switch ($_FILES[$element]['error']){
                    case'1':$error = "Ukuran File Terlalu Besar.";break;
                    case'3':$error = "File Yang Ter-Upload Tidak Sempurna.";break;
                    case'4':$error = "File Kosong Atau Belum Dipilih.";break;
                    case'6':$error = "Direktori Penyimpanan Sementara Tidak Ditemukan.";break;
                    case'7':$error = "File Gagal Ter-Upload.";break;
                    case'8':$error = "Proses Upload File Dibatalkan.";break;
                    default :$error = "Pesan Error Tidak Ditemukan.";break;
                }
            }
            else if (empty($_FILES[$element]['tmp_name']) || ($_FILES[$element]['tmp_name'] == 'none')) {
                $error = "File Gagal Ter-Upload.";
            } 
            else if($ftype != 'BUE') {
                $error = "Tipe File Salah.<br>Tipe File Yang Diterima : *.BUE";
            }
            else {
                $filename = 'TRANSACTION/' . $EDINUMBER . '/' . $this->kd_trader . '.' .$ftype;
                mkdir('TRANSACTION/' . $EDINUMBER . '/',0777,true);
                chmod('TRANSACTION/' . $EDINUMBER . '/',0777);
                @unlink($filename);
                if(move_uploaded_file($_FILES[$element]['tmp_name'], $filename)){
                    $ftp_conn = ftp_connect('10.1.5.78') or die('Could not connect to 10.1.5.78');
                    if(ftp_login($ftp_conn, 'Administrator', 'Bismillah')){
                        //move to ftp
                        ftp_pasv($ftp_conn, true);
                        $file = $this->kd_trader . '.' .$ftype;
                        $paht_local = getcwd() . '/TRANSACTION/' . $EDINUMBER . '/'.$file;
                        $paht_remot = '/wsT2G/files/' . $file ;
                        if (ftp_put($ftp_conn, $paht_remot , $paht_local, FTP_BINARY)){
                            $this->load->library("nuSoap_lib");
                            $nuSoap_lib = new nusoap_client('http://10.1.5.78/wsT2g/index.php?wsdl');
                            if ($nuSoap_lib->fault) {
                                $error = 'Error: ' . $nuSoap_lib->fault;
                            }
                            else {
                                if ($nuSoap_lib->getError()) {
                                    $error = 'error ws : ' . $nuSoap_lib->getError();
                                }
                                else {
                                    $error = 'return ws : '.$nuSoap_lib->call('restoreBUEpkbe', array( 'namaFile'=> $file ,
                                                                              'kode_trader'  => $this->kd_trader));
                                }
                            }
                            //$error = "Put file from ftp berhasil.";
                        }
                        else{ 
                            $error = "Put file from ftp gagal.";
                        }
                    }else{
                        $error = "Login ftp gagal.";
                    }
                    ftp_close($ftp_conn);
                }
                else {
                    $error = "File Gagal Ter-Upload.";
                }
            }
        }
        else {
            $error = "Parameter Tidak Ditemukan.";
        }
        @unlink($_FILES[$element]);
        return $error;
    }
    
    function getTOD($idModa,$Regiona, $Tgdaft){ 
        $this->load->model('actMain');
        if($Tgdaft != '' && $Tgdaft != 'undefined'){
            $sql="select CONCAT(a.REGION,'|',a.NFREIGHT,'|',a.NASURANSI,'|',b.URAIAN_NEGARA) as VAL FROM M_TOD a
                    left join m_negara b on b.REGION = a.region
                  where b.KODE_NEGARA = '".$Regiona."' and date_format(a.TGLBERLAKU, '%d%m%Y') <= '".$Tgdaft."' and a.JNSMODA='".$idModa."' order by a.TGLBERLAKU desc limit 1";
            $rtn = $this->actMain->get_uraian($sql,'VAL');
        }else{
            $sql = "select CONCAT(a.REGION,'|',a.NFREIGHT,'|',a.NASURANSI,'|',b.URAIAN_NEGARA) as VAL FROM M_TOD a
                    left join m_negara b on b.REGION = a.region
                  where b.KODE_NEGARA = '".$Regiona."' and a.JNSMODA='".$idModa."' order by MAX(a.TGLBERLAKU) desc limit 1";
            $rtn = $this->actMain->get_uraian($sql,'VAL');
        }
        return $rtn;
    }
}