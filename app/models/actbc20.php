<?php
if (!defined('BASEPATH')){exit('No direct script access allowed');}
class actBC20 extends CI_Model {
    var $kd_trader = '';
    public function __construct() {
        parent::__construct();
        $this->kd_trader = $this->newsession->userdata('KODE_TRADER');
        return true;
    }
    
    public function daftar($tipe = "") { 
        $this->load->model("actMain");
        $this->load->library('newtable');
        $jenis = "pib";
        $title = "Daftar Permohonan PIB";
        $prosesnya = array(
            'Create'    => array('ADDAJAX', site_url('bc20/create'), '0', '_content_'),
            'View'      => array('GETAJAX', site_url('bc20/edit'), '1', '_content_'),
            'Copy'      => array('GETPOP', site_url('bc20/copydata'), '1', ' COPY DOKUMEN PIB |400|200'),
            'Print'     => array('GETPOP', site_url('bc20/beforecetak'), '1', ' CETAK DOKUMEN PIB |400|200'),
            'Upload BUP'=> array('GETPOP', site_url('bc20/uploadbup/viewform'), '0', ' UPLOAD FILE BUP |400|200'),
            'Download BUP' => array('DOWNLOADAJAX', site_url('bc20/downloadbup/download'), '0', ' UPLOAD FILE BUP |400|200'),
            'Delete' => array('DELETEAJAX', site_url('bc20/delete'), '1', '_content_')
        );
        //'Print Respon' => array('GETPOP', site_url('bc20/beforecetakrespon'), '1', ' CETAK RESPON PIB |800|450'),
        $judul = "Dokumen BC 2.0";
        $SQL = "SELECT  concat(SUBSTRING(hdr.CAR,1,6),'-',SUBSTRING(hdr.CAR,7,6),'-',SUBSTRING(hdr.CAR,13,8),'-',SUBSTRING(hdr.CAR,21,8))  AS 'No Aju',
                        DATE_FORMAT(hdr.CREATED_TIME, '%d %M %Y') AS 'Tgl Dokumen', 
                        hdr.PASOKNAMA AS 'Pemasok',
                        hdr.INDNAMA AS 'Pemilik',
                        hdr.JMBRG AS 'Jml Barang',
                        hdr.JMCONT AS 'Jml Kontainer',
                        tbl.URAIAN AS 'Status',
                        hdr.CAR
                FROM T_BC20HDR hdr Left Join M_TABEL tbl ON hdr.STATUS = tbl.KDREC AND tbl.KDTAB='STATUS' AND tbl.MODUL='BC20' 
                WHERE KODE_TRADER= '" . $this->kd_trader . "'";
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->action(site_url("bc20/daftar"));
        $this->newtable->keys(array('CAR'));
        $this->newtable->hiddens(array('CAR'));
        $this->newtable->search(array(array('CAR', 'No. Aju'),
            array('CREATED_TIME', 'tgl Dokumen', 'tag-tanggal'),
            array('PASOKNAMA', 'Pemasok'),
            array('INDNAMA', 'Pemilik'),
            array('STATUS', 'Status', 'tag-select', $this->actMain->get_mtabel('STATUS'))));
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->orderby(1, 2);
        $this->newtable->sortby("DESC");
        $this->newtable->tipe_check('radio');
        $this->newtable->set_formid("f" . $tipe);
        $this->newtable->set_divid("div" . $tipe);
        $this->newtable->rowcount(20);
        $this->newtable->tipe_proses('button');
        $this->newtable->menu($prosesnya);
        $this->newtable->clear();
        $tabel .= $this->newtable->generate($SQL);
        $arrdata = array('title' => $title,
            'judul' => $judul,
            'tabel' => $tabel,
            'jenis' => $jenis,
            'tipe' => $tipe);
        if ($this->input->post("ajax"))
            return $tabel;
        else
            return $arrdata;
    }

    public function daftarRespon($car,$tipe) {
        $this->load->library('newtable');
        $this->load->model("actMain");
        $SQL = "SELECT  CAR, RESKD, RESTG, RESWK, KPBC, concat(RESKD,' - ',b.URAIAN) as 'Respon', 
                        RESTG as 'Tgl Respon', RESWK as 'Wkt Respon', PIBNO as 'Nomor PIB'
                FROM T_BC20RES a LEFT JOIN  M_TABEL b ON a.RESKD = b.KDREC and b.MODUL = 'BC20' AND b.KDTAB = 'JENIS_RESPON'
                WHERE a.CAR='" . $car . "' AND a.KODE_TRADER = '" . $this->kd_trader . "'";
        $this->newtable->keys(array('CAR','RESKD','RESTG','RESWK'));
        $this->newtable->hiddens(array('CAR','RESKD','RESTG','RESWK'));
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->action(site_url('bc20/beforecetakrespon/'.$car));
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $prosesnya = array(
            'Print Respon' => array('NEWPOPCETAK', site_url('bc20/cetak'), '1', ' CETAK RESPON |900|550'));
        
        $this->newtable->orderby(1, 2);
        $this->newtable->sortby("DESC");
        $this->newtable->tipe_check('radio');
        $this->newtable->set_formid("f" . $tipe);
        $this->newtable->set_divid("div" . $tipe);
        $this->newtable->rowcount(10);
        $this->newtable->clear();
        $this->newtable->menu($prosesnya);
        $this->newtable->show_search(false);
        $this->newtable->tipe_proses('button');
        $tabel .= $this->newtable->generate($SQL);
        $arrdata['CAR'] = $car;
        $arrdata['tabel'] = $tabel;
        $arrdata['tipe'] = $tipe;
        if ($this->input->post("ajax")){return $tabel;}
        else{return $arrdata;}
    }

    public function get_header($aju = "") {
        $data = array();
        $data_pgt = array();
        $this->load->model("actMain");
        if ($aju) {
            $query = "SELECT A.*,
                            f_kpbc(A.KDKPBC) URAIAN_KPBC,
                            f_negara(A.PASOKNEG) URPASOKNEG,
                            f_negara(A.ANGKUTFL) URANGKUTFL,
                            f_timbun(A.TMPTBN,A.KDKPBC) URTMPTBN,
                            f_valuta(A.KDVAL) URKDVAL,
                            f_pelab(A.PELMUAT) PELMUATUR,
                            f_pelab(A.PELTRANSIT) PELTRANSITUR,
                            f_pelab(A.PELBKR) PELBKRUR,
                            f_ref('SKEP_FASILITAS',A.KDFAS) URKDFAS,
                            B.URAIAN as URSTATUS,
                            C.NAMA_TTD,
                            C.KOTA_TTD,
                            IF(A.TANGGAL_TTD='0000-00-00',curdate(),A.TANGGAL_TTD) as 'TANGGAL_TTD'
                    FROM t_bc20hdr A left join m_tabel B ON B.KDREC = A.STATUS AND B.KDTAB = 'STATUS' AND B.MODUL = 'BC20'
                                     left join m_trader_bc20 C ON C.KODE_TRADER = A.KODE_TRADER
                    WHERE A.CAR='" . $aju . "' AND A.KODE_TRADER='" . $this->kd_trader . "'"; 
                    // die($query);

            $dataarray = $this->actMain->get_result($query);
            $query = "SELECT KDBEBAN,
                             KDFASIL,
                             NILBEBAN 
                      FROM  t_bc20pgt 
                      WHERE t_bc20pgt.CAR='" . $aju . "' AND t_bc20pgt.KODE_TRADER = '" . $this->kd_trader . "'";
            $arrPGT = $this->actMain->get_result($query, true);
            foreach ($arrPGT as $row) {
                if ($row['KDBEBAN'] == '5' or $row['KDBEBAN'] == '6' or $row['KDBEBAN'] == '7') { //cukai dijadikan 1 kodebeban
                    $row['KDBEBAN'] = '5';
                }
                $data_pgt[$row['KDBEBAN']][$row['KDFASIL']] += $row['NILBEBAN'];
                $data_pgt['TOTAL'][$row['KDFASIL']] += $row['NILBEBAN'];
            }
            $data['act'] = 'update';// array('act' => 'update');
        }
        else {
            $query = "SELECT 	A.KODE_TRADER, A.KODE_ID 'IMPID', A.ID 'IMPNPWP', A.NAMA 'IMPNAMA', A.ALAMAT 'IMPALMT', A.KODE_ID 'PPJKID', " .
                               "A.ID 'PPJKNPWP', A.NAMA 'PPJKNAMA', A.ALAMAT 'PPJKALMT', A.NO_PPJK 'PPJKNO', A.TANGGAL_PPJK 'PPJKTG', A.EDINUMBER, " .
                               "B.LAST_AJU, B.NAMA_TTD, B.KOTA_TTD, B.IMPSTATUS, B.APIKD, B.APINO, C.NAMA 'NAMA_CP', C.TELEPON 'TELP_CP', " .
                               "ID AS 'ID_TRADER', 0 AS 'FOB', 0 AS 'FREIGHT', 0 AS 'ASURANSI', 0 AS 'CIF', 0 AS 'CIFRP', 0 AS 'BRUTO', " .
                               "0 AS 'NETTO', 0 AS 'TAMBAHAN', 0 AS 'DISCOUNT', 0 AS 'NILAI_INVOICE', 0 AS 'NDPBM', curdate() as 'TANGGAL_TTD' " .
                     "FROM T_USER C INNER JOIN M_TRADER A ON A.KODE_TRADER = C.KODE_TRADER " .
                                   "INNER JOIN M_TRADER_BC20 B ON B.KODE_TRADER = C.KODE_TRADER ".
                     "WHERE C.USER_ID = ". $this->newsession->userdata('USER_ID');//die($query);
            $dataarray = $this->actMain->get_result($query);
            $data['act'] = 'save';
        }
        
        //get_mtabel($jenis, $by = 1, $empty = TRUE, $where = "",$uraian='KODEUR',$modul='BC20')
        $data['aju'] = $aju;
        $data['TIPE_TRADER']    = $this->newsession->userdata('TIPE_TRADER');
        $data['HEADER']         = array_change_key_case($dataarray, CASE_UPPER);
        $data['JNPIB']          = $this->actMain->get_mtabel('JENIS_PIB', 1, false, '', 'KODEDANUR');
        $data['JNIMP']          = $this->actMain->get_mtabel('JENIS_IMPOR', 1, false, '', 'KODEDANUR');
        $data['CRBYR']          = $this->actMain->get_mtabel('CARA_PEMBAYARAN', 1, false, '', 'KODEDANUR');
        $data['IMPID']          = $this->actMain->get_mtabel('JENIS_IDENTITAS', 1, false, '', 'KODEDANUR');
        $data['APIKD']          = $this->actMain->get_mtabel('JENIS_API', 1, true, '', 'KODEDANUR');
        $data['MODA']           = $this->actMain->get_mtabel('MODA', 1, false, '', 'KODEDANUR');
        $data['DOKTUPKD']       = $this->actMain->get_mtabel('JENIS_DOKUMEN', 1, false, '', 'KODEDANUR');
        $data['data_pgt']       = $data_pgt;
        $data['data_container'] = $this->get_tblKontainer($aju);
        $data['data_kemasan']   = $this->get_tblKemasaan($aju);
        $data['data_dokumen']   = $this->get_tblDokumen($aju);
        $data['MAXLENGTH']      = '<input type="hidden" id="MAXLENGTH" value="' . $this->actMain->get_uraian("SELECT F_MAXLENGTH('t_bc20hdr') as MAXLENGTH", 'MAXLENGTH') . '"/>';
        return $data;
    }

    public function set_header($type = "") {
        $this->load->model("actMain");
        if (strtolower($type) == "save" || strtolower($type) == "update") {
            $HEADER = $this->input->post('HEADER');
            //echo($HEADER['INDNPWP'].'<br>');
            $HEADER['KODE_TRADER']  = $this->kd_trader;
            $HEADER['IMPSTATUS']    = implode(',', $HEADER['IMPSTATUS']);
            $HEADER['IMPNPWP']      = str_replace('.', '', str_replace('-', '', $HEADER['IMPNPWP']));
            $HEADER['PPJKNPWP']     = str_replace('.', '', str_replace('-', '', $HEADER['PPJKNPWP']));
            $HEADER['INDNPWP']      = str_replace('.', '', str_replace('-', '', $HEADER['INDNPWP']));
            $this->actMain->insertRefernce('M_TRADER_SUPPLIER', $HEADER);
            $this->actMain->insertRefernce('M_TRADER_KAPAL', $HEADER);
            $this->actMain->insertRefernce('M_TRADER_IMPORTIR', $HEADER);
            $this->actMain->insertRefernce('M_TRADER_INDENTOR', $HEADER);
            if (strtolower($type) == "save") {
                $CAR = $this->actMain->get_car('BC20');
                $HEADER["CAR"]          = $CAR;
                $HEADER["STATUS"]       = '000';
                $HEADER["CIFRP"]        = str_replace(",", "", $HEADER["CIFRP"]);
                $HEADER['KODE_TRADER']  = $this->kd_trader;
                $HEADER["CREATED_BY"]   = $this->newsession->userdata('USER_ID');
                $HEADER["CREATED_TIME"] = date('Y-m-d H:i:s', time() - 60 * 60 * 1);
                $exec = $this->actMain->insertRefernce('T_BC20HDR', $HEADER);
                $sql = "call bc20hitungPungutan('" . $CAR . "'," . $HEADER['KODE_TRADER'] . ")";
                $this->db->query($sql);
                if ($exec) {
                    $this->actMain->set_car('BC20');
                    echo "MSG#OK#Proses header Berhasil#$CAR#";
                } 
                else {
                    echo "MSG#ERR#Proses data header Gagal#";
                }
            }
            else if (strtolower($type) == "update") {
                $CAR = $HEADER["CAR"];
                $HEADER["CIFRP"] = str_replace(",", "", $HEADER["CIFRP"]);
                $HEADER["UPDATED_BY"] = $this->newsession->userdata('USER_ID');
                $HEADER["UPDATED_TIME"] = date('Y-m-d H:i:s', time() - 60 * 60 * 1);
                $exec = $this->actMain->insertRefernce('T_BC20HDR', $HEADER);
                $sql = "call bc20hitungPungutan('" . $CAR . "'," . $HEADER['KODE_TRADER'] . ")";
                $this->db->query($sql);
                if ($exec) {
                    echo "MSG#OK#Proses header Berhasil#" . $CAR . "#";
                } else {
                    echo "MSG#ERR#Proses data header Gagal#";
                }
                //print_r($HEADER);die();
            }
        }
    }
    
    private function get_tblKontainer($car = '') {
        //$data = $this->lstKontainer($car);
        $arrdata = $this->lstDetil('kontainer', $car);
        $data['list'] = $this->load->view('list', $arrdata, true);
        $data['type'] = 'kontainer';
        return $this->load->view("bc20/lst", $data, true);
    }

    private function get_tblKemasaan($car = '') {
        //$data = $this->lstKemasan($car);
        $arrdata = $this->lstDetil('kemasan', $car);
        $data['list'] = $this->load->view('list', $arrdata, true);
        $data['type'] = 'kemasan';
        return $this->load->view("bc20/lst", $data, true);
    }

    private function get_tblDokumen($car = '') {
        //$data = $this->lstDokumen($car);
        $arrdata = $this->lstDetil('dokumen', $car);
        $data['list'] = $this->load->view('list', $arrdata, true);
        $data['type'] = 'dokumen';
        return $this->load->view("bc20/lst", $data, true);
    }

    function getPungutan($car) {
        $this->load->model("actMain");
        $query = "SELECT KDBEBAN,
                         KDFASIL,
                         NILBEBAN 
                  FROM  t_bc20pgt 
                  WHERE t_bc20pgt.CAR='" . $car . "' AND t_bc20pgt.KODE_TRADER = '" . $this->kd_trader . "'";
        $arrPGT = $this->actMain->get_result($query, true);
        foreach ($arrPGT as $row) {
            $data_pgt[$row['KDBEBAN']][$row['KDFASIL']] += $row['NILBEBAN'];
            $data_pgt['TOTAL'][$row['KDFASIL']] += $row['NILBEBAN'];
        }
        return $data_pgt;
    }

    function lstDetil($type = '', $aju){
        $this->load->library('newtable');
        $this->newtable->keys = '';
        if ($type == "barang") {
            $this->load->model('actMain');
            $SQL = "SELECT BRGURAI as 'URAIAN BARANG',f_formaths(NOHS) 'KODE HS',
                           SERIAL ,DNILINV AS 'Nilai Invoice', KEMASJM AS 'JLM. KEMASAN', 
                           CONCAT(KEMASJN,' - ',f_kemas(KEMASJN)) AS 'JENIS KEMASAN', 
                           NETTODTL AS 'NETTO', b.URAIAN as Status,CAR, NOHS
                     FROM t_bc20dtl a left join m_tabel b on b.MODUL = 'BC20' AND b.KDTAB = 'STATUS_DETIL' AND b.KDREC = a.DTLOK 
                     WHERE CAR= '$aju' AND KODE_TRADER = '" . $this->kd_trader . "'";
            $this->newtable->keys(array('CAR', 'SERIAL', 'NOHS'));
            $this->newtable->hiddens(array('CAR', 'NOHS'));
            $this->newtable->search(array(array('BRGURAI', 'Uraian Barang'),
                array('NOHS', 'Kode HS'),
                array('DTLOK', 'Status', 'tag-select', $this->actMain->get_mtabel('STATUS_DETIL'))));
            $this->newtable->orderby("SERIAL");
            $this->newtable->sortby("DESC");
        }
        else if ($type == "kemasan") {
            $SQL = "SELECT  JMKEMAS AS 'JUMLAH', 
                            JNKEMAS AS 'SERI',  
                            CONCAT(JNKEMAS,' - ',f_kemas(JNKEMAS)) AS 'KODE', 
                            MERKKEMAS AS 'MEREK', 
                            CAR 
                    FROM t_bc20kms 
                    WHERE CAR= '$aju' AND KODE_TRADER = '" . $this->kd_trader . "'";
            $this->newtable->keys(array("CAR", "SERI", "MEREK"));
            $this->newtable->hiddens(array("CAR", "SERI"));
            $this->newtable->search(array(array('JNKEMAS', 'Kode'), array('MERKKEMAS', 'Merek')));
            $this->newtable->orderby(4);
            $this->newtable->sortby("DESC");
        } 
        else if ($type == "kontainer") {
            $SQL = "SELECT  CONTNO AS 'Nomor', 
                            CONTNO AS 'SERI', 
                            f_ref('UKURAN_CONTAINER',CONTUKUR) AS 'Ukuran', 
                            f_ref('JENIS_CONTAINER',CONTTIPE) AS 'Tipe', 
                            CAR
                    FROM t_bc20con 
                    WHERE CAR= '$aju' AND KODE_TRADER = '" . $this->kd_trader . "'";
            $this->newtable->keys(array("CAR", "SERI"));
            $this->newtable->hiddens(array("CAR", "SERI"));
            $this->newtable->search(array(array('CONTNO', 'Nomor'), array('UKURAN_CONTAINER', 'Ukuran'), array('JENIS_CONTAINER', 'Tupe')));
            $this->newtable->orderby(4);
            $this->newtable->sortby("DESC");
        } 
        else if ($type == "dokumen") {
            $SQL = "SELECT  DOKKD AS 'Kode',
                            f_ref('KODE_DOKUMEN',DOKKD) AS 'Jenis', 
                            DOKNO AS 'Nomor',
                            DATE_FORMAT(DOKTG,'%Y-%m-%d') AS 'Tanggal', 
                            CAR, 
                            DOKKD AS SERI 
                    FROM t_bc20dok  
                    WHERE CAR= '$aju' AND KODE_TRADER = '" . $this->kd_trader . "'";
            $this->newtable->keys(array('CAR', 'SERI', 'Nomor'));
            $this->newtable->hiddens(array('CAR', 'SERI'));
            $this->newtable->search(array(
                array('DOKKD', 'Kode'),
                array("f_ref('KODE_DOKUMEN',DOKKD)", 'Jenis'),
                array('DOKNO', 'Nomor'),
                array("DATE_FORMAT(DOKTG,'%Y-%m-%d')", 'Tanggal', 'tag-tanggal')));
            $this->newtable->orderby(4);
            $this->newtable->sortby("DESC");
        } 
        else {
            return "Failed";
            exit();
        }
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->action(site_url('bc20/daftarDetil/' . $type . '/' . $aju));
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->rowcount(10);
        $this->newtable->clear();
        $this->newtable->set_formid("f" . $type);
        $this->newtable->set_divid("div" . $type);
        $process = array(
            'Tambah '   . $type => array('ADDAJAX', site_url('bc20/' . $type . '/' . $aju), '0', 'f' . $type . '_form'),
            'Ubah '     . $type => array('EDITAJAX', site_url('bc20/' . $type . '/' . $aju), '1', 'f' . $type . '_form'),
            'Hapus '    . $type => array('DELETEAJAX', site_url('bc20/' . $type . '/' . $aju), 'N', 'f' . $type . '_list'));
        $this->newtable->menu($process);
        $tabel = $this->newtable->generate($SQL);
        $arrdata = array("tabel" => $tabel);
        if ($this->input->post("ajax"))
            return $tabel;
        else
            return $arrdata;
    }

    function lstKontainer($car, $contno = '') {
        $data = array();
        $this->load->model("actMain");
        if ($car && $contno) {
            $query = "SELECT * FROM t_bc20con WHERE CAR = '$car' AND CONTNO = '$contno' AND KODE_TRADER = '$this->kd_trader'";
            $hasil = $this->actMain->get_result($query);
            $data['KONTAINER'] = array_change_key_case($hasil, CASE_UPPER);
            $data['act'] = 'update';
        }
        else {
            $data['act'] = 'save';
        }
        $data['TIPE']   = $this->actMain->get_mtabel('JENIS_CONTAINER');
        $data['UKURAN'] = $this->actMain->get_mtabel('UKURAN_CONTAINER');
        $data['KONTAINER']['CAR'] = $car;
        return $data;
    }

    function lstKemasan($car = "", $jns = "", $merk = "") {
        $data = array();
        $this->load->model("actMain");
        if ($car && $jns && $merk) {
            $query = "SELECT CAR, JNKEMAS, JMKEMAS, MERKKEMAS, f_kemas(JNKEMAS) URAIAN 
                      FROM t_bc20kms 
		      WHERE KODE_TRADER=" . $this->kd_trader . " AND 
                            CAR = '" . $car . "' AND 
                            JNKEMAS = '" . $jns . "' AND 
                            MERKKEMAS = '" . $merk . "'";
            $hasil = $this->actMain->get_result($query);
            $data['KEMASAN'] = array_change_key_case($hasil, CASE_UPPER);
            $data['act'] = 'update';
        }
        else {
            $data['act'] = 'save';
        }
        $data['KEMASAN']['CAR'] = $car;
        return $data;
    }

    function lstDokumen($car , $DOKKD = '', $DOKNO = '') {
        $this->load->model("actMain");
        if ($car && $DOKKD && $DOKNO) {
            $query = "SELECT * FROM t_bc20dok WHERE KODE_TRADER='" . $this->kd_trader . "' AND CAR = '$car' AND DOKKD = '$DOKKD' AND DOKNO = '$DOKNO'";
            $hasil = $this->actMain->get_result($query);
            $data['DOKUMEN'] = array_change_key_case($hasil, CASE_UPPER);
            $data['act'] = 'update';
        }
        else {
            $data['act'] = 'save';
        }
        $data['DOKUMEN']['CAR'] = $car;
        $data['DOKKD'] = $this->actMain->get_mtabel('KODE_DOKUMEN', 1, true, '', 'KODEDANUR');
        return $data;
    }

    function lstBarang($car = "", $seri = "") {
        $this->load->model("actMain");
        if ($car && $seri) {
            $query = "SELECT a.*, 
                             b.*,
                             c.*,
                             f_ref('SKEP_FASILITAS',a.KDFASDTL) KDFASDTLUR,
                             f_kemas(a.KEMASJN) KODE_KEMASANUR,
                             f_negara(a.BRGASAL) NEGARA_ASALUR, 
                             f_satuan(a.KDSAT) KODE_SATUANUR,
                             d.KDHRG ,
                             d.FREIGHT   AS HFREIGHT,
                             d.NILINV    AS HINVOICE,
                             d.ASURANSI  AS HASURANSI,
                             d.BTAMBAHAN AS HBTAMBAHAN,
                             d.DISCOUNT  AS HDISKON,
                             d.NDPBM     AS HNDPBM,
                             d.ApiNo     AS 'APINO',
                             e.URAIAN    AS 'URDTLOK',
                             a.CAR       AS 'CAR',
                             a.SERIAL    AS 'SERIAL',
                             a.KODE_TRADER AS 'KODE_TRADER'
                      FROM t_bc20dtl a LEFT JOIN t_bc20trf c ON c.KODE_TRADER = a.KODE_TRADER AND c.CAR=a.CAR AND c.SERIAL=a.SERIAL
                                       LEFT JOIN t_bc20fas b ON b.KODE_TRADER = a.KODE_TRADER AND b.CAR=a.CAR AND b.SERIAL=a.SERIAL
                                       LEFT JOIN t_bc20hdr d ON d.KODE_TRADER = a.KODE_TRADER AND d.CAR=a.CAR
                                       LEFT JOIN m_tabel e   ON e.MODUL = 'BC20' AND e.KDTAB = 'STATUS_DETIL' AND e.KDREC = a.DTLOK
                      WHERE a.CAR = '" . $car . "' AND a.SERIAL = '" . $seri . "' AND a.KODE_TRADER = " . $this->kd_trader;
            $hasil = $this->actMain->get_result($query);
            $data['DETIL']  = array_change_key_case($hasil, CASE_UPPER);
            $data['act']    = 'update';
        } 
        else if ($car != '') {
            $query = "SELECT  0 AS 'SERI_HS', 0 AS 'KEMASJM', 0 AS 'NETTODTL', 0 AS 'DNILINV', 0 AS 'BTDISKON', 0 AS 'JMLSAT', 0 AS 'FOB', 
                              0 AS 'DCIF', 0 AS 'CIFRP', 0 AS 'TRPBM', 0 AS 'FASBM', 0 AS 'TRPPPN', 0 AS 'FASPPN', 0 AS 'TRPPBM', 0 AS 'FASPBM', 
                              0 AS 'FASPPH', 0 AS 'TRPCUK', 0 AS 'SATCUKJM',0 AS 'FASCUK', IF(TRIM(APINO)='','7.5','2.5') AS 'TRPPPH',
                   (SELECT IFNULL(MAX(SERIAL) + 1,1) FROM t_bc20dtl WHERE CAR = t_bc20hdr.CAR  AND KODE_TRADER = t_bc20hdr.KODE_TRADER) AS 'SERIAL',
                              'Tidak Lenglap' AS 'URDTLOK', 
                              KDHRG ,
                              FREIGHT   AS HFREIGHT,
                              NILINV    AS HINVOICE,
                              ASURANSI  AS HASURANSI,
                              BTAMBAHAN AS HBTAMBAHAN,
                              DISCOUNT  AS HDISKON,
                              NDPBM     AS HNDPBM,
                              APINO     AS 'APINO',
                              CAR
		      FROM t_bc20hdr 
                      WHERE CAR='" . $car . "' AND KODE_TRADER = " . $this->kd_trader;//die($query);
            $dataarray = $this->actMain->get_result($query);
            $data['act']    = 'save';
            $data['DETIL']  = $dataarray;
        }
        $data['JENIS_TARIF']= $this->actMain->get_mtabel('JENIS_TARIF', 1, false, "AND KDREC IN ('1','2')");
        $data['KOMODITI']   = $this->actMain->get_mtabel('KOMODITI');
        $data['MAXLENGTH']  = '<input type="hidden" id="MAXLENGTH" value="' . $this->actMain->get_uraian("SELECT F_MAXLENGTH('t_bc20dtl') as MAXLENGTH", 'MAXLENGTH') . '"/>';
        $data['JMLDTL']  = $this->actMain->get_uraian("SELECT COUNT(*) JML FROM T_BC20DTL WHERE CAR='" . $car . "' AND KODE_TRADER = " . $this->kd_trader, 'JML');
        $data['JENIS_KERINGANAN']  = $this->actMain->get_mtabel('JENIS_KERINGANAN',1, false);
        return $data;
    }

    function setKontainer($type, $car) {
        if ($type == 'save' || $type == 'update') {
            $this->load->model("actMain");
            $data = $this->input->post('KONTAINER');
            if (!$data['CAR']) {
                return "MSG|ERR||Simpan data kontainer Gagal.<br>Data header belum tersimpan.";
            }
            $data['KODE_TRADER'] = $this->kd_trader;
            $exec = $this->actMain->insertRefernce('t_bc20con',$data);
            if ($exec) {
                $sql = "call P_BC20UPDCON('" . $data['CAR'] . "'," . $this->kd_trader . ")";
                $this->db->query($sql);
                return "MSG|OK|".site_url('bc20/daftarDetil/kontainer/'.$data['CAR'])."|Data kontainer berhasil disimpan.";
            } else {
                return "MSG|ERR||Proses data kontainer gagal.";
            }
        }
        else if ($type == 'delete'){
            $this->input->post('tb_chkfkontainer') ;
            foreach ($this->input->post('tb_chkfkontainer') as $chkitem){
                $arrchk = explode("|", $chkitem);
                $car    = $arrchk[0];
                $arrSeri .= ",'" . $arrchk[1] . "'";
            }
            $arrSeri = substr($arrSeri, 1);
            $sql = "DELETE FROM T_BC20CON WHERE CAR='" . $car . "' AND KODE_TRADER = '" . $this->kd_trader . "' AND CONTNO in (" . $arrSeri . ')';
            $exec = $this->db->query($sql);
            if ($exec){
                $sql = "call P_BC20UPDCON('" . $car . "'," . $this->kd_trader . ')';
                $this->db->query($sql);
                return "MSG|OK|".site_url('bc20/daftarDetil/kontainer/'.$car)."|Data kontainer berhasil didelete.";
            }
            else {
                return "MSG|ERR||Data kontainer gagal didelete.";
            }
        }
    }

    function setKemasan($type,$car) {
        if ($type == 'save' || $type == 'update'){
            $this->load->model('actMain');
            $data = $this->input->post('KEMASAN');
            if (!$data['CAR']) {
                return "MSG|ERR||Simpan data kemasan Gagal.<br>Data header belum tersimpan.";
            }
            $data['KODE_TRADER'] = $this->kd_trader;
            $exec = $this->actMain->insertRefernce('t_bc20kms',$data);
            if ($exec) {
                return "MSG|OK|".site_url('bc20/daftarDetil/kemasan/'.$data['CAR'])."|Data kemasan berhasil disimpan.";
            }
            else {
                return "MSG|ERR||Proses data kemasan gagal.";
            }
        }
        else if ($type == 'delete') {
            foreach ($this->input->post('tb_chkfkemasan') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $data['CAR']        = $arrchk[0];
                $data['JNKEMAS']    = $arrchk[1];
                $data['MERKKEMAS']  = $arrchk[2];
                $data['KODE_TRADER']= $this->kd_trader;
                $this->db->where($data);
                $exec = $this->db->delete('t_bc20kms');
            }
            if ($exec) {
                return 'MSG|OK|'.site_url('bc20/daftarDetil/kemasan/'.$car).'|Data kemasan berhasil didelete.';
            }
            else {
                return 'MSG|ERR||Data kemasan gagal didelete.';
            }
        }
    }

    function setDokumen($type,$car) {
        if ($type == 'save' || $type == 'update') {
            $this->load->model('actMain');
            $data = $this->input->post('DOKUMEN');
            if (!$data['CAR']) {
                return "MSG|ERR||Simpan data dokumen Gagal.<br>Data header belum tersimpan.";
            }
            $data['KODE_TRADER'] = $this->kd_trader;
            $exec = $this->actMain->insertRefernce('t_bc20dok',$data);
            if ($exec) {
                return "MSG|OK|".site_url('bc20/daftarDetil/dokumen/'.$data['CAR'])."|Data dokumen berhasil disimpan.";
            } else {
                return "MSG|ERR||Proses data dokumen gagal.";
            }
        }
        else if ($type == 'delete') {
            foreach ($this->input->post('tb_chkfdokumen') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $data['CAR']        = $arrchk[0];
                $data['DOKKD']      = $arrchk[1];
                $data['DOKNO']      = $arrchk[2];
                $data['KODE_TRADER']= $this->kd_trader;
                $this->db->where($data);
                $exec = $this->db->delete('t_bc20dok');
            }
            if ($exec) {
                return 'MSG|OK|'.site_url('bc20/daftarDetil/dokumen/'.$car).'|Data dokumen berhasil didelete.';
            } else {
                return 'MSG|ERR||Data dokumen gagal didelete.';
            }
        }
    }

    function setBarang($type,$car) {
        $this->load->model('actMain');
        if ($type == 'save' || $type == 'update'){
            $post = $this->input->post();
            $car = $this->input->post('CAR');
            if ($type == 'save') {
                $seri = (int) $this->actMain->get_uraian("SELECT MAX(SERIAL) AS MAXSERI FROM t_bc20dtl WHERE CAR='" . $car . "'", "MAXSERI") + 1;
            }
            else {
                $seri = $post['SERIAL'];
            }
            $BARANG                 = $post['DETIL'];
            $BARANG["KODE_TRADER"]  = $this->kd_trader;
            $BARANG["CAR"]          = $car;
            $BARANG["NOHS"]         = str_replace(".", "", $BARANG["NOHS"]);
            $BARANG["SERIAL"]       = $seri;
            unset($BARANG["INVOICE"]);
            unset($BARANG["CIFRP"]);
            $exec['barang']         = $this->actMain->insertRefernce('t_bc20dtl', $BARANG, true);
            $exec['trader_barang']  = $this->actMain->insertRefernce('m_trader_barang', $BARANG, true);

            $FASILITAS                  = $post['FASILITAS'];
            $FASILITAS["KODE_TRADER"]   = $this->kd_trader;
            $FASILITAS["CAR"]           = $car;
            $FASILITAS["SERIAL"]        = $seri;
            $exec['fasilitas']          = $this->actMain->insertRefernce('t_bc20fas', $FASILITAS, true);

            $TARIF                  = $post['TARIF'];
            $TARIF["KODE_TRADER"]   = $this->kd_trader;
            $TARIF["CAR"]           = $car;
            $TARIF["SERIAL"]        = $seri;
            $TARIF["SERITRP"]       = $BARANG["SERITRP"];
            $TARIF["NOHS"]          = $BARANG["NOHS"];
            $exec['tarif']          = $this->actMain->insertRefernce('t_bc20trf', $TARIF, true);
            $exec['trader_tarif']   = $this->actMain->insertRefernce('m_trader_tarif', $TARIF, true);

            $sql = "call bc20hitungPungutan('" . $car . "','" . $this->kd_trader . "')";
            $this->db->query($sql);
            
            $sql = "call P_BC20UPDDTL('" . $car . "'," . $this->kd_trader . ")";
            $this->db->query($sql);
            
            
            list($msg,$status) = $this->validasiDetil($car,$seri);
            if ($exec['barang'] && $exec['trader_barang'] && $exec['fasilitas'] && $exec['tarif']) {
                return "MSG|OK|".site_url('bc20/daftarDetil/barang/'.$car).'/|<pre style="float: left;">'.$msg.'</pre>';
            } else {
                return "MSG|ERR||Proses data barang gagal.";
            }
        }
        else if ($type == "delete") {
            foreach ($this->input->post('tb_chkfbarang') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $car = $arrchk[0];
                $seri = $arrchk[1];
                $key = array('KODE_TRADER' => $this->kd_trader, 'CAR' => $car, 'SERIAL' => $seri);
                $tables = array('t_bc20dtl', 't_bc20fas', 't_bc20trf');
                $this->db->where($key);
                $this->db->delete($tables);
                //update urutkan serial
                $sql = "call bc20UrutkanSerial(" . $this->kd_trader . ",'" . $car . "'," . $seri . ")";
                $exec = $this->db->query($sql);
            }
            
            if ($exec) {
                return "MSG|OK|".site_url('bc20/daftarDetil/barang/'.$car).'/|Delete data barang berhasil.';
            } else {
                return "MSG|ERR||Delete data barang gagal.";
            }
            die();
        }
    }

    function validasi($car, $tbl) {
        $tipe_trader = $this->newsession->userdata('TIPE_TRADER');
        $hasil = '';
        $i = 0;
        $this->load->model("actMain", "actMain", true);
        //Validasi Header
        $query = "SELECT A.*, B.URAIAN as 'URSTATUS' "
                . " FROM t_bc20hdr A LEFT JOIN M_TABEL B ON B.MODUL = 'BC20' AND B.KDTAB = 'STATUS' AND B.KDREC = A.STATUS "
                . " WHERE A.CAR = '" . $car . "' AND A.KODE_TRADER = " . $this->kd_trader;
        $aH = $this->actMain->get_result($query);
        if (count($aH) > 0) {
            $query = "SELECT * FROM m_trader WHERE KODE_TRADER = " . $this->kd_trader;
            $strMy = $this->actMain->get_result($query);
            $query = "SELECT FLDNAME,MESSAGE,TIPE from m_validasi WHERE SCRNAME = '" . $tbl . "' AND MANDATORY =1 ";
            $arrdataV = $this->actMain->get_result($query, true);
            foreach ($arrdataV as $aV) {
                if (($aH[$aV['FLDNAME']] == '' && $aV['TIPE']=='') || ($aH[$aV['FLDNAME']] <= 0 && $aV['TIPE']=='NUMBER') || (($aH[$aV['FLDNAME']] == '' || $aH[$aV['FLDNAME']]=='00-00-0000') && $aV['TIPE']=='DATE') || (($aH[$aV['FLDNAME']] == '' || $aH[$aV['FLDNAME']]=='00:00:00') && $aV['TIPE']=='TIME') || (($aH[$aV['FLDNAME']] == '' || $aH[$aV['FLDNAME']]=='00-00-0000 00:00:00') && $aV['TIPE']=='DATETIME')) {
                    $hasil .= substr('   ' . ++$i, -3) . '. ' . ucwords(strtolower($aV['MESSAGE'])) . ' Harus diisi<br>';
                }
            }
            unset($arrdataV);
            //Validasi Khusus
            //Cek jika jumlah barang < 0
            if ((int) $aH['JMBRG'] < 0) {
                $hasil .= substr('   ' . ++$i, -3) . '. Jumlah Barang Tidak Boleh Negatif<br>';
                $aH['JMBRG'] = 0;
            }
            //jika kode API isi status harus diisi
            if ($aH['APIKD'] == '' || $aH['APINO'] == '') {
                $aH['APIKD'] = '';
                $aH['APINO'] = '';
            }
            if ($aH['APIKD'] != '' && $aH['IMPSTATUS'] == '') {
                $hasil .= substr('   ' . ++$i, -3) . '. Status Importir harus diisi<br>';
            }
            //jika impor sementara maka jangkawaktu harus diisi
            if ($aH['JNIMP'] == '2' && (int) $aH['JKWAKTU'] <= 0)
                $hasil .= substr('   ' . ++$i, -3) . '. Jangka waktu impor sementara harus diisi<br>';
            //jika impor sementara maka caya bayar "Dengan jaminan"
            if ($aH['JNIMP'] == '2' && $aH['CRBYR'] != '3')
                $hasil .= substr('   ' . ++$i, -3) . '. Impor Sementara, cara pembayaran : [3 - Dengan jaminan]<br>';
            //Cek Pengisian harga
            switch ($aH['KDHRG']) {
                case '1': //CIF
                    $aH['ASURANSI'] = 0;
                    $aH['FREIGHT'] = 0;
                    $aH['FOB'] = 0;
                    break;
                case '2': //CNF
                    $aH['FREIGHT'] = 0;
                    $aH['FOB'] = 0;
                    if ($aH['ASURANSI'] == 0 && $aH['KDASS']=='1') {
                        $hasil .= substr('   ' . ++$i, -3) . '. Asuransi Luar Negeri, harus diisi<br>';
                    }
                    break;
                case '3': //FOB
                    $aH['FOB'] = $aH['NILINV'] + $aH['BTAMBAHAN'] + $aH['DISCOUNT'];
                    if ($aH['FREIGHT'] == '0') {
                        $hasil .= substr('   ' . ++$i, -3) . '. Kode Harga FOB, Freight harus diisi<br>';
                    }
                    if ($aH['ASURANSI'] == '0' && $aH['KDASS'] == '1') {
                        $hasil .= substr('   ' . ++$i, -3) . '. Kode Harga FOB, dan bayar asuransi di LN, nilai asuransi harus diisi<br>';
                    }
                    break;
                default :
                    $hasil .= substr('   ' . ++$i, -3) . '. Kode harga salah<br>';
                    break;
            }
            //Unsur Harga
            if ($aH['NILINV'] < 0 || $aH['ASURANSI'] < 0 || $aH['FREIGHT'] < 0 || $aH['DISCOUNT'] < 0) {
                $hasil .= substr('   ' . ++$i, -3) . '. Unsur harga tidak boleh kurang dari 0<br>';
            }
            //Validasi Berat
            if ($aH['BRUTO'] < $aH['NETTO']) {
                $hasil .= substr('   ' . ++$i, -3) . '. Brutto tidak boleh lebih kecil dari netto<br>';
            }
            //cek pengisian dokumen
            $queryDok = "SELECT GROUP_CONCAT(DOKKD separator '|') as ADOK FROM t_bc20dok WHERE CAR = '" . $car . "' AND KODE_TRADER = " . $this->kd_trader;
            $strDok = $this->actMain->get_uraian($queryDok, 'ADOK');
            $dok['INVOICE'] = $this->actMain->findArr2Str($strDok, array('380'));
            $dok['BL'] = $this->actMain->findArr2Str($strDok, array('704', '705'));
            $dok['AWB'] = $this->actMain->findArr2Str($strDok, array('740', '741'));
            $dok['SKEP'] = $this->actMain->findArr2Str($strDok, array('814', '815', '851', '853', '911', '993', '998'));
            $dok['BPJ'] = $this->actMain->findArr2Str($strDok, array('994'));
            $dok['STTJ'] = $this->actMain->findArr2Str($strDok, array('997'));
            $dok['COO'] = $this->actMain->findArr2Str($strDok, array('861'));
            //Invoice
            if (!$dok['INVOICE']) {
                $hasil .= substr('   ' . ++$i, -3) . '. Dokumen Invoice belum diisi<br>';
            }
            //BL
            if (!$dok['BL'] && $aH['MODA'] == '1') {
                $hasil .= substr('   ' . ++$i, -3) . '. Dokumen BL tidak ada [Angkutan Laut]<br>';
            }
            //AWB
            if (!$dok['AWB'] && $aH['MODA'] == '4') {
                $hasil .= substr('   ' . ++$i, -3) . '. Dokumen AWB tidak ada [Angkutan Udara]<br>';
            }
            //COO
            if (!$dok['COO'] && strstr('|06|54|55|56|57|58|', $aH['KDFAS'])) {
                $hasil .= substr('   ' . ++$i, -3) . '. Ada fasilitas ' . $aH['KDFAS'] . ' tetapi belum mengisi Dokumen SKA/COO [kode 861]<br>';
            }
            //SKEP
            if (!$dok['SKEP'] && trim($aH['KDFAS']) != '' && !strstr('06|54|55|56|57|58', $aH['KDFAS'])) {
                $hasil .= substr('   ' . ++$i, -3) . '. Ada fasilitas ' . $aH['KDFAS'] . ' tetapi belum mengisi Dokumen Skep<br>               [kode 814, 815, 851, 853, 911, 993, 998]<br>';
            }
            //cek isian kemasan
            $queryKms = "SELECT COUNT(CAR) as jmlKMS FROM t_bc20kms WHERE CAR = '" . $car . "' AND KODE_TRADER = " . $this->kd_trader;
            $jmlKms = $this->actMain->get_uraian($queryKms, 'jmlKMS');
            if ($jmlKms <= 0) {
                $hasil .= substr('   ' . ++$i, -3) . '. Data kemasan masih kosong.<br>';
            }
            //Kode gudang harus sesuai dengan kantor KPBC yang dipilih
            if ($aH['TMPTBN'] != '') {
                $queryGdg = "SELECT COUNT(KDKPBC) as jmlKPBC FROM m_gudang WHERE KDKPBC ='" . $aH['KDKPBC'] . "' AND KDGDG ='" . $aH['TMPTBN'] . "'";
                $jmlGdg = $this->actMain->get_uraian($queryGdg, 'jmlKPBC');
                if ($jmlGdg <= 0) {
                    $hasil .= substr('   ' . ++$i, -3) . '. Kode gudang ' . $aH['TMPTBN'] . ' tidak ada di kantor ' . $aH['KDKPBC'];
                }
            }
            //Cek User
            if (!strpos("|020|030|040|080|082|084|086|087|088|090|100|102|105|110|112|114|116|120|150|", $aH['STATUS'])) {
                if ($tipe_trader == '1') { //Importir
                    $aH['IMPID'] = $strMy['KODE_ID'];
                    $aH['IMPNPWP'] = $strMy['ID'];
                    $aH['IMPNAMA'] = $strMy['NAMA'];
                    $aH['IMPALMT'] = $strMy['ALAMAT'];

                    $aH['PPJKID'] = '';
                    $aH['PPJKNPWP'] = '';
                    $aH['PPJKNAMA'] = '';
                    $aH['PPJKALMT'] = '';
                    $aH['PPJKNO'] = '';
                    $aH['PPJKTG'] = NULL;
                } elseif ($tipe_trader == '2') {
                    $aH['PPJKID'] = $strMy['KODE_ID'];
                    $aH['PPJKNPWP'] = $strMy['ID'];
                    $aH['PPJKNAMA'] = $strMy['NAMA'];
                    $aH['PPJKALMT'] = $strMy['ALAMAT'];
                    $aH['PPJKNO'] = $strMy['NO_PPJK'];
                    $aH['PPJKTG'] = $strMy['TANGGAL_PPJK'];
                } else {
                    $hasil .= substr('   ' . ++$i, -3) . '. Tipe user yang anda gunakan tidak sesuai dengan dokumen yang anda entry.<br>';
                }
            }
            //Validasi Detail
            $data = $this->validasiDetilBatch($car);
            if (strlen($data['SERIAL']) > 0) {
                $arrSerial = explode(',', $data['SERIAL']);
                foreach ($arrSerial as $seri) {
                    $hasil .= substr('   ' . ++$i, -3) . '. Pengisian Detil ke- ' . $seri . ' belum benar<br>';
                }
            }
            //Resume detil error
            if (count($arrSerial) > 0) {
                $hasil .= substr('   ' . ++$i, -3) . '. Ada ' . count($arrSerial) . ' data barang yang belum benar.<br>';
            }
            //jika di detil ada fasilitas di header juga ada.
            if ($data['FASILITAS'] > 0 && $aH['KDFAS'] == '') {
                $hasil .= substr('   ' . ++$i, -3) . '. Jenis fasilitas di Header belum diisi<br>';
            }
            //jika di header ada fasiltas di detil harus juga ada
            if ($data['FASILITAS'] == 0 && $aH['KDFAS'] != '') {
                $hasil .= substr('   ' . ++$i, -3) . '. Jenis fasilitas di Detil belum diisi<br>';
            }
            //harga di detil harus sama dengan di header
            if ($aH['NILINV'] != (int) $data['NILDTL']) {
                if (abs($aH['NILINV'] - $data['NILDTL']) > 1) {
                    $hasil .= substr('   ' . ++$i, -3) . '. Harga Header = ' . $aH['NILINV'] . ', Harga Detil = ' . (int) $data['NILDTL'] . '<br>';
                }
            }
            //status importir maksimal 3
            $sts = explode(',', $aH['IMPSTATUS']);
            if (count($sts) > 3) {
                $hasil .= substr('   ' . ++$i, -3) . '. Status importir tidak boleh lebih dari 3.<br>';
            }
            //apakah ada penangguhan 
            $datapgt = (int) $this->actMain->get_uraian("SELECT COUNT(CAR) AS JML FROM t_bc20pgt WHERE CAR = '" . $car . "' AND KDFASIL = '2'", 'JML');
            //update status [exist coy]
            if ($aH['STATUS'] == '')
                $aH['STATUS'] = '000';
            #tidak ada perubahan jika status diliar 010 dan 020
            if (strpos('|000|010|060|', $aH['STATUS']))
                $aH['STATUS'] = ($i == 0) ? '010' : '000';
            //apply ke database
            $upd = $aH;
            unset($upd['URSTATUS']);
			
			$query = "SELECT B.HS_CODE, GROUP_CONCAT(DISTINCT C.UR_IJIN  separator '\n') as 'Ijin', D.NMGA ,C.KD_MODUL, G.URAIAN 
						FROM m_lartas_impor as B 
						inner join t_bc20dtl as A on left(A.NOHS,10) = B.HS_CODE
						LEFT JOIN m_ijin C ON C.KD_IJIN=B.KD_IJIN
						LEFT JOIN m_ga D ON D.ID_GA=C.KD_GA
						INNER JOIN m_kdijin E on C.KD_IJIN=E.KD_IJIN
						INNER JOIN m_kdijin F ON D.ID_GA=F.ID_GA    
						LEFT JOIN M_TABEL G ON G.MODUL = 'BC20' AND G.KDTAB = 'KODE_DOKUMEN' AND G.KDREC = C.KD_MODUL                           
						WHERE CAR = '".$car."'
						GROUP BY b.HS_CODE";            
            $HS_Lartas  = $this->actMain->get_result($query, true);
            foreach($HS_Lartas as $hs_l){
                //print_r($hs_l);die();
				$no++;
				$hasil .= substr('   ' . $no, -3) . '. Detail barang dengan HS:'.$hs_l['HS_CODE'].' Terkena Lartas. '. $hs_l['Ijin'].'<br>  Pada '.$hs_l['NMGA']. '<br>';
				$hasil .= substr('   ' . '*' , -10) . '. Harus Melengkapi Dokumen '. $hs_l['KD_MODUL'] .' - '.$hs_l['URAIAN'] .' <br>';
			}
			unset($HS_Lartas);
						
            $this->db->where(array('CAR' => $car, 'KODE_TRADER' => $this->kd_trader));
            $exec = $this->db->update('t_bc20hdr', $upd);
            unset($upd);
            //tulis informasi
            if ($i == 0) {
                $hasil .= '<br>------------------ Data Lengkap ------------------<br>';
            } else {
                $hasil .= '<br>--------------- Data Tidak Lengkap ---------------<br>';
            }
            $infonya .= '<br>';
            if ($tipe_trader == '2' && ($aH['PPJKNO'] == '' || $aH['NILINV'] == 'PPJKTG')) {
                $hasil .= 'Perhatian : Nomor/Tanggal PPJK belum diisi.<br>';
            }
            if ($aH['APIKD'] == '' || $aH['APINO'] == '') {
                $hasil .= 'Perhatian : PIB tanpa nomor API atau APIT, PPH = 7,5.<br>';
            }
            if ($aH['KDFAS'] == '03' && !$dok['STTJ']) {
                $hasil .= 'Perhatian : PIB Fasilitas Kemudahan Ekspor (03), seharusnya direkam dokumen 997 - STTJ / Costoms Bond.<br>';
            }
            if ($datapgt > 0 && !$dok['BPJ'] && !$dok['STTJ'] && $aH['KDFAS'] != '03') {
                $hasil .= 'Perhatian : Ada pungutan yang ditangguhkan, seharusnya direkam dokumen 994 - Bukti Penerimaan Jaminan (BPJ).<br>';
            }
            if ($aH['JNIMP'] == '2' && !$dok['BPJ']) {
                $hasil .= 'Perhatian : Impor Sementara, seharusnya direkam dokumen 994 - Bukti Penerimaan Jaminan (BPJ).<br>';
            }
            if ($aH['JNIMP'] == '3' && $aH['JNIMP'] != '3') {
                $hasil .= 'Perhatian : Perhatian : PIB Reimpor, cara pembayaran seharusnya [3 - Dengan Jaminan].<br>';
            }
        }

        $judul = 'Hasil validasi PIB Nomor Aju : ' . substr($car, 20) . ' tanggal ' . substr($car, 12, 8);
        $hasil .= '<br><br>' . $this->informasi($aH);
        unset($aH);
        return array($hasil, $judul, $aH['URSTATUS']);
    }

    function informasi($arrH) {
        $this->load->model('actMain');
        $arti = explode('|', $this->actMain->get_uraian("SELECT URAIAN FROM M_TABEL WHERE MODUL = 'BC20' AND KDTAB = 'ARTI_STATUS' AND KDREC = '" . $arrH['STATUS'] . "'", 'URAIAN'));
        $hasil = 'Informasi Data PIB<br>';
        $hasil .= '--------------------------------------------------<br>';
        $hasil .= 'Nomor Aju              : ' . $arrH['CAR'] . '<br>';
        $hasil .= 'Status                 : ' . $arrH['URSTATUS'] . ' ( ' . $arti[0] . ' ) <br>';
        $hasil .= 'Keterangan Status      : ' . $arti[1] . ' <br>';
        $hasil .= 'Jumlah Barang          : ' . str_pad(number_format($arrH['JMBRG']), 20, ' ', STR_PAD_LEFT) . ' Barang<br>';
        $hasil .= 'Jumlah Kontainer       : ' . str_pad(number_format($arrH['JMCONT']), 20, ' ', STR_PAD_LEFT) . ' Kontainer<br>';
        $hasil .= 'Bruto                  : ' . str_pad(number_format($arrH['BRUTO'], 4), 20, ' ', STR_PAD_LEFT) . ' Kgm<br>';
        $hasil .= 'Netto                  : ' . str_pad(number_format($arrH['NETTO'], 4), 20, ' ', STR_PAD_LEFT) . ' Kgm<br>';
        $hasil .= 'CIF (' . $arrH['KDVAL'] . ')               : ' . str_pad(number_format($arrH['CIF'], 2), 20, ' ', STR_PAD_LEFT) . ' <br>';
        $hasil .= 'SNRF                   :' . $arrH['SNRF'] . ' <br>';
        $hasil .= 'Respons                :<br>';

        $SQL = "SELECT CONCAT(A.RESKD,' - ', B.URAIAN) AS RESPON "
                . "FROM T_BC20RES A LEFT JOIN M_TABEL B ON B.MODUL = 'BC20' AND B.KDTAB = 'JENIS_RESPON' AND B.KDREC = A.RESKD "
                . 'WHERE A.KODE_TRADER = ' . $this->kd_trader . " AND A.CAR = '" . $arrH['CAR'] . "'";
        $res = $this->actMain->get_result($SQL, true);
        if (isset($res)) {
            foreach ($res as $a) {
                $hasil .= '                       : ' . $a['RESPON'].'<br>';
            }
        }
        $hasil .= 'Pemberitahu            : ' . $arrH['NAMA_TTD'];
        unset($arrH);
        return $hasil;
    }

    function validasiDetilBatch($car) {
        $this->load->model('actMain');
        $sql = "SELECT * FROM m_validasi WHERE SCRNAME = 't_bc20dtl' AND MANDATORY =1 ";
        $aarVal = $this->actMain->get_result($sql, true);
        $sql = "SELECT  *
                   FROM    t_bc20dtl a Left Join t_bc20trf c ON a.KODE_TRADER = c.KODE_TRADER AND a.CAR = c.CAR AND a.SERIAL = c.SERIAL
                                       Left Join t_bc20fas b ON a.KODE_TRADER = b.KODE_TRADER AND a.CAR = b.CAR AND a.SERIAL = b.SERIAL
                   WHERE   a.CAR = '" . $car . "' AND a.KODE_TRADER ='" . $this->kd_trader . "'"; //die($sql);
        $arrDtl = $this->actMain->get_result($sql, true); //print_r($arrDtl);die();
        $ada['FASILITAS'] = 0;
        $this->db->query("UPDATE t_bc20dtl SET DTLOK = '1' WHERE CAR = '" . $car . "' AND KODE_TRADER ='" . $this->kd_trader . "'"); //khusnuzon di buat lengkap semua
        foreach ($arrDtl as $aD) {
            $hasil['NILDTL'] += $aD['DNILINV'];
            $i = 0;
            //via table m_validasi
            foreach ($aarVal as $aV) {
                if ($aD[$aV['FLDNAME']] == '') {
                    $i++;
                    //$test .= $aV['FLDNAME'];
                }
                if ($aD[$aV['FLDNAME']] == '0' && $aV['FLDNAME'] == 'NUMBER') {
                    $i++;
                    //$test .= $aV['FLDNAME'];
                }
            }
            unset($aarVal);
            //validasi kusus
            //Nilai Barang negatif
            if ($aD['DNILINV'] < 0) {
                $i++;
                //$test .= '1';
            }
            if ($aD['KDFASBM'] != '' && $aD['KDFASDTL'] == '') {
                $i++;
                //$test .= '2-'.$aD['SERIAL'];//print_r($aD);die();
            }
            if ($aD['KDFASDTL'] != '') { //untuk pengecekan di header.
                $ada['FASILITAS'] ++;
                //$test .= '3';
            }
            //Jika nilai detil ada yang 0 (NVC) gak ngerti maksute
            //Cek Panjang HS
            if (strlen($aD['NOHS']) < 9) {
                $i++;
                //$test .= '4';
            }
            //Cek jika kode jenis Tarif kosong
            if ($aD['KDTRPBM'] == '') {
                $i++;
                //$test .= '5';
            }
            if ($aD['KDTRPBM'] == '2') {
                if ((int) $aD['TRPBM'] == 0 || $aD['KDSATBM'] == '' || (int) $aD['SATBMJM'] == 0) {
                    $i++;
                    //$test .= '6';
                }
            }
            if ($i > 0) {
                $hasil['SERIAL'].=',' . $aD['SERIAL'];
            }
        }
        unset($arrDtl);
        $hasil['SERIAL'] = substr($hasil['SERIAL'], 1);
        $hasil['FASILITAS'] = $ada['FASILITAS'];
        //print_r($hasil);die($test);
        if ($hasil['SERIAL'] != '') {
            $sql = "UPDATE t_bc20dtl SET DTLOK = '0' WHERE CAR = '" . $car . "' AND KODE_TRADER =" . $this->kd_trader . " AND SERIAL IN ('" . str_replace(',', "','", $hasil['SERIAL']) . "') "; //die($sql);
            $this->db->query($sql);
        }
        return $hasil;
    }

    function validasiDetil($car, $seri) {
        $this->load->model('actMain');
        $sql = "SELECT * FROM m_validasi WHERE SCRNAME = 'T_BC20DTL' AND MANDATORY = 1";
        $aarVal = $this->actMain->get_result($sql, true); //
        $sql = "SELECT  * 
                FROM    t_bc20dtl a Left Join t_bc20fas b ON a.KODE_TRADER = b.KODE_TRADER AND a.CAR = b.CAR AND a.SERIAL = b.SERIAL
                                    Left Join t_bc20trf c ON a.KODE_TRADER = c.KODE_TRADER AND a.CAR = c.CAR AND a.SERIAL = c.SERIAL
                WHERE   a.CAR = '" . $car . "' AND a.KODE_TRADER ='" . $this->kd_trader . "' AND a.SERIAL = '" . $seri . "'"; //die($sql);
        $aD = $this->actMain->get_result($sql); //print_r($aD);die();
        $i = 0;
        $hasil = 'Validasi error';
        $oke = 'ER';
        //via table m_validasi
        if (count($aarVal) > 0) {
            $hasil = '';
            $DATA['CAR'] = $car;
            $DATA['SERIAL'] = $seri;
            $DATA['KODE_TRADER'] = $this->kd_trader;
            foreach ($aarVal as $aV) {
                //print_r($aV);
                if (trim($aD[$aV['FLDNAME']]) == '') {
                    $hasil .= substr('   ' . ++$i, -3) . '. ' . $aV['MESSAGE'] . ' Harus diisi.<br>';
                }
                if (trim($aD[$aV['FLDNAME']]) == 0 && $aV['FLDNAME'] == 'NUMBER') {
                    $hasil .= substr('   ' . ++$i, -3) . '. ' . $aV['MESSAGE'] . ' Harus diisi.<br>';
                }
            }
            unset($aarVal);
            //print_r($hasil);die();
            //validasi kusus
            //Nilai Barang negatif
            if ($aD['DNILINV'] < 0) {
                $hasil .= substr('   ' . ++$i, -3) . '. ' . $aV['MESSAGE'] . ' Harus diisi.<br>';
            }
            if ($aD['KDFASBM'] != '0' && $aD['KDFASDTL'] == '') {
                $hasil .= substr('   ' . ++$i, -3) . '. Fasilitas BM terisi. Jenis fasilitas harus diisi.<br>';
            }
            //Jika nilai detil ada yang 0 (NVC) gak ngerti maksute
            //Cek Panjang HS
            if (strlen($aD['NOHS']) < 9) {
                $hasil .= substr('   ' . ++$i, -3) . '. Pengisian HS salah.<br>';
            }
            //Cek jika kode jenis Tarif kosong
            if ($aD['KDTRPBM'] == '') {
                $hasil .= substr('   ' . ++$i, -3) . '. Kode jenis tarif BM (Advalorum/Spesifik) harus diisi.<br>';
            }
            if ($aD['KDTRPBM'] == '2') {
                if ((int) $aD['TRPBM'] == 0 || $aD['KDSATBM'] == '' || (int) $aD['SATBMJM'] == 0) {
                    $hasil .= substr('   ' . ++$i, -3) . '. Tarif Spesifik, Besar tarif dan kode satuan tarif harus diisi.<br>';
                }
            }
            if ($i > 0) {
                $hasil.="<br>************  Data Detil ke-$seri BELUM lengkap **********<br>";
                $oke = 'ER';
                $DATA['DTLOK'] = '0';
            } else {
                $hasil.="<br>************  Data Detil ke-$seri SUDAH lengkap **********<br>";
                $oke = 'OK';
                $DATA['DTLOK'] = '1';
            }
            $this->actMain->insertRefernce('t_bc20dtl', $DATA);
            if ((int) $aD['DNILINV'] == 0) {
                $hasil.="*Perhatian : Nilai invoice barang ini = 0<br>";
            }
            $this->actMain->insertRefernce('t_bc20dtl', $DATA);
        }
        return array($hasil, $oke);
    }

    function cetakDokumen($car, $jnPage, $arr ) {
        //die($car.'|snv'.$jnPage);
        $this->load->library('fpdf');
        $this->load->model('actMain');
        $this->load->model('dokBC20');
        $this->dokBC20->ciMain($this->actMain);
        $this->dokBC20->fpdf($this->fpdf);
        $this->dokBC20->segmentUri($arr);		
        //$this->dokBC20->data($data);
        $this->dokBC20->showPage($car, $jnPage);
    }

    function getSSPCP($car) {
        $this->load->model('actMain');
        $query = "SELECT a.*,URAIAN_KPBC as 'URKPBC'
                  FROM   t_bc20ntb a Left Join m_kpbc b on a.KDKPBC = b.KDKPBC
                  WHERE  CAR = '" . $car . "' AND KODE_TRADER = " . $this->kd_trader;
        $aNtb = $this->actMain->get_result($query);
        if (count($aNtb) > 0) {
            $query = "SELECT AKUN_SSPCP,NILAI
                      FROM   t_bc20ntbakun
                      WHERE CAR = '" . $car . "' AND 
                            KODE_TRADER = " . $this->kd_trader;
            $arrakun = $this->actMain->get_result($query, true);
            foreach ($arrakun as $a) {
                $hasil[$a['AKUN_SSPCP']] = $a['NILAI'];
            }
            $hasil = $hasil + $aNtb;
            return $hasil;
        } else {
            $arrSet = $this->actMain->get_Setting('BC20', 'PNBP');
            $hasil['423216'] = $arrSet['PNBP'];
            $query = "SELECT a.KDKPBC ,URAIAN_KPBC as 'URKPBC', NPWP, IMPNAMA as 'NAMA',IMPNPWP as 'IMPNPWP'
                      FROM   t_bc20hdr a Left Join m_kpbc b on a.KDKPBC = b.KDKPBC
                      WHERE CAR = '" . $car . "' AND 
                            KODE_TRADER = " . $this->kd_trader;
            $arrHdr = $this->actMain->get_result($query);
            $query = "SELECT KDBEBAN,KDFASIL,NILBEBAN
                      FROM   t_bc20pgt
                      WHERE CAR = '" . $car . "' AND 
                            KDFASIL = '0' AND 
                            KODE_TRADER = " . $this->kd_trader;
            $arrPgt = $this->actMain->get_result($query, true);
            $pgt = array();
            foreach ($arrPgt as $arr) {
                $pgt[$arr['KDBEBAN']][$arr['KDFASIL']] = $arr['NILBEBAN'];
            }
            $hasil['412111'] = $pgt['1']['0']; //bm
            $hasil['411212'] = $pgt['2']['0']; //ppn
            $hasil['411222'] = $pgt['3']['0']; //pbm
            $hasil['411123'] = $pgt['4']['0']; //pph
            $hasil['411511'] = $pgt['5']['0']; //cukai Tembakau
            $hasil['411513'] = $pgt['6']['0']; //cukai MMEA
            $hasil['411512'] = $pgt['7']['0']; //cukai EA
            $hasil = $hasil + $arrHdr;
            return $hasil;
        }
    }

    function setSSPCP() {
        $this->load->model('actMain');
        $SSPCP = $this->input->post('SSPCP');
        $SSPCP['KODE_TRADER'] = $this->kd_trader;
        $this->actMain->insertRefernce('t_bc20ntb', $SSPCP);
        //insert batch
        //detelete
        $this->db->delete('t_bc20ntbakun', array('KODE_TRADER' => $SSPCP['KODE_TRADER'], 'CAR' => $SSPCP['CAR']));
        $SSPCPAKUN = $this->input->post('SSPCPAKUN');
        foreach ($SSPCPAKUN as $key => $val) {
            if ((int) $val > 0) {
                $dataAkun[] = array('KODE_TRADER' => $SSPCP['KODE_TRADER'], 'CAR' => $SSPCP['CAR'], 'AKUN_SSPCP' => $key, 'NILAI' => $val);
            }
        }
        $this->db->insert_batch('t_bc20ntbakun', $dataAkun);
        return true;
    }

    function accessWSC($rtn) {
        $this->load->library("nuSoap_lib");
        $nuSoap_lib = new nusoap_client('http://localhost/wsclartas/server.php');
        if ($nuSoap_lib->fault) {
            $text = 'Error: ' . $nuSoap_lib->fault;
        } else {
            if ($nuSoap_lib->getError()) {
                $text = 'Error: ' . $nuSoap_lib->getError();
            } else {
                $row = $nuSoap_lib->call('testWSC', array($rtn));
                return $row;
            }
        }
    }

    function genFlatfile($car,$srpAwal='') {
        function fixLen($str, $len, $chr = ' ', $alg = STR_PAD_RIGHT) {
            $hasil = str_pad(substr($str, 0, $len), $len, $chr, $alg);
            return $hasil;
        }
        $this->load->model('actMain');
        $EDINUMBER = $this->newsession->userdata('EDINUMBER');
        $SQL = "SELECT a.*,"
                . "       DATE_FORMAT(a.PPJKTG,'%Y%m%d') as PPJKTG, "
                . "       DATE_FORMAT(a.TGTIBA,'%Y%m%d') as TGTIBA, "
                . "       DATE_FORMAT(a.DOKTUPTG,'%Y%m%d') as DOKTUPTG, "
                . "       b.SRP "
                . "FROM T_BC20HDR a INNER JOIN M_TRADER_BC20 b ON a.KODE_TRADER = b.KODE_TRADER "
                . "WHERE a.CAR = '" . $car . "' AND a.KODE_TRADER = " . $this->kd_trader;
        $hdr = $this->actMain->get_result($SQL);
        $hdr = str_replace("\n", '', str_replace("\r", '', $hdr));
        if($this->newsession->userdata('TIPE_TRADER')=='1'){
            $SRP = $hdr['SRP'];
        }else{
            $SRP = $srpAwal;
        }
        if ($hdr['STATUS'] != '010') {
            if ($hdr['STATUS'] == '020') {
                $whHDR['KODE_TRADER'] = $this->kd_trader;
                $whHDR['CAR'] = $car;
                $this->db->where($whHDR);
                $upHDR['SNRF'] = '';
                $upHDR['DIRFLT'] = '';
                $upHDR['DIREDI'] = '';
                $upHDR['STATUS'] = '010';
                $this->db->update('t_bc20hdr', $upHDR);
                unset($upHDR);
                #delete dokoutbound
                $whHDR['KDKPBC'] = $hdr['KDKPBC'];
                $whHDR['JNSDOK'] = 'BC20';
                $this->db->delete('m_trader_dokoutbound', $whHDR);
                unset($whHDR);
                unlink($hdr['DIRFLT']);
                unlink($hdr['DIREDI']);
                $hsl = '1|';
            }
            else {
                $hsl = '0|';
            }
            unset($hdr);
            return $hsl;
        }
        $KDKPBC = $hdr['KDKPBC'];
        $SQL = "SELECT NOMOREDI FROM M_TRADER_PARTNER WHERE KDKPBC = '" . $hdr['KDKPBC'] . "' AND KODE_TRADER = " . $this->kd_trader;
        $partner = $this->actMain->get_uraian($SQL, 'NOMOREDI');
        if ($partner == '') {
            return 'Edinumber Kantor Pelayanan belum terekam.';
        }

        #SEPARATOR UNTUK HEADER
        $FF.= "ENV00101" . fixLen($EDINUMBER, 20) . fixLen($partner, 20) . "DOKPIB09  " . "2\n";
        $FF.= "HDEC0101" . $hdr['CAR'] . "9\n";

        #RECORD TYPE HDEC0201 LOC
        ($hdr['PELMUAT'] != '') ? $FF.= "HDEC02019  " . fixLen($hdr['PELMUAT'], 6) . "\n" : '';
        ($hdr['PELBKR'] != '') ? $FF.= "HDEC020111 " . fixLen($hdr['PELBKR'], 6) . "\n" : '';
        ($hdr['TMPTBN'] != '') ? $FF.= "HDEC020114 " . fixLen($hdr['TMPTBN'], 6) . "\n" : '';
        ($hdr['KDKPBC'] != '') ? $FF.= "HDEC020141 " . fixLen($hdr['KDKPBC'], 6) . "\n" : '';
        ($hdr['PELTRANSIT'] != '') ? $FF.= "HDEC0201125" . fixLen($hdr['PELTRANSIT'], 6) . "\n" : '';

        #RECORD TYPE HDEC0301 DTM
        ($hdr['PPJKTG'] != '' || $hdr['PPJKTG'] != '00-00-0000') ? $FF.= "HDEC0301125" . fixLen($hdr['PPJKTG'], 8) . "102\n" : '';
        ($hdr['TGTIBA'] != '' || $hdr['TGTIBA'] != '00-00-0000') ? $FF.= "HDEC0301132" . fixLen($hdr['TGTIBA'], 8) . "102\n" : '';
        $FF.= "HDEC0301160" . date("Ymd") . "102\n";

        #RECORD TYPE HDEC0401 GIS
        ($hdr['JNPIB'] != '') ? $FF.= "HDEC040197 " . fixLen($hdr['JNPIB'], 1) . "\n" : '';
        ($hdr['JNIMP'] != '') ? $FF.= "HDEC040198 " . fixLen($hdr['JNIMP'], 1) . "\n" : '';
        ($hdr['CRBYR'] != '') ? $FF.= "HDEC040199 " . fixLen($hdr['CRBYR'], 1) . "\n" : '';

        #RECORD TYPE HDEC0501  (FII)
        #RECORD TYPE HDEC0601  (MEA)
        ($hdr['BRUTO'] >= 0) ? $FF.= "HDEC0601WT AADKGM" . fixLen(($hdr['BRUTO'] * 10000), 18, "0", STR_PAD_LEFT) . "\n" : '';
        ($hdr['NETTO'] >= 0) ? $FF.= "HDEC0601WT AACKGM" . fixLen(($hdr['NETTO'] * 10000), 18, "0", STR_PAD_LEFT) . "\n" : '';

        #RECORD TYPE HDEC0701  (MEA)
        if ($hdr['JMCONT'] > 0) {
            $SQL = "SELECT * FROM T_BC20CON WHERE CAR = '" . $car . "' AND KODE_TRADER = " . $this->kd_trader;
            $con = $this->actMain->get_result($SQL, true);
            foreach ($con as $a) {
                //"HDEC0701" + "CN " + GetFieldReal(rsCon, "contno") + rsCon!ContUkur
                //dari modul hanya mengambil nomor dan ukuran kontainer
                $FF.= "HDEC0701CN " . fixLen($a['CONTNO'], 17) . fixLen($a['CONTUKUR'], 2) . "\n";
            }
            unset($con);
        } else {
            $FF.= "HDEC0701\n";
        }
        #RECORD TYPE HDEC0801
        if ($hdr['DOKTUPNO'] != '') {
            switch ($hdr['DOKTUPKD']) {
                case'1':
                    $FF.= 'HDEC0801AFB' . fixLen($hdr['DOKTUPNO'], 6) . fixLen($hdr['POSNO'], 4) . fixLen($hdr['POSSUB'], 4, "0", STR_PAD_LEFT) . fixLen($hdr['POSSUBSUB'], 4, "0", STR_PAD_LEFT) . '128' . $hdr['DOKTUPTG'] . "102\n";
                    break;
                case'2':
                    $FF.= 'HDEC0801RKN' . fixLen($hdr['DOKTUPNO'], 6) . fixLen($hdr['POSNO'], 4) . fixLen($hdr['POSSUB'], 4, "0", STR_PAD_LEFT) . fixLen($hdr['POSSUBSUB'], 4, '0', STR_PAD_LEFT) . '128' . $hdr['DOKTUPTG'] . "102\n";
                    break;
                case'3':
                    $FF.= 'HDEC0801MDN' . fixLen($hdr['DOKTUPNO'], 6) . '            ' . '128' . $hdr['DOKTUPTG'] . "102\n";
                    break;
                case'4':
                    $FF.= 'HDEC0801MDE' . fixLen($hdr['DOKTUPNO'], 6) . '            ' . '128' . $hdr['DOKTUPTG'] . "102\n";
                    break;
                case'5':
                    $FF.= 'HDEC0801BST' . fixLen($hdr['DOKTUPNO'], 6) . '            ' . '128' . $hdr['DOKTUPTG'] . "102\n";
                    break;
            }
        }
        #KEMASAN
        $SQL = "SELECT * FROM T_BC20KMS WHERE CAR = '" . $car . "' AND KODE_TRADER = " . $this->kd_trader;
        $kms = $this->actMain->get_result($SQL, true);
        if (isset($kms)) {
            foreach ($kms as $a) {
                $FF.= 'HDEC0801ACH-                               ' . fixLen($a['JNKEMAS'], 2) . fixLen($a['JMKEMAS'], 8, "0", STR_PAD_LEFT) . fixLen($a['MERKKEMAS'], 30) . "\n";
            }
        }
        unset($kms);

        #versi Modul PIB
        $FF.= "HDEC0801AOZ0505\n";

        #RECORD TYPE HDEC0901
        ($hdr['MODA']) ? $FF.= 'HDEC090120 ' . fixLen($hdr['ANGKUTNO'], 7) . fixLen($hdr['MODA'], 1) . fixLen($hdr['ANGKUTNAMA'], 17) . fixLen($hdr['ANGKUTFL'], 2) . "\n" : '';

        #RECORD TYPE HDEC1001/kemasan
        $SQL = "SELECT *,DATE_FORMAT(DOKTG,'%Y%m%d') as DOKTG FROM T_BC20DOK WHERE CAR = '" . $car . "' AND KODE_TRADER = " . $this->kd_trader;
        $dok = $this->actMain->get_result($SQL, true);
        if (isset($dok)) {
            foreach ($dok as $a) {//"814", "815", "851", "853", "911", "993", "998", "861"
                if (strpos('|814|815|851|853|911|993|998|861|', $a['DOKKD']) > 0) {
                    $FF.= 'HDEC1001' . fixLen($a['DOKKD'], 3) . fixLen($a['DOKNO'], 30) . fixLen($hdr['KDFAS'], 2) . '182' . fixLen($a['DOKTG'], 8) . "102\n";
                } else {
                    $FF.= 'HDEC1001' . fixLen($a['DOKKD'], 3) . fixLen($a['DOKNO'], 30) . '  ' . '182' . fixLen($a['DOKTG'], 8) . "102\n";
                }
            }
        }
        unset($dok);
        #RECORD TYPE HDEC1101
        # IMPORTIR
        $FF.= 'HDEC1101IM ' . fixLen($hdr['IMPID'], 1) . fixLen($hdr['IMPNPWP'], 15) . fixLen($hdr['IMPNAMA'], 50) . fixLen($hdr['IMPALMT'], 70) . fixLen(str_replace(',', '', $hdr['IMPSTATUS']), 3) . '  IP ' . fixLen($hdr['APIKD'], 1) . fixLen($hdr['APINO'], 15) . "\n";

        # PEMBERITAHU
        (isset($hdr['PPJKNPWP'])) ? $FF.= 'HDEC1101CB ' . fixLen($hdr['PPJKID'], 1) . fixLen($hdr['PPJKNPWP'], 15) . fixLen($hdr['PPJKNAMA'], 50) . fixLen($hdr['PPJKALMT'], 70) . '     IP ' . fixLen($hdr['PPJKNO'], 16) . "\n" : '';

        # PEMASOK
        $FF.= 'HDEC1101SU                 ' . fixLen($hdr['PASOKNAMA'], 50) . fixLen($hdr['PASOKALAMAT'], 70) . '   ' . fixLen($hdr['PASOKNEG'], 2) . '                   ' . "\n";

        # INDENTOR
        $FF.= 'HDEC1101PC ' . fixLen($hdr['INDID'], 1) . fixLen($hdr['INDNPWP'], 15) . fixLen($hdr['INDNAMA'], 50) . fixLen($hdr['IMPALMT'], 70) . '                        ' . "\n";

        # SRP (Sertifikat Registrasi Pabean)
        if (trim($SRP)!='') {
            $FF.= 'HDEC1101BM ' . fixLen($SRP, 16) . '                                                                                                                  ' . "\n";
        }
        
        # PenandaTangan
        $FF.= 'HDEC1101AM                 ' . fixLen($hdr['NAMA_TTD'], 50) . fixLen($hdr['KOTA_TTD'], 70) . '                        ' . "\n";

        #RECORD TYPE HDEC1201
        # TOTAL (CIF) 18v2
        $FF.= 'HDEC120143 ' . fixLen($hdr['CIF'] * 10000, 18, "0", STR_PAD_LEFT) . '   ' . "\n";

        # FOB 18v2
        ((float) $hdr['FOB'] > 0) ? $FF.= 'HDEC120163 ' . fixLen($hdr['FOB'] * 10000, 18, "0", STR_PAD_LEFT) . '   ' . "\n" : '';

        # Freight 18v2
        ((float) $hdr['FREIGHT'] > 0) ? $FF.= 'HDEC120164 ' . fixLen($hdr['FREIGHT'] * 10000, 18, "0", STR_PAD_LEFT) . '   ' . "\n" : '';

        # Asuransi 18v2
        ((float) $hdr['ASURANSI'] > 0) ? $FF.= 'HDEC120170 ' . fixLen($hdr['ASURANSI'] * 10000, 18, "0", STR_PAD_LEFT) . '   ' . "\n" : '';

        # NDPBM 9v4
        $FF.= 'HDEC1201702' . fixLen($hdr['NDPBM'] * 10000, 18, "0", STR_PAD_LEFT) . fixLen($hdr['KDVAL'], 3) . "\n";
        $JMBRG = $hdr['JMBRG'];
        $JMCONT = $hdr['JMCONT'];
        unset($hdr);

        #DATA DETIL
        $SQL = "SELECT A.SERIAL,A.NOHS,A.KDFASDTL,A.BRGURAI,A.MERK,A.TIPE,A.SPFLAIN,A.BRGASAL,A.KDSAT,A.JMLSAT,A.SATBMJM,A.SATCUKJM,A.NETTODTL,A.KEMASJM,A.KEMASJN,A.DCIF, "
                . "       B.KDSATBM,B.KDSATCUK,B.KDCUK,B.KDTRPBM,B.KDTRPCUK,B.TRPCUK,B.TRPPPN,B.TRPPBM,B.TRPBM,B.TRPPPH, "
                . "       C.KDFASBM,C.KDFASCUK,C.KDFASPPN,C.KDFASPPH,C.KDFASPBM,C.FASBM,C.FASCUK,C.FASPPN,C.FASPPH,C.FASPBM "
                . "FROM T_BC20DTL A LEFT JOIN T_BC20TRF B ON B.KODE_TRADER = A.KODE_TRADER AND B.CAR = A.CAR AND B.SERIAL = A.SERIAL "
                . "                 LEFT JOIN T_BC20FAS C ON C.KODE_TRADER = A.KODE_TRADER AND C.CAR = A.CAR AND C.SERIAL = A.SERIAL "
                . "WHERE A.CAR = '" . $car . "' AND A.KODE_TRADER = " . $this->kd_trader
                . " ORDER BY A.SERIAL ASC";
        $dtl = $this->actMain->get_result($SQL, true);
        $dtl = str_replace("\n", '', str_replace("\r", '', $dtl));
        if (count($dtl) === 0) {
            return 'Detil Barang PIB Nomor : ' . $car . ' Kosong.';
        }
        $FF.= 'UNS00101D' . "\n";
        unset($a);
        foreach ($dtl as $a) {
            # RECORD TYPE DDEC0101
            $FF.= 'DDEC0101' . fixLen($a['SERIAL'], 5, "0", STR_PAD_LEFT) . fixLen($a['NOHS'], 12) . fixLen($a['KDFASDTL'], 2) . "\n";

            #RECORD TYPE DDEC0201
            $FF.= 'DDEC0201AAA' . fixLen($a['BRGURAI'], 95) . fixLen($a['MERK'], 15) . fixLen($a['TIPE'], 15) . fixLen($a['SPFLAIN'], 15) . '27 ' . fixLen($a['BRGASAL'], 2) . "\n";

            #RECORD TYPE DDEC0301
            $FF.= 'DDEC0301DX ' . fixLen($a['KDSAT'], 3) . fixLen($a['JMLSAT'] * 10000, 18, "0", STR_PAD_LEFT) . "\n";

            ($a['SATBMJM'] > 0) ? $FF.= 'DDEC0301JSB' . fixLen($a['KDSATBM'], 3) . fixLen($a['SATBMJM'] * 10000, 18, '0', STR_PAD_LEFT) . "\n" : '';

            ($a['SATBMJM'] > 0) ? $FF.= 'DDEC0301JSC' . fixLen($a['KDSATBCUK'], 3) . fixLen($a['SATCUKJM'] * 10000, 18, '0', STR_PAD_LEFT) . "\n" : '';

            ($a['NETTODTL'] > 0) ? $FF.= 'DDEC0301AAIKGM' . fixLen($a['NETTODTL'] * 10000, 18, '0', STR_PAD_LEFT) . "\n" : '';

            //RECORD TYPE DDEC0401
            $FF.= 'DDEC0401' . fixLen($a['KEMASJM'], 8, '0', STR_PAD_LEFT) . fixLen($a['KEMASJN'], 2) . ' 66' . fixLen($a['DCIF'] * 100, 16, '0', STR_PAD_LEFT) . "\n";

            //RECORD TYPE DDEC0501
            ($a['TRPBM'] > 0) ? $FF.= 'DDEC05011  CUD' . fixLen($a['TRPBM'] * 100, 9, '0', STR_PAD_LEFT) . (($a['KDFASBM'] == '') ? '0' : $a['KDFASBM']) . fixLen($a['FASBM'] * 100, 5, '0', STR_PAD_LEFT) . (($a['KDTRPBM'] == '1') ? 'SP ' : ($a['KDTRPBM'] == '2') ? 'SA ' : '   ') . "\n" : '';

            ($a['TRPPPN'] > 0) ? $FF.= 'DDEC05011  VAT' . fixLen($a['TRPPPN'] * 100, 9, '0', STR_PAD_LEFT) . (($a['KDFASPPN'] == '') ? '0' : $a['KDFASPPN']) . fixLen($a['FASPPN'] * 100, 5, '0', STR_PAD_LEFT) . 'SP ' . "\n" : '';

            ($a['TRPPBM'] > 0) ? $FF.= 'DDEC05011  LXT' . fixLen($a['TRPPBM'] * 100, 9, '0', STR_PAD_LEFT) . (($a['KDFASPBM'] == '') ? '0' : $a['KDFASPBM']) . fixLen($a['FASPBM'] * 100, 5, '0', STR_PAD_LEFT) . 'SP ' . "\n" : '';

            $FF.= 'DDEC05011  ICT' . fixLen($a['TRPPPH'] * 100, 9, '0', STR_PAD_LEFT) . (($a['KDFASPPH'] == '') ? '0' : $a['KDFASPPH']) . fixLen($a['FASPPH'] * 100, 5, '0', STR_PAD_LEFT) . 'SP ' . "\n";

            switch ($a['KDCUK']) {
                case'1':
                    $jnCukai = 'XHT';
                    break;
                case'2':
                    $jnCukai = 'XME';
                    break;
                case'3':
                    $jnCukai = 'XET';
                    break;
            }

            ($a['TRPCUK'] > 0) ? $FF.= 'DDEC05011  ' . $jnCukai . fixLen($a['TRPCUK'] * 100, 9, '0', STR_PAD_LEFT) . (($a['KDFASCUK'] == '') ? '0' : $a['KDFASCUK']) . fixLen($a['FASCUK'] * 100, 5, '0', STR_PAD_LEFT) . (($a['KDTRPCUK'] == '1') ? 'SP ' : ($a['KDTRPCUK'] == '2') ? 'SA ' : '   ') . "\n" : '';
        }
        unset($dtl);
        unset($a);
        #SEPARATOR UNTUK SUMMARY
        $FF.= 'UNS00201S' . "\n";

        $FF.= 'SDEC01016  ' . fixLen($JMBRG, 8, '0', STR_PAD_LEFT) . "\n";
        if ($JMCONT > 0) {
            $FF.= 'SDEC010116 ' . fixLen($JMCONT, 8, '0', STR_PAD_LEFT) . "\n";
        }

        $SQL = "SELECT KDBEBAN, KDFASIL, NILBEBAN "
                . "FROM T_BC20PGT  "
                . "WHERE CAR = '" . $car . "' AND KODE_TRADER = " . $this->kd_trader;
        $pgt = $this->actMain->get_result($SQL, true);
        $bbn[1] = 'CUD';
        $bbn[2] = 'VAT';
        $bbn[3] = 'LXT';
        $bbn[4] = 'ICT';
        $bbn[5] = 'XHT';
        $bbn[6] = 'XME';
        $bbn[7] = 'XET';
        foreach ($pgt as $a) {
            $FF.= 'SDEC02012  ' . fixLen($bbn[$a['KDBEBAN']], 3) . fixLen($a['KDFASIL'], 1) . '24 ' . fixLen($a['NILBEBAN'], 15, '0', STR_PAD_LEFT) . "\n";
        }
        $FF.= 'UNZ00101';
        unset($a);
        unset($bbn);
        
        $fullPaht = 'FLAT/' . $EDINUMBER;
        if (!is_dir($fullPaht)){mkdir($fullPaht,0777,true);}
        $fullPaht = $fullPaht . '/' . $car . '.FLT';
        if (file_exists($fullPaht)){unlink($fullPaht);}
        $handle = fopen($fullPaht, 'w');
        fwrite($handle, $FF);
        fclose($handle);
        if (file_exists($fullPaht)) {$hsl = '2|' . $KDKPBC;}
        else {$hsl = '0|';}
        return $hsl;
    }
    
    function crtQueue($car){
        $this->load->model('actTranslator');
        $SNRF       = date('ymdHis') . rand(10, 99);
        $EDINUMBER  = $this->newsession->userdata('EDINUMBER');
        $EDIPaht    = '/home/dev/t2g/EDI/' . $EDINUMBER;
        if (!is_dir($EDIPaht)){
            mkdir($EDIPaht,0777,true);
        }else{
            chmod($EDIPaht,0777);
        }
        $FLTPaht    = 'FLAT/' . $EDINUMBER;
        if (!is_dir($FLTPaht)){
            mkdir($FLTPaht,0777,true);
        }else{
            chmod($FLTPaht,0777);
        }
        $this->actTranslator->getEDIFILE($EDINUMBER,'DOKPIB09',$SNRF,$car.'.FLT');
        if (file_exists($EDIPaht.'/'. $car .'.EDI') && file_exists($FLTPaht.'/'. $car .'.FLT')){
            $dirEdiNumber = 'TRANSACTION/' . $EDINUMBER . '/' . date('Ymd');
            if (!is_dir($dirEdiNumber)){mkdir($dirEdiNumber,0777,true);}
            rename($FLTPaht.'/'. $car .'.FLT', $dirEdiNumber . '/' . $car . '.FLT');
            rename($EDIPaht.'/'. $car .'.EDI', $dirEdiNumber . '/' . $car . '.EDI');
            $rtn = $SNRF . '|' . $dirEdiNumber . '/' . $car . '.EDI' . '|' . $dirEdiNumber . '/' . $car . '.FLT';
        }
        else {
            $rtn = 'ERROR';
        }
        return $rtn;
    }
    
    function updateSNRF($car, $data) {
        #update t_bc20hdr
        $this->load->model('actMain');
        $arrData = explode('|', $data);
        $dataHdr['SNRF'] = $arrData[0];
        $dataHdr['DIRFLT'] = $arrData[2];
        $dataHdr['DIREDI'] = $arrData[1];
        $dataHdr['STATUS'] = '020';
        $dataHdr['CAR'] = $car;
        $dataHdr['KODE_TRADER'] = $this->kd_trader;
        $this->actMain->insertRefernce('t_bc20hdr', $dataHdr);

        #insert m_trader_dokoutbound
        $dataDOK['KODE_TRADER'] = $this->kd_trader;
        $dataDOK['KDKPBC'] = $arrData[3];
        $dataDOK['JNSDOK'] = 'BC20';
        $dataDOK['APRF'] = 'DOKPIB09';
        $dataDOK['CAR'] = $car;
        $dataDOK['STATUS'] = '00';
        $this->actMain->insertRefernce('m_trader_dokoutbound', $dataDOK);
        return '0';
    }
    
    function testTranslasi(){
        $this->load->library("nuSoap_lib");
        $Translator = new nusoap_client('http://192.168.5.22:8080/EDITranslator/Services?wsdl');
        if (!$Translator->fault) {
            if (!$Translator->getError()) {
                $keyTrans['APRF'] = 'RESPIB09';
                $exe = $Translator->call('extractEDIFACT', $keyTrans, 'http://services.translator.edii/');
                
                return $exe.'test';
                /*if(count($IDOKE)>0){
                    $IDOKE = implode(',', $IDOKE);
                    $this->db->update('m_trader_dokinbound', array('STATUS'=>'01'), "KODE_TRADER = ".$this->kd_trader." AND IDDOK IN (".$IDOKE.")");
                }*/
            }else{
                return $Translator->getError();
            }
        }else{
            return $Translator->fault;
        }
        
    }
    
    function uploadBUP(){
        $element = 'fileBUP';
        $EDINUMBER = $this->newsession->userdata('EDINUMBER');
        if ($element != "") {
            $path = $_FILES[$element]['name'];
            $ftype = pathinfo($path, PATHINFO_EXTENSION);
            if (!empty($_FILES[$element]['error'])) {
                switch ($_FILES[$element]['error']){
                    case'1':$error = "Ukuran File Terlalu Besar.";break;
                    case'3':$error = "File Yang Ter-Upload Tidak Sempurna.";break;
                    case'4':$error = "File Kosong Atau Belum Dipilih.";break;
                    case'6':$error = "Direktori Penyimpanan Sementara Tidak Ditemukan.";break;
                    case'7':$error = "File Gagal Ter-Upload.";break;
                    case'8':$error = "Proses Upload File Dibatalkan.";break;
                    default :$error = "Pesan Error Tidak Ditemukan.";break;
                }
            }
            else if (empty($_FILES[$element]['tmp_name']) || ($_FILES[$element]['tmp_name'] == 'none')) {
                $error = "File Gagal Ter-Upload.";
            } 
            else if($ftype != 'BUP') {
                $error = "Tipe File Salah.<br>Tipe File Yang Diterima : *.BUP";
            }
            else {
                $filename = 'TRANSACTION/' . $EDINUMBER . '/' . $this->kd_trader . '.' .$ftype;
                mkdir('TRANSACTION/' . $EDINUMBER . '/',0777,true);
                chmod('TRANSACTION/' . $EDINUMBER . '/',0777);
                @unlink($filename);
                if(move_uploaded_file($_FILES[$element]['tmp_name'], $filename)){
                    $ftp_conn = ftp_connect('10.1.5.78') or die('Could not connect to 10.1.5.78');
                    if(ftp_login($ftp_conn, 'Administrator', 'Bismillah')){
                        //move to ftp
                        ftp_pasv($ftp_conn, true);
                        $file = $this->kd_trader . '.' .$ftype;
                        $paht_local = getcwd() . '/TRANSACTION/' . $EDINUMBER . '/'.$file;
                        $paht_remot = '/wsT2G/files/' . $file ;
                        if (ftp_put($ftp_conn, $paht_remot , $paht_local, FTP_BINARY)){
                            $this->load->library("nuSoap_lib");
                            $nuSoap_lib = new nusoap_client('http://10.1.5.78/wsT2g/index.php?wsdl');
                            if ($nuSoap_lib->fault) {
                                $error = 'Error: ' . $nuSoap_lib->fault;
                            }
                            else {
                                if ($nuSoap_lib->getError()) {
                                    $error = 'error ws : ' . $nuSoap_lib->getError();
                                }
                                else {
                                    $error = 'return ws : '.$nuSoap_lib->call('restoreBUP', array( 'namaFile'=> $file ,
                                                                              'kode_trader'  => $this->kd_trader));
                                }
                            }
                            //$error = "Put file from ftp berhasil.";
                        }
                        else{
                            $error = "Put file from ftp gagal.";
                        }
                    }else{
                        $error = "Login ftp gagal.";
                    }
                    ftp_close($ftp_conn);
                }
                else {
                    $error = "File Gagal Ter-Upload.";
                }
            }
        }
        else {
            $error = "Parameter Tidak Ditemukan.";
        }
        @unlink($_FILES[$element]);
        return $error;
    }
    
    function downloadBUP(){
        $arrCar = $this->input->post('tb_chkfdraft');
        $car    = implode(',', $arrCar);
        $EDINUMBER = $this->newsession->userdata('EDINUMBER');
        $this->load->library("nuSoap_lib");
        $nuSoap_lib = new nusoap_client('http://10.1.5.78/wsT2g/index.php?wsdl');
        if ($nuSoap_lib->fault) {
            $error = 'Error: ' . $nuSoap_lib->fault;
        }
        else {
            if ($nuSoap_lib->getError()) {
                $error = 'error ws : ' . $nuSoap_lib->getError();
            }
            else {
                $genBUP = $nuSoap_lib->call('backupBUP', array( 'car'=> $car ,'kode_trader'  => $this->kd_trader));
                if($genBUP){
                    $ftp_conn = ftp_connect('10.1.5.78') or die('Could not connect to 10.1.5.78');
                    if(ftp_login($ftp_conn, 'Administrator', 'Bismillah')){
                        //move to ftp
                        ftp_pasv($ftp_conn, true);
                        $file = $this->kd_trader . '.BUP';
                        $paht_local = getcwd() . '/TRANSACTION/' . $EDINUMBER . '/'.$file;
                        $paht_remot = '/wsT2G/files/' . $file ;
                        if (ftp_get($ftp_conn, $paht_local, $paht_remot, FTP_BINARY)){
                            $error = 'MSG|OK|'. base_url('TRANSACTION/'.$EDINUMBER.'/'.$this->kd_trader.'.BUP').'|Generate File Berhasil.<br>Silahkan didownload.';
                        }
                        else{
                            $error = 'MSG|ERR||Generate File Berhasil.<br>Silahkan didownload.';
                        }
                    }else{
                        $error = "Login ftp gagal.";
                    }
                    ftp_close($ftp_conn);
                }
            }
        }
        return $error;
        
    }
    
    function deleteDokumen($car){
        $this->load->model("actMain");
        $sql = "SELECT CONCAT(a.`STATUS`,'|',b.URAIAN) as STATUS FROM t_bc20hdr a left join m_tabel b on b.MODUL = 'BC20' AND b.KDTAB = 'STATUS' AND b.KDREC =  a.`STATUS` WHERE a.KODE_TRADER = '".$this->kd_trader."' AND a.CAR = '".$car."'";
        $sts = $this->actMain->get_uraian($sql,'STATUS');
        $arrSts = explode('|', $sts);
        if(strpos('|020|030|040|050|060|',$arrSts[0])){
            $rtn = 'MSG|ER|' . site_url('bc20/daftar/draft') . '|Dokumen tidak dapat dihapus karena berstatus '.$arrSts[1].'.';
        }else{
            $key    = array('KODE_TRADER' =>  $this->kd_trader, 'CAR' => $car);
            $this->db->where($key);
            $tbl    = explode(',','t_bc20con,t_bc20conr,t_bc20dok,t_bc20dtl,t_bc20fas,t_bc20hdr,t_bc20kms,t_bc20npt,t_bc20ntb,t_bc20ntbakun,t_bc20pgt,t_bc20res,t_bc20trf');
            $exec = $this->db->delete($tbl);
            $rtn = 'MSG|OK|' . site_url('bc20/daftar/draft') . '|Delete data dokumen PIB berhasil.';
            //echo $this->db->last_query();die();
        }
        return $rtn;
    }
}