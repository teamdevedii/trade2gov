<?php
error_reporting(E_ERROR);
if (!defined('BASEPATH'))exit('No direct script access allowed');
class actMain extends CI_Model {
    function insertRefernce($table, $arrData, $allField = false){
        $tp_session = $this->newsession->userdata('KODE_TIPE_USER');
        $kd_session = $this->newsession->userdata('KODE_TRADER');
        $arrData = array_change_key_case($arrData,CASE_UPPER);
        $arrData = array_map('trim', $arrData);
        $arrData = array_map('strtoupper', $arrData);
        $sqlTBL = 'SHOW FIELDS FROM ' . $table;
        $datTBL = $this->db->query($sqlTBL);
        $arrTBL = $datTBL->result_array();
        $whrTBL = '';
        $data   = '';
        foreach ($arrTBL as $aTBL) {
            $aTBL = array_change_key_case($aTBL, CASE_UPPER);
            $aTBL = array_map('strtoupper' , $aTBL);
            $trueData = false;
            if ($aTBL['EXTRA'] != 'AUTO_INCREMENT') {
                if($allField){
                    $data[$aTBL['FIELD']] = $arrData[$aTBL['FIELD']];
                    $trueData = true;
                }else{
                    if(isset($arrData[$aTBL['FIELD']])){
                        $data[$aTBL['FIELD']] = $arrData[$aTBL['FIELD']];
                        $trueData = true;
                    }
                }
                if($trueData){
                    $type = explode('(', $aTBL['TYPE']);
                    switch ($type[0]){
                        case'DATE':
                            $date = explode('-', $arrData[$aTBL['FIELD']]);
                            if(checkdate($date[1], $date[0], $date[2])){
                                $data[$aTBL['FIELD']] = $date[2] . '-' . $date[1] . '-' . $date[0];
                            }else{
                                $data[$aTBL['FIELD']] = 'NULL';
                            }
                        break;
                        case'DOUBLE':
                        case'INT':
                            $data[$aTBL['FIELD']] = str_replace(',', '', $arrData[$aTBL['FIELD']]);
                        break;
                    }
                }
            }
            #find key
            if ($aTBL['KEY'] == 'PRI') {
                $whrTBL[$aTBL['FIELD']] = $arrData[$aTBL['FIELD']];//$data[$aTBL['FIELD']];
                if($trueData){
                    $whrTBL[$aTBL['FIELD']] = $data[$aTBL['FIELD']];//$data[$aTBL['FIELD']];
                }
            }
            #use defauld if any field KODE_TRADER
            if($aTBL['FIELD']=='KODE_TRADER' && (strpos('|4|5|6|', $tp_session)>0)){
                $data['KODE_TRADER']    =  $kd_session;
                $whrTBL['KODE_TRADER']  =  $kd_session ;
            }
        }
        $datCEK = $this->db->get_where($table, $whrTBL);
        if ($datCEK->num_rows() == 0) {
            $exec = $this->db->insert($table, $data);
        } else {
            $this->db->where($whrTBL);
            $exec = $this->db->update($table, $data);
        }
        return $exec;
    }
    
    function get_uraian($query, $select) {
        $data = $this->db->query($query);
        if ($data->num_rows() > 0) {
            $row = $data->row();
            return trim($row->$select);
        } else {
            return "";
        }
    }

    function get_result($query,$many=false) {
        #get who is the date
        $data = $this->db->query($query);
        if ($data){
            $arr = $data->result_array();
            if($many){
                foreach ($arr as $row){
                    $dataarray[] = $row;
                }
            }else{
                foreach ($arr as $row){
                    $dataarray = $row;
                }
            }
        }else{
            $dataarray = "";
        }
        return $dataarray;
    }

    function get_car($dok) {
        $sql = "select f_getcar(" . $this->newsession->userdata('KODE_TRADER') . ",'".$dok."') AS car ";
        //die($sql);
        $result = $this->db->query($sql);
        $row = $result->row();
        $car = $row->car;
        return $car;
    }

    function set_car($dok) {
        $this->db->simple_query("CALL p_setcar(". $this->newsession->userdata('KODE_TRADER').",'".$dok."')");
        return true;
    }

    function get_combobox($query, $key, $value, $empty = FALSE, &$disable = "") {
        $combobox = array();
        $data = $this->db->query($query);
        if ($empty==true) $combobox[""] = "&nbsp;";
        if ($data->num_rows() > 0) {
            $kodedis = "";
            $arrdis = array();
            foreach ($data->result_array() as $row) {
                if (is_array($disable)) {
                    if ($kodedis == $row[$disable[0]]) {
                        if (!array_key_exists($row[$key], $combobox))
                            $combobox[$row[$key]] = str_replace("'", "\'", "&nbsp; &nbsp;&nbsp;" . $row[$value]);
                    }else {
                        if (!array_key_exists($row[$disable[0]], $combobox))
                            $combobox[$row[$disable[0]]] = $row[$disable[1]];
                        if (!array_key_exists($row[$key], $combobox))
                            $combobox[$row[$key]] = str_replace("'", "\'", "&nbsp; &nbsp;&nbsp;" . $row[$value]);
                    }
                    $kodedis = $row[$disable[0]];
                    if (!in_array($kodedis, $arrdis))
                        $arrdis[] = $kodedis;
                }else {
                    $combobox[$row[$key]] = str_replace("'", "\'", $row[$value]);
                }
            }
            $disable = $arrdis;
        }
        return $combobox;
    }
    
    function get_mtabel($jenis, $by = 1, $empty = TRUE, $where = '',$uraian='KODEUR',$modul='BC20'){
        if ($by == 1)
            $by = "KDREC";
        else
            $by = "URAIAN";
        $combo = $this->get_combobox("SELECT KDREC,CONCAT(KDREC,' - ',URAIAN) as KODEDANUR,URAIAN AS KODEUR FROM M_TABEL WHERE MODUL = '".$modul."' AND KDTAB='".$jenis."'  ".$where." ORDER BY $by ", "KDREC", $uraian, $empty);
        return $combo;
    }
    
    function findArr2Str($str,$arr){
        $hasil = false;
        foreach($arr as $a){
            if(strstr($str,$a)){
                $hasil = true;
                break;
            }
        }
        return $hasil;
    }
    
    function genPassword($length=8){
        $chars      = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
        $password   = substr( str_shuffle( $chars ), 0, $length );
        return $password;
    }
    
    function testFungsi(){die('kill');
        $hasil = $this->send_mail('uqi_illusion@yahoo.com','Testing email','test tok');
        echo $hasil;die();
    }
    
    function send_mail($to, $subject, $isi) {
        $config = array(
            'protocol'  => 'smtp',
            'smtp_host' => 'mail.edi-indonesia.co.id',
            'smtp_port' => '25',
            'smtp_user' => '',
            'smtp_pass' => '',
            'mailtype'  => 'html',
            'charset'   => 'iso-8859-1'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('noreplay@edi-indonesia.co.id', 'Administrator Trade to Goverment');
        $this->email->to(str_replace(';', ',', $to));
        $this->email->bcc('muklis@edi-indonesia.co.id');
        $this->email->subject($subject);
        $body = '<html><body style="background: #ffffff; color: #000000; font-family: arial; font-size: 13px; margin: 20px; color: #363636;"><table style="margin-bottom: 2px"><tr style="font-size: 13px; color: #0b1d90; font-weight: 700; font-family: arial;"><td width="41" style="margin: 0 0 6px 0px;"><img src="'.base_url().'img/logo.png" style="vertical-align: middle;"/></td><td style="font-family: arial; vertical-align: middle; color: #153f6f;">'.$subject.'<br/><span style="color: #858585; font-size: 10px; text-decoration: none;">www.trade2gov.com</span></td></tr></table><div style="background-color: #dee8f4; margin-top: 4px; margin-bottom: 10px; padding: 5px; font-family: Verdana; font-size: 11px;text-align:justify;">'.$isi.'</div><div style="border-top: 1px solid #dcdcdc; clear: both; font-size: 11px; margin-top: 10px; padding-top: 5px;"><div style="font-family: arial; font-size: 10px; color: #a7aaab;">Trade to Goverment<br>ElektroniC Data Interchange</div><a style="text-decoration: none; font-family: arial; font-size: 10px; font-weight: normal;" href="http://www.edi-indonesia.co.id/">Website EDII </a> | <a style="text-decoration: none; font-family: arial; font-size: 10px; font-weight: normal;" href="'.site_url().'">Website PIB Online</a></div></body></html>';
        $this->email->message($body);
        $hasil = $this->email->send();
        return $hasil;
    }

    function upload($folder = "", $element = "", $id = "") {
        $error = "";
        $msg = "";
        $arrtype = array("JPG", "JPEG", "GIF", "DOC", "PDF", "RAR", "ZIP", "PNG");
        if ($element != "") {
            $ftype = explode("|", $_FILES[$element]['name']);
            $ftype = trim(strtoupper($ftype[count($ftype) - 1]));
            if (!empty($_FILES[$element]['error'])) {
                if (($_FILES[$element]['error'] == "1") || ($_FILES[$element]['error'] == "2")) {
                    $error = "<b>Error :</b><br>Ukuran File " . strtoupper($element) . " Terlalu Besar.";
                }
                else if ($_FILES[$element]['error'] == "3") {
                    $error = "<b>Error :</b><br>File " . strtoupper($element) . " Yang Ter-Upload Tidak Sempurna.";
                }
                else if ($_FILES[$element]['error'] == "4") {
                    $error = "<b>Error :</b><br>File " . strtoupper($element) . " Kosong Atau Belum Dipilih.";
                } 
                else if ($_FILES[$element]['error'] == "6") {
                    $error = "<b>Error :</b><br>Direktori Penyimpanan Sementara Tidak Ditemukan.";
                } 
                else if ($_FILES[$element]['error'] == "7") {
                    $error = "<b>Error :</b><br>File " . strtoupper($element) . " Gagal Ter-Upload.";
                } 
                else if ($_FILES[$element]['error'] == "8") {
                    $error = "<b>Error :</b><br>Proses Upload File " . strtoupper($element) . " Dibatalkan.";
                } 
                else {
                    $error = "<b>Error :</b><br>Pesan Error Tidak Ditemukan.";
                }
            }
            else if (empty($_FILES[$element]['tmp_name']) || ($_FILES[$element]['tmp_name'] == 'none')) {
                $error = "<b>Error :</b><br>File " . strtoupper($element) . " Gagal Ter-Upload.";
            } 
            else if (!in_array($ftype, $arrtype)) {
                $error = "<b>Error :</b><br>Tipe File " . strtoupper($element) . " Salah.<br>Tipe File Yang Diterima : *.JPG, *.JPEG, *.GIF, *.DOC, *.PDF, *.RAR Dan *.ZIP *.PNG";
            } 
            else {

                $path = "img/" . $folder . '/';
                $cekpath = $this->validate_upload_path($path);
                if ($cekpath != 1) {
                    return "{error: '$path',\n msg: '$msg'\n}";
                }
                $filename = $path . md5($id) . ".$ftype";
                $imagename = str_replace($path, "", $filename);
                if (move_uploaded_file($_FILES[$element]['tmp_name'], $filename)) {
                    $rstemp = 1;
                    if ($rstemp == 1) {
                        if ($ftype == "JPG" || $ftype == "JPEG" || $ftype == "GIF" || $ftype == "PNG") {
                            $msg1 = "<table border='0' cellpadding='0' cellpadding='0'><tr><td><img src=\"" . base_url() . "img/" . $folder . '/' . $imagename . "\" alt='' style=\"max-height:181px;max-width:181px;border:solid 1px #CCC\" align=\"middle\"></td><td valign='top'>";
                            $msg2 = "</td><tr></table>";
                        }
                        $msg .= $msg1;
                        $msg .= "<table border='0' cellpadding='0' cellpadding='0'><tr><td colspan='3'>Upload File Berhasil.</td></tr>";
                        $msg .= '<tr><td>Nama File</td><td>:</td><td>' . $_FILES[$element]['name'] . "</td></tr>";
                        if ($element == "logo") {
                            $msg .= '<tr><td>Tipe File</td><td>:</td><td>' . $_FILES[$element]['type'] . "</td></tr>";
                            $msg .= '<tr><td>Ukuran File</td><td>:</td><td>' . @filesize($filename) . " byte</td></tr>";
                            $msg .= "<tr><td><a href='javascript:void(0);' class='button del' id='btnupload' onClick=\"deleteFupload(\'" . $imagename . "\',\'" . $element . "\')\"><span><span class='icon'></span>&nbsp;Delete&nbsp;</span></a></td></tr>";
                        }
                        $msg .= "</table>";
                        if ($element == "logo")
                            $msg .= "<input type=\"hidden\" name=\"DATA[LOGO]\" id=\"LOGO\" value=\"" . $imagename . "\"/>";
                        else
                            $msg .= "<input type=\"hidden\" name=\"PROFIL[LOGO]\" id=\"LOGO\" value=\"" . $imagename . "\"/>";
                        $msg .= $msg2;
                    }else {
                        $error = "<b>Error :</b> File " . strtoupper($element) . " Gagal Ter-Upload.";
                        @unlink($filename);
                    }
                } else {
                    $error = "<b>Error :</b> File " . strtoupper($element) . " Gagal Ter-Upload.";
                }
                @unlink($_FILES[$element]);
            }
        } else {
            $error = "<b>Error :</b><br>Parameter Tidak Ditemukan.";
        }
        return "{error: '$error',\n msg: '$msg'\n}";
    }

    private function validate_upload_path($upload_path) {
        if ($upload_path == '') {
            return 'No filepath 1';
        }
        if (function_exists('realpath') AND @realpath($upload_path) !== FALSE) {
            $upload_path = str_replace("\\", "/", realpath($upload_path));
        }
        if (!@is_dir($upload_path)) {
            return 'No filepath 2';
        }
        if (!is_really_writable($upload_path)) {
            return 'upload Not Writable';
        }

        $upload_path = preg_replace("/(.+?)\/*$/", "\\1/", $upload_path);
        return '1';
    }
    
    function get_Setting($modul,$variable=''){
        if(is_array($variable)){
            $var = "'".implode("','", $variable)."'";
            $query = "SELECT VARIABLE,VALUE FROM m_setting WHERE MODUL = '$modul' AND VARIABLE in (".$var.")";
        }else{
            $query = "SELECT VARIABLE,VALUE FROM m_setting WHERE MODUL = '$modul' AND VARIABLE = '".$variable."'";
        }
        $data = $this->get_result($query,true);
        foreach($data as $a){
            $hasil[$a['VARIABLE']] = $a['VALUE'];
        }
        return $hasil;
    }
    
    function formatNPWP($npwp) {
        $strlen = strlen($npwp);
        if ($strlen == 15) {
            $npwpnya = substr($npwp, 0, 2).".".substr($npwp, 2, 3).".".substr($npwp, 5, 3).".".substr($npwp, 8, 1)."-".substr($npwp, 9, 3).".".substr($npwp, 12, 3);
        } else if($strlen == 12){
            $npwpnya = substr($npwp, 0, 2).".".substr($npwp, 2, 3).".".substr($npwp, 5, 3).".".substr($npwp, 8, 1)."-".substr($npwp, 9, 3);
        }else{
            $npwpnya = $npwp;
        }
        return $npwpnya;
    }
}

?>