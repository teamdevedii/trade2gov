<?php
if (!defined('BASEPATH')){exit('No direct script access allowed');}
class dokBC30PKBE extends CI_Model {
    var $main = '';      #load actMain
    var $pdf = '';       #load fpdf library
    var $nPungutan = ''; #pungutan
    var $car = '';       #Car
    var $hdr = '';       #data header dalam array 1D
    var $res = '';       #data header dalam array 1D
    var $dok = '';       #data dokumen dalam array 2D
    var $con = '';       #data container dalam array 2D
    var $conr = '';      #data container dalam array 2D
    var $kms = '';       #data kemasan dalam array 2D
    var $dtl = '';       #data detil dalam array 2D
    var $pgt = '';       #data pungutan dalam array 2D
    var $ntb = '';       #data ntb dalam array 1D
    var $npt = '';       #data ntp dalam array 1D
    var $ktr = '';       #data kantor kpbc dalam array 1D
    var $rpt = '';       #data caption respon NPP
    var $kd_trader = ''; #session data kode trader
    var $tp_trader = ''; #session data tipe trader
    var $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
    var $segmentUri = array(); #segment url

    function fpdf($pdf) {
        $this->pdf = $pdf;
    }

    function ciMain($main) {
        $this->main = $main;
    }

    function segmentUri($arr) {
        $this->segmentUri = $arr;
    }

    function showPage($car, $jnPage) {
        $this->car = $car;
        $this->kd_trader = $this->newsession->userdata('KODE_TRADER');
        $this->tp_trader = $this->newsession->userdata('TIPE_TRADER');
        $this->pdf->AddPage();
        switch ($jnPage) {
            case'1':
                $this->getHDR();
                $this->getDOK();
                $this->getCON();
                $this->getKMS();
                $this->getDTL();
                $this->getPGT();
                $this->drawHeaderTPB();
                break;
            case'2':
                $this->getNTB();
                $this->drawSSPCP();
                break;
            case'3':
                $this->getHDR();
                $this->getDOK();
                $this->getCON();
                $this->getKMS();
                $this->getDTL();
                $this->drawLembarCatPencocokan();
                break;
            case'4':
                $this->getHDR();
                $this->getDOK();
                $this->getCON();
                $this->getKMS();
                $this->getDTL();
                $this->drawLembarCatPemerikasaanFisikBrg();
                break;
            case'100':case'110':case'120':case'200':case'210':case'220':case'230':case'240':case'270':case'900':
            case'470':case'130':case'600':case'700':case'295':case'250':
                $this->getHDR();
                $this->getRES();
                $this->isiRespon();
                $this->drawResponUmum();
                break;
            case'280';
                $this->getHDR();
                $this->getRES();                
                $this->drawResponSPPB();
                break;
            case'470';
                $this->getHDR();
                $this->getRES();                
                $this->drawResponPembatalan(); 
                break;
            case'500';
                $this->getHDR();
                $this->getRES();                
                $this->drawResponSPKPBM(); 
                break;
            default :
                $this->pdf->SetFont('times', 'B', '12');
                $this->pdf->cell(131.6, 4, 'Tidak ada jenis laporan ini', 0, 0, 'R', 0);
                break;
        }
        $this->pdf->Output();
    }

    function getHDR() {
        $sql = "SELECT *, 
                       F_KPBC(KDKPBC)   as URKDKPBC ,
                       F_KPBC(KDKPBCBONGKAR)  as URKDKPBCBONGKAR ,
                       F_KPBC(KDKPBCAWAS)  as URKDKPBCAWAS ,
                       F_TABEL('BC23','JENIS_IDENTITAS',USAHAID) as URIMPID,
                       F_TABEL('BC23','JNS_NOIJIN',APIKD) as URAPIKD,                       
                       F_TABEL('BC23','DOKTUPKD',DOKTUPKD) as URDOKTUPKD,
                       DATE_FORMAT(DOKTUPTG,'%d-%m-%Y') as DOKTUPTG,
                       DATE_FORMAT(PPJKTG,'%d-%m-%Y') as PPJKTG,
                       F_TIMBUN(TMPTBN,KDKPBC) as URTMPTBN,
                       F_TABEL('BC23','MODA',MODA) as URMODA,
                       F_NEGARA(ANGKUTFL) as URANGKUTFL,
                       F_VALUTA(KDVAL) as URKDVAL,
                       F_TABEL('BC23','KODE_HARGA',KDHRG) as URKDHRG,
                       F_PELAB(PELTRANSIT) as URPELTRANSIT,
                       F_PELAB(PELMUAT) as URPELMUAT,
                       F_PELAB(PELBKR) as URPELBKR,
                       DATE_FORMAT(TGLTTD,'%d-%m-%Y') as TANGGAL_TTD
                FROM t_bc23hdr WHERE CAR = '" . $this->car . "' AND KODE_TRADER = '" . $this->kd_trader . "'"; //die($sql);
        $this->hdr = $this->main->get_result($sql);
        //print_r($this->hdr);die();
    }

    function getRESTPB() {
        $sql = "SELECT *,concat(KDGUDANG,'    /  ',b.URAIAN) as 'URGUDANG'
                FROM   t_bc23RESTPB a LEFT JOIN  M_GUDANG b ON a.KDGUDANG = b.KDGDG
                WHERE  CAR = '" . $this->car . "' AND "
                . "    KODE_TRADER = '" . $this->kd_trader . "' AND "
                . "    RESKD = '" . $this->segmentUri[4] . "' AND "
                . "    RESTG = '" . $this->segmentUri[5] . "' AND "
                . "    RESWK = '" . $this->segmentUri[6] . "'";
        $this->res = $this->main->get_result($sql);
        $this->getKTR($this->res['KPBC']);
        //print_r($this->res);die();
    }

    function getDOK() {
        $sql = "SELECT DOKKD,DOKNO,DATE_FORMAT(DOKTG,'%d-%m-%Y') AS DOKTG, b.URAIAN as 'URDOKKD'
                FROM   t_bc23dok 
                Left Join m_tabel b on b.MODUL = 'BC23' AND b.KDREC = t_bc23dok.DOKKD
                WHERE  CAR = '" . $this->car . "' AND 
                       KODE_TRADER = '" . $this->kd_trader . "'";
        $this->dok = $this->main->get_result($sql, true);
        //print_r($this->dok);die();
    }

    function getCON() {
        $sql = "SELECT CONTNO,CONCAT(CONTUKUR,' Feed') as CONTUKUR,CONCAT(CONTTIPE,'CL') as CONTTIPE
                FROM t_bc23con 
                WHERE CAR = '" . $this->car . "' AND KODE_TRADER = '" . $this->kd_trader . "'";
        $this->con = $this->main->get_result($sql, true);        
    }    

    function getKMS() {
            $sql = "SELECT JNKEMAS,JMKEMAS,MERKKEMAS,F_KEMAS(JNKEMAS) as URJNKEMAS
                    FROM t_bc23kms 
                WHERE CAR = '" . $this->car . "' AND KODE_TRADER = '" . $this->kd_trader . "'";
        $this->kms = $this->main->get_result($sql, true);
        //print_r($this->con);die();
    }

    function getDTL() {
        $sql = "SELECT *,
                        F_NEGARA(BRGASAL) as URBRGASAL,
                        F_SATUAN(KDSAT) AS URKDSAT,
                        F_KEMAS(KEMASJN) AS URKEMASJN,
                        F_TABEL('BC23','SKEP_FASILITAS',KDFASDTL) AS URKDFASDTL,
                        F_TABEL('BC23','PENGGUNAAN',PENGGUNAAN) AS URPENGGUNAAN
                FROM t_bc23dtl left join t_bc23fas on t_bc23dtl.KODE_TRADER = t_bc23fas.KODE_TRADER and  
                                                      t_bc23dtl.CAR         = t_bc23fas.CAR and 
                                                      t_bc23dtl.SERIAL      = t_bc23fas.SERIAL
                               left join t_bc23trf on t_bc23dtl.KODE_TRADER = t_bc23trf.KODE_TRADER and
                                                      t_bc23dtl.CAR         = t_bc23trf.CAR and
                                                      t_bc23dtl.SERIAL      = t_bc23trf.SERIAL
                WHERE t_bc23dtl.CAR = '" . $this->car . "' AND t_bc23dtl.KODE_TRADER = '" . $this->kd_trader . "'
                ORDER BY t_bc23dtl.SERIAL ASC";
        $this->dtl = $this->main->get_result($sql, true);        
    }

    function getPGT() {
        $sql = "SELECT KDBEBAN,KDFASIL,NILBEBAN
                FROM  t_bc23pgt 
                WHERE t_bc23pgt.CAR = '" . $this->car . "' AND t_bc23pgt.KODE_TRADER = '" . $this->kd_trader . "'";
        $this->pgt = $this->main->get_result($sql, true);
        //print_r($this->pgt);die();
    }
     
    function getKTR($kdKPBC){
        $SQL = "SELECT URAIAN_KPBC,KOTA,ESELON FROM m_kpbc WHERE KDKPBC = '".$kdKPBC."'";
        $this->ktr = $this->main->get_result($SQL);
        $this->ktr['KWBC'] = $this->main->get_uraian("SELECT URAIAN_KPBC FROM m_kpbc WHERE KDKPBC = '".$this->ktr['ESELON']."'",'URAIAN_KPBC');
        if($kdKPBC==='040300'){
            $this->ktr['KOP'] = "KEMENTERIAN KEUANGAN REPUBLIK INDONESIA\nDIREKTORAT JENDERAL BEA DAN CUKAI\nKANTOR PELAYANAN UTAMA TANJUNG PRIOK";
        }elseif($kdKPBC==='020400'){
            $this->ktr['KOP'] = "KEMENTERIAN KEUANGAN REPUBLIK INDONESIA\nDIREKTORAT JENDERAL BEA DAN CUKAI\nKANTOR PELAYANAN UTAMA BATAM";
        }else{
            $this->ktr['KOP'] = "KEMENTERIAN KEUANGAN REPUBLIK INDONESIA\nDIREKTORAT JENDERAL BEA DAN CUKAI\n" . (($this->ktr['KWBC']=='')?'':$this->ktr['KWBC']."\n"). str_replace('KPPBC', 'KANTOR PENGAWASAN DAN PELAYANAN',str_replace('KPU', 'KANTOR PELAYANAN UTAMA',strtoupper($this->ktr['URAIAN_KPBC'])));
        }
    }
    
    function getNTB() {
        $query = "SELECT a.*,URAIAN_KPBC as 'URKPBC' " .
                 "FROM   t_bc23ntb a Left Join m_kpbc b on a.KDKPBC = b.KDKPBC ".
                 "WHERE  CAR = '" . $this->car . "' AND KODE_TRADER = " . $this->kd_trader;
        $ntb = $this->main->get_result($query);

        $query = "SELECT AKUN_SSPCP,NILAI FROM t_bc23ntbakun WHERE CAR = '" . $this->car . "' AND KODE_TRADER = " . $this->kd_trader;
        $arrakun = $this->actMain->get_result($query, true);
        foreach ($arrakun as $a) {
            $hasil[$a['AKUN_SSPCP']] = $a['NILAI'];
            $hasil['TOTAL'] += $a['NILAI'];
        }
        $this->ntb = $hasil + $ntb; 
    }
       
    //SSPCP
    function drawSSPCP() {
        $this->pdf->SetMargins(5, 0, 0);
        $this->pdf->SetAutoPageBreak(0, 0);
        $this->pdf->SetY(5);
        $this->pdf->SetFont('times', 'B', '8.5');
        $Y = $this->pdf->getY();
        $this->pdf->multicell(65, 4, "KEMENTERIAN KEUANGAN R.I.\nDIREKTORAT JENDERAL BEA DAN CUKAI", 0, 'L');
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->multicell(65, 4, $this->ntb['URKPBC'] . ' / ' . $this->ntb['KDKPBC'], 0, 'L');
        $arrY[] = $this->pdf->getY() - 5;
        $this->pdf->setXY(70, $Y);
        $this->pdf->SetFont('times', 'B', '12');
        $this->pdf->multicell(70, 6, "SURAT SETORAN\nPABEAN, CUKAI DAN PAJAK\n( SSPCP )", 0, 'C');
        $this->pdf->setXY(140, $Y);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->multicell(65, 4, "Lembar ke-1  :  Wajib Bayar\nLembar ke-2  :  KPPN\nLembar ke-3  :  Kantor Bea dan Cukai\nLembar ke-4  :  Bank Devisa Persepsi / Bank\n                          Persepsi / Pos Persepsi", 0, 'L');
        $arrY[] = $this->pdf->getY() - 5; 
        $Y = max($arrY);
        $this->pdf->Rect(5, 5, 65, $Y);
        $this->pdf->Rect(70, 5, 70, $Y);
        $this->pdf->Rect(140, 5, 65, $Y);
        $this->pdf->SetY($Y + 5);
        $this->pdf->SetFont('times', 'B', '9');
        $this->pdf->cell(65, 6, 'A. JENIS PENERIMAAN NEGARA', 1, 0, 'L', 0);
        $this->pdf->cell(5, 6, 'X', 1, 0, 'C', 0);
        $this->pdf->cell(20, 6, 'IMPOR', 1, 0, 'L', 0);
        $this->pdf->cell(5, 6, ' ', 1, 0, 'C', 0);
        $this->pdf->cell(20, 6, 'EXSPOR', 1, 0, 'L', 0);
        $this->pdf->cell(5, 6, ' ', 1, 0, 'L', 0);
        $this->pdf->cell(20, 6, 'CUKAI', 1, 0, 'L', 0);
        $this->pdf->cell(5, 6, ' ', 1, 0, 'C', 0);
        $this->pdf->cell(55, 6, 'BARANG TERTENTU', 1, 0, 'L', 0);
        $this->pdf->ln();
        $this->pdf->cell(65, 6, 'B. JENIS IDENTITAS', 1, 0, 'L', 0);
        $this->pdf->cell(5, 6, 'X', 1, 0, 'C', 0);
        $this->pdf->cell(20, 6, 'NPWP', 1, 0, 'L', 0);
        $this->pdf->cell(5, 6, ' ', 1, 0, 'C', 0);
        $this->pdf->cell(20, 6, 'PASPOR', 1, 0, 'L', 0);
        $this->pdf->cell(5, 6, ' ', 1, 0, 'L', 0);
        $this->pdf->cell(80, 6, 'KTP', 1, 0, 'L', 0);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->ln();
        $Y = $this->pdf->getY();
        $this->pdf->SetX(10);
        $npwp = str_split($this->ntb['USAHANPWP']);
        $i = 0;
        $this->pdf->cell(20, 5, 'NOMOR', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(7, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->cell(9, 5, $npwp[$i++], 1, 0, 'C', 0);
        $this->pdf->ln();
        $this->pdf->SetX(10);
        $this->pdf->cell(20, 5, 'NAMA', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->cell(170, 5, $this->ntb['NAMA'], 0, 0, 'L', 0);

        $this->pdf->ln();
        $this->pdf->SetX(10);
        $this->pdf->cell(20, 5, 'ALAMAT', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->cell(170, 5, $this->ntb['ALAMAT'], 0, 0, 'L', 0);
        $this->pdf->ln();
        $this->pdf->SetX(150);
        $this->pdf->cell(55, 5, 'Kode Pos  :  ' . $this->ntb['KDPOS'], 0, 0, 'L', 0);
        $this->pdf->ln();
        $Yawal = $Y;
        $Y = $this->pdf->getY() + 2;
        $this->pdf->Rect(5, $Yawal, 200, $Y - $Yawal);
        $this->pdf->setY($Y);
        $this->pdf->SetFont('times', 'B', '9');
        $this->pdf->cell(65, 5, 'C. DOKUMEN DASAR PEMBAYARAN  :', 0, 0, 'L', 0);
        $this->pdf->SetFont('times', '', '9');  
        $this->pdf->cell(135, 5, 'Pemberitahuan Penyelesaian Barang Asal Impor (BC 2.3)', 0, 0, 'L', 0); 
        $this->pdf->ln();
        $car = substr($this->ntb['CAR'], 0, 6) . " - " . substr($this->ntb['CAR'], 6, 6) . " - " . substr($this->ntb['CAR'], 12, 8) . " - " . substr($this->ntb['CAR'], 20);
        $tgl = substr($this->ntb['CAR'], 12, 8);
        $tgl = substr($tgl, 0, 4) . '-' . substr($tgl, 4, 2) . '-' . substr($tgl, 6, 2);
        $this->pdf->cell(145, 5, '      Nomor  :  Nomor Pengajuan BC 2.3 : ' . $car, 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, 'Tanggal : ' . $tgl, 0, 0, 'L', 0);
        $this->pdf->ln();
        $Yawal = $Y;
        $Y = $this->pdf->getY() + 2;
        $this->pdf->Rect(5, $Yawal, 200, $Y - $Yawal);
        $this->pdf->setY($Y);
        $this->pdf->SetFont('times', 'B', '9');
        $this->pdf->cell(200, 5, 'D. PEMBAYARAN PENERIMAAN NEGARA  :', 1, 0, 'L', 0);
        $this->pdf->ln();
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->cell(120, 5, 'AKUN', 1, 0, 'C', 0);
        $this->pdf->cell(24, 5, 'KODE AKUN', 1, 0, 'C', 0);
        $this->pdf->cell(56, 5, 'JUMLAH PEMBAYARAN', 1, 0, 'C', 0);
        $this->pdf->ln();

        $this->pdf->cell(120, 5, '     Bea Masuk', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '412111', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['412111'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(120, 5, '     Bea Masuk Ditanggung Pemerintah atas Hibah (SPM) Nihil', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '412112', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['412112'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(120, 5, '     Bea Masuk Dalam Rangka Kemudahan Impor Tujuan Elspor (KITE)', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '412114', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['412114'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(120, 5, '     Denda Administrasi Pabean', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '412113', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['412113'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(120, 5, '     Denda Administrasi Atas Pengangkutan Barang Tertentu', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '412115', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['412115'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(120, 5, '     Pendapatan Pabean lainnya', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '412119', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['412119'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(120, 5, '     Bea Keluar', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '412211', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['412211'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(120, 5, '     Denda Administrasi Bea Keluar', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '412212', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['412212'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(120, 5, '     Bunga Bea Keluar', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '412213', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['412213'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(120, 5, '     Cukai Hasil Tembakau', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '411511', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['411511'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(120, 5, '     Cukai Etil Alkohol', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '411512', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['411512'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(120, 5, '     Cukai Minuman Mengandung Etil Alkohol', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '411513', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['411513'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(120, 5, '     Pendapatan Cukai lainnya', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '411519', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['411519'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(120, 5, '     Denda Administrasi Cukai', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '411514', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['411514'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(120, 5, '     PNBP/Pendapatan DJBC', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '423216', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['423216'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(70, 5, '     PPN IMPOR', 0, 0, 'L', 0);
        $this->pdf->cell(15, 5, 'NPWP : ', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, ($this->ntb['411212'] != '') ? $this->formatNPWP($this->ntb['USAHANPWP']) : '', 'B', 0, 'L', 0);
        $this->pdf->cell(24, 5, '411212', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['411212'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(120, 5, '     PPN Hasil Tembakau / PPN Dalam Negeri', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '411211', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['411211'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(70, 5, '     PPnBM Impor', 0, 0, 'L', 0);
        $this->pdf->cell(15, 5, 'NPWP : ', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, ($this->ntb['411222'] != '') ? $this->formatNPWP($this->ntb['USAHANPWP']) : '', 'B', 0, 'L', 0);
        $this->pdf->cell(24, 5, '411222', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['411222'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(70, 5, '     PPh Pasal 22 Impor ', 0, 0, 'L', 0);
        $this->pdf->cell(15, 5, 'NPWP : ', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, ($this->ntb['411123'] != '') ? $this->formatNPWP($this->ntb['USAHANPWP']) : '', 'B', 0, 'L', 0);
        $this->pdf->cell(24, 5, '411123', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['411123'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();

        $this->pdf->cell(120, 5, '     Bunga Penagihan PPN', 0, 0, 'L', 0);
        $this->pdf->cell(24, 5, '411622', 0, 0, 'C', 0);
        $this->pdf->cell(16, 5, 'Rp.', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntb['411622'], 2, '.', ','), 'B', 0, 'R', 0);
        $this->pdf->ln();
        $Yawal = $Y;
        $Y = $this->pdf->getY() + 2;
        $this->pdf->Rect(5, $Yawal, 200, $Y - $Yawal);
        $this->pdf->Rect(125, $Yawal, 24, $Y - $Yawal);
        $this->pdf->setY($Y);
        $this->pdf->cell(144, 5, 'Masa Pajak', 1, 0, 'C', 0);
        $this->pdf->cell(56, 5, 'Tahun Pajak', 0, 0, 'C', 0);
        $this->pdf->ln();
        $bln = array('Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nop', 'Des');
        foreach ($bln as $a) {
            $x[] = $this->pdf->getX();
            $this->pdf->cell(12, 6, $a, 1, 0, 'C', 0);
        }
        $this->pdf->setX($x[$this->ntb['MASAPAJAK'] - 1]);
        $this->pdf->SetFont('times', 'B', '15');
        $this->pdf->cell(12, 6, 'X', 0, 0, 'C', 0);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->setX($x[11] + 22);
        $thn = str_split($this->ntb['TAHUNPAJAK']);
        foreach ($thn as $a) {
            $this->pdf->cell(9, 6, $a, 1, 0, 'C', 0);
        }
        $this->pdf->ln();
        $Yawal = $Y;
        $Y = $this->pdf->getY();
        $this->pdf->Rect(5, $Yawal, 200, $Y - $Yawal);
        $this->pdf->SetFont('times', 'B', '9');
        $this->pdf->cell(200, 6, 'E. JUMLAH PEMBAYARAN PENERIMAAN NEGARA   : Rp.  ' . number_format($this->ntb['TOTAL'], 2, '.', ','), 0, 0, 'L', 0);
        $this->pdf->ln();
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->cell(30, 5, '     Dengan Huruf    :', 0, 0, 'L', 0);
        $this->pdf->multicell(170, 5, strtoupper($this->terbilang($this->ntb['TOTAL'])), 0, 'L');
        $Yawal = $Y;
        $Y = $this->pdf->getY() + 2;
        $this->pdf->Rect(5, $Yawal, 200, $Y - $Yawal);
        $this->pdf->setY($Y);
        $this->pdf->cell(25, 6, 'Diterima Oleh', 0, 0, 'L', 0);
        $this->pdf->cell(6, 6, '', 1, 0, 'L', 0);
        $this->pdf->cell(35, 6, 'Kantor Bea dan Cukai', 0, 0, 'L', 0);
        $this->pdf->cell(6, 6, '', 1, 0, 'L', 0);
        $this->pdf->cell(32, 6, 'Kantor Pos', 0, 0, 'L', 0);
        $this->pdf->cell(6, 6, '', 1, 0, 'L', 0);
        $this->pdf->cell(32, 6, 'Bank Devisa Persepsi', 0, 0, 'L', 0);
        $this->pdf->cell(6, 6, '', 1, 0, 'L', 0);
        $this->pdf->cell(23, 6, 'Bank Persepsi', 0, 0, 'L', 0);
        $this->pdf->cell(6, 6, '', 1, 0, 'L', 0);
        $this->pdf->cell(23, 6, 'Pos Persepsi', 0, 0, 'L', 0);
        $this->pdf->ln(7);
        $Y2 = $this->pdf->getY();
        $this->pdf->setX(10);
        $this->pdf->cell(25, 5, 'NPWP', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->cell(65, 5, $this->formatNPWP($this->ntb['NPWP']), 0, 0, 'L', 0);
        $this->pdf->ln();
        $this->pdf->setX(10);
        $this->pdf->cell(25, 5, 'Nama Kantor', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->multicell(65, 5, $this->ntb['URKPBC'], 0, 'L');
        $this->pdf->setX(10);
        $this->pdf->cell(25, 5, 'Kode Kantor', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->cell(65, 5, $this->ntb['KDKPBC'], 0, 0, 'L', 0);
        $this->pdf->ln();
        $this->pdf->setX(10);
        $this->pdf->cell(25, 5, 'Nomor SSPCP', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->cell(65, 5, '........................................................................', 0, 0, 'L', 0);
        $this->pdf->ln();
        $this->pdf->setX(10);
        $this->pdf->cell(25, 5, 'Tanggal', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->cell(65, 5, '........................................................................', 0, 0, 'L', 0);
        $this->pdf->ln(7);
        $this->pdf->SetFont('times', 'i', '9');
        $this->pdf->cell(100, 8, 'Cap dan tanda tangan', 0, 0, 'C', 0);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->ln(7);
        $this->pdf->setX(25);
        $this->pdf->cell(15, 5, 'Nama', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->multicell(60, 5, "..............................................................\n..............................................................", 0, 'L');
        $arY[] = $this->pdf->getY();
        $this->pdf->setY($Y2);
        $this->pdf->setX(110);
        $this->pdf->cell(25, 5, 'Nama Bank/Pos', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->cell(65, 5, '........................................................................', 0, 0, 'L', 0);
        $this->pdf->ln();
        $this->pdf->setX(110);
        $this->pdf->cell(25, 5, 'Kode Bank/Pos', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->cell(65, 5, '........................................................................', 0, 0, 'L', 0);
        $this->pdf->ln();
        $this->pdf->setX(110);
        $this->pdf->cell(25, 5, 'Nomor SSPCP', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->cell(65, 5, '........................................................................', 0, 0, 'L', 0);
        $this->pdf->ln();
        $this->pdf->setX(110);
        $this->pdf->cell(25, 5, 'Unit KPPN', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->cell(65, 5, '........................................................................', 0, 0, 'L', 0);
        $this->pdf->ln();
        $this->pdf->setX(110);
        $this->pdf->cell(25, 5, 'Tanggal', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->cell(65, 5, '........................................................................', 0, 0, 'L', 0);
        $this->pdf->ln();
        $this->pdf->ln(7);
        $this->pdf->SetFont('times', 'i', '9');
        $this->pdf->setX(110);
        $this->pdf->cell(100, 8, 'Cap dan tanda tangan', 0, 0, 'C', 0);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->ln(7);
        $this->pdf->setX(125);
        $this->pdf->cell(15, 5, 'Nama', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->multicell(60, 5, "..............................................................\n..............................................................", 0, 'L');
        $arY[] = $this->pdf->getY();
        $Yawal = $Y;
        $Y = max($arY) + 2;
        $this->pdf->Rect(5, $Yawal, 100, $Y - $Yawal);
        $this->pdf->Rect(105, $Yawal, 100, $Y - $Yawal);
        $this->pdf->setY($Y);
        $this->pdf->SetFont('times', 'B', '9');
        $this->pdf->cell(100, 5, 'NTB / NTP  : ' . $this->ntb['NTB'], 1, 0, 'L', 0);
        $this->pdf->cell(100, 5, 'NTPN  : ' . $this->ntb['NTPN'], 1, 0, 'L', 0);
    }

    //TPB
    function drawHeaderTPB() {//
        $noPrinLampiranDok = '';
        $this->pdf->SetMargins(5.4, 0, 0);
        $this->pdf->SetAutoPageBreak(0, 0);
        $this->pdf->SetY(5.4);
        $this->pdf->SetFont('times', 'B', '12');
        $this->pdf->SetX(30);
        $this->pdf->cell(131.6, 4, 'PEMBERITAHUAN IMPOR BARANG UNTUK DITIMBUN DI', 0, 0, 'R', 0);
        
        $this->pdf->Ln(); 
        $this->pdf->SetX(0);
        $this->pdf->cell(131.6, 4, 'TEMPAT PENIMBUNAN BERIKAT', 0, 0, 'R', 0);
        $this->pdf->SetX(140);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->cell(67.6, 4, 'BC 2.3', 10, 0, 'R', 0);
        
        //KPBC
        $this->pdf->Rect(5.4, 13.4, 201, 28, 1.5, 'F');
        $this->pdf->Ln();
        $this->pdf->cell(45, 4, 'Kantor Pabean', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(103, 4, $this->hdr['URKDKPBC'], 0, 0, 'L', 0);
        $this->pdf->cell(15, 4, $this->hdr['KDKPBC'], 1, 0, 'C', 0);
        $this->pdf->cell(30, 4, " Halaman " . $this->pdf->PageNo() . " dari {nb}", 0, 0, 'R', 0);
        $this->pdf->AliasNbPages();
        $this->pdf->Ln();
        $this->pdf->cell(45, 4, 'Nomor Pengajuan', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(80, 4, substr($this->hdr['CAR'], 0, 6) . " - " . substr($this->hdr['CAR'], 6, 6) . " - " . substr($this->hdr['CAR'], 12, 8) . " - " . substr($this->hdr['CAR'], 20), 0, 0, 'L', 0);
        $this->pdf->Ln();

        // Section A
        $this->pdf->cell(40, 4, 'A . Tujuan', 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, '', 0, 0, 'C', 0);
        $this->pdf->cell(5, 3, $this->hdr['TUJUAN'], 1, 0, 'C', 0);
        $this->pdf->cell(30, 4, '1. Kawasan Berikat', 0, 0, 'L', 0);
        $this->pdf->cell(30, 4, '2. Gudang Berikat', 0, 0, 'L', 0);        
        $this->pdf->cell(30, 4, '3. TPPB', 0, 0, 'L', 0);    
        $this->pdf->setX(135);
        $this->pdf->cell(30, 4, '4. TBB', 0, 0, 'L', 0);
        $this->pdf->setX(155);
        $this->pdf->cell(30, 4, '5. TLB', 0, 0, 'L', 0);        
        $this->pdf->setX(177);
        $this->pdf->cell(30, 4, '6. KDUB', 0, 0, 'L', 0);
        $this->pdf->Ln();

        // Section B
        $this->pdf->cell(40, 4, 'B . Jenis Barang', 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, '', 0, 0, 'C', 0);
        $this->pdf->cell(5, 3, $this->hdr['JNSBARANG'], 1, 0, 'C', 0);
        $this->pdf->cell(30, 4, '01. Bahan Baku', 0, 0, 'L', 0);
        $this->pdf->cell(30, 4, '02. Bahan Penolong', 0, 0, 'L', 0);
        $this->pdf->cell(30, 4, '03. Mesin/Spare Part', 0, 0, 'L', 0);        
        $this->pdf->cell(30, 4, '04. Peralatan Pabrik', 0, 0, 'L', 0); // Masih kurang waktu lama
        $this->pdf->cell(30, 4, '05. Peralatan Kantoran', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->setX(55.5);
        $this->pdf->cell(30, 4, '06. Peralatan Konstruksi', 0, 0, 'L', 0);
        $this->pdf->setX(88);
        $this->pdf->cell(30, 4, '07. Barang Reimpor', 0, 0, 'L', 0);
        $this->pdf->setX(115);
        $this->pdf->cell(30, 4, '08. Barang Contoh/Test', 0, 0, 'L', 0);
        $this->pdf->setX(147);
        $this->pdf->cell(30, 4, '09. Lainnya', 0, 0, 'L', 0);
        $this->pdf->setX(163); 
        $this->pdf->cell(30, 4, '10. Lebih dr 1 jenis Brg (1 s/d 6)', 0, 0, 'L', 0);
        $this->pdf->Ln();
        
        // Section C
        $this->pdf->cell(40, 4, 'C . Tujuan Pengiriman', 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, '', 0, 0, 'C', 0);
        $this->pdf->cell(5, 3, $this->hdr['TUJUANKIRIM'], 1, 0, 'C', 0);
        $this->pdf->cell(30, 4, '01. Ditimbun', 0, 0, 'L', 0);
        $this->pdf->cell(30, 4, '02. Diproses', 0, 0, 'L', 0);
        $this->pdf->cell(30, 4, '03. Dipinjamkan', 0, 0, 'L', 0);
        $this->pdf->cell(30, 4, '04. Disubkontrakkan', 0, 0, 'L', 0);
        $this->pdf->cell(30, 4, '05. Diperbaiki', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->setX(55.5);
        $this->pdf->cell(30, 4, '06. Pengembalian Subkontrak', 0, 0, 'L', 0);
        $this->pdf->setX(97);
        $this->pdf->cell(30, 4, '07. Pengembalian Pinjaman', 0, 0, 'L', 0);
        $this->pdf->setX(135);
        $this->pdf->cell(30, 4, '08. Pengembalian Perbaikan', 0, 0, 'L', 0);
        $this->pdf->setX(175.5);
        $this->pdf->cell(30, 4, '09. Lainnya', 0, 0, 'L', 0);
        $this->pdf->Ln();
        
        
        // Section D         
        $this->pdf->cell(40, 4, 'D. DATA PEMBERITAHUAN', 0, 0, 'L', 0);
        //PEMASOK
        $this->pdf->Rect(5.4, 41.4, 99.6, 21.5, 3.5, 'F');
        $this->pdf->Rect(105, 41.4, 101.5,21.5, 3.5, 'F');
        $this->pdf->Ln();

        $this->pdf->cell(79.6, 4, 'PEMASOK', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, $this->hdr['PASOKNEG'], 1, 0, 'C', 0);
        $this->pdf->cell(99.6, 4, 'F. DIISI OLEH BEA DAN CUKAI', 0, 0, 'L', 0);
        $this->pdf->Ln();

        $this->pdf->cell(99.6, 4, '1. Nama, Alamat, Negara', 0, 0, 'L', 0);
        $this->pdf->cell(54.8, 4, 'No. & Tgl Pendaftaran', 0, 0, 'L', 0);        
        $this->pdf->cell(23, 4, $this->hdr['PIBNO'], 1, 0, 'C', 0);
        $this->pdf->cell(1, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(22.7, 4, $this->hdr['PIBTG'], 1, 0, 'C', 0);        
        $this->pdf->Ln();
        

        $this->pdf->cell(3, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(96.6, 4, $this->hdr['PASOKNAMA'], 0, 0, 'L', 0);
        $this->pdf->cell(120, 4, 'Kantor Pabean Bongkar', 0, 0, 'L', 0);
        $this->pdf->setX(136.7);
        $this->pdf->cell(5, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(44.8, 4, substr($this->hdr['URKDKPBCBONGKAR'], 0, 22), 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, $this->hdr['KDKPBCBONGKAR'], 1, 0, 'C', 0);
        $this->pdf->Ln();

        $this->pdf->cell(3, 4, '', 0, 0, 'L', 0);
        $pskalmt = $this->trimstr($this->hdr['PASOKALMT'], 50);
        $this->pdf->cell(96.6, 4, $pskalmt[0], 0, 0, 'L', 0);
        $this->pdf->cell(120, 4, 'Kantor Pabean Pengawas', 0, 0, 'L', 0);
        $this->pdf->setX(136.7);
        $this->pdf->cell(5, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(44.8, 4, substr($this->hdr['URKDKPBCAWAS'], 0, 22), 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, $this->hdr['KDKPBCAWAS'], 1, 0, 'C', 0);

        $this->pdf->Rect(5.4, 62.9, 99.6, 19.5, 3.5, 'F');
        //$this->pdf->Rect(105, 49, 101.3, 19.5, 3.5, 'F');
        $this->pdf->Ln(6);        
        $dokInv = $this->showDok("380", $this->dok);
        $this->pdf->cell(99.6, 4, 'IMPORTIR', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '14. Invoice', 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, ':', 0, 0, 'C', 0);
        if (count($dokInv['NO']) <= 3) {
            $this->pdf->cell(50, 4, $dokInv['NO'][0], 0, 0, 'L', 0);
            $this->pdf->cell(24, 4, 'Tgl. ' . $dokInv['TG'][0], 0, 0, 'L', 0);
            $noPrinLampiranDok .= '380|'; //separator |
        } else {
            $this->pdf->cell(50, 4, '=== Lihat Lampiran ===', 0, 0, 'L', 0);
            $this->pdf->cell(24, 4, 'Tgl. ', 0, 0, 'L', 0);
        }
        $this->pdf->Ln();
        $this->pdf->cell(20, 3, ' 2. Identitas : ', 0, 0, 'L', 0);
        $this->pdf->cell(35, 3, $this->hdr['URIMPID'] . ' / ' . $this->hdr['USAHAID'], 0, 0, 'L', 0);
        
        $this->pdf->cell(69.6, 3, $this->formatNPWP($this->hdr['USAHANPWP']), 0, 0, 'L', 0);
        if (($dokInv['NO'][1] != "") || ($dokInv['TG'][1] != "")) {
            $this->pdf->cell(50, 3, $dokInv['NO'][1], 0, 0, 'L', 0);
            $this->pdf->cell(24, 3, 'Tgl. ' . $dokInv['TG'][1], 0, 0, 'L', 0);
        }
        $this->pdf->Ln();
        //$this->pdf->setX(130);
        if ($dokInv['NO'][2] != "" || $dokInv['TG'][2] != "") {
            $this->pdf->cell(50, 3, $dokInv['NO'][2], 0, 0, 'L', 0);
            $this->pdf->cell(24, 3, 'Tgl. ' . $dokInv['TG'][2], 0, 0, 'L', 0);
        }

        $this->pdf->cell(25, 4, ' 3. Nama, Alamat', 0, 0, 'L', 0);
        $this->pdf->cell(69.6, 4, ': ' . substr($this->hdr['USAHANAMA'], 0, 37), 0, 0, 'L', 0);
        $this->pdf->setX(105);        
        $this->pdf->cell(20, 4, '15. Surat Keputusan', 0, 0, 'L', 0);       
        $this->pdf->Ln();                
        $this->pdf->setX(9.5);
        $this->pdf->cell(69.6, 4, substr($this->hdr['USAHAALMT'], 0, 37), 0, 0, 'L', 0);        
        $this->pdf->setX(105);
        $dokSk = $this->showDok("911", $this->dok);
        $this->pdf->cell(5, 4, '', 0, 0, 'C', 0);
        $this->pdf->cell(20, 4, '/Persetujuan :', 0, 0, 'L', 0);
         if (count($dokSk['NO']) <= 3) {
            $this->pdf->cell(50, 4, $dokSk['NO'][0], 0, 0, 'L', 0);
            $this->pdf->cell(24, 4, 'Tgl. ' . $dokSk['TG'][0], 0, 0, 'L', 0);
            $noPrinLampiranDok .= '911|'; //separator |
        } else {
            $this->pdf->cell(50, 4, '=== Lihat Lampiran ===', 0, 0, 'L', 0);
            $this->pdf->cell(24, 4, 'Tgl. ', 0, 0, 'L', 0);
        }                        
        $this->pdf->Ln();
        $this->pdf->cell(15, 4, ' 4. Status :', 0, 0, 'L', 0);
        $this->pdf->cell(34.6, 4, $this->main->get_uraian("SELECT GROUP_CONCAT(URAIAN SEPARATOR ' ') AS UR FROM M_TABEL WHERE MODUL = 'BC23' AND KDTAB = 'STATUS_PERUSAHAAN' AND KDREC IN (" . $this->hdr['USAHASTATUS'] . ")", "UR"), 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '5.  : ' . $this->hdr['URAPIKD'], 0, 0, 'L', 0);
        $this->pdf->cell(30, 4, $this->hdr['APINO'], 0, 0, 'L', 0);
        $this->pdf->setX(105);
        $this->pdf->cell(20, 4, '      Dokumen Terkait', 0, 0, 'L', 0);
        $this->pdf->Ln();
        //$this->pdf->Rect(5.4, 45.4, 99.6, 0, 3.5, 'F');        
        if ($this->hdr['MODA'] == '1') {//BL
            $dokKD = "704|705";
        } elseif ($this->hdr['MODA'] == '4') {//AWB
            $dokKD = "740|741";
        }
        $dokBLA = $this->showDok($dokKD, $this->dok); //print_r($dokBLA);die();

        $this->pdf->Rect(5.4, 82.4, 99.6, 20, 3.5, 'F');
        $this->pdf->Rect(105, 62.8, 101.4, 39.6, 3.5, 'F');
      
        $this->pdf->setX(6);
        $this->pdf->cell(99, 4, 'PPJK', 0, 0, 'L', 0);
        $this->pdf->setX(105);
        $this->pdf->cell(20, 4,'   (BC30) :', 0, 0, 'C', 0);
        $dokSk = $this->showDok("816", $this->dok);
        $this->pdf->cell(5, 4, '', 0, 0, 'C', 0);        
         if (count($dokSk['NO']) <= 3) {
            $this->pdf->cell(50, 4, $dokSk['NO'][0], 0, 0, 'L', 0);
            $this->pdf->cell(24, 4, 'Tgl. ' . $dokSk['TG'][0], 0, 0, 'L', 0);
            $noPrinLampiranDok .= '816|'; //separator |
        } else {
            $this->pdf->cell(50, 4, '=== Lihat Lampiran ===', 0, 0, 'L', 0);
            $this->pdf->cell(24, 4, 'Tgl. ', 0, 0, 'L', 0);
        }
        $this->pdf->Ln();
        $this->pdf->cell(25, 4, ' 6. NPWP', 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, ':', 0, 0, 'C', 0);       
        $this->pdf->cell(69.6, 4, ($this->hdr['PPJKID'] == '5') ? $this->formatNPWP($this->hdr['PPJKNPWP']) : '', 0, 0, 'L', 0);
        $this->pdf->setX(105);
        $dokLC = $this->showDok("465", $this->dok);
        $this->pdf->cell(20, 4, '16. LC', 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, ':', 0, 0, 'C', 0);
        if (count($dokLC['NO']) <= 2) {
            $this->pdf->cell(50, 4, $dokLC['NO'][0], 0, 0, 'L', 0);
            $this->pdf->cell(24, 4, 'Tgl. ' . $dokLC['TG'][0], 0, 0, 'L', 0);
            $noPrinLampiranDok.='465|';
        } else {
            $this->pdf->cell(50, 4, '=== Lihat Lampiran ===', 0, 0, 'L', 0);
            $this->pdf->cell(24, 4, 'Tgl. ', 0, 0, 'L', 0);
        }        
        $this->pdf->cell(3, 4, '', 0, 0, 'L', 0);        
        //$this->pdf->Ln();
        if ($dokLC['NO'][1] != "" || $dokLC['TG'][1] != "") {
            $this->pdf->cell(25, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(50, 4, $dokLC['NO'][1], 0, 0, 'L', 0);
            $this->pdf->cell(24, 4, 'Tgl. ' . $dokLC['TG'][1], 0, 0, 'L', 0);
        }
        $this->pdf->SetX(108.4);
        $this->pdf->cell(96.4, 4, $this->hdr['URKDFAS'], 0, 0, 'L', 0);
        $this->pdf->Ln(); 
        $this->pdf->cell(25, 4, ' 7. Nama, Alamat', 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(64.6, 4, ($this->hdr['PPJKID'] == '5') ? $this->hdr['PPJKNAMA'] : '', 0, 0, 'L', 0);        
         $this->pdf->setX(105);
        $this->pdf->cell(20, 4, '17. BL/AWB', 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, ':', 0, 0, 'C', 0);
        if (count($dokBLA['NO']) <= 3) {
            $this->pdf->cell(50, 4, $dokBLA['NO'][0], 0, 0, 'L', 0);
            $this->pdf->cell(24, 4, 'Tgl. ' . $dokBLA['TG'][0], 0, 0, 'L', 0);
            $noPrinLampiranDok .= $dokKD . '|';
        } else {
            $this->pdf->cell(50, 4, '=== Lihat Lampiran ===', 0, 0, 'L', 0);
            $this->pdf->cell(24, 4, 'Tgl. ', 0, 0, 'L', 0);
        }               
        if ($dokBLA['NO'][1] != "" || $dokBLA['TG'][1] != "") {
            $this->pdf->cell(50, 4, $dokBLA['NO'][1] . 'nomorBL01', 0, 0, 'L', 0);
            $this->pdf->cell(24, 4, 'Tgl. ' . $dokBLA['TG'][1] . 'tanggalBL01', 0, 0, 'L', 0);
        }

        $this->pdf->Ln();                
        if ($dokBLA['NO'][2] != "" || $dokBLA['TG'][2] != "") {
            $this->pdf->setX(130);
            $this->pdf->cell(50, 4, $dokBLA['NO'][2], 0, 0, 'L', 0);
            $this->pdf->cell(24, 4, 'Tgl. ' . $dokBLA['TG'][2], 0, 0, 'L', 0);
        }
        $this->pdf->cell(3, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(96.6, 4, substr($this->hdr['PPJKALMT'], 0, 35), 0, 0, 'L', 0); //
        $ksdok = ($this->hdr['KDFAS'] == '') ? "" : "861|814|815|851|853|911|993|998";
        $dokSKP = $this->showDok($ksdok, $this->dok);
        if (count($dokSKP['NO']) == 1) {
            $this->pdf->cell(12, 4, 'Nomor', 0, 0, 'L', 0);
            $this->pdf->cell(62.5, 4, ': ' . $dokSKP['NO'][0], 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, 'Tgl. ' . $dokSKP['TG'][0], 0, 0, 'L', 0);
        } else {
            $this->pdf->cell(100, 4, '=== Lihat lembar lampiran ===', 0, 0, 'L', 0);
            $lampiran['dokumen'] = '1';
        }
        $this->pdf->Ln();
        $this->pdf->SetX(5.4);
        $this->pdf->cell(25, 4, ' 8. No. & Tgl Surat Izin :', 0, 0, 'L', 0);
        $this->pdf->cell(34.6, 4, ' ', 0, 0, 'C', 0);
        $this->pdf->cell(20, 4, $this->hdr['PPJKNO'], 1, 0, 'C', 0);
        $this->pdf->cell(20, 4, $this->hdr['PPJKTG'], 1, 0, 'L', 0);
        
        $this->pdf->cell(3, 4, '', 0, 0, 'L', 0);       
        $this->pdf->setX(105);
        $this->pdf->cell(20, 4, '18. ' . $this->hdr['URDOKTUPKD'], 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(13, 4, $this->hdr['DOKTUPNO'], 0, 0, 'L', 0);
        $this->pdf->cell(7.5, 4, 'Pos :', 0, 0, 'L', 0);
        $this->pdf->cell(8, 4, $this->hdr['POSNO'], 0, 0, 'L', 0);
        $this->pdf->cell(7.5, 4, 'Sub :', 0, 0, 'L', 0);
        $this->pdf->cell(14.5, 4, substr('0000' . $this->hdr['POSSUB'], -4) . '.' . substr('0000' . $this->hdr['POSSUBSUB'], -4), 0, 0, 'L', 0);
        $this->pdf->cell(30, 4, 'Tgl. ' . $this->hdr['DOKTUPTG'], 0, 0, 'L', 0);

        //MODA
        $this->pdf->Rect(5.4, 102.4, 99.6, 8, 3.5, 'F');
        $this->pdf->Rect(105, 102.4, 101.4, 8, 3.5, 'F');
        $this->pdf->Ln();
        $this->pdf->SetX(5.4);
        $this->pdf->cell(79.6, 4, ' 9. Cara Pengangkutan', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, $this->hdr['MODA'], 1, 0, 'C', 0);
        $this->pdf->cell(79.6, 4, '19. Tempat Penimbunan:', 0, 0, 'L', 0);
        $this->pdf->SetX(186.4);
        $this->pdf->cell(20, 4, $this->hdr['TMPTBN'], 1, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(35, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(69.2, 4, $this->hdr['URMODA'], 0, 0, 'L', 0);
        $this->pdf->cell(99.2, 4, $this->hdr['URTMPTBN'], 0, 0, 'L', 0);

        //Nama Sarana
        $this->pdf->Rect(5.4, 110.4, 99.6, 8, 3.5, 'F');
        $this->pdf->Rect(105, 110.4, 49.8, 8, 3.5, 'F');
        $this->pdf->Rect(154.8, 110.4, 51.6, 8, 3.5, 'F');
        $this->pdf->Ln();
        $this->pdf->SetX(5.4);
        $this->pdf->cell(79.6, 4, '10. Nama Sarana Pengangkut & No. Voy/Flight dan Bendera:', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, $this->hdr['ANGKUTFL'], 1, 0, 'C', 0);
        $this->pdf->cell(29.8, 4, '20. Valuta :', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, $this->hdr['KDVAL'], 1, 0, 'C', 0);
        $this->pdf->cell(49.8, 4, '21. NDPBM:', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(5, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(46.6, 4, $this->hdr['ANGKUTNAMA'] . '  ' . $this->hdr['ANGKUTNO'], 0, 0, 'L', 0);
        $this->pdf->cell(18, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(30, 4, $this->hdr['URANGKUTFL'], 0, 0, 'L', 0);
        $this->pdf->cell(49.8, 4, $this->hdr['URKDVAL'], 0, 0, 'R', 0);
        $this->pdf->cell(50.5, 4, number_format($this->hdr['NDPBM'], 2, '.', ','), 0, 0, 'R', 0);
        
        $this->pdf->Rect(154.7, 118.4, 51.7, 12, 3.5, 'F');
        $this->pdf->Ln();
        $this->pdf->SetX(105);                       

        //Pelabuhan
        $this->pdf->Rect(5.4, 118.4, 99.6, 12, 3.5, 'F');
        $this->pdf->SetX(5.4);
        $this->pdf->cell(30, 4, '11. Pelabuhan Muat', 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(44.6, 4, substr($this->hdr['URPELMUAT'], 0, 22), 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, $this->hdr['PELMUAT'], 1, 0, 'C', 0);
        $this->pdf->cell(49.8, 4, '22. FOB :', 0, 0, 'L', 0);
        $this->pdf->cell(0.2, 4, ($this->hdr['FOB'] != '') ? number_format($this->hdr['FOB'], 2, '.', ',') : '-', 0, 0, 'R', 0);
        $this->pdf->cell(49.8, 4, '25. Nilai CIF :', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetX(5.4);

        $this->pdf->cell(30, 4, '12. Pelabuhan Transit', 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(44.6, 4, substr($this->hdr['URPELTRANSIT'], 0, 22), 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, $this->hdr['PELTRANSIT'], 1, 0, 'C', 0);
        $this->pdf->cell(49.8, 4, '23. Freight :', 0, 0, 'L', 0);
        $this->pdf->cell(0.1, 4, ((int) $this->hdr['FREIGHT'] != 0) ? number_format($this->hdr['FREIGHT'], 2, '.', ',') : '-', 0, 0, 'R', 0);
        $this->pdf->cell(49.8, 4, ((int) $this->hdr['CIF'] != 0) ? number_format($this->hdr['CIF'], 2, '.', ',') : '-', 0, 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->SetX(5.4);
        $this->pdf->cell(30, 4, '13. Pelabuhan Bongkar', 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(44.6, 4, substr($this->hdr['URPELBKR'], 0, 22), 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, $this->hdr['PELBKR'], 1, 0, 'C', 0);

        if ($this->hdr['KDHRG'] == '1') {
            $this->pdf->cell(29.8, 4, '24. Asuransi LN/DN', 0, 0, 'L', 0);
            $this->pdf->setx(127);
            $this->pdf->cell(2, 4, ' ', 0, 0, 'L', 0);
        } elseif ($this->hdr['KDHRG'] == '2' or $this->kdhrg == '3') {
            if ($this->hdr['ASURANSI'] > 0) {
                $this->pdf->cell(29.8, 4, ' 24. Asuransi LN/DN :', 0, 0, 'L', 0);
                $this->pdf->setx(127);
                $this->pdf->cell(2, 4, '==', 0, 0, 'L', 0);
            } else {
                $this->pdf->cell(29.8, 4, ' 24. Asuransi LN/DN :', 0, 0, 'L', 0);
                $this->pdf->setx(123);
                $this->pdf->cell(7, 4, '==', 0, 0, 'L', 0);
            }
        }
        $this->pdf->cell(25.8, 4, ((int) $this->hdr['ASURANSI'] != 0) ? number_format($this->hdr['ASURANSI'], 2, '.', ',') : '-', 0, 0, 'R', 0);
        $this->pdf->cell(29.8, 4, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, number_format($this->hdr['CIFRP'], 2, '.', ','), 0, 0, 'R', 0);

        //Merk n No Peti Kemas
        $this->pdf->Rect(5.4, 130.4, 169.4, 31, 3.5, 'F');
        $this->pdf->Rect(174.8, 130.4, 31.6, 31, 3.5, 'F');
        $this->pdf->Ln();
        $this->pdf->SetX(5.4);
        $this->pdf->cell(99.6, 4, '26. Merek dan nomor kemasan/peti kemas:', 0, 0, 'L', 0);
        $this->pdf->cell(49.8, 4, '27. Jumlah dan Jenis kemasan:', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '', 1, 0, 'C', 0);
        $this->pdf->cell(29.8, 4, '28. Berat Kotor (kg) :', 0, 0, 'L', 0);
        $this->pdf->Ln();

        if (count($this->con) > 8) {
            $lampiran['kontainer'] = '1';
            $this->pdf->cell(49.8, 4, '', 0, 0, 'L', 0); // Apa ini, array??
            $this->pdf->cell(49.8, 4, '', 0, 0, 'L', 0); // Apa ini, array??
            $this->pdf->cell(99.5, 4, number_format($this->hdr['BRUTO'], 2, '.', ','), 0, 0, 'R', 0);
            $this->pdf->Ln();
            $this->pdf->cell(5, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(49.8, 4, '=== ' . count($this->con) . ' Kontainer ===', 0, 0, 'L', 0); // Apa ini, array??
            $this->pdf->cell(49.8, 4, '', 0, 0, 'L', 0); // Apa ini, array??
            $this->pdf->cell(69.8, 4, '', 0, 0, 'L', 0); 
            $this->pdf->cell(25, 4, '29. Berat Bersih (kg) :', 0, 0, 'R', 0);
            $this->pdf->Ln();
            $this->pdf->cell(5, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(49.8, 4, '=== Lihat Lembar Lampiran ===', 0, 0, 'L', 0); // Apa ini, array??
            $this->pdf->cell(49.8, 4, '', 0, 0, 'L', 0); // Apa ini, array??
            $this->pdf->cell(69.8, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, number_format($this->hdr['NETTO'], 2, '.', ','), 0, 0, 'R', 0);
            $this->pdf->Ln();
        } else {
            $this->pdf->cell(5, 4, '', 0, 0, 'L', 0);
            $i = 0;
            $this->pdf->cell(49.8, 4, $this->setnocont($this->con[$i]['CONTNO']) . ' ' . $this->con[$i]['CONTTIPE'] . ' ' . $this->con[$i]['CONTUKUR'], 0, 0, 'L', 0);
            $i = 4;
            $this->pdf->cell(49.8, 4, $this->setnocont($this->con[$i]['CONTNO']) . ' ' . $this->con[$i]['CONTTIPE'] . ' ' . $this->con[$i]['CONTUKUR'], 0, 0, 'L', 0);
            $this->pdf->cell(95, 4, number_format($this->hdr['BRUTO'], 4, '.', ','), 0, 0, 'R', 0);
            $this->pdf->Ln();
            $this->pdf->cell(5, 4, '', 0, 0, 'L', 0);
            $i = 1;
            $this->pdf->cell(49.8, 4, $this->setnocont($this->con[$i]['CONTNO']) . ' ' . $this->con[$i]['CONTTIPE'] . ' ' . $this->con[$i]['CONTUKUR'], 0, 0, 'L', 0);
            $i = 5;
            $this->pdf->cell(49.8, 4, $this->setnocont($this->con[$i]['CONTNO']) . ' ' . $this->con[$i]['CONTTIPE'] . ' ' . $this->con[$i]['CONTUKUR'], 0, 0, 'L', 0);
            $this->pdf->cell(69.8, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, '29. Berat Bersih (kg) :', 0, 0, 'R', 0);
            $this->pdf->Ln();
            $this->pdf->cell(5, 4, '', 0, 0, 'L', 0);
            $i = 2;
            $this->pdf->cell(49.8, 4, $this->setnocont($this->con[$i]['CONTNO']) . ' ' . $this->con[$i]['CONTTIPE'] . ' ' . $this->con[$i]['CONTUKUR'], 0, 0, 'L', 0);
            $i = 6;
            $this->pdf->cell(49.8, 4, $this->setnocont($this->con[$i]['CONTNO']) . ' ' . $this->con[$i]['CONTTIPE'] . ' ' . $this->con[$i]['CONTUKUR'], 0, 0, 'L', 0);
            $this->pdf->cell(69.8, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, number_format($this->hdr['NETTO'], 4, '.', ','), 0, 0, 'R', 0);
            $this->pdf->Ln();
            $this->pdf->cell(5, 4, '', 0, 0, 'L', 0);
            $i = 3;
            $this->pdf->cell(49.8, 4, $this->setnocont($this->con[$i]['CONTNO']) . ' ' . $this->con[$i]['CONTTIPE'] . ' ' . $this->con[$i]['CONTUKUR'], 0, 0, 'L', 0);
            $i = 7;
            $this->pdf->cell(49.8, 4, $this->setnocont($this->con[$i]['CONTNO']) . ' ' . $this->con[$i]['CONTTIPE'] . ' ' . $this->con[$i]['CONTUKUR'], 0, 0, 'L', 0);
            $this->pdf->cell(69.8, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(29.8, 4, '', 0, 0, 'R', 0);
        }
        $this->pdf->setxy(105, 141.5);
        if (count($this->kms) > 2) {
            $lampiran['kemasan'] = '1';
            $this->pdf->multicell(70, 4, "\n\n=== Lihat lembar lampiran ===", 0, 'C');
        } else {
            $kms = (count($this->kms) > 1) ? "\n" . $this->kms[1]['JMKEMAS'] . ' ' . $this->kms[1]['JNKEMAS'] . '/' . $this->kms[1]['URJNKEMAS'] . ' Merk: ' . $this->kms[1]['MERKKEMAS'] : '';
            $this->pdf->multicell(70, 4, $this->kms[0]['JMKEMAS'] . ' ' . $this->kms[0]['JNKEMAS'] . '/' . $this->kms[0]['URJNKEMAS'] . ' Merk: ' . $this->kms[0]['MERKKEMAS'] . $kms, 0, 'L');
        }

        //Pos Tarif
        $this->pdf->setxy(105, 157.5);
        $this->pdf->Rect(5.4, 161.4, 6, 16, 3.5, 'F');
        $this->pdf->Rect(11.4, 161.4, 68, 16, 3.5, 'F');
        $this->pdf->Rect(79.4, 161.4, 18, 16, 3.5, 'F');
        $this->pdf->Rect(97.4, 161.4, 18, 16, 3.5, 'F');              
        $this->pdf->Rect(115.4, 161.4, 30, 16, 3.5, 'F');
        $this->pdf->Rect(145.4, 161.4, 29.4, 16, 3.5, 'F');
        $this->pdf->Rect(174.8, 161.4, 31.6, 16, 3.5, 'F');
        $this->pdf->Rect(5.4, 177.4, 6, 28, 3.5, 'F');
        $this->pdf->Rect(11.4, 177.4, 68, 28, 3.5, 'F');
        $this->pdf->Rect(79.4, 177.4, 18, 28, 3.5, 'F');        
        $this->pdf->Rect(97.4, 177.4, 18, 28, 3.5, 'F');
        $this->pdf->Rect(115.4, 177.4, 30, 28, 3.5, 'F');
        $this->pdf->Rect(145.4, 177.4, 29.4, 28, 3.5, 'F');
        $this->pdf->Rect(174.8, 177.4, 31.6, 28, 3.5, 'F');
        $this->pdf->Ln();
        $this->pdf->cell(6, 4, '30.', 0, 0, 'L', 0);
        $this->pdf->cell(68, 4, '31. - Pos tarif /HS', 0, 0, 'L', 0);
        $this->pdf->cell(18, 4, '32. Kode', 0, 0, 'L', 0);
        $this->pdf->cell(18, 4, '33. Negara', 0, 0, 'L', 0);
        $this->pdf->cell(30, 4, '34. Tarif', 0, 0, 'L', 0);
        $this->pdf->cell(29.4, 4, '35. Jumlah &', 0, 0, 'L', 0);
        $this->pdf->cell(29.8, 4, '36. Jumlah Nilai CIF', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(10, 4, 'No.', 0, 0, 'L', 0);
        $this->pdf->cell(64, 4, ' - Uraian jenis dan jumlah barang secara lengkap', 0, 0, 'L', 0);
        $this->pdf->cell(18, 4, 'Penggunaan', 0, 0, 'L', 0);
        $this->pdf->cell(18, 4, '      Asal', 0, 0, 'L', 0);
        $this->pdf->cell(30, 4, ' BM, Cukai, PPN,', 0, 0, 'L', 0);
        $this->pdf->cell(29.4, 4, '    - Jenis Satuan,', 0, 0, 'L', 0);
        $this->pdf->cell(29.8, 4, '', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(10, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(64, 4, '    merk, tipe, ukuran, dan spesifikasi lainnya', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, 'Barang', 0, 0, 'L', 0);
        $this->pdf->cell(17, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(29, 4, 'PPnBM, PPh', 0, 0, 'L', 0); 
        $this->pdf->cell(32.4, 4, '   - Berat Bersih (kg)', 0, 0, 'L', 0);
        $this->pdf->cell(29.8, 4, '', 0, 0, 'L', 0);
        //------------------------------- Baru Tambahan ---------------------------------------//
        $this->pdf->Ln();
        $this->pdf->cell(10, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(75, 4, ' - Kode Barang', 0, 0, 'L', 0);
        $this->pdf->cell(18, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(30, 4, '', 0, 0, 'L', 0);        
        $this->pdf->cell(29.8, 4, '', 0, 0, 'L', 0);
        //-------------------------------------------------------------------------------------------//
        $this->pdf->SetFont('times', '', '9');
        //Isi Detail
        if (count($this->dtl) == 1) {
            $this->pdf->Ln();
            $yAWL = $this->pdf->getY();
            $this->pdf->cell(6, 4, '1', 0, 0, 'L', 0);
            $fas = ($this->dtl[0]['KDBRG'] != '') ? "\nKd Barang : " . $this->dtl[0]['KDBRG'] : '';
            $urai = $this->formaths($this->dtl[0]['NOHS']) . "\n" . $this->dtl[0]['BRGURAI'] . "\n" . $this->dtl[0]['MERK'] . " - " . $this->dtl[0]['TIPE'] . " - " . $this->dtl[0]['SPFLAIN'] . $fas;
            $xAWL = $this->pdf->getX() + 87;
            $this->pdf->multicell(68, 3, $urai, 0, 'L');
            $this->pdf->setY($yAWL);
            
            $this->pdf->setY($yAWL);
            $this->pdf->setX($xAWL);
            $this->pdf->multicell(17, 3, $this->dtl[0]['BRGASAL'] . ' / ' . $this->dtl[0]['URBRGASAL'], 0, 'L');
            $this->pdf->setY($yAWL);
            $this->pdf->setX(80);
            $this->pdf->multicell(17, 3, $this->dtl[0]['PENGGUNAAN'] . ' / ' . $this->dtl[0]['URPENGGUNAAN'], 0, 'L');
            $this->pdf->setY($yAWL);
            $this->pdf->setX($xAWL + 17);
            $FASBM = ($this->dtl[0]['FASBM'] > 0) ? $this->getfas($this->dtl[0]['KDFASBM']) . ' : ' . ($this->dtl[0]['FASBM'] / 100) . ' %' : '';
            $BM = $this->strip($this->dtl[0]['TRPBM']) . ' ' . $FASBM;
            $FASCUK = ($this->dtl[0]['FASCUK'] > 0) ? $this->getfas($this->dtl[0]['KDFASCUK']) . ' : ' . ($this->dtl[0]['FASCUK'] / 100) . ' %' : '';
            $CUK = $this->strip($this->dtl[0]['TRPCUK']) . ' ' . $FASCUK;
            $FASPPN = ($this->dtl[0]['FASPPN'] > 0) ? $this->getfas($this->dtl[0]['KDFASPPN']) . ' : ' . ($this->dtl[0]['FASPPN'] / 100) . ' %' : '';
            $PPN = $this->strip($this->dtl[0]['TRPPPN']) . ' ' . $FASPPN;
            $FASPBM = ($this->dtl[0]['FASPBM'] > 0) ? $this->getfas($this->dtl[0]['KDFASPBM']) . ' : ' . ($this->dtl[0]['FASPBM'] / 100) . ' %' : '';
            $PBM = $this->strip($this->dtl[0]['TRPPBM']) . ' ' . $FASPBM;
            $FASPPH = ($this->dtl[0]['FASPPH'] > 0) ? $this->getfas($this->dtl[0]['KDFASPPH']) . ' : ' . ($this->dtl[0]['FASPPH'] / 100) . ' %' : '';
            $PPH = $this->strip($this->dtl[0]['TRPPPH']) . ' ' . $FASPPH;
            $this->pdf->multicell(30, 3, 'BM : ' . $BM . "\nCukai : " . $CUK . "\nPPN : " . $PPN . "\nPBM : " . $PBM . "\nPPh : " . $PPH, 0, 'L');
            $this->pdf->setY($yAWL);
            $this->pdf->setX(145.5);
            $satuan = ($this->dtl[0]['KDSAT'] != '') ? ' ' . $this->dtl[0]['KDSAT'] . ' / ' . $this->dtl[0]['URKDSAT'] . ',' . ' BB: ' . number_format($this->dtl[0]['NETTODTL'], 4, '.', ',') . ' Kg' : '';
            $kms = ((int) $this->dtl[0]['KEMASJM'] > 0) ? "\n" . $this->dtl[0]['KEMASJM'] . ' ' . $this->dtl[0]['KEMASJN'] . ' / ' . $this->dtl[0]['URKEMASJN'] : '';
            $this->pdf->multicell(29.4, 3, number_format($this->dtl[0]['JMLSAT'], 4, '.', ',') . $satuan . $kms, 0, 'L');
            $this->pdf->setY($yAWL);
            $this->pdf->setX(174.8);
            $this->pdf->multicell(29.8, 3, number_format($this->dtl[0]['DCIF'], 2, '.', ','), 0, 'R');
        } else {
            $lampiran['detil'] = '1';
            $this->pdf->Ln(15);
            $this->pdf->cell(170, 4, '==== ' . count($this->dtl) . ' Jenis barang. Lihat Lembar Lanjutan ====', 0, 0, 'C', 0);
            $this->pdf->cell(29, 4, number_format($this->hdr['CIF'], 4, '.', ','), 0, 0, 'R', 0);
        }
        $pgt = array();
        foreach ($this->pgt as $a) {
            if ($a['KDBEBAN'] == '5' or $a['KDBEBAN'] == '6' or $a['KDBEBAN'] == '7') { //cukai dijadikan 1 kodebeban
                $a['KDBEBAN'] = '5';
            }
            $pgt[$a['KDBEBAN']][$a['KDFASIL']] += $a['NILBEBAN'];
            $pgt['TOTAL'][$a['KDFASIL']] += $a['NILBEBAN'];
        }

        //Pungutan
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->sety(205.5);
        $this->pdf->cell(39.2, 4, 'Jenis Pungutan', 1, 0, 'C', 0);
        $this->pdf->cell(30, 4, 'Dibayar (Rp)', 1, 0, 'C', 0); //0        
        $this->pdf->cell(30.4, 4, 'Ditangguhkan (Rp)', 1, 0, 'C', 0);//2        
        $this->pdf->setX(105);
        $this->pdf->multicell(99.6, 4, "G. UNTUK PEJABAT KANTOR PABEAN BONGKAR", 0, 'L');        
        $this->pdf->cell(5, 4, '37.', 1, 0, 'L', 0);
        $this->pdf->cell(34.2, 4, 'BM', 1, 0, 'L', 0); //1
        $this->pdf->cell(30, 4, number_format((int) $pgt[1][0], 0, '.', ','), 1, 0, 'R', 0);        
        $this->pdf->cell(30.4, 4, number_format((int) $pgt[1][2], 0, '.', ','), 1, 0, 'R', 0);       
        $this->pdf->Ln();
        $this->pdf->cell(5, 4, '38.', 1, 0, 'L', 0);
        $this->pdf->cell(34.2, 4, 'Cukai', 1, 0, 'L', 0);
        $this->pdf->cell(30, 4, number_format((int) $pgt[5][0], 0, '.', ','), 1, 0, 'R', 0);        
        $this->pdf->cell(30.4, 4, number_format((int) $pgt[5][2], 0, '.', ','), 1, 0, 'R', 0);       
        $this->pdf->Ln();
        $this->pdf->cell(5, 4, '39.', 1, 0, 'L', 0);
        $this->pdf->cell(34.2, 4, 'PPN', 1, 0, 'L', 0);
        $this->pdf->cell(30, 4, number_format((int) $pgt[2][0], 0, '.', ','), 1, 0, 'R', 0);        
        $this->pdf->cell(30.4, 4, number_format((int) $pgt[2][2], 0, '.', ','), 1, 0, 'R', 0);       
        $this->pdf->Ln();
        $this->pdf->cell(5, 4, '40.', 1, 0, 'L', 0);
        $this->pdf->cell(34.2, 4, 'PPnBM', 1, 0, 'L', 0);
        $this->pdf->cell(30, 4, number_format((int) $pgt[3][0], 0, '.', ','), 1, 0, 'R', 0);        
        $this->pdf->cell(30.4, 4, number_format((int) $pgt[3][2], 0, '.', ','), 1, 0, 'R', 0);        
        $this->pdf->Ln();
        $this->pdf->cell(5, 4, '41.', 1, 0, 'L', 0);
        $this->pdf->cell(34.2, 4, 'PPh', 1, 0, 'L', 0);
        $this->pdf->cell(30, 4, number_format((int) $pgt[4][0], 0, '.', ','), 1, 0, 'R', 0);        
        $this->pdf->cell(30.4, 4, number_format((int) $pgt[4][2], 0, '.', ','), 1, 0, 'R', 0);                
        $this->pdf->setX(105);
        $this->pdf->multicell(99.6, 4, "H. UNTUK PEMBAYARAN KE BANK/KANTOR PABEAN", 0, 'L');
        $this->pdf->cell(5, 4, '42.', 1, 0, 'L', 0);
        $this->pdf->cell(34.2, 4, 'PNBP', 1, 0, 'L', 0);
        $this->pdf->cell(30, 4, number_format((int) $pgt[8][0], 0, '.', ','), 1, 0, 'R', 0);        
        $this->pdf->cell(30.4, 4, number_format((int) $pgt[8][2], 0, '.', ','), 1, 0, 'R', 0);        
        $this->pdf->setX(105);
        $this->pdf->multicell(99.6, 4, "No Penerimaan", 0, 'L');
        $this->pdf->cell(5, 4, '43.', 1, 0, 'L', 0);
        $this->pdf->cell(34.2, 4, 'Total', 1, 0, 'L', 0);
        $this->pdf->cell(30, 4, number_format((int) $pgt['TOTAL'][0], 0, '.', ','), 1, 0, 'R', 0);        
        $this->pdf->cell(30.4, 4, number_format((int) $pgt['TOTAL'][2], 0, '.', ','), 1, 0, 'R', 0);        
        $this->pdf->Ln();
        $yAWL = $this->pdf->getY();
        $this->pdf->Rect(5.4, 233.5, 99.6, 50, 3.5, 'F');
        $this->pdf->Rect(105, 233.5, 101.4, 50, 3.5, 'F');
        $this->pdf->Rect(105, 225.5, 101.4, 8, 3.5, 'F');
        $this->pdf->Rect(105, 205.5, 101.4, 20, 3.5, 'F');
        $this->pdf->multicell(99.6, 3, "\n E. Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal\n      yang diberitahukan dalam dokumen ini.", 0, 'L');
        $imp = ($this->tp_trader == '1') ? 'Importir' : 'PPJK';
        $this->pdf->multicell(99.6, 4, "\n Jakarta, " . $this->hdr['TGLTTD'] . "\n" . $imp . "\n\n\n\n\n" . $this->hdr['NAMATTD'], 0, 'C');
        $this->pdf->SetFont('times', 'I', '9');
        $this->pdf->multicell(99.6, 4, "Tgl.Cetak " . date('d-m-Y'), 0, 'L');
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->setY($yAWL);
                
        $this->pdf->setX(105);
        $this->pdf->cell(20, 4, 'Jns. Pen', 1, 0, 'C', 0);
        $this->pdf->cell(20.5, 4, 'Kode Akun', 1, 0, 'C', 0);
        $this->pdf->cell(33, 4, 'No Tanda Pembayaran', 1, 0, 'C', 0);
        $this->pdf->cell(27.8, 4, 'Tgl.', 1, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->setX(105);
        $this->pdf->cell(20, 4, 'BM', 1, 0, 'L', 0);
        $this->pdf->cell(20.5, 4, '', 1, 0, 'L', 0);
        $this->pdf->cell(33, 4, '', 1, 0, 'L', 0);
        $this->pdf->cell(27.8, 4, '', 1, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->setX(105);
        $this->pdf->cell(20, 4, 'Cukai', 1, 0, 'L', 0);
        $this->pdf->cell(20.5, 4, '', 1, 0, 'L', 0);
        $this->pdf->cell(33, 4, '', 1, 0, 'L', 0);
        $this->pdf->cell(27.8, 4, '', 1, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->setX(105);
        $this->pdf->cell(20, 4, 'PPN', 1, 0, 'L', 0);
        $this->pdf->cell(20.5, 4, '', 1, 0, 'L', 0);
        $this->pdf->cell(33, 4, '', 1, 0, 'L', 0);
        $this->pdf->cell(27.8, 4, '', 1, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->setX(105);
        $this->pdf->cell(20, 4, 'PPnBM', 1, 0, 'L', 0);
        $this->pdf->cell(20.5, 4, '', 1, 0, 'L', 0);
        $this->pdf->cell(33, 4, '', 1, 0, 'L', 0);
        $this->pdf->cell(27.8, 4, '', 1, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->setX(105);
        $this->pdf->cell(20, 4, 'PPh', 1, 0, 'L', 0);
        $this->pdf->cell(20.5, 4, '', 1, 0, 'L', 0);
        $this->pdf->cell(33, 4, '', 1, 0, 'L', 0);
        $this->pdf->cell(27.8, 4, '', 1, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->setX(105);
        $this->pdf->cell(20, 4, 'PNBP', 1, 0, 'L', 0);
        $this->pdf->cell(20.5, 4, '', 1, 0, 'L', 0);
        $this->pdf->cell(33, 4, '', 1, 0, 'L', 0);
        $this->pdf->cell(27.8, 4, '', 1, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->setX(105);
        $yAWL = $this->pdf->getY();
        $this->pdf->multicell(49.3, 4, "Pejabat Penerima\n\n\n(...................................)", 0, 'C');
        $this->pdf->setY($yAWL);
        $this->pdf->setX(155);
        $this->pdf->multicell(49.3, 4, "Nama/Stempel Instansi\n\n\n(...................................)", 0, 'C');
        $this->pdf->setY($yAWL + 23);
        $this->pdf->cell(99.8, 4, 'Perdirjen BC No. PER-44/BC/2011 Tanggal 16 September 2011', 0, 0, 'L', 0);
        $this->pdf->cell(75, 4, 'Rangkap ke-1 / 2 / 3 untuk Kantor Pabean / BPS / BI', 0, 0, 'L', 0);
        $this->pdf->cell(24.6, 4, 'Trade to Goverment', 0, 0, 'R', 0);
        if ($lampiran['detil'] == '1') {
            $this->dataLampiranBarang();
        }
        if ($lampiran['dokumen'] == '1') {
            $this->dataLampiranDokumen($noPrinLampiranDok);
        }
        if ($lampiran['kemasan'] == '1') {
            $this->dataLampiranKemasan();
        }
        if ($lampiran['kontainer'] == '1') {
            $this->dataLampiranKontainer();
        }
    }

    function headerLampiran($tipe, $lamp) {
        $this->pdf->AddPage();
        $this->pdf->setY(5);
        $this->pdf->Rect(5.4, 17, 199.2, 12, 3.5, 'F');
        $this->pdf->SetFont('times', 'B', '10');
        $this->pdf->cell(200, 4, 'LEMBAR ' . $lamp, 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(200, 4, 'PEMBERITAHUAN IMPOR BARANG UNTUK DITIMBUN DI', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(200, 4, 'TEMPAT PENIMBUNAN BERIKAT', 0, 0, 'C', 0);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->Ln();
        $this->pdf->cell(45, 4, 'Kantor Pabean', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(103, 4, $this->hdr['URKDKPBC'], 0, 0, 'L', 0);
        $this->pdf->cell(15, 4, $this->hdr['KDKPBC'], 1, 0, 'C', 0);
        $this->pdf->cell(30, 4, " Halaman " . $this->pdf->PageNo() . " dari {nb}", 0, 0, 'R', 0);
        //$this->pdf->AliasNbPages();
        $this->pdf->Ln();
        $this->pdf->cell(45, 4, 'Nomor Pengajuan', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(80, 4, substr($this->hdr['CAR'], 0, 6) . " - " . substr($this->hdr['CAR'], 6, 6) . " - " . substr($this->hdr['CAR'], 12, 8) . " - " . substr($this->hdr['CAR'], 20), 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(45, 4, 'Nomor Pendaftaran', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(80, 4, $this->pibno . ' / ' . $this->pibtg, 0, 0, 'L', 0);
        switch ($tipe) {
            case 'barang':
                $this->pdf->Ln();
                $Yawl = $this->pdf->getY();
                $this->pdf->multicell(7, 3.5, "31.\nNo\n\n\n ", 1, 'L');
                $this->pdf->setXY($this->pdf->getX() + 7, $Yawl);
                $this->pdf->multicell(72, 3.5, "32 - Pos tarif /HS\n - Uraian barang secara lengkap meliputi jenis, jumlah,\n    merk, type, ukuran, spesifikasi lainnya\n - Jenis Fasilitas\n ", 1, 'L');
                $this->pdf->setXY($this->pdf->getX() + 79, $Yawl);
                $this->pdf->multicell(30, 3.5, "33. Negara Asal\n\n\n\n ", 1, 'L');
                $this->pdf->setXY($this->pdf->getX() + 109, $Yawl);
                $this->pdf->multicell(30, 3.5, "34. Tarif & Fasilitas\n - BM -PPN - PPnBM\n - Cukai       - PPh\n\n ", 1, 'L');
                $this->pdf->setXY($this->pdf->getX() + 139, $Yawl);
                $this->pdf->multicell(30, 3.5, "35. Jumlah & Jenis\n    satuan Barang\n - Berat bersih(Kg)\n - Jumlah & Jenis\n    Kemasan ", 1, 'L');
                $this->pdf->setXY($this->pdf->getX() + 169, $Yawl);
                $this->pdf->multicell(30.2, 3.5, "36. Jumlah Nilai CIF\n\n\n\n ", 1, 'L');
                //-------------------------------------------------------------------------------------------//
                break;
            case 'dokumen':
                $this->pdf->Ln();
                $this->pdf->cell(10, 4, '', 'LTB', 0, 'L', 0);
                $this->pdf->cell(65, 4, 'Jenis Dokumen', 'TB', 0, 'L', 0);
                $this->pdf->cell(64.2, 4, 'Nomor Dokumen', 'TB', 0, 'L', 0);
                $this->pdf->cell(60, 4, 'Tanggal Dokumen', 'TBR', 0, 'L', 0);
                $this->pdf->Ln();
                break;
            case 'kemasan':
                $this->pdf->Ln();
                $this->pdf->cell(10, 4, '', 'LTB', 0, 'L', 0);
                $this->pdf->cell(35, 4, 'Jumlah', 0, 'L', 0);
                $this->pdf->cell(79.2, 4, 'Jenis Kemasan', 'TB', 0, 'L', 0);
                $this->pdf->cell(75, 4, 'Merk Kemasan', 'TBR', 0, 'L', 0);
                $this->pdf->Ln();
                break;
            case 'kontainer':
                $this->pdf->Ln();
                $this->pdf->cell(12.6, 4, 'No.Urut', 'LTB', 0, 'C', 0);
                $this->pdf->cell(37, 4, 'Nomor Kontainer', 'TB', 0, 'L', 0);
                $this->pdf->cell(25, 4, 'Ukuran', 'TB', 0, 'L', 0);
                $this->pdf->cell(25, 4, 'Tipe', 'TBR', 0, 'L', 0);

                $this->pdf->cell(12.6, 4, 'No.Urut', 'LTB', 0, 'C', 0);
                $this->pdf->cell(37, 4, 'Nomor Kontainer', 'TB', 0, 'L', 0);
                $this->pdf->cell(25, 4, 'Ukuran', 'TB', 0, 'L', 0);
                $this->pdf->cell(25, 4, 'Tipe', 'TBR', 0, 'L', 0);
                break;
        }
    }

    function dataLampiranBarang($idPrint = 0) {
        $this->headerLampiran('barang', 'LANJUTAN BARANG');
        $this->pdf->SetFont('times', '', '8');
        $Yawl = $this->pdf->getY();
        $Y = $this->pdf->getY();
        $totData = count($this->dtl);
        for ($i = $idPrint; $i < $totData; $i++) {
            $h = '';
            $this->pdf->setY($Yawl);
            $this->pdf->multicell(7, 3.5, ($i + 1), 0, 'L');
            $h[] = $this->pdf->getY();
            //Uraiana
            $this->pdf->setXY($this->pdf->getX() + 7, $Yawl);
            $fas = ($this->dtl[$i]['KDFASDTL'] != '') ? "\nFas : " . $this->dtl[$i]['KDFASDTL'] . '/' . $this->dtl[$i]['URKDFASDTL'] : '';
            $urai = $this->formaths($this->dtl[$i]['NOHS']) . "\n" . $this->dtl[$i]['BRGURAI'] . "\n" . $this->dtl[$i]['MERK'] . " - " . $this->dtl[$i]['TIPE'] . " - " . $this->dtl[$i]['SPFLAIN'] . $fas;
            $this->pdf->multicell(72, 3.5, $urai, 0, 'L');
            $h[] = $this->pdf->getY();
            //Barang asal
            $this->pdf->setXY($this->pdf->getX() + 79, $Yawl);
            $this->pdf->multicell(30, 3.5, $this->dtl[$i]['BRGASAL'] . ' / ' . $this->dtl[$i]['URBRGASAL'], 0, 'L');
            $h[] = $this->pdf->getY();
            //Perhitungan
            $this->pdf->setXY($this->pdf->getX() + 109, $Yawl);
            $FASBM = ($this->dtl[$i]['FASBM'] > 0) ? $this->getfas($this->dtl[$i]['KDFASBM']) . ' : ' . ($this->dtl[$i]['FASBM'] / 100) . ' %' : '';
            $BM = $this->strip($this->dtl[$i]['TRPBM']) . ' ' . $FASBM;
            $FASCUK = ($this->dtl[$i]['FASCUK'] > 0) ? $this->getfas($this->dtl[$i]['KDFASCUK']) . ' : ' . ($this->dtl[$i]['FASCUK'] / 100) . ' %' : '';
            $CUK = $this->strip($this->dtl[$i]['TRPCUK']) . ' ' . $FASCUK;
            $FASPPN = ($this->dtl[$i]['FASPPN'] > 0) ? $this->getfas($this->dtl[$i]['KDFASPPN']) . ' : ' . ($this->dtl[$i]['FASPPN'] / 100) . ' %' : '';
            $PPN = $this->strip($this->dtl[$i]['TRPPPN']) . ' ' . $FASPPN;
            $FASPBM = ($this->dtl[$i]['FASPBM'] > 0) ? $this->getfas($this->dtl[$i]['KDFASPBM']) . ' : ' . ($this->dtl[$i]['FASPBM'] / 100) . ' %' : '';
            $PBM = $this->strip($this->dtl[$i]['TRPPBM']) . ' ' . $FASPBM;
            $FASPPH = ($this->dtl[$i]['FASPPH'] > 0) ? $this->getfas($this->dtl[$i]['KDFASPPH']) . ' : ' . ($this->dtl[$i]['FASPPH'] / 100) . ' %' : '';
            $PPH = $this->strip($this->dtl[$i]['TRPPPH']) . ' ' . $FASPPH;
            $this->pdf->multicell(30, 3.5, 'BM : ' . $BM . "\nCukai : " . $CUK . "\nPPN : " . $PPN . "\nPBM : " . $PBM . "\nPPh : " . $PPH, 0, 'L');
            $h[] = $this->pdf->getY();
            //kemasan
            $this->pdf->setXY($this->pdf->getX() + 139, $Yawl);
            $satuan = ($this->dtl[$i]['KDSAT'] != '') ? ' ' . $this->dtl[$i]['KDSAT'] . ' / ' . $this->dtl[$i]['URKDSAT'] . ',' . ' BB: ' . number_format($this->dtl[$i]['NETTODTL'], 4, '.', ',') . ' Kg' : '';
            $kms = ((int) $this->dtl[$i]['KEMASJM'] > 0) ? "\n" . $this->dtl[$i]['KEMASJM'] . ' ' . $this->dtl[$i]['KEMASJN'] . ' / ' . $this->dtl[$i]['URKEMASJN'] : '';
            $this->pdf->multicell(30, 3.5, number_format($this->dtl[$i]['JMLSAT'], 4, '.', ',') . $satuan . $kms, 0, 'L');
            $h[] = $this->pdf->getY();
            //Jumlah Nilain CIF
            $this->pdf->setXY($this->pdf->getX() + 169, $Yawl);
            $this->pdf->multicell(30.2, 3.5, number_format($this->dtl[$i]['DCIF'], 4, '.', ','), 0, 'R');
            $Yawl = max($h) + 2;
            if ($Yawl > 230) {
                $i++;
                break;
            }
        }
        //Rect($x, $y, $w, $h, $style='')
        $this->pdf->Rect(5.4, $Y, 7, ($Yawl - $Y));
        $this->pdf->Rect(12.4, $Y, 72, ($Yawl - $Y));
        $this->pdf->Rect(84.4, $Y, 30, ($Yawl - $Y));
        $this->pdf->Rect(114.4, $Y, 30, ($Yawl - $Y));
        $this->pdf->Rect(144.4, $Y, 30, ($Yawl - $Y));
        $this->pdf->Rect(174.6, $Y, 30.2, ($Yawl - $Y));
        $this->footerLampiran();
        if ($i < $totData) {
            $this->dataLampiranBarang($i);
        }
    }

    function dataLampiranDokumen($noPrint, $idPrint = 0) {
        $this->headerLampiran('dokumen', 'LANJUTAN DOKUMEN');
        $this->pdf->SetFont('times', '', '8');
        $Yawl = $this->pdf->getY();
        $Y = $this->pdf->getY();
        $totData = count($this->dok);
        for ($i = $idPrint; $i < $totData; $i++) {
            if (!strstr($noPrint, $this->dok[$i]['DOKKD'])) {
                $h = '';
                $this->pdf->setY($Yawl);
                //Uraian Kode Dokumen
                $this->pdf->cell(10, 4, '', 0, 0, 'L', 0);
                $this->pdf->multicell(65, 4, $this->dok[$i]['URDOKKD'], 0, 'L');
                $h[] = $this->pdf->getY();
                //Nomor Dokumen
                $this->pdf->setXY($this->pdf->getX() + 75, $Yawl);
                $this->pdf->multicell(64.2, 4, $this->dok[$i]['DOKNO'], 0, 'L');
                $h[] = $this->pdf->getY();
                //Tanggal Dokumen
                $this->pdf->setXY($this->pdf->getX() + 139.2, $Yawl);
                $this->pdf->multicell(60, 4, $this->dok[$i]['DOKTG'], 0, 'L');
                $h[] = $this->pdf->getY();

                $Yawl = max($h);
                if ($Yawl > 230) {
                    $i++;
                    break;
                }
            }
        }
        $this->pdf->Rect(5.4, $Y, 199.3, ($Yawl - $Y));
        $this->footerLampiran();
        if ($i < $totData) {
            $this->dataLampiranDokumen($noPrint, $i);
        }
    }

    function dataLampiranKemasan($idPrint = 0) {
        $this->headerLampiran('kemasan', 'LANJUTAN KEMASAN');
        $this->pdf->SetFont('times', '', '8');
        $Yawl = $this->pdf->getY();
        $Y = $this->pdf->getY(); //print_r($this->kms);die();
        $totData = count($this->kms);
        for ($i = $idPrint; $i < $totData; $i++) {
            $h = '';
            $this->pdf->setY($Yawl);
            //Uraian Kode Dokumen
            $this->pdf->cell(10, 4, '', 0, 0, 'L', 0);
            $this->pdf->multicell(35, 4, $this->kms[$i]['JMKEMAS'], 0, 'R');
            $h[] = $this->pdf->getY();
            //Nomor Dokumen
            $this->pdf->setXY($this->pdf->getX() + 45, $Yawl);
            $this->pdf->multicell(79.2, 4, $this->kms[$i]['JNKEMAS'] . ' / ' . $this->kms[$i]['URJNKEMAS'], 0, 'L');    
            $h[] = $this->pdf->getY();
            //Tanggal Dokumen
            $this->pdf->setXY($this->pdf->getX() + 124.2, $Yawl);
            $this->pdf->multicell(75, 4, $this->kms[$i]['MERKKEMAS'], 0, 'L');
            $h[] = $this->pdf->getY();
            $Yawl = max($h);
            if ($Yawl > 230) {
                $i++;
                break;
            }
        }
        $this->pdf->Rect(5.4, $Y, 199.3, ($Yawl - $Y));
        $this->footerLampiran();
        if ($i < $totData) {
            $this->dataLampiranKemasan($i);
        }
    }

    function dataLampiranKontainer($idPrint = 0) {
        $this->headerLampiran('kontainer', 'LANJUTAN KEMASAN');
        $this->pdf->SetFont('times', '', '8');
        $totData = count($this->con);
        $Y = $this->pdf->getY();
        for ($i = $idPrint; $i < $totData; $i++) {
            $this->pdf->Ln();
            $this->pdf->cell(12.6, 4, ($i + 1), 0, 0, 'C', 0);
            $this->pdf->cell(37, 4, $this->setnocont($this->con[$i]['CONTNO']), 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, $this->con[$i]['CONTUKUR'], 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, $this->con[$i]['CONTTIPE'], 0, 0, 'L', 0);
            $i++;
            $this->pdf->cell(12.6, 4, ($this->con[$i]['CONTNO'] != '') ? ($i + 1) : '', 'L', 0, 'C', 0);
            $this->pdf->cell(37, 4, $this->setnocont($this->con[$i]['CONTNO']), 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, $this->con[$i]['CONTUKUR'], 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, $this->con[$i]['CONTTIPE'], 0, 0, 'L', 0);
        }
        $this->pdf->Ln();
        $this->pdf->Rect(5.4, $Y, 199.3, ( $this->pdf->getY() - $Y));
        $this->footerLampiran();
        if ($i < $totData) {
            $this->dataLampiranKontainer($i);
        }
    }

    function footerLampiran() {
        $this->pdf->setXY(150, 255);
        $this->pdf->SetFont('times', '', '9');
        $imp = ($this->tp_trader == '1') ? 'Importir' : 'PPJK';
        $this->pdf->multicell(50, 4, "Jakarta, " . $this->hdr['TANGGAL_TTD'] . "\n" . $imp . "\n\n\n\n\n\n" . $this->hdr['NAMA_TTD'], 0, 'C');
        $this->pdf->SetFont('times', 'I', '9');
        $this->pdf->multicell(99.6, 4, "Tgl.Cetak " . date('d-m-Y'), 0, 'L');
    }
    
    function drawLembarCatPencocokan(){        
        $noPrinLampiranDok = '';
        $this->pdf->SetMargins(5.4, 0, 0);
        $this->pdf->SetAutoPageBreak(0, 0);
        $this->pdf->SetY(5.4);
        $this->pdf->SetFont('times', 'B', '12');        
        $this->pdf->cell(200, 4, 'LEMBAR LAMPIRAN III', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(200, 4, 'PEMBERITAHUAN IMPOR BARANG UNTUK DITIMBUN ', 0, 0, 'C', 0);
        $this->pdf->Ln(); 
        $this->pdf->cell(200, 4, 'DI TEMPAT PENIMBUNAN BERIKAT', 0, 0, 'C', 0);              
        $this->pdf->Ln(10);         
        $this->pdf->cell(200, 4, 'UNTUK CATATAN PENCOCOKAN', 0, 0, 'C', 0);
        $this->pdf->SetX(137);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->cell(67.6, 4, 'BC 2.3', 10, 0, 'R', 0);
        
        //KPBC
        $this->pdf->Ln(2);                 
        $this->pdf->Rect(5.4, 29.4, 199, 14, 1.5, 'F');
        $this->pdf->Ln();
        $this->pdf->cell(45, 4, 'Kantor Pabean', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(103, 4, $this->hdr['URKDKPBC'], 0, 0, 'L', 0);
        $this->pdf->cell(15, 4, $this->hdr['KDKPBC'], 1, 0, 'C', 0);                
        $this->pdf->Ln(); 
        $this->pdf->cell(45, 4, 'Nomor Pengajuan', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(80, 4, substr($this->hdr['CAR'], 0, 6) . " - " . substr($this->hdr['CAR'], 6, 6) . " - " . substr($this->hdr['CAR'], 12, 8) . " - " . substr($this->hdr['CAR'], 20), 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(45, 4, 'Nomor Pendaftaran', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(103, 4, $this->hdr[''], 0, 0, 'L', 0);
        $this->pdf->setX(73);
        $this->pdf->cell(15, 4, 'Tgl.', 0, 0, 'C', 0);               
        
        $this->pdf->Ln(7);
        $this->pdf->cell(45, 4, 'DIISI DALAM HAL DILAKUKAN  PENCOCOKAN JUMLAH & JENIS KEMASAN/PETI KEMAS', 0, 0, 'L', 0);
        $this->pdf->Ln(10);
        $this->pdf->cell(20, 4, 'PETUGAS', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(20, 4, 'NAMA', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(20, 4, 'NIP', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        
        $this->pdf->Ln();
        $this->pdf->setX(110);
        $yAWL = $this->pdf->getY();
        $this->pdf->multicell(49.3, 4, "...................................", 0, 'C');
        $this->pdf->setY($yAWL);
        $this->pdf->setX(141);
        $this->pdf->multicell(49.3, 4, "Tgl ...................................", 0, 'C');        
        $this->pdf->setX(120);        
        $this->pdf->cell(49.3, 4, 'Pejabat', 0, 0, 'L', 0);
        $this->pdf->Ln(8);
        $this->pdf->setX(120);        
        $this->pdf->cell(49.3, 4, 'ttd', 0, 0, 'L', 0);
        
        $this->pdf->Ln(10);
        $this->pdf->setX(120);
        $this->pdf->cell(20, 4, 'NAMA', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->setX(120);
        $this->pdf->cell(20, 4, 'NIP.', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        
        $this->pdf->Ln(10);
        $this->pdf->cell(45, 4, 'TEMPAT PENCOCOKAN', 0, 0, 'L', 0);              
        $this->pdf->setX(120);
        $this->pdf->cell(45, 4, 'TGL. PENCOCOKAN', 0, 0, 'L', 0);
        
        $this->pdf->Rect(5.4, 43.5, 199, 235, 1.5, 'F');
        
        $this->pdf->Ln(120);
        $this->pdf->setX(110);
        $yAWL = $this->pdf->getY();
        $this->pdf->multicell(49.3, 4, "...................................", 0, 'C');
        $this->pdf->setY($yAWL);
        $this->pdf->setX(141);
        $this->pdf->multicell(49.3, 4, "Tgl ...................................", 0, 'C');        
        $this->pdf->setX(120);        
        $this->pdf->cell(49.3, 4, 'Pemeriksa Bea dan Cukai', 0, 0, 'L', 0);
        $this->pdf->Ln(8);
        $this->pdf->setX(120);        
        $this->pdf->cell(49.3, 4, 'ttd', 0, 0, 'L', 0);
        
        $this->pdf->Ln(10);
        $this->pdf->setX(120);
        $this->pdf->cell(20, 4, 'NAMA', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->setX(120);
        $this->pdf->cell(20, 4, 'NIP', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
    }
    
    function drawLembarCatPemerikasaanFisikBrg(){
        $noPrinLampiranDok = '';
        $this->pdf->SetMargins(5.4, 0, 0);
        $this->pdf->SetAutoPageBreak(0, 0);
        $this->pdf->SetY(5.4);
        $this->pdf->SetFont('times', 'B', '12');        
        $this->pdf->cell(200, 4, 'LEMBAR LAMPIRAN IV', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(200, 4, 'PEMBERITAHUAN IMPOR BARANG UNTUK DITIMBUN ', 0, 0, 'C', 0);
        $this->pdf->Ln(); 
        $this->pdf->cell(200, 4, 'DI TEMPAT PENIMBUNAN BERIKAT', 0, 0, 'C', 0);              
        $this->pdf->Ln(10);         
        $this->pdf->cell(200, 4, 'UNTUK CATATAN PEMERIKSAAN FISIK BARANG', 0, 0, 'C', 0);
        
        $this->pdf->SetX(137);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->cell(67.6, 4, 'BC 2.3', 10, 0, 'R', 0);
        //KPBC
        $this->pdf->Ln(2);                 
        $this->pdf->Rect(5.4, 29.4, 199, 14, 1.5, 'F');
        $this->pdf->Ln();
        $this->pdf->cell(45, 4, 'Kantor Pabean', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(103, 4, $this->hdr['URKDKPBC'], 0, 0, 'L', 0);
        $this->pdf->cell(15, 4, $this->hdr['KDKPBC'], 1, 0, 'C', 0);                
        $this->pdf->Ln();
        $this->pdf->cell(45, 4, 'Nomor Pengajuan', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(80, 4, substr($this->hdr['CAR'], 0, 6) . " - " . substr($this->hdr['CAR'], 6, 6) . " - " . substr($this->hdr['CAR'], 12, 8) . " - " . substr($this->hdr['CAR'], 20), 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(45, 4, 'Nomor Pendaftaran', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(103, 4, $this->hdr[''], 0, 0, 'L', 0);
        $this->pdf->setX(73);
        $this->pdf->cell(15, 4, 'Tgl.', 0, 0, 'C', 0);               
        
        $this->pdf->Ln(7);
        $this->pdf->cell(45, 4, 'DIISI DALAM HAL DILAKUKAN  PEMERIKSAAN FISIK BARANG', 0, 0, 'L', 0);
        $this->pdf->Ln(10);
        $this->pdf->cell(20, 4, 'PETUGAS', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(20, 4, 'NAMA', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(20, 4, 'NIP', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(20, 4, 'TINGKAT PEMERIKSAAN', 0, 0, 'L', 0);        
        
        $this->pdf->Ln();
        $this->pdf->setX(110); 
        $yAWL = $this->pdf->getY();
        $this->pdf->multicell(49.3, 4, "...................................", 0, 'C');
        $this->pdf->setY($yAWL);
        $this->pdf->setX(141);
        $this->pdf->multicell(49.3, 4, "Tgl ...................................", 0, 'C');        
        $this->pdf->setX(120);        
        $this->pdf->cell(49.3, 4, 'Pejabat', 0, 0, 'L', 0);
        $this->pdf->Ln(8);
        $this->pdf->setX(120);        
        $this->pdf->cell(49.3, 4, 'ttd', 0, 0, 'L', 0);
        
        $this->pdf->Ln(10);
        $this->pdf->setX(120);
        $this->pdf->cell(20, 4, 'NAMA', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->setX(120);
        $this->pdf->cell(20, 4, 'NIP', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        
        $this->pdf->Ln(10);
        $this->pdf->cell(45, 4, 'TEMPAT PEMERIKSAAN FISIK', 0, 0, 'L', 0);              
        $this->pdf->setX(120);
        $this->pdf->cell(45, 4, 'TGL. DILAKUKAN PEMERIKSAAN FISIK BARANG', 0, 0, 'L', 0);
        $this->pdf->Ln(10   );
        $this->pdf->cell(45, 4, 'IKHTISAR PEMERIKSAAN', 0, 0, 'L', 0);
        
        $this->pdf->Rect(5.4, 43.5, 199, 235, 1.5, 'F');
        
        $this->pdf->Ln(120);
        $this->pdf->setX(110);
        $yAWL = $this->pdf->getY();
        $this->pdf->multicell(49.3, 4, "...................................", 0, 'C');
        $this->pdf->setY($yAWL);
        $this->pdf->setX(141);
        $this->pdf->multicell(49.3, 4, "Tgl ...................................", 0, 'C');        
        $this->pdf->setX(120);        
        $this->pdf->cell(49.3, 4, 'Pemeriksa Bea dan Cukai', 0, 0, 'L', 0);
        $this->pdf->Ln(8);
        $this->pdf->setX(120);        
        $this->pdf->cell(49.3, 4, 'ttd', 0, 0, 'L', 0);
        
        $this->pdf->Ln(10);
        $this->pdf->setX(120);
        $this->pdf->cell(20, 4, 'NAMA', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->setX(120);
        $this->pdf->cell(20, 4, 'NIP', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        
    }
    /* ------------------ fungsi pendukung --------------------- */

    function trimstr($strpotong, $panjang) {
        $hsl = '';
        $strpotong = trim($strpotong);
        $len = strlen($strpotong);
        $str = 0;
        while ($str < $len) {
            $hsl[] = substr($strpotong, $str, $panjang);
            $str += $panjang;
        }
        return $hsl;
    }

    function formatNPWP($npwp) {
        $strlen = strlen($npwp);
        if ($strlen == 15) {
            $npwpnya = substr($npwp, 0, 2) . "." . substr($npwp, 2, 3) . "." . substr($npwp, 5, 3) . "." . substr($npwp, 8, 1) . "-" . substr($npwp, 9, 3) . "." . substr($npwp, 12, 3);
        } else if ($strlen == 12) {
            $npwpnya = substr($npwp, 0, 2) . "." . substr($npwp, 2, 3) . "." . substr($npwp, 5, 3) . "." . substr($npwp, 8, 1) . "-" . substr($npwp, 9, 3);
        } else {
            $npwpnya = $npwp;
        }
        return $npwpnya;
    }

    function setnocont($nocont) {
        return substr($nocont, 0, 4) . '-' . substr($nocont, 4, 11);
    }

    function formaths($hs) {
        $formaths = substr($hs, 0, 4) . '.' . substr($hs, 4, 2) . '.' . substr($hs, 6, 4);
        return $formaths;
    }

    function strip($strstrip) {
        if (trim($strstrip) != 0) {
            $hasile = $strstrip . '%';
        } else {
            $hasile = ' - ';
        }
        return $hasile;
    }

    function showDok($jnDok, $arrDok) {//'380|365|861'
        $hasil = array();
        foreach ($arrDok as $a) {
            if (strstr($jnDok, $a['DOKKD'])) {
                $hasil['NO'][] = $a['DOKNO'];
                $hasil['TG'][] = $a['DOKTG'];
            }
        }
        return $hasil;
    }

    function formatDokArr($arr) {
        $hasil = '';
        foreach ($arr as $a) {
            $hasil[$a['DOKKD']]['NO'][] = $a['DOKNO'];
            $hasil[$a['DOKKD']]['TG'][] = $a['DOKTG'];
        }
        return $hasil;
    }

    function getfas($kodefas) {
        switch ($kodefas) {
            case 1:
                $kdfaspbm_nama = 'DTP';
                break;
            case 2:
                $kdfaspbm_nama = 'DTG';
                break;
            case 3:
                $kdfaspbm_nama = 'BKL';
                break;
            case 4:
                $kdfaspbm_nama = 'BBS';
                break;
        }
        return $kdfaspbm_nama;
    }

    function terbilang($bilangan) {
        $angka = array('0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
        $kata = array('', 'satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 'tujuh', 'delapan', 'sembilan');
        $tingkat = array('', 'ribu', 'juta', 'milyar', 'triliun');
        $panjang_bilangan = strlen($bilangan);

        if ($panjang_bilangan > 15) {
            $kalimat = "Diluar Batas";
            return $kalimat;
        }

        /* mengambil angka-angka yang ada dalam bilangan, dimasukkan ke dalam array */
        for ($i = 1; $i <= $panjang_bilangan; $i++) { 
            $angka[$i] = substr($bilangan, -($i), 1);
        }

        $i = 1;
        $j = 0;
        $kalimat = "";

        /* mulai proses iterasi terhadap array angka */
        while ($i <= $panjang_bilangan) {
            $subkalimat = "";
            $kata1 = "";
            $kata2 = "";
            $kata3 = "";

            /* untuk ratusan */
            if ($angka[$i + 2] != "0") {
                if ($angka[$i + 2] == "1") {
                    $kata1 = "seratus";
                } else {
                    $kata1 = $kata[$angka[$i + 2]] . " ratus";
                }
            }

            /* untuk puluhan atau belasan */
            if ($angka[$i + 1] != "0") {
                if ($angka[$i + 1] == "1") {
                    if ($angka[$i] == "0") {
                        $kata2 = "sepuluh";
                    } elseif ($angka[$i] == "1") {
                        $kata2 = "sebelas";
                    } else {
                        $kata2 = $kata[$angka[$i]] . " belas";
                    }
                } else {
                    $kata2 = $kata[$angka[$i + 1]] . " puluh";
                }
            }

            /* untuk satuan */
            if ($angka[$i] != "0") {
                if ($angka[$i + 1] != "1") {
                    $kata3 = $kata[$angka[$i]];
                }
            }

            /* pengujian angka apakah tidak nol semua, lalu ditambahkan tingkat */
            if (($angka[$i] != "0") OR ( $angka[$i + 1] != "0") OR ( $angka[$i + 2] != "0")) {
                $subkalimat = "$kata1 $kata2 $kata3 " . $tingkat[$j] . " ";
            }

            /* gabungkan variabel sub kalimat (untuk satu blok 3 angka) ke variabel kalimat */
            $kalimat = $subkalimat . $kalimat;
            $i = $i + 3;
            $j = $j + 1;
        }
        /* mengganti satu ribu jadi seribu jika diperlukan */
        if (($angka[5] == "0") AND ( $angka[6] == "0")) {
            $kalimat = str_replace("satu ribu", "seribu", $kalimat);
        }
        return trim($kalimat);
    }

    function formatTglMysql($tglMysql){
        $arr = explode('-', $tglMysql);
        return $arr[2].' '.$this->bulan[(int)$arr[1]].' '.$arr[0];
    }
    
    function formatCar($CAR){
        //$arr = explode('-', $tglMysql);
        return substr($CAR, 0,6).'-'.substr($CAR, 6,6).'-'.substr($CAR, 12,8).'-'.substr($CAR, 20,6);
    }
    
    
    function fixLen($str, $len, $chr = ' ', $alg = STR_PAD_RIGHT) {
        $hasil = str_pad(substr($str, 0, $len), $len, $chr, $alg);
        return $hasil;
    }
    
    /* ------------------ fungsi pendukung --------------------- */
   /* 
    private function isiRespon(){
        
        $this->rpt['LBLKOP']        = $this->ktr['KOP'];
        $this->rpt['LBLNODAFTAR']   = $this->res['CAR'];
        $this->rpt['LABEL33']       = $this->res['PIBTG'];
        $this->rpt['LBLTGLDAFTAR']  = substr($this->res['CAR'],18,2).'-'.substr($this->res['CAR'],16,2).'-'.substr($this->res['CAR'],12,4);
        $this->rpt['LBLDESKRIPSI']  = $this->res['DESKRIPSI'];
        
        $this->rpt['IMPNPWP']       = $this->formatNPWP($this->hdr['IMPNPWP']);
        $this->rpt['IMPNAMA']       = $this->hdr['IMPNAMA'];
        $this->rpt['IMPALMT']       = $this->hdr['IMPALMT']."\n".$this->hdr['INDALMT'];
        
        $this->rpt['PPJKNPWP']      = $this->formatNPWP($this->hdr['PPJKNPWP']);
        $this->rpt['PPJKNAMA']      = $this->hdr['PPJKNAMA'];
        $this->rpt['PPJKALMT']      = $this->hdr['PPJKALMT'];
        $this->rpt['PPJKNP']        = $this->hdr['PPJKNO'];
        $this->rpt['LBL_13']        = $this->hdr['PIBNO'];
        switch($this->res['RESKD']){
            case'011':case'110':
                if($this->res['RESKD']=='110'){
                    $this->rpt['LBLJUDUL']= 'NOTA PEMBERITAHUAN PENOLAKAN (NPP)' ;
                }
                else{
                    $this->rpt['LBLJUDUL']= 'NOTA PEMBERITAHUAN PENOLAKAN (NPP) - NSW';
                }
                $this->rpt['LABEL28'] = '';
                $this->rpt['LABEL29'] = '';
                $this->rpt['LABEL30'] = '';
                $this->rpt['LABEL31'] = '';
                break;
            case'100':case'010': //Penerimaan PIB
                if($this->res['RESKD']==='100'){
                    $this->rpt['LBLJUDUL']= 'PENERIMAAN PIB' ;
                }
                else{
                    $this->rpt['LBLJUDUL']= 'PENERIMAAN PIB - NSW';
                }
                $this->rpt['LBL_11'] = 'Diterima pada    : ';
                $this->rpt['LBL_13'] = $this->res['RESTG'].' '.$this->res['RESWK'];
                $this->rpt['LBLNARASI'] = 'Catatan      : ';
                break;
            case'200'://Penerimaan PIB 
                $this->rpt['LBLJUDUL']= 'KONFIRMASI JAMINAN' ;
                $this->rpt['LBLNARASI']= 'Sehubungan dengan belum adanya surat tanda jaminan atas PIB Saudara Nomor Pengajuan seperti di atas, dengan ini Saudara diminta agar mengirimkan / menyampaikan dokumen bukti jaminan kepada Pejabat Pengelola Jaminan' ;
                $this->rpt['LBL_11']= 'Waktu Konfirmasi  : ' ;
                $this->rpt['LBL_13'] = $this->res['RESTG'].' '.$this->res['RESWK'];
                break;
            case '205':
                $this->rpt['LBLJUDUL']= 'KONFIRMASI SURAT TANDA TERIMA JAMINAN' ;
                $this->rpt['LBLNARASI']= 'Sehubungan dengan belum adanya surat tanda terima jaminan atas PIB Saudara Nomor Pengajuan seperti di atas, dengan ini Saudara diminta agar mengirimkan / menyampaikan dokumen STTJ kepada Pejabat Pengelola Jaminan.' ;
                $this->rpt['LBL_11']= 'Waktu Konfirmasi  : ' ;
                $this->rpt['LBL_13'] = $this->res['RESTG'].' '.$this->res['RESWK'];
                break;
            case '210':
                $this->rpt['LBLJUDUL']= 'KONFIRMASI PEMBAYARAN' ;
                $this->rpt['LBLNARASI']= 'Sehubungan dengan belum adanya Surat Tanda Pembayaran atas PIB Saudara Nomor Pengajuan seperti di atas, dengan ini Saudara diminta untuk :' ;
                $this->rpt['LBLDESKRIPSI']= $this->res['DESKRIPSI'] ;
                
                $this->rpt['LBL_11']= 'Waktu Konfirmasi  : ' ;
                $this->rpt['LBL_13'] = $this->res['RESTG'].' '.$this->res['RESWK'];
                break;
            case'023':case'230':case'600':
                switch ($this->res['RESKD']){
                    case'230':$this->rpt['LBLJUDUL']= 'NOTA PEMBERITAHUAN BARANG LARANGAN/PEMBATASAN (NPBL)' ;break;
                    case'023':$this->rpt['LBLJUDUL']= 'NOTA PEMBERITAHUAN BARANG LARANGAN/PEMBATASAN (NPBL) - NSW';break;
                    case'600':$this->rpt['LBLJUDUL']= 'NOTA PEMBERITAHUAN' ;break;    
                }
                $this->rpt['LBLNARASI'] = 'Dalam PIB yang Saudara sampaikan terdapat barang yang terkena ketentuan larangan / pembatasan. Untuk itu  diminta  menyerahkan  persetujuan dari  instansi terkait dalam waktu 3 (tiga) hari sejak tanggal Nota Pemberitahuan ini.' ;
                $this->rpt['LABEL2']    = 'Pejabat Peneliti Barang Larangan/Pembatasan';
                $this->rpt['LBL_11']    = 'Nomor Pendaftaran';
                $this->rpt['LABEL33']   = date('d-m-Y', strtotime($this->hdr['PIBTG'])) ;
                break;
            case '240':
                $this->rpt['LBLJUDUL']  = 'KONFIRMASI BC 1.1' ;
                $this->rpt['LBLNARASI'] = "Sehubungan dengan belum adanya dokumen BC 1.1 atas PIB Saudara Nomor Pengajuan seperti di atas, dengan ini Saudara diminta agar mengirimkan / menyampaikan dokumen BC 1.1 kepada Pejabat: \n"
                        . " NIP         : " . $this->res['NIP1']."\n"
                        . " Nama        : " . $this->res['PEJABAT1'];
                $this->rpt['LBLDESKRIPSI'] = "Catatan : \n" . $this->res['DESKRIPSI'];
                $this->rpt['LBL_11']= 'Waktu Konfirmasi  : ' ;
                $this->rpt['LBL_13'] = $this->res['RESTG'].' '.$this->res['RESWK'];
                break;
            case '250':
                $this->rpt['LBLJUDUL']  = 'PEMBERITAHUAN PENERIMAAN PIB PENYELESAIAN' ;
                $this->rpt['LBL_11']    = 'Nomor Pendaftaran : ' ;
                $this->rpt['LABEL33']   = date('d-m-Y', strtotime($this->hdr['PIBTG'])) ;
                break;
            case '900':
                $arrJDL = explode("\n", $this->res['DESKRIPSI']);
                $this->rpt['LBLJUDUL']  = $arrJDL[0] ." \n( RESPON UMUM )";
                
                break;
        }
        $this->rpt['LBLKOTATTD']  = $this->ktr['KOTA'].', '.$this->formatTglMysql($this->res['RESTG']);
        $this->rpt['LBLNMTTD']  = $this->res['PEJABAT1'];
        $this->rpt['LBLNIPTTD']  = $this->res['NIP1'];
        
    }
    
    function drawResponNPP(){
        $this->pdf->SetMargins(5, 5, 5, 5);
        $this->pdf->SetAutoPageBreak(true, 5);
        $this->pdf->setY(5);
        $this->pdf->SetFont('courier', '', '11');
        $this->pdf->multicell(0, 5, $this->rpt['LBLKOP'], 0, 'L');
        $this->pdf->Ln(10);
        $this->pdf->SetFont('courier', 'BU', '13');
        $this->pdf->multicell(0, 6,  str_replace('  ', '', $this->rpt['LBLJUDUL']), 0, 'C');
        $this->pdf->Ln(8);
        $this->pdf->SetFont('courier', '', '10');
        $this->pdf->cell(0, 4.5, $this->rpt['LBLNOTGL'], 0, 0, 'C', 0);
        $this->pdf->Ln(6);
        $this->pdf->cell(45, 4.5, 'Nomor Pengajuan  : ', 0, 0, 'L', 0);
        $this->pdf->cell(85, 4.5, $this->formatCar($this->rpt['LBLNODAFTAR']), 0, 0, 'L', 0);
        $this->pdf->cell(24, 4.5, 'Tanggal : ', 0, 0, 'L', 0);
        $this->pdf->cell(46, 4.5, $this->rpt['LBLTGLDAFTAR'], 0, 1, 'L', 0);
        $this->pdf->cell(45, 4.5, $this->rpt['LBL_11'], 0, 0, 'L', 0);
        $this->pdf->cell(85, 4.5, $this->rpt['LBL_13'], 0, 0, 'L', 0);
        if($this->rpt['LABEL33']!=''){
            $this->pdf->cell(24, 4.5, 'Tanggal : ', 0, 0, 'L', 0);
            $this->pdf->cell(46, 4.5, $this->rpt['LABEL33'], 0, 0, 'L', 0);
        }
        $this->pdf->Ln(10);
        $this->pdf->cell(0, 6,'Kepada       :', 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'IMPORTIR' , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NPWP    : ' . $this->rpt['IMPNPWP'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Nama    : ' . $this->rpt['IMPNAMA'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Alamat  : ' . $this->rpt['IMPALMT'], 0, 1, 'L', 0);
        $this->pdf->Ln(2);
        $this->pdf->cell(0, 4.5, 'PPJK' , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NPWP    : ' . $this->rpt['PPJKNPWP'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Nama    : ' . $this->rpt['PPJKNAMA'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Alamat  : ' . $this->rpt['PPJKALMT'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NP PPJK : ' .$this->rpt['PPJKNP'], 0, 1, 'L', 0);
        $this->pdf->multicell(0, 6,$this->rpt['LBLNARASI'], 0, 'J');
        $this->pdf->Ln(2);
        $this->pdf->multicell(0, 6, str_replace("\n\n", "\n", $this->rpt['LBLDESKRIPSI']), 0, 'J');
        
        #---------------------footer----------------------
        $this->pdf->Ln();
        $this->pdf->cell(70, 5, $this->rpt['LBLKOTATTD'], 0, 0, 'L', 0);
        //$this->pdf->cell(5,  5, ',', 0, 0, 'C', 0);
        //$this->pdf->cell(40, 5, $this->rpt['LBLTGLTTD'], 1, 0, 'C', 0);
        $this->pdf->Ln(15);
        $this->pdf->cell(70, 5, $this->rpt['LABEL2'], 0, 0, 'L', 0);
        $this->pdf->Ln(10);
        $this->pdf->cell(35, 5, 'Tanda tangan', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5,  ':', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(35, 5, 'Nama', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5,  ':', 0, 0, 'L', 0);
        $this->pdf->cell(0, 5, $this->rpt['LBLNMTTD'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(35, 5, 'Nip', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5,  ':', 0, 0, 'L', 0);
        $this->pdf->cell(0, 5,  $this->rpt['LBLNIPTTD'], 0, 0, 'L', 0);
        $this->footerNPP();
    }
        
    function footerNPP(){
        $this->pdf->SetY(-25);
        $this->pdf->SetFont('courier','',10);
        $this->pdf->cell(0, 5, $this->rpt['LABEL28'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 5, $this->rpt['LABEL29'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 5, $this->rpt['LABEL30'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 5, $this->rpt['LABEL31'], 0, 0, 'L', 0);
    }
    
    function footerSPPB(){
        $this->pdf->SetY(-30);
        $this->pdf->line(5, $this->pdf->getY(), 205, $this->pdf->getY());
        $this->pdf->SetFont('courier','',10);
        $this->pdf->cell(0, 5,'Peruntukan :', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 5,'1. Importir.', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 5,'2. Pejabat yang mengawasi pengeluaran barang.', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->multicell(0, 5, 'Formulir ini dicetak secara otomatis oleh sistem komputer dan tidak memerlukan nama, tanda tangan pejabat, dan cap dinas.', 0, 'J');
    }

    function drawResponSPPB() {
        $this->pdf->SetMargins(5, 5, 5, 5);
        $this->pdf->SetAutoPageBreak(0, 10);
        $this->pdf->setY(5);
        $this->pdf->SetFont('courier', '', '11');
        $this->pdf->multicell(0, 5, $this->ktr['KOP'], 0, 'L');
        $this->pdf->Ln(10);
        $this->pdf->SetFont('courier', 'BU', '13');
        $this->pdf->cell(0, 5, 'SURAT PERSETUJUAN PENGELUARAN BARANG (SPPB)', 0, 0, 'C', 0);
        
        $this->pdf->Ln();
        $this->pdf->SetFont('courier', '', '10');
        $this->pdf->multicell(0, 4.5, 'Nomor : ' . $this->res['DOKRESNO'] . '   Tanggal : '. date('d-m-Y',strtotime($this->res['DOKRESTG'])) , 0, 'C');
        $this->pdf->Ln(8);
        $this->pdf->cell(180, 4.5, 'Nomor Pengajuan           : ' . $this->formatCar($this->hdr['CAR']), 0, 1, 'L', 0);
        $this->pdf->cell(180, 4.5, 'Nomor Pendaftaran PIB     : ' . $this->hdr['PIBNO'] . '   Tanggal : ' . date('d-m-Y',strtotime($this->hdr['PIBTG'])) , 0, 0, 'L', 0);
        $this->pdf->Ln(7);
        $this->pdf->cell(0, 4.5,'Kepada       :', 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'IMPORTIR' , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NPWP    : ' . $this->hdr['IMPNPWP'] , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Nama    : ' . $this->hdr['IMPNAMA'] , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Alamat  : ' . $this->hdr['IMPALMT'] , 0, 1, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 4.5, 'PPJK' , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NPWP    : ' . $this->hdr['PPJKNPWP'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Nama    : ' . $this->hdr['PPJKNAMA'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Alamat  : ' . $this->hdr['PPJKALMT'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NP PPJK : ' . $this->hdr['PPJKNO'], 0, 1, 'L', 0);
        $this->pdf->Ln(2);
        $this->pdf->cell(0, 4.5, 'Lokasi Barang             : ' . $this->hdr['TMPTBN'] . ' - ' . $this->main->get_uraian("SELECT URAIAN FROM m_gudang WHERE KDKPBC = '".$this->hdr['KDKPBC']."' AND KDGDG = '".$this->hdr['TMPTBN']."'",'URAIAN') , 0, 1, 'L', 0);
    if($this->hdr['MODA']=='1'){
        $dok = explode('|', $this->main->get_uraian("SELECT CONCAT(DOKNO,'|',DOKTG) AS UR FROM t_bc20dok WHERE CAR = '".$this->hdr['CAR']."' and KODE_TRADER = '".$this->hdr['KODE_TRADER']."' and DOKKD in ('705','704')",'UR'));
        $this->pdf->cell(0, 4.5, 'No. dan Tanggal B/L       : ' . $this->fixLen($dok[0],30) .'  Tanggal : '.date('d-m-Y',strtotime($dok[1])) , 0, 1, 'L', 0);
    }
    elseif($this->hdr['MODA']=='4'){
        $dok = explode('|', $this->main->get_uraian("SELECT CONCAT(DOKNO,'|',DOKTG) AS UR FROM t_bc20dok WHERE CAR = '".$this->hdr['CAR']."' and KODE_TRADER = '".$this->hdr['KODE_TRADER']."' and DOKKD in ('741','740')",'UR'));
        $this->pdf->cell(0, 4.5, 'No. dan Tanggal AWB       : ' . $this->fixLen($dok[0],30) .'  Tanggal : '.date('d-m-Y',strtotime($dok[1])) , 0, 1, 'L', 0);
    }
        $this->pdf->cell(0, 4.5, 'Sarana Pengangkut         : ' . $this->hdr['ANGKUTNAMA'] . '    ' . $this->hdr['ANGKUTNO'] , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'No Voy./Flight            : ' . $this->hdr['ANGKUTNO'] , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'No/Tgl BC 1.1             : ' . $this->fixLen($this->hdr['DOKTUPNO'],6) . '    Tanggal : ' . $this->fixLen(date('d-m-Y', strtotime($this->hdr['doktuptg'])),10) . '  Pos     : ' . $this->fixLen($this->hdr['POSNO'],4) . ' ' . $this->fixLen($this->hdr['POSSUB'],4) . ' ' . $this->fixLen($this->hdr['POSSUBSUB'],4) . ' ' , 0, 1, 'L', 0);
        
        $kms = $this->main->get_uraian("SELECT group_concat(concat(a.JMKEMAS,' ',a.JNKEMAS) SEPARATOR ';') as UR FROM t_bc20kms a WHERE a.KODE_TRADER = '".$this->hdr['KODE_TRADER']."' AND a.CAR = '".$this->hdr['CAR']."'",'UR');
        $this->pdf->cell(0, 4.5, 'Jumlah / Jenis Kemasan    : ' . $this->fixLen($kms, 31) . ' Berat   : ' . number_format($this->hdr['BRUTO'], 2, '.', ',') . ' KGM' , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'Merk Kemasan              : ' , 0, 1, 'L', 0);
        $this->pdf->Ln();
        $jmlConr = count($this->conr);
        $this->pdf->cell(0, 4.5, 'Jumlah Peti Kemas         : ' . $jmlConr , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'Nomor Peti Kemas / Ukuran : ' , 0, 1, 'L', 0);
        $arr = array(1=>array('CAPTION'=>'No.','WEIGHT'=>'10','FIELD'=>''),
                     2=>array('CAPTION'=>'No.Peti Kemas','WEIGHT'=>'30','FIELD'=>'CONTNO'),
                     3=>array('CAPTION'=>'Ukuran','WEIGHT'=>'20','FIELD'=>'CONTUKUR'),
                     4=>array('CAPTION'=>'Penegahan','WEIGHT'=>'25','FIELD'=>'CONTTIPE'),
                     5=>array('CAPTION'=>'Ket','WEIGHT'=>'10','FIELD'=>''));
        
        foreach($arr as $a){
            $this->pdf->cell($a['WEIGHT'], 5, $a['CAPTION'] , 1, 0, 'L', 0);
        }
        if($jmlConr>1){
            $this->pdf->setX($this->pdf->getX()+5);
            foreach($arr as $a){
                $this->pdf->cell($a['WEIGHT'], 5, $a['CAPTION'] , 1, 0, 'L', 0);
            }
        }
        $this->pdf->Ln();
        $akr = ($jmlConr > 14)?14:$jmlConr;
        for($i=1;$i<$akr;$i++){
            foreach($arr as $a){
                if($a['CAPTION']=='No.'){
                    $this->pdf->cell($a['WEIGHT'], 5, $i , 1, 0, 'L', 0);
                }else{
                    $this->pdf->cell($a['WEIGHT'], 5, $this->conr[$i-1][$a['FIELD']] , 1, 0, 'L', 0);
                }
            }
            if($i % 2 == 1){
                $this->pdf->setX($this->pdf->getX()+5);
            }else{
                $this->pdf->Ln();
            }
        }
        $this->pdf->Ln(2);
        $this->pdf->cell(30, 4.5, 'Catatan     : ' , 0, 0, 'L', 0);
        $this->pdf->multicell(0, 4.5, $this->res['DESKRIPSI'] , 0, 'L');
        $this->pdf->Ln(2);
        $this->pdf->cell(100, 4.5, $this->ktr['KOTA'].' , ' . date('d-m-Y',strtotime($this->res['RESTG'])) , 0, 0, 'L', 0);
        $this->pdf->cell(100, 4.5, $this->ktr['KOTA'].' , ' . date('d-m-Y',strtotime($this->res['RESTG'])), 0, 1, 'L', 0);
        $this->pdf->cell(100, 4.5, 'Pejabat Pemeriksa Dokumen', 0, 0, 'L', 0);
        $this->pdf->cell(100, 4.5, 'Pejabat yang mengawasi pengeluaran barang', 0, 0, 'L', 0);
        $this->pdf->Ln(15);
        $this->pdf->cell(100, 4.5, 'Tanda Tangan  :', 0, 0, 'L', 0);
        $this->pdf->cell(100, 4.5, 'Tanda Tangan  :', 0, 1, 'L', 0);
        $this->pdf->cell(100, 4.5, 'Nama          : ' . $this->res['PEJABAT1'], 0, 0, 'L', 0);
        $this->pdf->cell(100, 4.5, 'Nama          : ' . $this->res['NIP1'], 0, 1, 'L', 0);
        $this->pdf->cell(100, 4.5, 'NIP           : ' . $this->res['PEJABAT2'], 0, 0, 'L', 0);
        $this->pdf->cell(100, 4.5, 'NIP           : ' . $this->res['NIP2'], 0, 0, 'L', 0);
        
        $this->pdf->SetY(-30);
        $this->pdf->line(5, $this->pdf->getY(), 205, $this->pdf->getY());
        $this->pdf->SetFont('courier','',10);
        $this->pdf->cell(0, 4.5,'Peruntukan :', 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5,'1. Importir.', 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5,'2. Pejabat yang mengawasi pengeluaran barang.', 0, 1, 'L', 0);
        $this->pdf->multicell(0, 4.5, 'Formulir ini dicetak secara otomatis oleh sistem komputer dan tidak memerlukan nama, tanda tangan pejabat, dan cap dinas.', 0, 'J');
        if($jmlConr>14){
            while($i<=$jmlConr){
                $this->pdf->addPage();
                $this->pdf->SetFont('courier', '', '11');
                $this->pdf->multicell(0, 5, $this->ktr['KOP'], 0, 'L');
                $this->pdf->Ln(10);
                $this->pdf->SetFont('courier', 'BU', '13');
                $this->pdf->cell(0, 5, 'SURAT PERSETUJUAN PENGELUARAN BARANG (SPPB)', 0, 0, 'C', 0);
                $this->pdf->Ln();
                $this->pdf->SetFont('courier', '', '10');
                $this->pdf->multicell(0, 4.5, 'Nomor : ' . $this->res['DOKRESNO'] . '   Tanggal : '. date('d-m-Y',strtotime($this->res['DOKRESTG'])) , 0, 'C');
                $this->pdf->Ln(8);
                $this->pdf->cell(180, 4.5, 'Nomor Pengajuan           : ' . $this->formatCar($this->hdr['CAR']), 0, 1, 'L', 0);
                $this->pdf->cell(180, 4.5, 'Nomor Pendaftaran PIB     : ' . $this->hdr['PIBNO'] . '   Tanggal : ' . date('d-m-Y',strtotime($this->hdr['PIBTG'])) , 0, 1, 'L', 0);
                
                $awl = $i;
                $akr = ($jmlConr > $awl+79)?$awl+79: $jmlConr;
                foreach($arr as $a){
                    $this->pdf->cell($a['WEIGHT'], 5, $a['CAPTION'] , 1, 0, 'L', 0);
                }
                if(($akr - $awl)>0){
                    $this->pdf->setX($this->pdf->getX()+5);
                    foreach($arr as $a){
                        $this->pdf->cell($a['WEIGHT'], 5, $a['CAPTION'] , 1, 0, 'L', 0);
                    }
                }
                $this->pdf->Ln();
                for ($i = $awl; $i <= $akr; $i++) {
                    foreach ($arr as $a) {
                        if ($a['CAPTION'] == 'No.') {
                            $this->pdf->cell($a['WEIGHT'], 5, $i, 1, 0, 'L', 0);
                        } else {
                            $this->pdf->cell($a['WEIGHT'], 5, $this->conr[$i - 1][$a['FIELD']], 1, 0, 'L', 0);
                        }
                    }
                    if ($i % 2 == 1) {
                        $this->pdf->setX($this->pdf->getX() + 5);
                    } else {
                        $this->pdf->Ln();
                    }
                }
                
                $this->pdf->SetY(-30);
                $this->pdf->line(5, $this->pdf->getY(), 205, $this->pdf->getY());
                $this->pdf->SetFont('courier','',10);
                $this->pdf->cell(0, 4.5,'Peruntukan :', 0, 1, 'L', 0);
                $this->pdf->cell(0, 4.5,'1. Importir.', 0, 1, 'L', 0);
                $this->pdf->cell(0, 4.5,'2. Pejabat yang mengawasi pengeluaran barang.', 0, 1, 'L', 0);
                $this->pdf->multicell(0, 4.5, 'Formulir ini dicetak secara otomatis oleh sistem komputer dan tidak memerlukan nama, tanda tangan pejabat, dan cap dinas.', 0, 'J');
            }
        }
        
        
        
        
        
        //$this->footerSPPB();
    }
    
    function drawResponINP(){
        $arr = explode("\n", $this->res['DESKRIPSI']);
        $this->pdf->SetMargins(7, 5, 5);
        $this->pdf->SetAutoPageBreak(2, 10);
        $this->pdf->setY(5);
        $this->pdf->SetFont('courier', '', '11');
        $this->pdf->multicell(0, 5, $this->ktr['KOP'], 0, 'L');
        $this->pdf->Ln(4);
        $this->pdf->SetFont('courier', 'BU', '13');
        $this->pdf->cell(0, 5, 'INFORMASI NILAI PABEAN', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('courier', '', '10');
        $this->pdf->multicell(0, 5, ($this->res['DOKRESNO']=='')?'':'Nomor : '.$this->res['DOKRESNO'] , 0, 'C');
        $this->pdf->Ln(3);
        $this->pdf->cell(0, 6,'Kepada Yth.', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 6, 'PEMBELI' , 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(33, 4.5, '     NPWP    : ' , 0, 0, 'L', 0);$this->pdf->cell(0, 5, $this->hdr['IMPNPWP'] , 0, 1, 'L', 0);
        $this->pdf->cell(33, 4.5, '     Nama    : ' , 0, 0, 'L', 0);$this->pdf->cell(0, 5, $this->hdr['IMPNAMA'] , 0, 1, 'L', 0);
        $this->pdf->cell(33, 4.5, '     Alamat  : ' , 0, 0, 'L', 0);$this->pdf->multicell(0, 5, $this->hdr['IMPALMT'], 0, 'L');
        $this->pdf->cell(0, 4.5, 'PEMBERITAHU' , 0, 1, 'L', 0);
        $this->pdf->cell(33, 4.5, '     NPWP    : ' , 0, 0, 'L', 0);$this->pdf->cell(0, 5, $this->hdr['PPJKNPWP'] , 0, 1, 'L', 0);
        $this->pdf->cell(33, 4.5, '     Nama    : ' , 0, 0, 'L', 0);$this->pdf->cell(0, 5, $this->hdr['PPJKNAMA'] , 0, 1, 'L', 0);
        $this->pdf->cell(33, 4.5, '     Alamat  : ' , 0, 0, 'L', 0);$this->pdf->cell(0, 5, $this->hdr['PPJKALMT'] , 0, 1, 'L', 0);
        $this->pdf->cell(33, 4.5, '     NP PPJK : ' , 0, 0, 'L', 0);$this->pdf->cell(0, 5, $this->hdr['PPJKNO'] , 0, 1, 'L', 0);
        $this->pdf->Ln(2);
        $this->pdf->cell(0, 4.5, 'PIB ' , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '    Nomor Pengajuan    : ' . $this->formatCar($this->hdr['CAR']) , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '    Nomor Pendaftaran  : ' . $this->hdr['PIBNO'] , 0, 1, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->multicell(0, 4.5, "   Berhubung nilai pabean yang Saudara beritahukan di dalam pemberitahuan pabean impor diragukan kebenarannya, yaitu untuk jenis barang nomor urut ".$arr[0]." pada pemberitahuan pabean impor No: ".$this->res['PIBNO'] .' Tanggal : ' . date('d-m-Y', strtotime($this->res['PIBTG'])) . ", maka dengan ini diminta Saudara untuk menyerahkan Deklarasi Nilai Pabean (DNP) yang merupakan deklarasi atas fakta transaksi jual-beli/importasi yang berkaitan dengan barang yang Saudara impor disertai dengan dokumen-dokumen pendukung yang menguatkan deklarasi yang diajukan, misalnya:

a. Kontrak Penjualan (Sales Contract);
b. Kontrak (Agreement) lainnya, misalnya Royalty Agreement, Kontrak Penunjukan
   Keagenan untuk Pembelian Barang, Kontrak Penunjukan sebagai Agen/Distributor;
c. Purchase Order;
d. Letter of Credit (L/C);
e. Bukti pelunasan atas pembelian barang impor (bukti transfer uang);
f. Rekening Koran yang berkaitan dengan transaksi tersebut;
g. Bukti lain yang menyatakan kewajiban importir kepada penjual yang belum
   dipenuhi terkait dengan transaksi impor;
h. Bukti pembayaran atas barang yang sama pada penjual yang sama untuk transaksi
   sebelumnya;
i. Dokumen/bukti negosiasi terbentuknya harga;
j. Dokumen lainnya yang terkait dengan transaksi tersebut.

    DNP dan dokumen-dokumen pendukung tersebut harus diserahkan paling lambat dalam waktu 3 (tiga) hari kerja setelah tanggal pengiriman Informasi Nilai Pabean (INP) kepada Pejabat Bea dan Cukai:" , 0, 'J');
        $this->pdf->cell(0, 4.5, '            Nama                  : ' . $this->res['PEJABAT1'] , 0, 2, 'L', 0);
        $this->pdf->cell(0, 4.5, '            Nip                   : ' . $this->res['NIP1'] , 0, 2, 'L', 0);
        $this->pdf->cell(0, 4.5, '            Jabatan               : ' . $this->res['JABATAN1'] , 0, 2, 'L', 0);
        $this->pdf->cell(0, 4.5, '            Telepon dan Faxsimile : ' . $arr[1] . ' / ' . $arr[2] , 0, 2, 'L', 0);
        
        $this->pdf->multicell(0, 4.5, "   Apabila DNP dan dokumen pendukungnya tidak diserahkan dalam jangka waktu tersebut di atas, nilai pabean ditentukan berdasarkan nilai transaksi barang identik sampai dengan metode pengulangan sesuai hierarki penggunaannya." , 0, 'J');
        $this->pdf->setX($this->pdf->getX()+100);
        $this->pdf->cell(0, 4.5, $this->ktr['KOTA'] . ' , ' . $this->formatTglMysql($this->res['RESTG']) , 0, 2, 'C', 0);
        $this->pdf->cell(0, 4.5, 'Pejabat yang memeriksa dokumen' , 0, 2, 'C', 0);
        $this->pdf->ln(12);
        $this->pdf->setX($this->pdf->getX()+100);
        $this->pdf->cell(0, 4.5, $this->res['PEJABAT1'] , 0, 2, 'C', 0);
        $this->pdf->cell(0, 4.5, $this->res['NIP1'] , 0, 0, 'C', 0);
    }
    
    function drawResponSPTNP() {
        $this->pdf->SetMargins(5, 5, 5);
        $this->pdf->SetAutoPageBreak(0, 0);
        $this->pdf->setY(5);
        
        $this->pdf->SetFont('courier', '', '10');
        $this->pdf->cell(0, 5, 'KEMENTERIAN KEUANGAN REPUBLIK INDONESIA', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 5, 'DIREKTORAT JENDERAL BEA DAN CUKAI', 0, 0, 'L', 0);
        $this->pdf->Ln(15);
        $this->pdf->SetFont('courier', 'UB', '13');
        $this->pdf->cell(0, 5, 'SURAT PENETAPAN  TARIF DAN/ ATAU NILAI PABEAN (SPTNP)', 0, 0, 'C', 0);
        $this->pdf->Ln(5);
        $this->pdf->SetFont('courier', '', '11');
        $this->pdf->setX(48);
        $this->pdf->cell(0, 5, 'Nomor      :'.$this->res['DOKRESNO'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->setX(48);
        $this->pdf->cell(0, 5, 'Tanggal    :'.$this->res['DOKRESTG'], 0, 0, 'L', 0);
        
        $this->pdf->Ln(8);
        $this->pdf->SetFont('courier', '', '10');
        $this->pdf->cell(40, 5, 'Kepada Yth.', 0, 0, 'L', 0);
        $this->pdf->Ln(5);
        $this->pdf->cell(0, 5, 'Nama               : '.$this->hdr['IMPNAMA'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 5, 'Alamat             : '.$this->hdr['IMPALMT'], 0, 0, 'L', 0);
        $this->pdf->Ln(6);
        $this->pdf->cell(0, 5, 'Dengan ini diberitahukan atas Pemberitahuan Pabean Impor (PIB)   :', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(120, 5, 'Nomor Pendaftaran  : '.$this->hdr['PIBNO'], 0, 0, 'L', 0);
        $this->pdf->cell(0, 5, 'Tanggal  : '.$this->res['PIBTG'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(120, 5, 'Importir           : '.$this->hdr['IMPNAMA'], 0, 0, 'L', 0);
        $this->pdf->cell(0, 5, 'NPWP     : '.$this->formatNPWP($this->hdr['IMPNPWP']), 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(120, 5, 'PPJK               : '.$this->hdr['PPJKNAMA'], 0, 0, 'L', 0);
        $this->pdf->cell(0, 5, 'NPWP     : '.$this->formatNPWP($this->hdr['PPJKNPWP']), 0, 0, 'L', 0);
        $this->pdf->Ln(6);
        $this->pdf->multicell(200, 4, 'ditetapkan tarif dan / atau nilai pabean sehingga mengaibatkan kekurangan / kelebihan pembayaran bea masuk, pajak dalam rangka impor, dan/ atau sanksi administrasi berupa denda dengan rincian sebagai berikut :', 0, 'J');
        
        $this->pdf->Ln();
        $this->pdf->cell(40, 5, 'URAIAN', 1, 0, 'C', 0);
        $this->pdf->cell(40, 5, 'DIBERITAHUKAN', 1, 0, 'C', 0);
        $this->pdf->cell(40, 5, 'DITETAPKAN', 1, 0, 'C', 0);
        $this->pdf->cell(40, 5, 'KEKURANGAN', 1, 0, 'C', 0);
        $this->pdf->cell(40, 5, 'KELEBIHAN', 1, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40, 5, '1. Bea Cukai', 'LR', 0, 'L', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['BM_ASAL'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['BMBYR'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['BM_KURANG'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['BM_LEBIH'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40, 5, '2. Cukai', 'LR', 0, 'L', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['CUK_ASAL'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['CUKBYR'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['CUK_KURANG'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['CUK_LEBIH'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40, 5, '3. PPN', 'LR', 0, 'L', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPN_ASAL'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPNBYR'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPN_KURANG'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPN_LEBIH'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40, 5, '4. PPnBM', 'LR', 0, 'L', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPNBM_ASAL'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPNBMBYR'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPNBM_KURANG'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPNBM_LEBIH'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40, 5, '5. PPh Pasal 22', 'LR', 0, 'L', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPH_ASAL'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPHBYR'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPH_KURANG'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['PPH_LEBIH'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40, 5, '6. Denda', 'LR', 0, 'L', 0);
        $this->pdf->cell(40, 5, '0', 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, '0', 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->ntp['DENDA'], 0, '.', ','), 'LR', 0, 'R', 0);
        $this->pdf->cell(40, 5, '0', 'LR', 0, 'R', 0);
        $this->pdf->Ln();
        $total = $this->ntp['BM_KURANG'] + $this->ntp['CUK_KURANG'] + $this->ntp['PPN_KURANG'] + $this->ntp['PPNBM_KURANG'] + $this->ntp['PPH_KURANG'] + $this->ntp['DENDA'];
        $totalLbh = $this->ntp['BM_LEBIH'] + $this->ntp['CUK_LEBIH'] + $this->ntp['PPN_LEBIH'] + $this->ntp['PPNBM_LEBIH'] + $this->ntp['PPH_LEBIH'];
        $this->pdf->cell(120, 5, 'JUMLAH KEKURANGAN / KELEBIHAN PEMBAYARAN', 1, 0, 'C', 0);
        $this->pdf->cell(40, 5, number_format($total, 0, '.', ','), 1, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($totalLbh, 0, '.', ','), 1, 0, 'R', 0);
        
        $this->pdf->Ln(6);
        $this->pdf->cell(0, 4, 'Dengan rincian kesalahan sebagai berikut : ', 0, 0, 'L', 0);
        $this->pdf->Ln(6);
        
        $this->pdf->cell(60, 4, 'JENIS KESALAHAN', 1, 0, 'C', 0);
        $this->pdf->cell(0, 4, 'NOMOR URUT BARANG', 1, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(60, 4, '1. Jenis Barang', 'LTR', 0, 'L', 0);$this->pdf->cell(0, 4, $this->ntp['S_JNSBRG'], 'TR', 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(60, 4, '2. Jumlah Barang', 'LR', 0, 'L', 0);$this->pdf->cell(0, 4, $this->ntp['S_JMLBRG'], 'R', 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(60, 4, '3. Tarif', 'LR', 0, 'L', 0);$this->pdf->cell(0, 4, $this->ntp['S_TARIF'], 'R', 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(60, 4, '4. Nilai Pabean', 'LBR', 0, 'L', 0);$this->pdf->cell(0, 4, $this->ntp['S_NILPAB'], 'BR', 0, 'L', 0);
        $this->pdf->Ln(8);
        
        if($total > 0){
            $jthTemp = $this->formatTglMysql($this->res['JATUHTEMPO']);
        }else{
            $jthTemp = ' - ';
        } 
        
        $this->pdf->multicell(200, 4,'     Dalam hal terdapat kekurangan pembayaran, Saudara  wajib melunasi kekurangan pembayaran tersebut paling lambat pada tanggal ' . $jthTemp . ', dan bukti  pelunasan  agar disampaikan kepada Kepala Kantor '. $this->hdr['URKDKPBC'].'.', 0, 'J');
        $this->pdf->Ln();
        $this->pdf->multicell(200, 4, '	    Apabila tagihan tidak dilunasi atau tidak diajukan keberatan smapai dengan tanggal ' . $jthTemp . ', dikenakan bunga sebesar 2% (dua persen) setiap bulan untuk paling lama 24 (dua puluh empat) bulan dari jumlah kekurangan pembayaran, bagian bulan dihitung satu bulan penuh.', 0, 'J');
        $this->pdf->Ln();
        $this->pdf->multicell(200, 4, '     Dalam hal terdapat kelebihan pembayaran, Saudara dapat mengajukan permohonan pengembalian sesuai ketentuan peraturan perundang-undangan.', 0, 'J');
        $this->pdf->Ln();
        $this->pdf->multicell(200, 4, '     Keberatan atas penetapan ini hanya dapat diajukan secara tertulis kepada Direktur Jenderal Bea dan Cukai melalui ' . $this->hdr['URKDKPBC'] . ' sesuai dengan ketentuan tentang keberatan, paling lambat pada tanggal  ' . $jthTemp . ".", 0, 'J');
        $this->pdf->SetFont('courier', '', '11');
        $this->pdf->setXY(100,$this->pdf->getY()+10);
        $this->pdf->cell(100, 5, $this->res['JABATAN1'], 0, 0, 'C', 0);
        $this->pdf->setXY(100,$this->pdf->getY()+20);
        $this->pdf->cell(100, 5, $this->res['PEJABAT1'], 0, 0, 'C', 0);
        $this->pdf->setXY(100,$this->pdf->getY()+5);
        $this->pdf->cell(100, 5, $this->res['NIP1'], 0, 0, 'C', 0);
        $this->pdf->Ln(6);
        $this->pdf->cell(0, 5, 'STNP ini dibuat 3 (tiga) :', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 5, '- Rangkap ke-1 untuk Importir;', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 5, '- Rangkap ke-2 untuk Kepala Kantor;', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 5, '- Rangkap ke-3 untuk arsip Pejabar Bea dan Cukai.', 0, 0, 'L', 0);
    }
    
    function drawResponSPKNP(){
        $arr = explode("\n", $this->res['DESKRIPSI']);
        $this->pdf->SetMargins(7, 5, 5);
        $this->pdf->SetAutoPageBreak(2, 10);
        $this->pdf->setY(5);
        $this->pdf->SetFont('courier', '', '11');
        $this->pdf->multicell(0, 5, $this->ktr['KOP'], 0, 'L');
        $this->pdf->Ln(4);
        $this->pdf->SetFont('courier', 'BU', '13');
        $this->pdf->cell(0, 5, 'SURAT PEMBERITAHUAN KONSULTASI NILAI PABEAN', 0, 1, 'C', 0);
        $this->pdf->SetFont('courier', '', '10');
        $this->pdf->multicell(0, 5, ($this->res['DOKRESNO']=='')?'':'Nomor : '.$this->res['DOKRESNO'] , 0, 'C');
        $this->pdf->Ln(3);
        $this->pdf->cell(0, 6,'Kepada Yth.', 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'Direktur PT  : ' . $this->hdr['IMPNAMA'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'Alamat       : ' . $this->hdr['IMPALMT'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'NPWP         : ' . $this->hdr['IMPNPWP'], 0, 1, 'L', 0);
        $this->pdf->Ln(2);
        $this->pdf->multicell(0, 4.5, '    Sehubungan dengan Deklarasi Nilai Pabean (DNP) atas nilai transaksi yang diberitahukan dalam PIB:', 0, 'J');
        $this->pdf->Ln(4);
        $this->pdf->cell(0, 4.5, 'No. Aju               : ' . $this->hdr['CAR'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'No / Tgl Pendaftaran  : ' . $this->hdr['PIBNO'] . '  Tangga : ' . date('d-m-Y',strtotime($this->hdr['PIBTG'])), 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'Nomor Urut            : ' . $arr[0], 0, 1, 'L', 0);
        $this->pdf->Ln(2);
        $this->pdf->multicell(0, 4.5, '    Saudara diharapkan hadir di '.$this->ktr['URAIAN_KPBC'].' dalam waktu 2 (dua) hari kerja sejak tanggal Surat Pemberitahuan Konsultasi Nilai Pabean untuk melakukan konsultasi tentang nilai transaksi yang Saudara beritahukan dalam pemberitahuan pabean impor di atas dengan membawa data dan/atau informasi tambahan berupa :', 0, 'J');
        $this->pdf->Ln(2);
        $this->pdf->multicell(0, 4.5, str_replace($arr[0] . "\n", '', $this->res['DESKRIPSI']) , 0, 'J');
        $this->pdf->Ln(2);
        $this->pdf->multicell(0, 4.5, '    Apabila Saudara tidak hadir dalam jangka waktu tersebut, nilai pabean ditentukan berdasarkan nilai transaksi barang identik sampai dengan metode pengulangan sesuai hierarki penggunaannya.', 0, 'J');
        
        
        $this->pdf->setXY($this->pdf->getX()+100,-70);
        $this->pdf->cell(0, 4.5, $this->ktr['KOTA'] . ' , ' . $this->formatTglMysql($this->res['RESTG']) , 0, 2, 'C', 0);
        $this->pdf->cell(0, 4.5, 'Pejabat Bea dan Cukai' , 0, 2, 'C', 0);
        $this->pdf->ln(25);
        $this->pdf->setX($this->pdf->getX()+100);
        $this->pdf->cell(0, 4.5, $this->res['PEJABAT1'] , 0, 2, 'C', 0);
        $this->pdf->cell(0, 4.5, $this->res['NIP1'] , 0, 0, 'C', 0);
    }
       
    function drawResponSPJM() {
        $arr = explode("\n", $this->res['DESKRIPSI']."\n");
        $this->pdf->SetMargins(7, 5, 5);
        $this->pdf->SetAutoPageBreak(2, 10);
        $this->pdf->setY(5);
        $this->pdf->SetFont('courier', '', '11');
        $this->pdf->multicell(0, 5, $this->ktr['KOP'], 0, 'L');
        $this->pdf->Ln(4);
        $this->pdf->SetFont('courier', 'BU', '13');
        $this->pdf->cell(0, 5, ($this->res['RESKD']=='400')?'SURAT PEMBERITAHUAN JALUR MERAH (SPJM)':'SURAT PEMBERITAHUAN JALUR KUNING (SPJK)', 0, 1, 'C', 0);
        $this->pdf->SetFont('courier', '', '10');
        $this->pdf->multicell(0, 5, ($this->res['DOKRESNO']=='')?'':'Nomor : ' . $this->res['DOKRESNO'] . '   Tanggal : ' . date('d-m-Y',strtotime($this->res['DOKRESTG'])), 0, 'C');
        $this->pdf->Ln(3);
        $this->pdf->cell(0, 4.5, 'Nomor Pengajuan    : ' . $this->formatCar($this->hdr['CAR']) , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'Nomor Pendaftaran  : ' . $this->hdr['PIBNO'] , 0, 1, 'L', 0);
        $this->pdf->Ln();
        
        $this->pdf->cell(0, 4.5, 'Kepada       :'  , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'IMPORTIR'       , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NPWP    : ' . $this->hdr['IMPNPWP'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Nama    : ' . $this->hdr['IMPNAMA'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Alamat  : ' . $this->hdr['IMPALMT'], 0, 1, 'L', 0);
        $this->pdf->Ln(4);
        $this->pdf->cell(0, 4.5, 'PPJK' , 0, 0, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NPWP    : ' . $this->rpt['PPJKNPWP'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Nama    : ' . $this->rpt['PPJKNAMA'] , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Alamat  : ' . $this->rpt['PPJKALMT'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NP PPJK : ' . $this->rpt['PPJKNP'], 0, 1, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(32, 4.5, 'Lokasi Barang ', 0, 0, 'L', 0);
        $this->pdf->multicell(0, 4.5, $arr[0] , 0, 'L');
        $this->pdf->Ln();
        $this->pdf->multicell(0, 4.5, str_replace($arr[0]."\n", '', $this->res['DESKRIPSI']."\n") , 0, 'L');
        $this->pdf->Ln();
        $this->pdf->multicell(0, 4.5, ($this->res['RESKD']=='400')?"Berdasarkan  hasil penelitian  dokumen, PIB Saudara ditetapkan JALUR MERAH.  Agar Saudara menyerahkan hasil cetak PIB dan dokumen pelengkap pabean serta menyiapkan barang untuk dilakukan pemeriksaan fisik dalam jangka waktu 3 ( tiga ) hari kerja setelah tanggal SPJM ini.":"Berdasarkan  hasil penelitian  dokumen, PIB Saudara ditetapkan JALUR KUNING.  Agar Saudara menyerahkan hasil cetak PIB dan dokumen pelengkap pabean dalam jangka waktu 3 ( tiga ) hari kerja setelah tanggal SPJK ini." , 0, 'L');
        
        $this->pdf->setY(-75);
        $this->pdf->cell(0, 4.5, 'Pejabat yang menangani pelayanan pabean / ' , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'Pejabat Pemeriksa Dokumen' , 0, 1, 'L', 0);
        $this->pdf->ln(15);
        $this->pdf->cell(0, 4.5, 'Tanda Tangan     :' , 0, 1, 'L', 0);
        $this->pdf->ln();
        $this->pdf->cell(0, 4.5, 'Nama             : ' . $this->res['PEJABAT1'] , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'Nip              : ' . $this->res['NIP1'] , 0, 1, 'L', 0);
        $this->pdf->multicell(0, 4.5, "Peruntukan :
1. Importir;
2. Pejabat pemeriksa barang
Formulir ini dicetak secara otomatis oleh sistem komputer dan tidak memerlukan nama, tanda tangan pejabat, dan cap dinas." , 0, 'L');
    }
    
    function drawRespon450(){
        $this->pdf->SetMargins(5, 5, 5, 5);
        $this->pdf->SetAutoPageBreak(0, 10);
        $this->pdf->setY(5);
        $this->pdf->SetFont('courier', '', '11');
        $this->pdf->multicell(0, 5, $this->ktr['KOP'], 0, 'L');
        $this->pdf->Ln(10);
        $this->pdf->SetFont('courier', 'BU', '13');
        $this->pdf->cell(0, 5, 'SURAT PEMBERITAHUAN PEMERIKSAAN FISIK (SPPF)', 0, 0, 'C', 0);
        
        $this->pdf->Ln();
        $this->pdf->SetFont('courier', '', '10');
        $this->pdf->multicell(0, 4.5, 'Nomor : ' . $this->res['DOKRESNO'] . '   Tanggal : '. date('d-m-Y',strtotime($this->res['DOKRESTG'])) , 0, 'C');
        $this->pdf->Ln(8);
        $this->pdf->cell(180, 4.5, 'Nomor Pengajuan           : ' . $this->formatCar($this->hdr['CAR']), 0, 1, 'L', 0);
        $this->pdf->cell(180, 4.5, 'Nomor Pendaftaran PIB     : ' . $this->hdr['PIBNO'] . '   Tanggal : ' . date('d-m-Y',strtotime($this->hdr['PIBTG'])) , 0, 0, 'L', 0);
        $this->pdf->Ln(7);
        $this->pdf->cell(0, 4.5,'Kepada       :', 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'IMPORTIR' , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NPWP    : ' . $this->hdr['IMPNPWP'] , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Nama    : ' . $this->hdr['IMPNAMA'] , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Alamat  : ' . $this->hdr['IMPALMT'] , 0, 1, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 4.5, 'PPJK' , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NPWP    : ' . $this->hdr['PPJKNPWP'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Nama    : ' . $this->hdr['PPJKNAMA'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     Alamat  : ' . $this->hdr['PPJKALMT'], 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, '     NP PPJK : ' . $this->hdr['PPJKNO'], 0, 1, 'L', 0);
        $this->pdf->Ln(2);
        $this->pdf->cell(0, 4.5, 'Lokasi Barang             : ' . $this->hdr['TMPTBN'] . ' - ' . $this->main->get_uraian("SELECT URAIAN FROM m_gudang WHERE KDKPBC = '".$this->hdr['KDKPBC']."' AND KDGDG = '".$this->hdr['TMPTBN']."'",'URAIAN') , 0, 1, 'L', 0);
    if($this->hdr['MODA']=='1'){
        $dok = explode('|', $this->main->get_uraian("SELECT CONCAT(DOKNO,'|',DOKTG) AS UR FROM t_bc20dok WHERE CAR = '".$this->hdr['CAR']."' and KODE_TRADER = '".$this->hdr['KODE_TRADER']."' and DOKKD in ('705','704')",'UR'));
        $this->pdf->cell(0, 4.5, 'No. dan Tanggal B/L       : ' . $this->fixLen($dok[0],30) .'  Tanggal : '.date('d-m-Y',strtotime($dok[1])) , 0, 1, 'L', 0);
    }
    elseif($this->hdr['MODA']=='4'){
        $dok = explode('|', $this->main->get_uraian("SELECT CONCAT(DOKNO,'|',DOKTG) AS UR FROM t_bc20dok WHERE CAR = '".$this->hdr['CAR']."' and KODE_TRADER = '".$this->hdr['KODE_TRADER']."' and DOKKD in ('741','740')",'UR'));
        $this->pdf->cell(0, 4.5, 'No. dan Tanggal AWB       : ' . $this->fixLen($dok[0],30) .'  Tanggal : '.date('d-m-Y',strtotime($dok[1])) , 0, 1, 'L', 0);
    }
        $this->pdf->cell(0, 4.5, 'Sarana Pengangkut         : ' . $this->hdr['ANGKUTNAMA'] . '    ' . $this->hdr['ANGKUTNO'] , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'No Voy./Flight            : ' . $this->hdr['ANGKUTNO'] , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'No/Tgl BC 1.1             : ' . $this->fixLen($this->hdr['DOKTUPNO'],6) . '    Tanggal : ' . $this->fixLen(date('d-m-Y', strtotime($this->hdr['doktuptg'])),10) . '  Pos     : ' . $this->fixLen($this->hdr['POSNO'],4) . ' ' . $this->fixLen($this->hdr['POSSUB'],4) . ' ' . $this->fixLen($this->hdr['POSSUBSUB'],4) . ' ' , 0, 1, 'L', 0);
        
        $kms = $this->main->get_uraian("SELECT group_concat(concat(a.JMKEMAS,' ',a.JNKEMAS) SEPARATOR ';') as UR FROM t_bc20kms a WHERE a.KODE_TRADER = '".$this->hdr['KODE_TRADER']."' AND a.CAR = '".$this->hdr['CAR']."'",'UR');
        $this->pdf->cell(0, 4.5, 'Jumlah / Jenis Kemasan    : ' . $this->fixLen($kms, 31) . ' Berat   : ' . number_format($this->hdr['BRUTO'], 2, '.', ',') . ' KGM' , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'Merk Kemasan              : ' , 0, 1, 'L', 0);
        $this->pdf->Ln();
        $jmlConr = count($this->conr);
        $this->pdf->cell(0, 4.5, 'Jumlah Peti Kemas         : ' . $jmlConr , 0, 1, 'L', 0);
        $this->pdf->cell(0, 4.5, 'Nomor Peti Kemas / Ukuran : ' , 0, 1, 'L', 0);
        $arr = array(1=>array('CAPTION'=>'No.','WEIGHT'=>'10','FIELD'=>''),
                     2=>array('CAPTION'=>'No.Peti Kemas','WEIGHT'=>'30','FIELD'=>'CONTNO'),
                     3=>array('CAPTION'=>'Ukuran','WEIGHT'=>'20','FIELD'=>'CONTUKUR'),
                     4=>array('CAPTION'=>'Penegahan','WEIGHT'=>'25','FIELD'=>'CONTTIPE'),
                     5=>array('CAPTION'=>'Ket','WEIGHT'=>'10','FIELD'=>''));
        
        foreach($arr as $a){
            $this->pdf->cell($a['WEIGHT'], 5, $a['CAPTION'] , 1, 0, 'L', 0);
        }
        if($jmlConr>1){
            $this->pdf->setX($this->pdf->getX()+5);
            foreach($arr as $a){
                $this->pdf->cell($a['WEIGHT'], 5, $a['CAPTION'] , 1, 0, 'L', 0);
            }
        }
        $this->pdf->Ln();
        $akr = ($jmlConr > 14)?14:$jmlConr;
        for($i=1;$i<$akr;$i++){
            foreach($arr as $a){
                if($a['CAPTION']=='No.'){
                    $this->pdf->cell($a['WEIGHT'], 5, $i , 1, 0, 'L', 0);
                }else{
                    $this->pdf->cell($a['WEIGHT'], 5, $this->conr[$i-1][$a['FIELD']] , 1, 0, 'L', 0);
                }
            }
            if($i % 2 == 1){
                $this->pdf->setX($this->pdf->getX()+5);
            }else{
                $this->pdf->Ln();
            }
        }
        $this->pdf->setY(-93);
        $this->pdf->multicell(0, 4.5, "Petunjuk : 
1. Importir;
2. Pejabat pemeriksa barang;
3. Pejabat yang mengawasi pengeluaran barang.
Formulir ini dicetak secara otomatis  oleh sistem komputer dan tidak memerlukan nama, tanda tangan pejabat, dan cap dinas.

Diberitahukan  bahwa dari hasil penelitian dokumen, terhadap barang dalam PIB dengan nomor pendaftaran  tersebut di atas disetujui untuk  dikeluarkan dengan pemeriksaan fisik di tempat Saudara, dengan ketentuan sebagai berikut:
1. Dilakukan penyegelan dan/atau pengawalan oleh Pejabat Bea dan Cukai;
2. Wajib memberikan bantuan yang layak kepada Pejabat Bea dan Cukai yang melaksanakan.

Pejabat Pemeriksa Dokumen


Tanda Tangan :
Nama        : " . $this->res['PEJABAT1'] . "  
NIP         : " . $this->res['NIP1'], 0, 'L');
        if($jmlConr>14){
            while($i<=$jmlConr){
                $this->pdf->addPage();
                $this->pdf->SetFont('courier', '', '11');
                $this->pdf->multicell(0, 5, $this->ktr['KOP'], 0, 'L');
                $this->pdf->Ln(10);
                $this->pdf->SetFont('courier', 'BU', '13');
                $this->pdf->cell(0, 5, 'SURAT PEMBERITAHUAN PEMERIKSAAN FISIK (SPPF)', 0, 0, 'C', 0);
                $this->pdf->Ln();
                $this->pdf->SetFont('courier', '', '10');
                $this->pdf->multicell(0, 4.5, 'Nomor : ' . $this->res['DOKRESNO'] . '   Tanggal : '. date('d-m-Y',strtotime($this->res['DOKRESTG'])) , 0, 'C');
                $this->pdf->Ln(8);
                $this->pdf->cell(180, 4.5, 'Nomor Pengajuan           : ' . $this->formatCar($this->hdr['CAR']), 0, 1, 'L', 0);
                $this->pdf->cell(180, 4.5, 'Nomor Pendaftaran PIB     : ' . $this->hdr['PIBNO'] . '   Tanggal : ' . date('d-m-Y',strtotime($this->hdr['PIBTG'])) , 0, 1, 'L', 0);
                
                $awl = $i;
                $akr = ($jmlConr > $awl+79)?$awl+79: $jmlConr;
                foreach($arr as $a){
                    $this->pdf->cell($a['WEIGHT'], 5, $a['CAPTION'] , 1, 0, 'L', 0);
                }
                if(($akr - $awl)>0){
                    $this->pdf->setX($this->pdf->getX()+5);
                    foreach($arr as $a){
                        $this->pdf->cell($a['WEIGHT'], 5, $a['CAPTION'] , 1, 0, 'L', 0);
                    }
                }
                $this->pdf->Ln();
                for ($i = $awl; $i <= $akr; $i++) {
                    foreach ($arr as $a) {
                        if ($a['CAPTION'] == 'No.') {
                            $this->pdf->cell($a['WEIGHT'], 5, $i, 1, 0, 'L', 0);
                        } else {
                            $this->pdf->cell($a['WEIGHT'], 5, $this->conr[$i - 1][$a['FIELD']], 1, 0, 'L', 0);
                        }
                    }
                    if ($i % 2 == 1) {
                        $this->pdf->setX($this->pdf->getX() + 5);
                    } else {
                        $this->pdf->Ln();
                    }
                }
                
                $this->pdf->setY(-30);
                $this->pdf->cell(100, 4.5, 'Pejabat yang memeriksa dokumen I/II' , 0, 0, 'L', 0);
                $this->pdf->cell(100, 4.5, 'Pejabat yang melaksanakan pengeluaran barang:' , 0, 1, 'L', 0);
                $this->pdf->cell(100, 4.5, 'Tanda Tangan : .............................' , 0, 0, 'L', 0);
                $this->pdf->cell(100, 4.5, 'Tanda Tangan : .............................' , 0, 1, 'L', 0);
                $this->pdf->cell(100, 4.5, 'Nama         : ' . $this->res['PEJABAT1'] , 0, 0, 'L', 0);
                $this->pdf->cell(100, 4.5, 'Nama         : ' . $this->res['PEJABAT2'] , 0, 1, 'L', 0);
                $this->pdf->cell(100, 4.5, 'NIP          : ' . $this->res['NIP1'], 0, 0, 'L', 0);
                $this->pdf->cell(100, 4.5, 'NIP          : ' . $this->res['NIP2'], 0, 1, 'L', 0);
                $this->pdf->line(5,$this->pdf->getY(),205,$this->pdf->getY());
                $this->pdf->cell(0, 4.5, 'Lembar 1 Untuk DJBC' . $this->res['NIP2'], 0, 1, 'L', 0);
            }
        }
    #=====================================================
    }
    */
}