<?php
if (!defined('BASEPATH')){exit('No direct script access allowed');}
class dokBC11 extends CI_Model {
    var $main = '';      #load actMain
    var $pdf = '';       #load fpdf library
    var $nPungutan = ''; #pungutan
    var $car = '';       #Car
    var $hdr = '';       #data header dalam array 1D
    var $pos = '';       #data header dalam array 1D
    var $res = '';       #data header dalam array 1D
    var $dok = '';       #data dokumen dalam array 2D
    var $con = '';       #data container dalam array 2D    
    var $kms = '';       #data kemasan dalam array 2D
    var $dtl = '';       #data detil dalam array 2D
    var $pgt = '';       #data pungutan dalam array 2D
    var $ntb = '';       #data ntb dalam array 1D
    var $npt = '';       #data ntp dalam array 1D
    var $ktr = '';       #data kantor kpbc dalam array 1D
    var $rpt = '';       #data caption respon NPP
    var $kd_trader = ''; #session data kode trader
    var $tp_trader = ''; #session data tipe trader
    var $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
    var $segmentUri = array(); #segment url

    function fpdf($pdf) {
        $this->pdf = $pdf;
    }

    function ciMain($main) {
        $this->main = $main;
    }

    function segmentUri($arr) {
        $this->segmentUri = $arr;
    }

    function showPage($car, $jnPage) {
        $this->car = $car;
        $this->kd_trader = $this->newsession->userdata('KODE_TRADER');
        $this->tp_trader = $this->newsession->userdata('TIPE_TRADER');
        $this->pdf->AliasNbPages('{nb}');
        switch ($jnPage) {
            case'1':
                $this->getHDR();
                $this->getDTL();
                $this->getPOS();
                $this->perPos();
                break;
            case'2':
                $this->getHDR();
                $this->getDTL($this->segmentUri[5]);
                $this->getPOS($this->segmentUri[5]);
                $this->perPos();
                break;
            default :
                $this->pdf->SetFont('times', 'B', '12');
                $this->pdf->cell(131.6, 4, 'Tidak ada jenis laporan ini', 0, 0, 'R', 0);
                break;
        }
        $this->pdf->Output();
    }

    function getHDR() {
        $SQL = "SELECT A.*, "
                       . " DATE_FORMAT(A.TGBC10,'%d-%m-%Y') as TGBC10, "
                       . " DATE_FORMAT(A.TGBC11,'%d-%m-%Y') as TGBC11, "
                       . " DATE_FORMAT(A.TGTIBABERANGKAT,'%d-%m-%Y') as TGTIBABERANGKAT, "
                       . " B.URAIAN AS 'URJNSMANIFEST', "
                       . " D.URAIAN_NEGARA as 'URVOYFLAG', "
                       . " E.URAIAN_PELABUHAN as 'URPELMUAT', "
                       . " F.URAIAN_PELABUHAN as 'URPELTRANSIT', "
                       . " G.URAIAN_PELABUHAN as 'URPELBKR', "
                       . " H.URAIAN_PELABUHAN as 'URPELNEXT', "
                       . " I.URAIAN_KPBC as 'URKPBC' "
                . "FROM T_BC11HDR A  LEFT JOIN m_tabel B ON A.JNSMANIFEST = B.KDREC AND B.MODUL = 'BC11' "
                                  . "LEFT JOIN m_negara D ON D.KODE_NEGARA = A.VOYFLAG "
                                  . "LEFT JOIN m_pelabuhan E ON E.KODE_PELABUHAN = A.PELMUAT "
                                  . "LEFT JOIN m_pelabuhan F ON F.KODE_PELABUHAN = A.PELTRANSIT "
                                  . "LEFT JOIN m_pelabuhan G ON G.KODE_PELABUHAN = A.PELBKR "
                                  . "LEFT JOIN m_pelabuhan H ON H.KODE_PELABUHAN = A.PELNEXT "
                                  . "LEFT JOIN m_kpbc I ON I.KDKPBC = A.KPBC "
                 . "WHERE CAR = '" . $this->car . "' AND KODE_TRADER = '" . $this->kd_trader . "'";
        $this->hdr = $this->main->get_result($SQL);
        #print_r($this->hdr);die();
    }

    function getCON() {
        #array container
        $SQL = "SELECT a.* FROM t_bc11con a WHERE a.KODE_TRADER = '" .  $this->kd_trader. "' AND a.CAR = '" . $this->car . "'";
        $con = $this->actMain->get_result($SQL, true);
        foreach ($con as $a){
            $this->con[$a['KDGRUP']][$a['NOPOS']][$a['NOPOSSUB']][$a['NOPOSSUBSUB']][$a['SERI']]['NOCONT']     = $a['NOCONT'];
            $this->con[$a['KDGRUP']][$a['NOPOS']][$a['NOPOSSUB']][$a['NOPOSSUBSUB']][$a['SERI']]['UKCONT']     = $a['UKCONT'];
            $this->con[$a['KDGRUP']][$a['NOPOS']][$a['NOPOSSUB']][$a['NOPOSSUBSUB']][$a['SERI']]['TIPECONT']   = $a['TIPECONT'];
            $this->con[$a['KDGRUP']][$a['NOPOS']][$a['NOPOSSUB']][$a['NOPOSSUBSUB']][$a['SERI']]['PARTIAL']    = $a['PARTIAL'];
        }
        #print_r($this->con);die();
    }

    function getDTL($Filter='') {
        if($Filter!=''){
            $addQuery =  " AND a.KDGRUP = '" . $Filter ."'";
        }
        $SQL = "SELECT 	a.NOPOS, a.KDGRUP,
		CONCAT('No. BL : ', a.NOBL, '\nTg. BL : ', date_format(a.TGBL,'%d-%m-%Y'), '\nMother Vessel : ', a.MOTHERVESSEL) AS 'URBL',
		CONCAT(	'Shipper Name : ', a.NMPENGIRIM , '\n',
                        'Shipper Address : ', a.ALPENGIRIM, '\n\n',
                        'Consignee Name : ', a.NMPENERIMA , '\n',
                        'Consignee Address : ', a.ALPENERIMA , '\n\n',
                        'Notify Name : ', a.NMPEMBERITAU , '\n',
                        'Notify Address : ', a.ALPEMBERITAU) AS 'URNM',
		CONCAT(	a.MERKKEMAS, '\n\n', 
                        a.JMLKEMAS, ' ', 
                        a.JNSKEMAS, '/', 
                        b.URAIAN_KEMASAN, '\n\n',
                        '** ', COUNT(c.SERI) , ' Kontainer **\n', 
                                GROUP_CONCAT(Concat(c.NOCONT, '; ', c.UKCONT,'\"', IF(c.TIPECONT='F','FCL', IF(c.TIPECONT='L','LCL', IF(c.TIPECONT='E','Empty', ''))), ' Seal Number : ', c.SEALNUM) SEPARATOR '\n')) AS 'URMR',
		CONCAT(	a.URBRG, '\n\nBruto Total : ', a.BRUTO, ' Kgm\nVolume Total : ', a.VOLUME ,' m³') as 'URBR',
		CONCAT(	a.DPELASAL ,'/', d.URAIAN_PELABUHAN, '\n',
				a.DPELSEBELUM ,'/', e.URAIAN_PELABUHAN , '\n',
				a.DPELBONGKAR ,'/', f.URAIAN_PELABUHAN , '\n',
				a.DPELLANJUT ,'/', g.URAIAN_PELABUHAN , '\n',
				GROUP_CONCAT(if(h.DOKNO is null, '' , CONCAT('No ', i.URAIAN, ' : ', h.DOKNO, '\nTgl : ', date_format(h.DOKTG, '%d-%m-%Y'), '\nKPBC : ', h.DOKKPBC, '\n')) SEPARATOR '' )) AS 'URPL'
FROM t_bc11dtl a LEFT JOIN m_kemasan b ON a.JNSKEMAS = b.KODE_KEMASAN 
				 LEFT JOIN t_bc11con c ON 	a.KODE_TRADER = c.KODE_TRADER AND a.CAR = c.CAR AND a.KDGRUP = c.KDGRUP AND a.NOPOS = c.NOPOS AND a.NOPOSSUB = c.NOPOSSUB AND a.NOPOSSUBSUB = c.NOPOSSUBSUB
				 LEFT JOIN m_pelabuhan d ON a.DPELASAL = d.KODE_PELABUHAN
				 LEFT JOIN m_pelabuhan e ON a.DPELSEBELUM = e.KODE_PELABUHAN
				 LEFT JOIN m_pelabuhan f ON a.DPELBONGKAR = f.KODE_PELABUHAN
				 LEFT JOIN m_pelabuhan g ON a.DPELLANJUT = g.KODE_PELABUHAN
				 LEFT JOIN t_bc11dok h ON 	a.KODE_TRADER = h.KODE_TRADER AND a.CAR = h.CAR AND a.KDGRUP = h.KDGRUP AND a.NOPOS = h.NOPOS AND a.NOPOSSUB = h.NOPOSSUB AND  a.NOPOSSUBSUB = h.NOPOSSUBSUB
				LEFT JOIN m_tabel i ON 	h.DOKKODE = i.KDREC AND i.MODUL = 'BC11' AND i.KDTAB = 'KDDOK'
WHERE a.KODE_TRADER = '" . $this->kd_trader . "' AND a.CAR = '" . $this->car . "' " . $addQuery ." 
GROUP BY a.KDGRUP, a.NOPOS, a.NOPOSSUB, a.NOPOSSUBSUB
ORDER BY a.KDGRUP, a.NOPOS";
        $dtl = $this->main->get_result($SQL, true);         
        foreach ($dtl as $a){
            $this->dtl[$a['KDGRUP']][] = $a;
        }
        #print_r($this->dtl);die();
    }
    
    function getPOS($Filter=''){
        #DATA DETIL
        switch ($this->hdr['JNSMANIFEST']){
            case'I':$JNSManifest = 'a.INWARD';break;
            case'O':$JNSManifest = 'a.OUTWARD';break;
            default :$JNSManifest = 'a.INWARD';break;
        }
        switch ($this->hdr['TIPEMANIFEST']){
            case'0':$TIPEManifest = " and a.FTZ = 'N' ";break;
            case'1':$TIPEManifest = " and a.FTZ = 'Y' ";break;
            case'2':$TIPEManifest = '';break;
            default :$TIPEManifest = " and a.FTZ = 'N' ";break;
        }
        if($Filter!=''){
            $addQuery =  " AND a.KDGRUP = '" . $Filter ."'";
        }
        $SQL = "SELECT 	a.KDGRUP, a.URKDGRUP, count(b.NOPOS) as 'GJMLPOS', (SELECT COUNT(DISTINCT c.NOCONT) FROM t_bc11con c WHERE c.KODE_TRADER = b.KODE_TRADER AND c.CAR = b.CAR AND c.KDGRUP = b.KDGRUP) as 'GJMLCON', SUM(b.JMLKEMAS) as 'GJMLKMS', SUM(b.BRUTO) as 'GJMLBRT', SUM(b.VOLUME) as 'GJMLVOL' FROM m_manifestgrup a RIGHT JOIN t_bc11dtl b on b.KODE_TRADER = '" . $this->kd_trader . "' AND b.CAR = '" . $this->hdr['CAR'] . "' AND a.KDGRUP = b.KDGRUP WHERE " . $JNSManifest . " = 'Y' " . $TIPEManifest . $addQuery ." GROUP BY b.KDGRUP";
        $this->pos = $this->actMain->get_result($SQL, true);
        #print_r($this->pos);die();
    }

    function HeaderDok($aPos){
        $this->pdf->addPage('L');
        $this->pdf->SetMargins(5, 0, 0, 5);
        $this->pdf->SetAutoPageBreak(0, 10);
        $this->pdf->SetY(5);
        $this->pdf->SetFont('arial', 'B', '11');
        $this->pdf->cell(54, 4, strtoupper($this->hdr['URJNSMANIFEST'] . ' MANIFEST'), 0, 0, 'L', 0);
        $this->pdf->SetX(60);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->cell(45, 4, 'No. Pengajuan', 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, ' : ', 0, 0, 'L', 0);
        $this->pdf->cell(65, 4, substr($this->hdr['CAR'], 0, 6) . '-' . substr($this->hdr['CAR'], 6, 6) . '-' . substr($this->hdr['CAR'], 12, 8)  . '-' . substr($this->hdr['CAR'], 20, 6) , 0, 0, 'L', 0);
        $this->pdf->cell(100, 4, $this->hdr['NAMAPGUNA'], 0, 0, 'L', 0);
        $this->pdf->cell(15, 4, 'BC 1.1', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetX(60);
        $this->pdf->cell(45, 4, 'No. BC 1.0', 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, ' : ', 0, 0, 'L', 0);
        $this->pdf->cell(65, 4, ($this->hdr['NOBC10']!='')?$this->hdr['NOBC10'] . ' / ' . $this->hdr['TGBC10']:'-' , 0, 0, 'L', 0);
        $this->pdf->cell(100, 4, 'NPWP : ' . $this->main->formatNPWP($this->hdr['NPWPPGUNA']) , 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetX(60);
        $this->pdf->cell(45, 4, 'No. BC 1.1', 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, ' : ', 0, 0, 'L', 0);
        $this->pdf->cell(65, 4, ($this->hdr['NOBC11']!='')?$this->hdr['NOBC11'] . ' / ' . $this->hdr['TGBC11']:'-' , 0, 0, 'L', 0);
        $yKPBC = $this->pdf->getY();
        $this->pdf->SetX($this->pdf->getX());
        $this->pdf->multicell(100, 4, $this->hdr['ALMTGUNA'] , 0, 'L');
        $this->pdf->SetY($yKPBC);
        $this->pdf->Ln();
        $this->pdf->cell(40, 4, 'Nomor KPBC', 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, ' : ', 0, 0, 'L', 0);
        $this->pdf->cell(125, 4, $this->hdr['URKPBC'], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40, 4, 'Kelompok', 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, ' : ', 0, 0, 'L', 0);
        $this->pdf->cell(125, 4, $aPos['URKDGRUP'], 0, 0, 'L', 0);
        $this->pdf->Ln(5);
        $getX = $this->pdf->getX();
        $getY = $this->pdf->getY();
        $this->pdf->Line($getX,$getY,$getX+285,$getY);
        $this->pdf->Ln(1);
        $this->pdf->cell(30, 4, 'Nama Sarana Angkut', 0, 0, 'L', 0);
        $this->pdf->cell(3, 4, ' : ', 0, 0, 'L', 0);
        $this->pdf->cell(55, 4, $this->hdr['NMANGKUT'], 0, 0, 'L', 0);
        $this->pdf->cell(35, 4, 'Pelabuhan Asal-Bongkar', 0, 0, 'L', 0);
        $this->pdf->cell(3, 4, ' : ', 0, 0, 'L', 0);
        $this->pdf->cell(70, 4, ucwords(strtolower($this->hdr['URPELMUAT'].' - '.$this->hdr['URPELBKR'])), 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, 'Jml. BL', 0, 0, 'L', 0);
        $this->pdf->cell(3, 4, ' : ', 0, 0, 'L', 0);
        $this->pdf->cell(15, 4, number_format($aPos['GJMLPOS'], 0, '.', ',') , 0, 0, 'L', 0);
        $this->pdf->cell(15, 4, '   Bruto', 0, 0, 'L', 0);
        $this->pdf->cell(3, 4, ' : ', 0, 0, 'L', 0);
        $this->pdf->cell(33, 4, number_format($aPos['GJMLBRT'], 4, '.', ',').' Kgm', 0, 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->cell(30, 4, 'No Voy', 0, 0, 'L', 0);
        $this->pdf->cell(3, 4, ' : ', 0, 0, 'L', 0);
        $this->pdf->cell(55, 4, $this->hdr['NOVOY'], 0, 0, 'L', 0);
        $this->pdf->cell(35, 4, 'Pelabuhan Muat-Akhir', 0, 0, 'L', 0);
        $this->pdf->cell(3, 4, ' : ', 0, 0, 'L', 0);
        $this->pdf->cell(70, 4, ucwords(strtolower($this->hdr['URPELTRANSIT'].' - '.$this->hdr['URPELNEXT'])), 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, 'Jml. Kontainer', 0, 0, 'L', 0);
        $this->pdf->cell(3, 4, ' : ', 0, 0, 'L', 0);
        $this->pdf->cell(15, 4, number_format($aPos['GJMLCON'], 0, '.', ',') , 0, 0, 'L', 0);
        $this->pdf->cell(15, 4, '   Volume', 0, 0, 'L', 0);
        $this->pdf->cell(3, 4, ' : ', 0, 0, 'L', 0);
        $this->pdf->cell(33, 4, number_format($aPos['GJMLVOL'], 4, '.', ',').' M3', 0, 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->cell(30, 4, 'Bendera', 0, 0, 'L', 0);
        $this->pdf->cell(3, 4, ' : ', 0, 0, 'L', 0);
        $this->pdf->cell(55, 4, ucwords(strtolower($this->hdr['URVOYFLAG'])), 0, 0, 'L', 0);
        $this->pdf->cell(35, 4, 'Tanggal,Jam Tiba', 0, 0, 'L', 0);
        $this->pdf->cell(3, 4, ' : ', 0, 0, 'L', 0);
        $this->pdf->cell(70, 4, $this->hdr['TGTIBABERANGKAT'] . '  ' . $this->hdr['JAMTIBABERANGKAT'], 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, 'Jml. Kemasan', 0, 0, 'L', 0);
        $this->pdf->cell(3, 4, ' : ', 0, 0, 'L', 0);
        $this->pdf->cell(15, 4, number_format($aPos['GJMLKMS'], 0, '.', ',') , 0, 0, 'L', 0);
        $this->pdf->Ln();
        $getX = $this->pdf->getX();
        $getY = $this->pdf->getY();
        $this->pdf->SetFillColor(255, 255, 128);
        $this->pdf->Rect(5, $getY, $getX+280, 12, 'DF');
        $this->pdf->cell(15, 4, 'No. Pos', 1, 0, 'L', 0);
        $this->pdf->cell(35, 4, 'Bill of lading', 1, 0, 'L', 0);
        $this->pdf->cell(60, 4, 'Shipper, Consignee, Notify Party', 1, 0, 'L', 0);
        $getX = $this->pdf->getX();
        $this->pdf->multicell(50, 4, "Merek\nJumlah/Jenis Kemasan\nNo.Kontainer" , 1, 'C');
        $this->pdf->setXY($getX+50,$getY);
        $this->pdf->cell(70, 4, 'Uraian Barang, Bruto/Volume', 1, 0, 'L', 0);
        $this->pdf->multicell(55, 4, "Pel Asal / Pel Transit Terakhir(Muat)\nPel Bongkar / Pel Akhir" , 1, 'L');
        //$this->pdf->arrCell(55, 4, "Pel Asal / Pel Transit Terakhir(Muat)\nPel Bongkar / Pel Akhir" , 1, 'L');
        
        
        $this->pdf->Ln();
    }
    
    function perPos(){
        foreach ($this->pos as $aPos){
            $this->tableDetil($aPos);
        }
    }
    
    function tableDetil($aPos){
        $this->HeaderDok($aPos);
        $yAwl = $this->pdf->getY();
        $arr['WIDE']['NOPOS'] = 15;
        $arr['WIDE']['URBL']  = 35;
        $arr['WIDE']['URNM']  = 60;
        $arr['WIDE']['URMR']  = 50;
        $arr['WIDE']['URBR']  = 70;
        $arr['WIDE']['URPL']  = 55;
        
        $arr['LIGN']['NOPOS'] = 'C';
        $arr['LIGN']['URBL']  = 'L';
        $arr['LIGN']['URNM']  = 'L';
        $arr['LIGN']['URMR']  = 'L';
        $arr['LIGN']['URBR']  = 'L';
        $arr['LIGN']['URPL']  = 'L';
        foreach ($this->dtl[$aPos['KDGRUP']] as $a){
            $arrData['NOPOS'] = $this->pdf->arrCell($arr['WIDE']['NOPOS'], 4, $a['NOPOS'], 1, 'L');
            $arrData['URBL']  = $this->pdf->arrCell($arr['WIDE']['URBL'], 4, $a['URBL'], 1, 'L');
            $arrData['URNM']  = $this->pdf->arrCell($arr['WIDE']['URNM'], 4, $a['URNM'], 1, 'L');
            $arrData['URMR']  = $this->pdf->arrCell($arr['WIDE']['URMR'], 4, $a['URMR'], 1, 'L');
            $arrData['URBR']  = $this->pdf->arrCell($arr['WIDE']['URBR'], 4, $a['URBR'], 1, 'L');
            $arrData['URPL']  = $this->pdf->arrCell($arr['WIDE']['URPL'], 4, $a['URPL'], 1, 'L');
            unset($arrLen);
            foreach ($arrData as $v){
                $arrLen[] = count($v);
            }
            $maxLen  = max($arrLen);
            for($i=0; $i<$maxLen; $i++){
                foreach ($arrData as $k=>$v){
                    $this->pdf->cell($arr['WIDE'][$k], 4, $arrData[$k][$i], 'LR', 0, $arr['LIGN'][$k], 0);
                }
                $this->pdf->Ln();
                $yAwl = $this->pdf->getY();
                $hPage = round($this->pdf->h) - 17;
                if($yAwl >  $hPage){
                    $this->FooterDok();
                    $this->HeaderDok($aPos);
                }
            }
            #$this->pdf->Line($this->pdf->getX(),$this->pdf->getY(),$this->pdf->getX()+285,$this->pdf->getY());
        }
        $this->pdf->Line($this->pdf->getX(),$this->pdf->getY(),$this->pdf->getX()+285,$this->pdf->getY());
    }
    
    function FooterDok(){
        $this->pdf->Line($this->pdf->getX(),$this->pdf->getY(),$this->pdf->getX()+285,$this->pdf->getY());
        $this->pdf->SetY(-15);
        $this->pdf->cell(0,10,date('d-m-Y').'                                                                                                                                                                                                            '.$this->hdr['NAMAPGUNA'].'                                      '.'Halaman '.$this->pdf->PageNo().' dari {nb}',0,0,'L');
        
    }
    
    
    
}