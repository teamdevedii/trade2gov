<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class actUser extends CI_Model{
    function login($uid_, $pwd_, $adm = FALSE) {
        $query = "SELECT A.USER_ID, A.KODE_TRADER, A.USERNAME, A.PASSWORD, A.NAMA, A.ALAMAT, A.TELEPON, A.JABATAN, ".
                        "D.TIPE_TRADER, A.STATUS_USER,D.DOKUMEN, C.URAIAN AS NAMA_ROLE, A.TIPE_USER AS KODE_TIPE_USER, ".
                        "D.NAMA AS NAMA_TRADER, D.LOGO, A.EXPIRED_DATE, D.EDINUMBER, D.UNLOCKCODE ".
                "FROM t_user A	Inner Join m_trader D on A.KODE_TRADER 	= D.KODE_TRADER ".
                               "Left Join  m_tabel C on C.MODUL = 'BC20' AND C.KDTAB = 'TIPE_USER' AND A.TIPE_USER = C.KDREC ".
                "WHERE A.USERNAME='$uid_' AND A.PASSWORD='$pwd_' AND A.STATUS_USER='1'";//die($query);
        $data = $this->db->query($query);
        if ($data->num_rows() > 0) {
            $rs = $data->row();
            if ($rs->EXPIRED_DATE) {
                if ($this->cekEkspiredDate($rs->EXPIRED_DATE)) {
                    return "2";
                }
            }
            foreach ($data->result_array() as $row) {
                $datses['LOGGED']       = true;
                $datses['IP']           = $_SERVER['REMOTE_ADDR'];
                $datses['USER_ID']      = $row['USER_ID'];
                $datses['USER_NAME']    = $uid_;
                //$datses['PASSWORD']     = $pwd_;
                $datses['NAMA']         = $row['NAMA'];
                $datses['ALAMAT']       = $row['ALAMAT'];
                $datses['KODE_TRADER']  = $row['KODE_TRADER'];
                $datses['NAMA_TRADER']  = $row['NAMA_TRADER'];
                $datses['LOGO']         = $row['LOGO'];
                $datses['KODE_TIPE_USER'] = $row['KODE_TIPE_USER'];
                $datses['NAMA_ROLE']    = $row['NAMA_ROLE'];
                $datses['DOKUMEN']      = $row['DOKUMEN'];
                $datses['TIPE_TRADER']  = $row['TIPE_TRADER'];
                $datses['EDINUMBER']    = $row['EDINUMBER'];
                $datses['UNLOCKCODE']   = $row['UNLOCKCODE'];
            }
            date_default_timezone_set('Asia/Jakarta');
            $data = array('LAST_LOGIN' => date('Y-m-d H:i:s') ,'EXPIRED_DATE' => date("Y-m-d",strtotime(date()." +30 day")));
            $this->db->where('USER_ID', $rs->USER_ID);
            $this->db->update('t_user', $data);
            $this->newsession->set_userdata($datses);
            return "1";
        } else {
            return "0";
        }
    }
    
    function cekEkspiredDate($date = "") {
        if (date('Y-m-d') >= $date)
            return true;
        else
            return false;
    }
/*
    function verify($uid_, $pwd_, $adm = FALSE) {
        $query = "SELECT A.USER_ID, A.KODE_TRADER, A.USERNAME, A.PASSWORD, A.NAMA, A.ALAMAT, A.TELEPON, A.JABATAN, D.TIPE_TRADER, A.EMAIL, A.STATUS_USER, A.ROLE, C.URAIAN AS NAMA_ROLE, D.NAMA AS NAMA_TRADER, D.LOGO, A.EXPIRED_DATE 
                  FROM t_user A Inner Join m_trader D 	on A.KODE_TRADER = D.KODE_TRADER 
				Left Join  m_tabel C 	on A.ROLE = C.KDREC AND C.KDTAB = 'ROLE'
		  WHERE	A.USERNAME='$uid_' AND A.PASSWORD='$pwd_' AND A.STATUS_USER='1'";
        $data = $this->db->query($query);
        if ($data->num_rows() > 0) {
            $rs = $data->row();
            foreach ($data->result_array() as $row) {
                $datses['LOGGED'] = true;
                $datses['IP'] = $_SERVER['REMOTE_ADDR'];
                $datses['USER_ID'] = $row['USER_ID'];
                $datses['USER_NAME'] = $uid_;
                $datses['PASSWORD'] = $pwd_;
                $datses['NAMA'] = $row['NAMA'];
                $datses['EMAIL'] = $row['EMAIL'];
                $datses['ALAMAT'] = $row['ALAMAT'];
                $datses['KODE_TRADER'] = $row['KODE_TRADER'];
                $datses['NAMA_TRADER'] = $row['NAMA_TRADER'];
                $datses['LOGO'] = $row['LOGO'];
                $datses['KODE_ROLE'] = $row['ROLE'];
                $datses['NAMA_ROLE'] = $row['NAMA_ROLE'];
                $datses['TIPE_TRADER'] = $row['TIPE_TRADER'];
            }
            date_default_timezone_set('Asia/Jakarta');
            $data = array('LAST_LOGIN' => date('Y-m-d H:i:s'));
            $this->db->where('USER_ID', $rs->USER_ID);
            $this->db->update('t_user', $data);
            $this->newsession->set_userdata($datses);
            return 1;
        } else {
            return 0;
        }
    }
    
    function forgot() {
        $conn = get_instance();
        $conn->load->model("main", "main", true);
        $usr = $this->input->post('usr');
        $uid = $this->input->post('uid');
        $email = $this->input->post('email');
        $SQLUser = "SELECT USERNAME FROM t_user WHERE USERNAME = '$usr'";
        $dtUser = $this->db->query($SQLUser);
        if ($dtUser->num_rows() > 0) {
            $SQLId = "SELECT ID FROM m_trader WHERE ID = '$uid'";
            $dtId = $this->db->query($SQLId);
            if ($dtId->num_rows() > 0) {
                $SQLEmail = "SELECT EMAIL FROM t_user WHERE EMAIL = '$email'";
                $dtemail = $this->db->query($SQLEmail);
                if ($dtemail->num_rows() > 0) {
                    $SQL = "SELECT A.USER_ID,A.EMAIL,A.NAMA,A.USERNAME FROM t_user A, m_trader B WHERE A.KODE_TRADER=B.KODE_TRADER 
							AND B.ID='$uid' AND A.USERNAME='$usr' AND A.EMAIL='$email'";
                    $data = $conn->main->get_result($SQL);
                    if ($data) {
                        foreach ($SQL->result() as $row) {
                            $email = $row->EMAIL;
                            $nama = $row->NAMA;
                            $userid = $row->USER_ID;
                            $username = $row->USERNAME;
                            $pwd = str_shuffle("0123456789");
                            $pwd = substr($pwd, 3, 6);
                            $subject = "Lupa Password";
                            $isi = 'Password Akun Anda di www.PIB Online.com telah diubah oleh sistem. Silahkan login dengan:<table style="margin: 4px 4px 4px 10px; font-family: arial; font-size:12px; font-weight: 700; width:580px;"><tr><td width="65">Username</td><td>: <b>' . $username . '</b></td></tr><tr><td>Password</td><td>: <b>' . $pwd . '</b></td></tr></table>Terima kasih.';
                            if ($conn->main->send_mail($email, $nama, $subject, $isi)) {
                                $this->db->where('USER_ID', $userid);
                                if ($this->db->update('T_USER', array("STATUS" => "1", "PASSWORD" => md5($pwd))))
                                    $ret = "1|OKE";
                            }else {
                                $ret = "0|Send Mail Error";
                            }
                        }
                    } else {
                        $ret = "0|Data yang anda masukan salah.";
                    }
                } else {
                    $ret = "0|Email belum terdaftar";
                }
            } else {
                $ret = "0|No. Identitas belum terdaftar";
            }
        } else {
            $ret = "0|Username belum terdaftar";
        }
        return $ret;
    }

    function ubahpassword($isajax) {
        if ($this->newsession->userdata('LOGGED')) {
            $conn = get_instance();
            $conn->load->model("main", "main", true);
            if (md5($this->input->post('oldpwd')) == $this->newsession->userdata('PASSWORD')) {
                $arrpwd = array('PASSWORD' => md5($this->input->post('pwd')));
                $this->db->where('USER_ID', $this->newsession->userdata('USER_ID'));
                $id = $this->newsession->userdata('USER_ID');
                $pwd = $this->input->post('pwd');
                if ($this->db->update('T_USER', $arrpwd)) {
                    $this->newsession->set_userdata('PASSWORD', md5($this->input->post('pwd')));
                    if ($isajax != "ajax") {
                        redirect(base_url());
                        exit();
                    }
                    $query = "SELECT NAMA, EMAIL, USER_NAME FROM t_user WHERE USER_ID = '$id'";
                    $dt = $conn->main->get_result($query);
                    if ($dt) {
                        foreach ($query->result() as $row) {
                            $email = $row->EMAIL;
                            $nama = $row->NAMA;
                            $userid = $row->USER_NAME;
                            $subject = "Ubah Password";
                            $isi = 'Password Akun Anda di PIB Online telah diubah. Silahkan login dengan:<table style="margin: 4px 4px 4px 10px; font-family: arial; font-size:12px; font-weight: 700; width:580px;"><tr><td width="65">Username</td><td>: <b>' . $userid . '</b></td></tr><tr><td>Password</td><td>: <b>' . $pwd . '</b></td></tr></table>Terima kasih.';
                            $conn->main->send_mail($email, $nama, $subject, $isi);
                        }
                    }
                    return "MSG#OK#Ubah Password Berhasil#" . site_url();
                }
            }
            if ($isajax != "ajax") {
                redirect(base_url());
                exit();
            }
            return "MSG#ERR#Ubah Password Gagal";
        } else {
            redirect(base_url());
            exit();
        }
    }

    function ubahprofil($isajax) {
        if ($this->newsession->userdata('LOGGED')) {
            $arrdata = array('NAMA' => $this->input->post('nama'),
                'JABATAN' => $this->input->post('jabatan'),
                'EMAIL' => $this->input->post('email'));
            if ($this->newsession->userdata('TRADER_ID') == 0)
                $arrdata['NIP'] = $this->input->post('nip');
            $this->db->where('USER_ID', $this->newsession->userdata('USER_ID'));
            if ($this->db->update('T_USER', $arrdata)) {
                $this->newsession->set_userdata($arrdata);
                if ($isajax != "ajax") {
                    redirect(base_url());
                    exit();
                }
                return "MSG#OK#Ubah Profil Berhasil#" . site_url();
            }
            if ($isajax != "ajax") {
                redirect(base_url());
                exit();
            }
            return "MSG#ERR#Ubah Profil Gagal";
        } else {
            redirect(base_url());
            exit();
        }
    }

    function kontak($nama, $email, $pertanyaan) {
        $conn = get_instance();
        $conn->load->model("main", "main", true);
        $to = "muklis@edi-indonesia.co.id";
        $nma = "Administrator PIB Online";
        $nm = $nama;
        $from = $email;
        $tanya = $pertanyaan;
        $subject = "Kontak Kami";
        $isi = '<table style="margin: 4px 4px 4px 10px; font-family: arial; font-size:12px; font-weight: 700; width:580px;"><tr><td width="65">Nama</td><td>: ' . $nm . '</td></tr><tr><td>Email</td><td>: ' . $from . '</td></tr><tr><td valign="top">Pertanyaan</td><td valign="top">: ' . $tanya . '</td></tr></table>';
        if ($conn->main->send_mail($to, $nma, $subject, $isi)) {
            $ret = "MAIL";
        } else {
            $ret = "ERRMAIL";
        }

        return $ret;
    }
*/
}

?>