<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class actKomunikasi extends CI_Model {
    var $kd_trader  = '';
    var $nuSoap_lib = '';
    var $unLockCode = '';
    var $EDINumber  = '';
    function __construct(){
        parent::__construct();
        $this->kd_trader    = $this->newsession->userdata('KODE_TRADER');
        $this->unLockCode   = $this->newsession->userdata('UNLOCKCODE');
        $this->EDINumber    = $this->newsession->userdata('EDINUMBER');
        return true;
    }
    
    function getDokOutbound(){
        $jns = 'DokOutbound';
        $this->load->model('actMain');
        $this->load->library('newtable');
        $SQL = "SELECT A.CAR, B.KDKPBC, A.JNSDOK, A.STATUS,A.JNSDOK as 'Jns. Dokumen', 
                       concat(SUBSTRING(A.CAR,1,6),'-',SUBSTRING(A.CAR,7,6),'-',SUBSTRING(A.CAR,13,8),'-',SUBSTRING(A.CAR,21,6)) as 'Nomor Aju', 
                       concat(B.KDKPBC,' - ',C.URAIAN_KPBC) as 'KPBC', A.APRF ,
                       (IFNULL(D.SNRF,IFNULL(F.SNRF,IFNULL(G.SNRF,IFNULL(I.SNRF,IFNULL(J.SNRF,'')))))) as 'SNRF', 
                        A.WKBUAT as 'Wkt. Buat', CONCAT(E.URAIAN, IFNULL(CONCAT('<div>', a.KETERANGAN,'</div>'),'')) as 'Status Kom'
                FROM M_TRADER_DOKOUTBOUND A INNER JOIN M_TRADER_PARTNER B ON A.KODE_TRADER = B.KODE_TRADER AND A.KDKPBC = B.KDKPBC
                                            INNER JOIN M_KPBC C     ON B.KDKPBC      = C.KDKPBC
                                            LEFT  JOIN T_BC20HDR D  ON A.KODE_TRADER = D.KODE_TRADER AND A.CAR = D.CAR AND A.JNSDOK = 'BC20'
                                            LEFT  JOIN T_BC23HDR F  ON A.KODE_TRADER = F.KODE_TRADER AND A.CAR = F.CAR AND A.JNSDOK = 'BC23'
                                            LEFT  JOIN T_BC30HDR G  ON A.KODE_TRADER = G.KODE_TRADER AND A.CAR = G.CAR AND A.JNSDOK = 'BC30'
                                            LEFT  JOIN T_BC11HDR I  ON A.KODE_TRADER = I.KODE_TRADER AND A.CAR = I.CAR AND A.JNSDOK = 'BC11'
                                            LEFT  JOIN T_BC10HDR J  ON A.KODE_TRADER = J.KODE_TRADER AND A.CAR = J.CAR AND A.JNSDOK = 'BC10'
                                            LEFT  JOIN T_BC30PKBEHDR H  ON A.KODE_TRADER = H.KODE_TRADER AND A.CAR = H.CAR AND A.JNSDOK = 'BC30PKBE'
                                            INNER JOIN M_TABEL E    ON E.MODUL = 'KOMUNIKASI' AND E.KDTAB = 'STATUS' AND E.KDREC = A.STATUS
                WHERE A.KODE_TRADER = " . $this->kd_trader ;
        #die($SQL);
        $this->newtable->keys(array('CAR', 'KDKPBC', 'JNSDOK'));
        $this->newtable->hiddens(array('CAR', 'KDKPBC', 'JNSDOK', 'STATUS'));
        $this->newtable->search(array(array('A.CAR'   , 'Nomor Aju'),
                                      array('A.STATUS', 'Status Kom', 'tag-select', $this->actMain->get_mtabel('STATUS', 1, false, '', 'KODEUR', 'KOMUNIKASI')),
                                      array('A.JNSDOK', 'Jns. Dokumen'),
                                      array("concat(B.KDKPBC,' - ',C.URAIAN_KPBC)", 'KPBC'),
                                      array('A.APRF', 'APRF'),
                                      array('D.SNRF', 'SNRF')));
                                      //,  as ,  as 'KPBC', A.APRF ,D.SNRF, A.WKBUAT
        $prosesnya = array(
            'Send / Receive'    => array('GETPOP', site_url('komunikasi/sendreceive'), '0',' KOMUNIKASI DOKUMEN |400|200'),
            'View EDI File'     => array('GETPOP', site_url('komunikasi/openEDI'), '1',' Edi File |900|600'),
            'View Flat File'    => array('GETPOP', site_url('komunikasi/openFLT'), '1',' Flat File |900|600'),
            'Resend'            => array('DELETEAJAX', site_url('komunikasi/resendDokumen'), '1', '_content_')
            );
        $this->newtable->menu($prosesnya);
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->action(site_url('komunikasi/dataSent'));
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->orderby('A.WKBUAT');
        $this->newtable->sortby("DESC");
        $this->newtable->rowcount(10);
        $this->newtable->clear();
        $this->newtable->set_formid("f".$jns);
        $this->newtable->set_divid("div".$jns);
        $this->newtable->tipe_check('radio');
        $this->newtable->tipe_proses('button');
        $tabel = $this->newtable->generate($SQL);
        $arrdata = array("tabel" => $tabel);
        if ($this->input->post("ajax"))
            return $tabel;
        else
            return $arrdata;
    }
    
    function getDokInbound(){
        $this->load->model('actMain');
        $this->load->library('newtable');
        $jns = 'DokInbound';
        $SQL = "SELECT a.IDDOK, a.PARTNER, concat(b.KDKPBC,' - ',d.URAIAN_KPBC) 'KPBC', "
                   . " a.PARTNER 'EDI Number', a.APRF, concat(SUBSTRING(a.CAR,1,6),'-',SUBSTRING(a.CAR,7,6),'-',SUBSTRING(a.CAR,13,8),'-',SUBSTRING(a.CAR,21,8)) as 'Nomor Aju', a.SNRF, concat(a.SENDAT,'<br>',a.RECEIVEAT) as 'wkt. Kirim <br>wkt. Terima', c.URAIAN as 'STATUS' ".
               "FROM m_trader_dokinbound a LEFT JOIN m_trader_partner b on a.PARTNER = b.NOMOREDI and a.KODE_TRADER = b.KODE_TRADER ".
                                          "LEFT JOIN m_tabel c on c.MODUL = 'KOMUNIKASI' and c.KDTAB = 'STATUSINBOUND' and a.`STATUS` = c.KDREC " . 
                                          "LEFT JOIN m_kpbc  d on b.KDKPBC= d.KDKPBC ".
               "WHERE a.KODE_TRADER = ".$this->kd_trader;
        $this->newtable->keys(array('IDDOK','PARTNER', 'APRF'));
        $this->newtable->hiddens(array('IDDOK','PARTNER'));
        $this->newtable->search(array(array('b.KDKPBC'  , 'KPBC'),
                                      array('a.CAR'     , 'Nomor Aju'),
                                      array("concat(b.KDKPBC,' - ',c.URAIAN_KPBC)", 'KPBC'),
                                      array('a.APRF', 'APRF'),
                                      array('a.SNRF', 'SNRF'),
                                      array('A.STATUS', 'Status', 'tag-select', $this->actMain->get_mtabel('STATUSINBOUND',1,false,'','KODEUR','KOMUNIKASI'))
            ));
        $prosesnya = array(
            'Send / Receive'  => array('GETPOP', site_url('komunikasi/sendreceive'), '0',' KOMUNIKASI DOKUMEN |400|200'),
            'View EDI File' => array('GETPOP', site_url('komunikasi/openFILERES/DIREDI'), '1',' Edi File |900|600'),
            'View Flat File' => array('GETPOP', site_url('komunikasi/openFILERES/DIRFLT'), '1',' Flat File |900|600'),
            'Delete'            => array('DELETEAJAX', site_url('komunikasi/delInbound'), '1', '_content_'));
        $this->newtable->menu($prosesnya);
        $this->newtable->orderby("a.RECEIVEAT");
        $this->newtable->sortby("DESC");
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->action(site_url('komunikasi/dataReceive'));
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->rowcount(10);
        $this->newtable->clear();
        $this->newtable->set_formid("f".$jns);
        $this->newtable->set_divid("div".$jns);
        $this->newtable->tipe_check('radio');
        $this->newtable->tipe_proses('button');
        $tabel = $this->newtable->generate($SQL);
        $arrdata = array("tabel" => $tabel);
        if ($this->input->post("ajax"))
            return $tabel;
        else
            return $arrdata;
    }
    
    function communication(){
        $this->load->library("nuSoap_lib");
        $this->load->model('actMain');
        $this->load->model('actTranslator');
        
        $this->nuSoap_lib = new nusoap_client(base_url('Extreme.jws'),true);
        if ($this->nuSoap_lib->fault) {
            $rtn = 'Error: ' . $nuSoap_lib->fault;
        }
        else {
            if ($this->nuSoap_lib->getError()) {
                $rtn = 'Error: ' . $nuSoap_lib->getError();
            }
            else {
                unset($ws);
                $loginKey['userEDI'] = $this->EDINumber;
                $loginKey['string0'] = $this->newsession->userdata('UNLOCKCODE');
                #login ke web services EDII
                unset($msgErr);
                $ws = $this->nuSoap_lib->call('login', $loginKey,'http://www.openuri.org/');
                if(isset($ws['loginResult'])){
                    $SQL = "SELECT LASTDAYEXEC,DELMAIL,DELPOST FROM M_TRADER WHERE KODE_TRADER = ".$this->kd_trader;
                    $flg = $this->actMain->get_result($SQL);
                    switch ($ws['loginResult']){
                        case'1':$msgErr['Login'] = '1-Login Fail Unauthorized user';break;
                        case'2':$msgErr['Login'] = '2-Login Fail EDINUMBER expired';break;
                        case'9':$msgErr['Login'] = '9-Login Fail Unknown Error';break;
                        default:$ws['loginResult'] = explode('|', $ws['loginResult']);$msgErr['Login'] = '0-Login Success';break;
                    }
                    if($ws['loginResult'][1]=='1000'){
                    #EstablishRelationship
                        $SQL = "SELECT a.NOMOREDI,b.APRF,b.DIRECTION,b.STATUS ".
                               "FROM M_TRADER_PARTNER a INNER JOIN M_TRADER_PARTNER_DETIL b ON a.KODE_TRADER = b.KODE_TRADER AND ".
                                                                                              "a.KDKPBC      = b.KDKPBC AND ".
                                                                                              "a.NOMOREDI    = b.NOMOREDI AND ".
                                                                                              "b.STATUS      = '2' ".
                               "WHERE a.KODE_TRADER = ".$this->kd_trader;

                        $prt = $this->actMain->get_result($SQL,true);
                        foreach ($prt as $a){
                            $int = $this->EstablishRelationship($a['NOMOREDI'],$a['APRF'],$a['DIRECTION'],$ws['loginResult'][0]);
                            if(isset($int)){
                                $msgErr['Relationship'] = $int;break;
                            }
                        }
                        #ListRelationShip dari WS
                        $lst = $this->ListingRelationship($ws['loginResult'][0]);
                        foreach ($lst as $a){
                            $arrPar = explode(',', $a);
                            $wsUPD[$arrPar[0].'|'.$arrPar[1].'|'.$arrPar[2]] = $arrPar[3];
                            $wsDFT[] = $arrPar[0].'|'.$arrPar[1].'|'.$arrPar[2];
                        }
                        #ListRelationShip dari DB
                        $SQL = "SELECT Concat(NOMOREDI,'|',APRF,'|',DIRECTION) as GRB, KDKPBC, STATUS as STS "
                             . "FROM M_TRADER_PARTNER_DETIL WHERE KODE_TRADER = ".$this->kd_trader;
                        $TRDFTR = $this->actMain->get_result($SQL,true);
                        foreach ($TRDFTR as $a){
                            $dbUPD[$a['GRB']] = $a['STS'];
                            $dbDFT[] = $a['GRB'];
                            $KDKPBC[$a['GRB']] = $a['KDKPBC'];
                        }
                        
                        #data di ws ada dan di db ada -> update status di db sesuai return komunikasi
                        $AVA = array_intersect($wsDFT, $dbDFT);
                        //return print_r($AVA,true);
                        foreach ($AVA as $a){
                            if($dbUPD[$a]!=$wsUPD[$a]){
                                $arr = explode('|', $a);
                                $UPD['KODE_TRADER'] = $this->kd_trader;
                                $UPD['KDKPBC']      = $KDKPBC[$a];
                                $UPD['NOMOREDI']    = $arr[0];
                                $UPD['APRF']        = $arr[1];
                                $UPD['DIRECTION']   = $arr[2];
                                $UPD['STATUS']      = $wsUPD[$a];
                                $this->actMain->insertRefernce('M_TRADER_PARTNER_DETIL', $UPD);
                            }
                        }
                        
                        #data di ws tidak ada dan di db ada -> update status 2 di db
                        $BVA = array_diff($dbDFT,$wsDFT);
                        //return print_r($BVA,true);
                        foreach ($BVA as $a){
                            $arr = explode('|', $a);
                            $UPD['KODE_TRADER'] = $this->kd_trader;
                            $UPD['KDKPBC']      = $KDKPBC[$a];
                            $UPD['NOMOREDI']    = $arr[0];
                            $UPD['APRF']        = $arr[1];
                            $UPD['DIRECTION']   = $arr[2];
                            $UPD['STATUS']      = '2';
                            $this->actMain->insertRefernce('M_TRADER_PARTNER_DETIL', $UPD);
                        }
                        
                        #data di ws ada dan di db tidak ada ->  
                        $UNA = array_diff($wsDFT,$dbDFT);
                        foreach ($UNA as $a){
                            $arr = explode('|', $a);
                            $this->DeleteRelationship($arr[0] ,$arr[1] ,$arr[2], $ws['loginResult'][0]);
                        }
                        
                    #TransferDocuments
$SQL = "
SELECT 	A.KODE_TRADER, A.KDKPBC, A.JNSDOK, A.APRF, B.SNRF, B.DIREDI, C.NOMOREDI, D.DOKTYPE, B.CAR  
FROM M_TRADER_DOKOUTBOUND A INNER JOIN T_BC20HDR B ON A.KODE_TRADER = B.KODE_TRADER AND A.CAR = B.CAR AND A.KDKPBC = B.KDKPBC AND A.JNSDOK = 'BC20' 
							INNER JOIN M_TRADER_PARTNER C ON A.KODE_TRADER = C.KODE_TRADER AND A.KDKPBC = C.KDKPBC 
							INNER JOIN M_TRADER_PARTNER_DETIL D ON D.KODE_TRADER = A.KODE_TRADER AND D.NOMOREDI = C.NOMOREDI AND D.APRF = A.APRF AND D.DIRECTION = 'S' AND D.`STATUS` IN ('1','0') 
WHERE A.`STATUS` = '00' AND A.KODE_TRADER = '" . $this->kd_trader . "' UNION ALL 
SELECT 	A.KODE_TRADER, A.KDKPBC, A.JNSDOK, A.APRF, B.SNRF, B.DIREDI, C.NOMOREDI, D.DOKTYPE, B.CAR  
FROM M_TRADER_DOKOUTBOUND A INNER JOIN T_BC23HDR B ON A.KODE_TRADER = B.KODE_TRADER AND A.CAR = B.CAR AND A.KDKPBC = B.KDKPBC AND A.JNSDOK = 'BC23' 
							INNER JOIN M_TRADER_PARTNER C ON A.KODE_TRADER = C.KODE_TRADER AND A.KDKPBC = C.KDKPBC 
							INNER JOIN M_TRADER_PARTNER_DETIL D ON D.KODE_TRADER = A.KODE_TRADER AND D.NOMOREDI = C.NOMOREDI AND D.APRF = A.APRF AND D.DIRECTION = 'S' AND D.`STATUS` IN ('1','0') 
WHERE A.`STATUS` = '00' AND A.KODE_TRADER = '" . $this->kd_trader . "' UNION ALL 
SELECT 	A.KODE_TRADER, A.KDKPBC, A.JNSDOK, A.APRF, B.SNRF, B.DIREDI, C.NOMOREDI, D.DOKTYPE, B.CAR  
FROM M_TRADER_DOKOUTBOUND A INNER JOIN T_BC11HDR B ON A.KODE_TRADER = B.KODE_TRADER AND A.CAR = B.CAR AND A.KDKPBC = B.KPBC AND A.JNSDOK = 'BC11' 
							INNER JOIN M_TRADER_PARTNER C ON A.KODE_TRADER = C.KODE_TRADER AND A.KDKPBC = C.KDKPBC 
							INNER JOIN M_TRADER_PARTNER_DETIL D ON D.KODE_TRADER = A.KODE_TRADER AND D.NOMOREDI = C.NOMOREDI AND D.APRF = A.APRF AND D.DIRECTION = 'S' AND D.`STATUS` IN ('1','0') 
WHERE A.`STATUS` = '00' AND A.KODE_TRADER = '" . $this->kd_trader . "' UNION ALL 
SELECT 	A.KODE_TRADER, A.KDKPBC, A.JNSDOK, A.APRF, B.SNRF, B.DIREDI, C.NOMOREDI, D.DOKTYPE, B.CAR  
FROM M_TRADER_DOKOUTBOUND A INNER JOIN T_BC10HDR B ON A.KODE_TRADER = B.KODE_TRADER AND A.CAR = B.CAR AND A.KDKPBC = B.KDKPBC AND A.JNSDOK = 'BC10' 
							INNER JOIN M_TRADER_PARTNER C ON A.KODE_TRADER = C.KODE_TRADER AND A.KDKPBC = C.KDKPBC 
							INNER JOIN M_TRADER_PARTNER_DETIL D ON D.KODE_TRADER = A.KODE_TRADER AND D.NOMOREDI = C.NOMOREDI AND D.APRF = A.APRF AND D.DIRECTION = 'S' AND D.`STATUS` IN ('1','0') 
WHERE A.`STATUS` = '00' AND A.KODE_TRADER = '" . $this->kd_trader . "'  UNION ALL 
SELECT  A.KODE_TRADER, A.KDKPBC, A.JNSDOK, A.APRF, B.SNRF, B.DIREDI, C.NOMOREDI, D.DOKTYPE, B.CAR  
FROM M_TRADER_DOKOUTBOUND A INNER JOIN T_BC30HDR B ON A.KODE_TRADER = B.KODE_TRADER AND A.CAR = B.CAR AND A.KDKPBC = B.KDKTR AND A.JNSDOK = 'BC30' 
                            INNER JOIN M_TRADER_PARTNER C ON A.KODE_TRADER = C.KODE_TRADER AND A.KDKPBC = C.KDKPBC 
                            INNER JOIN M_TRADER_PARTNER_DETIL D ON D.KODE_TRADER = A.KODE_TRADER AND D.NOMOREDI = C.NOMOREDI AND D.APRF = A.APRF AND D.DIRECTION = 'S' AND D.`STATUS` IN ('1','0') 
WHERE A.`STATUS` = '00' AND A.KODE_TRADER = '" . $this->kd_trader . "'";
                        $dok = $this->actMain->get_result($SQL,true);
                        foreach($dok as $a){
                            unset($UPD);
                            $snd = $this->sendDokumen($a['NOMOREDI'], $a['APRF'], $a['SNRF'], $a['DIREDI'], $a['DOKTYPE'], $ws['loginResult'][0]);
                            return print_r($snd);
                            $UPD['KODE_TRADER'] = $a['KODE_TRADER'];
                            $UPD['KDKPBC']      = $a['KDKPBC'];
                            $UPD['JNSDOK']      = $a['JNSDOK'];
                            $UPD['CAR']         = $a['CAR'];
                            $UPD['KETERANGAN']  = $snd['MSG'];
                            $UPD['STATUS']      = ($snd['KODE']=='0')?'01':'02';
                            $this->actMain->insertRefernce('M_TRADER_DOKOUTBOUND', $UPD);
                            unset($UPD);
                            $jmlSnd[$a['APRF']]++;
                        }
                        
                        #update status dokumen post box
                        $lstBox = $this->listPostBox($ws['loginResult'][0]);
                        foreach($lstBox as $a){
                            $arr = explode(',',$a);
                            unset($UPD);
                            switch ($arr[2]) {
                                case 'DOKPIB09':
                                    switch ($arr[5]){
                                        case '0':$UPD['STATUS'] = '030';break;
                                        case '1':$UPD['STATUS'] = '040';break;
                                        default :$UPD['STATUS'] = '050';break;
                                    }
                                    $this->db->update('t_bc20hdr', $UPD, "KODE_TRADER = ".$this->kd_trader." AND SNRF = '".$arr[3]."' AND STATUS IN ('000','010','020','030','040','050')");
                                break;
                                case 'BC23':
                                    switch ($arr[5]){
                                        case '0':$UPD['STATUS'] = '03';break;
                                        case '1':$UPD['STATUS'] = '04';break;
                                        default :$UPD['STATUS'] = '05';break;
                                    }
                                    #execute update status
                                    $this->db->update('t_bc23hdr', $UPD, "KODE_TRADER = ".$this->kd_trader." AND SNRF = '".$arr[3]."' AND STATUS IN ('00','01','02','03','04','05')");
                                break;
                                case 'DOKMANIF':
                                    switch ($arr[5]){
                                        case '0':$UPD['STATUS'] = '03';break;
                                        case '1':$UPD['STATUS'] = '04';break;
                                        default :$UPD['STATUS'] = '05';break;
                                    }
                                    #execute update status
                                    $this->db->update('t_bc11hdr', $UPD, "KODE_TRADER = ".$this->kd_trader." AND SNRF = '".$arr[3]."' AND STATUS IN ('00','01','02','03','04','05')");
                                break;
                                case 'DOKRKSP':
                                    switch ($arr[5]){
                                        case '0':$UPD['STATUS'] = '03';break;
                                        case '1':$UPD['STATUS'] = '04';break;
                                        default :$UPD['STATUS'] = '05';break;
                                    }
                                    #execute update status
                                    $this->db->update('t_bc10hdr', $UPD, "KODE_TRADER = ".$this->kd_trader." AND SNRF = '".$arr[3]."' AND STATUS IN ('00','01','02','03','04','05')");
                                break;
                                case 'DOKPEB':
                                    switch ($arr[5]){
                                        case '0':$UPD['STATUS'] = '03';break;
                                        case '1':$UPD['STATUS'] = '04';break;
                                        default :$UPD['STATUS'] = '05';break;
                                    }
                                    #execute update status
                                    $this->db->update('t_bc30hdr', $UPD, "KODE_TRADER = ".$this->kd_trader." AND SNRF = '".$arr[3]."' AND STATUS IN ('00','01','02','03','04','05')");
                                break;
                            }
                            unset($arr);
                        }
                    #ReceiveDocument
                        #listMailBox
                        $lstMailBox = $this->listMailBox($ws['loginResult'][0]);
                        foreach($lstMailBox as $a){
                            if($a!==''){
                                $arr = explode(',',$a);
                                unset($key);
                                $key['sessionNumber']= $ws['loginResult'][0];
                                $key['id']           = $arr[0];
                                #download file
                                $dok = $this->nuSoap_lib->call('extractDocByte', $key,'http://www.openuri.org/');
                                $isi = str_replace("~M~", "?'", str_replace("'", "'\n", str_replace("?'", "~M~", base64_decode($dok['extractDocByteResult'])))) ;
                                #buat file
                                $EDIPaht    = 'EDI/'.$this->EDINumber ;
                                if(!is_dir($EDIPaht)){mkdir($EDIPaht,0777,true);}
                                $fullPath   = $EDIPaht .'/'. $arr[0].'.'. $arr[1].'.'.$arr[2].'.'.$arr[3].'.'.$arr[4].'.EDI';
                                if (file_exists($fullPath)){unlink($fullPath);}
                                $handle = fopen($fullPath, 'w');
                                fwrite($handle, $isi);
                                fclose($handle);
                                if (file_exists($fullPath)){
                                    #buat array insert
                                    unset($arrInBound);
                                    $arrInBound['KODE_TRADER'] = $this->kd_trader;
                                    $arrInBound['IDDOK']       = $arr[0];
                                    $arrInBound['PARTNER']     = $arr[1];
                                    $arrInBound['APRF']        = $arr[2];
                                    $arrInBound['SNRF']        = $arr[3];
                                    $arrInBound['SENDAT']      = date('Y-m-d H:i:s', strtotime($arr[4]));
                                    $arrInBound['RECEIVEAT']   = date('Y-m-d H:i:s');
                                    $arrInBound['DIREDI']      = $fullPath;
                                    $arrInBound['STATUS']      = '00';
                                    #insert ke m_trader_dokinbound
                                    $exe = $this->actMain->insertRefernce('M_TRADER_DOKINBOUND', $arrInBound);
                                    if($exe){
                                        #update file di IDX
                                        $this->nuSoap_lib->call('updateStatusMB', array('id'=>$arr[0]),'http://www.openuri.org/');
                                    }
                                }
                            }
                        }
                        
                        #Translasi
                        #select semua aprf yang responnya 00
                        $SQL = "SELECT DISTINCT APRF, DIREDI FROM M_TRADER_DOKINBOUND WHERE KODE_TRADER = ".$this->kd_trader." AND STATUS = '00'";
                        $data = $this->actMain->get_result($SQL,true);
                        chmod('FLAT/'.$this->EDINumber ,0777);
                        foreach ($data as $a){
                            if(file_exists($a['DIREDI'])){
                                $this->actTranslator->getFLTFILE($this->EDINumber, $a['APRF'], basename($a['DIREDI']));#$EDINUMBER,$APRF,$FILENAME
                            }
                        }
                        
                        #select in bound yang berstatus 00 untuk
                        $SQL = "SELECT IDDOK,PARTNER,APRF,DIREDI FROM M_TRADER_DOKINBOUND WHERE KODE_TRADER = ".$this->kd_trader." AND STATUS = '00'";
                        $inb = $this->actMain->get_result($SQL,true);
                        $jmlInb = count($inb);
                        if($jmlInb>0){
                            foreach($inb as $a){
                                $oldEDI = $a['DIREDI'];
                                $nmFile = basename($oldEDI, '.EDI');
                                $oldFLT = 'FLAT/'.$this->EDINumber.'/' . $nmFile . '.FLT';
                                if(file_exists($oldFLT) && file_exists($oldEDI)){
                                    $Path = 'TRANSACTION/' . $this->EDINumber . '/' . date('Ymd');
                                    if(!is_dir($Path)){mkdir($Path,0777,true);}
                                    $newEDI = $Path.'/'.$nmFile.'.EDI';
                                    if(file_exists($newEDI)){unlink($newEDI);}
                                    $newFLT = $Path.'/'.$nmFile.'.FLT';
                                    if(file_exists($newFLT)){unlink($newFLT);}
                                    rename($oldEDI, $newEDI);
                                    rename($oldFLT, $newFLT);
                                    unset($UPD);
                                    $UPD['KODE_TRADER'] = $this->kd_trader;
                                    $UPD['IDDOK']   = $a['IDDOK'];
                                    $UPD['PARTNER'] = $a['PARTNER'];
                                    $UPD['APRF']    = $a['APRF'];
                                    $UPD['DIREDI']  = $newEDI;
                                    $UPD['DIRFLT']  = $newFLT;
                                    $UPD['STATUS']  = '01';
                                    $this->actMain->insertRefernce('M_TRADER_DOKINBOUND',$UPD);
                                }
                            }
                        }
                        
                        #select in bound yang berstatus 01 untuk parsing hasil translate
                        $SQL = "SELECT IDDOK,PARTNER,APRF,DIREDI,DIRFLT FROM M_TRADER_DOKINBOUND WHERE KODE_TRADER = ".$this->kd_trader." AND STATUS = '01'";
                        $inb = $this->actMain->get_result($SQL,true);
                        $jmlInb = count($inb);
                        if($jmlInb>0){
                            foreach ($inb as $a){
                                switch($a['APRF']){
                                    case'RESPIB09':
                                        $exePIB = $this->RESPIB09($a['DIRFLT'], $this->actMain, $a);
                                        $jmlRcv['RESPIB09'] += $exePIB;
                                        break;
                                    case'RESBC23':
                                        $exeBC23 = $this->RESBC23($a['DIRFLT'], $this->actMain, $a);
                                        $jmlRcv['BC23'] += $exeBC23;
                                        break;
                                    case'RESMANIF':
                                        $exeBC11 = $this->RESMANIF($a['DIRFLT'], $this->actMain, $a);
                                        $jmlRcv['RESMANIF'] += $exeBC11;
                                        break;
                                    case'RESRKSP':
                                        $exeBC10 = $this->RESRKSP($a['DIRFLT'], $this->actMain, $a);
                                        $jmlRcv['RESRKSP']  += $exeBC10;
                                        break;
                                }
                                
                            }
                        }
                        
                    #Execute 1 day 1 time
                        if($flg['LASTDAYEXEC'] < date('Ymd')){
                            unset($arrKey);
                            $arrKey['sessionNumber']= $ws['loginResult'][0];
                            $arrKey['userEDI']      = $this->EDINumber;
                            $arrKey['string0']      = $this->unLockCode;
                            $arrKey['day']          = $flg['DELPOST'];
                            //return print_r($arrKey,true);
                            #deletePostBox
                            $this->nuSoap_lib->call('deletePostbox', $arrKey,'http://www.openuri.org/');
                            
                            #deleteMailBox
                            $arrKey['day']          = $flg['DELMAIL'];
                            $this->nuSoap_lib->call('deleteMailBox', $arrKey,'http://www.openuri.org/');
                            
                            #update Data trader
                            unset($UPD);
                            $UPD['KODE_TRADER']     = $this->kd_trader;
                            $UPD['LASTDAYEXEC']     = date('Ymd');
                            $this->actMain->insertRefernce('M_TRADER', $UPD);
                        }
                        $rtn = ($jmlSnd['DOKPIB09']>0)? $jmlSnd['DOKPIB09']     . ' Dokumen PIB terkirim.<br>':'';
                        $rtn .= ($jmlRcv['RESPIB09']>0)?$jmlRcv['RESPIB09']     . ' Respon PIB diterima <br>':'';
                        $rtn .= ($jmlSnd['BC23']>0)?    $jmlSnd['BC23']         . ' Dokumen BC23 terkirim.<br>':'';
                        $rtn .= ($jmlRcv['RESBC23']>0)? $jmlRcv['DOKPIB09']     . ' Respon BC23 diterima <br>':'';
                        $rtn .= ($jmlSnd['DOKMANIF']>0)?$jmlSnd['DOKMANIF']     . ' Dokumen Manifest terkirim.<br>':'';
                        $rtn .= ($jmlRcv['RESMANIF']>0)?$jmlRcv['RESMANIF']     . ' Respon Manifest diterima <br>':'';
                        $rtn .= ($jmlSnd['DOKRKSP']>0)?$jmlSnd['DOKRKSP']       . ' Dokumen RKSP terkirim.<br>':'';
                        $rtn .= ($jmlRcv['RESRKSP']>0)?$jmlRcv['RESRKSP']       . ' Respon RKSP diterima <br>':'';
                        $rtn .= ($rtn=='')?'Tidak ada Dokumen terkirim atau diterima':'';   
                    }
                    else{
                        $rtn = $msgErr['Login'];
                    }
                }
                else{
                    $rtn = 'Error Login IDX Estreme';
                }
            }
        }
        return $rtn;
    }
    
    function getPathFLT($car,$dok='BC20'){
        $this->load->model('actMain');
        $sql = 'SELECT DIRFLT FROM T_' . $dok ."HDR WHERE CAR = '".$car."'";
        $rtn = $this->actMain->get_uraian($sql,'DIRFLT');
        return $rtn;
    }
    
    function getPathEDI($car,$dok='BC20'){
        $this->load->model('actMain');
        $sql = 'SELECT DIREDI FROM T_' . $dok . "HDR WHERE CAR = '".$car."'";
        $rtn = $this->actMain->get_uraian($sql,'DIREDI');
        return $rtn;
    }
    
    function getPathFileRes($TYPE,$ID,$PARTNER,$APRF){
        $this->load->model('actMain');
        $sql = "SELECT ".$TYPE." as 'DIRFILE' FROM M_TRADER_DOKINBOUND "
                . "WHERE KODE_TRADER = '".$this->kd_trader."' AND "
                . "IDDOK = '".$ID."' AND "
                . "PARTNER = '".$PARTNER."' AND "
                . "APRF = '".$APRF."'";
        $rtn = $this->actMain->get_uraian($sql,'DIRFILE');
        return $rtn;
    }
    
    private function EstablishRelationship($strPartner ,$strAPRF ,$strDirection, $sessionNumber){
        $EstKey['sessionNumber']= $sessionNumber;
        $EstKey['string0']      = $this->unLockCode;
        $EstKey['userEDI']      = $this->EDINumber;
        $EstKey['partnerEDI']   = $strPartner;
        $EstKey['aprf']         = $strAPRF;
        $EstKey['direction']    = $strDirection;
        #Insert Trading Partner
        $ins = $this->nuSoap_lib->call('insertTP', $EstKey,'http://www.openuri.org/');
        switch($ins){
            Case '0':$rtn = '0-Trading Partnership Completed';break;
            Case '1':$rtn = '1-User not authorized';break;
            Case '2':$rtn = '2-Trading partner relationship already exist';break;
            Case '3':$rtn = '3-Invalid partner EDINUMBER';break;
            Case '9':$rtn = '9-Local Error';break;
        }
        return $rtn;
    }
    
    private function ListingRelationship($sessionNumber){
        $EstKey['sessionNumber']= $sessionNumber;
        $EstKey['userEDI']      = $this->EDINumber;
        #Listing Trading Partner
        $lst = $this->nuSoap_lib->call('listRelationship', $EstKey,'http://www.openuri.org/');
        if(is_array($lst['listRelationshipResult']['String'])){
            $rtn = $lst['listRelationshipResult']['String'];
        }
        else{
            $rtn[] = $lst['listRelationshipResult']['String'];
        }
        return $rtn;
    }
    
    private function DeleteRelationship($strPartner ,$strAPRF ,$strDirection,$sessionNumber){
        $EstKey['sessionNumber']= $sessionNumber;
        $EstKey['string0']      = $this->unLockCode;
        $EstKey['userEDI']      = $this->EDINumber;
        $EstKey['partnerEDI']   = $strPartner;
        $EstKey['aprf']         = $strAPRF;
        $EstKey['direction']    = $strDirection;
        #Delete Trading Partner
        $dlt = $this->nuSoap_lib->call('deleteTP', $EstKey,'http://www.openuri.org/');
        return $dlt;
    }
    
    private function sendDokumen($strPartner ,$strAPRF ,$strSnrf ,$strFile ,$sDocType, $sessionNumber){
        if(!file_exists($strFile)){
            $rtn['MSG'] = 'x-[Failed] File Not Found';
            $rtn['KODE']= 'x';
        }
        else{
            $isi = file_get_contents($strFile);
            $EstKey['sessionNumber']= $sessionNumber;
            $EstKey['string0']      = $this->unLockCode;
            $EstKey['userEDI']      = $this->EDINumber;
            $EstKey['partnerEDI']   = $strPartner;
            $EstKey['aprf']         = $strAPRF;
            $EstKey['snrf']         = $strSnrf;
            $EstKey['fileType']     = ($sDocType=='EDIFACT')?'F':'B';
            $EstKey['fileLength']   = filesize($strFile);
            $EstKey['fileStream']   = base64_encode($isi).'=';
            //return print_r($EstKey,true);
            $snd = $this->nuSoap_lib->call('sendDocument', $EstKey,'http://www.openuri.org/');
            $rtn['KODE'] = $snd['sendDocumentResult'];
            switch($rtn['KODE']){
                Case 0:$rtn['MSG'] = '0-[Success] Send Document';break;
                Case 1:$rtn['MSG'] = '1-[Failed] File Corrupt';break;
                Case 2:$rtn['MSG'] = '2-[Failed] User not authorize';break;
                Case 3:$rtn['MSG'] = '3-[Failed] Trading partner does not exists';break;
                Case 4:$rtn['MSG'] = '4-[Failed] Duplicate File';break;
                Case 5:$rtn['MSG'] = '5-[Failed] Invalid TUN Number';break;
                Case 6:$rtn['MSG'] = '6-[Failed] Connect out task';break;
                Case 7:$rtn['MSG'] = '7-[Failed] Business relationship will not permit transmission of the file';break;
                Case 8:$rtn['MSG'] = '8-[Failed] Digital Signature is not valid';break;
                Case 9:$rtn['MSG'] = '9-[Failed] Run time error';break;
            }
        }
        return $rtn;
    }
                
    private function listPostBox($sessionNumber){
        $EstKey['sessionNumber']= $sessionNumber;
        $EstKey['string0']      = $this->unLockCode;
        $EstKey['userEDI']      = $this->EDINumber;
        $lst = $this->nuSoap_lib->call('listPostBox', $EstKey,'http://www.openuri.org/');
        if(is_array($lst['listPostBoxResult']['String'])){
            $rtn = $lst['listPostBoxResult']['String'];
        }
        else{
            $rtn[] = $lst['listPostBoxResult']['String'];
        }
        return $rtn;
    }
    
    private function listMailBox($sessionNumber){
        $mailKey['sessionNumber']   = $sessionNumber;
        $mailKey['string0']         = $this->unLockCode;
        $mailKey['userEDI']         = $this->EDINumber;
        $mailKey['status']          = 'N';
        $lst = $this->nuSoap_lib->call('listMailBox', $mailKey,'http://www.openuri.org/');
        if(is_array($lst['listMailBoxResult']['String'])){
            $rtn = $lst['listMailBoxResult']['String'];
        }
        else{
            if(strtolower($lst['listMailBoxResult']['String'])==='no record found'){
                return '';
            }
            if(strtolower($lst['listMailBoxResult']['String'])==='user not authorized'){
                return '';
            }
            $rtn[] = $lst['listMailBoxResult']['String'];
        }
        return $rtn;
    }
    
    private function RESPIB09($filePath, $actMain, $arrInBound){
        $jmlRes = 0;
        $handle = fopen($filePath, 'r');
        if ($handle) {
            unset($data);
            while (($line = fgets($handle)) !== false) {
                $segment = substr($line, 0, 8);
                switch($segment){
                    case'RES00101':
                        $data['CAR'] = substr($line, 8, 26);
                        break;
                    case'RES00201':
                        switch (substr($line,8,3)){
                            case'137':
                                $data['RESTG'] = substr($line, 17, 2) . '-' . substr($line, 15, 2) . '-' . substr($line, 11, 4);
                                $data['RESWK'] = substr($line, 19, 6);
                                break;
                            case'140':
                                $data['JATUHTEMPO'] = substr($line, 11, 4) . '-' . substr($line, 15, 2) . '-' . substr($line, 17, 2);
                                break;
                            }
                        break;
                    case'RES00301':
                        $data['DESKRIPSI'][] = substr($line, 33);
                        switch (substr($line,8,4)){
                            case 'AAG1':$data['S_JNSBRG'] = substr($line, 33);break;
                            case 'AAG2':$data['S_JMLBRG'] = substr($line, 33);break;
                            case 'AAG3':$data['S_TARIF']  = substr($line, 33);break;
                            case 'AAG4':$data['S_NILPAB'] = substr($line, 33);break;
                        }
                        if(substr($line,0,11)==='RES00301ACB'){$data['NoTelFax'] = substr($line, 11, 33);}
                        break;
                    case'RES00401':break;
                    case'RES00501':
                        switch (substr($line, 8, 3)){
                            case'22 ':
                                $data['KPBC'] = substr($line, 11, 6);
                                $data['RESKD']= substr($line, 17, 3);
                                break;
                            case'14 ':
                                $data['KDGUDANG'] = substr($line, 11, 4);
                                break;
                        }
                        break;
                    case'RES00601':
                        if(substr($line,8,3)==='CN '){
                            $data['CONTNO'][] = substr($line, 11, 17);
                            $data['CONTUKUR'][] = substr($line, 28, 2);
                        }
                        break;
                    case'RES00701':
                        switch (substr($line,8,3)){
                            case'PFD':
                            case'KPK':
                                $data['NIP1']       = substr($line, 11, 14);
                                $data['PEJABAT1']   = substr($line, 25, 50);
                                $data['JABATAN1']   = substr($line, 75, 70);
                                break;
                            case'PFB':
                            case'PPB':
                                $data['NIP2']       = substr($line, 11, 14);
                                $data['PEJABAT2']   = substr($line, 25, 50);
                                break;
                        }
                        break;
                    case'RES00801':
                        switch (substr($line,8,3)){
                            case'ABD':
                                $data['PIBNO']  = substr($line, 11, 6);
                                $data['PIBTG']  = substr($line, 47, 2) . '-' . substr($line, 45, 2) . '-' . substr($line, 41, 4);
                                break;
                            case'DM ':
                            case'ABI':
                                $data['DOKRESNO'] = substr($line, 11, 30);
                                $data['DOKRESTG'] = substr($line, 47, 2) . '-' . substr($line, 45, 2) . '-' . substr($line, 41, 4);
                                break;
                        }
                        break;
                    case'RES00901':
                        switch (substr($line,8,6)){
                            case'1  CUD':case'1  CDH':
                                switch(trim(substr($line, 32, 2))){
                                    case'7':$data['BMBYR']     = substr($line, 17, 15); break;
                                    case'9':$data['BM_ASAL']   = substr($line, 17, 15); break;
                                    case'29':$data['BM_KURANG'] = substr($line, 17, 15); break;
                                    case'30':$data['BM_LEBIH']  = substr($line, 17, 15); break;
                                }
                                break;
                            case'1  XHT':case'1  XME':case'1  XET':case'1  XCL':
                                switch(trim(substr($line, 32, 2))){
                                    case'7':$data['CUKBYR']    = substr($line, 17, 15); break;
                                    case'9':$data['CUK_ASAL']  = substr($line, 17, 15); break;
                                    case'29':$data['CUK_KURANG']= substr($line, 17, 15); break;
                                    case'30':$data['CUK_LEBIH'] = substr($line, 17, 15); break;
                                }
                                break;
                            case'1  VAT':
                                switch(trim(substr($line, 32, 2))){
                                    case'7':$data['PPNBYR']    = substr($line, 17, 15); break;
                                    case'9':$data['PPN_ASAL']  = substr($line, 17, 15); break;
                                    case'29':$data['PPN_KURANG']= substr($line, 17, 15); break;
                                    case'30':$data['PPN_LEBIH'] = substr($line, 17, 15); break;
                                }
                                break;
                            case'1  LXT':
                                switch(trim(substr($line, 32, 2))){
                                    case'7':$data['PPNBMBYR']    = substr($line, 17, 15); break;
                                    case'9':$data['PPNBM_ASAL']  = substr($line, 17, 15); break;
                                    case'29':$data['PPNBM_KURANG']= substr($line, 17, 15); break;
                                    case'30':$data['PPNBM_LEBIH'] = substr($line, 17, 15); break;
                                }
                                break;
                            case'1  ICT':
                                switch(trim(substr($line, 32, 2))){
                                    case'7':$data['PPHBYR']    = substr($line, 17, 15); break;
                                    case'9':$data['PPH_ASAL']  = substr($line, 17, 15); break;
                                    case'29':$data['PPH_KURANG']= substr($line, 17, 15); break;
                                    case'30':$data['PPH_LEBIH'] = substr($line, 17, 15); break;
                                }
                                break;
                            case'1  DAM':$data['DENDA'] = substr($line, 17, 15);break;
                        }
                    break;
                    case'UNZ00101':
                        $jmlRes++;
                        #insert data
                        $data['STATUS'] = $this->kdRes2Stat($data['RESKD']);
                        
                        #update Headr BC20
                        unset($BC20);
                        $BC20['KODE_TRADER']= $this->kd_trader;
                        $BC20['CAR']        = $data['CAR'];
                        $BC20['PIBNO']      = $data['PIBNO'];
                        $BC20['PIBTG']      = $data['PIBTG'];
                        $BC20['STATUS']     = $data['STATUS'];
                        $actMain->insertRefernce('t_bc20hdr',$BC20);
                        unset($BC20);
                        
                        #Respon
                        unset($BC20RES);
                        $BC20RES['KODE_TRADER'] = $this->kd_trader;
                        $BC20RES['CAR']         = $data['CAR'];
                        $BC20RES['RESKD']       = $data['RESKD'];
                        $BC20RES['RESTG']       = $data['RESTG'];
                        $BC20RES['RESWK']       = $data['RESWK'];
                        $BC20RES['DOKRESNO']    = $data['DOKRESNO'];
                        $BC20RES['DOKRESTG']    = $data['DOKRESTG'];
                        $BC20RES['KPBC']        = $data['KPBC'];
                        $BC20RES['PIBNO']       = $data['PIBNO'];
                        $BC20RES['PIBTG']       = $data['PIBTG'];
                        $BC20RES['PEJABAT1']    = $data['PEJABAT1'];
                        $BC20RES['JABATAN1']    = $data['JABATAN1'];
                        $BC20RES['NIP1']        = $this->ConvertNIP($data['NIP1']);
                        if($data['RESKD']===''){
                            $BC20RES['PEJABAT2']= $data['NOTELFAX'];
                        }
                        $BC20RES['DESKRIPSI']   = implode("\n", $data['DESKRIPSI']) ;
                        $BC20RES['JATUHTEMPO']  = $data['JATUHTEMPO'];
                        $BC20RES['KDGUDANG']    = $data['KDGUDANG'];
                        $BC20RES['KOMTG']       = date('d-m-Y');
                        $BC20RES['KOMWK']       = date('His');
                        $actMain->insertRefernce('t_bc20res',$BC20RES);
                        unset($BC20RES);
                        
                        #kode respon [300|310|450]
                        if($data['RESKD']==='300' || $data['RESKD']==='310' || $data['RESKD']==='450'){
                            foreach ($data['CONTNO'] as $k=>$a){
                                unset($BC20CONR);
                                $BC20CONR['KODE_TRADER']= $this->kd_trader;
                                $BC20CONR['CAR']        = $data['CAR'];
                                $BC20CONR['RESKD']      = $data['RESKD'];
                                $BC20CONR['CONTNO']     = $data['CONTNO'][$k];
                                $BC20CONR['CONTUKUR']   = $data['CONTUKUR'][$k];
                                $actMain->insertRefernce('t_bc20conr',$BC20CONR);
                                unset($BC20CONR);
                            }
                        }
                        #kode respon [500]
                        if($data['RESKD']==='500'){
                            unset($BC20NPT);
                            $BC20NPT['KODE_TRADER'] = $this->kd_trader;
                            $BC20NPT['CAR']         = $data['CAR'];
                            $BC20NPT['RESKD']       = $data['RESKD'];
                            $BC20NPT['RESTG']       = $data['RESTG'];
                            $BC20NPT['RESWK']       = $data['RESWK'];

                            $BC20NPT['BMBYR']       = $data['BMBYR'];
                            $BC20NPT['CUKBYR']      = $data['CUKBYR'];
                            $BC20NPT['PPNBYR']      = $data['PPNBYR'];
                            $BC20NPT['PPNBMBYR']    = $data['PPNBMBYR'];
                            $BC20NPT['PPHBYR']      = $data['PPHBYR'];
                            $BC20NPT['DENDA']       = $data['DENDA'];

                            $BC20NPT['BM_ASAL']     = $data['BM_ASAL'];
                            $BC20NPT['CUK_ASAL']    = $data['CUK_ASAL'];
                            $BC20NPT['PPN_ASAL']    = $data['PPN_ASAL'];
                            $BC20NPT['PPNBM_ASAL']  = $data['PPNBM_ASAL'];
                            $BC20NPT['PPH_Asal']    = $data['PPH_ASAL'];

                            $BC20NPT['BM_KURANG']   = $data['BM_KURANG'];
                            $BC20NPT['CUK_KURANG']  = $data['CUK_KURANG'];
                            $BC20NPT['PPN_KURANG']  = $data['PPN_KURANG'];
                            $BC20NPT['PPNBM_KURANG']= $data['PPNBM_KURANG'];
                            $BC20NPT['PPH_KURANG']  = $data['PPH_KURANG'];

                            $BC20NPT['BM_LEBIH']    = $data['BM_LEBIH'];
                            $BC20NPT['CUK_LEBIH']   = $data['CUK_LEBIH'];
                            $BC20NPT['PPN_LEBIH']   = $data['PPN_LEBIH'];
                            $BC20NPT['PPNBM_LEBIH'] = $data['PPNBM_LEBIH'];
                            $BC20NPT['PPH_LEBIH']   = $data['PPH_LEBIH'];

                            $BC20NPT['S_JNSBRG']    = $data['S_JNSBRG'];
                            $BC20NPT['S_JMLBRG']    = $data['S_JMLBRG'];
                            $BC20NPT['S_TARIF']     = $data['S_TARIF'];
                            $BC20NPT['S_NILPAB']    = $data['S_NILPAB'];
                            $actMain->insertRefernce('t_bc20npt',$BC20NPT);
                            unset($BC20NPT);
                        }
                        $arrInBound['KODE_TRADER']= $this->kd_trader;
                        $arrInBound['CAR']      = $data['CAR'];
                        $arrInBound['STATUS']   = '02';
                        $actMain->insertRefernce('M_TRADER_DOKINBOUND',$arrInBound);
                        unset($data);
                    break;
                }
            }
        }
        fclose($handle);
        return $jmlRes;
    }
    
    private function RESBC23($filePath, $actMain, $arrInBound){
        $jmlRes = 0;
        $handle = fopen($filePath, 'r');
        if ($handle) {
            unset($data);
            while (($line = fgets($handle)) !== false) {
                $segment = substr($line, 0, 8);
                switch($segment){
                    case'ENV00101':
                        
                        unset($data);
                    case'RES00101':
                        $data['CAR'] = substr($line, 8, 26);
                        $data['RESKD'] = substr($line, 34, 10);                        
                        break;
                    case'RES00201':
                        switch (substr($line,8,3)){
                            case'137':
                                $data['RESTG'] = substr($line, 17, 2) . '-' . substr($line, 15, 2) . '-' . substr($line, 11, 4);
                                $data['RESWK'] = substr($line, 19, 6);
                                break;
                        }
                        break;
                    case'RES00301':                        
                        switch (substr($line,8,4)){                            
                            case 'AAI':$data['DESKRIPSI'] = substr($line, 8,3);break;                        
                            case 'AAE':$data['JUDUL'] = substr($line, 11,70);break;
                            case 'Z01':break;
                            case 'Z02':break;
                            case 'Z03':break;
                            case 'Z04':break;
                            case 'Z05':break;
                            case 'Z06':$data['KDRAHASIA'] = substr($line, 11,30);break;
                        }                        
                    case'RES00311':
                        switch (substr($line,8,3)){
                            case'20':break;
                        }
                        break;
                    case'RES00401':
                        switch (substr($line,8,3)){
                            case'22':
                                $data['KPBC'] = substr($line, 11, 6);
                                $data['KOTA'] = substr($line, 17, 20);
                                break;
                            case'43':$data['KPBCAWAS'] = substr($line, 11, 6);break;
                            case'14':break;
                            case'41':$data['KPBCBONGKAR'] = substr($line, 11, 6);break;
                            case'Z02':$data['TUJUAN'] = substr($line, 11, 2);break;
                            case'11':$data['PELBONGKAR'] = substr($line, 11, 6);break;                            
                        }
                        break;
                    
                    case'RES00501':break;
                    case'RES00601':
                        switch (substr($line,8,3)){
                            case'PPD':
                                $data['NIP1']       = trim(substr($line, 11, 14));
                                $data['PEJABAT1']   = substr($line, 27, 20);
                                $data['JABATAN1']   = substr($line, 78, 50);
                                break;
                            case'PDL':
                                $data['NIP2']       = substr($line, 11, 14);
                                $data['PEJABAT2']   = substr($line, 27, 20);
                                break;
                            case'SU':
                                $data['IDPENGIRIM']      = substr($line, 11, 1);
                                $data['NPWPPENGIRIM']    = substr($line, 11, 15);
                                $data['NAMAPENGIRIM']    = substr($line, 27, 50);
                                $data['ALAMATPENGIRIM']  = substr($line, 77, 70);
                                break;
                            case'CB':break;
                            case'CN':
                                $data['IDPENERIMA']     = substr($line, 11, 1);
                                $data['NPWPPENERIMA']   = substr($line, 12, 15);
                                $data['NAMAPENERIMA']   = substr($line, 27, 50);
                                $data['ALAMATPENERIMA'] = substr($line, 77, 70);
                                break;
                        }                        
                        break;
                    
                    case'RES00701':     #RES00701CN 
                        if(substr($line,8,3)==='CN '){
                            $data['CONTNO'][]   = substr($line, 11, 17);
                            $data['CONTUKUR'][] = substr($line, 28, 2);
                            $data['JNSEGEL'][]  = substr($line, 30, 1);
                            $data['NOSEGEL'][]  = substr($line, 31, 15);
                            $data['TIPE'][]     = substr($line, 47, 1);
                        }
                        break;
                    case'RES00801':
                        if(substr($line,8,3) === '914'){
                            $data['JMKEMAS'][]      = substr($line, 11, 8);
                            $data['JNKEMAS'][]      = substr($line, 19, 2);
                            $data['MERKKEMAS'][]    = substr($line, 21, 35);
                            $data['NOSEGEL'][]      = substr($line, 57, 15);
                        } 
                        break;
                    case'RES00901':
                        switch(substr($line,8,3)){
                            case 'ABT':
                                $data['NODAFTAR']  = substr($line, 11, 6);
                                $data['TGDAFTAR']  = substr($line, 50, 2) . '/' . substr($line, 48, 2) . '/' . substr($line, 44, 4);
                            break;
                            case 'ADU':
                                $data['NOPPJK']  = substr($line, 11, 6);  
                                $data['TGPPJK']  = substr($line, 50, 2) . '/' . substr($line, 48, 2) . '/' . substr($line, 44, 4);
                            break;
                            case'AME';
                                $data['NOSETUJU']  = substr($line, 11, 6);
                                $data['TGSETUJU']  = substr($line, 50, 2) . '/' . substr($line, 48, 2) . '/' . substr($line, 44, 4);
                            break;
                            case'AHZ';
                                $data['DOKRESNO']  = substr($line, 11, 6);  
                                $data['DOKRESTG']  = substr($line, 50, 2) . '/' . substr($line, 48, 2) . '/' . substr($line, 44, 4);
                            break;
                            default :
                                $data['KDDOK']   = substr($line, 8, 3);
                                $data['NODOK']   = substr($line, 11, 30);                        
                                $data['TGLDOK']  = substr($line, 50, 2) . '/' . substr($line, 48, 2) . '/' . substr($line, 44, 4);    
                                break;
                        }
                        break;
                                    
                    case'RES00901':                        
                        switch (substr($line,8,6)){
                            case'1 CUD':$data['BMBYR']     = substr($line, 17, 15); break;                                   
                            case'1 CDH':$data['BMBYR']     = substr($line, 17, 15); break;                                                             
                            case'1 XHT':$data['CUKBYR']    = substr($line, 17, 15); break;
                            case'1 XME':$data['CUKBYR']    = substr($line, 17, 15); break;
                            case'1 XET':$data['CUKBYR']    = substr($line, 17, 15); break;
                            case'1 XCL':$data['CUKBYR']    = substr($line, 17, 15); break;                                
                            case'1 VAT':$data['PPNBYR']    = substr($line, 17, 15); break;
                            case'1 LXT':$data['PPNBMBYR']  = substr($line, 17, 15); break;
                            case'1 ICT':$data['PPHBYR']    = substr($line, 17, 15); break;                                                                                                    
                            case'1 DAM':$data['DENDA']     = substr($line, 17, 15);break;
                        }
                    break;
                
                    case 'RES01001':
                        switch (substr($line,8,2)){
                            case'AL':$data['CONTAKPERSON'] = substr($line, 11, 30);break;                                                
                        }                                                
                        break;
                    
                    case 'RES01301':
                        switch (substr($line,8,2)){
                            case'TE':$data['NOPHONE'] = substr($line, 8, 50);break;
                            case'FX':$data['NOFAX'] = substr($line, 8, 50);break;
                            case'EM':$data['EMAIL'] = substr($line, 8, 50);break;
                        }                                                
                        break;
                    
                    case 'RES01101':
                        switch (substr($line,11,2)){
                            case'AAD':$data['BRUTO'] = substr($line, 17,18);break;
                            case'AAC':$data['NETTO'] = substr($line, 17,18);break;                            
                        }                                                
                        break;
                
                    case'UNZ00101':
                        if(isset($data)){
                            $jmlRes++;
                            switch ($data['RESKD']){
                                case 'NPPRIS': $data['RESKD'] = '100';  # Respon NPP/Reject
                                case 'RCVRIS': $data['RESKD'] = '110';  # Respon Penerimaan
                                case 'NPDRIS': $data['RESKD'] = '230';  # Respon NPPD / Aju dokumen
                                case 'SPMRIS': $data['RESKD'] = '290';  # Respon SPPB Merah
                                case 'SPBRIS': $data['RESKD'] = '300';  # Respon SPPB
                                case 'CELRIS': $data['RESKD'] = '470';  # Respon Pembatalan
                                case 'NPSRIS': $data['RESKD'] = '600';  # Respon Penolakan Perbaikan
                                case 'NPRRIS': $data['RESKD'] = '700';  # Respon Penerimaan Perbaikan                            
                                case 'SPDRIS': $data['RESKD'] = '295';  # Respon SPPD SPPBM
                                case 'LHPRIS': $data['RESKD'] = '810';  # Respon LHP
                                case 'GENRIS': $data['RESKD'] = '900';  # Respon Umum
                            }
                            switch ($data['RESKD']){
                                case'100':$data['STATUS']='06';break;
                                case'110':$data['STATUS']='07';break;
                                case'120':$data['STATUS']='08';break;
                                case'130':$data['STATUS']='09';break;
                                case'200':$data['STATUS']='09';break;
                                case'210':$data['STATUS']='09';break;
                                case'220':$data['STATUS']='09';break;
                                case'230':$data['STATUS']='09';break;
                                case'240':$data['STATUS']='09';break;
                                case'270':$data['STATUS']='10';break;
                                case'280':$data['STATUS']='11';break;
                                case'290':$data['STATUS']='12';break;
                                case'295':$data['STATUS']=$data['RESKD'];break;
                                case'300':$data['STATUS']='13';break;
                                case'310':$data['STATUS']='14';break;
                                case'470':$data['STATUS']='15';break;
                                case'500':$data['STATUS']='16';break;
                                case'600':$data['STATUS']='17';break;
                                case'700':$data['STATUS']='18';break;
                                case'900':$data['STATUS']=$data['RESKD'];break;
                            }
                        
                        
                            #insert data
                            #$data['STATUS'] = $this->kdRes2Stat2();

                            #update Header BC23
                            unset($BC23);
                            $BC23['KODE_TRADER']    = $this->kd_trader;
                            $BC23['CAR']            = $data['CAR'];
                            $BC23['STATUS']         = $data['STATUS'];
                            $actMain->insertRefernce('t_bc23hdr',$BC23);
                            unset($BC23);

                            if($data['RESKD']==='300' || $data['RESKD']==='290'){
                                $arrDel['CAR']      = $data['CAR'];
                                $arrDel['JNDOK']    = '1';
                                $arrDel['CAR']      = $data['CAR'];

                                $this->db->simple_query("DELETE FROM t_bc23restpb WHERE CAR= '".$arrDel['CAR']."' AND KODE_TRADER='".$this->kd_trader."' AND JNDOK= '1' AND RESKD='300' AND RESKD='290'" );
                                $this->db->where($arrDel);
                                $tbl    = explode(',','t_bc23ressppbc,t_bc23ressppbh,t_bc23ressppbk');
                                $exec = $this->db->delete($tbl);
                            }

                            #Respon
                            unset($BC23RES);
                            $BC23RES['KODE_TRADER'] = $this->kd_trader;
                            $BC23RES['CAR']         = $data['CAR'];
                            $BC23RES['JNDOK']       = '1';
                            $BC23RES['RESKD']       = $data['RESKD'];
                            $BC23RES['RESTG']       = $data['RESTG'];
                            $BC23RES['RESWK']       = $data['RESWK'];
                            $BC23RES['DOKRESNO']    = $data['DOKRESNO'];
                            $BC23RES['DOKRESTG']    = $data['DOKRESTG'];
                            $BC23RES['NMPTG']       = $data['PEJABAT1'];
                            $BC23RES['NIPPTG']      = $this->ConvertNIP($data['NIP1']);
                            if($data['RESKD']=== '470'){
                                $BC23RES['NORESPON']= trim($data['NOSETUJU']);
                                $BC23RES['TGRESPON']= $data['TGSETUJU'];
                            }
                            else{
                                $BC23RES['NORESPON']= $data['DOKRESNO'];    
                                $BC23RES['TGRESPON']= $data['DOKRESTG'];    
                            }                        
                            $BC23RES['KDKTR']       = $data['KPBC'];
                            $BC23RES['NODAFT']      = $data['NODAFTAR'];                                                   
                            $BC23RES['TGDAFT']      = $data['TGDAFTAR'];                                                   
                            $BC23RES['DESKRIPSI']   = implode("\n", $data['DESKRIPSI']) ;                                                
                            $BC23RES['KOTA']        = trim($data['KOTA']);
                            $BC23RES['KOMTG']       = date('d-m-Y');
                            $BC23RES['KOMWK']       = date('His');
                            $actMain->insertRefernce('t_bc23restpb',$BC23RES);
                            unset($BC23RES);


                             #kode respon [500]
                            if($data['RESKD']==='500'){
                                unset($BC23RESNOTA);
                                $BC23RESNOTA['KODE_TRADER'] = $this->kd_trader;
                                $BC23RESNOTA['CAR']         = $data['CAR'];
                                $BC23RESNOTA['RESKD']       = $data['RESKD'];
                                $BC23RESNOTA['RESTG']       = $data['RESTG'];
                                $BC23RESNOTA['RESWK']       = $data['RESWK'];
                                $BC23RESNOTA['BMBYR']       = $data['BMBYR'];
                                $BC23RESNOTA['CUKBYR']      = $data['CUKBYR'];
                                $BC23RESNOTA['PPNBYR']      = $data['PPNBYR'];
                                $BC23RESNOTA['PPNBMBYR']    = $data['PPNBMBYR'];
                                $BC23RESNOTA['PPHBYR']      = $data['PPHBYR'];
                                $BC23RESNOTA['DENDA']       = $data['DENDA'];
                                $actMain->insertRefernce('t_bc23resnotul',$BC23RESNOTA);
                                unset($BC23RESNOTA);
                            }





                            $arrInBound['KODE_TRADER']  = $this->kd_trader;
                            $arrInBound['CAR']          = $data['CAR'];
                            $arrInBound['STATUS']       = '02';
                            $actMain->insertRefernce('M_TRADER_DOKINBOUND',$arrInBound);
                            unset($data);
                        }
                    break;
                }
            }
        }
        fclose($handle);
        return $jmlRes;
    }
    
    private function RESRKSP($filePath, $actMain, $arrInBound){
        $jmlRes = 0;
        $handle = fopen($filePath, 'r');
        if ($handle) {
            unset($data);
            while (($line = fgets($handle)) !== false){
                $segment = substr($line, 0, 8);
                switch($segment){
                    case'ENV00101':
                        unset($data);
                    case'RES00101':
                        $data['CAR']    = substr($line, 8, 26);
                        break;
                    case'RES00201':
                        switch (substr($line,8,3)){
                            case'137':
                            case'37 ':
                                $data['RESTG'] = substr($line, 17, 2) . '-' . substr($line, 15, 2) . '-' . substr($line, 11, 4);
                                $data['RESWK'] = substr($line, 19, 6);
                            break;
                        }
                        break;
                    case'RES00301':
                        switch (substr($line,8,3)){                            
                            case 'AAG':$data['DESKRIPSI'] = substr($line, 12,120);break;                        
                        }
                    case'RES00401':
                        $data['KDKPBC'] = substr($line, 8, 6);
                        $data['RESKD']  = substr($line, 14, 3);
                        break;
                    case'RES00701':
                        switch (substr($line,8,3)){
                            case 'AFI':
                                $data['NORKSP']  = substr($line, 17, 6);
                                $data['TGRKSP']  = substr($line, 37, 2) . '-' . substr($line, 35, 2). '-' . substr($line, 31, 4);
                            break;
                        }
                        break;
                    case'UNZ00101':
                        if(isset($data)){
                            $jmlRes++;
                            #insert data
                            #update Header BC10
                            unset($BC10);
                            $BC10['KODE_TRADER']    = $this->kd_trader;
                            $BC10['CAR']            = $data['CAR'];
                            $BC10['STATUS']         = $data['RESKD'];
                            $BC10['NOBC10']         = $data['NORKSP'];
                            $BC10['TGBC10']         = $data['TGRKSP'];
                            $actMain->insertRefernce('t_bc10hdr',$BC10);
                            unset($BC10);

                            #Respon
                            unset($BC10RES);
                            $BC10RES['KODE_TRADER'] = $this->kd_trader;
                            $BC10RES['CAR']         = $data['CAR'];
                            $BC10RES['RESKD']       = $data['RESKD'];
                            $BC10RES['RESTG']       = $data['RESTG'];
                            $BC10RES['RESWK']       = $data['RESWK'];
                            $BC10RES['KPBC']        = $data['KDKPBC'];
                            $BC10RES['NORKSP']      = $data['NORKSP'];
                            $BC10RES['TGRKSP']      = $data['TGRKSP'];
                            $BC10RES['KOMTG']       = date('d-m-Y');
                            $BC10RES['KOMWK']       = date('His');
                            $BC10RES['DESKRIPSI']   = $data['DESKRIPSI'];
                            $actMain->insertRefernce('t_bc10res',$BC10RES);
                            unset($BC10RES);

                            $arrInBound['KODE_TRADER']  = $this->kd_trader;
                            $arrInBound['CAR']          = $data['CAR'];
                            $arrInBound['STATUS']       = '02';
                            $actMain->insertRefernce('m_trader_dokinbound',$arrInBound);
                            unset($data);
                        }
                    break;
                }
            }
        }
        fclose($handle);
        return $jmlRes;
    }
    
    private function RESMANIF($filePath, $actMain, $arrInBound){
        $jmlRes = 0;
        $handle = fopen($filePath, 'r');
        if ($handle) {
            unset($data);
            while (($line = fgets($handle)) !== false){
                $segment = substr($line, 0, 8);
                switch($segment){
                    case'ENV00101':
                        unset($data);
                        $data['SENDER']     = substr($line, 8, 20);
                        $data['RECEIVER']   = substr($line, 28, 20);
                    case'RES00101':
                        $data['CAR']        = substr($line, 8, 26);
                        $data['MSGCODE']    = substr($line, 34, 2);
                        break;
                    case'RES00201':
                        switch (substr($line,8,3)){
                            case'137':
                                $data['RESTG'] = substr($line, 17, 2) . '-' . substr($line, 15, 2) . '-' . substr($line, 11, 4);
                                $data['RESWK'] = substr($line, 19, 6);
                            break;
                            case'111':
                                $data['MANIFTG'] = substr($line, 17, 2) . '-' . substr($line, 15, 2) . '-' . substr($line, 11, 4);
                                $data['MANIFWK'] = substr($line, 19, 6);
                            break;
                            case'265':
                                $data['DUETG'] = substr($line, 17, 2) . '-' . substr($line, 15, 2) . '-' . substr($line, 11, 4);
                                $data['DUEWK'] = substr($line, 19, 6);
                            break;
                        }
                        break;
                    case'RES00301':
                        switch (substr($line,8,3)){                            
                            case 'AAG':$data['DESKRIPSI'] .= substr($line, 12,120);break;
                        }
                    case'RES00401':
                        $data['KDKPBC'] = substr($line, 8, 6);
                        $data['RESKD']  = substr($line, 14, 3);
                        break;
                    case'RES00701':
                        switch (substr($line,8,3)){
                            case 'AFB':
                                $data['NOBC11']  = substr($line, 17, 6);
                                $data['TGBC11']  = substr($line, 37, 2) . '-' . substr($line, 35, 2). '-' . substr($line, 31, 4);
                            break;
                        }
                        break;
                    case'RES01001':
                        switch (trim(substr($line,25,3))){
                            case 'POS': $data['JMLPOS']  = substr($line, 28, 5)+0;break;
                            case 'CN':  $data['JMLCON']  = substr($line, 28, 5)+0;break;
                        }
                        break;
                    case'UNZ00101':
                        if(isset($data)){
                            $jmlRes++;
                            #insert data
                            #update Header BC10
                            unset($BC11);
                            $BC11['KODE_TRADER']    = $this->kd_trader;
                            $BC11['CAR']            = $data['CAR'];
                            $BC11['STATUS']         = $data['RESKD'];
                            $BC11['NOBC11']         = $data['NOBC11'];
                            $BC11['TGBC11']         = $data['TGBC11'];
                            $actMain->insertRefernce('t_bc11hdr',$BC11);
                            unset($BC11);

                            #Respon
                            unset($BC11RES);
                            $BC11RES['KODE_TRADER'] = $this->kd_trader;
                            $BC11RES['CAR']         = $data['CAR'];
                            $BC11RES['RESKD']       = $data['RESKD'];
                            $BC11RES['RESTG']       = $data['RESTG'];
                            $BC11RES['RESWK']       = $data['RESWK'];
                            $BC11RES['KPBC']        = $data['KDKPBC'];
                            $BC11RES['NOBC11']      = $data['NOBC11'];
                            $BC11RES['TGBC11']      = $data['TGBC11'];
                            $BC11RES['KOMTG']       = date('d-m-Y');
                            $BC11RES['KOMWK']       = date('H:i:s');
                            $BC11RES['DESKRIPSI']   = $data['DESKRIPSI'];
                            $actMain->insertRefernce('t_bc11res',$BC11RES);
                            unset($BC11RES);

                            $arrInBound['KODE_TRADER']  = $this->kd_trader;
                            $arrInBound['CAR']          = $data['CAR'];
                            $arrInBound['STATUS']       = '02';
                            $actMain->insertRefernce('m_trader_dokinbound',$arrInBound);
                            unset($data);
                        }
                    break;
                }
            }
        }
        fclose($handle);
        return $jmlRes;
        return 1;
    }
    
    private function BACAPEBRES($filePath, $actMain, $arrInBound){
        $jmlRes = 0;
        $handle = fopen($filePath, 'r');
        if ($handle) {
            unset($data);
            while (($line = fgets($handle)) !== false) {
                $segment = substr($line, 0, 8);
                switch($segment){
                    case'ENV':
                        switch ($data['RESKD']) {
                            case'40':$data['STATUS'] = '10';
                                break;
                            case'50':$data['STATUS'] = '15';
                                break;
                            case'10':$data['STATUS'] = '20';
                                break;
                            case'20':$data['STATUS'] = '30';
                                break;
                            case'30':$data['STATUS'] = '900';
                                break;
                            #case'45':case'55':$data['STATUS']='09';break;
                            case'60':$data['STATUS'] = '70';
                                break;
                            case'70':$data['STATUS'] = '60';
                                break;
                            case'80':$data['STATUS'] = '17';
                                break;
                        }
                        unset($data);
                    case'RES00101':
                        $data['CAR'] = substr($line, 8, 26);
                        #$data['RESKD'] = substr($line, 34, 10);                        
                        break;
                    case'RES00201':
                       switch (substr($line, 8, 3)) {
                           case'137':
                               $data['RESTG'] = substr($line, 17, 2) . '-' . substr($line, 15, 2) . '-' . substr($line, 11, 4);
                               $data['RESWK'] = substr($line, 19, 6);
                               break;
                           case'129':
                               $data['TGEKS'] = substr($line, 17, 2) . '-' . substr($line, 15, 2) . '-' . substr($line, 11, 4);
                               break;
                           case'351':
                               $data['TGSIAP'] = substr($line, 17, 2) . '-' . substr($line, 15, 2) . '-' . substr($line, 11, 4);
                               break;
                           case'411':
                               $data['TGSTUFF'] = substr($line, 17, 2) . '-' . substr($line, 15, 2) . '-' . substr($line, 11, 4);
                               break;
                       }
                       break;    
                    case'RES00301':
                        switch (substr($line, 8, 3)) {
                            case 'PAC':
                                $data['MERK'] = substr($line, 11, 100);
                                break;
                            case 'ACX':
                                $data['SERIUR'] = '0'; #FIELD APA ?
                                $data['URAIAN'] = substr($line, 16, 60);
                                break;
                            case 'ACD':
                                $data['SERIUR'] = substr($line, 11, 5);
                                $data['URAIAN'] = substr($line, 16, 60);
                                break;
                            default :
                                $data['SERIUR'] = substr($line, 11, 5);
                                $data['URAIAN'] = substr($line, 16, 60);
                                break;
                        }    
                    case'RES00401':
                        $data['MODA'] = substr($line, 11, 1);
                        $data['VOY'] = substr($line, 12, 7);
                        $data['CARRIER'] = substr($line, 19, 20);
                        break;    
                    case'RES00501':
                        switch (substr($line, 8, 3)) {
                            case '22':
                                $data['KDKTR'] = substr($line, 11, 6);
                                $data['LOKKTR'] = substr($line, 17, 20); #ini dapet dari mana ?
                                break;
                            case '42':
                                $data['KTRGATE'] = substr($line, 11, 6);
                                break;
                            case '43':
                                $data['KTRPRIKS'] = substr($line, 11, 6);
                                break;
                            case '9  ':
                                $data['PELMUAT'] = substr($line, 11, 5);
                                $data['ALMTSTUFF'] = substr($line, 17, 70);
                                break;
                            case '11':
                                $data['PELBONGKAR'] = substr($line, 11, 5);
                                break;
                            case '13':
                                $data['PELTRANSIT'] = substr($line, 11, 5);
                                break;
                            case '14':
                                $data['LOKBRG'] = substr($line, 11, 1);
                                $data['GUDANG'] = substr($line, 12, 1);
                                $data['ALMTSIAP'] = substr($line, 17, 70);
                                break;
                        }
                        break;
                    case'RES00601':
                        $data['JNEKS'] = substr($line, 11, 1);
                        break;    
                    case'RES00701':     #RES00701CN 
                        switch (substr($line, 8, 3)) {
                            case 'AX':
                                $data['NIPTTDRES'] = substr($line, 11, 14);
                                $data['NAMATTDRES'] = substr($line, 27, 30);
                                $data['NMKOMP'] = substr($line, 57, 10);
                                break;
                            case 'EX':
                                $data['IDEKS'] = substr($line, 11, 1);
                                $data['NPWPEKS'] = substr($line, 12, 15);
                                $data['NAMAEKS'] = substr($line, 27, 50);
                                $data['ALMTEKS'] = substr($line, 77, 70);
                                break;
                            case 'CS':
                                $data['IDEKS'] = substr($line, 11, 1);
                                $data['NPWPEKS'] = substr($line, 12, 15);
                                $data['NAMAEKS'] = substr($line, 27, 50);
                                $data['ALMTEKS'] = substr($line, 77, 70);
                                break;
                            case 'FO':
                                $data['NIPPRIKS'] = substr($line, 11, 14);
                                $data['NAMAPRIKS'] = substr($line, 27, 50);
                                $data['ALMTKPBC'] = substr($line, 77, 70);
                                break;
                            case 'CM':
                                $data['NAMAKANWIL'] = substr($line, 27, 50);
                                $data['NAMAKPBC'] = substr($line, 77, 50);
                                break;
                            case 'CB':
                                $data['NPWPPPJ'] = substr($line, 11, 15);
                                break;
                        }
                        break;
                    case'RES00801':
                        switch (substr($line, 8, 2)) {
                            case 'EX':
                                if (substr($line, 50, 2) === 'TE') {
                                    $data['PETUGASEX'][] = substr($line, 10, 40);
                                    $data['NOPHONEEX'][] = substr($line, 53, 20);
                                } else if (substr($line, 50, 2) === 'FX') {
                                    $data['NOFAX'][] = substr($line, 53, 20);
                                }
                                break;
                            case 'BC':
                                if (substr($line, 50, 2) === 'TE') {
                                    $data['NOPHONEBC'][] = substr($line, 53, 20);
                                } else if (substr($line, 50, 2) === 'FX') {
                                    $data['NOFAXBC'][] = substr($line, 53, 20);
                                }
                                break;
                        }
                        break;  
                    case'RES00901':
                        switch (substr($line, 8, 3)) {
                            case 'ABT':
                                $data['NODAFTAR'] = substr($line, 11, 6);
                                $data['TGDAFTAR'] = substr($line, 55, 2) . '-' . substr($line, 53, 2) . '-' . substr($line, 49, 4);
                                break;
                            case 'AKQ':
                                $data['NOBCF'] = substr($line, 11, 30);
                                $data['TGBCF'] = substr($line, 55, 2) . '-' . substr($line, 53, 2) . '-' . substr($line, 49, 4);
                                $data['SERIBCF'] = substr($line, 41, 5);
                                break;
                            case'AHZ':
                                $data['NOPM'] = substr($line, 11, 30);
                                $data['TGPM'] = substr($line, 55, 2) . '-' . substr($line, 53, 2) . '-' . substr($line, 49, 4);
                                break;
                            case'AEI':
                                $data['NOPPB'] = substr($line, 11, 30);
                                $data['TGPPB'] = substr($line, 55, 2) . '-' . substr($line, 53, 2) . '-' . substr($line, 49, 4);
                                break;
                            case'ACK':
                                $data['KDRAHASIA'] = substr($line, 11, 30);
                                break;
                            case'ACY':
                                $data['NOSPPBK'] = substr($line, 11, 35);
                                $data['TGSPPBK'] = substr($line, 55, 2) . '-' . substr($line, 53, 2) . '-' . substr($line, 49, 4);
                                break;
                        }
                        break;
                    case'RES01001':
                        switch (substr($line, 8, 3)) {
                            case'1  ':
                                switch (substr($line, 11, 3)) {
                                    case'56 ':
                                        $data['TOTBKTETAPBC'] = substr($line, 14, 16) . '.' . substr($line, 30, 2);
                                        break;
                                    case'201':
                                        $data['TOTDENDATETAPBC'] = substr($line, 17, 15) . '.' . substr($line, 30, 2);
                                        break;
                                }
                                break;
                            case'9  ':
                                switch (substr($line, 11, 3)) {
                                    case'56 ':
                                        $data['TOTBKSELISIH'] = substr($line, 17, 15);
                                        break;
                                    case'201':
                                        $data['TOTDENDASELISIH'] = substr($line, 17, 15);
                                        break;
                                }
                                break;
                        }
                        break;
                    case'RES01101':
                        switch (substr($line, 8, 3)) {
                            case'946':
                                if (substr($line, 11, 3) === 'CN ') {
                                    $data['JMCONT'] = substr($line, 14, 8);
                                }
                                break;
                            case'15 ':
                                switch (substr($line, 53, 3)) {
                                    case'AAD':
                                        $data['BRUTO'] = substr($line, 56, 14) . '.' . substr($line, 70, 4);
                                        break;
                                    case'AAC':
                                        $data['NETTO'] = substr($line, 56, 14) . '.' . substr($line, 70, 4);
                                        break;
                                }
                                break;
                            case'271':
                                $data['SERIKMS'][] = substr($line, 45, 5);#BENERIN FIELDNYA
                                $data['JMKEMAS'][] = substr($line, 14, 8);
                                $data['JNKEMAS'][] = substr($line, 22, 2);
                                $data['NOSEGEL'][] = substr($line, 27, 15);
                                $data['KEMASKE']++;  
                                break;
                            case'235':
                                $data['SERICONT'][] = substr($line, 45, 5);
                                $data['NOCONT'][]   = substr($line, 80, 15);
                                $data['STUFF'][]    = substr($line, 95, 1);
                                $data['JNPARTOF'][] = substr($line, 96, 1);
                                $data['SIZECONT'][] = substr($line, 97, 2);
                                $data['TYPECONT'][] = substr($line, 99, 1);
                                break;
                        }
                        break;
                    case'RES01201':
                        switch (substr($line, 25, 3)) {
                            case'AAA':
                                $data['SERIBRG'] = substr($line, 8, 5);
                                $data['HSTETAPBC'] = substr($line, 14, 12);
                                $data['URBRGTETAPBC'] = substr($line, 28, 160);
                                break;
                            case'ACD':
                                $data['DASARTETAPBC'] = substr($line, 28, 250);
                                break;
                        }
                        break;
                    case'RES01301':
                        switch(substr($line, 8, 2)){
                            case'1 ':
                                switch(substr($line, 22, 3)){
                                    case'56 ':
                                        $data['TARIFBKTETAPBC'] = substr($line, 14, 6) . '.' . substr($line, 20, 2);
                                        $data['BKTETAPBC']      = substr($line, 25, 16) . '.' . substr($line, 41, 2);
                                        break;
                                    case'172':
                                        $data['HETETAPBC'] = substr($line, 25, 14) . '.' . substr($line, 39, 4);
                                        break;
                                    case'10 ':
                                        $data['KURSTETAPBC']  = substr($line, 34, 5) . '.' . substr($line, 39, 4);
                                        $data['KDVALTETAPBC'] = substr($line, 43, 3);
                                        break;
                                    case'201':
                                        break;
                                }                                
                                break;
                            case'9 ':
                                switch (substr($line, 22, 3)) {
                                    case'56 ':
                                        $data['BKSELISIH'] = substr($line, 25, 16) . '.' . substr($line, 41, 2);
                                        break;
                                    case'172':
                                        $data['HESELISIH'] = substr($line, 25, 14) . '.' . substr($line, 39, 4);
                                        break;
                                    case'201':
                                        $data['DENDATETAP'] = substr($line, 25, 16) . '.' . substr($line, 39, 2);
                                        break;
                                    default:
                                        if(substr($line, 46, 3) == 'AAF'){
                                            $data['JMSATTETAPBC'] = substr($line, 49, 10) . '.' . substr($line, 59, 4);
                                            $data['JNSATTETAPBC'] = substr($line, 63, 3);
                                        }
                                        if (substr($line, 46, 3) == 'AAE') {
                                            $data['JMSATSELISIH'] = substr($line, 49, 10) . '.' . substr($line, 59, 4);
                                        }
                                        break;
                                }
                                break;
                        }
                        break;
                    case'UNZ00101':
                        $jmlRes++;
                        
                        #insert data
                        $tbl_hdr= (strpos('|45|55|65|75|',$data['RESKD']))?'T_BC30PKBEHDR':'T_BC30HDR';
                        $data['STATUS'] = $this->kdRes2Stat($data['RESKD']);
                        
                        #update Headr BC30
                        unset($BC30);
                        $BC30['KODE_TRADER']= $this->kd_trader;
                        $BC30['CAR']        = $data['CAR'];
                        $BC30['STATUS']     = $data['STATUS'];
                        $actMain->insertRefernce($tbl_hdr,$BC30);
                        unset($BC30);
                        
                        #Respon
                        unset($BC30RESPEB);
                        $BC30RESPEB['KODE_TRADER'] = $this->kd_trader;
                        $BC30RESPEB['CAR']         = $data['CAR'];
                        $BC30RESPEB['RESKD']       = $data['RESKD'];
                        $BC30RESPEB['RESTG']       = $data['RESTG'];
                        $BC30RESPEB['RESWK']       = $data['RESWK'];
                        $BC30RESPEB['NOLPSE']      = trim($data['NOLPSE']);
                        $BC30RESPEB['KDKTR']       = $data['KDKTR'];
                        $BC30RESPEB['NODAFT']      = trim($data['NODAFT']);
                        $BC30RESPEB['TGDAFT']      = $data['TGDAFT'];
                        $BC30RESPEB['NOBCF305']    = trim($data['NOBCF']);
                        $BC30RESPEB['TGBCF305']    = $data['TGBCF'];
                        $BC30RESPEB['SERIBCF305']  = $data['SERIBCF'];
                        $BC30RESPEB['IDEKS']       = trim($data['IDEKS']);
                        $BC30RESPEB['NPWPEKS']     = trim($data['NPWPEKS']);
                        $BC30RESPEB['NAMAEKS']     = trim($data['NAMAEKS']);
                        $BC30RESPEB['ALMTEKS']     = trim($data['ALMTEKS']);
                        $BC30RESPEB['NIPPTG']      = $this->ConvertNIP($data['NIPTTDRES']);
                        $BC30RESPEB['NMPTG']       = $data['NAMATTDRES'];
                        $BC30RESPEB['NMKOMP']      = $data['NMKOMP'];
                        $BC30RESPEB['KOMTG']       = date('d-m-Y');
                        $BC30RESPEB['KOMWK']       = date('His');
                        $BC30RESPEB['EDISENDER']   = implode("\n", $data['DESKRIPSI']) ;
                        #$BC30RESPEB['SNRFRESPON']  = $data['JATUHTEMPO']; di vb di remark
                        #$BC30RESPEB['DIBACA']    = $data['KDGUDANG'];
                        #$BC30RESPEB['KDRAHASIA']       = date('d-m-Y');
                        $actMain->insertRefernce('T_BC30RESPEB',$BC30RESPEB);
                        unset($BC30RESPEB);
                        
                        #CONTAINER
                        #$data['SERICONT'][] = substr($line, 45, 5); $data['CONTNO'][$k];
                        if(count($data['SERICONT'])>0){
                            foreach ($data['SERICONT'] as $con=>$a){
                                unset($BC30PMC);
                                $BC30PMC['KODE_TRADER'] = $this->kd_trader;
                                $BC30PMC['CAR']         = $data['CAR'];
                                $BC30PMC['RESKD']       = $data['RESKD'];
                                $BC30PMC['RESTG']    	= $data['RESTG'];
                                $BC30PMC['RESWK']       = $data['RESWK'];
                                $BC30PMC['SERI']   	= $data['SERICONT'][$con];
                                $BC30PMC['NOCONT']   	= $data['NOCONT'][$con];
                                $BC30PMC['SIZE']        = $data['SIZECONT'][$con];
                                $BC30PMC['TYPE']   	= $data['TYPECONT'][$con];
                                $BC30PMC['NOSEGEL']   	= $data['NOSEGEL'][$con];
                                $BC30PMC['JNPARTOF']  	= $data['JNPARTOF'][$con];
                                $BC30PMC['STUFF']  	= $data['STUFF'][$con];
                                $actMain->insertRefernce('t_bc30respmc',$BC30PMC);
                                unset($BC30PMC);
                            }
                        }
                        
                        #KEMASAN    
                        #$data['SERIKMS'][] = substr($line, 56, 14); $data['kemas'][$k];
                        if(count($data['SERIKMS'])>0){
                            foreach ($data['SERIKMS'] as $kms=>$a){
                                unset($BC30PEBKMS);
                                $BC30PEBKMS['KODE_TRADER']	= $this->kd_trader;
                                $BC30PEBKMS['CAR']        	= $data['CAR'];
                                $BC30PEBKMS['RESKD']      	= $data['RESKD'];
                                $BC30PEBKMS['RESTG']     	= $data['RESTG'];
                                $BC30PEBKMS['RESWK']   		= $data['RESWK'];
                                $BC30PEBKMS['JNKEMAS']   	= $data['JNKEMAS'][$kms];
                                $BC30PEBKMS['JMKEMAS']   	= $data['JMKEMAS'][$kms];
                                $BC30PEBKMS['NOSEGEL']   	= $data['NOSEGEL'][$kms]; #nosegel statusnya gimana ? keasan atau container
                                $actMain->insertRefernce('t_bc30respebkms',$BC30PEBKMS);
                                unset($BC30PEBKMS);
                            }
                        }
                        
                        #kode respon [20]
                        if($data['RESKD']==='20'){
                            unset($BC30PPB);
                            $BC30PPB['KODE_TRADER'] = $this->kd_trader;
                            $BC30PPB['CAR']         = $data['CAR'];
                            $BC30PPB['RESTG']       = $data['RESTG'];
                            $BC30PPB['RESWK']       = $data['RESWK'];
                            $BC30PPB['NOPPB']       = $data['NOPPB'];
                            $BC30PPB['TGPPB']       = $data['TGPPB'];
                            $BC30PPB['ALMTSIAP']    = $data['ALMTSIAP'];
                            $BC30PPB['TGSIAP']      = $data['TGSIAP'];
                            #$BC30PPB['KDKTRPRIKS'] = $data['as'];
                            $BC30PPB['JMKEMAS']     = $data['JMKEMAS'][$kms];
                            $BC30PPB['JNKEMAS']     = $data['JNKEMAS'][$kms];
                            $BC30PPB['JMCONT']      = $data['JMCONT'];
                            $BC30PPB['GUDANG']      = $data['GUDANG'];
                            $BC30PPB['NOPHONE']     = $data['NOPHONEEX'];
                            $BC30PPB['NOFAX']  	    = trim($data['NOFAX']);
                            $BC30PPB['PETUGAS']     = $data['PETUGASEX'];
                            $BC30PPB['TGSTUFF']     = $data['TGSTUFF'];
                            $BC30PPB['ALMTSTUFF']   = $data['ALMTSTUFF'];
                            $BC30PPB['NIPPPB']      = $this->ConvertNIP($data['NIPTTDRES']);
                            $BC30PPB['NMPPB']  	    = $data['NAMATTDRES'];
                            $BC30PPB['KOTA']  	    = trim($data['LOKKTR']);
                            #$BC30PPB['JABPPB']      = $data['Jabppb'];
                            $BC30PPB['LOKBRG']      = $data['LOKBRG'];
                            $actMain->insertRefernce('T_BC30RESPPB',$BC30PPB);
                            unset($BC30PPB);
                        }
                        
                        #kode respon [30]
                        if($data['RESKD']==='30'){
                            unset($BC30PMH);
                            $BC30PMH['KODE_TRADER']  = $this->kd_trader;
                            $BC30PMH['CAR']          = $data['CAR'];
                            $BC30PMH['RESTG']        = $data['RESTG'];
                            $BC30PMH['RESWK']        = $data['RESWK'];
                            $BC30PMH['LOKKTR']       = $data['LOKKTR'];
                            $BC30PMH['NOPM']   	     = $data['NOPM'];
                            $BC30PMH['TGPM']         = $data['TGPM'];
                            $BC30PMH['JNEKS']        = $data['JNEKS'];
                            $BC30PMH['MODA']         = $data['MODA'];
                            $BC30PMH['CARRIER']      = $data['CARRIER'];
                            $BC30PMH['VOY']          = $data['VOY'];
                            $BC30PMH['KDKTRPRIKS']   = $data['KDKTRPRIKS'];
                            $BC30PMH['MERK']         = $data['MERK'];
                            $BC30PMH['JMCONT']       = $data['JMCONT'];
                            $BC30PMH['NIPTTDPM']     = $this->ConvertNIP($data['NIPTTDRES']);
                            $BC30PMH['NMTTDPM']      = $data['NAMATTDRES'];
                            $BC30PMH['GUDANG']       = $data['GUDANG'];
                            $BC30PMH['KTRGATE']      = $data['KTRGATE'];
                            $BC30PMH['TGEKS']  	     = $data['TGEKS'];
                            $BC30PMH['PELTRANSIT']   = $data['PELTRANSIT'];
                            $BC30PMH['PELMUAT']      = $data['PELMUAT'];
                            $BC30PMH['PELBONGKAR']   = $data['PELBONGKAR'];
                            $BC30PMH['BRUTO']        = $data['BRUTO'];
                            $BC30PMH['LOKBRG']       = $data['LOKBRG'];
                            $actMain->insertRefernce('T_BC30RESPMH',$BC30PMH);
                            unset($BC30PMH);
                        }
                                    
                        #kode respon [40]
                        if($data['RESKD']==='40'){
                            unset($BC30LPBCH);
                            $BC30LPBCH['KODE_TRADER'] = $this->kd_trader;
                            $BC30LPBCH['CAR']         = $data['CAR'];
                            $BC30LPBCH['RESTG']       = $data['RESKD'];
                            $BC30LPBCH['RESWK']       = $data['RESWK'];
                            $BC30LPBCH['NOLPSE']      = $data['NOLPSE'];
                            $BC30LPBCH['TGLPSE']      = $data['TGLPSE'];
                            $BC30LPBCH['IDQQ']        = $data['IDQQ'];
                            $BC30LPBCH['NPWPQQ']      = $data['NPWPQQ'];
                            $BC30LPBCH['NAMAQQ']      = trim($data['NAMAQQ']);
                            $BC30LPBCH['ALMTQQ']      = $data['ALMTQQ'];
                            $BC30LPBCH['NAMABELI']    = $data['NAMABELI'];
                            $BC30LPBCH['ALMTBELI']    = $data['ALMTBELI'];
                            $BC30LPBCH['NEGBELI']     = $data['NEGBELI'];
                            $BC30LPBCH['PELMUAT']     = $data['PELMUAT'];
                            $BC30LPBCH['PELBONGKAR']  = $data['PELBONGKAR'];
                            $BC30LPBCH['NOINV']       = $data['NOINV'];
                            $BC30LPBCH['TGINV']       = $data['TGINV'];
                            $BC30LPBCH['KDVAL']       = $data['KDVAL'];
                            $BC30LPBCH['MERK']        = $data['MERK'];
                            $BC30LPBCH['JMBRG']       = $data['JMBRG'];
                            $BC30LPBCH['JABLPSE']     = $data['JABLPSE'];
                            $BC30LPBCH['TTDLPSE']     = $data['TTDLPSE'];
                            $BC30LPBCH['NIPTTDLPSE']  = $this->ConvertNIP($data['NIPTTDLPSE']);
                            #$BC30LPBCH['NOPROEKS']   = $data['as'];
                            #$BC30LPBCH['TGPROEKS']   = $data['as'];
                            $BC30LPBCH['FASILITAS']   = $data['FASILITAS'];
                            $BC30LPBCH['ALMTSIAP']    = $data['ALMTSIAP'];
                            $BC30LPBCH['TGSIAP']      = $data['TGSIAP'];
                            $BC30LPBCH['NOPL']        = $data['NOPL'];
                            $BC30LPBCH['TGPL']        = $data['TGPL'];
                            $BC30LPBCH['KDKTRPRIKS']  = $data['KDKTRPRIKS'];
                            $BC30LPBCH['FOBR']        = $data['FOBR'];
                            $BC30LPBCH['JMKEMASR']    = $data['JMKEMASR'];
                            $BC30LPBCH['JNKEMASR']    = $data['JNKEMASR'];
                            $BC30LPBCH['JMCONTR']     = $data['JMCONTR'];
                            #$BC30LPBCH['JMCONT20R']  = $data['JMCONT20R'];
                            #$BC30LPBCH['JMCONT40R']  = $data['JMCONT40R'];
                            #$BC30LPBCH['BRUTOR']     = $data['BRUTOR'];
                            #$BC30LPBCH['NETTOR']     = $data['NETTOR'];
                            #$BC30LPBCH['WKREKPM']    = $data['WKREKPM'];
                            $BC30LPBCH['INSTFAS']     = $data['INSTFAS'];
                            $BC30LPBCH['FASBC']       = $data['FASBC'];
                            $BC30LPBCH['KDKTRFAS']    = $data['KDKTRFAS'];
                            #$BC30LPBCH['DIAMMEND']   = $data['DIAMMEND'];
                            $actMain->insertRefernce('T_BC30RESLPBCH',$BC30LPBCH);
                            unset($BC30LPBCH);
                        }
                        
                        #kode respon [65]||[75]
                        if($data['RESKD']==='65'||$data['RESKD']==='75'){
                            unset($BC30BCF305);
                            $BC30BCF305['KODE_TRADER']	= $this->kd_trader;
                            $BC30BCF305['CAR']        	= $data['CAR'];
                            $BC30BCF305['RESKD']      	= $data['RESKD'];
                            $BC30BCF305['RESTG']     	= $data['RESTG'];
                            $BC30BCF305['RESWK']        = $data['RESWK'];
                            #$BC30BCF305['IDEKS']   	= $data['as']; gak ada di vb
                            #$BC30BCF305['NPWPEKS']   	= $data['as'];
                            #$BC30BCF305['NAMAEKS']   	= $data['as'];
                            #$BC30BCF305['ALMTEKS']   	= $data['as'];
                            $BC30BCF305['NOBCF305']   	= $data['NOBCF'];
                            #$BC30BCF305['SERIBCF305']   = $data['as'];
                            $BC30BCF305['TGBCF305']   	= $data['TGBCF'];
                            $BC30BCF305['NIPPTG']   	= $this->ConvertNIP($data['NIPTTDRES']);
                            $BC30BCF305['NMPTG']   	= $data['NAMATTDRES'];
                            $BC30BCF305['NMKOMP']   	= $data['NMKOMP'];
                            $actMain->insertRefernce('T_BC30RESBCF305',$BC30BCF305);
                            unset($BC30BCF305);
                        }
                        
                        #kode respon [60]||[70]
                        if($data['RESKD']==='60'||$data['RESKD']==='70'){
                            unset($BC30BCF305);
                            $BC30BCF305['KODE_TRADER']	= $this->kd_trader;
                            $BC30BCF305['CAR']        	= $data['CAR'];
                            $BC30BCF305['RESKD']      	= $data['RESKD'];
                            $BC30BCF305['RESTG']     	= $data['RESTG'];
                            $BC30BCF305['RESWK']        = $data['RESWK'];
                            #$BC30BCF305['IDEKS']   	= $data['as']; gak ada di vb
                            #$BC30BCF305['NPWPEKS']   	= $data['as'];
                            #$BC30BCF305['NAMAEKS']   	= $data['as'];
                            #$BC30BCF305['ALMTEKS']   	= $data['as'];
                            $BC30BCF305['NOBCF305']   	= $data['NOBCF'];
                            #$BC30BCF305['SERIBCF305']   = $data['as'];
                            $BC30BCF305['TGBCF305']   	= $data['TGBCF'];
                            $BC30BCF305['NIPPTG']   	= $this->ConvertNIP($data['NIPTTDRES']);
                            $BC30BCF305['NMPTG']   	= $data['NAMATTDRES'];
                            $BC30BCF305['NMKOMP']   	= $data['NMKOMP'];
                            $actMain->insertRefernce('T_BC30RESBCF305',$BC30BCF305);
                            unset($BC30BCF305);
                        }
                        
                        #kode respon [90]
                        if($data['RESKD']==='90'){
                            unset($BC30SPPBK);
                            $BC30SPPBK['KODE_TRADER'] = $this->kd_trader;
                            $BC30SPPBK['CAR']         = $data['CAR'];
                            $BC30SPPBK['RESTG']       = $data['RESTG'];
                            $BC30SPPBK['RESWK']       = $data['RESWK'];
                            $BC30SPPBK['NOSPPBK']     = $data['NOSPPBK'];
                            $BC30SPPBK['TGSPPBK']     = $data['TGSPPBK'];
                            $BC30SPPBK['BKAWAL']      = $data['TOTBKTETAPBC']-$data['TOTBKSELISIH'];
                            $BC30SPPBK['BKTETAP']     = $data['TOTBKTETAPBC'];
                            $BC30SPPBK['BKSELISIH']   = $data['TOTBKSELISIH'];
                            $BC30SPPBK['SANKSITETAP'] = $data['TOTDENDATETAPBC'];
                            $BC30SPPBK['SANKSISELISIH']  = $data['TOTDENDATETAPBC'];
                            $actMain->insertRefernce('T_BC30RESSPPBK',$BC30SPPBK);
                            unset($BC30SPPBK);
                        }
                        
                        unset($BC30PEBUR);
                        $BC30PEBUR['KODE_TRADER'] = $this->kd_trader;
                        $BC30PEBUR['CAR']         = $data['CAR'];
                        $BC30PEBUR['RESKD']       = $data['RESKD'];
                        $BC30PEBUR['RESTG']    	  = $data['RESTG'];
                        $BC30PEBUR['RESWK']   	  = $data['RESWK'];
                        $BC30PEBUR['SERIUR']   	  = $data['SERIUR'];
                        $BC30PEBUR['URAIAN']   	  = $data['URAIAN'];
                        $actMain->insertRefernce('T_BC30RESPEBUR',$BC30PEBUR);
                        unset($BC30PEBUR);
                        
                        unset($BC30LPBCD);
                        $BC30LPBCD['KODE_TRADER'] = $this->kd_trader;
                        $BC30LPBCD['CAR']         = $data['CAR'];
                        $BC30LPBCD['RESKD']       = $data['RESKD'];
                        $BC30LPBCD['RESTG']    	  = $data['RESTG'];
                        $BC30LPBCD['RESWK']   	  = $data['RESWK'];
                        $BC30LPBCD['NOLPSE']   	  = $data['NOLPSE'];
                        $BC30LPBCD['SERIBRG']     = $data['SERIBRG'];
                        $BC30LPBCD['URBRG']       = $data['URBRG'];
                        $BC30LPBCD['HSR']   	  = $data['HSR'];
                        $BC30LPBCD['JMSATUANR']   = $data['JMSATUANR'];
                        $BC30LPBCD['JNSATUANR']   = $data['JNSATUANR'];
                        $BC30LPBCD['MERK']   	  = $data['MERK'];
                        $BC30LPBCD['TYPE']  	  = $data['TYPE'];
                        $actMain->insertRefernce('T_BC30RESLPBCD',$BC30LPBCD);
                        unset($BC30LPBCD);
                        
                        unset($BC30LPBCC);
                        $BC30LPBCC['KODE_TRADER'] = $this->kd_trader;
                        $BC30LPBCC['CAR']         = $data['CAR'];
                        $BC30LPBCC['RESKD']       = $data['RESKD'];
                        $BC30LPBCC['RESTG']    	  = $data['RESTG'];
                        $BC30LPBCC['RESWK']   	  = $data['RESWK'];
                        $BC30LPBCC['NOLPSE']   	  = $data['NOLPSE'];
                        $BC30LPBCC['SERI']   	  = $data['SERICONT'][$con];
                        $BC30LPBCC['NOCONT']      = $data['NOCONT'][$con];
                        $BC30LPBCC['SIZE']   	  = $data['SIZECONT'][$con];
                        $BC30LPBCC['TYPE']   	  = $data['TYPECONT'][$con];
                        $BC30LPBCC['NOSEGEL']  	  = $data['NOSEGEL'][$con];
                        $actMain->insertRefernce('T_BC30RESLPBCC',$BC30LPBCC);
                        unset($BC30LPBCC);
                        
                        unset($BC30LPBCK);
                        $BC30LPBCK['KODE_TRADER'] = $this->kd_trader;
                        $BC30LPBCK['CAR']         = $data['CAR'];
                        $BC30LPBCK['RESKD']       = $data['RESKD'];
                        $BC30LPBCK['RESTG']    	  = $data['RESTG'];
                        $BC30LPBCK['RESWK']   	  = $data['RESWK'];
                        $BC30LPBCK['NOLPSE']   	  = $data['NOLPSE'];
                        $BC30LPBCK['SERI']   	  = $data['SERIKMS'];
                        $BC30LPBCK['JNKEMASR']    = $data['JNKEMASR'];
                        $BC30LPBCK['JMKEMASR']    = $data['JMKEMASR'];
                        $actMain->insertRefernce('T_BC30RESLPBCK',$BC30LPBCK);
                        unset($BC30LPBCK);
                        
                        unset($BC30SPPBKDTL); #ada yang perlu ditanya
                        $BC30SPPBKDTL['KODE_TRADER'] = $this->kd_trader;
                        $BC30SPPBKDTL['CAR']         = $data['CAR'];
                        $BC30SPPBKDTL['RESTG']       = $data['RESTG'];
                        $BC30SPPBKDTL['RESWK']       = $data['RESWK'];
                        $BC30SPPBKDTL['SERI']        = $data['SERIBRG'];
                        $BC30SPPBKDTL['URBRGTETAP']  =  substr(trim($data['URBRGTETAPBC']) . space(100),0,100);
                        $BC30SPPBKDTL['SATBRGTETAP'] = $data['JNSATTETAPBC'];
                        $BC30SPPBKDTL['JMSATAWAL']   = $data['JMSATTETAPBC']-$data['JMSATSELISIH'];
                        $BC30SPPBKDTL['JMSATTETAP']  = $data['JMSATTETAPBC'];
                        $BC30SPPBKDTL['JMSATSELISIH'] = $data['JMSATSELISIH'];
                        $BC30SPPBKDTL['HSAWAL']      = $data['HETETAPBC']-$data['HESELISIH'];
                        $BC30SPPBKDTL['HSTETAP']     = trim($data['HSTETAPBC']);
                        $BC30SPPBKDTL['BKAWAL']      = $data['BKTETAPBC']-$data['BKSELISIH'];
                        $BC30SPPBKDTL['BKTETAP']     = $data['BKTETAPBC'];
                        $BC30SPPBKDTL['BKSELISIH']   = $data['BKSELISIH'];
                        $BC30SPPBKDTL['HETETAP']     = $data['HETETAPBC'];
                        $BC30SPPBKDTL['HESELISIH']   = $data['HESELISIH'];
                        $BC30SPPBKDTL['KURSTETAP']   = $data['KURSTETAPBC'];
                        $BC30SPPBKDTL['DENDATETAP']  = $data['DENDATETAP'];
                        $BC30SPPBKDTL['ALASAN']      = $data['DASARTETAPBC'];
                        $BC30SPPBKDTL['KDVALTETAP']  = $data['KDVALTETAPBC'];
                        $BC30SPPBKDTL['DENDASELISIH'] = $data['DENDATETAP'];
                        $BC30SPPBKDTL['TARIFBKTETAP'] = trim($data['TARIFBKTETAPBC']);
                        
                        $SQL = "SELECT * FROM T_BC30DTL WHERE WHERE CAR = '" . $data['CAR'] . "' AND seribrg = '". $data['SERIBRG'] ."' KODE_TRADER = " . $this->kd_trader;
                        $dtl = $this->actMain->get_result($SQL, true);
                        
                        if($dtl){
                            $BC30SPPBKDTL['KURSAWAL']    = $dtl['NILVALPE'];
                            $BC30SPPBKDTL['HEAWAL']      = $dtl['HPATOK'];
                            $BC30SPPBKDTL['SATBRGAWAL']  = $dtl['JNSATPE'];
                            $BC30SPPBKDTL['URBRGAWAL']   = trim($dtl['URBRG1']).' '.trim($dtl['URBRG2']).' '.trim($dtl['URBRG3']);
                            $BC30SPPBKDTL['KDVALAWAL']    = $dtl['KDVALPE'];
                            $BC30SPPBKDTL['TARIFBKAWAL']  = $dtl['TARIPPE'];
                        }
                        $actMain->insertRefernce('t_bc30ressppbkdtl',$BC30SPPBKDTL);
                        unset($BC30SPPBKDTL);
                        unset($dtl);
                                    
                        #kode respon [300|310|450]
                        if($data['RESKD']==='300' || $data['RESKD']==='310' || $data['RESKD']==='450'){
                            foreach ($data['CONTNO'] as $k=>$a){
                                unset($BC20CONR);
                                $BC20CONR['KODE_TRADER']= $this->kd_trader;
                                $BC20CONR['CAR']        = $data['CAR'];
                                $BC20CONR['RESKD']      = $data['RESKD'];
                                $BC20CONR['CONTNO']     = $data['CONTNO'][$con];
                                $BC20CONR['CONTUKUR']   = $data['CONTUKUR'];
                                $actMain->insertRefernce('t_bc20conr',$BC20CONR);
                                unset($BC20CONR);
                            }
                        }
                        
                        $arrInBound['KODE_TRADER']= $this->kd_trader;
                        $arrInBound['CAR']      = $data['CAR'];
                        $arrInBound['STATUS']   = '02';
                        $actMain->insertRefernce('M_TRADER_DOKINBOUND',$arrInBound);
                        unset($data);
                    break;
                }
            }
        }
        fclose($handle);
        return $jmlRes;
    }
    
    private function BACALPBC($filePath, $actMain, $arrInBound){
        $jmlRes = 0;
        $handle = fopen($filePath, 'r');
        if ($handle) {
            unset($data);
            while (($line = fgets($handle)) !== false) {
                $segment = substr($line, 0, 8);
                switch($segment){
                    case'ENV':
                        if($jmlRes > 1){
                            $KemasKe    = 0;
                            $ContKe     = 0;
                            $DetilKe    = 0;
                            $data['JMCONTR']    = 0;
                            $data['JMDLPBC']    = 0;
                            $data['FASILITAS']  = "";
                            $data['FASBC']      = "";
                        }
                        $data['EDINUMSENDER']   = "";
                        $data['RESKD']          = "40";
                        unset($data);
                    case'HDEC0101':
                        $data['CAR']    = substr($line, 8, 26);
                        $data['KDKTR']  = substr($line, 51, 6);
                        $data['NODAFT'] = substr($line, 57, 6); 
                        $data['NOLPSE'] = substr($line, 63, 10); 
                        break;
                    case'HDEC0201':
                       switch (substr($line, 8, 3)) {
                           case'9  ':
                               $data['PELMUAT'] = substr($line, 11, 5);
                               break;
                           case'11 ':
                               $data['PELBONGKAR'] = substr($line, 11, 5);
                               break;
                           case'22 ':
                               $data['KDKTRMUAT'] = substr($line, 11, 6);
                               $data['KDKTRPRIKS'] = substr($line, 17, 6);
                               break;
                           case'14 ':
                               $data['ALMTSIAP'] = substr($line, 31, 70);
                               break;
                       }
                       break;    
                    case'RES00301':
                        switch (substr($line, 8, 3)) {
                            case '150':
                                $data['TGDAFTAR'] = substr($line, 17, 2) . '-' . substr($line, 15, 2) . '-' . substr($line, 11, 4);
                                break;
                            case '345':
                                $data['TGDAFTAR'] = substr($line, 17, 2) . '-' . substr($line, 15, 2) . '-' . substr($line, 11, 4);
                                break;
                            case '129':
                                $data['TGDAFTAR'] = substr($line, 17, 2) . '-' . substr($line, 15, 2) . '-' . substr($line, 11, 4);
                                break;
                            case '255':
                                $data['TGDAFTAR'] = substr($line, 17, 2) . '-' . substr($line, 15, 2) . '-' . substr($line, 11, 4);
                                break;
                            case '182':
                                $data['TGDAFTAR'] = substr($line, 17, 2) . '-' . substr($line, 15, 2) . '-' . substr($line, 11, 4);
                                break;
                        }    
                    case'HDEC0401':
                        switch (substr($line, 8, 3)) {
                            case '101':
                                $data['JALUR']    = substr($line, 11, 1);
                                break;
                            case '98 ':
                                $data['INSTFAS']  = substr($line, 11, 1);
                                $data['KDKTRFAS'] = substr($line, 12, 6);
                                
                                if($data['INSTFAS'] == "1"){
                                    $data['FASBC'] = substr($line, 20, 1);
                                }else if($data['INSTFAS'] == "2"){
                                    $data['FASILITAS'] = substr($line, 20, 1);
                                }
                                break;
                        }
                        break;    
                    case'HDEC0701':
                        switch (substr($line, 8, 3)) {
                            case 'CN ':
                                $data['SERICONT']   = substr($line, 11, 5);
                                $data['NOSEGEL']    = substr($line, 16, 15); 
                                $data['NOCONT']     = substr($line, 31, 15); 
                                $data['SIZECONT']   = substr($line, 46, 2); 
                                $data['TYPECONT']   = substr($line, 48, 1); 
                                $data['STUFF']      = substr($line, 49, 1); 
                                $data['JNPARTOF']   = substr($line, 50, 1); 
                                break;
                            case 'PBP':
                                $data['NOSEGEL']    = substr($line, 16, 15);
                                break;
                        }
                        break;
                    case'HDEC0801':
                        $data['SERIUR'] = substr($line, 10, 5);
                        $data['URAIAN'] = substr($line, 16, 60);
                        break;    
                    case'HDEC0901': 
                        switch (substr($line, 8, 3)) {
                            case 'ACH':
                                $data['SERIKEMASR'] = substr($line, 11, 5);
                                $data['JMKEMASR']   = substr($line, 31, 8);
                                $data['JNKEMASR']   = substr($line, 39, 2);
                                $data['JMDLPBC']    = $KemasKe;
                                $KemasKe++;
                                break;
                            case '   ':
                                $data['MERK'] = substr($line, 22, 100);
                                break;
                        }
                        break;
                    case'HDEC1101':
                        $data['KDDOK'] = substr($line, 13, 3);
                        switch ($data['KDDOK']) {
                            case '380':
                                $data['NOINV'] = substr($line, 16, 30);
                                $data['TGINV'] = substr($line, 60, 2) . '-' . substr($line, 58, 2) . '-' . substr($line, 54, 4);
                                break;
                            case '217':
                                $data['NOPL'] = substr($line, 16, 30);
                                $data['TGINV'] = substr($line, 60, 2) . '-' . substr($line, 58, 2) . '-' . substr($line, 54, 4);
                                break;
                        }
                        break;  
                    case'HDEC1201':
                        switch (substr($line, 8, 3)) {
                            case 'EX ':
                                $data['IDEKS']   = substr($line, 11, 6);
                                $data['NPWPEKS'] = substr($line, 12, 15);
                                $data['NAMAEKS'] = substr($line, 27, 50);
                                $data['ALMTEKS'] = substr($line, 77, 70);
                                $data['NIPER']   = substr($line, 149, 5);
                                break;
                            case 'GO ':
                                $data['IDQQ']     = substr($line, 11, 6);
                                $data['NPWPQQ']   = substr($line, 12, 15);
                                $data['NAMAQQ']   = substr($line, 27, 50);
                                $data['ALMTQQ']   = substr($line, 77, 70);
                                $data['NIPERIND'] = substr($line, 149, 5);
                                break;
                            case'BY ':
                                $data['NAMABELI'] = substr($line, 27, 50);
                                $data['ALMTBELI'] = substr($line, 77, 70);
                                $data['NEGBELI']  = substr($line, 149, 2);
                                break;
                            case'AM ':
                                $data['NIPTTDLPSE'] = substr($line, 11, 14);
                                $data['TTDLPSE']    = substr($line, 27, 30);
                                $data['JABLPSE']    = substr($line, 77, 30);
                                $data['KDRAHASIA']  = substr($line, 107, 30);
                                break;
                        }
                        break;
                    case'HDEC1401':
                        $data['FOBR']   = substr($line, 11, 18)/10000;
                        $data['KDVAL']  = substr($line, 29, 3);
                        break;
                    case'DDEC0101':
                        $data['SERIBRG']   = substr($line, 8, 5);
                        $data['HSR']       = substr($line, 13, 10);
                        $data['URBRG1']    = substr($line, 27, 40);
                        $data['URBRG2']    = substr($line, 67, 40);
                        $data['URBRG3']    = substr($line, 107, 40);
                        $data['URBRG4']    = substr($line, 147, 40);
                        $data['URBRG']     = trim($data['URBRG1']).' '.trim($data['URBRG2']).' '.trim($data['URBRG3']).' '.trim($data['URBRG4']);
                        $data['EXSERIBRG'] = substr($line, 187, 20);
                        break;                  
                    case'DDEC0301':
                        if(substr($line, 187, 20) == "AAE"){
                            $data['JMSATUANR']    = substr($line, 11, 10).'.'.substr($line, 21, 4);
                            $data['JNSATUANR']    = substr($line, 25, 3);
                        }
                        break;
                    case'DDEC0601':
                        $data['DMERK']   = "";
                        $data['JMKEMAS'][] = substr($line, 8, 8);
                        $data['JNKEMAS'][] = substr($line, 16, 2);
                        $data['SIZE']    = substr($line, 18, 15);
                        $data['TYPE']    = substr($line, 33, 15);
                        $data['KDBRG']   = substr($line, 48, 15);
                        $data['DMERK']   = substr($line, 63, 14);
                        break;
                    case'DDEC0701':
                        if(substr($line, 8, 3) == "63 "){
                            $data['FOBPERSAT'] = substr($line, 11, 18)/ 10000;
                        }else if (substr($line, 8, 3) == "65 "){
                            $data['FOBPERBRG'] = substr($line, 11, 18)/ 10000;
                            $DetilKe++;
                        }
                        break;
                    case'SDEC0101':
                        if(substr($line, 8, 3) == "5  "){
                            $data['JmBrg'] = substr($line, 11, 5);
                        }else if (substr($line, 8, 3) == "16 "){
                            $data['JmContr'] = substr($line, 11, 5);
                        }
                        break;
                    case'UNZ00101':
                        $jmlRes++;
                        
                        #insert data
                        $tbl_hdr= (strpos('|45|55|65|75|',$data['RESKD']))?'T_BC30PKBEHDR':'T_BC30HDR';
                        $data['STATUS'] = $this->kdRes2Stat($data['RESKD']);
                        
                        #update Headr BC30
                        unset($BC30);
                        $BC30['KODE_TRADER']= $this->kd_trader;
                        $BC30['CAR']        = $data['CAR'];
                        $BC30['STATUS']     = $data['STATUS'];
                        $actMain->insertRefernce($tbl_hdr,$BC30);
                        unset($BC30);
                        
                        #Respon
                        unset($BC30RESPEB);
                        $BC30RESPEB['KODE_TRADER'] = $this->kd_trader;
                        $BC30RESPEB['CAR']         = $data['CAR'];
                        $BC30RESPEB['RESKD']       = $data['RESKD'];
                        $BC30RESPEB['RESTG']       = $data['RESTG'];
                        $BC30RESPEB['RESWK']       = $data['RESWK'];
                        $BC30RESPEB['NOLPSE']      = trim($data['NOLPSE']);
                        $BC30RESPEB['KDKTR']       = $data['KDKTR'];
                        $BC30RESPEB['NODAFT']      = trim($data['NODAFT']);
                        $BC30RESPEB['TGDAFT']      = $data['TGDAFT'];
                        $BC30RESPEB['NOBCF305']    = trim($data['NOBCF']);
                        $BC30RESPEB['TGBCF305']    = $data['TGBCF'];
                        $BC30RESPEB['SERIBCF305']  = $data['SERIBCF'];
                        $BC30RESPEB['IDEKS']       = trim($data['IDEKS']);
                        $BC30RESPEB['NPWPEKS']     = trim($data['NPWPEKS']);
                        $BC30RESPEB['NAMAEKS']     = trim($data['NAMAEKS']);
                        $BC30RESPEB['ALMTEKS']     = trim($data['ALMTEKS']);
                        $BC30RESPEB['NIPPTG']      = $this->ConvertNIP($data['NIPTTDRES']);
                        $BC30RESPEB['NMPTG']       = $data['NAMATTDRES'];
                        $BC30RESPEB['NMKOMP']      = $data['NMKOMP'];
                        $BC30RESPEB['KOMTG']       = date('d-m-Y');
                        $BC30RESPEB['KOMWK']       = date('His');
                        $BC30RESPEB['EDISENDER']   = implode("\n", $data['DESKRIPSI']) ;
                        #$BC30RESPEB['SNRFRESPON']  = $data['JATUHTEMPO']; di vb di remark
                        #$BC30RESPEB['DIBACA']    = $data['KDGUDANG'];
                        #$BC30RESPEB['KDRAHASIA']       = date('d-m-Y');
                        $actMain->insertRefernce('T_BC30RESPEB',$BC30RESPEB);
                        unset($BC30RESPEB);
                        
                        #CONTAINER
                        #$data['SERICONT'][] = substr($line, 45, 5); $data['CONTNO'][$k];
                        if(count($data['SERICONT'])>0){
                            foreach ($data['SERICONT'] as $con=>$a){
                                unset($BC30PMC);
                                $BC30PMC['KODE_TRADER'] = $this->kd_trader;
                                $BC30PMC['CAR']         = $data['CAR'];
                                $BC30PMC['RESKD']       = $data['RESKD'];
                                $BC30PMC['RESTG']    	= $data['RESTG'];
                                $BC30PMC['RESWK']       = $data['RESWK'];
                                $BC30PMC['SERI']   	= $data['SERICONT'][$con];
                                $BC30PMC['NOCONT']   	= $data['NOCONT'][$con];
                                $BC30PMC['SIZE']        = $data['SIZECONT'][$con];
                                $BC30PMC['TYPE']   	= $data['TYPECONT'][$con];
                                $BC30PMC['NOSEGEL']   	= $data['NOSEGEL'][$con];
                                $BC30PMC['JNPARTOF']  	= $data['JNPARTOF'][$con];
                                $BC30PMC['STUFF']  	= $data['STUFF'][$con];
                                $actMain->insertRefernce('t_bc30respmc',$BC30PMC);
                                unset($BC30PMC);
                            }
                        }
                        
                        #KEMASAN    
                        #$data['SERIKMS'][] = substr($line, 56, 14); $data['kemas'][$k];
                        if(count($data['SERIKMS'])>0){
                            foreach ($data['SERIKMS'] as $kms=>$a){
                                unset($BC30PEBKMS);
                                $BC30PEBKMS['KODE_TRADER']	= $this->kd_trader;
                                $BC30PEBKMS['CAR']        	= $data['CAR'];
                                $BC30PEBKMS['RESKD']      	= $data['RESKD'];
                                $BC30PEBKMS['RESTG']     	= $data['RESTG'];
                                $BC30PEBKMS['RESWK']   		= $data['RESWK'];
                                $BC30PEBKMS['JNKEMAS']   	= $data['JNKEMAS'][$kms];
                                $BC30PEBKMS['JMKEMAS']   	= $data['JMKEMAS'][$kms];
                                $BC30PEBKMS['NOSEGEL']   	= $data['NOSEGEL'][$kms]; #nosegel statusnya gimana ? keasan atau container
                                $actMain->insertRefernce('t_bc30respebkms',$BC30PEBKMS);
                                unset($BC30PEBKMS);
                            }
                        }
                        
                        #kode respon [20]
                        if($data['RESKD']==='20'){
                            unset($BC30PPB);
                            $BC30PPB['KODE_TRADER'] = $this->kd_trader;
                            $BC30PPB['CAR']         = $data['CAR'];
                            $BC30PPB['RESTG']       = $data['RESTG'];
                            $BC30PPB['RESWK']       = $data['RESWK'];
                            $BC30PPB['NOPPB']       = $data['NOPPB'];
                            $BC30PPB['TGPPB']       = $data['TGPPB'];
                            $BC30PPB['ALMTSIAP']    = $data['ALMTSIAP'];
                            $BC30PPB['TGSIAP']      = $data['TGSIAP'];
                            #$BC30PPB['KDKTRPRIKS'] = $data['as'];
                            $BC30PPB['JMKEMAS']     = $data['JMKEMAS'][$kms];
                            $BC30PPB['JNKEMAS']     = $data['JNKEMAS'][$kms];
                            $BC30PPB['JMCONT']      = $data['JMCONT'];
                            $BC30PPB['GUDANG']      = $data['GUDANG'];
                            $BC30PPB['NOPHONE']     = $data['NOPHONEEX'];
                            $BC30PPB['NOFAX']  	    = trim($data['NOFAX']);
                            $BC30PPB['PETUGAS']     = $data['PETUGASEX'];
                            $BC30PPB['TGSTUFF']     = $data['TGSTUFF'];
                            $BC30PPB['ALMTSTUFF']   = $data['ALMTSTUFF'];
                            $BC30PPB['NIPPPB']      = $this->ConvertNIP($data['NIPTTDRES']);
                            $BC30PPB['NMPPB']  	    = $data['NAMATTDRES'];
                            $BC30PPB['KOTA']  	    = trim($data['LOKKTR']);
                            #$BC30PPB['JABPPB']      = $data['Jabppb'];
                            $BC30PPB['LOKBRG']      = $data['LOKBRG'];
                            $actMain->insertRefernce('T_BC30RESPPB',$BC30PPB);
                            unset($BC30PPB);
                        }
                        
                        #kode respon [30]
                        if($data['RESKD']==='30'){
                            unset($BC30PMH);
                            $BC30PMH['KODE_TRADER']  = $this->kd_trader;
                            $BC30PMH['CAR']          = $data['CAR'];
                            $BC30PMH['RESTG']        = $data['RESTG'];
                            $BC30PMH['RESWK']        = $data['RESWK'];
                            $BC30PMH['LOKKTR']       = $data['LOKKTR'];
                            $BC30PMH['NOPM']   	     = $data['NOPM'];
                            $BC30PMH['TGPM']         = $data['TGPM'];
                            $BC30PMH['JNEKS']        = $data['JNEKS'];
                            $BC30PMH['MODA']         = $data['MODA'];
                            $BC30PMH['CARRIER']      = $data['CARRIER'];
                            $BC30PMH['VOY']          = $data['VOY'];
                            $BC30PMH['KDKTRPRIKS']   = $data['KDKTRPRIKS'];
                            $BC30PMH['MERK']         = $data['MERK'];
                            $BC30PMH['JMCONT']       = $data['JMCONT'];
                            $BC30PMH['NIPTTDPM']     = $this->ConvertNIP($data['NIPTTDRES']);
                            $BC30PMH['NMTTDPM']      = $data['NAMATTDRES'];
                            $BC30PMH['GUDANG']       = $data['GUDANG'];
                            $BC30PMH['KTRGATE']      = $data['KTRGATE'];
                            $BC30PMH['TGEKS']  	     = $data['TGEKS'];
                            $BC30PMH['PELTRANSIT']   = $data['PELTRANSIT'];
                            $BC30PMH['PELMUAT']      = $data['PELMUAT'];
                            $BC30PMH['PELBONGKAR']   = $data['PELBONGKAR'];
                            $BC30PMH['BRUTO']        = $data['BRUTO'];
                            $BC30PMH['LOKBRG']       = $data['LOKBRG'];
                            $actMain->insertRefernce('T_BC30RESPMH',$BC30PMH);
                            unset($BC30PMH);
                        }
                                    
                        #kode respon [40]
                        if($data['RESKD']==='40'){
                            unset($BC30LPBCH);
                            $BC30LPBCH['KODE_TRADER'] = $this->kd_trader;
                            $BC30LPBCH['CAR']         = $data['CAR'];
                            $BC30LPBCH['RESTG']       = $data['RESKD'];
                            $BC30LPBCH['RESWK']       = $data['RESWK'];
                            $BC30LPBCH['NOLPSE']      = $data['NOLPSE'];
                            $BC30LPBCH['TGLPSE']      = $data['TGLPSE'];
                            $BC30LPBCH['IDQQ']        = $data['IDQQ'];
                            $BC30LPBCH['NPWPQQ']      = $data['NPWPQQ'];
                            $BC30LPBCH['NAMAQQ']      = trim($data['NAMAQQ']);
                            $BC30LPBCH['ALMTQQ']      = $data['ALMTQQ'];
                            $BC30LPBCH['NAMABELI']    = $data['NAMABELI'];
                            $BC30LPBCH['ALMTBELI']    = $data['ALMTBELI'];
                            $BC30LPBCH['NEGBELI']     = $data['NEGBELI'];
                            $BC30LPBCH['PELMUAT']     = $data['PELMUAT'];
                            $BC30LPBCH['PELBONGKAR']  = $data['PELBONGKAR'];
                            $BC30LPBCH['NOINV']       = $data['NOINV'];
                            $BC30LPBCH['TGINV']       = $data['TGINV'];
                            $BC30LPBCH['KDVAL']       = $data['KDVAL'];
                            $BC30LPBCH['MERK']        = $data['MERK'];
                            $BC30LPBCH['JMBRG']       = $data['JMBRG'];
                            $BC30LPBCH['JABLPSE']     = $data['JABLPSE'];
                            $BC30LPBCH['TTDLPSE']     = $data['TTDLPSE'];
                            $BC30LPBCH['NIPTTDLPSE']  = $this->ConvertNIP($data['NIPTTDLPSE']);
                            #$BC30LPBCH['NOPROEKS']   = $data['as'];
                            #$BC30LPBCH['TGPROEKS']   = $data['as'];
                            $BC30LPBCH['FASILITAS']   = $data['FASILITAS'];
                            $BC30LPBCH['ALMTSIAP']    = $data['ALMTSIAP'];
                            $BC30LPBCH['TGSIAP']      = $data['TGSIAP'];
                            $BC30LPBCH['NOPL']        = $data['NOPL'];
                            $BC30LPBCH['TGPL']        = $data['TGPL'];
                            $BC30LPBCH['KDKTRPRIKS']  = $data['KDKTRPRIKS'];
                            $BC30LPBCH['FOBR']        = $data['FOBR'];
                            $BC30LPBCH['JMKEMASR']    = $data['JMKEMASR'];
                            $BC30LPBCH['JNKEMASR']    = $data['JNKEMASR'];
                            $BC30LPBCH['JMCONTR']     = $data['JMCONTR'];
                            #$BC30LPBCH['JMCONT20R']  = $data['JMCONT20R'];
                            #$BC30LPBCH['JMCONT40R']  = $data['JMCONT40R'];
                            #$BC30LPBCH['BRUTOR']     = $data['BRUTOR'];
                            #$BC30LPBCH['NETTOR']     = $data['NETTOR'];
                            #$BC30LPBCH['WKREKPM']    = $data['WKREKPM'];
                            $BC30LPBCH['INSTFAS']     = $data['INSTFAS'];
                            $BC30LPBCH['FASBC']       = $data['FASBC'];
                            $BC30LPBCH['KDKTRFAS']    = $data['KDKTRFAS'];
                            #$BC30LPBCH['DIAMMEND']   = $data['DIAMMEND'];
                            $actMain->insertRefernce('T_BC30RESLPBCH',$BC30LPBCH);
                            unset($BC30LPBCH);
                        }
                        
                        #kode respon [65]||[75]
                        if($data['RESKD']==='65'||$data['RESKD']==='75'){
                            unset($BC30BCF305);
                            $BC30BCF305['KODE_TRADER']	= $this->kd_trader;
                            $BC30BCF305['CAR']        	= $data['CAR'];
                            $BC30BCF305['RESKD']      	= $data['RESKD'];
                            $BC30BCF305['RESTG']     	= $data['RESTG'];
                            $BC30BCF305['RESWK']        = $data['RESWK'];
                            #$BC30BCF305['IDEKS']   	= $data['as']; gak ada di vb
                            #$BC30BCF305['NPWPEKS']   	= $data['as'];
                            #$BC30BCF305['NAMAEKS']   	= $data['as'];
                            #$BC30BCF305['ALMTEKS']   	= $data['as'];
                            $BC30BCF305['NOBCF305']   	= $data['NOBCF'];
                            #$BC30BCF305['SERIBCF305']   = $data['as'];
                            $BC30BCF305['TGBCF305']   	= $data['TGBCF'];
                            $BC30BCF305['NIPPTG']   	= $this->ConvertNIP($data['NIPTTDRES']);
                            $BC30BCF305['NMPTG']   	= $data['NAMATTDRES'];
                            $BC30BCF305['NMKOMP']   	= $data['NMKOMP'];
                            $actMain->insertRefernce('T_BC30RESBCF305',$BC30BCF305);
                            unset($BC30BCF305);
                        }
                        
                        #kode respon [60]||[70]
                        if($data['RESKD']==='60'||$data['RESKD']==='70'){
                            unset($BC30BCF305);
                            $BC30BCF305['KODE_TRADER']	= $this->kd_trader;
                            $BC30BCF305['CAR']        	= $data['CAR'];
                            $BC30BCF305['RESKD']      	= $data['RESKD'];
                            $BC30BCF305['RESTG']     	= $data['RESTG'];
                            $BC30BCF305['RESWK']        = $data['RESWK'];
                            #$BC30BCF305['IDEKS']   	= $data['as']; gak ada di vb
                            #$BC30BCF305['NPWPEKS']   	= $data['as'];
                            #$BC30BCF305['NAMAEKS']   	= $data['as'];
                            #$BC30BCF305['ALMTEKS']   	= $data['as'];
                            $BC30BCF305['NOBCF305']   	= $data['NOBCF'];
                            #$BC30BCF305['SERIBCF305']   = $data['as'];
                            $BC30BCF305['TGBCF305']   	= $data['TGBCF'];
                            $BC30BCF305['NIPPTG']   	= $this->ConvertNIP($data['NIPTTDRES']);
                            $BC30BCF305['NMPTG']   	= $data['NAMATTDRES'];
                            $BC30BCF305['NMKOMP']   	= $data['NMKOMP'];
                            $actMain->insertRefernce('T_BC30RESBCF305',$BC30BCF305);
                            unset($BC30BCF305);
                        }
                        
                        #kode respon [90]
                        if($data['RESKD']==='90'){
                            unset($BC30SPPBK);
                            $BC30SPPBK['KODE_TRADER'] = $this->kd_trader;
                            $BC30SPPBK['CAR']         = $data['CAR'];
                            $BC30SPPBK['RESTG']       = $data['RESTG'];
                            $BC30SPPBK['RESWK']       = $data['RESWK'];
                            $BC30SPPBK['NOSPPBK']     = $data['NOSPPBK'];
                            $BC30SPPBK['TGSPPBK']     = $data['TGSPPBK'];
                            $BC30SPPBK['BKAWAL']      = $data['TOTBKTETAPBC']-$data['TOTBKSELISIH'];
                            $BC30SPPBK['BKTETAP']     = $data['TOTBKTETAPBC'];
                            $BC30SPPBK['BKSELISIH']   = $data['TOTBKSELISIH'];
                            $BC30SPPBK['SANKSITETAP'] = $data['TOTDENDATETAPBC'];
                            $BC30SPPBK['SANKSISELISIH']  = $data['TOTDENDATETAPBC'];
                            $actMain->insertRefernce('T_BC30RESSPPBK',$BC30SPPBK);
                            unset($BC30SPPBK);
                        }
                        
                        unset($BC30PEBUR);
                        $BC30PEBUR['KODE_TRADER'] = $this->kd_trader;
                        $BC30PEBUR['CAR']         = $data['CAR'];
                        $BC30PEBUR['RESKD']       = $data['RESKD'];
                        $BC30PEBUR['RESTG']    	  = $data['RESTG'];
                        $BC30PEBUR['RESWK']   	  = $data['RESWK'];
                        $BC30PEBUR['SERIUR']   	  = $data['SERIUR'];
                        $BC30PEBUR['URAIAN']   	  = $data['URAIAN'];
                        $actMain->insertRefernce('T_BC30RESPEBUR',$BC30PEBUR);
                        unset($BC30PEBUR);
                        
                        unset($BC30LPBCD);
                        $BC30LPBCD['KODE_TRADER'] = $this->kd_trader;
                        $BC30LPBCD['CAR']         = $data['CAR'];
                        $BC30LPBCD['RESKD']       = $data['RESKD'];
                        $BC30LPBCD['RESTG']    	  = $data['RESTG'];
                        $BC30LPBCD['RESWK']   	  = $data['RESWK'];
                        $BC30LPBCD['NOLPSE']   	  = $data['NOLPSE'];
                        $BC30LPBCD['SERIBRG']     = $data['SERIBRG'];
                        $BC30LPBCD['URBRG']       = $data['URBRG'];
                        $BC30LPBCD['HSR']   	  = $data['HSR'];
                        $BC30LPBCD['JMSATUANR']   = $data['JMSATUANR'];
                        $BC30LPBCD['JNSATUANR']   = $data['JNSATUANR'];
                        $BC30LPBCD['MERK']   	  = $data['MERK'];
                        $BC30LPBCD['TYPE']  	  = $data['TYPE'];
                        $actMain->insertRefernce('T_BC30RESLPBCD',$BC30LPBCD);
                        unset($BC30LPBCD);
                        
                        unset($BC30LPBCC);
                        $BC30LPBCC['KODE_TRADER'] = $this->kd_trader;
                        $BC30LPBCC['CAR']         = $data['CAR'];
                        $BC30LPBCC['RESKD']       = $data['RESKD'];
                        $BC30LPBCC['RESTG']    	  = $data['RESTG'];
                        $BC30LPBCC['RESWK']   	  = $data['RESWK'];
                        $BC30LPBCC['NOLPSE']   	  = $data['NOLPSE'];
                        $BC30LPBCC['SERI']   	  = $data['SERICONT'][$con];
                        $BC30LPBCC['NOCONT']      = $data['NOCONT'][$con];
                        $BC30LPBCC['SIZE']   	  = $data['SIZECONT'][$con];
                        $BC30LPBCC['TYPE']   	  = $data['TYPECONT'][$con];
                        $BC30LPBCC['NOSEGEL']  	  = $data['NOSEGEL'][$con];
                        $actMain->insertRefernce('T_BC30RESLPBCC',$BC30LPBCC);
                        unset($BC30LPBCC);
                        
                        unset($BC30LPBCK);
                        $BC30LPBCK['KODE_TRADER'] = $this->kd_trader;
                        $BC30LPBCK['CAR']         = $data['CAR'];
                        $BC30LPBCK['RESKD']       = $data['RESKD'];
                        $BC30LPBCK['RESTG']    	  = $data['RESTG'];
                        $BC30LPBCK['RESWK']   	  = $data['RESWK'];
                        $BC30LPBCK['NOLPSE']   	  = $data['NOLPSE'];
                        $BC30LPBCK['SERI']   	  = $data['SERIKMS'];
                        $BC30LPBCK['JNKEMASR']    = $data['JNKEMASR'];
                        $BC30LPBCK['JMKEMASR']    = $data['JMKEMASR'];
                        $actMain->insertRefernce('T_BC30RESLPBCK',$BC30LPBCK);
                        unset($BC30LPBCK);
                        
                        unset($BC30SPPBKDTL); #ada yang perlu ditanya
                        $BC30SPPBKDTL['KODE_TRADER'] = $this->kd_trader;
                        $BC30SPPBKDTL['CAR']         = $data['CAR'];
                        $BC30SPPBKDTL['RESTG']       = $data['RESTG'];
                        $BC30SPPBKDTL['RESWK']       = $data['RESWK'];
                        $BC30SPPBKDTL['SERI']        = $data['SERIBRG'];
                        $BC30SPPBKDTL['URBRGTETAP']  =  substr(trim($data['URBRGTETAPBC']) . space(100),0,100);
                        $BC30SPPBKDTL['SATBRGTETAP'] = $data['JNSATTETAPBC'];
                        $BC30SPPBKDTL['JMSATAWAL']   = $data['JMSATTETAPBC']-$data['JMSATSELISIH'];
                        $BC30SPPBKDTL['JMSATTETAP']  = $data['JMSATTETAPBC'];
                        $BC30SPPBKDTL['JMSATSELISIH'] = $data['JMSATSELISIH'];
                        $BC30SPPBKDTL['HSAWAL']      = $data['HETETAPBC']-$data['HESELISIH'];
                        $BC30SPPBKDTL['HSTETAP']     = trim($data['HSTETAPBC']);
                        $BC30SPPBKDTL['BKAWAL']      = $data['BKTETAPBC']-$data['BKSELISIH'];
                        $BC30SPPBKDTL['BKTETAP']     = $data['BKTETAPBC'];
                        $BC30SPPBKDTL['BKSELISIH']   = $data['BKSELISIH'];
                        $BC30SPPBKDTL['HETETAP']     = $data['HETETAPBC'];
                        $BC30SPPBKDTL['HESELISIH']   = $data['HESELISIH'];
                        $BC30SPPBKDTL['KURSTETAP']   = $data['KURSTETAPBC'];
                        $BC30SPPBKDTL['DENDATETAP']  = $data['DENDATETAP'];
                        $BC30SPPBKDTL['ALASAN']      = $data['DASARTETAPBC'];
                        $BC30SPPBKDTL['KDVALTETAP']  = $data['KDVALTETAPBC'];
                        $BC30SPPBKDTL['DENDASELISIH'] = $data['DENDATETAP'];
                        $BC30SPPBKDTL['TARIFBKTETAP'] = trim($data['TARIFBKTETAPBC']);
                        
                        $SQL = "SELECT * FROM T_BC30DTL WHERE WHERE CAR = '" . $data['CAR'] . "' AND seribrg = '". $data['SERIBRG'] ."' KODE_TRADER = " . $this->kd_trader;
                        $dtl = $this->actMain->get_result($SQL, true);
                        
                        if($dtl){
                            $BC30SPPBKDTL['KURSAWAL']    = $dtl['NILVALPE'];
                            $BC30SPPBKDTL['HEAWAL']      = $dtl['HPATOK'];
                            $BC30SPPBKDTL['SATBRGAWAL']  = $dtl['JNSATPE'];
                            $BC30SPPBKDTL['URBRGAWAL']   = trim($dtl['URBRG1']).' '.trim($dtl['URBRG2']).' '.trim($dtl['URBRG3']);
                            $BC30SPPBKDTL['KDVALAWAL']    = $dtl['KDVALPE'];
                            $BC30SPPBKDTL['TARIFBKAWAL']  = $dtl['TARIPPE'];
                        }
                        $actMain->insertRefernce('t_bc30ressppbkdtl',$BC30SPPBKDTL);
                        unset($BC30SPPBKDTL);
                        unset($dtl);
                                    
                        #kode respon [300|310|450]
                        if($data['RESKD']==='300' || $data['RESKD']==='310' || $data['RESKD']==='450'){
                            foreach ($data['CONTNO'] as $k=>$a){
                                unset($BC20CONR);
                                $BC20CONR['KODE_TRADER']= $this->kd_trader;
                                $BC20CONR['CAR']        = $data['CAR'];
                                $BC20CONR['RESKD']      = $data['RESKD'];
                                $BC20CONR['CONTNO']     = $data['CONTNO'][$con];
                                $BC20CONR['CONTUKUR']   = $data['CONTUKUR'];
                                $actMain->insertRefernce('t_bc20conr',$BC20CONR);
                                unset($BC20CONR);
                            }
                        }
                        
                        $arrInBound['KODE_TRADER']= $this->kd_trader;
                        $arrInBound['CAR']      = $data['CAR'];
                        $arrInBound['STATUS']   = '02';
                        $actMain->insertRefernce('M_TRADER_DOKINBOUND',$arrInBound);
                        unset($data);
                    break;
                }
            }
        }
        fclose($handle);
        return $jmlRes;
    }
    
    private function kdRes2Stat($kdres){
        switch ($kdres){
            case'100':$rtn='065';break;
            case'110':$rtn='060';break;
            case'120':$rtn='060';break;
            case'200':$rtn='082';break;
            case'205':$rtn='084';break;
            case'210':$rtn='080';break;
            case'220':$rtn='090';break;
            case'225':$rtn='095';break;
            case'230':$rtn='086';break;
            case'240':$rtn='088';break;
            case'250':$rtn='116';break;
            case'300':$rtn='110';break;
            case'310':$rtn='112';break;
            case'400':$rtn='100';break;
            case'420':$rtn='105';break;
            case'450':$rtn='102';break;
            case'500':$rtn='120';break;//vDesKripsi = ""'isinya dikosongkan karena sudah dikonversi ke vS_JnsBrg, vS_JmlBrg,... tambahan v5x
            case'600':$rtn='114';break;
            case'700':$rtn=$kdres;break;//dihilangkan
            case'800':$rtn='150';break;
            case'900':$rtn=$kdres;break;
            case'010':$rtn='066';break;
            case'011':$rtn='061';break;
            case'023':$rtn='087';break;
        }
        return $rtn;
    }
    
    private function ConvertNIP($strNip){
        $vNip = trim($strNip);
        if(strlen($vNip)===14){
            $ThLahir  = ((int)substr($vNip,0,2)>50)?'19':'20';
            $ThAngkat = ((int)substr($vNip,6,2)>50)?'19':'20';
            $rtn = $ThLahir . substr($vNIP,0,6) . $ThAngkat . substr($vNIP, 6, 8);
        }else{
            $rtn = $vNip;
        }
        return $rtn;
    }
    
    function actAutoRespon(){
        $this->load->library("nuSoap_lib");
        $this->load->model('actMain');
        $this->nuSoap_lib = new nusoap_client(base_url('Extreme.jws'),true);
        if ($this->nuSoap_lib->fault) {
            $rtn = 'Error: ' . $nuSoap_lib->fault;
        }
        else {
            if ($this->nuSoap_lib->getError()) {
                $rtn = 'Error: ' . $nuSoap_lib->getError();
            }
            else {  
                unset($ws);
                $loginKey['userEDI'] = $this->EDINumber;
                $loginKey['string0'] = $this->newsession->userdata('UNLOCKCODE');
                #login ke web services EDII
                unset($msgErr);
                $ws = $this->nuSoap_lib->call('login', $loginKey,'http://www.openuri.org/');
                if(isset($ws['loginResult'])){
                    $SQL = "SELECT LASTDAYEXEC,DELMAIL,DELPOST FROM M_TRADER WHERE KODE_TRADER = ".$this->kd_trader;
                    $flg = $this->actMain->get_result($SQL);
                    switch ($ws['loginResult']){
                        case'1':$msgErr['Login'] = '1-Login Fail Unauthorized user';break;
                        case'2':$msgErr['Login'] = '2-Login Fail EDINUMBER expired';break;
                        case'9':$msgErr['Login'] = '9-Login Fail Unknown Error';break;
                        default:$ws['loginResult'] = explode('|', $ws['loginResult']);$msgErr['Login'] = '0-Login Success';break;
                    }
                    if($ws['loginResult'][1]=='1000'){
                    #EstablishRelationship
                    #TransferDocuments
                        #update status dokumen post box
                        $lstBox = $this->listPostBox($ws['loginResult'][0]);
                        foreach($lstBox as $a){
                            $arr = explode(',',$a);
                            unset($UPD);
                            switch ($arr[2]) {
                                case 'DOKPIB09':
                                    switch ($arr[5]){
                                        case '0':$UPD['STATUS'] = '030';break;
                                        case '1':$UPD['STATUS'] = '040';break;
                                        default :$UPD['STATUS'] = '050';break;
                                    }
                                    #execute update status
                                    $this->db->update('t_bc20hdr', $UPD, "KODE_TRADER = ".$this->kd_trader." AND SNRF = '".$arr[3]."' AND STATUS IN ('000','010','020','030','040','050')");
                                break;
                            }
                            unset($arr);
                        }
                        
                    #ReceiveDocument
                        #listMailBox
                        $lstMailBox = $this->listMailBox($ws['loginResult'][0]);
                        foreach($lstMailBox as $a){
                            if($a!==''){
                                $arr = explode(',',$a);
                                unset($key);
                                $key['sessionNumber']= $ws['loginResult'][0];
                                $key['id']           = $arr[0];
                                #download file
                                $dok = $this->nuSoap_lib->call('extractDocByte', $key,'http://www.openuri.org/');
                                $isi = str_replace("~M~", "?'", str_replace("'", "'\n", str_replace("?'", "~M~", base64_decode($dok['extractDocByteResult'])))) ;
                                #buat file
                                $EDIPaht    = 'EDI/'.$this->EDINumber ;
                                if(!is_dir($EDIPaht)){mkdir($EDIPaht,0777,true);}
                                $fullPath   = $EDIPaht .'/'. $arr[0].'.'. $arr[1].'.'.$arr[2].'.'.$arr[3].'.'.$arr[4].'.EDI';
                                if (file_exists($fullPath)){unlink($fullPath);}
                                $handle = fopen($fullPath, 'w');
                                fwrite($handle, $isi);
                                fclose($handle);
                                if (file_exists($fullPath)){
                                    #buat array insert
                                    unset($arrInBound);
                                    $arrInBound['KODE_TRADER'] = $this->kd_trader;
                                    $arrInBound['IDDOK']       = $arr[0];
                                    $arrInBound['PARTNER']     = $arr[1];
                                    $arrInBound['APRF']        = $arr[2];
                                    $arrInBound['SNRF']        = $arr[3];
                                    $arrInBound['SENDAT']      = date('Y-m-d H:i:s', strtotime($arr[4]));
                                    $arrInBound['RECEIVEAT']   = date('Y-m-d H:i:s');
                                    $arrInBound['DIREDI']      = $fullPath;
                                    $arrInBound['STATUS']      = '00';
                                    //return print_r($arrInBound,true);
                                    #insert ke m_trader_dokinbound
                                    $exe = $this->actMain->insertRefernce('M_TRADER_DOKINBOUND', $arrInBound);
                                    if($exe){
                                        #update file di IDX
                                        $this->nuSoap_lib->call('updateStatusMB', array('id'=>$arr[0]),'http://www.openuri.org/');
                                    }
                                }
                            }
                        }
                        
                        //Translasi
                        $Translator = new nusoap_client('http://192.168.5.22:8080/EDITranslator/Services?wsdl');
                        if (!$Translator->fault) {
                            if (!$Translator->getError()) {
                                #select semua aprf yang responnya 00
                                $SQL = "SELECT DISTINCT APRF FROM M_TRADER_DOKINBOUND WHERE KODE_TRADER = ".$this->kd_trader." AND STATUS = '00'";
                                $data = $this->actMain->get_result($SQL,true);
                                chmod('FLAT/'.$this->EDINumber ,0777);
                                foreach ($data as $a){
                                    $parameter['EDINumber'] = $this->EDINumber;
                                    $parameter['APRF']      = $a['APRF'];
                                    $Translator->call('extractEDIFACT', $parameter, 'http://services.translator.edii/');
                                }
                            }
                        }
                        unset($Translator);
                        
                        #select in bound yang berstatus 00
                        $SQL = "SELECT IDDOK,PARTNER,APRF,DIREDI FROM M_TRADER_DOKINBOUND WHERE KODE_TRADER = ".$this->kd_trader." AND STATUS = '00'";
                        $inb = $this->actMain->get_result($SQL,true);
                        $jmlInb = count($inb);
                        if($jmlInb>0){
                            foreach($inb as $a){
                                $oldEDI = $a['DIREDI'];
                                $nmFile = basename($oldEDI, '.EDI');
                                $oldFLT = 'FLAT/'.$this->EDINumber.'/' . $nmFile . '.FLT';
                                if(file_exists($oldFLT) && file_exists($oldEDI)){
                                    $Path = 'TRANSACTION/' . $this->EDINumber . '/' . date('Ymd');
                                    if(!is_dir($Path)){mkdir($Path,0777,true);}
                                    $newEDI = $Path.'/'.$nmFile.'.EDI';
                                    if(file_exists($newEDI)){unlink($newEDI);}
                                    $newFLT = $Path.'/'.$nmFile.'.FLT';
                                    if(file_exists($newFLT)){unlink($newFLT);}
                                    rename($oldEDI, $newEDI);
                                    rename($oldFLT, $newFLT);
                                    unset($UPD);
                                    $UPD['KODE_TRADER'] = $this->kd_trader;
                                    $UPD['IDDOK']   = $a['IDDOK'];
                                    $UPD['PARTNER'] = $a['PARTNER'];
                                    $UPD['APRF']    = $a['APRF'];
                                    $UPD['DIREDI']  = $newEDI;
                                    $UPD['DIRFLT']  = $newFLT;
                                    $UPD['STATUS']  = '01';
                                    $this->actMain->insertRefernce('M_TRADER_DOKINBOUND',$UPD);
                                }
                            }
                        }
                        
                        #select in bound yang berstatus 01 untuk di translate
                        $SQL = "SELECT IDDOK,PARTNER,APRF,DIREDI,DIRFLT FROM M_TRADER_DOKINBOUND WHERE KODE_TRADER = ".$this->kd_trader." AND STATUS = '01'";
                        $inb = $this->actMain->get_result($SQL,true);
                        $jmlInb = count($inb);
                        if($jmlInb>0){
                            foreach ($inb as $a){
                                switch($a['APRF']){
                                    case'RESPIB09':
                                        $exePIB = $this->RESPIB09($a['DIRFLT'], $this->actMain, $a);
                                        $jmlRcv['RESPIB09'] += $exePIB;
                                        break;
                                }
                                
                            }
                        }
                        
                    #Execute 1 day 1 time
                        if($flg['LASTDAYEXEC'] < date('Ymd')){
                            unset($arrKey);
                            $arrKey['sessionNumber']= $ws['loginResult'][0];
                            $arrKey['userEDI']      = $this->EDINumber;
                            $arrKey['string0']      = $this->unLockCode;
                            $arrKey['day']          = $flg['DELPOST'];
                            #deletePostBox
                            $this->nuSoap_lib->call('deletePostbox', $arrKey,'http://www.openuri.org/');
                            
                            #deleteMailBox
                            $arrKey['day']          = $flg['DELMAIL'];
                            $this->nuSoap_lib->call('deleteMailBox', $arrKey,'http://www.openuri.org/');
                            
                            #update Data trader
                            unset($UPD);
                            $UPD['KODE_TRADER']     = $this->kd_trader;
                            $UPD['LASTDAYEXEC']     = date('Ymd');
                            $this->actMain->insertRefernce('M_TRADER', $UPD);
                        }
                        $jmlRes = (int)$jmlRcv['RESPIB09'];
                        if($jmlRes>0){
                            $rtn = 'RES|'.$jmlRes.' Respon PIB diterima <br>';
                        }else{
                            $rtn = '';
                        }
                    }
                    else{
                        $rtn = 'RES|'.$msgErr['Login'];
                    }
                }
                else{
                    $rtn = 'RES|Error Login IDX Estreme';
                }
            }
        }
        return $rtn;
    }
    
    function deleteIndound($datas){
        $this->load->model('actMain');
        $rtn = false;
        foreach ($datas as $data){
            $arrA   = explode('|', $data);
            $whr['KODE_TRADER'] = $this->kd_trader ;
            $whr['IDDOK']       = $arrA[0];
            $whr['PARTNER']     = $arrA[1];
            $whr['APRF']        = $arrA[2];
            $sql = "SELECT * FROM m_trader_dokinbound WHERE KODE_TRADER = '" . $whr['KODE_TRADER'] . "' AND IDDOK = '" . $whr['IDDOK'] . "' AND PARTNER = '" . $whr['PARTNER'] . "' AND APRF = '" . $whr['APRF'] . "'";
            $arr = $this->actMain->get_result($sql);
            if(unlink($arr['DIRFLT']) && unlink($arr['DIREDI'])){
                $this->db->where($whr);
                $this->db->delete(array('m_trader_dokinbound'));
                $rtn = $rtn && true;
            }
        }
        return $rtn;
    }
    
    function resendDokOutbound($datas){
        $this->load->model('actMain');
        $rtn = false;
        foreach ($datas as $data){
            $arrA   = explode('|', $data);
            $whr['KODE_TRADER'] = $this->kd_trader ;
            $whr['KDKPBC']      = $arrA[1];
            $whr['JNSDOK']      = $arrA[2];
            $whr['CAR']         = $arrA[0];
            $whr['STATUS']      = '00';
            $rtn = $this->actMain->insertRefernce('m_trader_dokoutbound',$whr);
        }
        return $rtn;
    }
    
}