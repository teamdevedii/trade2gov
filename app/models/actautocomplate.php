<?php
if (!defined('BASEPATH')){exit('No direct script access allowed');}

class actAutocomplate extends CI_Model {
    function getData($genre, $keys, $by = "", $grp = "") {
        switch ($genre) {
            case'KPBC':
                $query = "SELECT KDKPBC as 'id', concat(KDKPBC,' - ',URAIAN_KPBC) as 'label' ,concat(KDKPBC,'|',URAIAN_KPBC) as 'value' FROM M_KPBC WHERE (LOWER(KDKPBC) LIKE '$keys%' OR LOWER(URAIAN_KPBC) LIKE '$keys%') LIMIT 10";
                break;
            case'PEMASOK':
                $query = "SELECT PASOKNAMA as 'id', 
                         concat(PASOKNAMA,' - ',PASOKALMT,' - ',PASOKNEG) as 'label' , 
                         concat(PASOKNAMA,'|',PASOKALMT,'|',PASOKNEG,'|',f_negara(PASOKNEG)) as 'value' 
                  FROM m_trader_supplier 
                  WHERE (LOWER(PASOKNAMA) LIKE '$keys%' OR LOWER(PASOKALMT) LIKE '$keys%') AND KODE_TRADER = " . $this->newsession->userdata('KODE_TRADER') . " LIMIT 10";
                break;
            case'NEGARA':
                $addQuery = "WHERE (LOWER(KODE_NEGARA) LIKE '$keys%' OR LOWER(URAIAN_NEGARA) LIKE '$keys%')";
                if ($by != '') {
                    $addQuery = "WHERE LOWER(" . $by . ") LIKE '$keys%'";
                }
                $query = "SELECT KODE_NEGARA as 'id', 
                                 concat(KODE_NEGARA,' - ',URAIAN_NEGARA) as 'label' ,  
                                 concat(KODE_NEGARA,'|',URAIAN_NEGARA) as 'value' 
                          FROM M_NEGARA 
                          " . $addQuery . " LIMIT 10";
                // WHERE (LOWER() LIKE '%$keys%' OR LOWER() LIKE '%$keys%')
                break;
            case'INDENTOR':
                $addQuery = "WHERE (LOWER(INDNPWP) LIKE '$keys%' OR LOWER(INDNAMA) LIKE '$keys%')";
                if ($by != '') {
                    $addQuery = "WHERE LOWER(" . $by . ") LIKE '$keys%'";
                }
                $addQuery .= " AND KODE_TRADER = " . $this->newsession->userdata('KODE_TRADER');
                $query = "SELECT concat(INDNAMA,' - ',INDNPWP) as 'label' ,  
                                 concat(INDNPWP,'|',INDNAMA,'|',INDALMT,'|',INDID) as 'value' 
                          FROM M_TRADER_INDENTOR 
                          " . $addQuery . " LIMIT 10";
                break;
            case'MODA':
                $query = "SELECT    concat(ANGKUTNAMA,' - ',ANGKUTFL) as 'label' ,  
                                    concat(MODA,'|',ANGKUTNAMA,'|',ANGKUTFL,'|',B.URAIAN_NEGARA) as 'value' 
                          FROM M_TRADER_KAPAL A LEFT JOIN M_NEGARA B ON A.ANGKUTFL = B.KODE_NEGARA 
                          WHERE ANGKUTNAMA LIKE '$keys%' AND KODE_TRADER = " . $this->newsession->userdata('KODE_TRADER') . " LIMIT 10";
                break;
            case'MODAMANIFEST':
                $query = "SELECT    concat(NMMODA,' - ',B.URAIAN_NEGARA) as 'label' ,  
                                   concat(REGMODA,'|',NMMODA,'|',FLAGMODA,'|',B.URAIAN_NEGARA,'|',GT,'|',LOA,'|',DRAFTDEPAN,'|',DRAFTBELAKANG) as 'value' 
                          FROM M_TRADER_KAPALMANIFEST A LEFT JOIN M_NEGARA B ON A.FLAGMODA = B.KODE_NEGARA 
                          WHERE $by LIKE '$keys%' AND MODA = '$grp' AND KODE_TRADER = " . $this->newsession->userdata('KODE_TRADER') . " LIMIT 10";
                break;
            case'FASILITAS':
                $query = "SELECT    concat(KDREC,' - ',URAIAN) as 'label' ,  
                                    concat(KDREC,'|',URAIAN) as 'value' 
                          FROM M_TABEL
                          WHERE KDTAB = 'SKEP_FASILITAS' AND MODUL = 'BC20' AND (KDREC LIKE '$keys%' OR URAIAN LIKE '$keys%')LIMIT 10";
                break;
            case'TIMBUN':
                $query = "SELECT    concat(KDGDG,' - ',URAIAN) as 'label' ,  
                                    concat(KDGDG,'|',URAIAN) as 'value' 
                          FROM M_GUDANG
                          WHERE KDKPBC = '" . $grp . "' AND " . $by . " LIKE '$keys%' LIMIT 10"; //PRINT_R($query);DIE();
                break;            
            case'VALUTA':
                $query = "SELECT    concat(KDEDI,' - ',UREDI) as 'label' ,  
                                    concat(KDEDI,'|',UREDI) as 'value' 
                          FROM M_VALUTA
                          WHERE " . $by . " LIKE '$keys%' LIMIT 10";
                break;
            case'DAERAH':
                $query = "SELECT    concat(KDDAERAH,' - ',URDAERAH,' - ',IBUKOTA) as 'label' ,  
                                    concat(KDDAERAH,'|',URDAERAH,'|',IBUKOTA) as 'value' 
                          FROM m_daerah
                          WHERE " . $by . " LIKE '$keys%' LIMIT 10";
                break;
            case'VALUTA2':
                $query = "SELECT    concat(KDEDI,' - ',UREDI) as 'label' ,  
                                    concat(KDEDI,'|',KDEDI) as 'value' 
                          FROM M_VALUTA
                          WHERE " . $by . " LIKE '$keys%' LIMIT 10";
                break;
            case'PELABUHAN':
                if ($grp != '') {
                    $addQuery = " AND KODE_NEGARA = '" . $grp . "' ";
                }
                $query = "SELECT    concat(KODE_PELABUHAN,' - ',URAIAN_PELABUHAN) as 'label' ,  
                                    concat(KODE_PELABUHAN,'|',URAIAN_PELABUHAN) as 'value' 
                          FROM M_PELABUHAN
                          WHERE " . $by . " LIKE '$keys%' $addQuery LIMIT 10"; //die($query);
                break;
            case'HS-TARIF': //NOHS;SERITRP;TRPBM;TRPPPN;TRPPBM;TRPCUK;TRPPPH
                $query = "SELECT concat(NOHS,' - ',SERITRP) as 'label',
                                 concat(NOHS,'|',SERITRP,'|',TRPBM,'|',TRPPPN,'|',TRPPBM,'|',TRPCUK,'|',TRPPPH) as 'value' 
                          FROM M_TRADER_TARIF
                          WHERE NOHS LIKE '$keys%' AND KODE_TRADER = ".$this->newsession->userdata('KODE_TRADER')."  LIMIT 10";
                break;
            case'HS-BARANG':
                $query = "SELECT HS AS 'label', CONCAT(HS,'|', SUBSTRING(BRGURAI, 1, 40),'|',SUBSTRING(BRGURAI, 41, 40),'|',SUBSTRING(BRGURAI, 81, 40),'|',SUBSTRING(BRGURAI, 121, 40) ,'|',MERK,'|',SIZE,'|',TYPE,'|',KDBRG) AS 'value'
                            FROM m_trader_barang_bc30
                          WHERE HS LIKE '$keys%' LIMIT 10";
               //die ($query);
			   
			    break;
            case'KEMASAN':
                $query = "SELECT concat(KODE_KEMASAN,' - ',URAIAN_KEMASAN) as 'label',
                                 concat(KODE_KEMASAN,'|',URAIAN_KEMASAN) as 'value' 
                          FROM M_KEMASAN
                          WHERE KODE_KEMASAN LIKE '$keys%' LIMIT 10";
                break;
            case'SATUAN':
                $query = "SELECT concat(KDEDI,' - ',UREDI) as 'label',
                                 concat(KDEDI,'|',UREDI) as 'value' 
                          FROM M_SATUAN
                          WHERE KDEDI LIKE '$keys%' LIMIT 10";
                break;
            case'IMPORTIR':
                $query = "SELECT concat(IMPNPWP,' - ',IMPNAMA,' - ',IMPALMT) as 'label',
                                 concat(IMPID,'|',IMPNPWP,'|',IMPNAMA,'|',IMPALMT,'|',APIKD,'|',APINO) as 'value'
                          FROM   m_trader_importir
                          WHERE  LOWER(" . $by . ") LIKE '$keys%' AND KODE_TRADER = " . $this->newsession->userdata('KODE_TRADER') . " LIMIT 10";
                break;
            case'TAMBAT':
                $query = "SELECT    KD_TAMBAT, NM_TAMBAT 
                          FROM M_TAMBAT
                          WHERE KDKPBC = '" . $grp . "' AND " . $by . " LIKE '$keys%' LIMIT 10";
                break;
            case'IDPENGANGKUT':
                $query = "SELECT MODUL, KDTAB, KDREC, URAIAN 
                          FROM M_TABEL 
                          WHERE KDTAB = 'JENIS_IDENTITAS' AND MODUL = 'BC10'";
                break;
            case'INDENTOREKS':
                $addQuery = "WHERE (LOWER(NPWPQQ) LIKE '$keys%' OR LOWER(NAMAQQ) LIKE '$keys%')";
                if ($by != '') {
                    $addQuery = "WHERE LOWER(" . $by . ") LIKE '$keys%'";
                }
                $addQuery .= " AND KODE_TRADER = " . $this->newsession->userdata('KODE_TRADER');
                $query = "SELECT concat(NAMAQQ,' - ',NPWPQQ) as 'label' ,  
                                 concat(IDQQ,'|',NPWPQQ,'|',NAMAQQ,'|',ALMTQQ,'|',NIPERQQ) as 'value' 
                          FROM M_TRADER_INDENTOREKS " . $addQuery . " LIMIT 10";
                break;
            case'PEMBELI':
                $query = "SELECT concat(NAMABELI,' - ',ALMTBELI) as 'label' ,  
                                 concat(NAMABELI,'|',ALMTBELI,'|',NEGBELI,'|',b.URAIAN_NEGARA) as 'value' 
                          FROM M_TRADER_PEMBELI a LEFT JOIN M_NEGARA b ON a.NEGBELI = b.KODE_NEGARA
                          WHERE (LOWER(NAMABELI) LIKE '$keys%') AND KODE_TRADER = " . $this->newsession->userdata('KODE_TRADER') . " LIMIT 10";
                break;
            case'EKSPORTIR': //IDEKS;NPWPEKS;NAMAEKS;ALMTEKS;NIPER;STATUSH;NOTDP;TGTDP
                $addQuery = "WHERE (LOWER(NAMAEKS) LIKE '$keys%' OR LOWER(NAMAQQ) LIKE '$keys%')";
                if ($by != '') {
                    $addQuery = "WHERE LOWER(" . $by . ") LIKE '$keys%'";
                }
                $query = "SELECT concat(NPWPEKS,' - ',NAMAEKS) as 'label',  
                                 concat(IDEKS,'|',NPWPEKS,'|',NAMAEKS,'|',ALMTEKS,'|',NIPER,'|',STATUSH,'|',NOTDP,'|',TGTDP) as 'value' 
                          FROM M_TRADER_EKSPORTIR 
                          $addQuery AND KODE_TRADER = " . $this->newsession->userdata('KODE_TRADER') . " LIMIT 10";
                break;
            case'EKSPORTIR2': //IDEKS;NPWPEKS;NAMAEKS;ALMTEKS;NIPER;STATUSH;NOTDP;TGTDP
                $addQuery = "WHERE (LOWER(NAMAEKS) LIKE '$keys%' OR LOWER(NAMAQQ) LIKE '$keys%')";
                if ($by != '') {
                    $addQuery = "WHERE LOWER(" . $by . ") LIKE '$keys%'";
                }
                $query = "SELECT concat(NPWPEKS,' - ',NAMAEKS) as 'label',  
                                 concat(IDEKS,'|',NPWPEKS,'|',NAMAEKS,'|',ALMTEKS) as 'value' 
                          FROM M_TRADER_EKSPORTIR 
                          $addQuery AND KODE_TRADER = " . $this->newsession->userdata('KODE_TRADER') . " LIMIT 10";
                break;
            case'HANGGAR':
                $query = "SELECT    concat(KDHGR,' - ',URHGR) as 'label' ,  
                                    concat(KDHGR,'|',URHGR) as 'value' 
                          FROM M_HANGGAR
                          WHERE KDKPBC = '" . $grp . "' AND " . $by . " LIKE '$keys%' LIMIT 10";
                break;
            case'BANK':
                $query = "SELECT    concat(KDBANK,' - ',NMBANK) as 'label' ,  
                                    concat(KDBANK,'-',NMBANK,'|') as 'value' 
                          FROM M_BANK
                          WHERE " . $by . " LIKE '%$keys%' LIMIT 10";
                $query = str_replace('~|', '(', $query);
                $query = str_replace('|~', ')', $query);
                break;
        }
        $hasil = $this->db->query($query); // $this->actMain->get_result($query);
        if ($hasil) {
            return $hasil->result_array();
        } else {
            return "";
        }
    }
    
    function checkCode($tipe, $data){
        $this->load->model('actMain');
        switch ($tipe){
            case'pelabuhan':$SQL = "SELECT URAIAN_PELABUHAN AS URAIAN FROM m_pelabuhan WHERE KODE_PELABUHAN = '" . $data['data'] . "'";break;
            case'kpbc':$SQL = "SELECT URAIAN_KPBC AS URAIAN FROM m_kpbc WHERE KDKPBC = '" . $data['data'] . "'";break;
            case'negara':$SQL = "SELECT URAIAN_NEGARA AS URAIAN FROM m_negara WHERE KODE_NEGARA = '" . $data['data'] . "'";break;
            case'kade':$SQL = "SELECT NM_TAMBAT AS URAIAN FROM m_tambat WHERE KD_TAMBAT = '" . $data['data'] . "'";break;
            case'kemasan':$SQL = "SELECT URAIAN_KEMASAN AS URAIAN FROM m_kemasan WHERE KODE_KEMASAN = '" . $data['data'] . "'";break;
            case'satuan':$SQL = "SELECT UREDI AS URAIAN FROM m_satuan WHERE KDEDI = '" . $data['data'] . "'";break;
            case'valuta':$SQL = "SELECT UREDI AS URAIAN FROM m_valuta WHERE KDEDI = '" . $data['data'] . "'";break;
            case'valuta2':$SQL = "SELECT KDEDI AS URAIAN FROM m_valuta WHERE KDEDI = '" . $data['data'] . "'";break;
            case'daerah':$SQL = "SELECT URDAERAH AS URAIAN FROM m_daerah WHERE KDDAERAH = '" . $data['data'] . "'";break;
            case'fasilitas':$SQL = "SELECT KDREC , URAIAN FROM m_tabel WHERE MODUL = 'BC20' AND KDTAB='SKEP_FASILITAS' AND KDREC = '" . $data['data'] . "'";break;
            
        }
        $urai = $this->actMain->get_uraian($SQL, 'URAIAN');
        return $urai;
    }
}

?>