<?php
set_time_limit(36000);
/* * *****************************************************************************
 * Class   : sspcp					                       *
 * File    : sspcpcetak.pdf.class.php	                                       *
 * Version	  : 1.2                                                        *
 * Date		  : 03-05-2006                                                 *
 * Author	  : Widya, Martin                                              *
 * License	  : Freeware                                                   *
 *                                                                              *
 * Class for print the SSPCP Document.                                          *
 *                                                                              *
 * You may use, modify and redistribute this software as you wish.              *
 * ***************************************************************************** */

class dokBC20x extends CI_Model {

    /** variabel untuk menyimpan koneksi database */
    var $db;

    /** variabel untuk menyimpan array MAP default */
    var $arrMap = array();

    /** variabel untuk menyimpan data ID SSPCP  */
    var $id = array();

    /** variabel untuk menyimpan tipe Print Out SSPCP */
    var $tdat = array();

    /** variabel sementara untuk print 6 digit */
    var $_jnsKdMap;
    var $petugasbank;
    var $temp;
    var $nPungutan = array();
    //var $tipe;	

    var $CAR;
    var $page;
    var $arrpage = array();
    var $noaju, $kdkpbc, $kpbc, $pibno, $pibtg; // Info Dok
    var $jnpib, $jnimp, $jkwaktu, $crbyr; // Info Jenis
    var $doktupkd, $doktupno, $doktuptg, $posno, $possub; // Info Dok
    var $impid_nama, $impid, $impnpwp, $impnama, $impalmt, $impstatus, $apikd, $apino; // Importir
    var $indid, $indnpwp, $indnama, $indalmt; // Indentor - QQ
    var $ppjkid, $ppjknpwp, $ppjknama, $ppjkalmt, $ppjkno, $ppjktg; // PPJK
    var $pasoknama, $pasokalmt, $pasokneg; // Supplier
    var $pelbkr, $pelbkr_nama, $pelmuat, $pelmuat_nama, $peltransit, $peltransit_nama; // Pelabuhan
    var $tmptbn, $tmptbn_nama; // Gudang
    var $moda, $moda_nama; // Mode Angkut
    var $angkutnama, $angkutno, $angkutfl, $tgtiba; // Angkutan
    var $kdval, $valuta, $ndpbm, $nilinv, $freight, $btambahan, $diskon; // Valuta - Nilai
    var $kdass, $asuransi; // Asuransi
    var $kdhrg, $fob, $cif; // Harga - FOB
    var $bruto, $netto; // Berat
    var $jmcont, $jmbrg; //Jumlah Container - Barang
    var $invno = array(), $invtgl = array(), $blawbno = array(), $blawbtgl = array();
    var $lcno, $lctgl, $skepkd, $skepno, $skeptgl; // Dok Inv - LC - BL/AWB - Skep
    var $status, $snrf, $kdfas, $fasilitas, $lengkap; // Info Pendukung Dok
    var $kota, $tanggal_ttd, $nama_ttd; //Data Penanggung jawab
    //var $nPungutan = array(); // Info Pungutan
    var $contno = array(), $contukur = array(), $conttipe = array(), $conttipe_nama = array(); // Peti Kemas
    var $jnkemas = array(), $jnkemas_nama = array(), $jmkemas = array(), $merkkemas = array(); // Jenis Kemasan

    /** Var PIB Detail */
    var $serial, $dnilinv, $nohs, $brgurai, $merk, $tipe, $spflain, $kemasjm, $nettodtl;
    var $kemasjn, $kemasjn_nama, $kdfasdtl, $kdfasdtl_nama, $brgasal, $brgasal_nama, $jmlsat, $kdsat, $kdsat_nama, $dcif;
    var $kdsatbm, $kdtrpbm, $trpbm, $kdcuk, $kdtrpcuk, $kdsatcuk, $trpcuk, $trpppn, $trppbm;
    var $kdfasbm, $fasbm, $kdfascuk, $fascuk, $kdfasppn, $fasppn, $kdfaspph, $faspph, $kdfaspbm, $faspbm; // PIB Fasilitas , $kdtrpbm

    /** Var Dokumen Lanjutan */
    var $dokkd_lanjutan, $dokno_lanjutan, $doktg_lanjutan, $dokkd_nama_lanjutan;

    /** Var Jumlah detail, kemasan, kontainer, dokumen */
    var $jmdtl, $jmkms, $jmcon, $jmdok;

    /** Var Dokumen SSPCP */
    var $nosspcp, $tglsspcp;

    /** Var PDF Dokumen */
    var $pdf, $kdprsh, $edinum_session;

    /** constructor */
    function sspcp($db, $id, $tdat, $petugas, $ttd, $kota, $jmlCetakPIB, $terminalID) {
        $this->db = $db;
        $this->id = $id;
        $this->ttd = $ttd;
        $this->kota = $kota;
        $this->petugasbank = $petugas;
        $this->tdat = $tdat;
        $this->jmlCetakPIB = $jmlCetakPIB;
        $this->terminalID = $terminalID;
        $this->arrMap = $this->setArrMAP();
    }

    function jnsKdMap($KodeMap) {
        $this->_jnsKdMap = $KodeMap;
    }

    /** fungsi untuk mencetak dokumen PDF */
    function displayPDF() {
        $this->load->library('fpdf');
        $this->pdf = $this->fpdf;
        $this->pdf->Open();
        $this->pdf->SetTitle('Dokumen Respon Pemberitahuan Import Barang');
        $this->pdf->SetAuthor('PT EDI Indonesia');
        $this->pdf->SetSubject('SSPCP');
        $this->pdf->SetLineWidth(0.1);
        $this->pdf->SetFillColor(255);
        $this->pdf->SetLeftMargin(45);
        $this->pdf->SetRightMargin(45);
        $this->pdf->Sety(30);
        $cnt = count($this->id);
        for ($i = 0; $i < $cnt; $i++) {
            $kunci = $this->id[$i];
            $cnttd = count($this->tdat);
            //PRINT TANDA TERIMA
            $this->pdf->SetLeftMargin(5);
            $this->pdf->SetRightMargin(5);
            for ($j = 0; $j < $cnttd; $j++) {
                $kunci2 = $this->tdat[$j];
                //echo $kunci2."<br>"; exit;
                if ($kunci2 != 5) {
                    switch ($kunci2) {
                        case "rs" :
                            $arrDokumen = $this->getData($kunci);
                            $this->drawTandaTerima($arrDokumen);
                            break;
                        case "ad" :
                            $arrDokumen = $this->getData($kunci);
                            $this->drawDebitAdvice($arrDokumen);
                            break;
                        case "all" :
                            $arrDokumen = $this->getData($kunci);
                            $this->drawSSPCPALL($arrDokumen);
                            break;
                        case "1a" :
                            $this->printSSPCP($kunci, $kunci2); //Create main document ; drawSSPCPALL
                            break;
                        case "2a" :
                            $this->printSSPCP($kunci, $kunci2); //Create main document ;
                            break;
                        case "3a" :
                            $this->printSSPCP($kunci, $kunci2); //Create main document ;
                            break;
                        case "4" :
                            $this->printSSPCP($kunci, $kunci2); //Create main document ;
                            break;
                        case "Pib" :
                            //echo $kunci; exit;
                            $this->CetakPIB($this->db, $kunci, $this->ttd, $this->kota);
                            break;
                    }
                }
            }
        }

        $this->pdf->Output();
    }

    /** fungsi untuk mengenerate SSPCP Page */
    function printSSPCP($kunci_data, $kunci_tipe) {
        $arrLembar = $this->getTipeData($kunci_tipe);
        $arrDokumen = $this->getData($kunci_data);
        //echo $arrLembar; exit;
        //if ($arrLembar != "P") {
        $this->drawSSPCP($arrLembar, $arrDokumen);
        //}
    }

    /** fungsi untuk menentukan tipe Print Out (KPBC Section) */
    function getTipeData($key) {
        $arrValue = array();
        //echo $key;
        $arrValue["kunci"] = $key;
        switch ($key) {
            case 'rs' :
                $arrValue["lembar1"] = 'rs';
                $arrValue["lembar2"] = 'rs';
                break;
            case '1a' :
                $arrValue["lembar1"] = 'KPBC';
                $arrValue["lembar2"] = '';
                //$arrValue["lembar1"] = 'Untuk KPBC Melalui';
                //$arrValue["lembar2"] = 'Penyetor/Wajib Pajak';
                break;
            case '1b' :
                $arrValue["lembar1"] = 'KPBC';
                $arrValue["lembar2"] = '';
                //$arrValue["lembar1"] = 'Untuk Penyetor/Wajib Pajak';
                //$arrValue["lembar2"] = '';
                break;
            case '2a' :
                $arrValue["lembar1"] = 'KPPN';
                $arrValue["lembar2"] = '';
                //$arrValue["lembar1"] = 'Untuk KPBC Melalui KPKN';
                //$arrValue["lembar2"] = '';
                break;
            case '2b' :
            case '2c' :
                $arrValue["lembar1"] = 'Untuk KPP Melalui KPKN';
                $arrValue["lembar2"] = '';
                break;
            case '2d' :
            case '2e' :
                $arrValue["lembar1"] = '';
                $arrValue["lembar2"] = '';
                break;
            case '3a' :
            case '3b' :
                $arrValue["lembar1"] = 'PENYETOR';
                $arrValue["lembar2"] = '';
                //$arrValue["lembar1"] = 'Untuk KPP Melalui Penyetor';
                //$arrValue["lembar2"] = 'Wajib Pajak';
                break;
            case '4' :
                $arrValue["lembar1"] = 'BANK/KANTOR POS & GIRO';
                $arrValue["lembar2"] = '';
                //$arrValue["lembar1"] = 'Untuk Bank Devisa Persepsi';
                //$arrValue["lembar2"] = '';
                break;
            /* case 'Pib' :
              break; */
        }
        return $arrValue;
    }

    /** fungsi untuk mendapatkan transaksi SSPCP */
    /* function getTXSSP($idbayar,$idsspcp){
      $arrValue = array("amount"=>"","kdmap"=>"", "ntpp" => "", "ntb" => "");
      //echo $idsspcp;exit;
      $this->db->connect();
      $sql = "select TXAMT, KDMAP, NTPP, NTB from tblTxSSP where idbyr = '".$idbayar."' and idsspcp = '".$idsspcp."'";
      //die();
      $data = $this->db->query($sql);
      if ($data->next()){
      $arrValue["amount"] = $data->get("TXAMT");
      $arrValue["kdmap"] = $data->get("KDMAP");
      $arrValue["ntpp"] = $data->get("NTPP");
      $arrValue["ntb"] = $data->get("NTB");
      }
      $this->db->disconnect();
      return $arrValue;
      } */

    function getTXSSP($kdmap, $idsspcp) {
        $arrValue = array("amount" => "", "kdmap" => "", "ntpp" => "", "ntb" => "");
        //echo $idsspcp;exit;
        $this->db->connect();
        $sql = "select TXAMT, KDMAP, NTPP, NTB from tblTxSSP where kdmap = '" . $kdmap . "' and idsspcp = '" . $idsspcp . "'";
        //echo $sql; exit;
        $data = $this->db->query($sql);
        if ($data->next()) {
            $arrValue["amount"] = $data->get("TXAMT");
            $arrValue["kdmap"] = $data->get("KDMAP");
            $arrValue["ntpp"] = $data->get("NTPP");
            $arrValue["ntb"] = $data->get("NTB");
        }
        $this->db->disconnect();
        return $arrValue;
    }

    /** fungsi untuk mendapatkan data SSPCP */
    function getData($kunci) {
        $arrSSPCP = array("kodekpbc" => "", "kpbc" => "", "kodebayar" => "", "kodesetor" => "", "npwp" => "", "nama" => "", "alamat" => "", "kota" => "",
            "kodepos" => "", "car" => "", "nospkbm" => "", "tgldokumen" => "", "mspjk" => "", "thpjk" => "", "total" => "",
            "nosspcp" => "", "kpkn" => "", "tgl" => "", "waktu" => "", "kodekpp" => "", "kpp" => "", "ntpnresi" => "", "ntbresi" => "", "jnsPenerimaan" => "",
            "KodeDokumen" => "", "kodeid" => "", "namadok" => "", "npwpa" => "", "npwpb" => "", "npwpc" => "", "cicilan" => "", "tglspkbm" => "", "idnpwp" => "", "tglbuku" => "", "tglByr" => "", "TrmlID" => "", "Ntb" => "", "NoRekCus" => "", "STAN" => "", "HdBranchNo" => "", "npwp_p" => "", "komisi" => "", "rate" => "", "tglByr2" => "");
        $sql = "SELECT tblSSPCP.KodeKPBC, tblSSPCP.KodeBayar, tblSSPCP.KodeSetor, tblSSPCP.NPWP, tblSSPCP.Nama, tblSSPCP.Alamat, tblSSPCP.Kota, 
			tblSSPCP.KodePos, tblSSPCP.Car, tblSSPCP.NoSPKBM, CONVERT(varchar, tblSSPCP.TglDokumen, 101) AS TGLDOKUMEN, tblSSPCP.MsPjk, 
			tblSSPCP.ThPjk, CAST(tblSSPCP.Total AS BIGINT) AS Total, tblSSPCP.NoSSPCP, tblSSPCP.KPKN, CONVERT(varchar, tblSSPCP.Tgl, 105) AS TGL, CONVERT(varchar, 
			tblSSPCP.Waktu, 108) AS WAKTU, tblSSPCP.KodeKPP, tblSSPCP.NTPP, tblSSPCP.jnsPenerimaan, tblSSPCP.KodeDokumen, tblSSPCP.KodeId, 
			kode_pembayaran.NAMADOK, tblSSPCP.NPWPA, tblSSPCP.NPWPB, tblSSPCP.NPWPC, tblSSPCP.cicilan, CONVERT(varchar, tblSSPCP.TglSPKBM, 105) 
			AS TglSPKBM, tblSSPCP.IdNpwp, CONVERT(varchar(10), tblSSPCP.tglbuku , 101)  AS tglbuku, CONVERT(varchar, tblSSPCP.DtAuthdByMP3 , 108)  AS tglByr, tblSSPCP.TrmlID, tblSSPCP.TxNo, tblSSPCP.NoRekCus, tblSSPCP.STAN, tblSSPCP.HdBranchNo, tblSSPCP.NPWPP, tblSSPCP.Komisi,tblSSPCP.rate, CONVERT(varchar(10), tblSSPCP.DtAuthdByMP3 , 101)  AS tglByr2
			FROM tblSSPCP INNER JOIN kode_pembayaran ON tblSSPCP.KodeBayar = kode_pembayaran.KDDOK 
			WHERE tblSSPCP.ID ='" . $kunci . "'";
        //echo $sql;exit;

        $this->db->connect();
        $data = $this->db->query($sql);
        if ($data->next()) {
            $arrSSPCP["kodekpbc"] = $data->get("KODEKPBC");
            $arrSSPCP["kodebayar"] = $data->get("KODEBAYAR");
            $arrSSPCP["kodesetor"] = $data->get("KODESETOR");
            $arrSSPCP["kodeid"] = $data->get("kodeid");
            $arrSSPCP["namadok"] = $data->get("NAMADOK");
            $arrSSPCP["npwpa"] = $data->get("NPWPA");
            $arrSSPCP["npwpb"] = $data->get("NPWPB");
            $arrSSPCP["npwpc"] = $data->get("NPWPC");
            $arrSSPCP["cicilan"] = $data->get("cicilan");
            $arrSSPCP["tglspkbm"] = $data->get("TglSPKBM");
            $arrSSPCP["idnpwp"] = $data->get("IdNpwp");
            //echo ($arrSSPCP["kodesetor"]);
            $arrSSPCP["npwp"] = $data->get("NPWP");
            $npwp = $arrSSPCP["npwp"];
            $imp = $this->db->query("select nama,alamat,kota,kodepos from tblImportirValid where npwp = '$npwp'");
            if ($imp->next()) {
                $arrSSPCP["nama"] = $imp->get("NAMA");
                $arrSSPCP["alamat"] = $imp->get("ALAMAT");
                $arrSSPCP["kota"] = $imp->get("KOTA");
                $arrSSPCP["kodepos"] = $imp->get("KODEPOS");
            } else {
                $arrSSPCP["nama"] = $data->get("NAMA");
                $arrSSPCP["alamat"] = $data->get("ALAMAT");
                $arrSSPCP["kota"] = $data->get("KOTA");
                $arrSSPCP["kodepos"] = $data->get("KODEPOS");
            }

            $penyetor = $this->db->query("select kpp,namapenyetor,domisili from tblImportir where npwp = '$npwp'");
            if ($penyetor->next()) {
                $arrSSPCP["namapenyetor"] = $penyetor->get("namapenyetor");
                $arrSSPCP["domisili"] = $penyetor->get("domisili");
                $arrSSPCP["domisili"] = $penyetor->get("domisili");
            } else {
                $arrSSPCP["namapenyetor"] = "Nama Jelas : ............................... ";
                $arrSSPCP["domisili"] = "............... ";
                $arrSSPCP["domisili"] = $penyetor->get("domisili");
            }
            if (trim($arrSSPCP["namapenyetor"]) == "") {
                $arrSSPCP["namapenyetor"] = "Nama Jelas : ............................... ";
            }
            if (trim($arrSSPCP["domisili"]) == "") {
                $arrSSPCP["domisili"] = "............... ";
            }
            $petugas = $this->db->query("select Nama,Jabatan,Departemen from tblPetugas where id = '$this->petugasbank'");
            if ($petugas->next()) {
                $arrSSPCP["petugasbank"] = $petugas->get("Nama");
                $arrSSPCP["jabatanbank"] = $petugas->get("Jabatan");
                $arrSSPCP["departemenbank"] = $petugas->get("Departemen");
            } else {
                $arrSSPCP["petugasbank"] = "Nama Jelas : ............................... ";
                $arrSSPCP["jabatanbank"] = "";
                $arrSSPCP["departemenbank"] = "";
            }

            $KPBC = $this->db->query("select KPBC from tblPartnerKPBC where Kode = '" . $arrSSPCP["kodekpbc"] . "'");
            if ($KPBC->next()) {
                $arrSSPCP["namaKPBC"] = $KPBC->get("KPBC");
            }

            $arrSSPCP["car"] = $data->get("CAR");
            $arrSSPCP["nospkbm"] = $data->get("NOSPKBM");
            $arrSSPCP["tgldokumen"] = $data->get("TGLDOKUMEN");
            $arrSSPCP["mspjk"] = $data->get("MSPJK");
            $arrSSPCP["thpjk"] = $data->get("THPJK");
            $arrSSPCP["total"] = $data->get("TOTAL");
            $arrSSPCP["nosspcp"] = $data->get("NOSSPCP");
            $arrSSPCP["kpkn"] = $data->get("KPKN");
            $arrSSPCP["tgl"] = $data->get("TGL");
            $arrSSPCP["waktu"] = $data->get("WAKTU");
            $arrSSPCP["kodekpp"] = $data->get("KODEKPP");
            $arrSSPCP["jpn"] = $data->get("jnsPenerimaan");
            $arrSSPCP["KodeDokumen"] = $data->get("KodeDokumen");
            $arrSSPCP["ntpn"] = $data->get("NTPP");
            $arrSSPCP["tglbuku"] = $data->get("tglbuku");
            $arrSSPCP["tglByr"] = $data->get("tglByr");
            $arrSSPCP["tglByr2"] = $data->get("tglByr2");
            $arrSSPCP["TrmlID"] = $data->get("TrmlID");
            $arrSSPCP["Ntb"] = $data->get("TxNo");
            $arrSSPCP["NoRekCus"] = $data->get("NoRekCus");
            $arrSSPCP["STAN"] = $data->get("STAN");
            $arrSSPCP["HdBranchNo"] = $data->get("HdBranchNo");
            $arrSSPCP["npwp_p"] = $data->get("NPWPP");
            $arrSSPCP["komisi"] = $data->get("KOMISI");
            $arrSSPCP["Rate"] = $data->get("Rate");
        }

        // KPBC
        $kodekpbc = $arrSSPCP["kodekpbc"];
        $sql = "select KPBC from tblPartnerKPBC where Kode='$kodekpbc'";
        $data = $this->db->query($sql);
        while ($data->next()) {
            $arrSSPCP["kpbc"] = $data->get("KPBC");
        }

        $npwp_p = $arrSSPCP["npwp_p"];
        $sql = "select NAMA,MataUang,Komisi from tblImportir where NPWP='$npwp_p'";
        $data = $this->db->query($sql);
        while ($data->next()) {
            $arrSSPCP["nama_p"] = $data->get("NAMA");
            $arrSSPCP["MU"] = $data->get("MATAUANG");
            $arrSSPCP["komisi"] = $data->get("KOMISI");
            if ($arrSSPCP["MU"] == "1") {
                $arrSSPCP["matauang"] = "Rp";
            } else {
                $arrSSPCP["matauang"] = "USD";
            }
        }

        $kodekpp = $arrSSPCP["kpkn"];
        $sql = "select nmkppn from tblkppn where kdkppn='$kodekpp'";
        $data = $this->db->query($sql);
        while ($data->next()) {
            $arrSSPCP["kpp"] = $data->get("nmkppn");
        }

        $sql = "select KDMAP,NTB from tblTxSSP where idsspcp = '$kunci'";
        $data = $this->db->query($sql);
        if ($data->next()) {
            $kdmaptemp = trim($data->get("KDMAP"));
        }
        if (strlen($kdmaptemp) == 4) {
            $map = 1;
        } elseif (strlen($kdmaptemp) == 6) {
            $map = 2;
        }

        $SQLC = "Select cabNama from tblCabangApp where kdcabBank='" . $arrSSPCP["HdBranchNo"] . "'";
        //die($SQLC);
        $ok = $this->db->query($SQLC);
        if ($ok->next()) {
            $arrSSPCP["cabNama"] = $ok->get("CABNAMA");
        }


        $sql = "SELECT TOP 1 NTPP, NTB, NoRek FROM TbltxSSP WHERE IDSSPCP = '$kunci' ORDER BY ID DESC";
        $data = $this->db->query($sql);
        while ($data->next()) {
            $arrSSPCP["ntpnresi"] = $data->get("NTPP");
            $arrSSPCP["ntbresi"] = $data->get("NTB");
            $arrSSPCP["NoRek"] = $data->get("NoRek");
        }



        // PENERIMAAN PABEAN DAN CUKAI
        $arrBea1 = $this->getTXSSP($this->arrMap[0][2], $kunci);
        $arrBea2 = $this->getTXSSP($this->arrMap[1][2], $kunci);
        $arrBea3 = $this->getTXSSP($this->arrMap[2][2], $kunci);
        $arrBea4 = $this->getTXSSP($this->arrMap[3][2], $kunci);
        $arrBea5 = $this->getTXSSP($this->arrMap[4][2], $kunci);
        $arrBea6 = $this->getTXSSP($this->arrMap[5][2], $kunci);
        $arrBea7 = $this->getTXSSP($this->arrMap[6][2], $kunci);
        $arrBea8 = $this->getTXSSP($this->arrMap[7][2], $kunci);
        $arrBea9 = $this->getTXSSP($this->arrMap[8][2], $kunci);
        $arrBea10 = $this->getTXSSP($this->arrMap[9][2], $kunci);
        $arrBea11 = $this->getTXSSP($this->arrMap[10][2], $kunci);
        $arrBea12 = $this->getTXSSP($this->arrMap[11][2], $kunci);
        $arrBea13 = $this->getTXSSP($this->arrMap[12][2], $kunci);
        $arrBea14 = $this->getTXSSP($this->arrMap[13][2], $kunci);
        $arrBea15 = $this->getTXSSP($this->arrMap[14][2], $kunci);
        $arrBea16 = $this->getTXSSP($this->arrMap[15][2], $kunci);
        $arrBea17 = $this->getTXSSP($this->arrMap[16][2], $kunci);
        $arrBea18 = $this->getTXSSP($this->arrMap[17][2], $kunci);
        $arrBea19 = $this->getTXSSP($this->arrMap[18][2], $kunci);
        $arrBea20 = $this->getTXSSP($this->arrMap[19][2], $kunci);
        /* $arrBea1 = $this->getTXSSP($this->arrMap[0][0],$kunci);
          $arrBea2 = $this->getTXSSP($this->arrMap[1][0],$kunci);
          $arrBea3 = $this->getTXSSP($this->arrMap[2][0],$kunci);
          $arrBea4 = $this->getTXSSP($this->arrMap[3][0],$kunci);
          $arrBea5 = $this->getTXSSP($this->arrMap[4][0],$kunci);
          $arrBea6 = $this->getTXSSP($this->arrMap[5][0],$kunci);
          $arrBea7 = $this->getTXSSP($this->arrMap[6][0],$kunci);
          $arrBea8 = $this->getTXSSP($this->arrMap[7][0],$kunci);
          $arrBea9 = $this->getTXSSP($this->arrMap[8][0],$kunci);
          $arrBea10 = $this->getTXSSP($this->arrMap[9][0],$kunci);
          $arrBea11 = $this->getTXSSP($this->arrMap[10][0],$kunci);
          $arrBea12 = $this->getTXSSP($this->arrMap[11][0],$kunci);
          $arrBea13 = $this->getTXSSP($this->arrMap[12][0],$kunci);
          $arrBea14 = $this->getTXSSP($this->arrMap[13][0],$kunci);
          $arrBea15 = $this->getTXSSP($this->arrMap[14][0],$kunci);
          $arrBea16 = $this->getTXSSP($this->arrMap[15][0],$kunci);
          $arrBea17 = $this->getTXSSP($this->arrMap[16][0],$kunci);
          $arrBea18 = $this->getTXSSP($this->arrMap[17][0],$kunci);
          $arrBea19 = $this->getTXSSP($this->arrMap[18][0],$kunci);
          $arrBea20 = $this->getTXSSP($this->arrMap[19][0],$kunci); */
        //print_r($arrBea16);
//		exit;
        $arrDokumen = array();
        $arrDokumen["MAP"] = $map;
        $arrDokumen["SSPCP"] = $arrSSPCP;
        $arrDokumen["BEA01"] = $arrBea1;
        $arrDokumen["BEA02"] = $arrBea2;
        $arrDokumen["BEA03"] = $arrBea3;
        $arrDokumen["BEA04"] = $arrBea4;
        $arrDokumen["BEA05"] = $arrBea5;
        $arrDokumen["BEA06"] = $arrBea6;
        $arrDokumen["BEA07"] = $arrBea7;
        $arrDokumen["BEA08"] = $arrBea8;
        $arrDokumen["BEA09"] = $arrBea9;
        $arrDokumen["BEA10"] = $arrBea10;
        $arrDokumen["BEA11"] = $arrBea11;
        $arrDokumen["BEA12"] = $arrBea12;
        $arrDokumen["BEA13"] = $arrBea13;
        $arrDokumen["BEA14"] = $arrBea14;
        $arrDokumen["BEA15"] = $arrBea15;
        $arrDokumen["BEA16"] = $arrBea16;
        $arrDokumen["BEA17"] = $arrBea17;
        $arrDokumen["BEA18"] = $arrBea18;
        $arrDokumen["BEA19"] = $arrBea19;
        $arrDokumen["BEA20"] = $arrBea20;
        return $arrDokumen;
        $this->db->disconnect();
    }

    /* ini untuk membuat PIB cetakan */

    // PIB DOKUMENT
    //CetakPIB($this->db,$data->get("CAR"),$ttd,$kota);
    function CetakPIB($db, $id, $ttd, $kota) {
        $nb = 1;
        $nbcont = 0;
        //query car
        $SQL = "SELECT CAR FROM tblSSPCP WHERE (ID = '" . $id . "')";
        $db->connect();
        $FQ = $db->query($SQL);
        //$FQ = $this->db->query($SQL);
        $FQ->next();
        $CAR = $FQ->get("Car");
        $this->CAR = $CAR;
        //echo $CAR; exit;
        $this->getJumlahDetail();
        $this->getJumlahCon();
        $this->getJumlahKms();
        $this->getJumlahDok();
        //dapetin jumlah print pib dengan detailnya.
        /* for ($x = 0; $x < $this->jmdtl; $x++) {//
          if($this->jmdtl <= 9){
          $nbcont=1;
          break;
          }
          else if($this->jmdtl > 9){
          if($x % 9==0){
          $nbcont++;
          }
          }
          }
          //halaman lebih
          if($this->jmdtl % 9 > 0 && $this->jmdtl > 9){
          $nbcont+=1;
          }

          if($this->jmdtl > 1 && $this->jmdtl <= 9){ #13-12-2011
          $nbcont+=1;
          }

          if($this->jmdtl % 9 == 0 && $this->jmdtl > 9){ #13-12-2011
          $nbcont+=1;
          }
         */
        $nbcont = ($this->jmdtl > 1) ? ceil($this->jmdtl / 9) : 0;
        $nb+=$nbcont;



        //echo $nbcont; exit;
        //$nb+=$nbcont;
        if ($this->jmcon > 8) {
            $nb+=1;
        }
        if ($this->jmdok >= 1) {
            $nb+=1;
        }

        if ($this->jmkms > 5) {
            $nb+=1;
        }
        for ($x = 0; $x < $this->jmlCetakPIB; $x++) {//
            $this->kota = $kota;
            $this->tanggal_ttd = date("d-m-Y");
            $this->nama_ttd = $ttd;

            $this->page = 1;
            $db->connect();
            //petugas bank			
            $petugasPIB = $db->query("select Nama from tblPetugas where id = '" . $this->petugasbank . "'");
            //$petugasPIB = $this->db->query("select Nama from tblPetugas where id = '".$this->petugasbank."'");
            $petugasPIB->next();
            $this->petugasbankPIB = $petugasPIB->get("Nama");
            //echo "select Nama from tblPetugas where id = '$this->petugasbank'";exit;								
            /* $this->pdf->cell(65.4,5,$kota.''.$ttd.''.$id,0,0,'L',0); */
            $this->pdf->AddPage();
            $this->getDataDokumenUtamaPIB();
            $this->getDataSSPCP();
            $this->drawHeaderPIB($nb);
            $this->db->connect();
            if ($this->jmdtl > 1) {
                $this->printLembarBarang($nb); //Create Lembar lanjutan Barang
                //echo "detil"; exit;
                //$temp=$this->pdf->Pageno()-$temppage;
            }
            if ($this->jmcon > 8) {
                $this->printLembarKontainer($nb); //Create Lembar lanjutan Kemasan
                //echo "kontainer"; exit;
                //$temp=$this->pdf->Pageno()-$temppage;
            }

            if ($this->jmdok >= 1) {
                $this->printLembarDokumen($nb); //Create Lembar lanjutan Dokumen
                //echo "dokumen"; exit;
                //$temp=$this->pdf->Pageno()-$temppage;
            }
            if ($this->jmkms > 5) {
                $this->printLembarKemasan($nb); //Create Lembar lanjutan Kemasan
                //echo "kemasan"; exit;
                //$temp=$this->pdf->Pageno()-$temppage;
            }

            $this->arrpage[$i] = $this->page;
            $this->db->disconnect();
//				$print = new pibPDF($db,$CAR,$this->petugasbank,$this->kota,$this->jnsPrinted,$jnLmbar);
        }

        //}
        for ($i = 0; $i <= count($this->arrpage); $i++) {
            //$string = str_replace($this->tes[0],$pale,$string);


            for ($n = 1; $n <= count($this->pdf->pages); $n++) {
                $this->pdf->pages[$n] = str_replace("{nb$i}", $this->arrpage[$i], $this->pdf->pages[$n]);
            }
        }
        /* for ($x = 0; $x < $this->jmlCetakPIB; $x++) {//
          $this->kota = $kota;
          $this->tanggal_ttd = date("d-m-Y");
          $this->nama_ttd = $ttd;

          //$this->page = 1;
          $nb="{nb$i}";

          //query car
          $SQL = "SELECT CAR FROM tblSSPCP WHERE (ID = '".$id."')";
          $db->connect();
          $FQ = $db->query($SQL);
          //$FQ = $this->db->query($SQL);
          $FQ->next();
          $CAR = $FQ->get("Car");
          $this->CAR = $CAR;
          //echo $CAR; exit;

          //petugas bank
          $petugasPIB = $db->query("select Nama from tblPetugas where id = '".$this->petugasbank."'");
          //$petugasPIB = $this->db->query("select Nama from tblPetugas where id = '".$this->petugasbank."'");
          $petugasPIB->next();
          $this->petugasbankPIB = $petugasPIB->get("Nama");
          //echo "select Nama from tblPetugas where id = '$this->petugasbank'";exit;
          /*$this->pdf->cell(65.4,5,$kota.''.$ttd.''.$id,0,0,'L',0);
          $this->pdf->AddPage();
          $this->getDataDokumenUtamaPIB();
          $this->getDataSSPCP();
          $this->drawHeaderPIB($nb);

          $this->db->connect();

          $this->getJumlahDetail();

          if ($this->jmdtl > 1){
          $this->printLembarBarang($nb); //Create Lembar lanjutan Barang
          //echo "detil"; exit;
          //$temp=$this->pdf->Pageno()-$temppage;
          }
          $this->getJumlahCon();
          if ($this->jmcon > 8){
          $this->printLembarKontainer($nb); //Create Lembar lanjutan Kemasan
          //echo "kontainer"; exit;
          //$temp=$this->pdf->Pageno()-$temppage;
          }
          $this->getJumlahDok();
          if ($this->jmdok >= 1){
          $this->printLembarDokumen($nb); //Create Lembar lanjutan Dokumen
          //echo "dokumen"; exit;
          //$temp=$this->pdf->Pageno()-$temppage;
          }
          $this->getJumlahKms();
          if ($this->jmkms > 5){
          $this->printLembarKemasan($nb); //Create Lembar lanjutan Kemasan
          //echo "kemasan"; exit;
          //$temp=$this->pdf->Pageno()-$temppage;
          }

          $this->arrpage[$i] = $this->page;

          $this->db->disconnect();
          //				$print = new pibPDF($db,$CAR,$this->petugasbank,$this->kota,$this->jnsPrinted,$jnLmbar);
          }

          //}
          for ($i=0;$i <= count($this->arrpage);$i++) {
          //$string = str_replace($this->tes[0],$pale,$string);


          for($n=1;$n<=count($this->pdf->pages);$n++){
          $this->pdf->pages[$n]=str_replace("{nb$i}",$this->arrpage[$i],$this->pdf->pages[$n]);
          }
          }
         */
    }

    function getDataSSPCP() {
        $this->db->connect();
        $SQL = "Select ID,NoSSPCP as NoSSPCP, convert(varchar,tgl,105) as TglSSPCP from tblSSPCP where car = '$this->CAR'";
        $data = $this->db->query($SQL);
        if ($data->next()) {
            $this->nosspcp = substr($data->get("NoSSPCP"), 0, 3) . '/' . substr($data->get("NoSSPCP"), 3, 3) . '/' . substr($data->get("NoSSPCP"), 6);
            $this->tglsspcp = $data->get("TglSSPCP");
            $this->IDsspcpNya = $data->get("ID");
        } else {
            $this->nosspcp = "";
            $this->tglsspcp = "";
            $this->IDsspcpNya = "";
        }
        $this->db->disconnect();
    }

    function getDataDokumenUtamaPIB() {

        $this->db->connect();
        // get data pibheader
        // get data pibheader
        $SQL = "select car,kdkpbc,pibno,convert(varchar,pibtg,105) as pibtg, jnpib,jnimp,jkwaktu,crbyr,doktupkd,doktupno, 
				convert(char,doktuptg,105) as doktuptg,posno,possub,impid,impnama,impnpwp,impalmt,impstatus,apikd, 
				apino,ppjkid,ppjknpwp,ppjknama,ppjkalmt,ppjkno,convert(char,ppjktg,105) as ppjktg,indid,indnpwp,indnama, 
				indalmt,pasoknama,pasokalmt,pasokneg,pelbkr,pelmuat,peltransit,tmptbn,moda,angkutnama, 
				angkutfl,angkutno,convert(char,tgtiba,105) as tgtiba,kdval,ndpbm,nilinv,freight,btambahan,diskon, 
				kdass,asuransi,kdhrg,fob,cif,bruto,netto,jmcont,jmbrg,status,snrf,kdfas,lengkap,EDINUM,NamaTTD,KotaTTD
				from tblpibhdr where car = '" . $this->CAR . "'";
        //echo "<br> $SQL"; exit;

        $data = $this->db->query($SQL);
        if ($data->next()) {
            $this->noaju = substr($data->get("CAR"), 0, 6) . " - " . substr($data->get("CAR"), 6, 6) . " - " . substr($data->get("CAR"), 12, 8) . " - " . substr($data->get("CAR"), 20);
            $this->kdkpbc = $data->get("KDKPBC");
            $this->pibno = $data->get("PIBNO");
            $this->pibtg = $data->get("PIBTG");
            $this->jnpib = $data->get("JNPIB");
            $this->jnimp = $data->get("JNIMP");
            $this->jkwaktu = $data->get("JKWAKTU");
            $this->crbyr = $data->get("CRBYR");
            $this->doktupkd = $data->get("DOKTUPKD");
            $this->doktupno = $data->get("DOKTUPNO");
            $this->doktuptg = $data->get("DOKTUPTG");
            $this->posno = $data->get("POSNO");
            $this->possub = $data->get("POSSUB");
            $this->impid = $data->get("IMPID");
            $this->impnpwp = $data->get("IMPNPWP");
            $this->impnama = $data->get("IMPNAMA");
            $this->impalmt = $data->get("IMPALMT");
            $this->impstatus = $data->get("IMPSTATUS");
            $this->apikd = $data->get("APIKD");
            $this->apino = $data->get("APINO");
            $this->ppjkid = $data->get("PPJKID");
            $this->ppjknpwp = $data->get("PPJKNPWP");
            $this->ppjknama = $data->get("PPJKNAMA");
            $this->ppjkalmt = $data->get("PPJKALMT");
            $this->ppjkno = $data->get("PPJKNO");
            $this->ppjktg = $data->get("PPJKTG");
            $this->indid = $data->get("INDID");
            $this->indnpwp = $data->get("INDNPWP");
            $this->indnama = $data->get("INDNAMA");
            $this->indalmt = $data->get("INDALMT");
            $this->pasoknama = $data->get("PASOKNAMA");
            $this->pasokalmt = $data->get("PASOKALMT");
            $this->pasokneg = $data->get("PASOKNEG");
            $this->pelbkr = $data->get("PELBKR");
            $this->pelmuat = $data->get("PELMUAT");
            $this->peltransit = $data->get("PELTRANSIT");
            $this->tmptbn = $data->get("TMPTBN");
            $this->moda = $data->get("MODA");
            $this->angkutnama = $data->get("ANGKUTNAMA");
            $this->angkutno = $data->get("ANGKUTNO");
            $this->angkutfl = $data->get("ANGKUTFL");
            $this->tgtiba = $data->get("TGTIBA");
            $this->kdval = $data->get("KDVAL");
            $this->ndpbm = $data->get("NDPBM");
            $this->nilinv = $data->get("NILINV");
            $this->freight = $data->get("FREIGHT");
            $this->btambahan = $data->get("BTAMBAHAN");
            $this->diskon = $data->get("DISKON");
            $this->kdass = $data->get("KDASS");
            $this->asuransi = $data->get("ASURANSI");
            $this->kdhrg = $data->get("KDHRG");
            $this->fob = $data->get("FOB");
            $this->cif = $data->get("CIF");
            $this->bruto = $data->get("BRUTO");
            $this->netto = $data->get("NETTO");
            $this->jmcont = $data->get("JMCONT");
            $this->jmbrg = $data->get("JMBRG");
            $this->status = $data->get("STATUS");
            $this->snrf = $data->get("SNRF");
            $this->kdfas = $data->get("KDFAS");
            $this->lengkap = $data->get("LENGKAP");
            $this->edinum = $data->get("EDINUM");
            $this->namaTTD = $data->get("NAMATTD");
            $this->kotaTTD = $data->get("KOTATTD");
        }

        // get data KPBC
        $SQL = "select * from tblkpbc where kdkpbc = '" . $this->kdkpbc . "'";
        $data = $this->db->query($SQL);
        if ($data->next()) {
            $this->kpbc = $data->get("URKDKPBC");
        }

        // get data importir
        $SQL = "select * from tbltabel where kdtab = '56' and kdrec = '" . $this->impid . "'";
        $data = $this->db->query($SQL);
        if ($data->next()) {
            $this->impid_nama = $data->get("URAIAN");
        }

        // get data indentor
        $SQL = "SELECT * FROM tbltabel where kdtab = '56' and kdrec = '" . $this->indid . "'";
        $data = $this->db->query($SQL);
        if ($data->next()) {
            $this->indid_nama = $data->get("URAIAN");
        } else {
            $this->indid_nama = '';
        }

        // get data moda
        $SQL = "select * from tbltabel where kdtab = '81' and kdrec = '" . $this->moda . "'";
        $data = $this->db->query($SQL);
        if ($data->next()) {
            $this->moda_nama = $data->get("URAIAN");
        }

        // get data angkut
        $SQL = "select * from tblnegara where kdedi = '" . $this->angkutfl . "'";
        $data = $this->db->query($SQL);
        if ($data->next()) {
            $this->angkut_negara = $data->get("UREDI");
        }

        // get data Gudang
        $SQL = "select * from tblgudang where kdgdg = '" . $this->tmptbn . "'";
        $data = $this->db->query($SQL);
        if ($data->next()) {
            $this->tmptbn_nama = $data->get("URAIAN");
        }

        // get data Pelabuhan muat
        $SQL = "select * from tblpelln where kdedi = '" . $this->pelmuat . "'";
        $data = $this->db->query($SQL);
        if ($data->next()) {
            $this->pelmuat_nama = $data->get("UREDI");
        }

        // get data Pelabuhan Transit
        $SQL = "select * from tblpelln where kdedi = '" . $this->peltransit . "'";
        $data = $this->db->query($SQL);
        if ($data->next()) {
            $this->peltransit_nama = $data->get("UREDI");
        }

        // get data Pelabuhan Bongkar
        $SQL = "select * from tblpeldn where kdedi = '" . $this->pelbkr . "'";
        $data = $this->db->query($SQL);
        if ($data->next()) {
            $this->pelbkr_nama = $data->get("UREDI");
        }

        // get data Valuta
        $SQL = "select * from tblvaluta where kdedi = '" . $this->kdval . "'";
        $data = $this->db->query($SQL);
        if ($data->next()) {
            $this->valuta = $data->get("UREDI");
        }

        // get skep fasilitas
        $SQL = "select * from tbltabel where kdtab = '75' and kdrec = '" . $this->kdfas . "'";
        $data = $this->db->query($SQL);
        if ($data->next()) {
            $this->fasilitas = $data->get("URAIAN");
        }

        // get dok invoice
        $SQL = "select dokno, convert(varchar,doktg,105) AS doktg from tblpibdok where dokkd = '380' and car = '" . $this->CAR . "'";
        $data2 = $this->db->query($SQL);
        $i = 1;
        $this->invno = "";
        $this->invtgl = "";
        while ($data2->next()) {
            $this->invno[$i] = "";
            $this->invno[$i] = $data2->get("DOKNO");
            $this->invtgl[$i] = $data2->get("DOKTG");
            $i = $i + 1;
        }
        if ($i > 4) {
            $this->invno = '';
            $this->invtgl = '';
            $this->invno[1] = '==lihat lampiran==';
        }

        // get LC
        $SQL = "select dokno, convert(varchar,doktg,105) AS doktg from tblpibdok where dokkd = '465' and car = '" . $this->CAR . "'";
        $data = $this->db->query($SQL);
        $x = 1;
        $this->lcno = "";
        $this->lctgl = "";
        while ($data->next()) {
            $this->lcno[$x] = "";
            $this->lcno[$x] = $data->get("DOKNO");
            $this->lctgl[$x] = $data->get("DOKTG");
            $x = $x + 1;
        }
        if ($x > 3) {
            $this->lcno = '';
            $this->lctgl = '';
            $this->lcno[1] = '==lihat lampiran==';
        }

        // get BL/AWB
        $SQL = "select dokno, convert(varchar,doktg,105) AS doktg from tblpibdok where dokkd in (704, 705, 740, 741) and car = '" . $this->CAR . "' order by dokKd asc";
        $data = $this->db->query($SQL);
        $i = 1;
        unset($this->blawbno);
        unset($this->blawbtgl);
        $this->blawbno = '';
        $this->blawbtgl = '';
        while ($data->next()) {
            $this->blawbno[$i] = $data->get("DOKNO");
            $this->blawbtgl[$i] = $data->get("DOKTG");
            $i = $i + 1;
        }
        if ($i > 3) {
            $this->blawbno = '';
            $this->blawbtgl = '';
            $this->blawbno[1] = '==lihat lampiran==';
        }

        // get Skep Fas
        $SQL = "select dokkd,dokno, convert(varchar,doktg,105) AS doktg from tblpibdok where dokkd in (814, 815, 851, 853, 911, 993, 998,861) and car = '" . $this->CAR . "' order by dokkd desc";
        $data = $this->db->query($SQL);
        $i = 1;
        $this->skepkd = "";
        $this->skepno = "";
        $this->skeptgl = "";
        while ($data->next()) {
            $this->skepkd = $data->get("DOKKD");
            $this->skepno = $data->get("DOKNO");
            $this->skeptgl = $data->get("DOKTG");
            $i = $i + 1;
        }
        // get skep Fas nama
        $SQL = "select uraian from tbltabel where kdtab = '06' and kdrec = '" . $this->skepkd . "'";
        $datanama = $this->db->query($SQL);
        $this->skepnama = "";
        if ($datanama->next()) {
            $this->skepnama = $datanama->get("URAIAN");
        }
        if ($i > 2) {
            $this->skepkd = '';
            $this->skepno = '';
            $this->skeptgl = '';
            $this->skepnama = '==lihat lampiran==';
        }

        // get Peti Kemas
        $SQL = "select contno,contukur,conttipe from tblpibcon where car = '" . $this->CAR . "'";
        $data = $this->db->query($SQL);
        $i = 1;
        $this->contno = "";
        $this->contukur = "";
        $this->conttipe = "";
        while ($data->next()) {
            $this->contno[$i] = $data->get("CONTNO");
            $this->contukur[$i] = $data->get("CONTUKUR");
            $this->conttipe[$i] = $data->get("CONTTIPE");
            $i++;
        }

        // get Jenis Kemasan
        $SQL = "select jnkemas,jmkemas,merkkemas from tblpibkms where car = '" . $this->CAR . "'";
        //echo "<br> $SQL";
        $data = $this->db->query($SQL);
        $i = 1;
        while ($data->next()) {
            $this->jnkemas[$i] = $data->get("JNKEMAS");
            $this->jmkemas[$i] = $data->get("JMKEMAS");
            $this->merkkemas[$i] = $data->get("MERKKEMAS");
            $jeniskms = $this->jnkemas[$i];
            // get Jenis Kemasan
            $SQL = "select * from tblkemasan where kdedi = '" . $jeniskms . "'";
            //echo "<br> $SQL";

            $datakemasan = $this->db->query($SQL);
            if ($datakemasan->next()) {
                $this->jnkemas_nama[$i] = $datakemasan->get("UREDI");
            }
            $i++;
        }

        // get barang - fasilitas
        $SQL = "SELECT tbl.serial, tbl.nohs, tbl.dnilinv, tbl.seritrp, tbl.brgurai, tbl.merk, tbl.tipe, tbl.spflain,
       				tbl.brgasal, tbl.dcif, tbl.kdsat, tbl.jmlsat, tbl.kemasjn, tbl.kemasjm,
       				tbl.satbmjm, tbl.satcukjm, tbl.nettodtl, tbl.kdfasdtl
  					FROM tblpibdtl tbl WHERE tbl.car = '" . $this->CAR . "'";
        //echo "<br> $SQL";

        $data = $this->db->query($SQL);
        if ($data->next()) {
            $this->serial = $data->get("SERIAL");
            // get PIB fasilitas
            $SQL = "select kdfasbm,fasbm,kdfascuk,fascuk,kdfasppn,fasppn,kdfaspph,faspph,kdfaspbm,faspbm from tblpibfas where serial = '" . $this->serial . "' and car = '" . $this->CAR . "'";
            $datapibfasilitas = $this->db->query($SQL);
            if ($datapibfasilitas->next()) {
                $this->kdfasbm = $datapibfasilitas->get("KDFASBM");
                $this->fasbm = $datapibfasilitas->get("FASBM");
                $this->kdfascuk = $datapibfasilitas->get("KDFASCUK");
                $this->fascuk = $datapibfasilitas->get("FASCUK");
                $this->kdfasppn = $datapibfasilitas->get("KDFASPPN");
                $this->fasppn = $datapibfasilitas->get("FASPPN");
                $this->kdfaspph = $datapibfasilitas->get("KDFASPPH");
                $this->faspph = $datapibfasilitas->get("FASPPH");
                $this->kdfaspbm = $datapibfasilitas->get("KDFASPBM");
                $this->faspbm = $datapibfasilitas->get("FASPBM");
            }
            $this->nohs = $data->get("NOHS");
            $this->brgurai = $data->get("BRGURAI");
            $this->dnilinv = $data->get("DNILINV");
            $this->merk = $data->get("MERK");
            $this->tipe = $data->get("TIPE");
            $this->spflain = $data->get("SPFLAIN");
            $this->kemasjm = $data->get("KEMASJM");
            $this->nettodtl = $data->get("NETTODTL");
            $this->kemasjn = $data->get("KEMASJN");
            $this->seritrp = $data->get("SERITRP");

            // get tarif
            $SQL = "select * from tblpibtrf where car = '" . $this->CAR . "' and seritrp = '" . $this->seritrp . "' and nohs = '" . $this->nohs . "'";
            //echo "SQL $SQL";
            $datatarif = $this->db->query($SQL);
            if ($datatarif->next()) {
                $this->kdtrpbm = $datatarif->get("KDTRPBM");
                //$this->kdsatbm = $data->get("KDSATBM");
                $this->trpbm = $datatarif->get("TRPBM");
                $this->kdcuk = $datatarif->get("KDCUK");
                $this->kdtrpcuk = $datatarif->get("KDTRPCUK");
                $this->kdsatcuk = $datatarif->get("KDSATCUK");
                $this->trpcuk = $datatarif->get("TRPCUK");
                $this->trpppn = $datatarif->get("TRPPPN");
                $this->trppbm = $datatarif->get("TRPPBM");
            }
            // get Jenis Kemasan
            $SQL = "select * from tblkemasan where kdedi = '" . $this->kemasjn . "'";
            //echo "<br> $SQL";

            $datakemasan = $this->db->query($SQL);
            if ($datakemasan->next()) {
                $this->kemasjn_nama = $datakemasan->get("UREDI");
            }
            $this->kdfasdtl = $data->get("KDFASDTL");

            // get Detail Fasilitas
            $SQL = "select * from tbltabel where kdrec = '" . $this->kdfasdtl . "' and kdtab = '75'";
            $datafasilitas = $this->db->query($SQL);
            if ($datafasilitas->next()) {
                $this->kdfasdtl_nama = $datafasilitas->get("URAIAN");
            } else {
                $this->kdfasdtl_nama = "";
            }
            $this->brgasal = $data->get("BRGASAL");

            // get Barang Asal
            $SQL = "select * from tblnegara where kdedi = '" . $this->brgasal . "'";
            //echo "<br> $SQL";

            $dataset = $this->db->query($SQL);
            if ($dataset->next()) {
                $this->brgasal_nama = $dataset->get("UREDI");
            }
            $this->jmlsat = $data->get("JMLSAT");
            $this->kdsat = $data->get("KDSAT");

            // get Kode Satuan
            $SQL = "select * from tblsatuan where kdedi = '" . $this->kdsat . "'";
            //echo "<br> $SQL";

            $datasatuan = $this->db->query($SQL);
            if ($datasatuan->next()) {
                $this->kdsat_nama = $datasatuan->get("UREDI");
            }
            $this->dcif = $data->get("DCIF");
            /* $this->kdtrpbm = $data->get("KDTRPBM");
              $this->kdsatbm = $data->get("KODESATBM");
              $this->trpbm = $data->get("TRPBM");
              $this->kdcuk = $data->get("KDCUK");
              $this->kdtrpcuk = $data->get("KDTRPCUK");
              $this->kdsatcuk = $data->get("KDSATCUK");
              $this->trpcuk = $data->get("TRPCUK");
              $this->trpppn = $data->get("TRPPPN");
              $this->trppbm = $data->get("TRPPBM");
             */
        }


        // get Pungutan
        $SQL = "select * from tblpibpgt where car = '" . $this->CAR . "'";
        //echo "<br> $SQL";
        $data = $this->db->query($SQL);
        $this->nPungutan = "";
        while ($data->next()) {
            $kdbeban = $data->get("KDBEBAN");
            $kdfasil = $data->get("KDFASIL");
            $nilbeban = $data->get("NILBEBAN");
            $this->nPungutan[$kdbeban][$kdfasil] = $nilbeban;
        }
        $SQL = "select kdprsh from tbldata where edinum = '" . $this->edinum . "'";
        //echo "<br> $SQL";
        $data = $this->db->query($SQL);
        if ($data->next()) {
            $this->kdprsh = $data->get("KDPRSH");
        }
        $this->db->disconnect();
    }

    function trimstr($strpotong, $panjang) {
        if (strlen($strpotong) > $panjang) {

            for ($i = $panjang; $i > 0; $i--) {
                $sub = substr($strpotong, $i, 1);
                if ($sub == " ") {
                    $potong = $i;
                    break;
                }
            }

            return array("str1" => substr($strpotong, 0, $potong), "str2" => substr($strpotong, $potong + 1, $panjang));
        } else {
            return array("str1" => $strpotong, "str2" => "");
            //$terbilang[0]	= $rupiah;
        }
    }

    function gettbltabel($dicari1, $dicari2) {
        $this->db->connect();
        $SQL = "Select uraian from tbltabel where kdtab = '$dicari2' and kdrec = '$dicari1'";
        $data = $this->db->query($SQL);
        if ($data->next()) {
            $gettbltabel = $data->get("uraian");
            return $gettbltabel;
            $this->db->disconnect();
        } else {
            $gettbltabel = "";
            return $gettbltabel;
        }
    }

    function setnocont($nocont) {
        if (count($nocont) != 0) {
            $hasile = substr($nocont, 0, 4) . '-' . substr($nocont, 4, 11);
        }
        return $hasile;
    }

    function formaths($hs) {
        //$formaths = substr($hs,0,4).'.'.substr($hs,4,2).'.'.substr($hs,6,2).'.'.substr($hs,8,2);
        $formaths = substr($hs, 0, 4) . '.' . substr($hs, 4, 2) . '.' . substr($hs, 6, 4);
        return $formaths;
    }

    function strip($strstrip) {
        if (trim($strstrip) != 0) {
            $hasile = $strstrip . '%';
        } else {
            $hasile = ' - ';
        }

        return $hasile;
    }

    function drawHeaderPIB($nb) {//
        //ambil nama petugas bank
        //$SQLPTGS = "SELECT Nama FROM tblPetugas WHERE ID = '".."'";
        //$this->db->connect();
        $this->pdf->SetX(5.4);
        $this->pdf->SetFont('times', 'B', '12');
        $this->pdf->cell(131.6, 4, 'PEMBERITAHUAN IMPOR BARANG (PIB)', 0, 0, 'R', 0);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->cell(67.6, 4, 'BC 2.0', 0, 0, 'R', 0);

        //KPBC
        $this->pdf->Rect(5.4, 9.4, 199.2, 20, 3.5, 'F');
        $this->pdf->Ln();
        $this->pdf->SetX(5.4);
        $this->pdf->cell(45, 4, 'Kantor Pabean', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(103, 4, $this->kpbc, 0, 0, 'L', 0);
        $this->pdf->cell(15, 4, $this->kdkpbc, 1, 0, 'C', 0);
        //$this->pdf->cell(30,4,' Halaman 1 dari {nb}',0,0,'R',0);	
        //$hapus = strlen($this->pdf->pages[$this->pdf->page]);	
        $this->pdf->cell(30, 4, " Halaman 1 dari $nb", 0, 0, 'R', 0); //
        //$this->tes[] = substr($this->pdf->pages[$this->pdf->page],$hapus);
        $this->pdf->AliasNbPages();
        $this->pdf->Ln();
        $this->pdf->SetX(5.4);
        $this->pdf->cell(45, 4, 'Nomor Pengajuan', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(80, 4, $this->noaju, 0, 0, 'L', 0);
        $this->pdf->Ln();

        // Section A
        $this->pdf->SetX(5.4);
        $this->pdf->cell(40, 4, 'A . Jenis PIB', 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, '', 0, 0, 'C', 0);
        $this->pdf->cell(5, 3, $this->jnpib, 1, 0, 'C', 0);
        $this->pdf->cell(30, 4, '1. Biasa', 0, 0, 'L', 0);
        $this->pdf->cell(30, 4, '2. Berkala', 0, 0, 'L', 0);
        $this->pdf->cell(89.2, 4, '3. Penyelesaian', 0, 0, 'L', 0);
        $this->pdf->Ln();

        // Section B
        $this->pdf->SetX(5.4);
        $this->pdf->cell(40, 4, 'B . Jenis Impor', 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, '', 0, 0, 'C', 0);
        $this->pdf->cell(5, 3, $this->jnimp, 1, 0, 'C', 0);
        $this->pdf->cell(30, 4, '1. Untuk Dipakai', 0, 0, 'L', 0);
        $this->pdf->cell(30, 4, '2. Sementara', 0, 0, 'L', 0);
        $this->pdf->cell(30, 4, '3. Reimpor', 0, 0, 'L', 0);
        //$this->pdf->cell(59.2,4,'4. Tempat Penimbunan Berikat (TPB)',0,0,'L',0); // Masih kurang waktu lama
        $this->pdf->cell(30, 4, '5. Pelayanan Segera', 0, 0, 'L', 0); // Masih kurang waktu lama
        $this->pdf->cell(30, 4, '6. Vooruitslag', 0, 0, 'L', 0);
        $this->pdf->Ln();

        // Section C
        $this->pdf->SetX(5.4);
        $this->pdf->cell(40, 4, 'C . Cara Pembayaran', 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, '', 0, 0, 'C', 0);
        $this->pdf->cell(5, 3, $this->crbyr, 1, 0, 'C', 0);
        $this->pdf->cell(30, 4, '1. Biasa/Tunai', 0, 0, 'L', 0);
        $this->pdf->cell(30, 4, '2. Berkala', 0, 0, 'L', 0);
        $this->pdf->cell(30, 4, '3. Dengan Jaminan', 0, 0, 'L', 0);
        $this->pdf->cell(30, 4, '9. Lainnya', 0, 0, 'L', 0);

        // Section D
        $this->pdf->Ln();
        $this->pdf->SetX(5.4);
        $this->pdf->cell(40, 4, 'D. DATA PEMBERITAHUAN', 0, 0, 'L', 0);

        //PEMASOK
        $this->pdf->Rect(5.4, 33.4, 99.6, 16, 3.5, 'F');
        $this->pdf->Rect(105, 33.4, 99.6, 16, 3.5, 'F');
        $this->pdf->Ln();
        $this->pdf->SetX(5.4);
        $this->pdf->cell(79.6, 4, 'PEMASOK', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, $this->pasokneg, 1, 0, 'C', 0);
        $this->pdf->cell(99.6, 4, 'F. DIISI OLEH BEA DAN CUKAI', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetX(5.4);
        $this->pdf->cell(99.6, 4, '1. Nama, Alamat, Negara', 0, 0, 'L', 0);
        $this->pdf->cell(54.6, 4, 'No. & Tgl Pendaftaran', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, $this->pibno, 1, 0, 'C', 0);
        $this->pdf->cell(5, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, $this->pibtg, 1, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(3, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(96.6, 4, $this->pasoknama, 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(3, 4, '', 0, 0, 'L', 0);
        $pskalmt = $this->trimstr($this->pasokalmt, 50);
        $this->pdf->cell(96.6, 4, $pskalmt['str1'], 0, 0, 'L', 0);

        $this->pdf->Rect(5.4, 49.4, 99.6, 36, 3.5, 'F');
        $this->pdf->Rect(105, 49.4, 99.6, 36, 3.5, 'F');
        $this->pdf->Ln();
        $this->pdf->cell(99.6, 4, 'IMPORTIR', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '15. Invoice', 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(50, 4, $this->invno[1], 0, 0, 'L', 0);
        $this->pdf->cell(24, 4, 'Tgl. ' . $this->invtgl[1], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(20, 3, ' 2. Identitas : ', 0, 0, 'L', 0);
        $this->pdf->cell(35, 3, $this->impid_nama . ' / ' . $this->impid, 0, 0, 'L', 0);
        $imppj = strlen(trim($this->impnpwp));
        if ($imppj == 15) {
            $npwpppjknya = $this->formatNPWP15($this->impnpwp);
        } else {
            $npwpppjknya = $this->formatNPWP12($this->impnpwp);
        }
        $npwpindknya = "";
        $namaindknya = $this->impnama;

        $npwpnyaitu = $npwpppjknya . $npwpindknya;
        $namanyaitu = $namaindknya;

        $this->pdf->cell(69.6, 3, $npwpnyaitu, 0, 0, 'L', 0);
        if (trim($this->invno[2] != "") || trim($this->invtgl[2] != "")) {
            //$this->pdf->cell(20,4,'',0,0,'L',0);
            //$this->pdf->cell(5,4,'',0,0,'C',0);
            $this->pdf->cell(50, 3, $this->invno[2], 0, 0, 'L', 0);
            $this->pdf->cell(24, 3, 'Tgl. ' . $this->invtgl[2], 0, 0, 'L', 0);
        }
        $this->pdf->Ln();
        $this->pdf->cell(20, 3, '', 0, 0, 'L', 0);
        $this->pdf->cell(35, 3, '', 0, 0, 'L', 0);
        $this->pdf->cell(69.6, 3, '', 0, 0, 'L', 0);
        if (trim($this->invno[3] != "") || trim($this->invtgl[3] != "")) {
            //$this->pdf->cell(20,4,'',0,0,'L',0);
            //$this->pdf->cell(5,4,'',0,0,'C',0);
            $this->pdf->cell(50, 3, $this->invno[3], 0, 0, 'L', 0);
            $this->pdf->cell(24, 3, 'Tgl. ' . $this->invtgl[3], 0, 0, 'L', 0);
        }

        $this->pdf->Ln(3);
        $this->pdf->cell(25, 4, ' 3. Nama, Alamat', 0, 0, 'L', 0);
        $this->pdf->cell(69.6, 4, ': ' . substr($namanyaitu, 0, 37), 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '16. LC', 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(50, 4, $this->lcno[1], 0, 0, 'L', 0);
        $this->pdf->cell(24, 4, 'Tgl. ' . $this->lctgl[1], 0, 0, 'L', 0);
        $this->pdf->Ln(3);
        $this->pdf->cell(3, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(96.6, 4, substr($this->impalmt, 0, 54), 0, 0, 'L', 0);
        if (trim($this->lcno[2] != "") || trim($this->lctgl[2] != "")) {
            $this->pdf->cell(20, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(5, 4, '', 0, 0, 'C', 0);
            $this->pdf->cell(50, 4, $this->lcno[2], 0, 0, 'L', 0);
            $this->pdf->cell(24, 4, 'Tgl. ' . $this->lctgl[2], 0, 0, 'L', 0);
        }
        //IMPORTIR
        $impstatus_a = substr($this->impstatus, 0, 1);
        switch ($impstatus_a) {
            case 1:
                $impstatus_a_nama = "IU";
                break;
            case 2:
                $impstatus_a_nama = "IP";
                break;
            case 3:
                $impstatus_a_nama = "IT";
                break;
            case 4:
                $impstatus_a_nama = "AT";
                break;
            case 5:
                $impstatus_a_nama = "BULOG";
                break;
            case 6:
                $impstatus_a_nama = "PERTAMINA";
                break;
            case 7:
                $impstatus_a_nama = "DAHANA";
                break;
            case 8:
                $impstatus_a_nama = "IPTN";
                break;
        }
        $impstatus_b = substr($this->impstatus, 1, 1);
        switch ($impstatus_b) {
            case 1:
                $impstatus_b_nama = "IU";
                break;
            case 2:
                $impstatus_b_nama = "IP";
                break;
            case 3:
                $impstatus_b_nama = "IT";
                break;
            case 4:
                $impstatus_b_nama = "AT";
                break;
            case 5:
                $impstatus_b_nama = "BULOG";
                break;
            case 6:
                $impstatus_b_nama = "PERTAMINA";
                break;
            case 7:
                $impstatus_b_nama = "DAHANA";
                break;
            case 8:
                $impstatus_b_nama = "IPTN";
                break;
        }
        $impstatus_c = substr($this->impstatus, 2, 1);
        switch ($impstatus_c) {
            case 1:
                $impstatus_c_nama = "IU";
                break;
            case 2:
                $impstatus_c_nama = "IP";
                break;
            case 3:
                $impstatus_c_nama = "IT";
                break;
            case 4:
                $impstatus_c_nama = "AT";
                break;
            case 5:
                $impstatus_c_nama = "BULOG";
                break;
            case 6:
                $impstatus_c_nama = "PERTAMINA";
                break;
            case 7:
                $impstatus_c_nama = "DAHANA";
                break;
            case 8:
                $impstatus_c_nama = "IPTN";
                break;
        }

        $this->pdf->Ln(3);
        $this->pdf->cell(15, 4, ' 4. Status :', 0, 0, 'L', 0);
        $this->pdf->cell(34.6, 4, $impstatus_a_nama . ' ' . $impstatus_b_nama . ' ' . $impstatus_c_nama, 0, 0, 'L', 0);
        if ($this->apikd == 1) {
            $this->pdf->cell(20, 4, '5. APIU :', 0, 0, 'L', 0);
        } else if ($this->apikd == 2) {
            $this->pdf->cell(20, 4, '5. APIP :', 0, 0, 'L', 0);
        } else {
            $this->pdf->cell(20, 4, '5.  :', 0, 0, 'L', 0);
        }
        $this->pdf->cell(30, 4, $this->apino, 0, 0, 'L', 0);
        $this->pdf->Rect(5.4, 69, 99.6, 0, 3.5, 'F');
        $this->pdf->Ln();
        $this->pdf->cell(99.6, 4, 'PEMILIK BARANG', 0, 0, 'L', 0);

        $this->pdf->cell(20, 4, '17. BL/AWB', 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(50, 4, $this->blawbno[1], 0, 0, 'L', 0);
        $this->pdf->cell(24, 4, 'Tgl. ' . $this->blawbtgl[1], 0, 0, 'L', 0);
        $this->pdf->cell(3, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, '', 0, 0, 'C', 0);
        $this->pdf->cell(56, 4, $this->blawbno[2], 0, 0, 'L', 0);
        $this->pdf->cell(24, 4, $this->blawbtgl[2], 0, 0, 'L', 0);

        $this->pdf->Ln();
        $this->pdf->cell(20, 4, ' 2a. Identitas : ', 0, 0, 'L', 0);
        $this->pdf->cell(35, 4, $this->indid_nama . ' / ' . $this->indid, 0, 0, 'L', 0);
        $imppjz = strlen(trim($this->indnpwp));
        if ($imppjz == 15) {
            $npwppemilik = $this->formatNPWP15($this->indnpwp);
        } else {
            $npwppemilik = $this->formatNPWP12($this->indnpwp);
        }
        $npwpindknya = "";
        $namaindknya = $this->indnama;

        $npwpnyaitu = $npwppemilik . $npwpindknya;
        $namanyaitu = $namaindknya;

        if (!empty($imppjz)) {
            $this->pdf->cell(69.6, 4, $npwpnyaitu, 0, 0, 'L', 0);
        } else {
            $this->pdf->cell(69.6, 4, '(Npwp Importir)', 0, 0, 'L', 0);
        }
        if (trim($this->blawbno[2] != "") || trim($this->blawbtgl[2] != "")) {
            //$this->pdf->cell(20,4,'',0,0,'L',0);
            //$this->pdf->cell(5,4,'',0,0,'C',0);
            $this->pdf->cell(50, 4, $this->blawbno[2], 0, 0, 'L', 0);
            $this->pdf->cell(24, 4, 'Tgl. ' . $this->blawbtgl[2], 0, 0, 'L', 0);
        }

        $this->pdf->Ln();
        $this->pdf->cell(25, 4, ' 3a. Nama, Alamat', 0, 0, 'L', 0);
        $this->pdf->cell(69.6, 4, ': ' . $namanyaitu, 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(3, 4, '', 0, 0, 'L', 0);
        if (!empty($imppjz)) {
            $this->pdf->cell(96.6, 4, substr($this->indalmt, 0, 54), 0, 0, 'L', 0);
        } else {
            $this->pdf->cell(96.6, 4, '(Alamat Pemilik Barang)', 0, 0, 'L', 0);
        }
        if ($this->doktupkd == "1") {
            $tglDoktub = $this->doktuptg;
            $kodeDokTub = "BC1.1";
        } else if ($this->doktupkd == "2") {
            $kodeDokTub = "BC1.2";
        } else if ($this->doktupkd == "3") {
            $kodeDokTub = "BC1.3";
        } else if ($this->doktupkd == "4") {
            $kodeDokTub = "Lainnya";
        } else {
            $tglDoktub = "";
            $kodeDokTub = "BC1.1";
        }
        $this->pdf->cell(20, 4, '18. ' . $kodeDokTub, 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(13, 4, $this->doktupno, 0, 0, 'L', 0);
        $this->pdf->cell(7.5, 4, 'Pos :', 0, 0, 'L', 0);
        $this->pdf->cell(8, 4, $this->posno, 0, 0, 'L', 0);
        $this->pdf->cell(7.5, 4, 'Sub :', 0, 0, 'L', 0);
        if (strlen($this->possub) == 0 or empty($this->possub) or $this->possub == " " or $this->possub == "") {
            $this->possub = "00000000";
        }
        $this->pdf->cell(14.5, 4, substr($this->possub, 0, 4) . '.' . substr($this->possub, 4), 0, 0, 'L', 0);
        $this->pdf->cell(30, 4, 'Tgl. ' . $tglDoktub, 0, 0, 'L', 0);

        if (trim($this->blawbno[2] != "") || trim($this->blawbtgl[2] != "")) {
            $this->pdf->cell(62, 4, '', 0, 0, 'C', 0);
            $this->pdf->cell(60, 4, '', 0, 0, 'C', 0);
            $this->pdf->cell(50, 4, $this->blawbno[2], 0, 0, 'L', 0);
            $this->pdf->cell(24, 4, 'Tgl. ' . $this->blawbtgl[2], 0, 0, 'L', 0);
        }
        //PPJK
        $this->pdf->Rect(5.4, 85.4, 99.6, 20, 3.5, 'F');
        $this->pdf->Rect(105, 85.4, 99.6, 20, 3.5, 'F');
        $this->pdf->Ln();
        $this->pdf->SetX(5.4);
        $this->pdf->cell(99, 4, 'PPJK', 0, 0, 'L', 0);
        $this->pdf->cell(80.2, 4, '19. Pemenuhan Persyaratan/Fasilitas Impor:', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, $this->kdfas, 1, 0, 'C', 0);

        $ppjkpj = strlen(trim($this->ppjknpwp));
        if ($ppjkpj == 15) {
            $formatppjkNPWP = substr($this->ppjknpwp, 0, 2) . "." . substr($this->ppjknpwp, 2, 3) . "." . substr($this->ppjknpwp, 5, 3) . "." . substr($this->ppjknpwp, 8, 1) . "-" . substr($this->ppjknpwp, 9, 3) . "." . substr($this->ppjknpwp, 12, 3);
        } else {
            $formatppjkNPWP = substr($this->ppjknpwp, 0, 2) . "." . substr($this->ppjknpwp, 2, 3) . "." . substr($this->ppjknpwp, 5, 3) . "." . substr($this->ppjknpwp, 8, 1) . "-" . substr($this->ppjknpwp, 9, 3);
        }

        $this->pdf->Ln();
        $this->pdf->cell(25, 4, ' 6. NPWP', 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, ':', 0, 0, 'C', 0);
        if ($ppjkpj == 15) {
            $this->pdf->cell(69.6, 4, $formatppjkNPWP, 0, 0, 'L', 0);
        } else {
            $this->pdf->cell(69.6, 4, '', 0, 0, 'L', 0);
        }
        $this->pdf->cell(3, 4, '', 0, 0, 'C', 0);
        $this->pdf->cell(96.6, 4, $this->gettbltabel($this->kdfas, '75'), 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(25, 4, ' 7. Nama, Alamat', 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(64.6, 4, $this->ppjknama, 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(3, 4, '', 0, 0, 'L', 0);
        $pjkalmt = $this->trimstr($this->ppjkalmt, 50);
        $this->pdf->cell(96.6, 4, $pjkalmt['str1'], 0, 0, 'L', 0);
        $this->pdf->SetX(108.4);
        if (trim($this->kdfas) <> "") {
            $this->pdf->cell(96.4, 4, $this->skepnama, 0, 0, 'L', 0);
        } else {
            $this->pdf->cell(96.4, 4, "", 0, 0, 'L', 0);
        }
        $this->pdf->Ln();
        $this->pdf->SetX(5.4);
        //$this->pdf->cell(25,4,'8. No. & Surat Izin :',0,0,'L',0);
        //$this->pdf->cell(34.6,4,' ',0,0,'C',0);
        //$this->pdf->cell(20,4,$this->ppjkno,1,0,'C',0);
        //$this->pdf->cell(20,4,$this->ppjktg,1,0,'R',0);
        $this->pdf->cell(25, 4, ' 8. No. & Tgl Surat Izin :', 0, 0, 'L', 0);
        $this->pdf->cell(34.6, 4, ' ', 0, 0, 'C', 0);
        $this->pdf->cell(20, 4, $this->ppjkno, 1, 0, 'C', 0);
        $this->pdf->cell(20, 4, $this->ppjktg, 1, 0, 'L', 0);
        if (trim($this->kdfas) == '') {
            if ($this->skepkd == '911') {
                $this->pdf->cell(30, 4, 'Nomor/Tanggal', 0, 0, 'C', 0);
                $this->pdf->cell(30.6, 4, $this->skepno, 0, 0, 'L', 0);
                $this->pdf->cell(30, 4, 'Tgl. ' . $this->skeptgl, 0, 0, 'C', 0);
            } else {
                $this->pdf->cell(30, 4, '', 0, 0, 'C', 0);
                $this->pdf->cell(30.6, 4, '', 0, 0, 'L', 0);
                $this->pdf->cell(30, 4, '', 0, 0, 'C', 0);
            }
        } else {
            $this->pdf->cell(3.6, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, $this->skepno, 0, 0, 'L', 0);
            $this->pdf->cell(45.6, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(30, 4, 'Tgl. ' . $this->skeptgl, 0, 0, 'L', 0);
        }
        //MODA
        $this->pdf->Rect(5.4, 105.4, 99.6, 8, 3.5, 'F');
        $this->pdf->Rect(105, 105.4, 99.6, 8, 3.5, 'F');
        $this->pdf->Ln();
        $this->pdf->SetX(5.4);
        $this->pdf->cell(79.6, 4, ' 9. Cara Pengangkutan', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, $this->moda, 1, 0, 'C', 0);
        $this->pdf->cell(79.6, 4, '20. Tempat Penimbunan:', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, $this->tmptbn, 1, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(35, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(69.2, 4, $this->moda_nama, 0, 0, 'L', 0);
        $this->pdf->cell(99.2, 4, ' ' . $this->tmptbn_nama, 0, 0, 'L', 0);

        //Nama Sarana
        $this->pdf->Rect(5.4, 113.4, 99.6, 8, 3.5, 'F');
        $this->pdf->Rect(105, 113.4, 49.8, 8, 3.5, 'F');
        $this->pdf->Rect(154.8, 113.4, 49.8, 8, 3.5, 'F');
        $this->pdf->Ln();
        $this->pdf->SetX(5.4);
        $this->pdf->cell(79.6, 4, '10. Nama Sarana Pengangkut & No. Voy/Flight dan Bendera:', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, $this->angkutfl, 1, 0, 'C', 0);
        $this->pdf->cell(29.8, 4, '21. Valuta :', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, $this->kdval, 1, 0, 'C', 0);
        $this->pdf->cell(49.8, 4, '22. NDPBM:', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(5, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(46.6, 4, $this->angkutnama . '  ' . $this->angkutno, 0, 0, 'L', 0);
        //$this->pdf->cell(20,4,$this->angkutno,0,0,'L',0);
        $this->pdf->cell(18, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(30, 4, $this->angkut_negara, 0, 0, 'L', 0);

        $this->pdf->cell(49.8, 4, $this->valuta, 0, 0, 'R', 0);
        /* 		if($this->kdval == "USD"){
          $this->pdf->cell(49.8,4,'US Dollar',0,0,'R',0);
          }elseif($this->kdval == "EUR"){
          $this->pdf->cell(49.8,4,'EURO',0,0,'R',0);
          }elseif($this->kdval == "IDR"){
          $this->pdf->cell(49.8,4,'Indonesian Rupiah',0,0,'R',0);
          } */
        $ndpbm = number_format($this->ndpbm, 4, '.', ',');
        $this->pdf->cell(50.5, 4, $ndpbm, 0, 0, 'R', 0);

        //Tanggal Tiba
        $this->pdf->Rect(5.4, 121.4, 99.6, 4, 3.5, 'F');
        $this->pdf->Rect(105, 121.4, 99.6, 4, 3.5, 'F');
        $this->pdf->Ln();
        $this->pdf->SetX(5.4);
        $this->pdf->cell(35, 4, '11. Perkiraan Tgl Tiba :', 0, 0, 'L', 0);
        $this->pdf->cell(64.6, 4, $this->tgtiba, 0, 0, 'L', 0);
        if ($this->kdhrg == "1") {
            $FOB = "FOB";
        } else if ($this->kdhrg == "2") {
            $FOB = "CNF";
        } else if ($this->kdhrg == "3") {
            $FOB = "FOB";
        } else {
            $FOB = "FOB";
        }
        $this->pdf->cell(25, 4, '23. ' . $FOB . ' :', 0, 0, 'L', 0);

        if ($this->fob == 0) {
            $fob = "-  ";
        } else {
            $fob = number_format($this->fob, 4, '.', ',');
        }
        $this->pdf->cell(74.9, 4, $fob, 0, 0, 'R', 0);

        //Pelabuhan
        $this->pdf->Rect(5.4, 125.4, 99.6, 12, 3.5, 'F');
        $this->pdf->Rect(105, 125.4, 49.8, 12, 3.5, 'F');
        $this->pdf->Rect(154.8, 125.4, 49.8, 12, 3.5, 'F');
        $this->pdf->Ln();
        $this->pdf->SetX(5.4);
        $this->pdf->cell(30, 4, '12. Pelabuhan Muat', 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(44.6, 4, substr($this->pelmuat_nama, 0, 22), 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, $this->pelmuat, 1, 0, 'C', 0);
        $this->pdf->cell(49.8, 4, '24. Freight :', 0, 0, 'L', 0);
        $this->pdf->cell(49.8, 4, '26. Nilai CIF :', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetX(5.4);

        $this->pdf->cell(30, 4, '13. Pelabuhan Transit', 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(44.6, 4, substr($this->peltransit_nama, 0, 22), 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, $this->peltransit, 1, 0, 'C', 0);
        if ($this->freight == 0) {
            $freight = "-  ";
        } else {
            $freight = number_format($this->freight, 2, '.', ',');
        }
        $this->pdf->cell(49.8, 4, $freight, 0, 0, 'R', 0);

        if ($this->cif == 0) {
            $cif = "-  ";
        } else {
            $cif = number_format($this->cif, 2, '.', ',');
        }
        $this->pdf->cell(49.8, 4, $cif, 0, 0, 'R', 0);

        $this->pdf->Ln();
        $this->pdf->SetX(5.4);
        $this->pdf->cell(30, 4, '14. Pelabuhan Bongkar', 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(44.6, 4, substr($this->pelbkr_nama, 0, 22), 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, $this->pelbkr, 1, 0, 'C', 0);
        /* if ($this->asuransi > 0) {
          $this->pdf->cell(29.8,4,'25. Asuransi LN/DN',0,0,'L',0);
          $this->pdf->setx(127);
          $this->pdf->cell(2,4,'==',0,0,'L',0);
          } else {
          $this->pdf->cell(29.8,4,'25. Asuransi LN/DN',0,0,'L',0);
          $this->pdf->setx(122);
          $this->pdf->cell(13,4,'==',0,0,'L',0);
          } */
        if ($this->kdhrg == "1") {
            $this->pdf->cell(29.8, 4, '25. Asuransi LN/DN', 0, 0, 'L', 0);
            $this->pdf->setx(127);
            $this->pdf->cell(2, 4, ' ', 0, 0, 'L', 0);
        } elseif ($this->kdhrg == "2" or $this->kdhrg == "3") {
            if ($this->asuransi > 0) {
                $this->pdf->cell(29.8, 4, ' 25. Asuransi LN/DN :', 0, 0, 'L', 0);
                $this->pdf->setx(127);
                $this->pdf->cell(2, 4, '==', 0, 0, 'L', 0);
            } else {
                $this->pdf->cell(29.8, 4, ' 25. Asuransi LN/DN :', 0, 0, 'L', 0);
                $this->pdf->setx(123);
                $this->pdf->cell(7, 4, '==', 0, 0, 'L', 0);
            }
        }
        if ($this->asuransi == 0) {
            $asuransi = "-  ";
        } else {
            $asuransi = number_format($this->asuransi, 2, '.', ',');
        }



        $this->pdf->cell(25.8, 4, $asuransi, 0, 0, 'R', 0);
        $this->pdf->cell(29.8, 4, 'Rp.', 0, 0, 'L', 0);
        $cifrp = floor($this->cif * $this->ndpbm);
        $this->pdf->cell(20, 4, number_format($cifrp, 2, '.', ','), 0, 0, 'R', 0);

        //Merk n No Peti Kemas
        $this->pdf->Rect(5.4, 137.4, 169.4, 24, 3.5, 'F');
        $this->pdf->Rect(174.8, 137.4, 29.8, 24, 3.5, 'F');
        $this->pdf->Ln();
        $this->pdf->SetX(5.4);
        $this->pdf->cell(99.6, 4, '27. Merek dan nomor kemasan/peti kemas:', 0, 0, 'L', 0);
        $this->pdf->cell(49.8, 4, '28. Jumlah dan Jenis kemasan:', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '', 1, 0, 'C', 0);
        $this->pdf->cell(29.8, 4, '29. Berat Kotor (kg) :', 0, 0, 'L', 0);
        $this->pdf->Ln();
        for ($i = 1; $i <= 8; $i++) {
            if ($this->conttipe[$i]) {
                $conttipe_nama[$i] = $this->conttipe[$i] . "CL";
            }
        }
        for ($i = 1; $i <= 8; $i++) {
            if ($this->contukur[$i]) {
                $contukur_nama[$i] = $this->contukur[$i] . " Feet";
            }
        }

        if ($this->jmcont > 8) {

            $this->pdf->cell(49.8, 4, '', 0, 0, 'L', 0); // Apa ini, array??
            $this->pdf->cell(49.8, 4, '', 0, 0, 'L', 0); // Apa ini, array??

            $bruto = number_format($this->bruto, 4, '.', ',');

            $this->pdf->cell(99.5, 4, $bruto, 0, 0, 'R', 0);
            $this->pdf->Ln();
            $this->pdf->cell(5, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(49.8, 4, '=== ' . $this->jmcont . ' Kontainer ===', 0, 0, 'L', 0); // Apa ini, array??
            $this->pdf->cell(49.8, 4, '', 0, 0, 'L', 0); // Apa ini, array??
            $this->pdf->cell(69.8, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, '30. Berat Bersih (kg) :', 0, 0, 'R', 0);
            $this->pdf->Ln();
            $this->pdf->cell(5, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(49.8, 4, '=== Lihat Lembar Lampiran ===', 0, 0, 'L', 0); // Apa ini, array??
            $this->pdf->cell(49.8, 4, '', 0, 0, 'L', 0); // Apa ini, array??
            $this->pdf->cell(69.8, 4, '', 0, 0, 'L', 0);
            $netto = number_format($this->netto, 4, '.', ',');
            $this->pdf->cell(25, 4, $netto, 0, 0, 'R', 0);
            $this->pdf->Ln();
            $this->pdf->cell(49.8, 4, '', 0, 0, 'L', 0); // Apa ini, array??
            $this->pdf->cell(49.8, 4, '', 0, 0, 'L', 0); // Apa ini, array??
            $this->pdf->cell(69.8, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(29.8, 4, '', 0, 0, 'R', 0);
        } else {
            $this->pdf->cell(5, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(49.8, 4, $this->setnocont($this->contno[1]) . ' ' . $contukur_nama[1] . ' ' . $conttipe_nama[1], 0, 0, 'L', 0); // Apa ini, array??
            $this->pdf->cell(49.8, 4, $this->setnocont($this->contno[5]) . ' ' . $contukur_nama[5] . ' ' . $conttipe_nama[5], 0, 0, 'L', 0); // Apa ini, array??
            //$this->pdf->cell(69.8,4,$this->jmkemas[1].' '.$this->jnkemas[1].'/'.$this->jnkemas_nama[1].' Merk: '.$this->merkkemas[1],0,0,'L',0);
            $bruto = number_format($this->bruto, 4, '.', ',');
            $this->pdf->cell(95, 4, $bruto, 0, 0, 'R', 0);
            $this->pdf->Ln();
            $this->pdf->cell(5, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(49.8, 4, $this->setnocont($this->contno[2]) . ' ' . $contukur_nama[2] . ' ' . $conttipe_nama[2], 0, 0, 'L', 0); // Apa ini, array??
            $this->pdf->cell(49.8, 4, $this->setnocont($this->contno[6]) . ' ' . $contukur_nama[6] . ' ' . $conttipe_nama[6], 0, 0, 'L', 0); // Apa ini, array??
            $this->pdf->cell(69.8, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, '30. Berat Bersih (kg) :', 0, 0, 'R', 0);
            $this->pdf->Ln();
            $this->pdf->cell(5, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(49.8, 4, $this->setnocont($this->contno[3]) . ' ' . $contukur_nama[3] . ' ' . $conttipe_nama[3], 0, 0, 'L', 0); // Apa ini, array??
            $this->pdf->cell(49.8, 4, $this->setnocont($this->contno[7]) . ' ' . $contukur_nama[7] . ' ' . $conttipe_nama[7], 0, 0, 'L', 0); // Apa ini, array??
            $this->pdf->cell(69.8, 4, '', 0, 0, 'L', 0);
            $netto = number_format($this->netto, 4, '.', ',');
            $this->pdf->cell(25, 4, $netto, 0, 0, 'R', 0);
            $this->pdf->Ln();
            $this->pdf->cell(5, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(49.8, 4, $this->setnocont($this->contno[4]) . ' ' . $contukur_nama[4] . ' ' . $conttipe_nama[4], 0, 0, 'L', 0); // Apa ini, array??
            $this->pdf->cell(49.8, 4, $this->setnocont($this->contno[8]) . ' ' . $contukur_nama[8] . ' ' . $conttipe_nama[8], 0, 0, 'L', 0); // Apa ini, array??
            $this->pdf->cell(69.8, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(29.8, 4, '', 0, 0, 'R', 0);
        }
        //$this->pdf->Ln();
        //$this->pdf->cell(49.8,4,$this->setnocont($this->contno[5]).' '.$contukur_nama[5].' '.$conttipe_nama[5],0,0,'L',0); // Apa ini, array??
        //$this->pdf->cell(49.8,4,$this->setnocont($this->contno[10]).' '.$contukur_nama[10].' '.$conttipe_nama[10],0,0,'L',0); // Apa ini, array??
        //$this->pdf->cell(69.8,4,'',0,0,'L',0);
        //$this->pdf->cell(29.8,4,'',0,0,'R',0);
        //merk
        //$this->pdf->cell(69.8,4,$this->jmkemas[1].' '.$this->jnkemas[1].'/'.$this->jnkemas_nama[1].' Merk: '.$this->merkkemas[1],0,0,'L',0);

        $this->pdf->setxy(105, 141.5);

        $kmses = $this->jmkemas[1] . ' ' . $this->jnkemas[1] . '/' . $this->jnkemas_nama[1] . ' Merk: ' . $this->merkkemas[1];
        if (strlen($kmses) > 37) {
            $merkkemas1 = substr($kmses, 0, 37);
            $merkkemas2 = substr($kmses, 37);
        } else {
            $merkkemas1 = $kmses;
            $merkkemas2 = "";
        }

        $this->pdf->cell(39.8, 4, $merkkemas1, 0, 0, 'L', 0);

        $this->pdf->setxy(105, 144.5);
        $this->pdf->cell(49.8, 4, $merkkemas2, 0, 0, 'L', 0);


        /* $this->pdf->setxy(105,141.5);
          $this->pdf->cell(69.8,4,'',0,0,'L',0); */
        /* $this->pdf->cell(69.8,4,'Merk: '.$merkkemas1,0,0,'L',0);
          $this->pdf->setxy(105,141.5);
          $this->pdf->cell(49.8,4,$merkkemas2,0,0,'L',0); */
        //Pos Tarif
        $this->pdf->setxy(105, 157.5);
        $this->pdf->Rect(5.4, 161.4, 10, 16, 3.5, 'F');
        $this->pdf->Rect(15.4, 161.4, 75, 16, 3.5, 'F');
        $this->pdf->Rect(90.4, 161.4, 25, 16, 3.5, 'F');
        $this->pdf->Rect(115.4, 161.4, 30, 16, 3.5, 'F');
        $this->pdf->Rect(145.4, 161.4, 29.4, 16, 3.5, 'F');
        $this->pdf->Rect(174.8, 161.4, 29.8, 16, 3.5, 'F');
        $this->pdf->Rect(5.4, 177.4, 10, 28, 3.5, 'F');
        $this->pdf->Rect(15.4, 177.4, 75, 28, 3.5, 'F');
        $this->pdf->Rect(90.4, 177.4, 25, 28, 3.5, 'F');
        $this->pdf->Rect(115.4, 177.4, 30, 28, 3.5, 'F');
        $this->pdf->Rect(145.4, 177.4, 29.4, 28, 3.5, 'F');
        $this->pdf->Rect(174.8, 177.4, 29.8, 28, 3.5, 'F');
        $this->pdf->Ln();
        $this->pdf->cell(10, 4, '31.', 0, 0, 'L', 0);
        $this->pdf->cell(75, 4, '32. - Pos tarif /HS', 0, 0, 'L', 0);
        $this->pdf->cell(25, 4, '33. Negara', 0, 0, 'L', 0);
        $this->pdf->cell(30, 4, '34. Tarif & Fasilitas', 0, 0, 'L', 0);
        $this->pdf->cell(29.4, 4, '35. Jumlah &', 0, 0, 'L', 0);
        $this->pdf->cell(29.8, 4, '36. Jumlah Nilai CIF', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(10, 4, 'No.', 0, 0, 'L', 0);
        $this->pdf->cell(75, 4, '      - Uraian barang secara lengkap meliputi jenis, jumlah,', 0, 0, 'L', 0);
        $this->pdf->cell(25, 4, '      Asal', 0, 0, 'L', 0);
        $this->pdf->cell(30, 4, ' - BM -PPN - PPnBM', 0, 0, 'L', 0);
        $this->pdf->cell(29.4, 4, '    Jenis Satuan,', 0, 0, 'L', 0);
        $this->pdf->cell(29.8, 4, '', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(10, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(75, 4, '      merk, tipe, ukuran, dan spesifikasi lainnya', 0, 0, 'L', 0);
        $this->pdf->cell(25, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(30, 4, ' - Cukai       - PPh', 0, 0, 'L', 0);
        $this->pdf->cell(29.4, 4, '    Berat Bersih (kg)', 0, 0, 'L', 0);
        $this->pdf->cell(29.8, 4, '', 0, 0, 'L', 0);
        //------------------------------- Baru Tambahan ---------------------------------------//
        $this->pdf->Ln();
        $this->pdf->cell(10, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(75, 4, '      - Jenis Fasilitas', 0, 0, 'L', 0);
        $this->pdf->cell(25, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(30, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(29.4, 4, ' - Jml/Jns Kemasan', 0, 0, 'L', 0);
        $this->pdf->cell(29.8, 4, '', 0, 0, 'L', 0);
        //-------------------------------------------------------------------------------------------//
        $this->pdf->SetFont('times', '', '9');
        //Isi Detail

        if ($this->jmbrg <= 1) {
            $this->pdf->Ln();
            $this->pdf->cell(10, 4, '1', 0, 0, 'L', 0);
            $this->pdf->cell(75, 4, $this->formaths($this->nohs), 0, 0, 'L', 0);

            $asal = $this->brgasal . '/' . $this->brgasal_nama;
            //$asal = "Percobaan apakah betul dengan saya";	
            if (strlen($asal) > 16) {
                $asal1 = substr($asal, 0, 16);
                $asal2 = substr($asal, 16);
            } else {
                $asal1 = $asal;
                $asal2 = "";
            }
            $this->pdf->cell(25, 4, ' ' . $asal1, 0, 0, 'L', 0);
            if ($this->fasbm != 0) {
                $BMNYA = $this->fasbm / 100;
                $strfasbm = $this->getfas($this->kdfasbm) . ' : ' . $BMNYA . ' %';
            }

            $this->pdf->cell(30, 4, ' BM:' . $this->strip($this->trpbm) . ' ' . $strfasbm, 0, 0, 'L', 0);

            $this->pdf->cell(29.4, 4, ' ' . number_format($this->jmlsat, 4, '.', ','), 0, 0, 'L', 0);
            $dnilinv = number_format($this->cif, 4, '.', ',');
            $this->pdf->cell(29.8, 4, $dnilinv, 0, 0, 'R', 0);

            $urai = $this->trimstr($this->brgurai, 35);
            $this->pdf->Ln();
            $this->pdf->cell(10, 4, '', 0, 0, 'L', 0);
            $kordinatx = $this->pdf->getx();
            $kordinaty = $this->pdf->gety();
            $this->pdf->multicell(65, 3.5, $this->brgurai, 0, 'L');

            $kordinatyy = $this->pdf->gety();
            $this->pdf->setxy($kordinatx + 75, $kordinaty);
            //$this->pdf->MultiCell(75,4,$this->brgurai);
            //$this->pdf->SetY(150);
            $this->pdf->cell(25, 4, ' ' . $asal2, 0, 0, 'L', 0);

            //$this->pdf->cell(25,4,'',0,0,'L',0);

            if ($this->fascuk != 0) {
                $CUKNYA = $this->fascuk / 100;
                $strfascuk = $this->getfas($this->kdfascuk) . ' : ' . $CUKNYA . ' %';
            }

            $kordinatx = $this->pdf->getx();
            $this->pdf->cell(30, 4, ' Cukai:' . $this->strip($this->trpcuk) . ' ' . $strfascuk, 0, 0, 'L', 0);
            $satuan = $this->trimstr($this->kdsat . '/' . $this->kdsat_nama, 20);
            $this->pdf->cell(29.4, 4, ' ' . $satuan['str1'], 0, 0, 'L', 0);
            $this->pdf->cell(29.8, 4, '', 0, 0, 'L', 0);
            $this->pdf->Ln();
            $this->pdf->setx($kordinatx);
            if ($this->fasppn != 0) {
                $BBSNYA = $this->fasppn / 100;
                $strfasppn = $this->getfas($this->kdfasppn) . ' : ' . $BBSNYA . ' %';
            }
            $this->pdf->cell(30, 4, ' PPN:' . intval($this->strip($this->trpppn)) . '%' . $strfasppn . '', 0, 0, 'L', 0);
            if (trim($satuan['str2']) <> "") {
                $this->pdf->cell(29.4, 4, " " . $satuan['str2'], 0, 0, 'L', 0);
            } else {
                $this->pdf->cell(29.4, 4, ' BB: ' . number_format($this->nettodtl, 4, '.', ',') . ' Kg', 0, 0, 'L', 0);
            }
            $this->pdf->cell(29.8, 4, '', 0, 0, 'L', 0);
            $this->pdf->Ln();
            $this->pdf->setx($kordinatx);
            if ($this->faspbm != 0) {
                $PBMNYA = $this->faspbm / 100;
                $strfaspbm = $this->getfas($this->kdfaspbm) . ' : ' . $PBMNYA . ' %';
            }

            $this->pdf->cell(30, 4, ' PPnBM:' . $this->strip($this->trppbm) . ' ' . $strfaspbm, 0, 0, 'L', 0);
            if (trim($satuan['str2']) <> "") {
                $this->pdf->cell(29.4, 4, ' BB: ' . number_format($this->nettodtl, 4, '.', ',') . ' Kg', 0, 0, 'L', 0);
            }
            $nsl = $this->kemasjn . ' / ' . $this->kemasjn_nama;
            //$asal = "Percobaan apakah betul dengan saya";	
            if (strlen($nsl) > 16) {
                $nsl1 = substr($nsl, 0, 14);
                $nsl2 = substr($nsl, 14);
            } else {
                $nsl1 = $nsl;
                $nsl2 = "";
            }
            $jn = number_format($this->kemasjm, 4, '.', ',');
            if (trim($satuan['str2']) == "") {
                $this->pdf->cell(29.4, 4, ' ' . $jn . ' ' . $nsl1, 0, 0, 'L', 0);
            }
            $this->pdf->cell(29.8, 4, '', 0, 0, 'L', 0);
            $this->pdf->Ln();
            $this->pdf->setx($kordinatx);

            if ($this->apino != ' ') {
                $trppph = '2.5';
            } else {
                $trppph = '7.5';
            }

            if ($this->faspph != 0) {
                $PPHNYA = $this->faspph / 100;
                $strfaspph = $this->getfas($this->kdfaspph) . ' : ' . $PPHNYA . ' %';
            }

            $this->pdf->cell(30, 4, ' PPh:' . $this->strip($trppph) . ' ' . $strfaspph, 0, 0, 'L', 0);
            if (trim($satuan['str2']) <> "") {
                $this->pdf->cell(29.4, 4, " " . $jn . ' ' . $nsl1, 0, 0, 'L', 0);
            } else {
                $this->pdf->cell(29.4, 4, ' ' . $nsl2, 0, 0, 'L', 0);
            }
            /* $this->pdf->Ln();
              $this->pdf->cell(10,4,'',0,0,'L',0);
              $this->pdf->cell(75,4,$this->merk.'-'.$this->tipe.'-'.$this->spflain,0,0,'L',0);
              $this->pdf->cell(25,4,'',0,0,'L',0); */

            $this->pdf->Ln();
            $this->pdf->cell(10, 4, '', 0, 0, 'L', 0);
            if (trim($this->kdfasdtl) <> "") {
                $this->pdf->cell(75, 4, 'Fas: ' . $this->kdfasdtl . '/' . $this->kdfasdtl_nama, 0, 0, 'L', 0);
            } else {
                $this->pdf->cell(75, 4, '' . $this->kdfasdtl . '/' . $this->kdfasdtl_nama, 0, 0, 'L', 0);
            }
            $this->pdf->cell(54, 4, '', 0, 0, 'L', 0);
            //$this->pdf->cell(29.4,4,' '.$nsl2,0,0,'L',0);
            $this->pdf->sety($kordinatyy);
            $this->pdf->cell(10, 4, '', 0, 0, 'L', 0);
            //$this->pdf->cell(75,4,$jn.' '.$this->kemasjn.' / '.$this->kemasjn_nama,0,0,'L',0);
            $this->pdf->cell(25, 4, '', 0, 0, 'L', 0);

            //$this->pdf->Ln();
            //$this->pdf->cell(10,4,'',0,0,'L',0);
            /* if(trim($this->kdfasdtl) <> ""){
              $this->pdf->cell(75,4,'Fas: '.$this->kdfasdtl.'/'.$this->kdfasdtl_nama,0,0,'L',0);
              }else{
              $this->pdf->cell(75,4,''.$this->kdfasdtl.'/'.$this->kdfasdtl_nama,0,0,'L',0);
              } */
            $this->pdf->cell(25, 4, '', 0, 0, 'L', 0);
            $this->kdfasdtl_nama = "";
            $this->pdf->cell(29.4, 4, '', 0, 0, 'L', 0);
            //$this->pdf->cell(29.8,4,'jono',0,0,'L',0);
            //$this->pdf->cell(29.4,4,' '.$jn.' '.$this->kemasjn.' / '.$this->kemasjn_nama,0,0,'L',0);
            $this->pdf->Ln();
            $this->pdf->cell(10, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(75, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(30, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(29.4, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(29.8, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(10, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(75, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(30, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(29.4, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(29.8, 4, '', 0, 0, 'L', 0);
            $this->pdf->Ln();
            $this->pdf->cell(10, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(75, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(30, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(29.4, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(29.8, 4, '', 0, 0, 'L', 0);
        } else {
            $this->pdf->Ln();
            $this->pdf->cell(10, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(75, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(30, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(29.4, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(29.8, 4, '', 0, 0, 'L', 0);
            $this->pdf->Ln();
            $this->pdf->cell(10, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(75, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(30, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(29.4, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(29.8, 4, '', 0, 0, 'L', 0);
            $this->pdf->Ln();
            $this->pdf->cell(10, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(75, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(30, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(29.4, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(29.8, 4, '', 0, 0, 'L', 0);
            $this->pdf->Ln();
            $this->pdf->cell(10, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(75, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(30, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(29.4, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(29.8, 4, '', 0, 0, 'L', 0);
            $this->pdf->Ln();
            $this->pdf->cell(140, 4, '==' . $this->jmbrg . ' Jenis barang. Lihat Lembar Lanjutan==', 0, 0, 'R', 0);
            $this->pdf->cell(29.4, 4, '', 0, 0, 'L', 0);
//			$this->pdf->cell(29.8,4,'',0,0,'L',0);
            $this->pdf->cell(30.5, 4, number_format($this->cif, 4, '.', ','), 0, 0, 'R', 0);
            $this->pdf->Ln();
            $this->pdf->cell(10, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(75, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(30, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(29.4, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(29.8, 4, '', 0, 0, 'L', 0);
            $this->pdf->Ln();
            $this->pdf->cell(10, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(75, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(30, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(29.4, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(29.8, 4, '', 0, 0, 'L', 0);
            $this->pdf->Ln();
            $this->pdf->cell(10, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(75, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(25, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(30, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(29.4, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(29.8, 4, '', 0, 0, 'L', 0);
        }
        //Pungutan

        $this->pdf->sety(201.41513888889);
        $this->pdf->Rect(5.4, 205.4, 39.2, 4, 3.5, 'F');
        $this->pdf->Rect(44.6, 205.4, 40, 4, 3.5, 'F');
        $this->pdf->Rect(84.6, 205.4, 40, 4, 3.5, 'F');
        $this->pdf->Rect(124.6, 205.4, 40.2, 4, 3.5, 'F');
        $this->pdf->Rect(164.8, 205.4, 39.85, 4, 3.5, 'F');

        $this->pdf->SetFont('times', '', '9');
        $this->pdf->Ln();
        $this->pdf->cell(39.2, 4, 'Jenis Pungutan', 0, 0, 'C', 0);
        $this->pdf->cell(40, 4, 'Dibayar (Rp)', 0, 0, 'C', 0);
        $this->pdf->cell(40, 4, 'Ditanggung pemerintah (Rp)', 0, 0, 'C', 0);
        $this->pdf->cell(40, 4, 'Ditangguhkan (Rp)', 0, 0, 'C', 0);
        $this->pdf->cell(40, 4, 'Dibebaskan (Rp)', 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->setx(5.4);
        $this->pdf->cell(5, 4, '37.', 1, 0, 'L', 0);
        $this->pdf->cell(34.2, 4, 'BM', 1, 0, 'L', 0);

        $JmlBM = floor($this->nPungutan["CUD"][0] + $this->nPungutan["CUD"][3]);
        //echo $JmlBM; exit;
        $this->pdf->cell(40, 4, number_format(floor($this->nPungutan["CUD"][0] + $this->nPungutan["LXT"][3]), 0, '.', ','), 1, 0, 'R', 0);
        $this->pdf->cell(40, 4, number_format($this->nPungutan["CUD"][1], 0, '.', ','), 1, 0, 'R', 0);
        $this->pdf->cell(40, 4, number_format($this->nPungutan["CUD"][2], 0, '.', ','), 1, 0, 'R', 0);
        $this->pdf->cell(40, 4, number_format($this->nPungutan["CUD"][4], 0, '.', ','), 1, 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->setx(5.4);
        $this->pdf->cell(5, 4, '38.', 1, 0, 'L', 0);
        $this->pdf->cell(34.2, 4, 'Cukai', 1, 0, 'L', 0);
        $JmlCukai = floor($this->nPungutan["VAC"][0] + $this->nPungutan["VAC"][3]);
        $this->pdf->cell(40, 4, number_format(floor($this->nPungutan["VAC"][0] + $this->nPungutan["VAC"][3]), 0, '.', ','), 1, 0, 'R', 0);
        $this->pdf->cell(40, 4, number_format($this->nPungutan["VAC"][1], 0, '.', ','), 1, 0, 'R', 0);
        $this->pdf->cell(40, 4, number_format($this->nPungutan["VAC"][2], 0, '.', ','), 1, 0, 'R', 0);
        $this->pdf->cell(40, 4, number_format($this->nPungutan["VAC"][4], 0, '.', ','), 1, 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->setx(5.4);
        $this->pdf->cell(5, 4, '39.', 1, 0, 'L', 0);
        $this->pdf->cell(34.2, 4, 'PPN', 1, 0, 'L', 0);
        $JmlPPN = floor($this->nPungutan["VAT"][0] + $this->nPungutan["VAT"][3]);
        $this->pdf->cell(40, 4, number_format(floor($this->nPungutan["VAT"][0] + $this->nPungutan["VAT"][3]), 0, '.', ','), 1, 0, 'R', 0);
        $this->pdf->cell(40, 4, number_format($this->nPungutan["VAT"][1], 0, '.', ','), 1, 0, 'R', 0);
        $this->pdf->cell(40, 4, number_format($this->nPungutan["VAT"][2], 0, '.', ','), 1, 0, 'R', 0);
        $this->pdf->cell(40, 4, number_format($this->nPungutan["VAT"][4], 0, '.', ','), 1, 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->setx(5.4);
        $this->pdf->cell(5, 5, '40.', 1, 0, 'L', 0);
        $this->pdf->cell(34.2, 5, 'PPnBM', 1, 0, 'L', 0);
        $JmlPPnBM = floor($this->nPungutan["LXT"][0] + $this->nPungutan["LXT"][3]);
        $this->pdf->cell(40, 5, number_format(floor($this->nPungutan["LXT"][0] + $this->nPungutan["LXT"][3]), 0, '.', ','), 1, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->nPungutan["LXT"][1], 0, '.', ','), 1, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->nPungutan["LXT"][2], 0, '.', ','), 1, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->nPungutan["LXT"][4], 0, '.', ','), 1, 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->setx(5.4);
        $this->pdf->cell(5, 5, '41.', 1, 0, 'L', 0);
        $this->pdf->cell(34.2, 5, 'PPh', 1, 0, 'L', 0);
        $JmlPPh = floor($this->nPungutan["ICT"][0] + $this->nPungutan["ICT"][3]);
        //echo $JmlPPh; exit;
        $this->pdf->cell(40, 5, number_format(floor($this->nPungutan["ICT"][0] + $this->nPungutan["ICT"][3]), 0, '.', ','), 1, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->nPungutan["ICT"][1], 0, '.', ','), 1, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->nPungutan["ICT"][2], 0, '.', ','), 1, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($this->nPungutan["ICT"][4], 0, '.', ','), 1, 0, 'R', 0);

        $nDibayar = $this->nPungutan["CUD"][0] + $this->nPungutan["CUD"][3] + $this->nPungutan["VAT"][0] + $this->nPungutan["VAT"][3] + $this->nPungutan["LXT"][0] + $this->nPungutan["LXT"][3] + $this->nPungutan["ICT"][0] + $this->nPungutan["ICT"][3] + $this->nPungutan["VAC"][0] + $this->nPungutan["VAC"][3];

        $nDTP = $this->nPungutan["CUD"][1] + $this->nPungutan["VAT"][1] + $this->nPungutan["LXT"][1] + $this->nPungutan["ICT"][1] + $this->nPungutan["VAC"][1];

        $nDTG = $this->nPungutan["CUD"][2] + $this->nPungutan["VAT"][2] + $this->nPungutan["LXT"][2] + $this->nPungutan["ICT"][2] + $this->nPungutan["VAC"][2];

        $nDibebaskan = $this->nPungutan["CUD"][4] + $this->nPungutan["VAT"][4] + $this->nPungutan["LXT"][4] + $this->nPungutan["ICT"][4] + $this->nPungutan["VAC"][4];

        $this->pdf->Ln();
        $this->pdf->setx(5.4);
        $this->pdf->cell(5, 5, '42.', 1, 0, 'L', 0);
        $this->pdf->cell(34.2, 5, 'TOTAL', 1, 0, 'L', 0);
        $this->pdf->cell(40, 5, number_format($nDibayar, 0, '.', ','), 1, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($nDTP, 0, '.', ','), 1, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($nDTG, 0, '.', ','), 1, 0, 'R', 0);
        $this->pdf->cell(40, 5, number_format($nDibebaskan, 0, '.', ','), 1, 0, 'R', 0);

        // Section E - H
        $this->pdf->Rect(5.4, 236.4, 99.6, 54, 3.5, 'F');
        //$this->pdf->Rect(5.4, 267.4, 99.6, 23, 3.5, 'F');
        $this->pdf->Rect(105, 236.4, 99.6, 54, 3.5, 'F');
        $this->pdf->Ln();
        $this->pdf->cell(99.6, 5, 'E. Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal', 0, 0, 'L', 0);
        $this->pdf->cell(99.6, 5, '  G. UNTUK PEMBAYARAN/JAMINAN', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(99.6, 5, '     yang diberitahukan dalam dokumen ini', 0, 0, 'L', 0);
        //$this->pdf->setx(5.4);
        $this->pdf->setx(108);
        //$this->pdf->cell(9.6,5,'',0,0,'L',0);
        $this->pdf->cell(26, 5, 'a. Pembayaran', 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, '', 1, 0, 'C', 0);
        $this->pdf->cell(65, 5, ' 1. Bank; 2. Pos; 3. Kantor Pabean.', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(24.6, 5, '', 0, 0, 'L', 0);
        $this->pdf->setx(15);
//		$this->pdf->cell(75,5,$this->kota.', '.$this->tanggal_ttd,0,0,'C',0);
        $this->pdf->cell(75, 5, $this->kotaTTD . ', ' . $this->tglsspcp, 0, 0, 'C', 0);
        $this->pdf->cell(9.6, 5, '', 0, 0, 'L', 0);
        $this->pdf->setx(108);
        $this->pdf->cell(26, 5, 'b. Jaminan', 0, 0, 'L', 0);
        $this->pdf->cell(5, 4, '', 1, 0, 'C', 0);
        $this->pdf->cell(65, 5, ' 1. Tunai; 2. Bank Garansi; 3. Customs Bond;', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(24.6, 5, '', 0, 0, 'L', 0);

        /** try to get edinum * */
        if (trim($this->ppjknpwp) <> "") {
            $ppjkx = "PPJK";
        } else {
            $ppjkx = "Importir";
        }

        $this->pdf->setx(15);
        $this->pdf->cell(70, 5, $ppjkx, 0, 0, 'C', 0);
        $this->pdf->setx(5.4);
        $this->pdf->cell(99.6, 4, '', 0, 0, 'C', 0);
        $this->pdf->cell(34, 4, '', 0, 0, 'C', 0);
        $this->pdf->cell(65, 5, ' 4. Lainnya', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->setx(5.4);
        $this->pdf->cell(99.6, 5, '', 0, 0, 'C', 0);

        $this->pdf->cell(27, 5, '', 1, 0, 'C', 0);
        $this->pdf->cell(50, 5, 'Nomor', 1, 0, 'C', 0);
        $this->pdf->cell(22.46, 5, 'Tanggal', 1, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->setx(5.4);
        $this->pdf->cell(99.6, 5, '', 0, 0, 'C', 0);

        $this->pdf->cell(27, 5, 'Pembayaran', 1, 0, 'L', 0);
        $this->pdf->cell(50, 5, $this->nosspcp, 1, 0, 'L', 0);
        $this->pdf->cell(22.46, 5, $this->tglsspcp, 1, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->setx(5.4);
        $this->pdf->cell(99.6, 5, '', 0, 0, 'C', 0);

        $this->pdf->cell(27, 5, 'Jaminan', 1, 0, 'L', 0);
        $this->pdf->cell(50, 5, '', 1, 0, 'L', 0);
        $this->pdf->cell(22.46, 5, '', 1, 0, 'L', 0);

        reset($this->nPungutan);
        $this->nPungutan['CUD'][0] = '';
        $this->nPungutan['CUD'][1] = '';
        $this->nPungutan['CUD'][2] = '';
        $this->nPungutan['CUD'][3] = '';
        $this->nPungutan['CUD'][4] = '';

        $this->nPungutan['VAT'][0] = '';
        $this->nPungutan['VAT'][1] = '';
        $this->nPungutan['VAT'][2] = '';
        $this->nPungutan['VAT'][3] = '';
        $this->nPungutan['VAT'][4] = '';

        $this->nPungutan['LXT'][0] = '';
        $this->nPungutan['LXT'][1] = '';
        $this->nPungutan['LXT'][2] = '';
        $this->nPungutan['LXT'][3] = '';
        $this->nPungutan['LXT'][4] = '';

        $this->nPungutan['ICT'][0] = '';
        $this->nPungutan['ICT'][1] = '';
        $this->nPungutan['ICT'][2] = '';
        $this->nPungutan['ICT'][3] = '';
        $this->nPungutan['ICT'][4] = '';

        $this->nPungutan['VAC'][0] = '';
        $this->nPungutan['VAC'][1] = '';
        $this->nPungutan['VAC'][2] = '';
        $this->nPungutan['VAC'][3] = '';
        $this->nPungutan['VAC'][4] = '';

        $nDibayar = '';
        $nDTP = '';
        $nDTG = '';
        $nDibebaskan = '';
        //-------------------------

        $this->pdf->Ln();
        $this->pdf->cell(99.6, 4, '', 0, 0, 'C', 0);
        $this->pdf->cell(49.8, 4, 'Pejabat Penerima', 0, 0, 'C', 0);
        $this->pdf->cell(49.8, 4, 'Nama/Stempel Instansi', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->Ln();
        //$this->pdf->Ln();		
        $this->pdf->setx(5.4);
        $this->pdf->cell(90, 5, $this->namaTTD, 0, 0, 'C', 0);
        $this->pdf->Ln();

        $this->pdf->SetFont('times', 'I', '9');
        $this->pdf->cell(24.6, 5, 'Tgl. Cetak ' . date("d-m-Y"), 0, 0, 'L', 0);
        $this->pdf->SetFont('times', '', '9');

        $this->pdf->setx(5.4);
        $this->pdf->cell(99.6, 4, '', 0, 0, 'C', 0);
        $this->pdf->cell(49.8, 4, $this->petugasbankPIB, 0, 0, 'C', 0);
        $this->pdf->cell(49.8, 4, ' ', 0, 0, 'L', 0);
        $this->pdf->Ln(2);
        $this->pdf->Ln();
        /* $this->pdf->cell(75,5,$ppjk,0,0,'C',0);
          $this->pdf->cell(5,5,'',0,0,'L',0);
          $this->pdf->cell(20,5,'',0,0,'L',0);
          $this->pdf->cell(5,4,'',0,0,'C',0);
          $this->pdf->cell(65,5,' 4. Lainnya',0,0,'L',0);
          $this->pdf->Ln();
          $this->pdf->setx(5.4);
          $this->pdf->cell(99.6,5,'',0,0,'C',0);
          $this->pdf->cell(9.6,5,'',0,0,'L',0);
          $this->pdf->cell(15,5,'Jn. Pen',1,0,'C',0);
          $this->pdf->cell(15,5,'Kd. Pen',1,0,'C',0);
          $this->pdf->cell(44,5,'No.Tanda Pembayaran/Jaminan',1,0,'C',0);
          $this->pdf->cell(16,5,'Tgl.',1,0,'C',0);
          $this->pdf->Ln();
          $this->pdf->setx(5.4);
          $this->pdf->cell(99.6,5,'',0,0,'C',0);
          $this->pdf->cell(9.6,5,'',0,0,'L',0);
          $this->pdf->cell(15,5,'BM',1,0,'L',0);
          if (($JmlBM != "") or ($JmlBM != 0)) {
          $KDMAPBM = $this->kodeMAPlagi($JmlBM);
          }
          $this->pdf->cell(15,5,$KDMAPBM,1,0,'C',0);

          if ($this->nPungutan["CUD"][0] != 0 or $this->nPungutan["CUD"][0] != "") {
          $this->pdf->cell(44,5,$this->nosspcp,1,0,'L',0);
          $this->pdf->cell(16,5,$this->tglsspcp,1,0,'L',0);
          } else {
          $this->pdf->cell(44,5,'',1,0,'L',0);
          $this->pdf->cell(16,5,'',1,0,'L',0);
          }
          $this->pdf->Ln();
          $this->pdf->setx(5.4);
          $this->pdf->SetFont('times','I','9');
          $this->pdf->cell(24.6,5,'Tgl. Cetak '.date("d-m-Y"),0,0,'L',0);
          $this->pdf->SetFont('times','','9');
          $this->pdf->cell(75,5,$this->nama_ttd,0,0,'C',0);
          $this->pdf->cell(9.6,5,'',0,0,'L',0);
          $this->pdf->cell(15,5,'Cukai',1,0,'L',0);
          if (($JmlCukai != "") or ($JmlCukai != 0)) {
          $KDMAPCukai = $this->kodeMAPlagi($JmlCukai);
          }
          $this->pdf->cell(15,5,$KDMAPCukai,1,0,'C',0);

          if ($this->nPungutan["VAC"][0] != 0 or  $this->nPungutan["VAC"][0] != "") {
          $this->pdf->cell(44,5,$this->nosspcp,1,0,'L',0);
          $this->pdf->cell(16,5,$this->tglsspcp,1,0,'L',0);
          } else {
          $this->pdf->cell(44,5,'',1,0,'L',0);
          $this->pdf->cell(16,5,'',1,0,'L',0);
          }
          $this->pdf->Ln();
          $this->pdf->setx(5.4);

          // Section G
          $this->pdf->cell(24.6,5,'G. UNTUK PEJABAT BC',0,0,'L',0);
          $this->pdf->cell(75,5,'',0,0,'C',0);
          $this->pdf->cell(9.6,5,'',0,0,'L',0);
          $this->pdf->cell(15,5,'PPN',1,0,'L',0);
          if (($JmlPPN != "") or ($JmlPPN != 0)) {
          $KDMAPPPN = $this->kodeMAPlagi($JmlPPN);
          }
          $this->pdf->cell(15,5,$KDMAPPPN,1,0,'C',0);

          if ($this->nPungutan["VAT"][0] != 0 or  $this->nPungutan["VAT"][0] != "") {
          $this->pdf->cell(44,5,$this->nosspcp,1,0,'L',0);
          $this->pdf->cell(16,5,$this->tglsspcp,1,0,'L',0);
          } else {
          $this->pdf->cell(44,5,'',1,0,'L',0);
          $this->pdf->cell(16,5,'',1,0,'L',0);
          }
          $this->pdf->Ln();
          $this->pdf->setx(5.4);
          $this->pdf->cell(99.6,5,'',0,0,'C',0);
          $this->pdf->cell(9.6,5,'',0,0,'L',0);
          $this->pdf->cell(15,5,'PPnBM',1,0,'L',0);
          if (($JmlPPnBM != "") or ($JmlPPnBM != 0)) {
          $KDMAPPPnBM = $this->kodeMAPlagi($JmlPPnBM);
          }
          $this->pdf->cell(15,5,$KDMAPPPnBM,1,0,'C',0);

          if ($this->nPungutan["LXT"][0] != 0 or  $this->nPungutan["LXT"][0] != "") {
          $this->pdf->cell(44,5,$this->nosspcp,1,0,'L',0);
          $this->pdf->cell(16,5,$this->tglsspcp,1,0,'L',0);
          } else {
          $this->pdf->cell(44,5,'',1,0,'L',0);
          $this->pdf->cell(16,5,'',1,0,'L',0);
          }
          $this->pdf->Ln();
          $this->pdf->setx(5.4);
          $this->pdf->cell(99.6,5,'',0,0,'C',0);
          $this->pdf->cell(9.6,5,'',0,0,'L',0);
          $this->pdf->cell(15,5,'PPh',1,0,'L',0);
          if (($JmlPPh != "") or ($JmlPPh != 0)) {
          $KDMAPPPh = $this->kodeMAPlagi($JmlPPh);
          }

          $this->pdf->cell(15,5,$KDMAPPPh,1,0,'C',0);

          if ($this->nPungutan["ICT"][0] != 0 or  $this->nPungutan["ICT"][0] != "") {
          $this->pdf->cell(44,5,$this->nosspcp,1,0,'L',0);
          $this->pdf->cell(16,5,$this->tglsspcp,1,0,'L',0);
          } else {
          $this->pdf->cell(44,5,'',1,0,'L',0);
          $this->pdf->cell(16,5,'',1,0,'L',0);
          }
          $this->pdf->Ln();
          $this->pdf->cell(99.6,4,'',0,0,'C',0);
          $this->pdf->cell(49.8,4,'Pejabat Penerima',0,0,'C',0);
          $this->pdf->cell(49.8,4,'Nama/Stempel Instansi',0,0,'L',0);
          $this->pdf->Ln();
          $this->pdf->cell(99.6,4,'',0,0,'C',0);
          $this->pdf->cell(49.8,4,$this->petugasbankPIB,0,0,'C',0);
          $this->pdf->cell(49.8,4,'',0,0,'C',0);
          $this->pdf->Ln(); */
        // Footer
        //$this->pdf->cell(99.8,4,'SK.MENKEU No.:190/KMK.05/2000 Tanggal 31 Mei 2000',0,0,'L',0);
        $this->pdf->SetY(291);
        $this->pdf->cell(99.8, 4, 'Perdirjen BC No. PER-44/BC/2011 Tanggal 16 September 2011', 0, 0, 'L', 0);
        $this->pdf->cell(75, 4, 'Rangkap ke-1 / 2 / 3 untuk Kantor Pabean / BPS / BI', 0, 0, 'L', 0);
        $this->pdf->cell(24.6, 4, 'Ver.5.0.5', 0, 0, 'R', 0);
    }

    function formatNPWP15($npwp) {
        if ($npwp != "") {
            $npwpnya = substr($this->impnpwp, 0, 2) . "." . substr($this->impnpwp, 2, 3) . "." . substr($this->impnpwp, 5, 3) . "." . substr($this->impnpwp, 8, 1) . "-" . substr($this->impnpwp, 9, 3) . "." . substr($this->impnpwp, 12, 3);
        } else {
            $npwpnya = "";
        }
        return $npwpnya;
    }

    function formatNPWP12($npwp) {
        if ($npwp != "") {
            $npwpnya = substr($this->impnpwp, 0, 2) . "." . substr($this->impnpwp, 2, 3) . "." . substr($this->impnpwp, 5, 3) . "." . substr($this->impnpwp, 8, 1) . "-" . substr($this->impnpwp, 9, 3);
        } else {
            $npwpnya = "";
        }
        return $npwpnya;
    }

    function getfas($kodefas) {
        switch ($kodefas) {
            case 1:
                $kdfaspbm_nama = 'DTP';
                break;
            case 2:
                $kdfaspbm_nama = 'DTG';
                break;
            case 3:
                $kdfaspbm_nama = 'BKL';
                break;
            case 4:
                $kdfaspbm_nama = 'BBS';
                break;
        }
        return $kdfaspbm_nama;
    }

    function kodeMAPlagi($value) {
        if ($this->IDsspcpNya != "") {
            $this->db->connect();
            $SQL = "SELECT KdMAP FROM TbltxSSP WHERE (IDSSPCP = '" . $this->IDsspcpNya . "') AND (TxAmt = '" . $value . "')";
            $data = $this->db->query($SQL);
            if ($data->next()) {
                $KdMAP = $data->get("KdMAP");
                return $KdMAP;
                $this->db->disconnect();
            } else {
                $KdMAP = "";
                return $KdMAP;
            }
        } else {
            $KdMAP = "";
            return $KdMAP;
        }
    }

    // Batas PIB DOKUMENT

    /** fungsi untuk menggambar SSPCP Print Out */
    function drawSSPCP($arrLembar, $arrDokumen) {
        $map = $arrDokumen["MAP"];
        //print_r($arrSSPCP); exit;
        $arrSSPCP = $arrDokumen["SSPCP"];
        $arrBea1 = $arrDokumen["BEA01"];
        $arrBea2 = $arrDokumen["BEA02"];
        $arrBea3 = $arrDokumen["BEA03"];
        $arrBea4 = $arrDokumen["BEA04"];
        $arrBea5 = $arrDokumen["BEA05"];
        $arrBea6 = $arrDokumen["BEA06"];
        $arrBea7 = $arrDokumen["BEA07"];
        $arrBea8 = $arrDokumen["BEA08"];
        $arrBea9 = $arrDokumen["BEA09"];
        $arrBea10 = $arrDokumen["BEA10"];
        $arrBea11 = $arrDokumen["BEA11"];
        $arrBea12 = $arrDokumen["BEA12"];
        $arrBea13 = $arrDokumen["BEA13"];
        $arrBea14 = $arrDokumen["BEA14"];
        $arrBea15 = $arrDokumen["BEA15"];
        $arrBea16 = $arrDokumen["BEA16"];
        $arrBea17 = $arrDokumen["BEA17"];
        $arrBea18 = $arrDokumen["BEA18"];
        $arrBea19 = $arrDokumen["BEA19"];
        $arrBea20 = $arrDokumen["BEA20"];

        $this->pdf->SetLeftMargin(5);
        $this->pdf->SetRightMargin(5);
        $this->pdf->AddPage();
        $this->pdf->Rect(5.4, 5.4, 71.4, 22, 3.5, 'F');
        $this->pdf->Rect(76.8, 5.4, 56.4, 22, 3.5, 'F');
        $this->pdf->Rect(133.2, 5.4, 71.4, 22, 3.5, 'F');

        $this->pdf->SetFont('arial', 'B', '9');
        $this->pdf->cell(1, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(65.4, 5, 'KEMENTERIAN KEUANGAN R.I.', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(1, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(65.4, 5, 'DIREKTORAT JENDRAL BEA DAN CUKAI', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('arial', '', '8');
        $this->pdf->cell(1, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(65.4, 5, 'KANTOR ' . $arrSSPCP["kpbc"], 0, 0, 'L', 0);
        $this->pdf->cell(65.4, 5, '', 0, 0, 'L', 0);

        $this->pdf->SetFont('arial', '', '8');
        $this->pdf->cell(55.4, 5, 'LEMBAR KE-          :' . ' ' . $arrLembar["lembar1"], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(50, 5, 'Kode Kantor : ' . $arrSSPCP["kodekpbc"], 1, 0, 'L', 0);

        $this->pdf->Ln();
        $this->pdf->Rect(5.4, 27.4, 71.4, 6, 3.5, 'F');
        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->Ln(1);
        $this->pdf->cell(5.4, 8.5, 'A.', 0, 0, 'C', 0);
        $this->pdf->cell(66.8, 8.5, 'JENIS PENERIMAAN NEGARA', 0, 0, 'L', 0);

        $this->pdf->Rect(76.8, 27.4, 56.4, 6, 3.5, 'F');

        $this->pdf->Ln(1);
        $this->pdf->cell(71.9, 6, '', 0, 0, 'C', 0);
        $this->pdf->cell(9, 6, '', 1, 0, 'C', 0);
        $this->pdf->cell(19, 6, 'IMPOR', 1, 0, 'C', 0);
        $this->pdf->cell(9, 6, '', 1, 0, 'C', 0);
        $this->pdf->cell(19.3, 6, 'EKSPOR', 1, 0, 'C', 0);
        $this->pdf->cell(9, 6, '', 1, 0, 'C', 0);

        $this->pdf->cell(18, 6, 'CUKAI', 1, 0, 'C', 0);
        $this->pdf->cell(9, 6, '', 1, 0, 'C', 0);
        $this->pdf->cell(35.4, 6, 'BARANG TERTENTU', 1, 0, 'L', 0);

        $this->pdf->Rect(5.4, 33.4, 71.4, 6, 3.5, 'F');
        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->Ln(1);
        $this->pdf->cell(5.4, 16.5, 'B.', 0, 0, 'C', 0);
        $this->pdf->cell(66.8, 16.5, 'JENIS IDENTITAS', 0, 0, 'L', 0);

        //$this->pdf->Rect(133.2, 27.4, 71.4, 6, 3.5, 'F');
        //$this->pdf->Rect(76.8, 33.4, 56.4, 6, 3.5, 'F');
        $this->pdf->Ln(5);
        $this->pdf->cell(71.9, 6, '', 0, 0, 'C', 0);
        $this->pdf->cell(9, 6, '', 1, 0, 'C', 0);
        $this->pdf->cell(33, 6, 'NPWP', 0, 0, 'C', 0);
        $this->pdf->cell(9, 6, '', 1, 0, 'C', 0);
        $this->pdf->cell(33, 6, 'PASPOR', 0, 0, 'C', 0);
        $this->pdf->cell(9, 6, '', 1, 0, 'C', 0);
        //$this->pdf->Rect(133.2, 27.4, 71.4, 6, 3.5, 'F');
        $this->pdf->cell(34.8, 6, 'KTP', 1, 0, 'C', 0);
        $this->pdf->Ln();
        //$this->pdf->Rect(133.2, 33.4, 71.4, 6, 3.5, 'F');
        //A. NPWP, NAMA, ALAMAT
        $npwp = $arrSSPCP["npwp"];
        for ($i = 0; $i < (strlen($npwp) + 1); $i++) {
            $awal = $i - 1;
            $n[$i] = substr($npwp, $awal, 1);
        }
        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->Rect(5.4, 39.4, 199.2, 18, 3.5, 'F');
        $this->pdf->Ln();
        $this->pdf->Ln(-6);
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(20, 5, 'NOMOR', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->SetFont('Arial', '', 9);

        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(7, 4, $n[1], 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, $n[2], 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, $n[3], 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, $n[4], 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, $n[5], 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, $n[6], 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, $n[7], 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, $n[8], 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, $n[9], 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, $n[10], 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, $n[11], 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, $n[12], 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, $n[13], 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, $n[14], 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, $n[15], 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, '', 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, '', 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, '', 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, '', 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, '', 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, '', 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, '', 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, '', 1, 0, 'C', 0);
        $this->pdf->cell(3.6, 4, '', 1, 0, 'C', 0);

        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(20, 5, 'NAMA', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(164.2, 4, $arrSSPCP["nama"], 0, 0, 'L', 0);

        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(20, 5, 'ALAMAT', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(164.2, 4, $arrSSPCP["alamat"], 0, 0, 'L', 0);

        //KODE POS
        $kodepos = $arrSSPCP["kodepos"];
        for ($i = 0; $i < (strlen($kodepos) + 1); $i++) {
            $awal = $i - 1;
            $k[$i] = substr($kodepos, $awal, 1);
        }

        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(20, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(100, 4, $arrSSPCP["kota"], 0, 0, 'L', 0);
        $this->pdf->cell(25, 4, 'Kode Pos', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(5, 4, $k[1], 0, 0, 'C', 0);
        $this->pdf->cell(5, 4, $k[2], 0, 0, 'C', 0);
        $this->pdf->cell(5, 4, $k[3], 0, 0, 'C', 0);
        $this->pdf->cell(5, 4, $k[4], 0, 0, 'C', 0);
        $this->pdf->cell(5, 4, $k[5], 0, 0, 'C', 0);

        // B. Berdasarkan dokumen		
        $this->pdf->Rect(5.4, 57.4, 199.2, 11.8, 3.5, 'F');
        $this->pdf->Rect(5.4, 69.4, 199.2, 5.9, 3.5, 'F');
        //$this->pdf->Rect(5.4, 75.4, 199.2, 6, 3.5, 'F');
        $this->pdf->SetFont('Arial', 'B', 9);

        // Kode bayar
        $kdByr = $arrSSPCP["kodebayar"];
        for ($i = 0; $i < (strlen($kdByr) + 1); $i++) {
            $awal = $i - 1;
            $kd[$i] = substr($kdByr, $awal, 1);
        }
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 8, 'C.', 0, 0, 'C', 0);
        $this->pdf->cell(50, 8, 'DOKUMEN DASAR PEMBAYARAN  :', 0, 0, 'L', 0);
        $this->pdf->Ln(1);
        $this->pdf->cell(60, 5, '', 0, 0, 'C', 0);
        if (strlen(trim($arrSSPCP["nospkbm"])) > 1) {
            if ($arrSSPCP["cicilan"] > 0) {
                $nomor = $arrSSPCP["nospkbm"] . ' Cicilan ke = ' . $arrSSPCP["cicilan"];
            } else {
                $nomor = $arrSSPCP["nospkbm"];
            }
            $tglDok = $arrSSPCP["tgldokumen"];
        } else {
            $nomor = substr($arrSSPCP["car"], 0, 6) . "-" . substr($arrSSPCP["car"], 6, 6) . "-" . substr($arrSSPCP["car"], 12, 8) . "-" . substr($arrSSPCP["car"], 20, 6);
            $tglcar = substr($arrSSPCP["car"], 12, 8);
            $tglDok = substr($tglcar, 4, 2) . "/" . substr($tglcar, 6, 2) . "/" . substr($tglcar, 0, 4);
        }

        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->cell(1, 8, '', 0, 0, 'C', 0);
        $this->pdf->cell(50, 6, $arrSSPCP["namadok"], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(25, 5, ' NOMOR 	:	', 0, 0, 'C', 0);
        $this->pdf->cell(50, 5, $nomor, 0, 0, 'L', 0);
        $this->pdf->cell(99.9, 5, ' TANGGAL 	:	' . $tglDok, 0, 0, 'R', 0);

        // c. pENERIMAAN PABAN DAN CUKAI	
        $this->pdf->Rect(5.4, 75.4, 103.5, 130, 3.5, 'F');
        $this->pdf->Rect(5.4, 83, 199.2, 122.4, 3.5, 'F');
        $this->pdf->Rect(131.2, 75.4, 73.4, 130, 3.5, 'F');
        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->Ln();
        $this->pdf->cell(5, 8, 'D.', 0, 0, 'C', 0);
        $this->pdf->cell(100, 8, 'PEMBAYARAN PENERIMAAN NEGARA', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(29.8, 3, '', 0, 0, 'C', 0);
        $this->pdf->cell(75, 8, 'AKUN', 0, 0, 'L', 0);
        $this->pdf->cell(20, 8, 'KODE AKUN', 0, 0, 'C', 0);
        $this->pdf->cell(79.9, 8, 'JUMLAH PEMBAYARAN', 0, 0, 'C', 0);

        $vbea1 = $arrBea1["amount"];
        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(100, 5, 'Bea Masuk', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '412111', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea1, 0, '', ','), 0, 0, 'R', 0);

        $vbea2 = $arrBea2["amount"];
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(100, 5, 'Bea Masuk Ditanggung Pemerintah atas Hibah (SPM) Nihil', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '412112', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea2, 0, '', ','), 0, 0, 'R', 0);

        //----------------New
        $vbea3 = $arrBea3["amount"];
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(100, 5, 'Bea Masuk Dalam Rangka Kemudahan Impor Tujuan Elspor (KITE)', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '412114', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea3, 0, '', ','), 0, 0, 'R', 0);

        $vbea4 = $arrBea4["amount"];
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(100, 5, 'Denda Administrasi Pabean', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '412113', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea4, 0, '', ','), 0, 0, 'R', 0);

        //-------------------New
        $vbea5 = $arrBea5["amount"];
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(100, 5, 'Denda Administrasi Atas Pengangkutan Barang Tertentu', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '412115', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea5, 0, '', ','), 0, 0, 'R', 0);

        $vbea6 = $arrBea6["amount"];
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(100, 5, 'Pendapatan Pabean lainnya', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '412119', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea6, 0, '', ','), 0, 0, 'R', 0);

        //--------------------New
        $vbea7 = $arrBea7["amount"];
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(100, 5, 'Bea Keluar', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, 412211, 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea7, 0, '', ','), 0, 0, 'R', 0);

        //-----------------------New
        $vbea8 = $arrBea8["amount"];
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(100, 5, 'Denda Administrasi Bea Keluar', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '412212', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea8, 0, '', ','), 0, 0, 'R', 0);

        //-------------------New
        $vbea9 = $arrBea9["amount"];
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(100, 5, 'Bunga Bea Keluar', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '412213', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea9, 0, '', ','), 0, 0, 'R', 0);

        //-------------------New
        $vbea10 = $arrBea10["amount"];
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(100, 5, 'Cukai Hasil Tembakau', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '411511', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea10, 0, '', ','), 0, 0, 'R', 0);


        $vbea11 = $arrBea11["amount"];
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(100, 5, 'Cukai Etil Alkohol', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '411512', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea11, 0, '', ','), 0, 0, 'R', 0);

        $vbea12 = $arrBea12["amount"];
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(100, 5, 'Cukai Minuman Mengandung Etil Alkohol', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '411513', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea12, 0, '', ','), 0, 0, 'R', 0);


        $vbea13 = $arrBea13["amount"];
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(100, 5, 'Pendapatan Cukai lainnya', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '411519', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea13, 0, '', ','), 0, 0, 'R', 0);

        $vbea14 = $arrBea14["amount"];
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(55, 5, 'Denda Administrasi Cukai', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '', 0, 0, 'C', 0);
        $this->pdf->cell(25, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '411514', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea14, 0, '', ','), 0, 0, 'R', 0);


        $vbea15 = $arrBea15["amount"];
        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(55, 5, 'PNBP/Pendapatan DJBC', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '', 0, 0, 'C', 0);
        $this->pdf->cell(25, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '423216', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea15, 0, '', ','), 0, 0, 'R', 0);

        //----------------------New
        $vbea16 = $arrBea16["amount"];
        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        if ($vbea16 > 0) {
            $npwpa = $arrSSPCP["npwpa"];
        } else {
            $npwpa = "";
        }
        $this->pdf->cell(80, 5, 'PPN Impor' . '																												' . 'NPWP	:' . $npwpa, 0, 0, 'L', 0);
        $this->pdf->cell(15, 4, '', 0, 0, 'C', 0);
        $this->pdf->cell(5, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '411212', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea16, 0, '.', ','), 0, 0, 'R', 0);

        $vbea17 = $arrBea17["amount"];
        $this->pdf->Ln();
        $this->pdf->Ln(1);
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(80, 5, 'PPN Hasil Tembakau/PPN Dalam Negeri', 0, 0, 'L', 0);
        $this->pdf->cell(15, 4, '', 0, 0, 'C', 0);
        $this->pdf->cell(5, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '411211', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea17, 0, '.', ','), 0, 0, 'R', 0);

        //----------------------New
        $vbea18 = $arrBea18["amount"];
        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        if ($vbea18 > 0) {
            $npwpb = $arrSSPCP["npwpb"];
        } else {
            $npwpb = "";
        }
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(80, 5, 'PPnBM Impor' . '																							' . 'NPWP	:' . $npwpb, 0, 0, 'L', 0);
        $this->pdf->cell(15, 4, '', 0, 0, 'C', 0);
        $this->pdf->cell(5, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, 411222, 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea18, 0, '.', ','), 0, 0, 'R', 0);

        $vbea19 = $arrBea19["amount"];
        $this->pdf->Ln();
        $this->pdf->Ln(1);
        if ($vbea19 > 0) {
            $npwpc = $arrSSPCP["npwpc"];
        } else {
            $npwpc = "";
        }
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(80, 5, 'PPh Pasal 22 Impor' . '													 ' . 'NPWP	:' . $npwpc, 0, 0, 'L', 0);
        $this->pdf->cell(15, 4, '', 0, 0, 'C', 0);
        $this->pdf->cell(5, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '411123', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea19, 0, '.', ','), 0, 0, 'R', 0);

        //-------------------New
        $vbea20 = $arrBea20["amount"];
        $this->pdf->Ln();
        $this->pdf->Ln(1);
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(80, 5, 'Bunga Penagihan PPN', 0, 0, 'L', 0);
        $this->pdf->cell(15, 4, '', 0, 0, 'C', 0);
        $this->pdf->cell(5, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '411622', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea20, 0, '.', ','), 0, 0, 'R', 0);

        $this->pdf->Ln(6);
        $this->pdf->cell(0.3, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(125.7, 5, 'Masa Pajak', 0, 0, 'C', 0);
        $this->pdf->cell(69.2, 5, 'Tahun', 0, 0, 'C', 0);

        $this->pdf->Rect(5.4, 205.4, 125.7, 10, 3.5, 'F');
        $this->pdf->Rect(5.4, 205.4, 199.2, 10, 3.5, 'F');
        $this->pdf->Rect(5.4, 215.4, 199.2, 15, 3.5, 'F');
        /* masa pajak */
        $this->pdf->Ln();
        $this->pdf->cell(0.4, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(10.5, 5, 'Jan', 1, 0, 'C', 0);
        $this->pdf->cell(10.5, 5, 'Feb', 1, 0, 'C', 0);
        $this->pdf->cell(10.5, 5, 'Mar', 1, 0, 'C', 0);
        $this->pdf->cell(10.5, 5, 'Apr', 1, 0, 'C', 0);
        $this->pdf->cell(10.5, 5, 'Mei', 1, 0, 'C', 0);
        $this->pdf->cell(10.5, 5, 'Jun', 1, 0, 'C', 0);
        $this->pdf->cell(10.5, 5, 'Jul', 1, 0, 'C', 0);
        $this->pdf->cell(10.5, 5, 'Ags', 1, 0, 'C', 0);
        $this->pdf->cell(10.5, 5, 'Sep', 1, 0, 'C', 0);
        $this->pdf->cell(10.5, 5, 'Okt', 1, 0, 'C', 0);
        $this->pdf->cell(10.5, 5, 'Nop', 1, 0, 'C', 0);
        $this->pdf->cell(10.2, 5, 'Des', 1, 0, 'C', 0);

        /* tahun pajak */
        $thpjk = $arrSSPCP["thpjk"];
        for ($i = 0; $i < (strlen($thpjk) + 1); $i++) {
            $awal = $i - 1;
            $t[$i] = substr($thpjk, $awal, 1);
        }

        $this->pdf->cell(14.6, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(10, 5, $t[1], 1, 0, 'C', 0);
        $this->pdf->cell(10, 5, $t[2], 1, 0, 'C', 0);
        $this->pdf->cell(10, 5, $t[3], 1, 0, 'C', 0);
        $this->pdf->cell(10, 5, $t[4], 1, 0, 'C', 0);
        $this->pdf->cell(14.6, 5, '', 0, 0, 'C', 0);

        $this->pdf->Ln();
        $this->pdf->SetFont('arial', 'I', '5');
        $this->pdf->cell(5, 1, '', 0, 0, 'C', 0);

        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, 'E.', 0, 0, 'C', 0);
        $this->pdf->cell(80, 5, 'Jumlah Pembayaran Penerimaan Negara :', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, number_format($arrSSPCP["total"], 0, '', ','), 0, 0, 'R', 0);

        $rupiah = $this->terbilang(($arrSSPCP["total"])) . ' rupiah';
        if (strlen($rupiah) > 100) {
            for ($i = 100; $i > 0; $i--) {
                $sub = substr($rupiah, $i, 1);
                if ($sub == " ") {
                    $potong = $i;
                    break;
                }
            }
            $rup1 = substr($rupiah, 0, $potong);
            $rup2 = substr($rupiah, $potong + 1);
        } else {
            $rup1 = $rupiah;
            $rup2 = "";
        }

        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(30, 5, 'Dengan huruf :', 0, 0, 'L', 0);
        $this->pdf->cell(164.2, 5, $rup1, 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(30, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(164.2, 5, $rup2, 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(30, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(164.2, 1, '', 0, 0, 'L', 0);

        //footer
        $this->pdf->Rect(5.4, 230.4, 100.2, 47, 3.5, 'F');
        $this->pdf->Rect(105.5, 230.4, 99.2, 47, 3.5, 'F');

        $this->pdf->SetFont('Arial', '', 7);
        $this->pdf->Ln(-1);
        $this->pdf->cell(23, 5, 'Diterima Oleh	:	', 0, 0, 'R', 0);
        $this->pdf->cell(5, 5, '', 1, 0, 'R', 0);
        $this->pdf->cell(26, 5, 'Kantor Bea dan Cukai', 0, 0, 'R', 0);
        $this->pdf->cell(5, 5, '', 0, 0, 'R', 0);
        $this->pdf->cell(5, 5, '', 1, 0, 'R', 0);
        $this->pdf->cell(14, 5, 'Kantor Pos', 0, 0, 'R', 0);
        $this->pdf->Ln(7);
        $this->pdf->SetFont('Arial', '', 8);
        $this->pdf->cell(4, 3, '', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, '	NPWP', 0, 0, 'L', 0);
        $this->pdf->cell(6, 5, ':', 0, 0, 'C', 0);
        $this->pdf->SetFont('Arial', 'B', 9);

        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', '', 8);
        $this->pdf->cell(4, 3, '', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, '	Nama Kantor', 0, 0, 'L', 0);
        $this->pdf->cell(6, 5, ':', 0, 0, 'C', 0);
        $this->pdf->SetFont('Arial', 'B', 7);

        $nosspcpa = substr($arrSSPCP["nosspcp"], 0, 3);
        $nosspcpb = substr($arrSSPCP["nosspcp"], 3, 4);
        $nosspcpc = substr($arrSSPCP["nosspcp"], 7, 6);
        $nosspcpcomplete = $nosspcpa . "/" . $nosspcpb . "/" . $nosspcpc;

        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', '', 8);
        $this->pdf->cell(4, 3, '', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, '	Kode Kantor', 0, 0, 'L', 0);
        $this->pdf->cell(6, 5, ':', 0, 0, 'C', 0);
        $this->pdf->SetFont('Arial', 'B', 9);

        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->cell(4.5, 5, '', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, 'Nomor SSPCP', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->cell(79.2, 5, '', 0, 0, 'C', 0);

        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', '', 8);
        $this->pdf->cell(4.5, 5, '', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, 'Tanggal', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->cell(79.2, 5, '', 0, 0, 'C', 0);

        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', 'I', 6);
        $this->pdf->cell(20.5, 5, '', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, 'Cap dan tanda tangan', 0, 0, 'C', 0);

        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', '', 7);
        $this->pdf->cell(20.5, 5, '', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, 'Nama	:	', 0, 0, 'C', 0);


        $this->pdf->Ln(-42);
        $this->pdf->cell(106, 5, '', 0, 0, 'R', 0);
        $this->pdf->SetFont('Arial', 'B', 12);
        $this->pdf->cell(5, 5, 'X', 1, 0, 'R', 0);
        $this->pdf->SetFont('Arial', '', 7);
        $this->pdf->cell(25, 5, 'Bank Devisa Persepsi', 0, 0, 'R', 0);
        $this->pdf->cell(5, 5, '', 0, 0, 'R', 0);
        $this->pdf->cell(5, 5, '', 1, 0, 'R', 0);
        $this->pdf->cell(17, 5, 'Bank Persepsi', 0, 0, 'R', 0);
        $this->pdf->cell(5, 5, '', 0, 0, 'R', 0);
        $this->pdf->cell(5, 5, '', 1, 0, 'R', 0);
        $this->pdf->cell(15.5, 5, 'Pos Persepsi', 0, 0, 'R', 0);
        $this->pdf->Ln(7);

        $this->pdf->SetFont('Arial', '', 8);
        $this->pdf->cell(105, 3, '', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, '	Nama Bank/Pos', 0, 0, 'L', 0);
        $this->pdf->cell(6, 5, ':', 0, 0, 'C', 0);
        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->cell(20, 5, 'Standard Chartered Bank ' . $cbg, 0, 0, 'L', 0);

        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', '', 8);
        $this->pdf->cell(105, 3, '', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, '	Kode Bank/Pos', 0, 0, 'L', 0);
        $this->pdf->cell(6, 5, ':', 0, 0, 'C', 0);
        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->cell(20, 5, $_SESSION["fino"], 0, 0, 'L', 0);

        $nosspcpa = substr($arrSSPCP["nosspcp"], 0, 3);
        $nosspcpb = substr($arrSSPCP["nosspcp"], 3, 4);
        $nosspcpc = substr($arrSSPCP["nosspcp"], 7, 6);
        $nosspcpcomplete = $nosspcpa . "/" . $nosspcpb . "/" . $nosspcpc;

        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->cell(105.5, 5, '', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, 'Nomor SSPCP', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->cell(65, 5, $nosspcpcomplete, 0, 0, 'L', 0);
        $this->pdf->cell(79.2, 5, '', 0, 0, 'C', 0);

        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', '', 8);
        $this->pdf->cell(105.5, 5, '', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, 'Unit KPPN', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->SetFont('Arial', 'B', 7);
        /* if ($arrSSPCP["kpkn"]=="139"){
          $nmKppn = "Surabaya II";
          }else{
          $nmKppn = "Jakarta V";
          } */

        if ($_SESSION['userweb'] == 'T0002') {
            $this->pdf->cell(65, 5, $arrSSPCP["kpp"] . '		' . 'Kode' . ' : ' . $arrSSPCP["kpkn"], 0, 0, 'L', 0);
            $this->pdf->cell(79.2, 5, '', 0, 0, 'C', 0);
        } else {
            if ($nosspcpb == '0001') {
                $this->pdf->cell(65, 5, $arrSSPCP["kpp"] . '		' . 'Kode' . ' : ' . $arrSSPCP["kpkn"], 0, 0, 'L', 0);
                $this->pdf->cell(79.2, 5, '', 0, 0, 'C', 0);
            } else {
                $this->pdf->cell(65, 5, $arrSSPCP["kpp"] . '		' . 'Kode' . ' : ' . $arrSSPCP["kpkn"], 0, 0, 'L', 0);
                $this->pdf->cell(79.2, 5, '', 0, 0, 'C', 0);
            }
        }
        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', '', 8);
        $this->pdf->cell(105.5, 5, '', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, 'Tanggal', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->cell(65, 5, $arrSSPCP["tgl"], 0, 0, 'L', 0);
        $this->pdf->cell(79.2, 5, '', 0, 0, 'C', 0);

        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', 'I', 6);
        $this->pdf->cell(125.5, 5, '', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, 'Cap dan tanda tangan', 0, 0, 'C', 0);

        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', '', 7);
        $this->pdf->cell(125.5, 5, '', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, 'Nama	:	' . $arrSSPCP["petugasbank"], 0, 0, 'C', 0);

        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', 'B', 12);
        $this->pdf->cell(104.5, 4, '', 0, 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', 'B', 7);
        $this->pdf->cell(99, 1, '', 0, 0, 'R', 0);

        $this->pdf->setxy(139, 214);
        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->Ln();

        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->Ln(56);
        $this->pdf->cell(11.5, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(20, 5, 'NTB/NTP 		:', 0, 0, 'L', 0);
        if ($arrSSPCP["ntbresi"] <> "") {
            $this->pdf->cell(10, 5, $arrSSPCP["ntbresi"], 0, 0, 'L', 0);
        } else {
            $this->pdf->cell(10, 5, $arrSSPCP["Ntb"], 0, 0, 'L', 0);
        }
        $this->pdf->cell(61.5, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(20, 5, 'NTPN								 :', 0, 0, 'L', 0);
        //$this->pdf->cell(10,5,$arrSSPCP["ntpnresi"],0,0,'L',0);
        $this->pdf->cell(10, 5, $arrSSPCP["ntpn"], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->Rect(5.4, 277.4, 199.2, 6, 3.5, 'F');
        $this->pdf->Ln();

        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->Ln();

        // untuk header sscpc
        $this->pdf->SetY(8);
        $this->pdf->SetFont('Arial', 'B', 11);
        $this->pdf->Cell(199.2, 5, 'SURAT SETORAN', 0, 0, 'C');
        $this->pdf->Ln();
        $this->pdf->Cell(199.2, 5, ' PABEAN, CUKAI, DAN', 0, 0, 'C');
        $this->pdf->Ln();
        $this->pdf->Cell(199.2, 5, 'PAJAK (SSPCP)', 0, 0, 'C');
        $this->pdf->Ln();
        $this->pdf->Cell(199.2, 5, '', 0, 0, 'C');
        $this->pdf->Ln();
        $this->pdf->Cell(199.2, 5, '', 0, 0, 'C');

        $this->pdf->Setx(190);
        $this->pdf->SetY(14);
        $this->pdf->SetFont('Arial', '', 30);
        $this->pdf->Cell(156, 14, substr($arrLembar["kunci"], 0, 1), 0, 0, 'R');

        $mspjkawal = substr($arrSSPCP["mspjk"], 0, 2);
        //echo $mspjkawal; exit;
        switch ($mspjkawal) {
            case '01' :
                $this->pdf->SetXY(10, 210.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
            case '02' :
                $this->pdf->SetXY(20, 210.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
            case '03' :
                $this->pdf->SetXY(30, 210.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
            case '04' :
                $this->pdf->SetXY(40, 210.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
            case '05' :
                $this->pdf->SetXY(50, 210.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
            case '06' :
                $this->pdf->SetXY(60, 210.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
            case '07' :
                $this->pdf->SetXY(70, 210.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
            case '08' :
                $this->pdf->SetXY(80, 210.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
            case '09' :
                $this->pdf->SetXY(90, 210.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
            case '10' :
                $this->pdf->SetXY(100, 210.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
            case '11' :
                $this->pdf->SetXY(110, 210.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
            case '12' :
                $this->pdf->SetXY(120, 210.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
        }

        $kodeid = trim($arrSSPCP["kodeid"]);
        //echo $kodeid; exit;
        //$kodeid = '2';
        switch ($kodeid) {
            case '2' :
                $this->pdf->SetXY(121, 33.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
            case '3' :
                $this->pdf->SetXY(163, 33.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
            case '5' :
                $this->pdf->SetXY(79, 33.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
        }

        $kodejpn = trim($arrSSPCP["jpn"]);
        //echo $kodejpn; exit;
        //$kodejpn = '4';
        switch ($kodejpn) {
            case '1' :
                $this->pdf->SetXY(79, 27.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
            case '2' :
                $this->pdf->SetXY(106, 27.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
            case '3' :
                $this->pdf->SetXY(134, 27.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
            case '4' :
                $this->pdf->SetXY(161, 27.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
        }
    }

    //-------------------- cetak SSPCP Semua Lembar------------------------------------------------//

    function drawSSPCPALL($arrDokumen) {
        $map = $arrDokumen["MAP"];
        //print_r($arrSSPCP); exit;
        $arrSSPCP = $arrDokumen["SSPCP"];
        $arrBea1 = $arrDokumen["BEA01"];
        $arrBea2 = $arrDokumen["BEA02"];
        $arrBea3 = $arrDokumen["BEA03"];
        $arrBea4 = $arrDokumen["BEA04"];
        $arrBea5 = $arrDokumen["BEA05"];
        $arrBea6 = $arrDokumen["BEA06"];
        $arrBea7 = $arrDokumen["BEA07"];
        $arrBea8 = $arrDokumen["BEA08"];
        $arrBea9 = $arrDokumen["BEA09"];
        $arrBea10 = $arrDokumen["BEA10"];
        $arrBea11 = $arrDokumen["BEA11"];
        $arrBea12 = $arrDokumen["BEA12"];
        $arrBea13 = $arrDokumen["BEA13"];
        $arrBea14 = $arrDokumen["BEA14"];
        $arrBea15 = $arrDokumen["BEA15"];
        $arrBea16 = $arrDokumen["BEA16"];
        $arrBea17 = $arrDokumen["BEA17"];
        $arrBea18 = $arrDokumen["BEA18"];
        $arrBea19 = $arrDokumen["BEA19"];
        $arrBea20 = $arrDokumen["BEA20"];

        $this->pdf->SetLeftMargin(5);
        $this->pdf->SetRightMargin(5);
        $this->pdf->AddPage();
        $this->pdf->Rect(5.4, 5.4, 71.4, 22, 3.5, 'F');
        $this->pdf->Rect(76.8, 5.4, 56.4, 22, 3.5, 'F');
        $this->pdf->Rect(133.2, 5.4, 71.4, 22, 3.5, 'F');

        $this->pdf->SetFont('arial', 'B', '9');
        $this->pdf->cell(1, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(65.4, 5, 'KEMENTERIAN KEUANGAN R.I.', 0, 0, 'L', 0);
        //$this->pdf->cell(55.4,5,'LEMBAR KE-1          :'.' '.$arrLembar["lembar1"],0,0,'L',0);
        $this->pdf->Ln();
        $this->pdf->cell(1, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(65.4, 5, 'DIREKTORAT JENDRAL BEA DAN CUKAI', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('arial', '', '8');
        $this->pdf->cell(1, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(65.4, 5, 'Kantor : ' . $arrSSPCP["kpbc"], 0, 0, 'L', 0);
        $this->pdf->cell(65.4, 5, '', 0, 0, 'L', 0);

        $this->pdf->SetFont('arial', '', '8');
        //$this->pdf->cell(55.4,5,'LEMBAR KE-1          :'.' '.$arrLembar["lembar1"],0,0,'L',0);
        $this->pdf->Ln();

        $this->pdf->cell(5, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(50, 5, 'Kode Kantor : ' . $arrSSPCP["kodekpbc"], 1, 0, 'L', 0);

        $this->pdf->Ln();
        $this->pdf->Rect(5.4, 27.4, 71.4, 6, 3.5, 'F');
        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->Ln(1);
        $this->pdf->cell(5.4, 8.5, 'A.', 0, 0, 'C', 0);
        $this->pdf->cell(66.8, 8.5, 'JENIS PENERIMAAN NEGARA', 0, 0, 'L', 0);

        $this->pdf->Rect(76.8, 27.4, 56.4, 6, 3.5, 'F');

        $this->pdf->Ln(1);
        $this->pdf->cell(71.9, 6, '', 0, 0, 'C', 0);
        $this->pdf->cell(9, 6, '', 1, 0, 'C', 0);
        $this->pdf->cell(19, 6, 'IMPOR', 1, 0, 'C', 0);
        $this->pdf->cell(9, 6, '', 1, 0, 'C', 0);
        $this->pdf->cell(19.3, 6, 'EKSPOR', 1, 0, 'C', 0);
        $this->pdf->cell(9, 6, '', 1, 0, 'C', 0);

        $this->pdf->cell(18, 6, 'CUKAI', 1, 0, 'C', 0);
        $this->pdf->cell(9, 6, '', 1, 0, 'C', 0);
        $this->pdf->cell(35.4, 6, 'BARANG TERTENTU', 1, 0, 'L', 0);

        $this->pdf->Rect(5.4, 33.4, 71.4, 6, 3.5, 'F');
        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->Ln(1);
        $this->pdf->cell(5.4, 16.5, 'B.', 0, 0, 'C', 0);
        $this->pdf->cell(66.8, 16.5, 'JENIS IDENTITAS', 0, 0, 'L', 0);

        //$this->pdf->Rect(133.2, 27.4, 71.4, 6, 3.5, 'F');
        //$this->pdf->Rect(76.8, 33.4, 56.4, 6, 3.5, 'F');
        $this->pdf->Ln(5);
        $this->pdf->cell(71.9, 6, '', 0, 0, 'C', 0);
        $this->pdf->cell(9, 6, '', 1, 0, 'C', 0);
        $this->pdf->cell(33, 6, 'NPWP', 0, 0, 'C', 0);
        $this->pdf->cell(9, 6, '', 1, 0, 'C', 0);
        $this->pdf->cell(33, 6, 'PASPOR', 0, 0, 'C', 0);
        $this->pdf->cell(9, 6, '', 1, 0, 'C', 0);
        //$this->pdf->Rect(133.2, 27.4, 71.4, 6, 3.5, 'F');
        $this->pdf->cell(34.8, 6, 'KTP', 1, 0, 'C', 0);
        $this->pdf->Ln();
        //$this->pdf->Rect(133.2, 33.4, 71.4, 6, 3.5, 'F');
        //A. NPWP, NAMA, ALAMAT
        $npwp = $arrSSPCP["npwp"];
        for ($i = 0; $i < (strlen($npwp) + 1); $i++) {
            $awal = $i - 1;
            $n[$i] = substr($npwp, $awal, 1);
        }
        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->Rect(5.4, 39.4, 199.2, 18, 3.5, 'F');
        $this->pdf->Ln();
        $this->pdf->Ln(-6);
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(20, 5, 'NOMOR', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->SetFont('Arial', '', 9);

        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(7, 4, $n[1], 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, $n[2], 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, $n[3], 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, $n[4], 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, $n[5], 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, $n[6], 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, $n[7], 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, $n[8], 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, $n[9], 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, $n[10], 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, $n[11], 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, $n[12], 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, $n[13], 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, $n[14], 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, $n[15], 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, '', 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, '', 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, '', 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, '', 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, '', 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, '', 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, '', 1, 0, 'C', 0);
        $this->pdf->cell(7, 4, '', 1, 0, 'C', 0);
        $this->pdf->cell(3.6, 4, '', 1, 0, 'C', 0);

        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(20, 5, 'NAMA', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(164.2, 4, $arrSSPCP["nama"], 0, 0, 'L', 0);

        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(20, 5, 'ALAMAT', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(164.2, 4, $arrSSPCP["alamat"], 0, 0, 'L', 0);

        //KODE POS
        $kodepos = $arrSSPCP["kodepos"];
        for ($i = 0; $i < (strlen($kodepos) + 1); $i++) {
            $awal = $i - 1;
            $k[$i] = substr($kodepos, $awal, 1);
        }

        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(20, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(100, 4, $arrSSPCP["kota"], 0, 0, 'L', 0);
        $this->pdf->cell(25, 4, 'Kode Pos', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(5, 4, $k[1], 0, 0, 'C', 0);
        $this->pdf->cell(5, 4, $k[2], 0, 0, 'C', 0);
        $this->pdf->cell(5, 4, $k[3], 0, 0, 'C', 0);
        $this->pdf->cell(5, 4, $k[4], 0, 0, 'C', 0);
        $this->pdf->cell(5, 4, $k[5], 0, 0, 'C', 0);

        // B. Berdasarkan dokumen		
        $this->pdf->Rect(5.4, 57.4, 199.2, 13.8, 3.5, 'F');
        $this->pdf->Rect(5.4, 71.4, 199.2, 5.9, 3.5, 'F');
        //$this->pdf->Rect(5.4, 75.4, 199.2, 6, 3.5, 'F');
        $this->pdf->SetFont('Arial', 'B', 9);

        // Kode bayar
        $kdByr = $arrSSPCP["kodebayar"];
        for ($i = 0; $i < (strlen($kdByr) + 1); $i++) {
            $awal = $i - 1;
            $kd[$i] = substr($kdByr, $awal, 1);
        }
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 8, 'C.', 0, 0, 'C', 0);
        $this->pdf->cell(50, 8, 'DOKUMEN DASAR PEMBAYARAN  :', 0, 0, 'L', 0);
        $this->pdf->Ln(1);
        $this->pdf->cell(60, 5, '', 0, 0, 'C', 0);
        if (strlen(trim($arrSSPCP["nospkbm"])) > 1) {
            if ($arrSSPCP["cicilan"] > 0) {
                $nomor = $arrSSPCP["nospkbm"] . ' Cicilan ke = ' . $arrSSPCP["cicilan"];
            } else {
                $nomor = $arrSSPCP["nospkbm"];
            }
            $tglDok = $arrSSPCP["tgldokumen"];
        } else {
            $nomor = substr($arrSSPCP["car"], 0, 6) . "-" . substr($arrSSPCP["car"], 6, 6) . "-" . substr($arrSSPCP["car"], 12, 8) . "-" . substr($arrSSPCP["car"], 20, 6);
            $tglcar = substr($arrSSPCP["car"], 12, 8);
            $tglDok = substr($tglcar, 4, 2) . "/" . substr($tglcar, 6, 2) . "/" . substr($tglcar, 0, 4);
        }

        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->cell(1, 8, '', 0, 0, 'C', 0);
        $NamaDok = $this->trimstr($arrSSPCP["kodebayar"] . '- ' . $arrSSPCP["namadok"], 90);
        $this->pdf->cell(50, 6, $NamaDok['str1'], 0, 0, 'L', 0);
        $this->pdf->Ln(4);
        $this->pdf->cell(1, 8, '', 0, 0, 'C', 0);
        $this->pdf->cell(50, 6, $NamaDok['str2'], 0, 0, 'L', 0);
        $this->pdf->Ln(5);
        $this->pdf->cell(25, 5, ' NOMOR 	:	', 0, 0, 'C', 0);
        $this->pdf->cell(50, 5, $nomor, 0, 0, 'L', 0);
        $this->pdf->cell(99.9, 5, ' TANGGAL 	:	' . $tglDok, 0, 0, 'R', 0);

        // c. pENERIMAAN PABAN DAN CUKAI	
        $this->pdf->Rect(5.4, 77.4, 103.5, 128, 3.5, 'F');
        $this->pdf->Rect(5.4, 83, 199.2, 122.4, 3.5, 'F');
        $this->pdf->Rect(131.2, 77.4, 73.4, 128, 3.5, 'F');
        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->Ln(4);
        $this->pdf->cell(5, 8, 'D.', 0, 0, 'C', 0);
        $this->pdf->cell(100, 8, 'PEMBAYARAN PENERIMAAN NEGARA', 0, 0, 'L', 0);
        $this->pdf->Ln(6);
        $this->pdf->cell(29.8, 3, '', 0, 0, 'C', 0);
        $this->pdf->cell(75, 8, 'AKUN', 0, 0, 'L', 0);
        $this->pdf->cell(20, 8, 'KODE AKUN', 0, 0, 'C', 0);
        $this->pdf->cell(79.9, 8, 'JUMLAH PEMBAYARAN', 0, 0, 'C', 0);

        $vbea1 = $arrBea1["amount"];
        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(100, 5, 'Bea Masuk', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '412111', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea1, 0, '', ','), 0, 0, 'R', 0);

        $vbea2 = $arrBea2["amount"];
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(100, 5, 'Bea Masuk Ditanggung Pemerintah atas Hibah (SPM) Nihil', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '412112', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea2, 0, '', ','), 0, 0, 'R', 0);

        //----------------New
        $vbea3 = $arrBea3["amount"];
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(100, 5, 'Bea Masuk Dalam Rangka Kemudahan Impor Tujuan Ekspor (KITE)', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '412114', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea3, 0, '', ','), 0, 0, 'R', 0);

        $vbea4 = $arrBea4["amount"];
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(100, 5, 'Denda Administrasi Pabean', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '412113', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea4, 0, '', ','), 0, 0, 'R', 0);

        //-------------------New
        $vbea5 = $arrBea5["amount"];
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(100, 5, 'Denda Administrasi Atas Pengangkutan Barang Tertentu', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '412115', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea5, 0, '', ','), 0, 0, 'R', 0);

        $vbea6 = $arrBea6["amount"];
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(100, 5, 'Pendapatan Pabean lainnya', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '412119', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea6, 0, '', ','), 0, 0, 'R', 0);

        //--------------------New
        $vbea7 = $arrBea7["amount"];
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(100, 5, 'Bea Keluar', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, 412211, 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea7, 0, '', ','), 0, 0, 'R', 0);

        //-----------------------New
        $vbea8 = $arrBea8["amount"];
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(100, 5, 'Denda Administrasi Bea Keluar', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '412212', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea8, 0, '', ','), 0, 0, 'R', 0);

        //-------------------New
        $vbea9 = $arrBea9["amount"];
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(100, 5, 'Bunga Bea Keluar', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '412213', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea9, 0, '', ','), 0, 0, 'R', 0);

        //-------------------New
        $vbea10 = $arrBea10["amount"];
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(100, 5, 'Cukai Hasil Tembakau', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '411511', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea10, 0, '', ','), 0, 0, 'R', 0);


        $vbea11 = $arrBea11["amount"];
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(100, 5, 'Cukai Etil Alkohol', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '411512', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea11, 0, '', ','), 0, 0, 'R', 0);

        $vbea12 = $arrBea12["amount"];
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(100, 5, 'Cukai Minuman Mengandung Etil Alkohol', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '411513', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea12, 0, '', ','), 0, 0, 'R', 0);


        $vbea13 = $arrBea13["amount"];
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(100, 5, 'Pendapatan Cukai lainnya', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '411519', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea13, 0, '', ','), 0, 0, 'R', 0);

        $vbea14 = $arrBea14["amount"];
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(55, 5, 'Denda Administrasi Cukai', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '', 0, 0, 'C', 0);
        $this->pdf->cell(25, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '411514', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea14, 0, '', ','), 0, 0, 'R', 0);


        $vbea15 = $arrBea15["amount"];

        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(55, 5, 'PNBP/Pendapatan DJBC', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '', 0, 0, 'C', 0);
        $this->pdf->cell(25, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '423216', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea15, 0, '', ','), 0, 0, 'R', 0);

        //----------------------New
        $vbea16 = $arrBea16["amount"];
        if ($vbea16 > 0) {
            $npwpa = $arrSSPCP["npwpa"];
        } else {
            $npwpa = "";
        }
        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(80, 5, 'PPN Impor' . '																												' . 'NPWP	:' . $npwpa, 0, 0, 'L', 0);
        $this->pdf->cell(15, 4, '', 0, 0, 'C', 0);
        $this->pdf->cell(5, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '411212', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea16, 0, '.', ','), 0, 0, 'R', 0);

        $vbea17 = $arrBea17["amount"];
        $this->pdf->Ln();
        $this->pdf->Ln(1);
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(80, 5, 'PPN Hasil Tembakau/PPN Dalam Negeri', 0, 0, 'L', 0);
        $this->pdf->cell(15, 4, '', 0, 0, 'C', 0);
        $this->pdf->cell(5, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '411211', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea17, 0, '.', ','), 0, 0, 'R', 0);

        //----------------------New
        $vbea18 = $arrBea18["amount"];
        if ($vbea18 > 0) {
            $npwpb = $arrSSPCP["npwpb"];
        } else {
            $npwpb = "";
        }
        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->Ln(1);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(80, 5, 'PPnBM Impor' . '																							' . 'NPWP	:' . $npwpb, 0, 0, 'L', 0);
        $this->pdf->cell(15, 4, '', 0, 0, 'C', 0);
        $this->pdf->cell(5, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, 411222, 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea18, 0, '.', ','), 0, 0, 'R', 0);

        $vbea19 = $arrBea19["amount"];
        if ($vbea19 > 0) {
            $npwpc = $arrSSPCP["npwpc"];
        } else {
            $npwpc = "";
        }
        $this->pdf->Ln();
        $this->pdf->Ln(1);
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(80, 5, 'PPh Pasal 22 Impor' . '													 ' . 'NPWP	:' . $arrSSPCP["npwpc"], 0, 0, 'L', 0);
        $this->pdf->cell(15, 4, '', 0, 0, 'C', 0);
        $this->pdf->cell(5, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '411123', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea19, 0, '.', ','), 0, 0, 'R', 0);

        //-------------------New
        $vbea20 = $arrBea20["amount"];
        $this->pdf->Ln();
        $this->pdf->Ln(1);
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(80, 5, 'Bunga Penagihan PPN', 0, 0, 'L', 0);
        $this->pdf->cell(15, 4, '', 0, 0, 'C', 0);
        $this->pdf->cell(5, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(20, 4, '411622', 0, 0, 'C', 0);
        $this->pdf->cell(0.9, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, number_format($vbea20, 0, '.', ','), 0, 0, 'R', 0);

        $this->pdf->Ln(6);
        $this->pdf->cell(0.3, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(125.7, 5, 'Masa Pajak', 0, 0, 'C', 0);
        $this->pdf->cell(69.2, 5, 'Tahun', 0, 0, 'C', 0);

        $this->pdf->Rect(5.4, 205.4, 125.7, 10, 3.5, 'F');
        $this->pdf->Rect(5.4, 205.4, 199.2, 10, 3.5, 'F');
        $this->pdf->Rect(5.4, 215.4, 199.2, 15, 3.5, 'F');
        /* masa pajak */
        $this->pdf->Ln();
        $this->pdf->cell(0.4, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(10.5, 5, 'Jan', 1, 0, 'C', 0);
        $this->pdf->cell(10.5, 5, 'Feb', 1, 0, 'C', 0);
        $this->pdf->cell(10.5, 5, 'Mar', 1, 0, 'C', 0);
        $this->pdf->cell(10.5, 5, 'Apr', 1, 0, 'C', 0);
        $this->pdf->cell(10.5, 5, 'Mei', 1, 0, 'C', 0);
        $this->pdf->cell(10.5, 5, 'Jun', 1, 0, 'C', 0);
        $this->pdf->cell(10.5, 5, 'Jul', 1, 0, 'C', 0);
        $this->pdf->cell(10.5, 5, 'Ags', 1, 0, 'C', 0);
        $this->pdf->cell(10.5, 5, 'Sep', 1, 0, 'C', 0);
        $this->pdf->cell(10.5, 5, 'Okt', 1, 0, 'C', 0);
        $this->pdf->cell(10.5, 5, 'Nop', 1, 0, 'C', 0);
        $this->pdf->cell(10.2, 5, 'Des', 1, 0, 'C', 0);

        /* tahun pajak */
        $thpjk = $arrSSPCP["thpjk"];
        for ($i = 0; $i < (strlen($thpjk) + 1); $i++) {
            $awal = $i - 1;
            $t[$i] = substr($thpjk, $awal, 1);
        }

        $this->pdf->cell(14.6, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(10, 5, $t[1], 1, 0, 'C', 0);
        $this->pdf->cell(10, 5, $t[2], 1, 0, 'C', 0);
        $this->pdf->cell(10, 5, $t[3], 1, 0, 'C', 0);
        $this->pdf->cell(10, 5, $t[4], 1, 0, 'C', 0);
        $this->pdf->cell(14.6, 5, '', 0, 0, 'C', 0);

        $this->pdf->Ln();
        $this->pdf->SetFont('arial', 'I', '5');
        $this->pdf->cell(5, 1, '', 0, 0, 'C', 0);

        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, 'E.', 0, 0, 'C', 0);
        $this->pdf->cell(80, 5, 'Jumlah Pembayaran Penerimaan Negara :', 0, 0, 'L', 0);
        $this->pdf->cell(9, 5, 'Rp.', 0, 0, 'L', 0);
        $this->pdf->cell(35, 5, number_format($arrSSPCP["total"], 0, '', ','), 0, 0, 'R', 0);

        $rupiah = $this->terbilang(($arrSSPCP["total"])) . ' rupiah';
        if (strlen($rupiah) > 100) {
            for ($i = 100; $i > 0; $i--) {
                $sub = substr($rupiah, $i, 1);
                if ($sub == " ") {
                    $potong = $i;
                    break;
                }
            }
            $rup1 = substr($rupiah, 0, $potong);
            $rup2 = substr($rupiah, $potong + 1);
        } else {
            $rup1 = $rupiah;
            $rup2 = "";
        }

        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(30, 5, 'Dengan huruf :', 0, 0, 'L', 0);
        $this->pdf->cell(164.2, 5, $rup1, 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(30, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(164.2, 5, $rup2, 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(30, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(164.2, 1, '', 0, 0, 'L', 0);

        //footer
        $this->pdf->Rect(5.4, 230.4, 100.2, 47, 3.5, 'F');
        $this->pdf->Rect(105.5, 230.4, 99.2, 47, 3.5, 'F');

        $this->pdf->SetFont('Arial', '', 7);
        $this->pdf->Ln(-1);
        $this->pdf->cell(23, 5, 'Diterima Oleh	:	', 0, 0, 'R', 0);
        $this->pdf->cell(5, 5, '', 1, 0, 'R', 0);
        $this->pdf->cell(26, 5, 'Kantor Bea dan Cukai', 0, 0, 'R', 0);
        $this->pdf->cell(5, 5, '', 0, 0, 'R', 0);
        $this->pdf->cell(5, 5, '', 1, 0, 'R', 0);
        $this->pdf->cell(14, 5, 'Kantor Pos', 0, 0, 'R', 0);
        $this->pdf->Ln(7);
        $this->pdf->SetFont('Arial', '', 8);
        $this->pdf->cell(4, 3, '', 0, 0, 'R', 0);
        $this->pdf->cell(26, 5, '	NPWP', 0, 0, 'L', 0);
        $this->pdf->cell(10, 5, ': ', 0, 0, 'L', 0);
        $this->pdf->SetFont('Arial', 'B', 9);

        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', '', 8);
        $this->pdf->cell(4, 3, '', 0, 0, 'R', 0);
        $this->pdf->cell(26, 5, '	Nama Kantor', 0, 0, 'L', 0);
        $this->pdf->cell(10, 5, ': ' . $arrSSPCP["kpbc"], 0, 0, 'L', 0);
        $this->pdf->SetFont('Arial', 'B', 7);

        $nosspcpa = substr($arrSSPCP["nosspcp"], 0, 3);
        $nosspcpb = substr($arrSSPCP["nosspcp"], 3, 4);
        $nosspcpc = substr($arrSSPCP["nosspcp"], 7, 6);
        $nosspcpcomplete = $nosspcpa . "/" . $nosspcpb . "/" . $nosspcpc;

        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', '', 8);
        $this->pdf->cell(4, 3, '', 0, 0, 'R', 0);
        $this->pdf->cell(26, 5, '	Kode Kantor', 0, 0, 'L', 0);
        $this->pdf->cell(10, 5, ': ' . $arrSSPCP["kodekpbc"], 0, 0, 'L', 0);
        $this->pdf->SetFont('Arial', 'B', 9);

        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->cell(4.5, 5, '', 0, 0, 'R', 0);
        $this->pdf->cell(26, 5, 'Nomor SSPCP', 0, 0, 'L', 0);
        $this->pdf->cell(10, 5, ':', 0, 0, 'L', 0);
        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->cell(79.2, 5, '', 0, 0, 'L', 0);

        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', '', 8);
        $this->pdf->cell(4.5, 5, '', 0, 0, 'R', 0);
        $this->pdf->cell(26, 5, 'Tanggal', 0, 0, 'L', 0);
        $this->pdf->cell(10, 5, ':', 0, 0, 'L', 0);
        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->cell(79.2, 5, '', 0, 0, 'L', 0);

        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', 'I', 6);
        $this->pdf->cell(20.5, 5, '', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, 'Cap dan tanda tangan', 0, 0, 'C', 0);

        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', '', 7);
        $this->pdf->cell(20.5, 5, '', 0, 0, 'R', 0);
        $this->pdf->cell(20, 5, 'Nama	:	', 0, 0, 'C', 0);


        $this->pdf->Ln(-42);
        $this->pdf->cell(106, 5, '', 0, 0, 'R', 0);
        $this->pdf->SetFont('Arial', 'B', 12);
        $this->pdf->cell(5, 5, '', 1, 0, 'R', 0);
        $this->pdf->SetFont('Arial', '', 7);
        $this->pdf->cell(25, 5, 'Bank Devisa Persepsi', 0, 0, 'R', 0);
        $this->pdf->cell(5, 5, '', 0, 0, 'R', 0);
        $this->pdf->cell(5, 5, '', 1, 0, 'R', 0);
        $this->pdf->cell(17, 5, 'Bank Persepsi', 0, 0, 'R', 0);
        $this->pdf->cell(5, 5, '', 0, 0, 'R', 0);
        $this->pdf->cell(5, 5, '', 1, 0, 'R', 0);
        $this->pdf->cell(15.5, 5, 'Pos Persepsi', 0, 0, 'R', 0);
        $this->pdf->Ln(7);

        $this->pdf->SetFont('Arial', '', 8);
        $this->pdf->cell(105, 3, '', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, '	Nama Bank/Pos', 0, 0, 'L', 0);
        $this->pdf->cell(6, 5, ':', 0, 0, 'C', 0);
        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->cell(20, 5, 'Standard Chartered Bank ' . $cbg, 0, 0, 'L', 0);

        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', '', 8);
        $this->pdf->cell(105, 3, '', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, '	Kode Bank/Pos', 0, 0, 'L', 0);
        $this->pdf->cell(6, 5, ':', 0, 0, 'C', 0);
        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->cell(20, 5, $_SESSION["fino"], 0, 0, 'L', 0);

        $nosspcpa = substr($arrSSPCP["nosspcp"], 0, 3);
        $nosspcpb = substr($arrSSPCP["nosspcp"], 3, 3);
        $nosspcpc = substr($arrSSPCP["nosspcp"], 6, 7);
        $nosspcpcomplete = $nosspcpa . "/" . $nosspcpb . "/" . $nosspcpc;

        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->cell(105.5, 5, '', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, 'Nomor SSPCP', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->cell(65, 5, $nosspcpcomplete, 0, 0, 'L', 0);
        $this->pdf->cell(79.2, 5, '', 0, 0, 'C', 0);

        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', '', 8);
        $this->pdf->cell(105.5, 5, '', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, 'Unit KPPN', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->SetFont('Arial', 'B', 7);
        /* if ($arrSSPCP["kpkn"]=="139"){
          $nmKppn = "Surabaya II";
          }else{
          $nmKppn = "Jakarta V";
          } */

        if ($_SESSION['userweb'] == 'T0002') {
            $this->pdf->cell(65, 5, $arrSSPCP["kpp"] . '		' . 'Kode' . ' : ' . $arrSSPCP["kpkn"], 0, 0, 'L', 0);
            $this->pdf->cell(79.2, 5, '', 0, 0, 'C', 0);
        } else {
            if ($nosspcpb == '0001') {
                $this->pdf->cell(65, 5, $arrSSPCP["kpp"] . '		' . 'Kode' . ' : ' . $arrSSPCP["kpkn"], 0, 0, 'L', 0);
                $this->pdf->cell(79.2, 5, '', 0, 0, 'C', 0);
            } else {
                $this->pdf->cell(65, 5, $arrSSPCP["kpp"] . '		' . 'Kode' . ' : ' . $arrSSPCP["kpkn"], 0, 0, 'L', 0);
                $this->pdf->cell(79.2, 5, '', 0, 0, 'C', 0);
            }
        }
        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', '', 8);
        $this->pdf->cell(105.5, 5, '', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, 'Tanggal', 0, 0, 'L', 0);
        $this->pdf->cell(5, 5, ':', 0, 0, 'C', 0);
        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->cell(65, 5, $arrSSPCP["tgl"], 0, 0, 'L', 0);
        $this->pdf->cell(79.2, 5, '', 0, 0, 'C', 0);

        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', 'I', 6);
        $this->pdf->cell(125.5, 5, '', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, 'Cap dan tanda tangan', 0, 0, 'C', 0);

        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', '', 7);
        $this->pdf->cell(125.5, 5, '', 0, 0, 'R', 0);
        $this->pdf->cell(40, 5, 'Nama	:	' . $arrSSPCP["petugasbank"], 0, 0, 'C', 0);

        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', 'B', 12);
        $this->pdf->cell(104.5, 4, '', 0, 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('Arial', 'B', 7);
        $this->pdf->cell(99, 1, '', 0, 0, 'R', 0);

        $this->pdf->setxy(139, 214);
        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->Ln();

        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->Ln(56);
        $this->pdf->cell(5, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(20, 5, 'NTB/NTP 		:', 0, 0, 'L', 0);
        if ($arrSSPCP["ntbresi"] <> "") {
            $this->pdf->cell(10, 5, $arrSSPCP["ntbresi"], 0, 0, 'L', 0);
        } else {
            $this->pdf->cell(10, 5, $arrSSPCP["Ntb"], 0, 0, 'L', 0);
        }
        $this->pdf->cell(61.5, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(20, 5, 'NTPN								 :', 0, 0, 'L', 0);
        //$this->pdf->cell(10,5,$arrSSPCP["ntpnresi"],0,0,'L',0);
        $this->pdf->cell(10, 5, $arrSSPCP["ntpn"], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->Rect(5.4, 277.4, 199.2, 6, 3.5, 'F');
        $this->pdf->Ln();

        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->Ln();

        // untuk header sscpc
        $this->pdf->SetY(8);
        $this->pdf->SetFont('Arial', 'B', 11);
        $this->pdf->Cell(199.2, 5, 'SURAT SETORAN', 0, 0, 'C');
        $this->pdf->Ln();
        $this->pdf->Cell(199.2, 5, ' PABEAN, CUKAI, DAN', 0, 0, 'C');
        $this->pdf->Ln();
        $this->pdf->Cell(199.2, 5, 'PAJAK (SSPCP)', 0, 0, 'C');
        $this->pdf->Ln();
        $this->pdf->Cell(199.2, 5, '', 0, 0, 'C');
        $this->pdf->Ln();
        $this->pdf->Cell(199.2, 5, '', 0, 0, 'C');

        $this->pdf->SetXY(135, 6);
        $this->pdf->SetFont('Arial', '', 8);
        $this->pdf->Cell(199.2, 4, 'Lembar ke-1		:		Wajib Bayar', 0, 0, 'L');
        $this->pdf->Ln();
        $this->pdf->SetX(135);
        $this->pdf->Cell(199.2, 4, 'Lembar ke-2		:		KPPN', 0, 0, 'L');
        $this->pdf->Ln();
        $this->pdf->SetX(135);
        $this->pdf->Cell(199.2, 4, 'Lembar ke-3		:		Kantor Bea dan Cukai', 0, 0, 'L');
        $this->pdf->Ln();
        $this->pdf->SetX(135);
        $this->pdf->Cell(199.2, 4, 'Lembar ke-4		:		Bank Devisa Persepsi / Bank', 0, 0, 'L');
        $this->pdf->Ln();
        $this->pdf->SetX(155);
        $this->pdf->Cell(199.2, 4, 'Persepsi / Pos Persepsi', 0, 0, 'L');


        $mspjkawal = substr($arrSSPCP["mspjk"], 0, 2);
        //echo $mspjkawal; exit;
        switch ($mspjkawal) {
            case '01' :
                $this->pdf->SetXY(10, 210.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
            case '02' :
                $this->pdf->SetXY(20, 210.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
            case '03' :
                $this->pdf->SetXY(30, 210.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
            case '04' :
                $this->pdf->SetXY(40, 210.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
            case '05' :
                $this->pdf->SetXY(50, 210.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
            case '06' :
                $this->pdf->SetXY(60, 210.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
            case '07' :
                $this->pdf->SetXY(70, 210.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
            case '08' :
                $this->pdf->SetXY(80, 210.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
            case '09' :
                $this->pdf->SetXY(90, 210.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
            case '10' :
                $this->pdf->SetXY(100, 210.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
            case '11' :
                $this->pdf->SetXY(110, 210.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
            case '12' :
                $this->pdf->SetXY(120, 210.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
        }

        $kodeid = trim($arrSSPCP["kodeid"]);
        //echo $kodeid; exit;
        //$kodeid = '2';
        switch ($kodeid) {
            case '2' :
                $this->pdf->SetXY(121, 33.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
            case '3' :
                $this->pdf->SetXY(163, 33.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
            case '5' :
                $this->pdf->SetXY(79, 33.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
        }

        $kodejpn = trim($arrSSPCP["jpn"]);
        //echo $kodejpn; exit;
        //$kodejpn = '4';
        switch ($kodejpn) {
            case '1' :
                $this->pdf->SetXY(79, 27.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
            case '2' :
                $this->pdf->SetXY(106, 27.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
            case '3' :
                $this->pdf->SetXY(134, 27.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
            case '4' :
                $this->pdf->SetXY(161, 27.8);
                $this->pdf->SetFont('Arial', '', 13);
                $this->pdf->Cell(2, 5, 'X', 0, 0, 'L');
                break;
        }
    }

    /** fungsi terbilang */
    function terbilang($bilangan) {
        $angka = array('0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
        $kata = array('', 'satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 'tujuh', 'delapan', 'sembilan');
        $tingkat = array('', 'ribu', 'juta', 'milyar', 'triliun');
        $panjang_bilangan = strlen($bilangan);

        if ($panjang_bilangan > 15) {
            $kalimat = "Diluar Batas";
            return $kalimat;
        }

        /* mengambil angka-angka yang ada dalam bilangan, dimasukkan ke dalam array */
        for ($i = 1; $i <= $panjang_bilangan; $i++) {
            $angka[$i] = substr($bilangan, -($i), 1);
        }

        $i = 1;
        $j = 0;
        $kalimat = "";

        /* mulai proses iterasi terhadap array angka */
        while ($i <= $panjang_bilangan) {
            $subkalimat = "";
            $kata1 = "";
            $kata2 = "";
            $kata3 = "";

            /* untuk ratusan */
            if ($angka[$i + 2] != "0") {
                if ($angka[$i + 2] == "1") {
                    $kata1 = "seratus";
                } else {
                    $kata1 = $kata[$angka[$i + 2]] . " ratus";
                }
            }

            /* untuk puluhan atau belasan */
            if ($angka[$i + 1] != "0") {
                if ($angka[$i + 1] == "1") {
                    if ($angka[$i] == "0") {
                        $kata2 = "sepuluh";
                    } elseif ($angka[$i] == "1") {
                        $kata2 = "sebelas";
                    } else {
                        $kata2 = $kata[$angka[$i]] . " belas";
                    }
                } else {
                    $kata2 = $kata[$angka[$i + 1]] . " puluh";
                }
            }

            /* untuk satuan */
            if ($angka[$i] != "0") {
                if ($angka[$i + 1] != "1") {
                    $kata3 = $kata[$angka[$i]];
                }
            }

            /* pengujian angka apakah tidak nol semua, lalu ditambahkan tingkat */
            if (($angka[$i] != "0") OR ($angka[$i + 1] != "0") OR
                    ($angka[$i + 2] != "0")) {
                $subkalimat = "$kata1 $kata2 $kata3 " . $tingkat[$j] . " ";
            }

            /* gabungkan variabel sub kalimat (untuk satu blok 3 angka) ke variabel kalimat */
            $kalimat = $subkalimat . $kalimat;
            $i = $i + 3;
            $j = $j + 1;
        }
        /* mengganti satu ribu jadi seribu jika diperlukan */
        if (($angka[5] == "0") AND ($angka[6] == "0")) {
            $kalimat = str_replace("satu ribu", "seribu", $kalimat);
        }
        return trim($kalimat);
    }

    /** fungsi set array Kode MAP */
    function setArrMAP() {
        $this->db->connect();
        // select idbayar
        $arrMap = array();
        $i = 0;
        $sql = "select idbyr, kdmap, kdmap6 from tbljnsbyrNew where show is not null order by show";
        //echo $sql; exit;
        $result = $this->db->query($sql);
        while ($result->next()) {
            $id = $result->get('idbyr');
            $map4 = $result->get('kdmap');
            $map6 = $result->get('kdmap6');
            $arrMap[$i] = array($id, $map4, $map6);
            $i++;
        }
        return $arrMap;
        $this->db->disconnect();
    }

    /** fungsi bantu untuk convert kode MAP */
    function convertMAP($jenis, $kdmapbr, $map) {
        $this->db->connect();
        if ($this->_jnsKdMap == 6) {
            $sql = "Select kdMap6 as chmap from TblJnsByrNew where kdMAP = '$kdmapbr'";
            $recmap = $this->db->query($sql);
            $sql = "";
        } else {
            $sql = "Select kdMAP as chmap from TblJnsByrNew where kdMap6 = '$kdmapbr'";
            $recmap = $this->db->query($sql);
            $sql = "";
        }
        if ($recmap->next()) {
            $kdmapnew = $recmap->get("chmap");
            return $kdmapnew;
        } else {
            return $kdmapbr;
        }
        $this->db->disconnect();
    }

    /** fungsi untuk menggambar form tanda terima */
    function drawTandaTerima($arrDokumen) {
        //print_r($arrDokumen); exit;
        $map = $arrDokumen["MAP"];
        $arrSSPCP = $arrDokumen["SSPCP"];
        $arrBea1 = $arrDokumen["BEA01"];
        $arrBea2 = $arrDokumen["BEA02"];
        $arrBea3 = $arrDokumen["BEA03"];
        $arrBea4 = $arrDokumen["BEA04"];
        $arrBea5 = $arrDokumen["BEA05"];
        $arrBea6 = $arrDokumen["BEA06"];
        $arrBea7 = $arrDokumen["BEA07"];
        $arrBea8 = $arrDokumen["BEA08"];
        $arrBea9 = $arrDokumen["BEA09"];
        $arrBea10 = $arrDokumen["BEA10"];
        $arrBea11 = $arrDokumen["BEA11"];
        $arrBea12 = $arrDokumen["BEA12"];
        $arrBea13 = $arrDokumen["BEA13"];
        $arrBea14 = $arrDokumen["BEA14"];
        $arrBea15 = $arrDokumen["BEA15"];
        $arrBea16 = $arrDokumen["BEA16"];
        $arrBea17 = $arrDokumen["BEA17"];
        $arrBea18 = $arrDokumen["BEA18"];
        $arrBea19 = $arrDokumen["BEA19"];
        $arrBea20 = $arrDokumen["BEA20"];
        $total = $arrBea1["amount"] + $arrBea2["amount"] + $arrBea3["amount"] + $arrBea4["amount"] + $arrBea5["amount"] + $arrBea6["amount"] + $arrBea7["amount"] + $arrBea8["amount"] + $arrBea9["amount"] + $arrBea10["amount"] + $arrBea11["amount"] + $arrBea12["amount"] + $arrBea13["amount"] + $arrBea14["amount"] + $arrBea15["amount"] + $arrBea16["amount"] + $arrBea17["amount"] + $arrBea18["amount"] + $arrBea19["amount"] + $arrBea20["amount"];


        $this->pdf->SetLeftMargin(10);
        $this->pdf->SetRightMargin(10);
        $this->pdf->Sety(10);
        $this->pdf->AddPage();

        $this->pdf->SetFont('arial', 'B', '10');
        $this->pdf->cell(219.2, 5, '', 0, 0, 'C', 0);


        $this->pdf->setxy(5.2, 40);
        $this->pdf->SetFont('arial', 'B', '15');

        $this->pdf->SetFont('arial', '', '8');
        $this->pdf->setxy(10, 5);
        $this->pdf->cell(0, 2, 'STANDARD CHARTERED BANK', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 5, $arrSSPCP["cabNama"], 5, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('arial', '', '12');
        $this->pdf->cell(2, 5, '', 0, 0, 'L', 0);
        if ($arrSSPCP["jpn"] == "03") {
            $byr = "CUKAI";
        } elseif ($arrSSPCP["jpn"] == "01") {
            $byr = "IMPOR";
        } elseif ($arrSSPCP["jpn"] == "02") {
            $byr = "EKSPOR";
        } else {
            $byr = "BARANG TERTENTU";
        }
        $this->pdf->cell(170.2, 5, 'BUKTI PENERIMAAN NEGARA ' . $byr, 5, 0, 'C', 0);
        $this->pdf->Ln();

        $this->pdf->cell(173.2, 5, 'KPBC : ' . $arrSSPCP["kodekpbc"] . '- ' . $arrSSPCP["namaKPBC"], 0, 0, 'C', 0);
        $this->pdf->setx(180);
        $this->pdf->SetFont('arial', '', '10');
        $this->pdf->cell(5.2, 0, $arrSSPCP["kpkn"], 0, 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('arial', 'I', '7');
        $this->pdf->setx(180);
        $this->pdf->cell(5.2, 10, 'Kode KPPN', 0, 0, 'C', 0);


        $this->pdf->Ln();
        $this->pdf->SetFont('arial', 'I', '7');
        $this->pdf->setxy(10, 30);
        $this->pdf->cell(40.2, 5, 'Identitas Transaksi', 0, 0, 'L', 0);
        $this->pdf->SetFont('arial', '', '9');
        $this->pdf->Ln();
        $this->pdf->cell(40.2, 5, 'Tanggal Setor ', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, $arrSSPCP["tglByr2"], 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, 'Transaksi Bank # ', 0, 0, 'L', 0);
        $this->pdf->cell(30, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        if ($arrSSPCP["ntbresi"] <> "") {
            $this->pdf->cell(10, 5, $arrSSPCP["ntbresi"], 0, 0, 'L', 0);
        } else {
            $this->pdf->cell(10, 5, $arrSSPCP["Ntb"], 0, 0, 'L', 0);
        }


        $this->pdf->Ln();
        $this->pdf->cell(40.2, 5, 'Tanggal Buku ', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, $arrSSPCP["tglbuku"], 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, 'Transaksi MPN # ', 0, 0, 'L', 0);
        $this->pdf->cell(30, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, '0050 ' . $arrSSPCP["tglByr2"], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40.2, 5, 'Rekening #', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, $arrSSPCP["NoRek"], 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, 'Bukti Pengesahan ', 0, 0, 'L', 0);
        $this->pdf->cell(30, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, $arrSSPCP["ntpn"], 0, 0, 'L', 0);

        $this->pdf->Ln();
        $this->pdf->SetFont('arial', 'I', '7');
        $this->pdf->setx(61);
        $this->pdf->cell(5.2, 5, 'Identitas Pelaku Transaksi Pelunasan Bea & Cukai', 0, 0, 'R', 0);
        $this->pdf->SetFont('arial', '', '9');
        $this->pdf->Ln();
        $this->pdf->cell(40.2, 5, 'Identitas ', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, $arrSSPCP["npwp"], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40.2, 5, 'Nama ', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, $arrSSPCP["nama"], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40.2, 5, 'Alamat ', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, $arrSSPCP["alamat"], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40.2, 5, 'Kota ', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, $arrSSPCP["Kota"], 0, 0, 'L', 0);

        $this->pdf->Ln();
        $this->pdf->SetFont('arial', 'I', '7');
        $this->pdf->setx(35);
        $this->pdf->cell(5.2, 5, 'Jenis dan Nomor Dokumen', 0, 0, 'R', 0);
        $this->pdf->SetFont('arial', '', '9');

        $this->pdf->Ln();
        $this->pdf->cell(40.2, 5, 'Jenis Dokumen ', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, $arrSSPCP["jpn"] . '. ' . $arrSSPCP["namadok"], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40.2, 5, 'Nomor Dokumen ', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, $arrSSPCP["car"], 0, 0, 'L', 0);
        $this->pdf->setx(150);
        $this->pdf->cell(15.2, 5, 'Tanggal ', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, $arrSSPCP["tgldokumen"], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40.2, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, '', 0, 0, 'L', 0);
        $this->pdf->SetFont('arial', '', '6');
        //$this->pdf->cell(2.2,5,"Jenis"." - "."Nomor Dokumen"." - "."Tanggal Dokumen",0,0,'L',0);

        $this->pdf->Ln();
        $this->pdf->SetFont('arial', 'I', '7');
        $this->pdf->setx(79);
        $this->pdf->cell(.2, 5, 'Rincian pembayaran untuk disetorkan ke rekening kas negara', 0, 0, 'R', 0);
        $this->pdf->SetFont('arial', '', '9');

        $this->pdf->Ln();
        //$this->pdf->cell(2,5,'',0,0,'L',0);
        $this->pdf->cell(98.2, 5, 'Bea Masuk ', 0, 0, 'L', 0);
        $this->pdf->cell(20.2, 5, '412111', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, 'Rp. ', 0, 0, 'L', 0);
        $this->pdf->setx(180);
        $this->pdf->cell(2.2, 5, number_format($arrBea1["amount"], 0, '', ','), 0, 0, 'R', 0);
        $this->pdf->Ln();
        //$this->pdf->cell(2,5,'',0,0,'L',0);
        $this->pdf->cell(98.2, 5, 'Bea Masuk Ditanggung Pemerintah atas Hibah (SPM) Nihil ', 0, 0, 'L', 0);
        $this->pdf->cell(20.2, 5, '412112', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, 'Rp. ', 0, 0, 'L', 0);
        $this->pdf->setx(180);
        $this->pdf->cell(2.2, 5, number_format($arrBea2["amount"], 0, '', ','), 0, 0, 'R', 0);
        $this->pdf->Ln();
        //$this->pdf->cell(2,5,'',0,0,'L',0);
        $this->pdf->cell(98.2, 5, 'Bea Masuk Dalam Rangka Kemudahan Impor Tujuan Ekspor (KITE) ', 0, 0, 'L', 0);
        $this->pdf->cell(20.2, 5, '412114', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, 'Rp. ', 0, 0, 'L', 0);
        $this->pdf->setx(180);
        $this->pdf->cell(2.2, 5, number_format($arrBea3["amount"], 0, '', ','), 0, 0, 'R', 0);
        $this->pdf->Ln();
        //$this->pdf->cell(2,5,'',0,0,'L',0);
        $this->pdf->cell(98.2, 5, 'Denda Administrasi Pabean ', 0, 0, 'L', 0);
        $this->pdf->cell(20.2, 5, '412113', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, 'Rp. ', 0, 0, 'L', 0);
        $this->pdf->setx(180);
        $this->pdf->cell(2.2, 5, number_format($arrBea4["amount"], 0, '', ','), 0, 0, 'R', 0);
        $this->pdf->Ln();
        //$this->pdf->cell(2,5,'',0,0,'L',0);
        $this->pdf->cell(98.2, 5, 'Pendapatan Pabean Lainnya ', 0, 0, 'L', 0);
        $this->pdf->cell(20.2, 5, '412119', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, 'Rp. ', 0, 0, 'L', 0);
        $this->pdf->setx(180);
        $this->pdf->cell(2.2, 5, number_format($arrBea6["amount"], 0, '', ','), 0, 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->Ln();
        //$this->pdf->cell(2,5,'',0,0,'L',0);
        $this->pdf->cell(98.2, 5, 'Cukai Hasil Tembakau ', 0, 0, 'L', 0);
        $this->pdf->cell(20.2, 5, '411511', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, 'Rp. ', 0, 0, 'L', 0);
        $this->pdf->setx(180);
        $this->pdf->cell(2.2, 5, number_format($arrBea10["amount"], 0, '', ','), 0, 0, 'R', 0);
        $this->pdf->Ln();
        //$this->pdf->cell(2,5,'',0,0,'L',0);
        $this->pdf->cell(98.2, 5, 'Cukai Etil Alkohol ', 0, 0, 'L', 0);
        $this->pdf->cell(20.2, 5, '411512', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, 'Rp. ', 0, 0, 'L', 0);
        $this->pdf->setx(180);
        $this->pdf->cell(2.2, 5, number_format($arrBea11["amount"], 0, '', ','), 0, 0, 'R', 0);
        $this->pdf->Ln();
        //$this->pdf->cell(2,5,'',0,0,'L',0);
        $this->pdf->cell(98.2, 5, 'Cukai Minuman Mengandung Etil Alkohol ', 0, 0, 'L', 0);
        $this->pdf->cell(20.2, 5, '411513', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, 'Rp. ', 0, 0, 'L', 0);
        $this->pdf->setx(180);
        $this->pdf->cell(2.2, 5, number_format($arrBea12["amount"], 0, '', ','), 0, 0, 'R', 0);
        $this->pdf->Ln();
        //$this->pdf->cell(2,5,'',0,0,'L',0);
        $this->pdf->cell(98.2, 5, 'Pendapatan Cukai Lainnya ', 0, 0, 'L', 0);
        $this->pdf->cell(20.2, 5, '411519', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, 'Rp. ', 0, 0, 'L', 0);
        $this->pdf->setx(180);
        $this->pdf->cell(2.2, 5, number_format($arrBea13["amount"], 0, '', ','), 0, 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->Ln();
        //$this->pdf->cell(2,5,'',0,0,'L',0);
        $this->pdf->cell(98.2, 5, 'Pendapatan DJBC ', 0, 0, 'L', 0);
        $this->pdf->cell(20.2, 5, '423216', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, 'Rp. ', 0, 0, 'L', 0);
        $this->pdf->setx(180);
        $this->pdf->cell(2.2, 5, number_format($arrBea15["amount"], 0, '', ','), 0, 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->Ln();

        //$this->pdf->cell(2,5,'',0,0,'L',0);
        $this->pdf->cell(32.2, 5, 'PPN Impor ', 0, 0, 'L', 0);
        $this->pdf->cell(34.2, 5, '- NPWP ', 0, 0, 'L', 0);
        if (number_format($arrBea16["amount"], 0, '', ',') <> 0) {
            $this->pdf->cell(32.2, 5, $arrSSPCP["npwpa"], 0, 0, 'L', 0);
        } else {
            $this->pdf->cell(32.2, 5, '', 0, 0, 'L', 0);
        }
        $this->pdf->cell(20.2, 5, '411212', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, 'Rp. ', 0, 0, 'L', 0);
        $this->pdf->setx(180);
        $this->pdf->cell(2.2, 5, number_format($arrBea16["amount"], 0, '', ','), 0, 0, 'R', 0);
        $this->pdf->Ln();
        //$this->pdf->cell(2,5,'',0,0,'L',0);
        $this->pdf->cell(32.2, 5, 'PPnBM Impor ', 0, 0, 'L', 0);
        $this->pdf->cell(34.2, 5, '- NPWP ', 0, 0, 'L', 0);
        if (number_format($arrBea18["amount"], 0, '', ',') <> 0) {
            $this->pdf->cell(32.2, 5, $arrSSPCP["npwpb"], 0, 0, 'L', 0);
        } else {
            $this->pdf->cell(32.2, 5, '', 0, 0, 'L', 0);
        }
        $this->pdf->cell(20.2, 5, '411222', 0, 0, 'L', 0);
        $this->pdf->cell(2.4, 5, 'Rp. ', 0, 0, 'L', 0);
        $this->pdf->setx(180);
        $this->pdf->cell(2.2, 5, number_format($arrBea18["amount"], 0, '', ','), 0, 0, 'R', 0);
        $this->pdf->Ln();
        //	$this->pdf->cell(2,5,'',0,0,'L',0);
        $this->pdf->cell(32.2, 5, 'PPh Pasal 22 Impor ', 0, 0, 'L', 0);
        $this->pdf->cell(34.2, 5, '- NPWP ', 0, 0, 'L', 0);
        if (number_format($arrBea19["amount"], 0, '', ',') <> 0) {
            $this->pdf->cell(32.2, 5, $arrSSPCP["npwpc"], 0, 0, 'L', 0);
        } else {
            $this->pdf->cell(32.2, 5, '', 0, 0, 'L', 0);
        }
        $this->pdf->cell(20.2, 5, '411123', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, 'Rp. ', 0, 0, 'L', 0);
        $this->pdf->setx(180);
        $this->pdf->cell(2.2, 5, number_format($arrBea19["amount"], 0, '', ','), 0, 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->Ln();
        //$this->pdf->cell(2,5,'',0,0,'L',0);
        $this->pdf->cell(98.6, 5, 'Bunga Penagihan PPN', 0, 0, 'L', 0);
        $this->pdf->cell(20.2, 5, '411622', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, 'Rp. ', 0, 0, 'L', 0);
        $this->pdf->setx(180);
        $this->pdf->cell(2.2, 5, number_format($arrBea20["amount"], 0, '', ','), 0, 0, 'R', 0);

        $this->pdf->Ln();
        $this->pdf->SetFont('arial', '', '9');
        $this->pdf->Ln();
        $this->pdf->Setx(10);
        $this->pdf->cell(96.6, 5, 'TOTAL', 0, 0, 'L', 0);
        $this->pdf->cell(22.2, 5, '', 0, 0, 'L', 0);
        $this->pdf->SetFont('arial', '', '9');
        $this->pdf->cell(2.2, 5, 'Rp. ', 0, 0, 'L', 0);
        $this->pdf->setx(180);
        //$this->pdf->cell(2.2,5,number_format($total,0,'',','),0,0,'R',0);
        $this->pdf->cell(2.2, 5, number_format($arrSSPCP["total"], 0, '', ','), 0, 0, 'R', 0);

        $rupiah = $this->terbilang(($arrSSPCP["total"])) . ' rupiah';
        if (strlen($rupiah) > 100) {
            for ($i = 100; $i > 0; $i--) {
                $sub = substr($rupiah, $i, 1);
                if ($sub == " ") {
                    $potong = $i;
                    break;
                }
            }
            $rup1 = substr($rupiah, 0, $potong);
            $rup2 = substr($rupiah, $potong + 1);
        } else {
            $rup1 = $rupiah;
            $rup2 = "";
        }

        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->Setx(10);
        $this->pdf->cell(16.6, 5, 'Terbilang ', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('arial', 'I', '9');
        $this->pdf->Setx(10);
        $this->pdf->cell(2.2, 5, $rup1, 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(30, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, $rup2, 0, 0, 'L', 0);

        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->SetFont('arial', 'I', '7');
        $this->pdf->Setx(10);
        $this->pdf->cell(16.6, 5, 'Rekening Kas Negara dan Pengesahan Bank ', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('arial', '', '9');
        $this->pdf->Setx(10);
        $this->pdf->cell(16.6, 5, "ID > " . $arrSSPCP["npwp"] . " > NAMA > " . trim($arrSSPCP["nama"]) . " > NTPN > " . $arrSSPCP["ntpn"] . " > P11 >" . $arrSSPCP["STAN"] . "> P37 >", 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->Setx(10);
        $this->pdf->cell(16.6, 5, $arrSSPCP["Ntb"] . " > KPPN > " . $arrSSPCP["kpkn"] . " > BANK > " . '0050' . $arrSSPCP["HdBranchNo"] . " > P7 >" . str_replace("/", "", substr($arrSSPCP["tglByr2"], 0, 5)) . str_replace(":", "", $arrSSPCP["tglByr"]) . " > P12 > " . str_replace(":", "", $arrSSPCP["tglByr"]) . " > P15 > " . str_replace("/", "", substr($arrSSPCP["tglbuku"], 0, 5)) . " > KPBC >" . $arrSSPCP["kodekpbc"], 0, 0, 'L', 0);
    }

    function drawDebitAdvice($arrDokumen) {
        //print_r($arrDokumen); exit;
        $map = $arrDokumen["MAP"];
        $arrSSPCP = $arrDokumen["SSPCP"];
        $arrBea1 = $arrDokumen["BEA01"];
        $arrBea2 = $arrDokumen["BEA02"];
        $arrBea3 = $arrDokumen["BEA03"];
        $arrBea4 = $arrDokumen["BEA04"];
        $arrBea5 = $arrDokumen["BEA05"];
        $arrBea6 = $arrDokumen["BEA06"];
        $arrBea7 = $arrDokumen["BEA07"];
        $arrBea8 = $arrDokumen["BEA08"];
        $arrBea9 = $arrDokumen["BEA09"];
        $arrBea10 = $arrDokumen["BEA10"];
        $arrBea11 = $arrDokumen["BEA11"];
        $arrBea12 = $arrDokumen["BEA12"];
        $arrBea13 = $arrDokumen["BEA13"];
        $arrBea14 = $arrDokumen["BEA14"];
        $arrBea15 = $arrDokumen["BEA15"];
        $arrBea16 = $arrDokumen["BEA16"];
        $arrBea17 = $arrDokumen["BEA17"];
        $arrBea18 = $arrDokumen["BEA18"];
        $arrBea19 = $arrDokumen["BEA19"];
        $arrBea20 = $arrDokumen["BEA20"];
        $total = $arrBea1["amount"] + $arrBea2["amount"] + $arrBea3["amount"] + $arrBea4["amount"] + $arrBea5["amount"] + $arrBea6["amount"] + $arrBea7["amount"] + $arrBea8["amount"] + $arrBea9["amount"] + $arrBea10["amount"] + $arrBea11["amount"] + $arrBea12["amount"] + $arrBea13["amount"] + $arrBea14["amount"] + $arrBea15["amount"] + $arrBea16["amount"] + $arrBea17["amount"] + $arrBea18["amount"] + $arrBea19["amount"] + $arrBea20["amount"];


        $this->pdf->SetLeftMargin(10);
        $this->pdf->SetRightMargin(10);
        $this->pdf->Sety(10);
        $this->pdf->AddPage();

        $this->pdf->SetFont('arial', 'B', '10');
        $this->pdf->cell(219.2, 5, '', 0, 0, 'C', 0);


        $this->pdf->setxy(5.2, 40);
        $this->pdf->SetFont('arial', 'B', '15');

        $this->pdf->SetFont('arial', '', '8');
        $this->pdf->setxy(10, 5);
        $this->pdf->cell(0, 2, 'STANDARD CHARTERED BANK', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(0, 5, $arrSSPCP["cabNama"], 5, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('arial', 'B', '12');
        $this->pdf->cell(2, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(170.2, 5, 'DEBIT ADVICE', 5, 0, 'C', 0);
        $this->pdf->Ln();

        if ($arrSSPCP["jpn"] == "03") {
            $byr = "CUKAI";
        } elseif ($arrSSPCP["jpn"] == "01") {
            $byr = "IMPOR";
        } elseif ($arrSSPCP["jpn"] == "02") {
            $byr = "EKSPOR";
        } else {
            $byr = "BARANG TERTENTU";
        }

        $this->pdf->SetFont('arial', '', '11');
        $this->pdf->cell(173.2, 5, 'SETORAN PABEAN, CUKAI, DAN PAJAK (SSPCP) ' . $byr, 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->setx(10);
        $this->pdf->cell(173.2, 5, 'KPBC : ' . $arrSSPCP["kodekpbc"] . '- ' . $arrSSPCP["namaKPBC"], 0, 0, 'C', 0);
        $this->pdf->setx(180);
        $this->pdf->SetFont('arial', '', '10');
        $this->pdf->cell(5.2, 0, $arrSSPCP["kpkn"], 0, 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('arial', 'I', '7');
        $this->pdf->setx(180);
        $this->pdf->cell(5.2, 10, 'Kode KPPN', 0, 0, 'C', 0);


        $this->pdf->Ln();
        $this->pdf->SetFont('arial', 'I', '7');
        $this->pdf->setxy(11, 30);
        $this->pdf->cell(40.2, 5, 'Identitas Transaksi', 0, 0, 'L', 0);
        $this->pdf->SetFont('arial', '', '9');
        $this->pdf->Ln();
        $this->pdf->cell(40.2, 5, 'Tanggal Setor ', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, $arrSSPCP["tglByr2"], 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, 'Transaksi Bank # ', 0, 0, 'L', 0);
        $this->pdf->cell(30, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(2, 5, ':', 0, 0, 'L', 0);
        if ($arrSSPCP["ntbresi"] <> "") {
            $this->pdf->cell(10, 5, $arrSSPCP["ntbresi"], 0, 0, 'L', 0);
        } else {
            $this->pdf->cell(10, 5, $arrSSPCP["Ntb"], 0, 0, 'L', 0);
        }

        $this->pdf->Ln();
        $this->pdf->cell(40.2, 5, 'Tanggal Buku ', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, $arrSSPCP["tglbuku"], 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, 'Transaksi MPN # ', 0, 0, 'L', 0);
        $this->pdf->cell(30, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, '0050 ' . $arrSSPCP["tglByr2"], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40.2, 5, 'Rekening ', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, $arrSSPCP["NoRek"], 0, 0, 'L', 0);
        $this->pdf->cell(55, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, 'Bukti Pengesahan ', 0, 0, 'L', 0);
        $this->pdf->cell(30, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, $arrSSPCP["ntpn"], 0, 0, 'L', 0);

        $this->pdf->Ln();
        $this->pdf->SetFont('arial', 'I', '7');
        $this->pdf->setx(62);
        $this->pdf->cell(5.2, 5, 'Identitas Pelaku Transaksi Pelunasan Bea & Cukai', 0, 0, 'R', 0);
        $this->pdf->SetFont('arial', '', '9');
        $this->pdf->Ln();
        $this->pdf->cell(40.2, 5, 'Identitas ', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, $arrSSPCP["npwp"], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40.2, 5, 'Nama ', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, $arrSSPCP["nama"], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40.2, 5, 'Alamat ', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, $arrSSPCP["alamat"], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40.2, 5, 'Kota ', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, $arrSSPCP["Kota"], 0, 0, 'L', 0);

        $this->pdf->Ln();
        $this->pdf->SetFont('arial', 'I', '7');
        $this->pdf->setx(58);
        $this->pdf->cell(5.2, 5, 'Jenis dan Nomor Dokumen Dasar Pembayaran', 0, 0, 'R', 0);
        $this->pdf->SetFont('arial', '', '9');
        $this->pdf->Ln();
        $this->pdf->cell(40.2, 5, 'NO SSPCP ', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, $arrSSPCP["nosspcp"], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40.2, 5, 'Jenis Dokumen ', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, $arrSSPCP["jpn"] . '. ' . $arrSSPCP["namadok"], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40.2, 5, 'Nomor Dokumen ', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, $arrSSPCP["car"], 0, 0, 'L', 0);
        $this->pdf->setx(150);
        $this->pdf->cell(15.2, 5, 'Tanggal ', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, $arrSSPCP["tgldokumen"], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40.2, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, '', 0, 0, 'L', 0);
        $this->pdf->SetFont('arial', '', '6');
        //$this->pdf->cell(2.2,5,"Jenis"." - "."Nomor Dokumen"." - "."Tanggal Dokumen",0,0,'L',0);

        $this->pdf->Ln();
        $this->pdf->SetFont('arial', 'I', '7');
        $this->pdf->setx(79);
        $this->pdf->cell(.2, 5, 'Rincian pembayaran untuk disetorkan ke rekening kas negara', 0, 0, 'R', 0);
        $this->pdf->SetFont('arial', '', '9');

        $this->pdf->Ln();
        //$this->pdf->cell(2,5,'',0,0,'L',0);
        $this->pdf->cell(98.2, 5, 'Bea Masuk ', 0, 0, 'L', 0);
        $this->pdf->cell(20.2, 5, '412111', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, 'Rp. ', 0, 0, 'L', 0);
        $this->pdf->setx(180);
        $this->pdf->cell(2.2, 5, number_format($arrBea1["amount"], 0, '', ','), 0, 0, 'R', 0);
        $this->pdf->Ln();
        //$this->pdf->cell(2,5,'',0,0,'L',0);
        $this->pdf->cell(98.2, 5, 'Bea Masuk Ditanggung Pemerintah atas Hibah (SPM) Nihil ', 0, 0, 'L', 0);
        $this->pdf->cell(20.2, 5, '412112', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, 'Rp. ', 0, 0, 'L', 0);
        $this->pdf->setx(180);
        $this->pdf->cell(2.2, 5, number_format($arrBea2["amount"], 0, '', ','), 0, 0, 'R', 0);
        $this->pdf->Ln();
        //$this->pdf->cell(2,5,'',0,0,'L',0);
        $this->pdf->cell(98.2, 5, 'Bea Masuk Dalam Rangka Kemudahan Impor Tujuan Ekspor (KITE) ', 0, 0, 'L', 0);
        $this->pdf->cell(20.2, 5, '412114', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, 'Rp. ', 0, 0, 'L', 0);
        $this->pdf->setx(180);
        $this->pdf->cell(2.2, 5, number_format($arrBea3["amount"], 0, '', ','), 0, 0, 'R', 0);
        $this->pdf->Ln();
        //$this->pdf->cell(2,5,'',0,0,'L',0);
        $this->pdf->cell(98.2, 5, 'Denda Administrasi Pabean ', 0, 0, 'L', 0);
        $this->pdf->cell(20.2, 5, '412113', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, 'Rp. ', 0, 0, 'L', 0);
        $this->pdf->setx(180);
        $this->pdf->cell(2.2, 5, number_format($arrBea4["amount"], 0, '', ','), 0, 0, 'R', 0);
        $this->pdf->Ln();
        //$this->pdf->cell(2,5,'',0,0,'L',0);
        $this->pdf->cell(98.2, 5, 'Pendapatan Pabean Lainnya ', 0, 0, 'L', 0);
        $this->pdf->cell(20.2, 5, '412119', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, 'Rp. ', 0, 0, 'L', 0);
        $this->pdf->setx(180);
        $this->pdf->cell(2.2, 5, number_format($arrBea6["amount"], 0, '', ','), 0, 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->Ln();
        //$this->pdf->cell(2,5,'',0,0,'L',0);
        $this->pdf->cell(98.2, 5, 'Cukai Hasil Tembakau ', 0, 0, 'L', 0);
        $this->pdf->cell(20.2, 5, '411511', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, 'Rp. ', 0, 0, 'L', 0);
        $this->pdf->setx(180);
        $this->pdf->cell(2.2, 5, number_format($arrBea10["amount"], 0, '', ','), 0, 0, 'R', 0);
        $this->pdf->Ln();
        //$this->pdf->cell(2,5,'',0,0,'L',0);
        $this->pdf->cell(98.2, 5, 'Cukai Etil Alkohol ', 0, 0, 'L', 0);
        $this->pdf->cell(20.2, 5, '411512', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, 'Rp. ', 0, 0, 'L', 0);
        $this->pdf->setx(180);
        $this->pdf->cell(2.2, 5, number_format($arrBea11["amount"], 0, '', ','), 0, 0, 'R', 0);
        $this->pdf->Ln();
        //$this->pdf->cell(2,5,'',0,0,'L',0);
        $this->pdf->cell(98.2, 5, 'Cukai Minuman Mengandung Etil Alkohol ', 0, 0, 'L', 0);
        $this->pdf->cell(20.2, 5, '411513', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, 'Rp. ', 0, 0, 'L', 0);
        $this->pdf->setx(180);
        $this->pdf->cell(2.2, 5, number_format($arrBea12["amount"], 0, '', ','), 0, 0, 'R', 0);
        $this->pdf->Ln();
        //$this->pdf->cell(2,5,'',0,0,'L',0);
        $this->pdf->cell(98.2, 5, 'Pendapatan Cukai Lainnya ', 0, 0, 'L', 0);
        $this->pdf->cell(20.2, 5, '411519', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, 'Rp. ', 0, 0, 'L', 0);
        $this->pdf->setx(180);
        $this->pdf->cell(2.2, 5, number_format($arrBea13["amount"], 0, '', ','), 0, 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->Ln();
        //$this->pdf->cell(2,5,'',0,0,'L',0);
        $this->pdf->cell(98.2, 5, 'Pendapatan DJBC ', 0, 0, 'L', 0);
        $this->pdf->cell(20.2, 5, '423216', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, 'Rp. ', 0, 0, 'L', 0);
        $this->pdf->setx(180);
        $this->pdf->cell(2.2, 5, number_format($arrBea15["amount"], 0, '', ','), 0, 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->Ln();

        //$this->pdf->cell(2,5,'',0,0,'L',0);
        $this->pdf->cell(45.2, 5, 'PPN Impor ', 0, 0, 'L', 0);
        $this->pdf->cell(21.2, 5, ' NPWP ', 0, 0, 'L', 0);
        if (number_format($arrBea16["amount"], 0, '', ',') <> 0) {
            $this->pdf->cell(32.2, 5, $arrSSPCP["npwpa"], 0, 0, 'L', 0);
        } else {
            $this->pdf->cell(32.2, 5, '', 0, 0, 'L', 0);
        }
        $this->pdf->cell(20.2, 5, '411212', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, 'Rp. ', 0, 0, 'L', 0);
        $this->pdf->setx(180);
        $this->pdf->cell(2.2, 5, number_format($arrBea16["amount"], 0, '', ','), 0, 0, 'R', 0);
        $this->pdf->Ln();
        //$this->pdf->cell(2,5,'',0,0,'L',0);
        $this->pdf->cell(45.2, 5, 'PPnBM Impor ', 0, 0, 'L', 0);
        $this->pdf->cell(21.2, 5, ' NPWP ', 0, 0, 'L', 0);
        if (number_format($arrBea18["amount"], 0, '', ',') <> 0) {
            $this->pdf->cell(32.2, 5, $arrSSPCP["npwpb"], 0, 0, 'L', 0);
        } else {
            $this->pdf->cell(32.2, 5, '', 0, 0, 'L', 0);
        }
        $this->pdf->cell(20.2, 5, '411222', 0, 0, 'L', 0);
        $this->pdf->cell(2.4, 5, 'Rp. ', 0, 0, 'L', 0);
        $this->pdf->setx(180);
        $this->pdf->cell(2.2, 5, number_format($arrBea18["amount"], 0, '', ','), 0, 0, 'R', 0);
        $this->pdf->Ln();
        //	$this->pdf->cell(2,5,'',0,0,'L',0);
        $this->pdf->cell(45.2, 5, 'PPh Pasal 22 Impor ', 0, 0, 'L', 0);
        $this->pdf->cell(21.2, 5, ' NPWP ', 0, 0, 'L', 0);
        if (number_format($arrBea19["amount"], 0, '', ',') <> 0) {
            $this->pdf->cell(32.2, 5, $arrSSPCP["npwpc"], 0, 0, 'L', 0);
        } else {
            $this->pdf->cell(32.2, 5, '', 0, 0, 'L', 0);
        }
        $this->pdf->cell(20.2, 5, '411123', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, 'Rp. ', 0, 0, 'L', 0);
        $this->pdf->setx(180);
        $this->pdf->cell(2.2, 5, number_format($arrBea19["amount"], 0, '', ','), 0, 0, 'R', 0);
        $this->pdf->Ln();
        $this->pdf->Ln();
        //$this->pdf->cell(2,5,'',0,0,'L',0);
        $this->pdf->cell(98.6, 5, 'Bunga Penagihan PPN', 0, 0, 'L', 0);
        $this->pdf->cell(20.2, 5, '411622', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, 'Rp. ', 0, 0, 'L', 0);
        $this->pdf->setx(180);
        $this->pdf->cell(2.2, 5, number_format($arrBea20["amount"], 0, '', ','), 0, 0, 'R', 0);

        $this->pdf->Ln();
        $this->pdf->SetFont('arial', '', '9');
        $this->pdf->Ln();
        $this->pdf->Setx(10);
        $this->pdf->cell(96.6, 5, 'TOTAL', 0, 0, 'L', 0);
        $this->pdf->cell(22.2, 5, '', 0, 0, 'L', 0);
        $this->pdf->SetFont('arial', '', '9');
        $this->pdf->cell(2.2, 5, 'Rp. ', 0, 0, 'L', 0);
        $this->pdf->setx(180);
        //$this->pdf->cell(2.2,5,number_format($total,0,'',','),0,0,'R',0);
        $this->pdf->cell(2.2, 5, number_format($arrSSPCP["total"], 0, '', ','), 0, 0, 'R', 0);

        $rupiah = $this->terbilang(($arrSSPCP["total"])) . ' rupiah';
        if (strlen($rupiah) > 100) {
            for ($i = 100; $i > 0; $i--) {
                $sub = substr($rupiah, $i, 1);
                if ($sub == " ") {
                    $potong = $i;
                    break;
                }
            }
            $rup1 = substr($rupiah, 0, $potong);
            $rup2 = substr($rupiah, $potong + 1);
        } else {
            $rup1 = $rupiah;
            $rup2 = "";
        }

        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->Setx(10);
        $this->pdf->cell(16.6, 5, 'Terbilang ', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('arial', '', '9');
        $this->pdf->Setx(10);
        $this->pdf->cell(2.2, 5, $rup1, 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(5, 5, '', 0, 0, 'C', 0);
        $this->pdf->cell(30, 5, '', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, $rup2, 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40.2, 5, 'NPWP Penyetor ', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, $arrSSPCP["npwp_p"], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(40.2, 5, 'Nama Penyetor ', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, ':', 0, 0, 'L', 0);
        $this->pdf->cell(2.2, 5, $arrSSPCP["nama_p"], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->cell(40.2, 5, 'We have debited the above transaction from your account Number ' . $arrSSPCP["NoRekCus"], 0, 0, 'L', 0);
        $this->pdf->Ln();
        //
        //alternate 1
        //$this->pdf->cell(40.2,5,'with additional commision of Rp. '.$arrSSPCP["komisi"],0,0,'L',0);
        //alternate 2 masih sementara di production
        $this->pdf->cell(40.2, 5, 'with additional commision of ' . $arrSSPCP["matauang"] . ' ' . $arrSSPCP["komisi"] . ' (indicative exchange rate at ' . number_format($arrSSPCP["Rate"], 0, '', ',') . ')', 0, 0, 'L', 0);

        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->SetFont('arial', 'I', '7');
        $this->pdf->Setx(10);
        $this->pdf->cell(16.6, 5, 'Rekening Kas Negara dan Pengesahan Bank ', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetFont('arial', '', '9');
        $this->pdf->Setx(10);
        $this->pdf->cell(16.6, 5, "ID > " . $arrSSPCP["npwp"] . " > NAMA > " . trim($arrSSPCP["nama"]) . " > NTPN > " . $arrSSPCP["ntpn"] . " > P11 >" . $arrSSPCP["STAN"] . "> P37 >", 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->Setx(10);
        $this->pdf->cell(16.6, 5, $arrSSPCP["Ntb"] . " > KPPN > " . $arrSSPCP["kpkn"] . " > BANK > " . '0050' . $arrSSPCP["HdBranchNo"] . " > P7 >" . str_replace("/", "", substr($arrSSPCP["tglByr2"], 0, 5)) . str_replace(":", "", $arrSSPCP["tglByr"]) . " > P12 > " . str_replace(":", "", $arrSSPCP["tglByr2"]) . " > P15 > " . str_replace("/", "", substr($arrSSPCP["tglbuku"], 0, 5)) . " > KPBC >" . $arrSSPCP["kodekpbc"], 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->cell(16.6, 5, "This is computer generated no signature required", 0, 0, 'L', 0);
    }

    function getJumlahDetail() {
        $SQL = "select count(serial) as jmdtl from tblpibdtl where car = '$this->CAR'";
        //die($SQL);
        $data = $this->db->query($SQL);
        if ($data->next()) {
            $this->jmdtl = $data->get("JMDTL");
        }
    }

    function getJumlahKms() {
        $SQL = "select count(car) as jmkms from tblpibkms where car = '$this->CAR'";
        $data = $this->db->query($SQL);
        if ($data->next()) {
            $this->jmkms = $data->get("JMKMS");
        }
    }

    function getJumlahCon() {
        $SQL = "select count(car) as jmcon from tblpibcon where car = '$this->CAR'";
        $data = $this->db->query($SQL);
        if ($data->next()) {
            $this->jmcon = $data->get("JMCON");
        }
    }

    function getJumlahDok() {
        $SQL = "select A.DOKKD from tblpibdok A WHERE A.car = '$this->CAR'";
        $data = $this->db->query($SQL);
        $i = 0;
        $jmlAWB = 0;
        $jmlFas = 0;
        $jmlInv = 0;
        $jmlLC = 0;
        while ($data->next()) {
            $dokkd_lanjutan = $data->get("DOKKD");
            if (in_array($dokkd_lanjutan, array(704, 705, 740, 741))) {
                $jmlAWB++;
                if ($jmlAWB > 2) {
                    $i = $i + 1;
                }
            } elseif (in_array($dokkd_lanjutan, array(814, 815, 851, 853, 911, 993, 998, 861))) {
                $jmlFas++;
                if ($jmlFas > 1) {
                    $i = $i + 1;
                }
            } elseif ($dokkd_lanjutan == '380') {
                $jmlInv++;
                if ($jmlInv > 3) {
                    $i = $i + 1;
                }
            } elseif ($dokkd_lanjutan == '465') {
                $jmlLC++;
                if ($jmlLC > 2) {
                    $i = $i + 1;
                }
            } else {
                $i = $i + 1;
            }
        }
        $this->jmdok = $i;
    }

    function printLembarBarang($nb) {
        $this->printForm("barang", $nb, " LANJUTAN");
        $this->printForm_Footer();
        $this->pdf->SetY(35);
        // get barang - fasilitas
        $SQL = "SELECT  tbl.serial, tbl.nohs, tbl.seritrp, tbl.brgurai, tbl.merk, tbl.tipe, tbl.spflain,
                        tbl.brgasal, tbl.dcif, tbl.kdsat, tbl.jmlsat, tbl.kemasjn, tbl.kemasjm,
       			tbl.satbmjm, tbl.satcukjm, tbl.nettodtl, tbl.kdfasdtl
  		FROM    tblpibdtl tbl
 		WHERE (tbl.car = '$this->CAR') 
                ORDER BY tbl.serial asc";
        //echo 'sql'.$SQL; die();
        $jum = 0;
        $data = $this->db->query($SQL);
        while ($data->next()) {
            $jum++;
        }

        $data = $this->db->query($SQL);
        $this->pdf->Ln();
        $i = 1;
        $noHal = 0;
        while ($data->next()) {
            $serial = $data->get("SERIAL");
            // get PIB fasilitas
            $SQL = "select kdfasbm,fasbm,kdfascuk,fascuk,kdfasppn,fasppn,kdfaspph,faspph,kdfaspbm,faspbm from tblpibfas where serial = '$serial' and car = '$this->CAR'";
            $datapibfasilitas = $this->db->query($SQL);
            if ($datapibfasilitas->next()) {
                $kdfasbm = $datapibfasilitas->get("KDFASBM");
                $fasbm = $datapibfasilitas->get("FASBM");
                $kdfascuk = $datapibfasilitas->get("KDFASCUK");
                $fascuk = $datapibfasilitas->get("FASCUK");
                $kdfasppn = $datapibfasilitas->get("KDFASPPN");
                $fasppn = $datapibfasilitas->get("FASPPN");
                $kdfaspph = $datapibfasilitas->get("KDFASPPH");
                $faspph = $datapibfasilitas->get("FASPPH");
                $kdfaspbm = $datapibfasilitas->get("KDFASPBM");
                $faspbm = $datapibfasilitas->get("FASPBM");
            }
            $nohs = $data->get("NOHS");
            $seritrp = $data->get("SERITRP");
            $brgurai = $data->get("BRGURAI");
            $merk = $data->get("MERK");
            $tipe = $data->get("TIPE");
            $spflain = $data->get("SPFLAIN");
            $kemasjm = $data->get("KEMASJM");
            $nettodtl = $data->get("NETTODTL");
            $kemasjn = $data->get("KEMASJN");
            // get Jenis Kemasan
            $SQL = "select * from tblkemasan where kdedi = '$kemasjn'";
            $datakemasan = $this->db->query($SQL);
            if ($datakemasan->next()) {
                $kemasjn_nama = $datakemasan->get("UREDI");
            }

            $kdfasdtl = $data->get("KDFASDTL");
            // get Detail Fasilitas
            $SQL = "select * from tbltabel where kdrec = '$kdfasdtl' and kdtab = '75'";
            $datafasilitas = $this->db->query($SQL);
            if ($datafasilitas->next()) {
                $kdfasdtl_nama = $datafasilitas->get("URAIAN");
            } else {
                $kdfasdtl_nama = "";
            }

            $brgasal = $data->get("BRGASAL");

            // get Barang Asal
            $SQL = "select * from tblnegara where kdedi = '$brgasal'";
            $dataset = $this->db->query($SQL);
            if ($dataset->next()) {
                $brgasal_nama = $dataset->get("UREDI");
            }
            $jmlsat = $data->get("JMLSAT");
            $kdsat = $data->get("KDSAT");

            // get Kode Satuan
            $SQL = "select * from tblsatuan where kdedi = '$kdsat'";
            $datasatuan = $this->db->query($SQL);
            if ($datasatuan->next()) {
                $kdsat_nama = $datasatuan->get("UREDI");
            }
            $dcif = $data->get("dcif");
            // get tarif
            $SQL = "select * from tblpibtrf where car = '$this->CAR' and seritrp = '$seritrp' and nohs = '$nohs'";
            $datatarif = $this->db->query($SQL);
            if ($datatarif->next()) {
                $kdtrpbm = $datatarif->get("KDTRPBM");
                //$kdsatbm = $datatarif->get("KDSATBM");
                $trpbm = $datatarif->get("TRPBM");
                $kdcuk = $datatarif->get("KDCUK");
                $kdtrpcuk = $datatarif->get("KDTRPCUK");
                $kdsatcuk = $datatarif->get("KDSATCUK");
                $trpcuk = $datatarif->get("TRPCUK");
                $trpppn = $datatarif->get("TRPPPN");
                $trppbm = $datatarif->get("TRPPBM");
            }

            // menghitung jumlah baris rec
            // mengurangi counter baris
            // jika counter baris < 0 maka printForm("dokumen") dan reset counter baris
            $no = $no + 1;
            $this->printData_BRG($nohs, $brgurai, $merk, $tipe, $spflain, $kemasjm, $kemasjn, $kemasjn_nama, $kdfasdtl, $kdfasdtl_nama, $brgasal, $brgasal_nama, $jmlsat, $kdsat, $kdsat_nama, $nettodtl, $dcif, $kdfasbm, $kdfasbm_nama, $kodesatbm, $trpbm, $satbmjm, $fasbm, $kdfascuk, $kdfascuk_nama, $trpcuk, $kdfascuk_nama, $fascuk, $kdsat, $kdsat_nama, $trpppn, $kdfasppn, $fasppn, $kdfasbm, $trppph, $kdfaspph, $faspph, $trpppn, $kdfasppn, $fasppn, $no, $trppbm, $kdfaspbm, $faspbm);

            if ($i % 9 == 0) {
                $noHal++;
                if ($jum > (9 * $noHal)) {
                    $this->printForm("barang", $nb, " LANJUTAN");
                    $this->printForm_Footer();
                    $this->pdf->SetY(35);
                    $this->pdf->Ln();
                }
            }
            $i++;
        }
    }

    function printLembarDokumen($nb) {
        $SQL = "select A.*,B.uraian, convert(varchar,A.DOKTG,105) as DOKTG from tblpibdok A, tbltabel B where B.kdtab= '06' 
and B.kdrec = A.dokkd and A.car = '$this->CAR'";
        $data = $this->db->query($SQL);
        $i = 0;
        $jmlAWB = 0;
        $jmlFas = 0;
        $jmlInv = 0;
        $jmlLC = 0;
        $jmlLain = 0;
        $showForm = 0;
        $dokkdinv = array();
        $dokkd_nama_inv = array();
        $dokno_inv = array();
        $doktg_inv = array();

        $dokkdawb = array();
        $dokkd_nama_awb = array();
        $dokno_awb = array();
        $doktg_awb = array();

        $dokkdfas = array();
        $dokkd_nama_fas = array();
        $dokno_fas = array();
        $doktg_fas = array();

        $dokkdlc = array();
        $dokkd_nama_lc = array();
        $dokno_lc = array();
        $doktg_lc = array();

        $dokkdLain = array();
        $dokkd_nama_Lain = array();
        $dokno_Lain = array();
        $doktg_Lain = array();

        while ($data->next()) {
            $dokkd_lanjutan = $data->get("DOKKD");
            $dokkd_nama_lanjutan = $data->get("URAIAN");
            $dokno_lanjutan = $data->get("DOKNO");
            $doktg_lanjutan = $data->get("DOKTG");

            if ($dokkd_lanjutan == '380') {
                $jmlInv++;
                $dokkdinv[$jmlInv] = $dokkd_lanjutan;
                $dokkd_nama_inv[$jmlInv] = $dokkd_nama_lanjutan;
                $dokno_inv[$jmlInv] = $dokno_lanjutan;
                $doktg_inv[$jmlInv] = $doktg_lanjutan;
            } elseif ($dokkd_lanjutan == '465') {
                $jmlLC++;
                $dokkdlc[$jmlLC] = $dokkd_lanjutan;
                $dokkd_nama_lc[$jmlLC] = $dokkd_nama_lanjutan;
                $dokno_lc[$jmlLC] = $dokno_lanjutan;
                $doktg_lc[$jmlLC] = $doktg_lanjutan;
            } elseif (in_array($dokkd_lanjutan, array(704, 705, 740, 741))) {
                $jmlAWB++;
                $dokkdawb[$jmlAWB] = $dokkd_lanjutan;
                $dokkd_nama_awb[$jmlAWB] = $dokkd_nama_lanjutan;
                $dokno_awb[$jmlAWB] = $dokno_lanjutan;
                $doktg_awb[$jmlAWB] = $doktg_lanjutan;
            } elseif (in_array($dokkd_lanjutan, array(814, 815, 851, 853, 911, 993, 998, 861))) {
                $jmlFas++;
                $dokkdfas[$jmlFas] = $dokkd_lanjutan;
                $dokkd_nama_fas[$jmlFas] = $dokkd_nama_lanjutan;
                $dokno_fas[$jmlFas] = $dokno_lanjutan;
                $doktg_fas[$jmlFas] = $doktg_lanjutan;
            } else {
                $jmlLain++;
                $dokkdLain[$jmlLain] = $dokkd_lanjutan;
                $dokkd_nama_Lain[$jmlLain] = $dokkd_nama_lanjutan;
                $dokno_Lain[$jmlLain] = $dokno_lanjutan;
                $doktg_Lain[$jmlLain] = $doktg_lanjutan;
            }
        }

        $cetakAWB = 0;
        $cetakFas = 0;
        $cetakInv = 0;
        $cetakLC = 0;

        if ($jmlInv > 3) {
            $this->printForm("dokumen", $nb, " LAMPIRAN DOKUMEN");
            $showForm = 1;
            for ($a = 1; $a <= $jmlInv; $a++) {
                $i = $i + 1;
                $this->printData_DOK($dokkdinv[$a], $dokkd_nama_inv[$a], $dokno_inv[$a], $doktg_inv[$a], $i);
            }
            if ($jmlLC == 2) {
                $cetakLC = 1;
                for ($a = 1; $a <= 2; $a++) {
                    $i = $i + 1;
                    $this->printData_DOK($dokkdlc[$a], $dokkd_nama_lc[$a], $dokno_lc[$a], $doktg_lc[$a], $i);
                }
            }
            if ($jmlAWB == 2) {
                $cetakAWB = 1;
                for ($a = 1; $a <= 2; $a++) {
                    $i = $i + 1;
                    $this->printData_DOK($dokkdawb[$a], $dokkd_nama_awb[$a], $dokno_awb[$a], $doktg_awb[$a], $i);
                }
            }
            if ($jmlFas == 1) {
                $cetakFas = 1;
                for ($a = 1; $a <= 1; $a++) {
                    $i = $i + 1;
                    $this->printData_DOK($dokkdfas[$a], $dokkd_nama_fas[$a], $dokno_fas[$a], $doktg_fas[$a], $i);
                }
            }
        }

        if ($jmlLC > 2) {
            if ($showForm != 1) {
                $this->printForm("dokumen", $nb, " LAMPIRAN DOKUMEN");
                $showForm = 1;
            }
            if ($jmlInv == 3) {
                $cetakInv = 1;
                for ($a = 1; $a <= $jmlInv; $a++) {
                    $i = $i + 1;
                    $this->printData_DOK($dokkdinv[$a], $dokkd_nama_inv[$a], $dokno_inv[$a], $doktg_inv[$a], $i);
                }
            }
            for ($a = 1; $a <= $jmlLC; $a++) {
                $i = $i + 1;
                $this->printData_DOK($dokkdlc[$a], $dokkd_nama_lc[$a], $dokno_lc[$a], $doktg_lc[$a], $i);
            }
            if ($jmlAWB == 2 and $cetakAWB != 1) {
                $cetakAWB = 1;
                for ($a = 1; $a <= 2; $a++) {
                    $i = $i + 1;
                    $this->printData_DOK($dokkdawb[$a], $dokkd_nama_awb[$a], $dokno_awb[$a], $doktg_awb[$a], $i);
                }
            }
            if ($jmlFas == 1 and $cetakFas != 1) {
                $cetakFas = 1;
                for ($a = 1; $a <= 1; $a++) {
                    $i = $i + 1;
                    $this->printData_DOK($dokkdfas[$a], $dokkd_nama_fas[$a], $dokno_fas[$a], $doktg_fas[$a], $i);
                }
            }
        }

        if ($jmlAWB > 2) {
            if ($showForm != 1) {
                $this->printForm("dokumen", $nb, " LAMPIRAN DOKUMEN");
                $showForm = 1;
            }
            if ($jmlInv == 3 and $cetakInv != 1) {
                $cetakInv = 1;
                for ($a = 1; $a <= $jmlInv; $a++) {
                    $i = $i + 1;
                    $this->printData_DOK($dokkdinv[$a], $dokkd_nama_inv[$a], $dokno_inv[$a], $doktg_inv[$a], $i);
                }
            }
            if ($jmlLC == 2 and $cetakLC != 1) {
                $cetakLC = 1;
                for ($a = 1; $a <= $jmlLC; $a++) {
                    $i = $i + 1;
                    $this->printData_DOK($dokkdlc[$a], $dokkd_nama_lc[$a], $dokno_lc[$a], $doktg_lc[$a], $i);
                }
            }

            for ($a = 1; $a <= $jmlAWB; $a++) {
                $i = $i + 1;
                $this->printData_DOK($dokkdawb[$a], $dokkd_nama_awb[$a], $dokno_awb[$a], $doktg_awb[$a], $i);
            }

            if ($jmlFas == 1 and $cetakFas != 1) {
                $cetakFas = 1;
                for ($a = 1; $a <= 1; $a++) {
                    $i = $i + 1;
                    $this->printData_DOK($dokkdfas[$a], $dokkd_nama_fas[$a], $dokno_fas[$a], $doktg_fas[$a], $i);
                }
            }
        }

        if ($jmlFas > 1) {
            if ($showForm != 1) {
                $this->printForm("dokumen", $nb, " LAMPIRAN DOKUMEN");
                $showForm = 1;
            }
            if ($jmlInv == 3 and $cetakInv != 1) {
                $cetakInv = 1;
                for ($a = 1; $a <= $jmlInv; $a++) {
                    $i = $i + 1;
                    $this->printData_DOK($dokkdinv[$a], $dokkd_nama_inv[$a], $dokno_inv[$a], $doktg_inv[$a], $i);
                }
            }
            if ($jmlLC == 2 and $cetakLC != 1) {
                for ($a = 1; $a <= $jmlLC; $a++) {
                    $i = $i + 1;
                    $this->printData_DOK($dokkdlc[$a], $dokkd_nama_lc[$a], $dokno_lc[$a], $doktg_lc[$a], $i);
                }
            }

            if ($jmlAWB == 2 and $cetakAWB != 1) {
                for ($a = 1; $a <= $jmlAWB; $a++) {
                    $i = $i + 1;
                    $this->printData_DOK($dokkdawb[$a], $dokkd_nama_awb[$a], $dokno_awb[$a], $doktg_awb[$a], $i);
                }
            }

            for ($a = 1; $a <= $jmlFas; $a++) {
                $i = $i + 1;
                $this->printData_DOK($dokkdfas[$a], $dokkd_nama_fas[$a], $dokno_fas[$a], $doktg_fas[$a], $i);
            }
        }

        if ($jmlLain > 0) {
            if ($showForm != 1) {
                $this->printForm("dokumen", $nb, " LAMPIRAN DOKUMEN");
            }
            if ($jmlInv == 3 and $cetakInv != 1) {
                $cetakInv = 1;
                for ($a = 1; $a <= $jmlInv; $a++) {
                    $i = $i + 1;
                    $this->printData_DOK($dokkdinv[$a], $dokkd_nama_inv[$a], $dokno_inv[$a], $doktg_inv[$a], $i);
                }
            }
            if ($jmlLC == 2 and $cetakLC != 1) {
                for ($a = 1; $a <= $jmlLC; $a++) {
                    $i = $i + 1;
                    $this->printData_DOK($dokkdlc[$a], $dokkd_nama_lc[$a], $dokno_lc[$a], $doktg_lc[$a], $i);
                }
            }

            if ($jmlAWB == 2 and $cetakAWB != 1) {
                for ($a = 1; $a <= $jmlAWB; $a++) {
                    $i = $i + 1;
                    $this->printData_DOK($dokkdawb[$a], $dokkd_nama_awb[$a], $dokno_awb[$a], $doktg_awb[$a], $i);
                }
            }

            if ($jmlFas == 1 and $cetakFas != 1) {
                $cetakFas = 1;
                for ($a = 1; $a <= 1; $a++) {
                    $i = $i + 1;
                    $this->printData_DOK($dokkdfas[$a], $dokkd_nama_fas[$a], $dokno_fas[$a], $doktg_fas[$a], $i);
                }
            }

            for ($a = 1; $a <= $jmlLain; $a++) {
                $i = $i + 1;
                $this->printData_DOK($dokkdLain[$a], $dokkd_nama_Lain[$a], $dokno_Lain[$a], $doktg_Lain[$a], $i);
            }
        }

        if ($i != 0) {
            $tinggi = $i * 4;
            $this->pdf->Rect(5.4, 29.4, 199.2, $tinggi, 3.5, 'F');
            $this->printForm_Footer();
        }
    }

    function printLembarKemasan() {
        $this->printForm("kemasan", $nb, " LAMPIRAN KEMASAN");
        //$this->page = 1;
        // get Dokumen
        $SQL = "SELECT tbl.jnkemas, tbl.jmkemas, tbl.merkkemas, kemasan.uredi
  FROM tblpibkms tbl, tblkemasan kemasan
 WHERE ((tbl.jnkemas = kemasan.kdedi) and (car = '$this->CAR'))";
        $data = $this->db->query($SQL);
        $i = 0;
        while ($data->next()) {
            $jnkemas_lanjutan = $data->get("JNKEMAS");
            $jnkemas_nama_lanjutan = $data->get("UREDI");
            $jmkemas_lanjutan = $data->get("JMNKEMAS");
            $merkkemas_lanjutan = $data->get("MERKKEMAS");
            $i = $i + 1;
            // menghitung jumlah baris rec
            // mengurangi counter baris
            // jika counter baris < 0 maka printForm("dokumen") dan reset counter baris
            $this->printData_KMS($jnkemas_lanjutan, $jnkemas_nama_lanjutan, $jmkemas_lanjutan, $merkkemas_lanjutan, $i);
        }
        $tinggi = $i * 4;
        $this->pdf->Rect(5.4, 29.4, 199.2, $tinggi, 3.5, 'F');
        $this->printForm_Footer();
    }

    function printLembarKontainer($nb) {
        $this->printForm("kontainer", $nb, " LAMPIRAN KONTAINER");
        //$this->page = 1;
        // get Dokumen
        $SQL = "SELECT * FROM tblpibcon WHERE car = '$this->CAR'";
        $data = $this->db->query($SQL);
        $i = 1;
        while ($data->next()) {
            $contno = $data->get("CONTNO");
            $contukur = $data->get("CONTUKUR");
            $conttipe = $data->get("CONTTIPE");

            $r = fmod($i, 2);

            if ($i == '1' || $r == '1') {
                $this->pdf->Ln();
                $this->pdf->cell(9.8, 4, $i, 0, 0, 'L', 0);
                $this->pdf->cell(40, 4, $this->setnocont($contno), 0, 0, 'C', 0);
                $this->pdf->cell(24.9, 4, $contukur, 0, 0, 'C', 0);
                $this->pdf->cell(24.9, 4, $conttipe . 'CL', 'R', 0, 'L', 0);
            } else {
                $this->pdf->cell(9.8, 4, $i, 0, 0, 'L', 0);
                $this->pdf->cell(40, 4, $this->setnocont($contno), 0, 0, 'C', 0);
                $this->pdf->cell(24.9, 4, $contukur, 0, 0, 'C', 0);
                $this->pdf->cell(24.9, 4, $conttipe . 'CL', 0, 0, 'L', 0);
            }
            $i = $i + 1;
        }
        $tinggi = (floor($i / 2)) * 4;
        $this->pdf->Rect(5.4, 29.4, 199.2, $tinggi, 3.5, 'F');
        $this->printForm_Footer();
    }

    function printForm($tipe, $nb, $lamp) {
        $this->pdf->AddPage();
        $this->page++;
        //header
        $this->pdf->Rect(5.4, 13.4, 199.2, 12, 3.5, 'F');
        $this->pdf->SetFont('times', 'B', '10');
        //$this->pdf->cell(199.2,4,'LEMBAR LANJUTAN '.$tipe,0,0,'C',0);
        $this->pdf->cell(199.2, 4, 'LEMBAR ' . $lamp, 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->cell(199.2, 4, 'PEMBERITAHUAN IMPOR BARANG (PIB)', 0, 0, 'C', 0);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->Ln();
        $this->pdf->cell(45, 4, 'Kantor Pabean', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(103, 4, $this->kpbc, 0, 0, 'L', 0);
        $this->pdf->cell(15, 4, $this->kdkpbc, 1, 0, 'C', 0);
        $this->pdf->cell(30, 4, " Halaman " . $this->page . " dari " . $nb, 0, 0, 'R', 0); //dari $nb
        $this->pdf->AliasNbPages();
        $this->pdf->Ln();
        $this->pdf->cell(45, 4, 'Nomor Pengajuan', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(80, 4, $this->noaju, 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->cell(45, 4, 'Nomor Pendaftaran', 0, 0, 'L', 0);
        $this->pdf->cell(2, 4, ':', 0, 0, 'C', 0);
        $this->pdf->cell(80, 4, $this->pibno . ' / ' . $this->pibtg, 0, 0, 'L', 0);


        switch ($tipe) {
            case "barang":
                $this->pdf->Rect(5.4, 25.4, 10, 16, 3.5, 'F');
                $this->pdf->Rect(15.4, 25.4, 70, 16, 3.5, 'F');
                $this->pdf->Rect(85.4, 25.4, 30, 16, 3.5, 'F');
                $this->pdf->Rect(115.4, 25.4, 30, 16, 3.5, 'F');
                $this->pdf->Rect(145.4, 25.4, 29.4, 16, 3.5, 'F');
                $this->pdf->Rect(174.8, 25.4, 29.8, 16, 3.5, 'F');

                //$this->pdf->Rect(5.4, 37.4, 199.20, 228, 3.5, 'F');
                $this->pdf->Rect(5.4, 25.4, 10, 240, 3.5, 'F');
                $this->pdf->Rect(5.4, 25.4, 80, 240, 3.5, 'F');
                $this->pdf->Rect(85.4, 25.4, 30, 240, 3.5, 'F');
                $this->pdf->Rect(115.4, 25.4, 30, 240, 3.5, 'F');
                $this->pdf->Rect(145.4, 25.4, 29.4, 240, 3.5, 'F');
                $this->pdf->Rect(174.8, 25.4, 29.8, 240, 3.5, 'F');
//				$this->pdf->Rect(184.8, 25.4, 29.8, 228, 3.5, 'F');

                $this->pdf->Ln();
                $this->pdf->cell(10, 4, '31.', 0, 0, 'L', 0);
                $this->pdf->cell(70, 4, '32 - Pos tarif /HS', 0, 0, 'L', 0);
                $this->pdf->cell(30, 4, '33. Negara', 0, 0, 'L', 0);
                $this->pdf->cell(30, 4, '34. Tarif & Fasilitas', 0, 0, 'L', 0);
                $this->pdf->cell(29.4, 4, '35. Jumlah &', 0, 0, 'L', 0);
                $this->pdf->cell(29.8, 4, '36. Jumlah Nilai CIF', 0, 0, 'L', 0);
                $this->pdf->Ln();
                $this->pdf->cell(10, 4, 'No.', 0, 0, 'L', 0);
                $this->pdf->cell(70, 4, ' - Uraian barang secara lengkap meliputi jenis, jumlah,', 0, 0, 'L', 0);
                $this->pdf->cell(30, 4, ' Asal', 0, 0, 'L', 0);
                $this->pdf->cell(30, 4, ' - BM -PPN - PPnBM', 0, 0, 'L', 0);
                $this->pdf->cell(29.4, 4, '    Jenis Satuan,', 0, 0, 'L', 0);
                $this->pdf->cell(29.8, 4, '', 0, 0, 'L', 0);
                $this->pdf->Ln();
                $this->pdf->cell(10, 4, '', 0, 0, 'L', 0);
                $this->pdf->cell(70, 4, '    merk, type, ukuran, spesifikasi lainnya', 0, 0, 'L', 0);
                $this->pdf->cell(30, 4, '', 0, 0, 'L', 0);
                $this->pdf->cell(30, 4, ' - Cukai       - PPh', 0, 0, 'L', 0);
                $this->pdf->cell(29.4, 4, '    Berat Bersih (kg)', 0, 0, 'L', 0);
                $this->pdf->cell(29.8, 4, '', 0, 0, 'L', 0);
                //------------------------------- Baru Tambahan ---------------------------------------//
                $this->pdf->Ln();
                $this->pdf->cell(10, 4, '', 0, 0, 'L', 0);
                $this->pdf->cell(75, 4, ' - Jenis Fasilitas', 0, 0, 'L', 0);
                $this->pdf->cell(25, 4, '', 0, 0, 'L', 0);
                $this->pdf->cell(30, 4, '', 0, 0, 'L', 0);
                $this->pdf->cell(29.4, 4, ' - Jml/Jns Kemasan', 0, 0, 'L', 0);
                $this->pdf->cell(29.8, 4, '', 0, 0, 'L', 0);
                //-------------------------------------------------------------------------------------------//
                break;
            case "dokumen":
                $this->pdf->Rect(5.4, 25.4, 199.2, 4, 3.5, 'F');

                $this->pdf->Ln();
                $this->pdf->cell(19.2, 4, '', 0, 0, 'L', 0);
                $this->pdf->cell(60, 4, 'Jenis Dokumen', 0, 0, 'L', 0);
                $this->pdf->cell(60, 4, 'Nomor Dokumen', 0, 0, 'L', 0);
                $this->pdf->cell(60, 4, 'Tanggal Dokumen', 0, 0, 'L', 0);
                break;
            case "kemasan":
                $this->pdf->Rect(5.4, 25.4, 199.2, 4, 3.5, 'F');
                $this->pdf->Ln();
                $this->pdf->cell(19.2, 4, '', 0, 0, 'L', 0);
                $this->pdf->cell(60, 4, 'Jumlah', 0, 0, 'L', 0);
                $this->pdf->cell(60, 4, 'Jenis Kemasan', 0, 0, 'L', 0);
                $this->pdf->cell(60, 4, 'Merk Kemasan', 0, 0, 'L', 0);
                break;
            case "kontainer":
                $this->pdf->Rect(5.4, 25.4, 99.6, 4, 3.5, 'F');
                $this->pdf->Rect(105, 25.4, 99.6, 4, 3.5, 'F');
                $this->pdf->Ln();
                $this->pdf->cell(9.8, 4, 'No.Urut', 0, 0, 'L', 0);
                $this->pdf->cell(40, 4, 'Nomor Kontainer', 0, 0, 'C', 0);
                $this->pdf->cell(24.9, 4, 'Ukuran', 0, 0, 'C', 0);
                $this->pdf->cell(24.9, 4, 'Tipe', 0, 0, 'L', 0);
                $this->pdf->cell(9.8, 4, 'No.Urut', 0, 0, 'L', 0);
                $this->pdf->cell(40, 4, 'Nomor Kontainer', 0, 0, 'C', 0);
                $this->pdf->cell(24.9, 4, 'Ukuran', 0, 0, 'C', 0);
                $this->pdf->cell(24.9, 4, 'Tipe', 0, 0, 'L', 0);
                break;
        }
    }

    function printData_BRG($nohs, $brgurai, $merk, $tipe, $spflain, $kemasjm, $kemasjn, $kemasjn_nama, $kdfas, $kdfas_nama, $brgasal, $brgasal_nama, $jmlsat, $kdsat, $kdsat_nama, $nettodtl, $dcif, $kdfasbm, $kdfasbm_nama, $kodesatbm, $trpbm, $satbmjm, $fasbm, $kdfascuk, $kdfascuk_nama, $trpcuk, $kdfascuk_nama, $fascuk, $kdsat, $kdsat_nama, $trpppn, $kdfasppn, $fasppn, $kdfasbm, $trppph, $kdfaspph, $faspph, $trpppn, $kdfasppn, $fasppn, $jmlbaris, $trppbm, $kdfaspbm, $faspbm) {
        $this->pdf->Ln();
        //$this->pdf->Ln();
        $this->pdf->cell(10, 4, $jmlbaris . '   ', 0, 0, 'R', 0);
        $kordinaty = $this->pdf->gety();
        $kordinatx = $this->pdf->getx();
        $this->pdf->cell(70, 4, $this->formaths($nohs), 0, 0, 'L', 0);
        $this->pdf->Ln();

        //$sat = $this->trimstr($kdsat.' / '.$kdsat_nama,20);
        //$sat = $kdsat;

        $sat = $kdsat . ' / ' . $kdsat_nama;
        if (strlen($sat) > 16) {
            $sat1 = substr($sat, 0, 16);
            $sat2 = substr($sat, 16);
        } else {
            $sat1 = $sat;
            $sat2 = "";
        }
        $urai = $this->trimstr($brgurai, 35);

        $this->pdf->cell(10, 4, '', 0, 0, 'L', 0);
        $this->pdf->setx($kordinatx);
        
        $this->pdf->multicell(70, 3.5, $brgurai, 0, 'L');
        $jn = number_format($kemasjm, 4, '.', ',');
        if (trim($spek['str2']) != '') {
            $this->pdf->cell(10, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(70, 4, $spek['str2'], 0, 0, 'L', 0);
            $this->pdf->cell(30, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(30, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(29.4, 4, '', 0, 0, 'L', 0);
            $this->pdf->cell(29.8, 4, '', 0, 0, 'L', 0);
        }
        $this->pdf->cell(10, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(70, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(30, 4, '', 0, 0, 'L', 0);

        $this->pdf->setxy($kordinatx + 70, $kordinaty);

        $kordinaty = $this->pdf->gety();
        $kordinatx = $this->pdf->getx();
        $this->pdf->cell(30, 4, $brgasal_nama . ' (' . $brgasal . ')', 0, 0, 'L', 0);
        $kordinatxx = $this->pdf->getx();
        $this->pdf->setxy($kordinatx, $kordinaty + 5);
        $this->pdf->cell(30, 4, '', 0, 0, 'L', 0);
        $this->pdf->setxy($kordinatxx, $kordinaty);
        if ($fasbm != 0) {
            $fbmNYA = $fasbm / 100;
            $strfasbm = $this->getfas($kdfasbm) . ' : ' . $fbmNYA . ' %';
        } else {
            if ($kdfasbm == '4') {
                $strfasbm = 'BBS : 100%';
            } else {
                $strfasbm = "";
            }
        }
        if ($fascuk != 0) {
            $fcukNYA = $fascuk / 100;
            $strfascuk = $this->getfas($kdfascuk) . ' : ' . $fcukNYA . ' %';
        }
        if ($fasppn != 0) {
            $fppnNYA = $fasppn / 100;
            $strfasppn = $this->getfas($kdfasppn) . ' : ' . $fppnNYA . ' %';
        }
        if ($faspbm != 0) {
            $fpbmNYA = $faspbm / 100;
            $strfaspbm = $this->getfas($kdfaspbm) . ' : ' . $fpbmNYA . ' %';
        }
        if ($faspph != 0) {
            $fpphNYA = $faspph / 100;
            $strfaspph = $this->getfas($kdfaspph) . ' : ' . $fpphNYA . ' %';
        }


        $kordinaty = $this->pdf->gety();
        $kordinatx = $this->pdf->getx();
        $this->pdf->cell(30, 4, 'BM:' . $this->strip($trpbm) . ' ' . $strfasbm, 0, 0, 'L', 0);
        $this->pdf->cell(29.4, 4, number_format($jmlsat, 4, '.', ','), 0, 0, 'L', 0);
        $this->pdf->cell(29, 4, number_format($dcif, 2, '.', ','), 0, 0, 'R', 0);

        $this->pdf->Ln();
        $this->pdf->setx($kordinatx);
        $this->pdf->cell(30, 4, 'Cukai:' . $this->strip($trpcuk) . ' ' . $strfascuk, 0, 0, 'L', 0);
        $this->pdf->cell(29.4, 4, $sat1 . '', 0, 0, 'L', 0);
        $this->pdf->cell(29.8, 4, '', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->setx($kordinatx);
        $this->pdf->cell(30, 4, 'PPN:' . $this->strip($trpppn) . ' ' . $strfasppn, 0, 0, 'L', 0);
        $this->pdf->cell(29.4, 4, $sat2 . '', 0, 0, 'L', 0);
        
        $this->pdf->cell(29.8, 4, '', 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->setx($kordinatx);
        $this->pdf->cell(30, 4, 'PPnBM:' . $this->strip($trppbm) . ' ' . $strfaspbm, 0, 0, 'L', 0);
        $this->pdf->cell(29.4, 4, number_format($nettodtl, 4, '.', ',') . ' Kg', 0, 0, 'L', 0);

        $this->pdf->cell(29.8, 4, '', 0, 0, 'L', 0);
        $this->pdf->Ln();
        if ($this->apikd == '' || $this->apino == "") {
            $trppph = '7.5';
        } else {
            $trppph = '2.5';
        }
        $this->pdf->setx($kordinatx);
        $this->pdf->cell(30, 4, 'PPh:' . $this->strip($trppph) . ' ' . $strfaspph, 0, 0, 'L', 0);

        $nsl = $kemasjn . ' / ' . $kemasjn_nama;
        if (strlen($nsl) > 16) {
            $nsl1 = substr($nsl, 0, 14);
            $nsl2 = substr($nsl, 14);
        } else {
            $nsl1 = $nsl;
            $nsl2 = "";
        }

        $this->pdf->cell(29.4, 4, $jn . ' ' . $nsl1, 0, 0, 'L', 0);
        $this->pdf->cell(29.4, 4, $sas['str2'] . '', 0, 0, 'L', 0);
        $this->pdf->cell(29.8, 4, '', 0, 0, 'L', 0);


        $this->pdf->cell(70, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(30, 4, '', 0, 0, 'L', 0);

        $this->pdf->cell(29.4, 4, $sat['str1'] . '', 0, 0, 'L', 0);
        $this->pdf->cell(29.8, 4, '', 0, 0, 'L', 0);

        $this->pdf->cell(29.4, 4, $sat['str2'], 0, 0, 'L', 0);
        $this->pdf->cell(29.8, 4, '', 0, 0, 'L', 0);

        $this->pdf->Ln();
        $this->pdf->cell(10, 4, '', 0, 0, 'L', 0);
        if (trim($kdfas) <> "") {
            $this->pdf->cell(30, 4, 'Fas: ' . $kdfas . ' / ' . $kdfas_nama, 0, 0, 'L', 0);
        } else {
            $this->pdf->cell(30, 4, '' . $kdfas . ' / ' . $kdfas_nama, 0, 0, 'L', 0);
        }
        $this->pdf->cell(100, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(29.4, 4, $nsl2, 0, 0, 'L', 0);
        $kdfas_nama = "";
        $this->pdf->cell(29.4, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(29.8, 4, '', 0, 0, 'L', 0);
    }

    function printData_DOK($kode, $nama, $no_dok, $tanggal, $jmlbaris) {
        $this->pdf->Ln();
        $this->pdf->cell(19.2, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(60, 4, $nama, 0, 0, 'L', 0);
        $this->pdf->cell(60, 4, $no_dok, 0, 0, 'L', 0);
        $this->pdf->cell(60, 4, $tanggal, 0, 0, 'L', 0);
    }

    function printData_KMS($jenis, $nama, $jumlah_kms, $merk, $jmlbaris) {
        $this->pdf->Ln();
        $this->pdf->cell(19.2, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(60, 4, $nama, 0, 0, 'L', 0);
        $this->pdf->cell(60, 4, $jumlah_kms, 0, 0, 'L', 0);
        $this->pdf->cell(60, 4, $merk, 0, 0, 'L', 0);
    }

    function formatcar($strcar) {
        $strcar = substr($strcar, 0, 6) . " - " . substr($strcar, 6, 6) . " - " . substr($strcar, 12, 8) . " - " . substr($strcar, 20);
        return $strcar;
    }

    function printForm_Footer() {
        $this->pdf->SetY(-30);
        $this->pdf->Ln();
        $this->pdf->cell(99.6, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(49.8, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(50, 4, $this->kota . ', ' . $this->tanggal_ttd, 0, 0, 'C', 0);

        if (trim($this->ppjknpwp) <> "") {
            $ppjkx = "PPJK";
        } else {
            $ppjkx = "Importir";
        }
        $this->pdf->Ln();
        $this->pdf->cell(99.6, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(49.8, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(50, 4, $ppjkx, 0, 0, 'C', 0);
        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->Ln();
        $this->pdf->SetFont('times', 'I', '9');
        $this->pdf->cell(99.8, 5, 'Tgl. Cetak ' . date("d-m-Y"), 0, 0, 'L', 0);
        $this->pdf->SetFont('times', '', '9');
        $this->pdf->cell(49.8, 4, '', 0, 0, 'L', 0);
        $this->pdf->cell(50, 4, $this->namaTTD, 0, 0, 'C', 0);
    }

}

?>
