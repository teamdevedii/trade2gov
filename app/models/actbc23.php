<?php
if (!defined('BASEPATH')){exit('No direct script access allowed');}
class actBC23 extends CI_Model {

    var $kd_trader = '';

    public function __construct() {
        parent::__construct();
        $this->kd_trader = $this->newsession->userdata('KODE_TRADER');
        return true;
    }
   
    function getHeader($car){ //print_r('sini');die();
        $data = array();
        $data_pgt = array();
        $this->load->model('actMain');        
        if ($car) {
            $sql = "SELECT  A.* , 
                            DATE_FORMAT(A.TGLTTD,'%Y-%m-%d'),
                            B.URAIAN as 'URSTATUS',
                            C.DOKNO as 'NOINVOICE',
                            C.DOKTG as 'TGINVOICE',
                            D.DOKNO as 'SKPEB',
                            D.DOKTG as 'TGSKPEB',
                            E.DOKNO as 'LC',
                            E.DOKTG as 'TGLC',
                            F.DOKNO as 'AWB',
                            F.DOKTG as 'TGAWB',
                            N.DOKNO as 'BL',
                            N.DOKTG as 'TGBL',
                            G.NAMA_TTD as 'NAMATTD',
                            G.KOTA_TTD as 'KOTATTD',
                            DATE_FORMAT(A.TGLTTD,'%Y-%m-%d') as TGLTTD,
                            H.URAIAN_KPBC AS 'URKDKPBC',
                            I.URAIAN_NEGARA AS URPASOKNEG,
                            J.URAIAN_NEGARA AS URANGKUTFL,
                            K.URAIAN_PELABUHAN AS URPELMUAT,
                            L.URAIAN_PELABUHAN AS URPELTRANSIT,
                            M.URAIAN_PELABUHAN AS URPELBKR,
                            O.URAIAN_KPBC AS URKDKPBCBONGKAR,
                            P.URAIAN_KPBC AS URKDKPBCAWAS,
                            Q.UREDI AS URKDVAL,
                            R.URAIAN AS URTMPTBN
                    FROM    t_bc23hdr A   LEFT JOIN m_tabel B ON B.KDREC = A.STATUS AND B.KDTAB = 'STATUS' AND B.MODUL = 'BC23'
					  LEFT JOIN t_bc23dok C ON A.KODE_TRADER = C.KODE_TRADER and A.CAR = C.CAR and C.DOKKD = '380'
					  LEFT JOIN t_bc23dok D ON A.KODE_TRADER = D.KODE_TRADER and A.CAR = D.CAR and D.DOKKD = '911'
					  LEFT JOIN t_bc23dok E ON A.KODE_TRADER = E.KODE_TRADER and A.CAR = E.CAR and E.DOKKD = '465'
					  LEFT JOIN t_bc23dok F ON A.KODE_TRADER = F.KODE_TRADER and A.CAR = F.CAR and F.DOKKD = '740'
					  LEFT JOIN t_bc23dok N ON A.KODE_TRADER = N.KODE_TRADER and A.CAR = N.CAR and N.DOKKD = '705'
                                          LEFT JOIN m_trader_bc23 G ON G.KODE_TRADER = A.KODE_TRADER
                                          LEFT JOIN m_kpbc H ON H.KDKPBC = A.KDKPBC
                                          LEFT JOIN m_negara I ON A.PASOKNEG = I.KODE_NEGARA
                                          LEFT JOIN m_negara J ON A.PASOKNEG = J.KODE_NEGARA
                                          LEFT JOIN m_pelabuhan K ON A.PELMUAT  = K.KODE_PELABUHAN
                                          LEFT JOIN m_pelabuhan L ON A.PELTRANSIT = L.KODE_PELABUHAN
                                          LEFT JOIN m_pelabuhan M ON A.PELBKR = M.KODE_PELABUHAN
                                          LEFT JOIN m_kpbc O ON A.KDKPBCBONGKAR = O.KDKPBC
                                          LEFT JOIN m_kpbc P ON A.KDKPBCAWAS = P.KDKPBC
                                          LEFT JOIN m_valuta Q ON A.KDVAL = Q.KDEDI
                                          LEFT JOIN m_gudang R ON A.TMPTBN = R.KDGDG
                     WHERE A.CAR = '".$car."' AND A.KODE_TRADER = '".$this->kd_trader."' "; 
        $data['HEADER'] = $this->actMain->get_result($sql);//die($sql);
        $query = "SELECT KDBEBAN, KDFASIL, NILBEBAN 
                  FROM  t_bc23pgt 
                  WHERE CAR='" . $car . "' AND KODE_TRADER = '" . $this->kd_trader . "'";
        $arrPGT = $this->actMain->get_result($query, true);
            foreach ($arrPGT as $row) {
                if ($row['KDBEBAN'] == '5' or $row['KDBEBAN'] == '6' or $row['KDBEBAN'] == '7') { //cukai dijadikan 1 kodebeban
                    $row['KDBEBAN'] = '5';
                }
                $data_pgt[$row['KDBEBAN']][$row['KDFASIL']] += $row['NILBEBAN'];
                $data_pgt['TOTAL'][$row['KDFASIL']] += $row['NILBEBAN'];
            }
        $data['act']    = 'update';
        }
        else{
            if($this->newsession->userdata('TIPE_TRADER')=='2'){
                $sql = "SELECT a.KODE_ID as 'PPJKID', 
                           a.ID as 'PPJKNPWP'  ,
                           a.NAMA as 'PPJKNAMA' ,
                           a.ALAMAT as 'PPJKALMT',
                           a.NO_PPJK as 'PPJKNO',
                           a.TANGGAL_PPJK as 'PPJKTG',
                           b.USAHASTATUS,                                                                                 
                           b.NAMA_TTD as 'NAMATTD', 
                           b.KOTA_TTD as 'KOTATTD'
                    FROM m_trader a LEFT JOIN m_trader_bc23 b ON a.KODE_TRADER = b.KODE_TRADER
                    WHERE a.KODE_TRADER = '".$this->kd_trader."'";//die($sql);
            }else{
                $sql = "SELECT a.KODE_ID as 'USAHAID', 
                           a.ID as 'USAHANPWP'  ,
                           a.NAMA as 'USAHANAMA' ,
                           a.ALAMAT as 'USAHAALMT', 
                           b.USAHASTATUS,
                           b.REGISTRASI, 
                           b.APIKD, 
                           b.APINO, 
                           b.KDTPB, 
                           b.NAMA_TTD as 'NAMATTD', 
                           b.KOTA_TTD as 'KOTATTD'
                    FROM m_trader a LEFT JOIN m_trader_bc23 b ON a.KODE_TRADER = b.KODE_TRADER
                    WHERE a.KODE_TRADER = '".$this->kd_trader."'";
            }
            $data['HEADER'] = $this->actMain->get_result($sql);
            $data['act']    = 'save';
        }
        $data['TIPE_TRADER']        = $this->newsession->userdata('TIPE_TRADER');
        $data['IMPID']              = $this->actMain->get_mtabel('JENIS_IDENTITAS', 1, false, '', 'KODEDANUR','BC23');
        $data['TUJUAN']             = $this->actMain->get_mtabel('TUJUAN', 1, false, '', 'KODEDANUR', 'BC23');
        $data['JNSBARANG']          = $this->actMain->get_mtabel('JNSBARANG', 1, false, '', 'KODEDANUR', 'BC23');
        $data['TUJUANKIRIM']        = $this->actMain->get_mtabel('TUJUANKIRIM', 1, false, '', 'KODEDANUR', 'BC23');
        $data['KDTPB']              = $this->actMain->get_mtabel('JENIS_TPB', 1, false, '', 'KODEDANUR', 'BC23');
        $data['DOKTUPKD']           = $this->actMain->get_mtabel('DOKTUPKD', 1, false, '', 'KODEDANUR', 'BC23');
        $data['JENIS_IDENTITAS']    = $this->actMain->get_mtabel('JENIS_IDENTITAS', 1, false, '', 'KODEDANUR','BC23');
        $data['MODA']               = $this->actMain->get_mtabel('MODA', 1, false, '', 'KODEDANUR','BC23');        
        $data['APIKD']              = $this->actMain->get_mtabel('JNS_NOIJIN', 1, false, '', 'KODEDANUR','BC23');
        $data['STATUS_PERUSAHAAN']  = $this->actMain->get_mtabel('STATUS_PERUSAHAAN', 1, false, '', 'KODEDANUR','BC23');
        $data['DATA_CONT']          = $this->get_tblKontainer($car);//die('aa');
        $data['DATA_KEMASAN']       = $this->get_tblKemasaan($car); 
        $data['data_dokumen']       = $this->get_tblDokumen($car);
        $data['data_pgt']           = $data_pgt;
        $data['MAXLENGTH']          = '<input type="hidden" id="MAXLENGTH" value="' . $this->actMain->get_uraian("SELECT F_MAXLENGTH('t_bc23hdr') as MAXLENGTH", 'MAXLENGTH') . '"/>';
        return $data;
    }
    
    function setHeader(){
        $this->load->model("actMain");
        $data = $this->input->post();       
        if($data['act']=='save'){
            $data['HEADER']['CAR'] = $this->actMain->get_car('BC23');
        }
        $data['HEADER']['USAHANPWP']    = str_replace('.', '', str_replace('-', '', $data['HEADER']['USAHANPWP']));
        $data['HEADER']["CIFRP"]        = str_replace(",", "", $data['HEADER']["CIFRP"]);
        $this->actMain->insertRefernce('M_TRADER_SUPPLIER', $data['HEADER']);
        $this->actMain->insertRefernce('M_TRADER_KAPAL', $data['HEADER']);        
        $exe = $this->actMain->insertRefernce('T_BC23HDR', $data['HEADER']);
        $sql = "call bc23hitungPungutan('" . $CAR . "'," . $HEADER['KODE_TRADER'] . ")";
        //die($this->db->last_query());
        $msg = $this->validasiHeader($data['HEADER']['CAR']);
        if($exe){
            $this->actMain->set_car('BC23');
            $rtn =  "MSG|OK|".site_url('bc23/edit/'.$data['HEADER']['CAR'])."|".$msg;
        }
        else {
            $rtn = "MSG|ERR||Data header gagal diproses.";
        }
        return $rtn;
    }
    
    function getPnbp($car) {//print_r($car); die();
        $this->load->model("actMain");
        $data ['CAR']    = $car; //print_r($data ['CAR']);die();       
        if($car){
            $sql = "SELECT A.* 
                            FROM t_bc23pgt A
                            WHERE A.CAR = '".$car."' AND A.KODE_TRADER ='".$this->kd_trader."' AND KDBEBAN = '8'" ;
            $data['POPUP'] = $this->actMain->get_result($sql);            
        }else{            
        }
       
        return $data;  
    } 
    
    function setHeaderPnbp(){
        $this->load->model("actMain");
        $data = $this->input->post('POPUP');
        if($data['CAR']!=''){
            $data["NILBEBAN"]= str_replace(",", "", $data["NILBEBAN"]);
            $data['KDBEBAN'] = '8';            
            $array = array('CAR' => $data['CAR'], 'KODE_TRADER' => $this->kd_trader, 'KDBEBAN' => '8');
            $this->db->where($array);
            $this->db->delete('t_bc23pgt');
            
            $exe = $this->actMain->insertRefernce('t_bc23pgt', $data);
            if($exe){
                //$this->actMain->set_car('BC30');
                $rtn =  "MSG|OK|".site_url('bc23/edit/'.$data['CAR'])."|".$msg;
            }
            else {
                $rtn = "MSG|ERR||Data Pnbp gagal diproses.";
            }
            return $rtn;
        }
        
    }
    
    function validasiHeader($car) {
        $this->load->model('actMain');
        $tipe_trader = $this->newsession->userdata('TIPE_TRADER');
        $i = 0;
        //Validasi Header
        $query = "SELECT A.* 
                    FROM t_bc23hdr A 
                    WHERE A.CAR = '" . $car . "' AND A.KODE_TRADER = " . $this->kd_trader;
        $aH = $this->actMain->get_result($query);        
        if (count($aH) > 0) {
            $judul = 'Hasil validasi TPB Nomor Aju : ' . substr($car, 20) . ' tanggal ' . date('d-m-Y',strtotime(substr($car, 12, 8)));
            $query = "SELECT FLDNAME,MESSAGE from m_validasi WHERE SCRNAME = 'T_BC23HDR' AND MANDATORY = 1 ";
            $arrdataV = $this->actMain->get_result($query, true);
            foreach ($arrdataV as $aV) {
                if (trim($aH[$aV['FLDNAME']]) == '') {
                    $hasil .= substr('   ' . ++$i, -3) . '. ' . $aV['MESSAGE'] . ' Harus diisi<br>';
                }
            }
        
         unset($arrdataV);
            //Validasi Khusus
            //Cek jika jumlah barang < 0
        if ((int) $aH['JMBRG'] < 0) {
                $hasil .= substr('   ' . ++$i, -3) . '. Jumlah Barang Tidak Boleh Negatif<br>';
                $aH['JMBRG'] = 0;
            }
            //jika kode API isi status harus diisi
        if ($aH['APIKD'] == '' || $aH['APINO'] == '') {
                $aH['APIKD'] = '';
                $aH['APINO'] = '';
            }
       //Cek Pengisian harga
        switch ($aH['KDHRG']) {
                case '1': //CIF
                    $aH['ASURANSI'] = 0;
                    $aH['FREIGHT'] = 0;
                    $aH['FOB'] = 0;
                    break;
                 case '2': //CNF
                    $aH['FREIGHT'] = 0;
                    $aH['FOB'] = $aH['NILINV'] + $aH['BTAMBAHAN'] - $aH['DISKON'];
                    if ($aH['ASURANSI'] == 0 && $aH['KDASS']=='1') {
                        $hasil .= substr('   ' . ++$i, -3) . '. Asuransi Luar Negeri, harus diisi<br>';
                    }
                    break;
                 case '3': //FOB
                      $aH['FOB'] = $aH['NILINV'] + $aH['BTAMBAHAN'] + $aH['DISKON'];
                    if ($aH['FREIGHT'] == '0') {
                        $hasil .= substr('   ' . ++$i, -3) . '. Kode Harga FOB, Freight harus diisi<br>';
                    }
                    if ($aH['ASURANSI'] == '0' && $aH['KDASS'] == '1') {
                        $hasil .= substr('   ' . ++$i, -3) . '. Kode Harga FOB, dan bayar asuransi di LN, nilai asuransi harus diisi<br>';
                    }
                    break;
                  default :
                    $hasil .= substr('   ' . ++$i, -3) . '. Kode harga salah<br>';
                    break;
        }
        if ($aH['NILINV'] < 0 || $aH['ASURANSI'] < 0 || $aH['FREIGHT'] < 0 || $aH['CIF'] < 0) {
                $hasil .= substr('   ' . ++$i, -3) . '. Unsur harga tidak boleh kurang dari 0<br>';
            }            
        //Validasi Berat
        if ($aH['BRUTO'] < $aH['NETTO']) {
                $hasil .= substr('   ' . ++$i, -3) . '. Brutto tidak boleh lebih kecil dari netto<br>';
            }
        //cek pengisian dokumen
            $queryDok = "SELECT GROUP_CONCAT(DOKKD separator '|') as ADOK 
                         FROM t_bc23dok 
                         WHERE CAR = '" . $car . "' AND KODE_TRADER = " . $this->kd_trader;
            $strDok                 = $this->actMain->get_uraian($queryDok, 'ADOK');
            $dok['INVOICE']         = $this->actMain->findArr2Str($strDok, array('380'));
            $dok['BL']              = $this->actMain->findArr2Str($strDok, array('704', '705'));
            $dok['AWB']             = $this->actMain->findArr2Str($strDok, array('740', '741'));
            $dok['SKEP']            = $this->actMain->findArr2Str($strDok, array('912'));
            $dok['BC30']            = $this->actMain->findArr2Str($strDok, array('816'));
            $dok['BPBP']            = $this->actMain->findArr2Str($strDok, array('449'));
            $dok['IzinPNBPKala']    = $this->actMain->findArr2Str($strDok, array('820'));
        
        //Invoice
        if (!$dok['INVOICE']) {
            $hasil .= substr('   ' . ++$i, -3) . '. Dokumen Invoice belum diisi<br>';
        }
        //BL
        if (!$dok['BL'] && $aH['MODA'] == '1') {
            $hasil .= substr('   ' . ++$i, -3) . '. Dokumen BL tidak ada [Angkutan Laut]<br>';
        }
        //AWB
        if (!$dok['AWB'] && $aH['MODA'] == '4') {
            $hasil .= substr('   ' . ++$i, -3) . '. Dokumen AWB tidak ada [Angkutan Udara]<br>';
        }
        //BC30
        if (!$dok['AWB'] && $aH['JNSBARANG'] == '7') {
            $hasil .= substr('   ' . ++$i, -3) . '. Barang Reimpor harus mencantumkann dokumen PEB<br>';
        }   
        //SKEP
        if (!$dok['SKEP'] && trim($aH['TUJUAN'])== '1' && strstr('|04|05|06|', $aH['JNSBARANG'])) {
            $hasil .= substr('   ' . ++$i, -3) . '. Jenis Barang Tidak Berhubungan langsung (04,05,06) harus ada Skep Penangguhan<br>[kode 912]<br>';
        }
       
       //JNSBARANG
        $query = "SELECT GROUP_CONCAT(DISTINCT A.JNSBARANGDTL ORDER BY JNSBARANGDTL ASC SEPARATOR '|') as 'JNSBARANG' 
                FROM t_bc23dtl A  
                WHERE A.CAR = '" . $car . "' AND A.KODE_TRADER = " . $this->kd_trader;
        $jnsBarang = $this->actMain->get_uraian($query, 'JNSBARANG');
        
        if (strstr('|04|05|06|', $jnsBarang)) {
            $hasil .= substr('   ' . ++$i, -3) . '. Di Detil Jenis Barang Tidak Berhubungan langsung (04,05,06) harus ada Skep Penangguhan<br>[kode 912]<br>';
        }
                
        //KEMASAN
        if ((int) $aH['JMKMS'] <= 0) {
                $hasil .= substr('   ' . ++$i, -3) . '. Belum Mengisi data kemasan<br>';
                
            }
        //GUDANG
        if ($aH['TMPTBN'] != '') {
                $queryGdg = "SELECT COUNT(KDKPBC) as jmlKPBC 
                            FROM m_gudang 
                            WHERE KDKPBC ='" . $aH['KDKPBC'] . "' AND KDGDG ='" . $aH['TMPTBN'] . "'";
                $jmlGdg = $this->actMain->get_uraian($queryGdg, 'jmlKPBC');
                if ($jmlGdg <= 0) {
                    $hasil .= substr('   ' . ++$i, -3) . '. Kode gudang ' . $aH['TMPTBN'] . ' tidak ada di kantor ' . $aH['KDKPBC'];
                }
            }
        //TUJUAN-TUJUAN PENGIRIMAN
        if ($aH['TUJUAN'] == '1' && $aH['TUJUANKIRIM'] != '09' ) {
                $hasil .= substr('   ' . ++$i, -3) . '. Tujuan ke Kawasan Berikat, bukan untuk tujuan ditimbun.<br>';
            }
        if (($aH['TUJUAN'] == '5' || $aH['TUJUAN'] == '6')  && $aH['TUJUANKIRIM'] != '09' ) {
                $hasil .= substr('   ' . ++$i, -3) . '. Jika tujuan ke TLB/KDUB, tujuan kirim hanya Lainnya.<br>';
            }
        if (($aH['TUJUAN'] == '2' || $aH['TUJUAN'] == '3' || $aH['TUJUAN'] == '4')  && ($aH['TUJUANKIRIM'] != '01' && $aH['TUJUANKIRIM'] != '09'  )) {
                $hasil .= substr('   ' . ++$i, -3) . '. Jika tujuan ke GB, TPPB atau TBB, tujuan kirim hanya untuk Ditimbun atau Lainnya.<br>';
            }
        //Cek User
         if (!strpos("|02|03|04|08|09|10|", $aH['STATUS'])) {
             $query = "SELECT KODE_ID, ID, NAMA, ALAMAT, NO_PPJK, TANGGAL_PPJK 
                        FROM M_TRADER 
                        WHERE  KODE_TRADER = " . $this->kd_trader ;
             $strMy = $this->actMain->get_result($query);
                if ($tipe_trader == '1') { //Importir
                    $aH['USAHAID']   = $strMy['KODE_ID'];
                    $aH['USAHANPWP'] = $strMy['ID'];
                    $aH['USAHANAMA'] = $strMy['NAMA'];
                    $aH['USAHAALMT'] = $strMy['ALAMAT'];

                    $aH['PPJKID']    = '';
                    $aH['PPJKNPWP']  = '';
                    $aH['PPJKNAMA']  = '';
                    $aH['PPJKALMT']  = '';
                    $aH['PPJKNO']    = '';
                    $aH['PPJKTG']    = NULL;
                } elseif ($tipe_trader == '2') {//ppjk
                    $aH['PPJKID']    = $strMy['KODE_ID'];
                    $aH['PPJKNPWP']  = $strMy['ID'];
                    $aH['PPJKNAMA']  = $strMy['NAMA'];
                    $aH['PPJKALMT']  = $strMy['ALAMAT'];
                    $aH['PPJKNO']    = $strMy['NO_PPJK'];
                    $aH['PPJKTG']    = $strMy['TANGGAL_PPJK'];
                } else {
                    $hasil .= substr('   ' . ++$i, -3) . '. Tipe user yang anda gunakan tidak sesuai dengan dokumen yang anda entry.<br>';
                }
            }
            //Validasi Detail
            $data = $this->validasiDetilBatch($car);
            if (strlen($data['SERIAL']) > 0) {
                $arrSerial = explode(',', $data['SERIAL']);
                foreach ($arrSerial as $seri) {
                    $hasil .= substr('   ' . ++$i, -3) . '. Pengisian Detil ke- ' . $seri . ' belum benar<br>';
                }
            }
             //Resume detil error
            if (count($arrSerial) > 0) {
                $hasil .= substr('   ' . ++$i, -3) . '. Ada ' . count($arrSerial) . ' data barang yang belum benar.<br>';
            }      
            
            //harga di detil harus sama dengan di header
            if ($aH['NILINV'] != (int) $data['DNILINV']) {
                if (abs($aH['NILINV'] - $data['DNILINV']) > 1 || abs($aH['NILINV'] - $data['DNILINV']) < -1) {
                    $hasil .= substr('   ' . ++$i, -3) . '. Harga Header = ' . $aH['NILINV'] . ', Harga Detil = ' . (int) $data['DNILINV'] . '<br>';
                }
            }
            
           //update status 
            if ($aH['STATUS'] == ''){
                $aH['STATUS'] = '00';
            }
            #tidak ada perubahan jika status diliar 010 dan 020
            if (strpos('|00|01|', $aH['STATUS']))
                $aH['STATUS'] = ($i == 0) ? '01' : '00';
            //apply ke database
            $upd = $aH;
            $this->db->where(array('CAR' => $car, 'KODE_TRADER' => $this->kd_trader));
            $exec = $this->db->update('t_bc23hdr', $upd);
            unset($upd);
            
         //tulis informasi
            if ($i == 0) {
                $hasil .= '<br>------------------ Data Lengkap ------------------<br>';
            } else {
                $hasil .= '<br>--------------- Data Tidak Lengkap ---------------<br>';
            }
            
            $sql = "SELECT URAIAN 
                    FROM m_tabel 
                    WHERE MODUL = 'BC23' AND KDTAB = 'STATUS' AND KDREC = '".$aH['STATUS']."'";            
            $strStatus = $this->actMain->get_uraian($sql,'URAIAN');
            $hasil .= '<br> Status BC 2.3 '.$strStatus .' .<br>';
            switch ($aH['STATUS']){                                    
                case '00':
                    $hasil .= 'Data BC 2.3 belum selesai .<br>';    
                break;
                case '01':
                    $hasil .= 'Data BC 2.3 sudah selesai dan siap untuk dicetak/kirim ke KPBC .<br>';    
                break;
                case '02':
                    $hasil .= 'Data sudah dalam format EDIFACT dan siap untuk dikomunikasikan. Jika Anda akan melakukan update terhadap data ini, lakukan pembatalan EDIFACT .<br>';    
                break;
                case '03':
                    $hasil .= 'Data sudah berada dalam mailbox, namun belum diambil oleh Bea dan Cukai .<br>';    
                break;
                case '04':
                    $hasil .= 'Data sudah diambil oleh Bea dan Cukai, menunggu respon .<br>';    
                break;
                case '05':
                    $hasil .= 'Data Gagal ditransfer, lakukan pembatalan EDIFACT untuk kirim kembali.<br>';    
                break;
                case '06':
                    $hasil .= 'Data ditolak karena belum memenuhi persyaratan untuk diproses .<br>';    
                break;
                case '07':
                    $hasil .= 'Data BC 2.3 telah diterima dan sedang diproses oleh BC.<br>';    
                break;
                case '08':
                    $hasil .= 'Data BC 2.3 telah diterima dan sedang dalam proses penelitian oleh BC .<br>';    
                break;
                case '09':
                    $hasil .= 'Konfirmasi dokumen persyaratan Impor.<br>';    
                break;
                case '10':
                    $hasil .= 'Respon Pengecekan Kemasan .<br>';    
                break;
                case '11':
                    $hasil .= 'Respon Pemberitahuan Pemeriksaan Barang .<br>';    
                break;
                case '12':
                    $hasil .= 'Barang dapat keluar dari Kawasan Pabean, dengan pemeriksaan barang di tempat Importir.<br>';    
                break;
                case '13':
                    $hasil .= 'Barang dapat dikeluarkan dari Kawasan Pabean .<br>';    
                break;
                case '14':
                    $hasil .= '.<br>';    
                break;
                case '15':
                    $hasil .= 'Dokumen BC 2.3 ini dibatalkan .<br>';    
                break;
                case '16':
                    $hasil .= 'Terjadi kekurangan pembayaran BM dan/atau pungutan lain terhadap BC 2.3 ini.<br>';    
                break;
                case '17':
                    $hasil .= 'Perbaikan Dokumen BC 2.3 DITOLAK .<br>';    
                break;
                case '18':
                    $hasil .= 'Perbaikan Dokumen BC 2.3 DITERIMA .<br>';    
                break;
                
            }
                       
            if ($tipe_trader == '2' && ($aH['PPJKNO'] == '' || $aH['PPJKTG'] == '')) {
                $hasil .= 'Perhatian : Nomor/Tanggal PPJK belum diisi.<br>';
            }
            if ($aH['APIKD'] == '' ) {
                $hasil .= 'Perhatian : BC 2.3 tanpa Nomor API/T, PPh = 7,5 %.<br>';
            }
            
        }
        $info = '<br><br>' . $this->informasi($aH);
        $view['data'] = $hasil.$info;
        $view['judul'] = $judul;
        
        $msg = $this->load->view('notify', $view, true);
       
           
        unset($aH);
        return $msg;
    }
    
    function informasi($arrH) {//print_r($arrH);die();
        $this->load->model('actMain');
        $arti = explode('|', $this->actMain->get_uraian("SELECT URAIAN FROM M_TABEL WHERE MODUL = 'BC23' AND KDTAB = 'ARTI_STATUS' AND KDREC = '" . $arrH['STATUS'] . "'", 'URAIAN'));
        $hasil = 'Informasi Data BC 2.3<br>';
        $hasil .= '--------------------------------------------------<br>';
        $hasil .= 'Nomor Aju              : ' . $arrH['CAR'] . '<br>';
        /*$hasil .= 'Status                 : ' . $arrH['URSTATUS'] . ' ( ' . $arti[0] . ' ) <br>';
        $hasil .= 'Keterangan Status      : ' . $arti[1] . ' <br>';*/
        $hasil .= 'Jumlah Barang          : ' . str_pad(number_format($arrH['JMBRG']), 20, ' ', STR_PAD_LEFT) . ' Item(s)<br>';
        $hasil .= 'Jumlah Kontainer       : ' . str_pad(number_format($arrH['JMCONT']), 20, ' ', STR_PAD_LEFT) . ' Kontainer<br>';
        $hasil .= 'Bruto                  : ' . str_pad(number_format($arrH['BRUTO'], 4), 20, ' ', STR_PAD_LEFT) . ' Kgm<br>';
        $hasil .= 'Netto                  : ' . str_pad(number_format($arrH['NETTO'], 4), 20, ' ', STR_PAD_LEFT) . ' Kgm<br>';
        $hasil .= 'CIF (' . $arrH['KDVAL'] . ')              : ' . str_pad(number_format($arrH['CIF'], 2), 20, ' ', STR_PAD_LEFT) . ' <br>';
        $hasil .= 'SNRF                   :' . $arrH['SNRF'] . ' <br>';
        $hasil .= 'Respons                :<br>';

        $SQL = "SELECT CONCAT(A.RESKD,' - ', B.URAIAN) AS RESPON "
                . "FROM T_BC23RESTPB A LEFT JOIN M_TABEL B ON B.MODUL = 'BC23' AND B.KDTAB = 'JENIS_RESPON' AND B.KDREC = A.RESKD "
                . 'WHERE A.KODE_TRADER = ' . $this->kd_trader . " AND A.CAR = '" . $arrH['CAR'] . "'";
        $res = $this->actMain->get_result($SQL, true);
        if (isset($res)) {
            foreach ($res as $a) {
                $hasil .= '                       : ' . $a['RESPON'].'<br>';
            }
        }
        $hasil .= 'Pemberitahu            : ' . $arrH['NAMATTD'];
        unset($arrH);
        return $hasil;
    }
    
    function validasiDetilBatch($car) {
        $this->load->model('actMain');
        $sql = "SELECT * FROM m_validasi WHERE SCRNAME = 't_bc23dtl' AND MANDATORY =1 ";
        $aarVal = $this->actMain->get_result($sql, true);
        $sql = "SELECT  *
                   FROM    T_BC23DTL A LEFT JOIN T_BC23TRF C ON A.KODE_TRADER = C.KODE_TRADER AND A.CAR = C.CAR AND A.SERIAL = C.SERIAL
                                       LEFT JOIN T_BC23FAS B ON A.KODE_TRADER = B.KODE_TRADER AND A.CAR = B.CAR AND A.SERIAL = B.SERIAL
                   WHERE   A.CAR = '" . $car . "' AND A.KODE_TRADER ='" . $this->kd_trader . "'"; //die($sql);
        $arrDtl = $this->actMain->get_result($sql, true); //print_r($arrDtl);die();
        $ada['FASILITAS'] = 0;
        $this->db->query("UPDATE t_bc23dtl SET DTLOK = '1' WHERE CAR = '" . $car . "' AND KODE_TRADER ='" . $this->kd_trader . "'"); //khusnuzon di buat lengkap semua
        foreach ($arrDtl as $aD) {
            $hasil['DNILINV'] += $aD['DNILINV'];
            $i = 0;
            //via table m_validasi
            foreach ($aarVal as $aV) {
                if ($aD[$aV['FLDNAME']] == '') {
                    $i++;
                    //$test .= $aV['FLDNAME'];
                }
                if ($aD[$aV['FLDNAME']] == '0' && $aV['FLDNAME'] == 'NUMBER') {
                    $i++;
                    //$test .= $aV['FLDNAME'];
                }
            }
            unset($aarVal);
            //validasi kusus
            //Nilai Barang negatif
            if ($aD['DNILINV'] < 0) {
                $i++;
                //$test .= '1';
            }
            if ($aD['KDFASBM'] != '' && $aD['KDFASDTL'] == '') {
                $i++;
                //$test .= '2-'.$aD['SERIAL'];//print_r($aD);die();
            }
            if ($aD['KDFASDTL'] != '') { //untuk pengecekan di header.
                $ada['FASILITAS'] ++;
                //$test .= '3';
            }
            //Jika nilai detil ada yang 0 (NVC) gak ngerti maksute
            //Cek Panjang HS
            if (strlen($aD['NOHS']) < 9) {
                $i++;
                //$test .= '4';
            }
            //Cek jika kode jenis Tarif kosong
            if ($aD['KDTRPBM'] == '') {
                $i++;
                //$test .= '5';
            }
            if ($aD['KDTRPBM'] == '2') {
                if ((int) $aD['TRPBM'] == 0 || $aD['KDSATBM'] == '' || (int) $aD['SATBMJM'] == 0) {
                    $i++;
                    //$test .= '6';
                }
            }
            if ($i > 0) {
                $hasil['SERIAL'].=',' . $aD['SERIAL'];
            }
        }
        unset($arrDtl);
        $hasil['SERIAL'] = substr($hasil['SERIAL'], 1);
        $hasil['FASILITAS'] = $ada['FASILITAS'];
        //print_r($hasil);die($test);
        if ($hasil['SERIAL'] != '') {
            $sql = "UPDATE t_bc23dtl SET DTLOK = '0' WHERE CAR = '" . $car . "' AND KODE_TRADER =" . $this->kd_trader . " AND SERIAL IN ('" . str_replace(',', "','", $hasil['SERIAL']) . "') "; //die($sql);
            $this->db->query($sql);
        }
        return $hasil;
    }
    
    function lsvHeaderTPB($car){
        $this->load->library('newtable');
        $this->load->model("actMain"); 
        $jenis = "tpb";
        $title = "Daftar Permohonan TPB";
        $SQL = "SELECT concat(SUBSTRING(A.CAR,1,6),'-',SUBSTRING(A.CAR,7,6),'-',SUBSTRING(A.CAR,13,8),'-',SUBSTRING(A.CAR,21,8))  AS 'No Aju', 
                       A.KDKPBC, A.PASOKNAMA AS 'Pemasok', A.USAHANAMA AS 'Importir' ,A.JMBRG AS 'Jml Barang',A.JMCONT AS 'Jml Kontainer', 
                       B.URAIAN as 'Status', A.CAR
                FROM t_bc23hdr A LEFT JOIN m_tabel B ON B.KDREC = A.STATUS AND B.KDTAB = 'STATUS' AND B.MODUL = 'BC23'
                WHERE A.KODE_TRADER = '".$this->kd_trader."' ";        
        $prosesnya = array(  
            'Create'        => array('ADDAJAX', site_url('bc23/create'), '0', '_content_'),
            'View'          => array('GETAJAX', site_url('bc23/edit'), '1', '_content_'),
            'Cetak'         => array('GETPOP', site_url('bc23/beforecetak'), '1', ' CETAK DOKUMEN TPB |400|250'),
            'Upload B23'    => array('GETPOP', site_url('bc23/uploadb23/viewform'), '0', ' UPLOAD FILE B23 |400|200'),  
            'Download B23'  => array('DOWNLOADAJAX', site_url('bc23/downloadb23/download'), '1', ' DOWNLOAD FILE B23 |400|200'),
            'Upload Excel'  => array('GETPOP', site_url('bc23/uploadexcel/viewformex'), '1', ' UPLOAD EXCEL |400|200'),  
            'Hapus '        => array('DELETEAJAX', site_url('bc23/deleteTPB'), 'N', '_content_')); 
        $this->newtable->menu($prosesnya); 
        $this->newtable->keys(array('CAR'));
        $this->newtable->hiddens(array('CAR'));
        $this->newtable->search(array(
            array('CAR',        'No. Aju'),
            array('A.PASOKNAMA',   'Pemasok'),  
            array('A.USAHANAMA', 'Importir'),
            array('A.KDKPBC','Kode KPBC'),
            array('A.STATUS', 'Status', 'tag-select', $this->actMain->get_mtabel('STATUS',1,false,'','KODEUR','BC23'))));
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : 
        $this->input->post("uri");
        $this->newtable->action(site_url("bc23/daftarTPB"));
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->orderby(1, 2);
        $this->newtable->sortby("DESC");
        $this->newtable->tipe_check('radio');
        $this->newtable->set_formid("f".$tipe);
        $this->newtable->set_divid("div".$tipe);
        $this->newtable->rowcount(20);
        $this->newtable->tipe_proses('button');
        $this->newtable->clear();
        $tabel .= $this->newtable->generate($SQL);       
        $arrdata = array('title' => $title,
                         'judul' => '',
                         'tabel' => $tabel,
                         'jenis' => $jenis,
                         'tipe'  => $tipe);
        if ($this->input->post("ajax")){
            return $tabel;
        }
        else{
            return $arrdata;
        }
    }
            
    function delTPB($car){
        $this->load->model("actMain");
        $sql = "SELECT CONCAT(a.`STATUS`,'|',b.URAIAN) as STATUS 
                FROM t_bc23hdr a left join m_tabel b on b.MODUL = 'BC23' AND 
                                                   b.KDTAB = 'STATUS' AND 
                                                   b.KDREC =  a.STATUS 
                WHERE a.KODE_TRADER = '".$this->kd_trader."' AND 
                      a.CAR = '".$car."'";
        $sts = $this->actMain->get_uraian($sql,'STATUS');
        $arrSts = explode('|', $sts);
        if(strpos('|020|030|040|050|060|',$arrSts[0])){
            $rtn = 'MSG|ER|' . site_url('bc23/daftarTPB/draft') . '|Dokumen tidak dapat dihapus karena berstatus '.$arrSts[1].'.';
        }else{
            $key    = array('KODE_TRADER' =>  $this->kd_trader, 'CAR' => $car);
            $this->db->where($key);
            $tbl    = explode(',','t_bc23con,t_bc23dok,t_bc23dtl,t_bc23fas,t_bc23hdr,t_bc23kms,t_bc23pgt,t_bc23restpb,t_bc23trf,t_bc23resppb,t_bc23ressppbc,t_bc23ressppbh,t_bc23ressppbk');
            $exec = $this->db->delete($tbl);
            $rtn = 'MSG|OK|' . site_url('bc23/daftarTPB/draft') . '|Delete data dokumen TPB berhasil.';            
        }
        return $rtn;
    }
    
    private function get_tblKontainer($car = '') {//die('aaa');
        $data    = $this->lstKontainer($car);
        $arrdata = $this->lstDetil('kontainer', $car);
        $data['list'] = $this->load->view('list', $arrdata, true);
        $data['type'] = 'kontainer';
        return $this->load->view("bc23/lst", $data, true);//die('aa');
    }

    private function get_tblKemasaan($car) {
        $data = $this->lstKemasan($car);
        $arrdata = $this->lstDetil('kemasan', $car);
        $data['list'] = $this->load->view('list', $arrdata, true);
        $data['type'] = 'kemasan';
        return $this->load->view("bc23/lst", $data, true);
    }
    
    private function get_tblDokumen($car) {
        $data = $this->lstDokumen($car);
        $arrdata = $this->lstDetil('dokumen', $car);
        $data['list'] = $this->load->view('list', $arrdata, true);
        $data['type'] = 'dokumen';
        return $this->load->view("bc23/lst", $data, true);
    }
        
    function lstKontainer($car, $contno = '') {
        $data = array();
        $this->load->model("actMain");
        if ($car && $contno) {
            $query = "SELECT * 
                      FROM t_bc23con 
                      WHERE CAR = '$car' AND 
                            CONTNO = '$contno' AND 
                            KODE_TRADER = '$this->kd_trader'";
            $hasil = $this->actMain->get_result($query);
            $data['KONTAINER'] = array_change_key_case($hasil, CASE_UPPER);
            $data['act'] = 'update';
        }
        else {
            $data['act'] = 'save';
        }
        $data['TIPE']             = $this->actMain->get_mtabel('JENIS_CONTAINER');
        $data['UKURAN']           = $this->actMain->get_mtabel('UKURAN_CONTAINER');
        $data['KONTAINER']['CAR'] = $car;
        return $data;
    }

    function lstKemasan($car = "", $jns = "", $merk = "") {
        $data = array();
        $this->load->model("actMain");
        if ($car && $jns && $merk) {
            $query = "SELECT CAR, JNKEMAS, JMKEMAS, MERKKEMAS, f_kemas(JNKEMAS) URAIAN 
                      FROM t_bc23kms 
		      WHERE KODE_TRADER=" . $this->kd_trader . " AND 
                            CAR = '" . $car . "' AND 
                            JNKEMAS = '" . $jns . "' AND 
                            MERKKEMAS = '" . $merk . "'";
            $hasil = $this->actMain->get_result($query);
            $data['KEMASAN'] = array_change_key_case($hasil, CASE_UPPER);
            $data['act'] = 'update';
        }
        else {
            $data['act'] = 'save';
        }
        $data['KEMASAN']['CAR'] = $car;//print_r($data);die();
        return $data;
    }
    
    function lstDokumen($car , $DOKKD = '', $DOKNO = '') {
        $this->load->model("actMain");
        if ($car && $DOKKD && $DOKNO) {
            $query = "SELECT * 
                      FROM t_bc23dok 
                      WHERE KODE_TRADER='" . $this->kd_trader . "' AND 
                            CAR = '$car' AND 
                            DOKKD = '$DOKKD' AND 
                            DOKNO = '$DOKNO'";
            $hasil = $this->actMain->get_result($query);
            $data['DOKUMEN'] = array_change_key_case($hasil, CASE_UPPER);
            $data['act'] = 'update';
        }
        else {
            $data['act'] = 'save';
        }
        $data['DOKUMEN']['CAR'] = $car;
        $data['DOKKD'] = $this->actMain->get_combobox("SELECT KDREC,
                                                     CONCAT(KDREC,' - ',URAIAN) as KODEDANUR
                                             FROM M_TABEL WHERE MODUL = 'BC23' AND KDTAB in ('DOKUMEN01','DOKUMEN02','DOKUMEN03') ORDER BY KDTAB,URUT ", 'KDREC', 'KODEDANUR', true);
        return $data;
    }
    
    function getPungutan($car) {//print_r($car);die();
        $this->load->model("actMain");
        $query = "SELECT KDBEBAN,
                         KDFASIL,
                         NILBEBAN 
                  FROM  t_bc23pgt 
                  WHERE CAR='" . $car . "' AND KODE_TRADER = '" . $this->kd_trader . "'";
        // print_r($query);die();
        $arrPGT = $this->actMain->get_result($query, true); //print_r($arrPGT);die();
        foreach ($arrPGT as $row) {
            $data_pgt[$row['KDBEBAN']][$row['KDFASIL']] += $row['NILBEBAN'];
            $data_pgt['TOTAL'][$row['KDFASIL']] += $row['NILBEBAN'];
        }
        return $data_pgt;
        
    }
    
    function getPungutanTarif($car) {//print_r($car);die();
        $this->load->model("actMain");
        $query = "SELECT KDBEBAN,
                         KDFASIL,
                         NILBEBAN 
                  FROM  t_bc23pgt 
                  WHERE CAR='" . $car . "' AND KODE_TRADER = '" . $this->kd_trader . "'";
        // print_r($query);die();
        $arrPGT = $this->actMain->get_result($query, true); //print_r($arrPGT);die();
        foreach ($arrPGT as $row) {
            $data_pgt[$row['KDBEBAN']][$row['KDFASIL']] += $row['NILBEBAN'];
            $data_pgt['TOTAL'][$row['KDFASIL']] += $row['NILBEBAN'];
        }
        return $data_pgt;
        
    }
    
    function setKontainer($type, $car) {
        if ($type == 'save' || $type == 'update') {
            $this->load->model("actMain");
            $data = $this->input->post('KONTAINER');
            if (!$data['CAR']) {
                return "MSG|ERR||Simpan data kontainer Gagal.<br>Data header belum tersimpan.";
            }
            $data['KODE_TRADER'] = $this->kd_trader;
            $exec = $this->actMain->insertRefernce('t_bc23con',$data);
            if ($exec) {
                $sql = "call P_BC23UPDCON('" . $data['CAR'] . "'," . $this->kd_trader . ")";
                $this->db->query($sql);
                return "MSG|OK|".site_url('bc23/daftarDetil/kontainer/'.$data['CAR'])."|Data kontainer berhasil disimpan.";
            } else {
                return "MSG|ERR||Proses data kontainer gagal.";
            }
        }
        else if ($type == 'delete'){
            $this->input->post('tb_chkfkontainer') ;
            foreach ($this->input->post('tb_chkfkontainer') as $chkitem){
                $arrchk = explode("|", $chkitem);
                $car    = $arrchk[0];
                $arrSeri .= ",'" . $arrchk[1] . "'";
            }
            $arrSeri = substr($arrSeri, 1);
            $sql = "DELETE FROM T_BC23CON WHERE CAR='" . $car . "' AND KODE_TRADER = '" . $this->kd_trader . "' AND CONTNO in (" . $arrSeri . ')';
            $exec = $this->db->query($sql);
            if ($exec){
                $sql = "call P_BC23UPDCON('" . $car . "'," . $this->kd_trader . ')';
                $this->db->query($sql);
                return "MSG|OK|".site_url('bc23/daftarDetil/kontainer/'.$car)."|Data kontainer berhasil didelete.";
            }
            else {
                return "MSG|ERR||Data kontainer gagal didelete.";
            }
        }
    }

    function setKemasan($type,$car) {//print_r();die()
        if ($type == 'save' || $type == 'update'){
            $this->load->model('actMain');
            $data = $this->input->post('KEMASAN');
            if (!$data['CAR']) {//print_r($data);die();
                return "MSG|ERR||Simpan data kemasan Gagal.<br>Data header belum tersimpan.";
            }
            $data['KODE_TRADER'] = $this->kd_trader;
            $exec = $this->actMain->insertRefernce('t_bc23kms',$data);
            if ($exec) {
                $sql = "call P_BC23UPDKMS('" . $data['CAR'] . "'," . $this->kd_trader . ")";
                $this->db->query($sql);
                return "MSG|OK|".site_url('bc23/daftarDetil/kemasan/'.$data['CAR'])."|Data kemasan berhasil disimpan.";
            }
            else {
                return "MSG|ERR||Proses data kemasan gagal.";
            }
        }
        else if ($type == 'delete') {
            foreach ($this->input->post('tb_chkfkemasan') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $data['CAR']        = $arrchk[0];
                $data['JNKEMAS']    = $arrchk[1];
                $data['MERKKEMAS']  = $arrchk[2];
                $data['KODE_TRADER']= $this->kd_trader;
                $this->db->where($data);
                $exec = $this->db->delete('t_bc23kms');
            }
            if ($exec) {
                return 'MSG|OK|'.site_url('bc23/daftarDetil/kemasan/'.$car).'|Data kemasan berhasil didelete.';
            }
            else {
                return 'MSG|ERR||Data kemasan gagal didelete.';
            }
        }
    }
    
    function setDokumen($type,$car) {
        if ($type == 'save' || $type == 'update') {
            $this->load->model('actMain');
            $data = $this->input->post('DOKUMEN');
            if (!$data['CAR'] || strpos('-'.$data['DOKKD'],'=')>0) {
                return "MSG|ERR||Simpan data dokumen Gagal.<br>Data header belum tersimpan.";
            }
            $data['KODE_TRADER'] = $this->kd_trader;
            $exec = $this->actMain->insertRefernce('t_bc23dok',$data);
            if ($exec) {
                return "MSG|OK|".site_url('bc23/daftarDetil/dokumen/'.$data['CAR'])."|Data dokumen berhasil disimpan.";
            } else {
                return "MSG|ERR||Proses data dokumen gagal.";
            }
        }
        else if ($type == 'delete') {
            foreach ($this->input->post('tb_chkfdokumen') as $chkitem) {
                $arrchk             = explode("|", $chkitem);
                $data['CAR']        = $arrchk[0];
                $data['DOKKD']      = $arrchk[1];
                $data['DOKNO']      = $arrchk[2];
                $data['KODE_TRADER']= $this->kd_trader;
                $this->db->where($data);
                $exec = $this->db->delete('t_bc23dok');
            }
            if ($exec) {
                return 'MSG|OK|'.site_url('bc23/daftarDetil/dokumen/'.$car).'|Data dokumen berhasil didelete.';
            } else {
                return 'MSG|ERR||Data dokumen gagal didelete.';
            }
        }
    }
    
    function lstDetil($type = '', $aju){//die($aju);
        $this->load->library('newtable');
        $this->newtable->keys = '';
        if ($type == "barang") {
            $this->load->model('actMain');
            $SQL = "SELECT BRGURAI as 'URAIAN BARANG',f_formaths(NOHS) 'KODE HS',
                           SERIAL ,DNILINV AS 'Nilai Invoice', KEMASJM AS 'JLM. KEMASAN', 
                           CONCAT(KEMASJN,' - ',f_kemas(KEMASJN)) AS 'JENIS KEMASAN', 
                           NETTODTL AS 'NETTO', b.URAIAN as Status,CAR, NOHS
                     FROM t_bc23dtl a left join m_tabel b on b.MODUL = 'BC23' AND 
                                                        b.KDTAB = 'STATUS_DETIL' AND 
                                                        b.KDREC = a.DTLOK 
                     WHERE CAR= '$aju' AND 
                           KODE_TRADER = '" . $this->kd_trader . "'";//print_r($SQL);die();
            $this->newtable->keys(array('CAR', 'SERIAL', 'NOHS'));
            $this->newtable->hiddens(array('CAR', 'NOHS'));
            $this->newtable->search(array(array('BRGURAI', 'Uraian Barang'),
                array('NOHS', 'Kode HS'),
                array('DTLOK', 'Status', 'tag-select', $this->actMain->get_mtabel('STATUS_DETIL'))));
            $this->newtable->orderby("SERIAL");
            $this->newtable->sortby("DESC");
        }
        else if ($type == "kemasan") {
            $SQL = "SELECT  JMKEMAS AS 'JUMLAH', 
                            JNKEMAS AS 'SERI',  
                            CONCAT(JNKEMAS,' - ',f_kemas(JNKEMAS)) AS 'KODE', 
                            MERKKEMAS AS 'MEREK', 
                            CAR 
                    FROM t_bc23kms 
                    WHERE CAR= '$aju' AND KODE_TRADER = '" . $this->kd_trader . "'";
            $this->newtable->keys(array("CAR", "SERI", "MEREK"));
            $this->newtable->hiddens(array("CAR", "SERI"));
            $this->newtable->search(array(array('JNKEMAS', 'Kode'), array('MERKKEMAS', 'Merek')));
            $this->newtable->orderby(4);
            $this->newtable->sortby("DESC");
        } 
        else if ($type == "kontainer") {
            $SQL = "SELECT  CONTNO AS 'Nomor', 
                            CONTNO AS 'SERI', 
                            f_ref('UKURAN_CONTAINER',CONTUKUR) AS 'Ukuran', 
                            f_ref('JENIS_CONTAINER',CONTTIPE) AS 'Tipe', 
                            CAR
                    FROM t_bc23con 
                    WHERE CAR= '$aju' AND KODE_TRADER = '" . $this->kd_trader . "'";
            $this->newtable->keys(array("CAR", "SERI"));
            $this->newtable->hiddens(array("CAR", "SERI"));
            $this->newtable->search(array(array('CONTNO', 'Nomor'), array('UKURAN_CONTAINER', 'Ukuran'), array('JENIS_CONTAINER', 'Tupe')));
            $this->newtable->orderby(4);
            $this->newtable->sortby("DESC");
        } 
        else if ($type == "dokumen") {
            $SQL = "SELECT  DOKKD AS 'Kode',
                            f_ref('KODE_DOKUMEN',DOKKD) AS 'Jenis', 
                            DOKNO AS 'Nomor',
                            DATE_FORMAT(DOKTG,'%Y-%m-%d') AS 'Tanggal', 
                            CAR, 
                            DOKKD AS SERI 
                    FROM t_bc23dok  
                    WHERE CAR= '$aju' AND KODE_TRADER = '" . $this->kd_trader . "'";
            $this->newtable->keys(array('CAR', 'SERI', 'Nomor'));
            $this->newtable->hiddens(array('CAR', 'SERI'));
            $this->newtable->search(array(
                array('DOKKD', 'Kode'),
                array("f_ref('KODE_DOKUMEN',DOKKD)", 'Jenis'),
                array('DOKNO', 'Nomor'),
                array("DATE_FORMAT(DOKTG,'%Y-%m-%d')", 'Tanggal', 'tag-tanggal')));
            $this->newtable->orderby(4);
            $this->newtable->sortby("DESC");
        } 
        else {
            return "Failed";
            exit();
        }
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->action(site_url('bc23/daftarDetil/' . $type . '/' . $aju));
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->rowcount(10);
        $this->newtable->clear();
        $this->newtable->set_formid("f" . $type);
        $this->newtable->set_divid("div" . $type);
        $process = array(
            'Tambah '   . $type => array('ADDAJAX', site_url('bc23/' . $type . '/' . $aju), '0', 'f' . $type . '_form'),
            'Ubah '     . $type => array('EDITAJAX', site_url('bc23/' . $type . '/' . $aju), '1', 'f' . $type . '_form'),
            'Hapus '    . $type => array('DELETEAJAX', site_url('bc23/' . $type . '/' . $aju), 'N', 'f' . $type . '_list'));
        $this->newtable->menu($process);
        $tabel = $this->newtable->generate($SQL);
        $arrdata = array("tabel" => $tabel);
        if ($this->input->post("ajax"))
            return $tabel;
        else
            return $arrdata;
    }
    
    function lstBarang($car = "", $seri = "") {
        $this->load->model("actMain");
        if ($car && $seri) {
            $query = "SELECT a.*, 
                             b.*,
                             c.*,
                             f_ref('SKEP_FASILITAS',a.KDFASDTL) KDFASDTLUR,
                             f_kemas(a.KEMASJN) KODE_KEMASANUR,
                             f_negara(a.BRGASAL) NEGARA_ASALUR, 
                             f_satuan(a.KDSAT) KODE_SATUANUR,
                             d.KDHRG ,
                             d.FREIGHT   AS HFREIGHT,
                             d.NILINV    AS HINVOICE,
                             d.ASURANSI  AS HASURANSI,
                             d.BTAMBAHAN AS HBTAMBAHAN,
                             d.DISKON    AS HDISKON,
                             d.NDPBM     AS HNDPBM,
                             d.ApiNo     AS 'APINO',
                             e.URAIAN    AS 'URDTLOK',
                             a.CAR       AS 'CAR',
                             a.SERIAL    AS 'SERIAL',
                             a.KODE_TRADER AS 'KODE_TRADER'
                      FROM t_bc23dtl a LEFT JOIN t_bc23trf c ON c.KODE_TRADER = a.KODE_TRADER AND c.CAR=a.CAR AND c.SERIAL=a.SERIAL
                                       LEFT JOIN t_bc23fas b ON b.KODE_TRADER = a.KODE_TRADER AND b.CAR=a.CAR AND b.SERIAL=a.SERIAL
                                       LEFT JOIN t_bc23hdr d ON d.KODE_TRADER = a.KODE_TRADER AND d.CAR=a.CAR
                                       LEFT JOIN m_tabel e   ON e.MODUL = 'bc23' AND e.KDTAB = 'STATUS_DETIL' AND e.KDREC = a.DTLOK
                      WHERE a.CAR = '" . $car . "' AND a.SERIAL = '" . $seri . "' AND a.KODE_TRADER = " . $this->kd_trader;
            $hasil = $this->actMain->get_result($query);
            $data['DETIL']  = array_change_key_case($hasil, CASE_UPPER);
            $data['act']    = 'update';
        } 
        else if ($car != '') {
            $query = "SELECT  0 AS 'SERI_HS', 0 AS 'KEMASJM', 0 AS 'NETTODTL', 0 AS 'DNILINV', 0 AS 'BTDISKON', 0 AS 'JMLSAT', 0 AS 'FOB', 
                              0 AS 'DCIF', 0 AS 'CIFRP', 0 AS 'TRPBM', 0 AS 'FASBM', 0 AS 'TRPPPN', 0 AS 'FASPPN', 0 AS 'TRPPBM', 0 AS 'FASPBM', 
                              0 AS 'FASPPH', 0 AS 'TRPCUK', 0 AS 'SATCUKJM',0 AS 'FASCUK', 0 AS 'TRPPPH', 
                   (SELECT IFNULL(MAX(SERIAL) + 1,1) FROM t_bc23dtl WHERE CAR = t_bc23hdr.CAR  AND KODE_TRADER = t_bc23hdr.KODE_TRADER) AS 'SERIAL',
                              'Tidak Lengkap' AS 'URDTLOK', 
                              KDHRG ,
                              FREIGHT   AS HFREIGHT,
                              NILINV    AS HINVOICE,
                              ASURANSI  AS HASURANSI,
                              BTAMBAHAN AS HBTAMBAHAN,
                              DISKON    AS HDISKON,
                              NDPBM     AS HNDPBM,
                              ApiNo     AS 'APINO',
                              CAR
		      FROM t_bc23hdr 
                      WHERE CAR='" . $car . "' AND KODE_TRADER = " . $this->kd_trader;//die($query);
            $dataarray = $this->actMain->get_result($query);
            $data['act']    = 'save';
            $data['DETIL']  = $dataarray;
        }
        $data['JENIS_TARIF']= $this->actMain->get_mtabel('JENIS_TARIF', 1, false, "AND KDREC IN ('1','2')");
        $data['KOMODITI']   = $this->actMain->get_mtabel('KOMODITI', 1, false, '', 'KODEDANUR', 'BC23');
        $data['PENGGUNAAN']   = $this->actMain->get_mtabel('PENGGUNAAN', 1, false, '', 'KODEDANUR', 'BC23');
        $data['JNSBARANG']   = $this->actMain->get_mtabel('JNSBARANG', 1, false, '', 'KODEDANUR', 'BC23');
        $data['MAXLENGTH']  = '<input type="hidden" id="MAXLENGTH" value="' . $this->actMain->get_uraian("SELECT F_MAXLENGTH('t_bc23dtl') as MAXLENGTH", 'MAXLENGTH') . '"/>';
        $data['JMLDTL']  = $this->actMain->get_uraian("SELECT COUNT(*) JML FROM T_bc23DTL WHERE CAR='" . $car . "' AND KODE_TRADER = " . $this->kd_trader, 'JML');
        $data['JENIS_KERINGANAN']  = $this->actMain->get_mtabel('JENIS_KERINGANAN' , 1, false, '', 'KODEDANUR', 'BC23');
        return $data;
    }
    
    function setBarang($type,$car) {
        $this->load->model('actMain');
        if ($type == 'save' || $type == 'update'){
            $post = $this->input->post();
            $car = $this->input->post('CAR');
            if ($type == 'save') {
                $seri = (int) $this->actMain->get_uraian("SELECT MAX(SERIAL) AS MAXSERI FROM t_bc23dtl WHERE CAR='" . $car . "'", "MAXSERI") + 1;
            }
            else {
                $seri = $post['SERIAL'];
            }
            $BARANG                     = $post['DETIL'];
            $BARANG["KODE_TRADER"]      = $this->kd_trader;
            $BARANG["CAR"]              = $car;
            $BARANG["NOHS"]             = str_replace(".", "", $BARANG["NOHS"]);
            $BARANG["SERIAL"]           = $seri;
            unset($BARANG["INVOICE"]);
            unset($BARANG["CIFRP"]);
            
            $exec['barang']             = $this->actMain->insertRefernce('t_bc23dtl', $BARANG, true);
            $exec['trader_barang']      = $this->actMain->insertRefernce('m_trader_barang', $BARANG, true);

            $FASILITAS                  = $post['FASILITAS'];
            $FASILITAS["KODE_TRADER"]   = $this->kd_trader;
            $FASILITAS["CAR"]           = $car;
            $FASILITAS["SERIAL"]        = $seri;
            $exec['fasilitas']          = $this->actMain->insertRefernce('t_bc23fas', $FASILITAS, true);

            $TARIF                      = $post['TARIF'];
            $TARIF["KODE_TRADER"]       = $this->kd_trader;
            $TARIF["CAR"]               = $car;
            $TARIF["SERIAL"]            = $seri;
            $TARIF["SERITRP"]           = $BARANG["SERITRP"];
            $TARIF["NOHS"]              = $BARANG["NOHS"];
            $exec['tarif']              = $this->actMain->insertRefernce('t_bc23trf', $TARIF, true);
            $exec['trader_tarif']       = $this->actMain->insertRefernce('m_trader_tarif', $TARIF, true);
           
            $sql = "call bc23hitungPungutan('" . $car . "','" . $this->kd_trader . "')";
            $this->db->query($sql); 
            
            $sql = "call P_BC23UPDDTL('" . $car . "'," . $this->kd_trader . ")";
            $this->db->query($sql);
            
            $msg = $this->validasiDetail($car, $seri);           
            if ($exec['barang'] && $exec['trader_barang'] && $exec['fasilitas'] && $exec['tarif']) {
                return "MSG|OK|".site_url('bc23/daftarDetil/barang/'.$car).'/|<pre style="float: left;">'.$msg.'</pre>';
            } else {
                return "MSG|ERR||Proses data barang gagal.";
            }
        }
        else if ($type == "delete") {
            foreach ($this->input->post('tb_chkfbarang') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $car = $arrchk[0];
                $seri = $arrchk[1];
                $key = array('KODE_TRADER' => $this->kd_trader, 'CAR' => $car, 'SERIAL' => $seri);
                $tables = array('t_bc23dtl', 't_bc23fas', 't_bc23trf');
                $this->db->where($key);
                $this->db->delete($tables);
                //update urutkan serial);
                $sql = "call bc23UrutkanSerial(" . $this->kd_trader . ",'" . $car . "'," . $seri . ")";
                $exec = $this->db->query($sql);
            }
            
            if ($exec) {    
                return "MSG|OK|".site_url('bc23/daftarDetil/barang/'.$car).'/|Delete data barang berhasil.';
            } else {
                return "MSG|ERR||Delete data barang gagal.";
            }
            die();
        }
    }
    
    function validasiDetail($car, $seri) {              
        $this->load->model('actMain');
        $sql = "SELECT * FROM m_validasi WHERE SCRNAME = 'T_BC23DTL' AND MANDATORY =1";
        $aarVal = $this->actMain->get_result($sql, true);               
        //Validasi Detail
        $sql = " SELECT A.*, C.*,  B.KDFASBM, B.FASBM, B.KDFASCUK, B.FASCUK, B.KDFASPPN, B.FASPPN, B.KDFASPBM, B.FASPBM, B.KDFASPPH, B.FASPPH  
                FROM T_BC23DTL A LEFT JOIN T_BC23FAS B ON B.KODE_TRADER=A.KODE_TRADER AND B.CAR=A.CAR AND B.SERIAL=A.SERIAL 
                                 LEFT JOIN T_BC23TRF C ON C.KODE_TRADER=A.KODE_TRADER AND C.CAR=A.CAR AND C.SERIAL=A.SERIAL 
                WHERE A.CAR = '" . $car . "' AND A.KODE_TRADER ='" . $this->kd_trader . "' AND A.SERIAL = '" . $seri . "'";
        $aD = $this->actMain->get_result($sql);
        $i = 0;
        $hasil = 'Validasi error';
        $oke = 'ER';
        //print_r($aH);die();
       if (count($aarVal) > 0) {
            $hasil = '';
            $DATA['CAR'] = $car;
            $DATA['SERIAL'] = $seri;
            $DATA['KODE_TRADER'] = $this->kd_trader;
            foreach ($aarVal as $aV) {
                //print_r($aV);
                if (trim($aD[$aV['FLDNAME']]) == '') {
                    $hasil .= substr('   ' . ++$i, -3) . '. ' . $aV['MESSAGE'] . ' Harus diisi.<br>';
                }
                if (trim($aD[$aV['FLDNAME']]) == 0 && $aV['FLDNAME'] == 'NUMBER') {
                    $hasil .= substr('   ' . ++$i, -3) . '. ' . $aV['MESSAGE'] . ' Harus diisi.<br>';
                }
            }
       unset($aarVal);                  
        if ($aD['DNILINV'] < 0) {
                $hasil .= substr('   ' . ++$i, -3) . '. ' . $aV['MESSAGE'] . ' Harus diisi.<br>';
            }
            if ($aD['KDFASBM'] != '' && $aD['KDFASDTL'] == '') {
                $hasil .= substr('   ' . ++$i, -3) . '. Jenis fasilitas belum diisi.<br>';
            }
            
        //Jika nilai detil ada yang 0 (NVC) gak ngerti maksute
        //Cek Panjang HS
        if (strlen($aD['NOHS']) < 9) {
            $hasil .= substr('   ' . ++$i, -3) . '. Pengisian HS salah.<br>';
        }
        //Cek jika kode jenis Tarif kosong
        if ($aD['KDTRPBM'] == '') {
            $hasil .= substr('   ' . ++$i, -3) . '. Kode jenis tarif BM (Advalorum/Spesifik) harus diisi.<br>';
        }
        if ($aD['KDTRPBM'] == '2') {
            if ((int) $aD['TRPBM'] == 0 || $aD['KDSATBM'] == '' || (int) $aD['SATBMJM'] == 0) {
                $hasil .= substr('   ' . ++$i, -3) . '. Tarif Spesifik, Besar tarif dan kode satuan tarif harus diisi.<br>';
            }
        }
                    
        if ($i > 0) {
                $hasil.="<br>************  Data Detil ke-$seri BELUM lengkap **********<br>";
                $oke = 'ER';
                $DATA['DTLOK'] = '0';
            } else {
                $hasil.="<br>************  Data Detil ke-$seri SUDAH lengkap **********<br>";
                $oke = 'OK';
                $DATA['DTLOK'] = '1';
            } 
            $this->actMain->insertRefernce('t_bc23dtl', $DATA);            
                
        $view['data'] = $hasil;       
        $msg = $this->load->view('notifydetil', $view, true);
        unset($aH);
        return $msg;
    }
    }
    
    function genFlatfile($car,$srpAwal='') { 
        function fixLen($str, $len, $chr = ' ', $alg = STR_PAD_RIGHT) {
            $hasil = str_pad(substr($str, 0, $len), $len, $chr, $alg);
            return $hasil;
        }
        $this->load->model('actMain');
        $EDINUMBER = $this->newsession->userdata('EDINUMBER');
        $SQL = "SELECT C.*, A.*, "
                . "       DATE_FORMAT(A.PPJKTG,'%Y%m%d') as PPJKTG, "
                . "       DATE_FORMAT(A.BC23TG,'%Y%m%d') as BC23TG, "
                . "       DATE_FORMAT(A.DOKTUPTG,'%Y%m%d') as DOKTUPTG "
                . "FROM T_BC23HDR A LEFT JOIN M_TRADER C ON C.KODE_TRADER = A.KODE_TRADER "
                                . " LEFT JOIN M_TRADER_PARTNER D ON A.KODE_TRADER = D.KODE_TRADER AND A.KDKPBC=D.KDKPBC "
                . "WHERE A.CAR = '" . $car . "' AND A.KODE_TRADER = " . $this->kd_trader;
        $hdr = $this->actMain->get_result($SQL);
        $hdr = str_replace("\n", '', str_replace("\r", '', $hdr));
        if ($hdr['STATUS'] != '01') {
            if ($hdr['STATUS'] == '02') {
                $whHDR['KODE_TRADER'] = $this->kd_trader;
                $whHDR['CAR'] = $car;
                $this->db->where($whHDR);
                $upHDR['SNRF']   = '';
                $upHDR['DIRFLT'] = '';
                $upHDR['DIREDI'] = '';
                $upHDR['STATUS'] = '01';
                $this->db->update('t_bc23hdr', $upHDR);
                unset($upHDR);
                #delete dokoutbound
                $whHDR['KDKPBC'] = $hdr['KDKPBC'];
                $whHDR['JNSDOK'] = 'BC23';
                $this->db->delete('m_trader_dokoutbound', $whHDR);
                unset($whHDR);
                unlink($hdr['DIRFLT']);
                unlink($hdr['DIREDI']);
                $hsl = '1|';
            }
            else {
                $hsl = '0|';
            }
            unset($hdr);
            return $hsl;
        }
        $KDKPBC = $hdr['KDKPBC'];
        $SQL = "SELECT NOMOREDI FROM M_TRADER_PARTNER WHERE KDKPBC = '" . $hdr['KDKPBC'] . "' AND KODE_TRADER = " . $this->kd_trader;
        $partner = $this->actMain->get_uraian($SQL, 'NOMOREDI');
        if ($partner == '') {
            return 'Edinumber Kantor Pelayanan belum terekam.';
        }

        #SEPARATOR UNTUK HEADER
        $FF.= "ENV00101" . fixLen($EDINUMBER, 20) . fixLen($partner, 20) . "BC23      " . "2\n";
        
        #RECORD TYPE HDEC0101
        $FF.= "HDEC0101" . fixLen($hdr['CAR'], 26) . '1      9' . "\n";

        #RECORD TYPE HDEC0201
        $FF.= "HDEC0201" . fixLen($hdr['JNSBARANG'], 2) . '185     '. fixLen($hdr['TUJUANKIRIM'], 2) . '181     ' . fixLen($hdr['TUJUAN'], 2) . 'Z03' ."\n";
        
        #RECORD TYPE HDEC0301
        ($hdr['TMPTBN'] != '') ?        $FF.= 'HDEC030114 ' . fixLen($hdr['TMPTBN'], 8) ."\n" : '';
        ($hdr['PELMUAT'] != '') ?       $FF.= 'HDEC03019  ' . fixLen($hdr['PELMUAT'], 8) ."\n" : '';
        ($hdr['PELBKR'] != '') ?        $FF.= 'HDEC030111 ' . fixLen($hdr['PELBKR'], 8) ."\n" : ''; 
        ($hdr['PELTRANSIT'] != '') ?    $FF.= 'HDEC030113 ' . fixLen($hdr['PELTRANSIT'], 8) ."\n" : '';
        ($hdr['KDKPBCAWAS'] != '') ?    $FF.= 'HDEC0301Z01' . fixLen($hdr['KDKPBCAWAS'], 8) ."\n" : '';
        ($hdr['KDKPBCBONGKAR'] != '') ? $FF.= 'HDEC030141 ' . fixLen($hdr['KDKPBCBONGKAR'], 8) ."\n" : '';
        ($hdr['KDKPBC'] != '') ?        $FF.= 'HDEC030122 ' . fixLen($hdr['KDKPBC'], 8) ."\n" : '';
        
        #RECORD TYPE HDEC0401
        $FF.= 'HDEC0401182' . fixLen($hdr['TGLTTD'], 8) ."\n";       
        
        #RECORD TYPE HDEC0501
        $FF.= 'HDEC0501WT AADKGM' . fixLen(($hdr['BRUTO'] * 10000), 18, '0', STR_PAD_LEFT) ."\n";
        $FF.= 'HDEC0501WT AACKGM' . fixLen(($hdr['NETTO'] * 10000), 18, '0', STR_PAD_LEFT) ."\n";
        
        #RECORD TYPE HDEC0601  
        if ($hdr['JMCONT'] > 0) {
            $SQL = "SELECT * FROM T_BC23CON WHERE CAR = '" . $car . "' AND KODE_TRADER = " . $this->kd_trader;
            $con = $this->actMain->get_result($SQL, true);
            foreach ($con as $a) {
                $FF.= 'HDEC0601CN ' . fixLen($a['CONTNO'], 17) . fixLen($a['CONTUKUR'], 2) . fixLen($a['CONTTIPE'], 2) ."\n";
            }
            unset($con);
        }
        
         #KEMASAN HDEC0701
        $SQL = "SELECT * FROM T_BC23KMS WHERE CAR = '" . $car . "' AND KODE_TRADER = " . $this->kd_trader;
        $kms = $this->actMain->get_result($SQL, true);
        if (isset($kms)) {
            foreach ($kms as $a) {
                $FF.= 'HDEC0701ACH' . fixLen($a['JMKEMAS'], 8,'0', STR_PAD_LEFT) . fixLen($a['JNKEMAS'], 2) . fixLen($a['MERKKEMAS'], 50) . "\n";
            }
        }
        unset($kms);
        
        #Record Type HDEC0801
        $FF.= 'HDEC080120 ' . fixLen($hdr['ANGKUTNO'], 10) . fixLen($hdr['MODA'], 1) . fixLen($hdr['ANGKUTNAMA'], 35) . fixLen($hdr['ANGKUTFL'], 2) ."\n";

        #Record Type HDEC0901
        $FF.= 'HDEC090185 ' . fixLen($hdr['DOKTUPNO'], 6) . fixLen($hdr['POSNO'], 4) . '  ' . fixLen($hdr['DOKTUPKD'], 1) . fixLen($hdr['POSSUB'], 4, "0", STR_PAD_LEFT) . fixLen($hdr['POSSUBSUB'], 4, "0", STR_PAD_LEFT) . '      ' . '182' . fixLen($hdr['DOKTUPTG'], 4) ."\n";
        
        $FF.= 'HDEC0901Z02' . fixLen($hdr['PPJKNO'], 25) . '  182'. fixLen($hdr['PPJKTG'], 8) ."\n"; 
         
        $SQL = "SELECT *,DATE_FORMAT(DOKTG,'%Y%m%d') as DOKTG FROM T_BC23DOK WHERE CAR = '" . $car . "' AND KODE_TRADER = " . $this->kd_trader;
        $dok = $this->actMain->get_result($SQL, true);
        if (isset($dok)) {
            foreach ($dok as $a) {               
                $FF.= 'HDEC0901' . fixLen($a['DOKKD'], 3) . fixLen($a['DOKNO'], 25) . '  182' . fixLen($a['DOKTG'], 8) . "\n";
            }
        }
        
        #Record Type HDEC1001
        #supplaier
        $FF.= 'HDEC1001SU                 ' . fixLen($hdr['PASOKNAMA'], 50) . fixLen($hdr['PASOKALMT'], 100) . '                              ' . fixLen($hdr['KDTPB'], 5) . fixLen($hdr['PASOKNEG'], 2) ."\n";
        
        
        if($hdr['PPJKNPWP']!='' && $hdr['TIPE_TRADER']==='2'){ #PPJK
            $FF.= 'HDEC1001CB ' . fixLen($hdr['PPJKID'], 1) . fixLen($hdr['PPJKNPWP'], 15) . fixLen($hdr['PPJKNAMA'], 50) . fixLen($hdr['PPJKALMT'], 100) . '                              ' . fixLen($hdr['KDTPB'], 5) . '  ' ."\n";
        }
        $FF.= 'HDEC1001CN ' . fixLen($hdr['USAHAID'], 1) . fixLen($hdr['USAHANPWP'], 15) . fixLen($hdr['USAHANAMA'], 50) . fixLen($hdr['USAHAALMT'], 100) . fixLen($hdr['KOTATTD'], 30) . '       ' ."\n";
        $FF.= 'HDEC1201API' . fixLen($hdr['APIKD'] . $hdr['APIKD'] , 16) . "\n";
        
        
        #Penandatangan
        $FF.= 'HDEC1001Z01' . '                ' . fixLen($hdr['NAMATTD'], 150) . fixLen($hdr['KOTATTD'], 30) ."\n";
        
        //Record Type HDEC1201
        $FF.= 'HDEC1201STA' . fixLen($hdr['USAHASTATUS'], 2) ."\n";
        $FF.= 'HDEC1201REG' . fixLen($hdr['REGISTRASI'], 35) ."\n";
        $FF.= 'HDEC1201API' . fixLen($hdr['APIKD'], 1) . fixLen($hdr['APINO'], 15) ."\n";
        
         //Record Type HDEC1301
        ($hdr['CONTAKPERSON']!='')?$FF.= "HDEC1301" . 'Z02' . fixLen($hdr['CONTAKPERSON'], 30) ."\n":'';
        
        //Record Type HDEC1401
        ($hdr['NOPHONE']=='')?$FF.= 'HDEC1401' . fixLen($hdr['NOPHONE'], 50) . 'TE ' ."\n":'';
        ($hdr['NOFAX']=='')?$FF.= 'HDEC1401' . fixLen($hdr['NOFAX'], 50) . 'FX ' ."\n":'';
        ($hdr['EMAIL']=='')?$FF.= 'HDEC1401' . fixLen($hdr['EMAIL'], 50) . 'EM ' ."\n":'';

        //Record Type HDEC1101
        #NDPBM 
        $FF.= 'HDEC1101172' . fixLen($hdr['NDPBM'] * 10000, 18, "0", STR_PAD_LEFT) . fixLen($hdr['KDVAL'], 3) . "\n";
        #FOB
        ((float) $hdr['FOB'] > 0) ? $FF.= 'HDEC110163' . fixLen($hdr['FOB'] * 10000, 18, '0', STR_PAD_LEFT) . "\n" : '';
        #FREIGHT
        ((float) $hdr['FREIGHT'] > 0) ? $FF.= 'HDEC110164' . fixLen($hdr['FREIGHT'] * 10000, 18, '0', STR_PAD_LEFT) . "\n" : '';
        #ASURANSI
        ((float) $hdr['ASURANSI'] > 0) ? $FF.= 'HDEC110167' . fixLen($hdr['ASURANSI'] * 10000, 18, '0', STR_PAD_LEFT) . fixLen($hdr['KDASS'] , 1) . "\n" : '';
        #CIF
        ((float) $hdr['CIF'] > 0) ? $FF.= 'HDEC1101141' . fixLen($hdr['CIF'] * 10000, 18, '0', STR_PAD_LEFT) . "\n" : '';
        #INVOICE
        ((float) $hdr['NILINV'] > 0) ? $FF.= 'HDEC110177' . fixLen($hdr['NILINV'] * 10000, 18, '0', STR_PAD_LEFT) . "\n" : '';

        #DATA DETIL
        $SQL = "SELECT A.SERIAL,A.NOHS,A.KDFASDTL,A.BRGURAI,A.MERK,A.TIPE,A.SPFLAIN,A.BRGASAL,A.KDSAT,A.JMLSAT,A.SATBMJM,A.SATCUKJM,A.NETTODTL,A.KEMASJM,A.KEMASJN,A.DCIF, "
                . "       B.KDSATBM,B.KDSATCUK,B.KDCUK,B.KDTRPBM,B.KDTRPCUK,B.TRPCUK,B.TRPPPN,B.TRPPBM,B.TRPBM,B.TRPPPH, "
                . "       C.KDFASBM,C.KDFASCUK,C.KDFASPPN,C.KDFASPPH,C.KDFASPBM,C.FASBM,C.FASCUK,C.FASPPN,C.FASPPH,C.FASPBM "
                . "FROM T_BC23DTL A LEFT JOIN T_BC23TRF B ON B.KODE_TRADER = A.KODE_TRADER AND B.CAR = A.CAR AND B.SERIAL = A.SERIAL "
                . "                 LEFT JOIN T_BC23FAS C ON C.KODE_TRADER = A.KODE_TRADER AND C.CAR = A.CAR AND C.SERIAL = A.SERIAL "
                . "WHERE A.CAR = '" . $car . "' AND A.KODE_TRADER = " . $this->kd_trader
                . " ORDER BY A.SERIAL ASC";
        $dtl = $this->actMain->get_result($SQL, true);
        $dtl = str_replace("\n", '', str_replace("\r", '', $dtl));
        if (count($dtl) == 0) {
            return 'Detil Barang PIB Nomor : ' . $car . ' Kosong.';
        }
        $FF.= 'UNS00101D' . "\n";
        unset($a);
        foreach ($dtl as $a) {
            # RECORD TYPE DDEC0101
            $FF.= 'DDEC0101' . fixLen($a['SERIAL'], 5, '0', STR_PAD_LEFT) . fixLen($a['NOHS'], 12) . fixLen($a['PENGGUNAAN'], 1) . fixLen($a['KDFASDTL'], 2) . fixLen($a['JNSBARANGDTL'], 5) . fixLen($a['TUJUANKIRIMDTL'], 5) . "\n";            
            
            # RECORD TYPE DDEC0201
            $FF.= 'DDEC0201AAA' . fixLen($a['BRGURAI'], 95) . fixLen($a['MERK'], 15) . fixLen($a['TIPE'], 15) . fixLen($a['SPFLAIN'], 15) . fixLen($a['KDBRG'], 15) . '27 ' . fixLen($a['BRGASAL'], 2) . "\n";
            
            # RECORD TYPE DDEC0301
            ($a['NETTODTL'] > 0) ? $FF.= 'DDEC0301AAIKGM' . fixLen($a['NETTODTL'] * 10000, 18, '0', STR_PAD_LEFT) . "\n" : '';
            ($a['NETTODTL'] > 0) ? $FF.= 'DDEC0301DX ' . fixLen($a['KDSAT'] ,3) . fixLen($a['JMLSAT'] * 10000 ,18 , '0', STR_PAD_LEFT) . "\n" : '';
            
            # RECORD TYPE DDEC0401
            $FF.= 'DDEC0401' . fixLen($a['KEMASJM'], 8, '0', STR_PAD_LEFT) . fixLen($a['KEMASJN'], 2) . "\n";
             
            # RECORD TYPE DDEC0501
            #DCIF
            $FF.= 'DDEC0501141' . fixLen($a['DCIF'] * 100, 18, '0', STR_PAD_LEFT) . '   ' . "\n";
            #DINVOICE
            $FF.= 'DDEC050138 ' . fixLen($a['DNILINV'] * 100, 18, '0', STR_PAD_LEFT) . '   ' . "\n";
            
            # RECORD TYPE DDEC0701
            ($a['TRPBM'] > 0)  ? $FF.= 'DDEC07011  CUD  ' . fixLen($a['KDTRPBM'], 1) . fixLen($a['TRPBM'] * 100, 10) . '161' . fixLen($a['SATBMJM'] * 10000, 18, '0', STR_PAD_LEFT) . fixLen($a['KDSATBM'] , 3) . "\n" : '';
            
            ($a['FASBM'] > 0)  ? $FF.= 'DDEC07011  CUF  ' . fixLen($a['KDFASBM'], 1) . fixLen($a['SATBMJM'] * 100, 10) . "\n" : '';
            
            ($a['TRPCUK'] > 0) ? $FF.= 'DDEC07011  EXC  ' . fixLen($a['KDCUK'], 2) . fixLen($a['KDTRPCUK'] , 1) . fixLen($a['TRPCUK'] * 100, 10) . '161' . fixLen($a['SATCUKJM'] * 10000, 18, '0', STR_PAD_LEFT)   . fixLen($a['KDSATCUK'] , 3) . "\n" : '';
            
            ($a['FASCUK'] > 0) ? $FF.= 'DDEC07011  EXF  ' . fixLen($a['KDFASCUK'], 1) . fixLen($a['FASCUK'] * 100, 10) . "\n" : '';
            
            ($a['TRPPPN'] > 0) ? $FF.= 'DDEC07011  VAT   '. fixLen($a['TRPPPN'] * 100, 10) . "\n" : '';
            
            ($a['FASPPN'] > 0) ? $FF.= 'DDEC07011  VAF  ' . fixLen($a['KDFASPPN'], 1) . fixLen($a['FASPPN'] * 100, 10) . "\n" : '';
            
            ($a['TRPPBM'] > 0) ? $FF.= 'DDEC07011  LUX   '. fixLen($a['TRPPBM'] * 100, 10) . "\n" : '';
            
            ($a['FASPBM'] > 0) ? $FF.= 'DDEC07011  LUF  ' . fixLen($a['KDFASPBM'], 1) . fixLen($a['FASPBM'] * 100, 10) . "\n" : '';
            
            ($a['TRPPPH'] > 0) ? $FF.= 'DDEC07011  WAG   '. fixLen($a['TRPPPH'] * 100, 1) . "\n" : '';
            
            ($a['FASPPH'] > 0) ? $FF.= 'DDEC07011  WAF  ' . fixLen($a['KDFASPPH'], 1) . fixLen($a['FASPPH'] * 100, 10) . "\n" : '';
        }
          
        #Record Type UNS00201
        $FF.= 'UNS00201S' . "\n";

        #Record Type SDEC0101
        ($hdr['JMCONT'] > 0)?   $FF.= 'SDEC010116 ' . fixLen($hdr['JMCONT'], 8, '0', STR_PAD_LEFT) . "\n":'';        
        ($hdr['JMBRG'] > 0)?    $FF.= 'SDEC01016  ' . fixLen($hdr['JMBRG'], 8, '0', STR_PAD_LEFT) . "\n":'';

        #Record Type SDEC0201
        $SQL = "SELECT KDBEBAN, KDFASIL, NILBEBAN FROM T_BC23PGT WHERE CAR = '" . $car . "' AND KODE_TRADER = " . $this->kd_trader;
        $pgt = $this->actMain->get_result($SQL, true);
        foreach ($pgt as $row) {
            if ($row['KDBEBAN'] == '5' or $row['KDBEBAN'] == '6' or $row['KDBEBAN'] == '7') { //cukai dijadikan 1 kodebeban
                $row['KDBEBAN'] = '5';
            }
            $nilai_pgt[$row['KDBEBAN']][$row['KDFASIL']] += $row['NILBEBAN'];
        }
        $bbn[1] = 'BM';
        $bbn[2] = 'PPN';
        $bbn[3] = 'PPNBM';
        $bbn[4] = 'PPH';
        $bbn[5] = 'CUKAI';
        $bbn[6] = 'PNBP';

        $fas[0] = 'BY';
        $fas[2] = 'TG';
        foreach ($nilai_pgt as $k=>$a){
            foreach ($a as $kk=>$aa){
                $FF.= 'SDEC02014  ' . fixLen($bbn[$k].$fas[$kk] , 10) . '24 ' . fixLen($aa, 15, '0', STR_PAD_LEFT) . "\n";
            }
        }
        unset($a);
        unset($bbn);
        unset($fas);
        
        #Record Type UNZ00101
        $FF.= 'UNZ00101' . "\n";
        
        $fullPaht = 'FLAT/' . $EDINUMBER;
        if (!is_dir($fullPaht)){mkdir($fullPaht,0777,true);}
        $fullPaht = $fullPaht . '/' . $car . '.FLT';
        if (file_exists($fullPaht)){unlink($fullPaht);}//print_r($fullPaht);die();
        $handle = fopen($fullPaht, 'w');
        fwrite($handle, $FF);
        fclose($handle);
        if (file_exists($fullPaht)){
            $hsl = '2|' . $hdr['KDKPBC'];
        }else{
            $hsl = '0|';
        }
        return $hsl; 
    }
    
    function crtQueue($car){
        $this->load->model('actTranslator');
        $SNRF       = date('ymdHis') . rand(10, 99);
        $EDINUMBER  = $this->newsession->userdata('EDINUMBER');
        $EDIPaht    = 'EDI/' . $EDINUMBER;
        if (!is_dir($EDIPaht)){
            mkdir($EDIPaht,0777,true);
        }else{
            chmod($EDIPaht,0777);
        }
        $FLTPaht    = 'FLAT/' . $EDINUMBER; 
        if (!is_dir($FLTPaht)){
            mkdir($FLTPaht,0777,true);
        }else{
            chmod($FLTPaht,0777);
        }
        $this->actTranslator->getEDIFILE($EDINUMBER, 'BC23', $SNRF, $car . '.FLT');
        if (file_exists($EDIPaht.'/'. $car .'.EDI') && file_exists($FLTPaht.'/'. $car .'.FLT')){
            $dirEdiNumber = 'TRANSACTION/' . $EDINUMBER . '/' . date('Ymd');
            if (!is_dir($dirEdiNumber)){mkdir($dirEdiNumber,0777,true);}
            rename($FLTPaht.'/'. $car .'.FLT', $dirEdiNumber . '/' . $car . '.FLT');
            rename($EDIPaht.'/'. $car .'.EDI', $dirEdiNumber . '/' . $car . '.EDI');
            $rtn = $SNRF . '|' . $dirEdiNumber . '/' . $car . '.EDI' . '|' . $dirEdiNumber . '/' . $car . '.FLT';
        }
        else {
            $rtn = 'ERROR';
        }
        return $rtn;
    }
    
    function updateSNRF($car, $data) {
        #update t_bc23hdr
        $this->load->model('actMain');
        $arrData = explode('|', $data);
        $dataHdr['SNRF'] = $arrData[0];
        $dataHdr['DIRFLT'] = $arrData[2];
        $dataHdr['DIREDI'] = $arrData[1];
        $dataHdr['STATUS'] = '02';
        $dataHdr['CAR'] = $car;
        $dataHdr['KODE_TRADER'] = $this->kd_trader;
        $this->actMain->insertRefernce('t_bc23hdr', $dataHdr);

        #insert m_trader_dokoutbound
        $dataDOK['KODE_TRADER'] = $this->kd_trader;
        $dataDOK['KDKPBC'] = $arrData[3];
        $dataDOK['JNSDOK'] = 'BC23';
        $dataDOK['APRF'] = 'BC23';
        $dataDOK['CAR'] = $car;
        $dataDOK['STATUS'] = '00';
        $this->actMain->insertRefernce('m_trader_dokoutbound', $dataDOK);
        return '0';
    }
    
    function cetakDokumen($car, $jnPage, $arr ) {
        //die($car.'|snv'.$jnPage);
        $this->load->library('fpdf');
        $this->load->model('actMain');
        $this->load->model('dokBC23');
        $this->dokBC23->ciMain($this->actMain);
        $this->dokBC23->fpdf($this->fpdf);
	$this->dokBC23->segmentUri($arr);				
        $this->dokBC23->showPage($car, $jnPage);
    }
    
    function uploadB23(){
        $element = 'fileB23';
        $EDINUMBER = $this->newsession->userdata('EDINUMBER');
        if ($element != "") {
            $path = $_FILES[$element]['name']; 
            $ftype = pathinfo($path, PATHINFO_EXTENSION);
            //die($ftype.' = '.$arrtype);
            if (!empty($_FILES[$element]['error'])) {
                switch ($_FILES[$element]['error']){
                    case'1':$error = "Ukuran File Terlalu Besar.";break;
                    case'3':$error = "File Yang Ter-Upload Tidak Sempurna.";break;
                    case'4':$error = "File Kosong Atau Belum Dipilih.";break;
                    case'6':$error = "Direktori Penyimpanan Sementara Tidak Ditemukan.";break;
                    case'7':$error = "File Gagal Ter-Upload.";break;
                    case'8':$error = "Proses Upload File Dibatalkan.";break;
                    default :$error = "Pesan Error Tidak Ditemukan.";break;
                }
            }
            else if (empty($_FILES[$element]['tmp_name']) || ($_FILES[$element]['tmp_name'] == 'none')) {
                $error = "File Gagal Ter-Upload.";
            } 
            else if($ftype != 'B23') {
                $error = "Tipe File Salah.<br>Tipe File Yang Diterima : *.B23";
            }
            else {
                $filename = 'TRANSACTION/' . $EDINUMBER . '/' . $this->kd_trader . '.' .$ftype;
                mkdir('TRANSACTION/' . $EDINUMBER . '/',0777,true);
                chmod('TRANSACTION/' . $EDINUMBER . '/',0777);
                @unlink($filename);
                if(move_uploaded_file($_FILES[$element]['tmp_name'], $filename)){
                    $ftp_conn = ftp_connect('10.1.5.78') or die('Could not connect to 10.1.5.78');
                    if(ftp_login($ftp_conn, 'Administrator', 'Bismillah')){
                        //move to ftp
                        ftp_pasv($ftp_conn, true);
                        $file = $this->kd_trader . '.' .$ftype;
                        $paht_local = getcwd() . '/TRANSACTION/' . $EDINUMBER . '/'.$file;
                        $paht_remot = '/wsT2G/files/' . $file ;
                        if (ftp_put($ftp_conn, $paht_remot , $paht_local, FTP_BINARY)){
                            $this->load->library("nuSoap_lib");
                            $nuSoap_lib = new nusoap_client('http://10.1.5.78/wsT2g/index.php?wsdl');
                            if ($nuSoap_lib->fault) {
                                $error = 'Error: ' . $nuSoap_lib->fault;
                            }
                            else {
                                if ($nuSoap_lib->getError()) {
                                    $error = 'error ws : ' . $nuSoap_lib->getError();
                                }
                                else {
                                    $error = 'return ws : '.$nuSoap_lib->call('restoreB23', array( 'namaFile'=> $file ,
                                                                              'kode_trader'  => $this->kd_trader));
                                }
                            }
                            //$error = "Put file from ftp berhasil.";
                        }
                        else{ 
                            $error = "Put file from ftp gagal.";
                        }
                    }else{
                        $error = "Login ftp gagal.";
                    }
                    ftp_close($ftp_conn);
                }
                else {
                    $error = "File Gagal Ter-Upload.";
                }
            }
        }
        else {
            $error = "Parameter Tidak Ditemukan.";
        }
        @unlink($_FILES[$element]);
        return $error;
    }
    
    function downloadB23(){
        $arrCar = $this->input->post('tb_chkf');
        $car    = implode(',', $arrCar);
        die($car);
        $EDINUMBER = $this->newsession->userdata('EDINUMBER');
        $this->load->library("nuSoap_lib");
        $nuSoap_lib = new nusoap_client('http://10.1.5.78/wsT2g/index.php?wsdl');
        if ($nuSoap_lib->fault) {
            $error = 'Error: ' . $nuSoap_lib->fault;
        }
        else {
            if ($nuSoap_lib->getError()) {
                $error = 'error ws : ' . $nuSoap_lib->getError();
            }
            else {
                $genB23 = $nuSoap_lib->call('backupB23', array( 'car'=> $car ,'kode_trader'  => $this->kd_trader));
                //return print_r($genB23,true);   
                if($genB23){
                    $ftp_conn = ftp_connect('10.1.5.78') or die('Could not connect to 10.1.5.78');
                    if(ftp_login($ftp_conn, 'Administrator', 'Bismillah')){
                        //move to ftp
                        ftp_pasv($ftp_conn, true);
                        $file = $this->kd_trader . '.B23';
                        $paht_local = getcwd() . '/TRANSACTION/' . $EDINUMBER . '/'.$file;
                        $paht_remot = '/wsT2G/files/' . $file ;
                        chmod($paht_local,0777);
                        if (ftp_get($ftp_conn, $paht_local, $paht_remot, FTP_BINARY)){
                            $error = 'MSG|OK|'. base_url('TRANSACTION/'.$EDINUMBER.'/'.$this->kd_trader.'.B23').'|Generate File Berhasil.<br>Silahkan didownload.';
                        }
                        else{
                            $error = 'MSG|ERR||Generate File Gagal';
                        }
                    }else{
                        $error = "Login ftp gagal.";
                    }
                    ftp_close($ftp_conn);
                }
            }
        }
        return $error;
        
    }
    
    function uploadexcel ($car){        
        $error = "";
        $msg = "";
        $element = 'fileexcel' ;
        $EDINUMBER = $this->newsession->userdata('EDINUMBER');  
        $dir = './TRANSACTION/' . $EDINUMBER;
        if(!is_dir($dir)) mkdir($dir);
        if(!empty($_FILES[$element]['error'])){ 
                switch($_FILES[$element]['error']){ 
                        case '1': 
                                $error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini'; 
                                break;
                        case '2':
                                $error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
                                break;
                        case '3':
                                $error = 'The uploaded file was only partially uploaded';
                                break;
                        case '4':
                                $error = 'The uploded file was not found';
                                break;
                        case '6':
                                $error = 'Missing a temporary folder';
                                break;
                        case '7':
                                $error = 'Failed to write file to disk';
                                break;
                        case '8':
                                $error = 'File upload stopped by extension';
                                break;
                        case '999':
                        default:
                                $error = 'No error code avaiable';
                }    
        }elseif(empty($_FILES[$element]['tmp_name']) || $_FILES[$element]['tmp_name'] == 'none'){
                $error = 'File Upload tidak ditemukan';
        }else{
            if(file_exists($dir) && is_dir($dir)){
                if(chmod($dir, 0777)){
                                $config['upload_path'] = $dir;
                }
            }else{
                if(mkdir($dir, 0777, true)){
                    if(chmod($dir, 0777)){
                        $config['upload_path'] = $dir;
                    }
                }
            }
            $config['remove_spaces'] = TRUE;
            $config['overwrite'] = TRUE;
            $config['allowed_types'] = 'xls|xlsx';
            $config['max_size'] = '5000'; 
            $config['file_name'] = $this->kd_trader;
            $this->load->library('upload' , $config);
            $this->upload->display_errors('' ,'' );
            if(!$this->upload->do_upload($element)){
                $error = $this->upload->display_errors();
            }else{
                $data = $this->upload->data();
                $filename = $data['file_name'];
                $filesize = $data['file_size'];
                $msg = $filename."#".$filesize;
                $this->bacaexcel('./TRANSACTION/' . $EDINUMBER . '/' . $filename,$car);
            }
        }        
        
    }
            
    function bacaexcel($dirExcel,$car){        
        $this->load->model("actMain");
        $this->load->library('Excel');
        $read = PHPExcel_IOFactory::createReaderForFile($dirExcel);        
        $read->setReadDataOnly(true);        
        $excel = $read->load($dirExcel);    
        $secval = $excel->getproperties()->gettitle();       
        $sheets = $read->listWorksheetNames($dirExcel);
        foreach ($sheets as $sheet) {
            $_sheet = $excel->setActiveSheetIndexByName($sheet);
            $maxRow = $_sheet->getHighestRow();
            $maxCol = $_sheet->getHighestColumn();
            $realmaxRow = $maxRow - 13;
            $maxCol = range('B', $maxCol);
            $seri = (int) $this->actMain->get_uraian("SELECT MAX(SERIAL) AS MAXSERI FROM t_bc23dtl WHERE CAR='" . $car . "'", "MAXSERI");
            $ii     = 0;
            for ($i = 3; $i <= $maxRow; $i++) {  
                $hs = trim($_sheet->getCell("F$i")->getCalculatedValue());
                if($hs == ''){break;}
                $data[$ii]['KODE_TRADER']       = $this->kd_trader;
                $data[$ii]['CAR']               = $car;                   
                $data[$ii]['SERIAL']            = ++$seri; 
                $data[$ii]['JNSBARANGDTL']      = $_sheet->getCell("D$i")->getCalculatedValue();
                $data[$ii]['TUJUANKIRIMDTL']    = $_sheet->getCell("E$i")->getCalculatedValue();
                $data[$ii]['NOHS']              = $hs;
                $data[$ii]['SERITRP']           = $_sheet->getCell("G$i")->getCalculatedValue();
                $data[$ii]['BRGURAI']           = $_sheet->getCell("H$i")->getCalculatedValue();
                $data[$ii]['MERK']              = $_sheet->getCell("I$i")->getCalculatedValue();
                $data[$ii]['TIPE']              = $_sheet->getCell("J$i")->getCalculatedValue();
                $data[$ii]['SPFLAIN']           = $_sheet->getCell("K$i")->getCalculatedValue();
                $data[$ii]['KDBRG']             = $_sheet->getCell("L$i")->getCalculatedValue();
                $data[$ii]['PENGGUNAAN']        = $_sheet->getCell("M$i")->getCalculatedValue();
                $data[$ii]['KEMASJN']           = $_sheet->getCell("N$i")->getCalculatedValue();
                $data[$ii]['KEMASJM']           = $_sheet->getCell("O$i")->getCalculatedValue();
                $data[$ii]['KDFASDTL']          = $_sheet->getCell("P$i")->getCalculatedValue();
                $data[$ii]['BRGASAL']           = $_sheet->getCell("Q$i")->getCalculatedValue();
                $data[$ii]['DNILINV']           = $_sheet->getCell("R$i")->getCalculatedValue();
                $data[$ii]['DCIF']              = $_sheet->getCell("S$i")->getCalculatedValue();
                $data[$ii]['KDSAT']             = $_sheet->getCell("T$i")->getCalculatedValue();
                $data[$ii]['JMLSAT']            = $_sheet->getCell("U$i")->getCalculatedValue();
                $data[$ii]['NETTODTL']          = $_sheet->getCell("V$i")->getCalculatedValue();
                $data[$ii]['SATBMJM']           = $_sheet->getCell("W$i")->getCalculatedValue();
                $data[$ii]['SATCUKJM']          = $_sheet->getCell("X$i")->getCalculatedValue();
                $data[$ii]['DTLOK']             = $_sheet->getCell("Y$i")->getCalculatedValue();                   
                $ii++;
            }
            $sql = $this->db->insert_batch('t_bc23dtl', $data);
            if($sql){
                $this->db->simple_query("UPDATE t_bc23hdr set STATUS='00' WHERE CAR= '".$car."' AND KODE_TRADER='".$this->kd_trader."'" );
                $rtn = "Data behasil diproses.";
            }else{
                $rtn = "Data gagal diproses.";
            }   
        }
         return $rtn;
    }   
    
    function getSSPCP($car) {
        $this->load->model('actMain');
        $query = "SELECT a.*,URAIAN_KPBC as 'URKPBC'
                  FROM   t_bc23ntb a Left Join m_kpbc b on a.KDKPBC = b.KDKPBC
                  WHERE  CAR = '" . $car . "' AND KODE_TRADER = " . $this->kd_trader;
        $aNtb = $this->actMain->get_result($query);
        if (count($aNtb) > 0) {
            $query = "SELECT AKUN_SSPCP,NILAI
                      FROM   t_bc23ntbakun
                      WHERE CAR = '" . $car . "' AND 
                            KODE_TRADER = " . $this->kd_trader;
            $arrakun = $this->actMain->get_result($query, true);
            foreach ($arrakun as $a) {
                $hasil[$a['AKUN_SSPCP']] = $a['NILAI'];
            }
            $hasil = $hasil + $aNtb;
            return $hasil;
        } else {           
            $query = "SELECT a.KDKPBC ,URAIAN_KPBC as 'URKPBC', NPWP, USAHANAMA as 'NAMA',USAHANPWP as 'USAHANPWP'
                      FROM   t_bc23hdr a Left Join m_kpbc b on a.KDKPBC = b.KDKPBC
                      WHERE CAR = '" . $car . "' AND 
                            KODE_TRADER = " . $this->kd_trader;
            $arrHdr = $this->actMain->get_result($query);
            $query = "SELECT KDBEBAN,KDFASIL,NILBEBAN
                      FROM   t_bc23pgt
                      WHERE CAR = '" . $car . "' AND 
                            KDFASIL = '0' AND 
                            KODE_TRADER = " . $this->kd_trader;
            $arrPgt = $this->actMain->get_result($query, true);
            $pgt = array();
            foreach ($arrPgt as $arr) {
                $pgt[$arr['KDBEBAN']][$arr['KDFASIL']] = $arr['NILBEBAN'];
            }
            $hasil['412111'] = $pgt['1']['0']; //bm
            $hasil['411212'] = $pgt['2']['0']; //ppn
            $hasil['411222'] = $pgt['3']['0']; //pbm
            $hasil['411123'] = $pgt['4']['0']; //pph
            $hasil['411511'] = $pgt['5']['0']; //cukai Tembakau
            $hasil['411513'] = $pgt['6']['0']; //cukai MMEA
            $hasil['411512'] = $pgt['7']['0']; //cukai EA
            $hasil['423216'] = $pgt['8']['0']; //pnbp           
            $hasil = $hasil + $arrHdr; 
            return $hasil;
        }
    }
    
    function setSSPCP() {
        $this->load->model('actMain');
        $SSPCP = $this->input->post('SSPCP');
        $SSPCP['KODE_TRADER'] = $this->kd_trader;
        $this->actMain->insertRefernce('t_bc23ntb', $SSPCP);
        //insert batch
        //detelete
        $this->db->delete('t_bc23ntbakun', array('KODE_TRADER' => $SSPCP['KODE_TRADER'], 'CAR' => $SSPCP['CAR']));
        $SSPCPAKUN = $this->input->post('SSPCPAKUN');
        foreach ($SSPCPAKUN as $key => $val) {
            if ((int) $val > 0) {
                $dataAkun[] = array('KODE_TRADER' => $SSPCP['KODE_TRADER'], 'CAR' => $SSPCP['CAR'], 'AKUN_SSPCP' => $key, 'NILAI' => $val);
            }
        }
        $this->db->insert_batch('t_bc23ntbakun', $dataAkun);
        return true;
    }
    
    function daftarRespon($car,$tipe) {
        $this->load->library('newtable');
        $this->load->model("actMain");
        $SQL = " SELECT CAR, a.KDKTR as 'Kpbc', concat(RESKD,' - ',b.URAIAN) as 'Respon', c.URAIAN as 'Dibaca', a.RESTG as 'Tgl Respon', a.RESWK as 'Wkt Respon',
                a.RESKD, a.RESTG, a.RESWK
                FROM T_BC23RESTPB a LEFT JOIN M_TABEL b ON a.RESKD = b.KDREC and b.MODUL = 'BC23' AND b.KDTAB = 'JENIS_RESPON' 
                                    LEFT JOIN M_TABEL c ON a.DIBACA = c.KDREC and c.MODUL = 'BC23' AND c.KDTAB = 'DIBACA'
                WHERE a.CAR='" . $car . "' AND a.KODE_TRADER = '" . $this->kd_trader . "'";      
        $this->newtable->keys(array('CAR','RESKD','RESTG','RESWK'));
        $this->newtable->hiddens(array('CAR','RESKD','RESTG','RESWK'));
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->action(site_url('bc23/beforecetakrespon/'.$car));
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $prosesnya = array(
            'Print Respon' => array('NEWPOPCETAK', site_url('bc23/cetak'), '1', ' CETAK RESPON |900|550'));
        
        $this->newtable->orderby(RESTG, RESWK, RESKD);
        $this->newtable->sortby("DESC");
        $this->newtable->tipe_check('radio');
        $this->newtable->set_formid("f" . $tipe);
        $this->newtable->set_divid("div" . $tipe);
        $this->newtable->rowcount(10);
        $this->newtable->clear();
        $this->newtable->menu($prosesnya);
        $this->newtable->show_search(false);
        $this->newtable->tipe_proses('button');
        $tabel .= $this->newtable->generate($SQL);
        $arrdata['CAR'] = $car;
        $arrdata['tabel'] = $tabel;
        $arrdata['tipe'] = $tipe;
        if ($this->input->post("ajax")){return $tabel;}
        else{return $arrdata;}
    }
}
?>