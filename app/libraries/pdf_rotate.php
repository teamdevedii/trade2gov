<?php
require('rotation.php');

class pdf_rotate extends rotation{
    function RotatedText($x,$y,$txt,$angle){
        $this->Rotate($angle,$x,$y);
        $this->Text($x,$y,$txt);
        $this->Rotate(0);
    }
}
?>