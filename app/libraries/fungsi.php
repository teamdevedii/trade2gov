<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class Fungsi
{
	function FormatHS($varnohs){
		if (!is_null($varnohs)){
			$varresult = '';
			$varresult = substr($varnohs,0,4).".".substr($varnohs,4,2).".".substr($varnohs,6,2).".".substr($varnohs,8,2);
			return $varresult;
		}
	}	
	function FormatNPWP($varnpwp){
		$varresult = '';
		$varresult = substr($varnpwp,0,2).".".substr($varnpwp,2,3).".".substr($varnpwp,5,3).".".substr($varnpwp,8,1)."-".substr($varnpwp,9,3).".".substr($varnpwp,12,3);
		return $varresult;
	}	
	function FormatDate($vardate)
	{
		$pecah1 = explode("-", $vardate);
		$tanggal = intval($pecah1[2]);
		$arrayBulan = array("", "January", "February", "March", "April", "May", "June", "July",
							"August", "September", "October", "November", "December");
		$bulan = $arrayBulan[intval($pecah1[1])];
		//$bulan = intval($pecah[1]);
		$tahun = intval($pecah1[0]);
		$balik = $tanggal." ".$bulan." ".$tahun;
		return $balik;
	}
	function FormatDateIndo($vardate)
	{
		$pecah1 = explode("-", $vardate);
		$tanggal = intval($pecah1[2]);
		$arrayBulan = array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli",
							"Agustus", "September", "Oktober", "November", "Desember");
		$bulan = $arrayBulan[intval($pecah1[1])];
		//$bulan = intval($pecah[1]);
		$tahun = intval($pecah1[0]);
		$balik = $tanggal." ".$bulan." ".$tahun;
		return $balik;
	}
	function FormatRupiah($angka,$decimal){
		$rupiah=number_format($angka,$decimal,'.',',');		
		return $rupiah;
	}	
	function dateformat($date){
	   if (strstr($date, "-"))   {
			   $date = preg_split("/[\/]|[-]+/", $date);
			   $date = $date[2]."-".$date[1]."-".$date[0];
			   return $date;
	   }
	   else if (strstr($date, "/"))   {
			   $date = preg_split("/[\/]|[-]+/", $date);
			   $date = $date[2]."-".$date[1]."-".$date[0];
			   return $date;
	   }
	   else if (strstr($date, ".")) {
			   $date = preg_split("[.]", $date);
			   $date = $date[2]."-".$date[1]."-".$date[0];
			   return $date;
	   }
	   return false;
	}
	function replace($input,$var){
		if (!is_null($input)){
			$varresult = '';
			$varresult = str_replace($var,'',$input);
			return $varresult;
		}
	}	
	function FormatAju($var){
		if (!is_null($var)){
			$varresult = '';
			$varresult = substr($var,0,6)."-".substr($var,6,6)."-".substr($var,12,8)."-".substr($var,20,6);
			return $varresult;
		}
	}
	function getkodefas($kode){
		switch($kode){
			case '0': $hasil = "DBY";break;
			case '1': $hasil = "DTP";break;
			case '2': $hasil = "DTG";break;
			case '3': $hasil = "BKL";break;
			case '4': $hasil = "BBS";break;
			default: $hasil = "";break;
		}
		return $hasil;
	}
}
?>