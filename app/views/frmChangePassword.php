<div id="myform" title="Ubah Password">
    <form id="frmUbahPassword" name="frmUbahPassword" method="POST" action="<?= site_url('usermanage/ubahpassword') ?>" onsubmit="return false;" class="form uniformForm" autocomplete=off >
        <table width="100%">
            <tr>
                <td>Username</td>
                <td>: <input type="text" name="DATA[USERNAME]" id="USERNAME" class="ltext" value="<?= $this->newsession->userdata('USER_NAME') ?>"/></td>
            </tr>
            <tr>
                <td>Password lama</td>
                <td>: <input type="password" name="DATA[PASSWORD]" id="PASSWORD" class="ltext" value="" /></td>
            </tr>
            <tr>
                <td>Password baru</td>
                <td>: <input type="password" name="DATA[PASSWORDBARU]" id="PASSWORDBARU" class="ltext" value="" wajib="yes"/></td>
            </tr>
            <tr>
                <td>Konfirmasi Password baru</td>
                <td>: <input type="password" name="DATA[PASSWORDBARUKONFIRMASI]" id="PASSWORDBARUKONFIRMASI" class="ltext" value="" wajib="yes"/></td>
            </tr>
            <tr>
                <td colspan="2">
                    <br>
                    <button class="btn btn-small" onclick="save_post_msg('frmUbahPassword');closedialog('msgbox');"> &nbsp; Simpan &nbsp; </button>
                    <button class="btn btn-small" onclick="document.getElementById('frmRegistrasi').reset();"> &nbsp; Reset  &nbsp; </button>
                </td>
            </tr>
        </table>
    </form>
</div>