<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);
?>
<html>
    <!DOCTYPE html>
    <head>
        <script type="text/javascript" src="<?= base_url(); ?>js/all.js"></script>
        <script>
            var base_url = '<?= base_url(); ?>';
            var site_url = '<?= site_url(); ?>';
            var site_name = '.: Trade to Goverment :.';
        </script>
        <title>Trade to Goverment</title>
        <meta charset="UTF-8" />
        <meta name="description" content="trade 2 goverment"/>
        <meta name="author" content="muklis"/>
        <meta name="viewport" content="width=device-width,initial-scale=1" />
        <link rel="shortcut icon" href="<?= base_url(); ?>img/logo.ico" type="image/x-icon" >
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/all.css" />
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/main.css" />
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/login.css" />
        <script type="text/javascript" src="<?= base_url(); ?>js/main.js"></script>
    </head>
    <body>
        <div class="container" >
            <table style=" width: 100%;height: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="borderLeftTop">&nbsp;
                        <img src="<?= base_url() ?>/img/logo.png" class="img-login" style="height: 65px;padding: 15px 20px;">
                    </td>
                    <td class="borderMiddleTop">&nbsp;</td>
                    <td class="borderRightTop" style="height: 1px">
                        <div style=" position: absolute; right:10px; top: 71px; ">
                            <nav id="navigation">
                                <ul>
                                    <li><a href="#" onclick="callnoAnim('<?= site_url('outLogin/callOut/home'); ?>','_content_')">Home</a></li>
                                    <li><a href="#" onclick="callnoAnim('<?= site_url('outLogin/callOut/request'); ?>','_content_')">Request</a></li>
                                    <li><a href="#" onclick="callnoAnim('<?= site_url('outLogin/callOut/login'); ?>','_content_')">Log In</a></li>
                                </ul>
                            </nav>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="vertical-align: middle; float: inside;">
                    <center>
                        <div id="_content_" style="height: 100%;font-family:Arial, 'Helvetica', Helvetica, sans-serif;" >{_content_}</div>
                    </center>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="borderLeftBotton">&nbsp;</td>
                </tr>
            </table>
        </div>
    </body>
</html>