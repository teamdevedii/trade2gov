<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);
?>	
<div class="content_luar">
    <div class="content_dalam">
        <form id="frmNoRegistrasi" name="frmNoRegistrasi" action="<?= site_url('usermanage/setNoRegister/') ?>" onsubmit="return false;" class="form uniformForm" method="post" autocomplete=off>
            <?= $MAXLENGTH ?>
            <input type="hidden" id="KODE_TRADER" name="DATA[KODE_TRADER]" value="<?= $DATA['KODE_TRADER'] ?>">
            <input type="hidden" id="tipe" name="tipe" value="<?= $tipe ?>">
            <table width="100%" border="0">
                <tr>
                    <td width="100px">KPBC Daftar</td>
                    <td>: <input type="text" name="DATA[KPBC]" id="KDKPBC" value="<?= $DATA['KPBC']; ?>" class="ssstext" url="<?= site_url('referensi/autoComplate/KPBC'); ?>" urai="KDKPBC;URKPBC" onfocus="Autocomp(this.id)" angka="yes" wajib="yes" full="yes" maxlength="6" >&nbsp;
                        <button class="btn" onclick="tb_search('kpbc', 'KDKPBC;URKPBC', 'Kode Kpbc', this.form.id, 650, 400)" >...</button> <span id="URKPBC"><?= $DATA['URKPBC']; ?></span></td>
                </tr>
                <tr>
                    <td>Nomor Registrasi</td>
                    <td>: <input type="text" name="DATA[NOREG]" id="NOREG" value="<?= $DATA['NOREG']; ?>" class="ssstext" angka="yes" wajib="yes" full="yes" maxlength="6" ></td>
                </tr>
                <tr>
                    <td colspan="2" >&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" >
                        <button class="btn" onclick="save_post_msg('frmNoRegistrasi');"> Simpan No Registrasi </button>
                        <button class="btn" onclick="document.getElementById('frmNoRegistrasi').reset();"> Reset </button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>