<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);
?>	
<div class="content_luar">
    <div class="content_dalam">
        <form id="frmApproval" name="frmApproval" action="<?= site_url('usermanage/approvalRegistrasi') ?>" onsubmit="return false;" class="form uniformForm" method="post" autocomplete=off>
            <?= $MAXLENGTH ?>
            <input type="hidden" id="KODE_TRADER" name="DATA[KODE_TRADER]" value="<?= $DATA['KODE_TRADER'] ?>">
            <input type="hidden" id="USER_ID" name="DATA[USER_ID]" value="<?= $DATA['USER_ID'] ?>">
            <table width="100%" border="0">
                <tr>
                    <td width="50%" valign="top">
                        <table width="100%" border="0">
                            <tr>
                                <td colspan="2"><u><b>DATA PERUSAHAAN</b></u></td>
                </tr>
                <tr>
                    <td width="120px">Identitas *</td>
                    <td>: <?= form_dropdown('DATA[KODE_ID]', $JENIS_IDENTITAS, ($DATA['KODE_ID'] == '') ? '5' : $DATA['KODE_ID'], 'id="KODE_ID" wajib="yes" '); ?>&nbsp;<input type="text" name="DATA[ID]" id="ID" class="smtext" value="<?= $DATA['ID']; ?>" wajib="yes" onfocus="unformatNPWP(this.id);" onblur="formatNPWP(this.id);" wajib="yes" />
                    </td>
                </tr>
                <tr>
                    <td>Nama *</td>
                    <td>: <input type="text" name="DATA[NAMA]" id="NAMA" class="ltext" value="<?= $DATA['NAMA']; ?>" wajib="yes"/></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Alamat *</td>
                    <td><font style="vertical-align:top"> :</font> <textarea name="DATA[ALAMAT]" id="ALAMAT" class="ltext" wajib="yes"><?= $DATA['ALAMAT'] ?></textarea></td>
                </tr>
                <tr>
                    <td>Nomor Telepon *</td>
                    <td>: <input type="text" name="DATA[TELEPON]" id="TELEPON" class="smtext" value="<?= $DATA['TELEPON']; ?>" wajib="yes"/></td>
                </tr>
                <tr>
                    <td>Nomor Fax *</td>
                    <td>: <input type="text" name="DATA[FAX]" id="FAX" class="smtext" value="<?= $DATA['FAX']; ?>" wajib="yes"/></td>
                </tr>
                <tr>
                    <td>Tipe Trader</td>
                    <td>: <?= form_dropdown('DATA[TIPE_TRADER]', $TIPE_TRADER, $DATA['TIPE_TRADER'], 'id="TIPE_TRADER" wajib="yes" onchange="hidePPJK(this.value)"'); ?></td>
                </tr>
                <tr id="PPJK">
                    <td>No/Tgl PPJK</td>
                    <td>: <input type="text" name="DATA[NO_PPJK]" id="NO_PPJK" class="sstext" value="<?= $DATA['NO_PPJK']; ?>"/> &nbsp;<input type="text" name="DATA[TANGGAL_PPJK]" id="TANGGAL_PPJK" class="sstext" value="<?= $DATA['TANGGAL_PPJK']; ?>" formatDate="yes"/></td>
                </tr>
                <tr>
                    <td style="vertical-align: top" >Dokumen Terdaftar</td>
                    <td><?php
            $dokumen = explode(",", $DATA['DOKUMEN']);
            foreach ($dokumen as $val) {
                $arr[$val] = 'checked';
            }
            ?>
                        <div class="field-group control-group">	
                            <div class="field">&nbsp;
                                <input type="checkbox" name="DATA[DOKUMEN][BC20]" id="DOKUMEN[BC20]" value="BC20" <?= $arr['BC20'] ?>/>
                                <label for="DOKUMEN[BC20]">BC 2.0 / PIB</label> &nbsp; 
                                <button class="btn" id="bc20" onclick="fNoRegistrasi('bc20')"> No. Registrasi PIB </button>
                            </div>
                            <div class="field">&nbsp;
                                <input type="checkbox" name="DATA[DOKUMEN][BC30]" id="DOKUMEN[BC30]" value="BC30" <?= $arr['BC30'] ?>/>
                                <label for="DOKUMEN[BC30]">BC 3.0 / PEB</label>
                                <button class="btn" id="bc30" onclick="fNoRegistrasi('bc30')"> No. Registrasi PEB</button>
                            </div>
                            <div class="field">&nbsp;
                                <input type="checkbox" name="DATA[DOKUMEN][BC23]" id="DOKUMEN[BC23]" value="BC23" <?= $arr['BC23'] ?>/>
                                <label for="DOKUMEN[BC23]">BC 2.3 / TPB</label>&nbsp;
                                <button class="btn" id="bc23" onclick="fNoRegistrasi('bc23')"> No. Registrasi TPB</button>
                            </div>
                            <div class="field">&nbsp; 
                                <input type="checkbox" name="DATA[DOKUMEN][BC10]" id="DOKUMEN[BC10]" value="BC10" <?= $arr['BC10'] ?>/>
                                <label for="DOKUMEN[BC10]">MANIFEST</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                <button class="btn" id="bc10" onclick="fNoRegistrasi('bc10')">No. Registrasi Manifest</button> 
                            </div> 
                        </div> 
                    </td>
                </tr>
            </table>
            </td>
            <td valign="top">
                <table width="100%" border="0">
                    <tr>
                        <td colspan="2">
                    <u><b>DATA PEMILIK PERUSAHAAN</b></u></td>
            </tr>
            <tr>
                <td width="120px">Nama *</td>
                <td>: <input type="text" name="DATA[NAMA_PEMILIK]" id="NAMA_PEMILIK" class="mtext" value="<?= $DATA['NAMA_PEMILIK']; ?>" wajib="yes"/>
                </td>
            </tr>
            <tr>
                <td>Tempat/Tanggal Lahir *</td>
                <td>: <input type="text" name="DATA[TEMPAT_LAHIR_PEMILIK]" id="TEMPAT_LAHIR_PEMILIK" class="mtext" value="<?= $DATA['TEMPAT_LAHIR_PEMILIK']; ?>" wajib="yes"/>&nbsp;<input type="text" name="DATA[TANGGAL_LAHIR_PEMILIK]" id="TANGGAL_LAHIR_PEMILIK" class="sstext" formatDate="yes" value="<?= $DATA['TANGGAL_LAHIR_PEMILIK']; ?>" wajib="yes"/></td>
            </tr>
            <tr>
                <td style="vertical-align: top;">Alamat *</td>
                <td><font style="vertical-align:top"> :</font> <textarea name="DATA[ALAMAT_PEMILIK]" id="ALAMAT_PEMILIK" class="ltext" wajib="yes"><?= $DATA['ALAMAT_PEMILIK']; ?></textarea></td>
            </tr>
            <tr>
                <td style="vertical-align: top;">e-Mail *</td>
                <td>: <input type="text" name="DATA[EMAIL_PEMILIK]" id="EMAIL_PEMILIK" class="ltext" value="<?= $DATA['EMAIL_PEMILIK']; ?>" wajib="yes"/></td>
            </tr>
            <tr>
                <td colspan="2"><u><b>DATA EDI</b></u></td>
            </tr>
            <tr>
                <td>EDI Number*</td>
                <td>: <input type="text" name="DATA[EDINUMBER]" id="EDINUMBER" class="mtext" value="<?= $DATA['EDINUMBER']; ?>" wajib="yes" maxlength="20"/></td>
            </tr>
            <?php if ($DATA['STATUS'] == '01' && $this->newsession->userdata('KODE_TIPE_USER') == '2') { ?>
                <tr>
                    <td>Keputusan</td>
                    <td>:
                        <input type="radio" id='STATUS01' name="DATA[STATUS]" <?= $STATUS['01'] ?> value="01">
                        <label for="STATUS01">Disetujui</label>
                        &nbsp;
                        <input type="radio" id='STATUS02' name="DATA[STATUS]" <?= $STATUS['02'] ?> value="02">
                        <label for="STATUS02">Ditolak</label>
                    </td>
                </tr>
            <?php } ?>
            <tr>
                <td>Unlock Code *</td>
                <td>:
                    <input type="text" name="DATA[UNLOCKCODE]" id="UNLOCKCODE" class="vltext" value="<?= $DATA['UNLOCKCODE']; ?>" wajib="yes"/>
                </td>
            </tr>
            </table>
            </td>
            </tr>
            <tr>
                <td colspan="2">
                    <button class="btn" onclick="save_post_msg('frmApproval');"> &nbsp; Approve &nbsp; </button>
                    <button class="btn" onclick="document.getElementById('frmApproval').reset();"> &nbsp; Reset  &nbsp; </button>
                </td>
            </tr>
            </table>
        </form>
    </div>
</div>
<script>
    $(document).ready(function() {
        f_MAXLENGTH('frmApproval');
        hidePPJK($('#TIPE_TRADER').val());
    });
    function fNoRegistrasi(type){
        var id_trader = $('#KODE_TRADER').val();
        Dialog(site_url+'/usermanage/frmNoRegistrasi/'+type+'/'+id_trader, 'msgbox', 'Form Nomor Registrasi', '500', '250');
        return false;
    }
    function hidePPJK(kd) {
        if (kd == '2') {
            $('#PPJK').show();
            $('#NO_PPJK').focus();
        } else {
            $('#NO_PPJK').val('');
            $('#TANGGAL_PPJK').val('');
            $('#PPJK').hide();
        }
    }
</script>