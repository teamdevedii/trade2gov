<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
?>
<div class="content_luar">
    <div class="content_dalam">
        <form id="cetak_" method="post" autocomplete="off" onsubmit="return false;"> 
            <input id="CAR" type="hidden" value="<?= $car ?>"/>
            <fieldset>
                <legend> Pilihan Pencetakan </legend>
                <table width="100%">
                    <tr>
                        <td>
                            <input type="radio" name="CETAK" id="cetakTPB" value="1" checked > <label for="cetakTPB">&nbsp;Dokumen TPB</label><br>
                            <input type="radio" name="CETAK" id="cetakSSPCP" value="2"> <label for="cetakSSPCP">&nbsp;Dokumen SSPCP</label><br>
                            <input type="radio" name="CETAK" id="cetakLembarCatatanPencocokan" value="3" > <label for="cetakLembarCatatanPencocokan">&nbsp;Lembar Catatan Pencocokan</label><br>
                            <input type="radio" name="CETAK" id="cetakLembarCatatanPemeriksaanFisikBarang" value="4"> <label for="cetakLembarCatatanPemerikasaanFisikBarang">&nbsp;Lembar Catatan Pemeriksaan Fisik Barang</label><br>
                            <input type="radio" name="CETAK" id="cetakRespon" value="5"> <label for="cetakRespon">&nbsp;Respon</label>
                        </td> 
                    </tr>
                    <tr>
                        <td>
                            <br>
                            <button onclick="cetakNewWin('<?= $car ?>');" class="btn" ><span class="icon-new-window"></span> Cetak </button>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
</div>
<script>
    $('#msgboxSSPCPx').html('');
    function cetakNewWin(id){//alert(id);
        var car = '<?= $car; ?>';
        if($('input[name="CETAK"]:checked').val()=='2'){
            //cetak($car,$jnPage,$frmSSPCP=0)
            Dialog(site_url+'/bc23/cetak/'+car+'/2/1', 'msgbox2', ' Data SSPCP ', 650, 600);
        }
        else if($('input[name="CETAK"]:checked').val()=='3'){            
            window.open(site_url+'/bc23/cetak/'+car+'/'+$('input[name="CETAK"]:checked').val(),"RptBC23CatBC","scrollbars=yes, resizable=yes,width=1100,height=700");
        }
        else if($('input[name="CETAK"]:checked').val()=='4'){            
            window.open(site_url+'/bc23/cetak/'+car+'/'+$('input[name="CETAK"]:checked').val(),"RptBC23CatBC","scrollbars=yes, resizable=yes,width=1100,height=700");
        }
        else if($('input[name="CETAK"]:checked').val()=='5'){
            Dialog(site_url+"/bc23/beforecetakrespon/"+car+'', 'beforeCetakRespon', ' CETAK RESPON TPB ', 800, 450);
        }
        else{
            window.open(site_url+"/bc23/cetak/"+car+'/'+$('input[name="CETAK"]:checked').val(),"Cetak Dokumen TPB","scrollbars=yes, resizable=yes,width=1100,height=700");
        }        
    }
</script>