<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
?>
<div class="content_luar">
    <div class="content_dalam">
        <form id="fbm_" method="post" autocomplete="off" onsubmit="return false;">
            <table width="100%">
                <tr>
                    <td width="17%">Jenis Tarif</td>
                    <td width="83%"><span id="jenis-tarif"></span></td>
                </tr>     
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>  
            </table>

            <span id="Advolorum">
                <h4><span class="info_2">&nbsp;</span>Tarif Advolorum (%)</h4>
                <table width="100%">
                    <tr>
                        <td width="17%">Besar Tarif </td>
                        <td width="83%">
                            <input type="text" name="TARIF_BM" id="TARIF_BM" value="<?= $sessDETIL['TRPBM']; ?>" class="ssstext" maxlength="3"/> %</td>
                    </tr>
                </table>
            </span>

            <span id="Spesifik" style="display:none">
                <h4><span class="info_2">&nbsp;</span>Tarif Spesifik (Satuan)</h4>
                <table width="100%">
                    <tr>
                        <td width="17%">Besar Tarif </td>
                        <td width="83%">
                            <input type="text" name="TARIF_BM2" id="TARIF_BM2" value="<?= $sessDETIL['TRPBM']; ?>" class="sstext"/> SATUAN
                            &nbsp;Per : &nbsp;<input type="text" name="KODE_SATUAN_BM" id="KODE_SATUAN_BM" value="<?= $sessDETIL['KODE_SATUAN_BM']; ?>" class="ssstext" url="<?= site_url('referensi/autoComplate/SATUAN'); ?>" urai="KODE_SATUAN_BM;URKODE_SATUAN_BM" onfocus="Autocomp(this.id, this.form.id);" maxlength="3"/>&nbsp;<span id="URKODE_SATUAN_BM"></span></td>
                    </tr>
                    <tr>
                        <td>Jumlah </td>
                        <td><input type="text" name="JUMLAH_BM" id="JUMLAH_BM" value="<?= $sessDETIL['JUMLAH_BM']; ?>" class="sstext" /></td>
                    </tr>   
                </table>
            </span>

            <table width="100%">    
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button onclick="Addbm()" class="btn">OK</button>
                    </td>
                </tr>	        
            </table>
        </form>
    </div>
</div>
<script>
    $(function() {
        changeBM($("#fbarang_ #KODE_TARIF_BM").val());
        $("#fbm_ ").find("#TARIF_BM").val($("#fbarang_ #TRPBM").val());
        $("#fbm_ ").find("#TARIF_BM2").val($("#fbarang_ #TRPBM").val());
        $("#fbm_ ").find("#KODE_SATUAN_BM").val($("#fbarang_ #KODE_SATUAN_BM").val());
        $("#fbm_ ").find("#JUMLAH_BM").val($("#fbarang_ #JUMLAH_BM").val());
        if ($("#fbarang_ #KODE_TARIF_BM").val() == 2) {
            $("#fbm_ ").find("#jenis-tarif").html('<select id="KODE_TARIF_BM" onchange="changeBM(this.value)" class="text"><option value="1">1 - ADVALORUM</option><option value="2" selected>2 - SPESIFIK</option></select>');
        } else {
            $("#fbm_ ").find("#jenis-tarif").html('<select id="KODE_TARIF_BM" onchange="changeBM(this.value)" class="text"><option value="1" selected>1 - ADVALORUM</option><option value="2">2 - SPESIFIK</option></select>');
        }
    })

    function Addbm() {
        var jenis = $("#fbm_ #KODE_TARIF_BM").val();
        if (jenis == 1) {
            $("#fbarang_ #TRPBM").val($("#fbm_ #TARIF_BM").val());
            $("#tipebm").html('Advolorum<input type="hidden" name="TARIF[KDTRPBM]" id="KODE_TARIF_BM" value="' + jenis + '">');
            $("#tipespesifik").html('');
        } else {
            var kodesatu = $("#fbm_ #KODE_SATUAN_BM").val();
            var jumlahbm = $("#fbm_ #JUMLAH_BM").val();
            $("#tipespesifik").html('<table><tr><td align="right">Per</td><td><input type="text" name="TARIF[KDSATBM]" id="KODE_SATUAN_BM" class="ssstext" value="' + kodesatu + '" url="<?= site_url(); ?>/autocomplete/satuan" onfocus="Autocomp(this.id,this.form.id);" url="<?= site_url('referensi/autoComplate/SATUAN'); ?>" urai="KODE_SATUAN_BM" onfocus="Autocomp(this.id, this.form.id);" maxlength="3"/>&nbsp;&nbsp;Jumlah Satuan&nbsp;<input type="text" name="BARANG[SATBMJM]" id="JUMLAH_BM" class="sstext" value="' + jumlahbm + '"/></td></tr></table>');
            $("#tipebm").html('Spesifik<input type="hidden" name="TARIF[KDTRPBM]" id="KODE_TARIF_BM" value="' + jenis + '">');
            $("#fbarang_ #TRPBM").val($("#fbm_ #TARIF_BM2").val());
        }
        closedialog('dialog-bm');
    }
</script>