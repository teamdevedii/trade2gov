<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
if ($HEADER['TIPE_TRADER'] == '1') { // field importir readonly
    $ro = 'readonly';
}
//print_r($HEADER);die()
// die($.'ted');
?>

<script>
    $(document).ready(function () {
        onoff(false);
        declareDP('fbc23_');
        f_MAXLENGTH('fbc23_');
        prosesHargaHeader('fbc23_');
        BLAWB($('#MODA').val());
    });
    function onoff(on) {

        if ($('#CAR').val() == '') {
            $('.outentry').hide();
        } else {
            $('.outentry').show();
        }

        if (on) {
            $('#on').show();
            $('#off').hide();
            $("#fbc23_ :input").removeAttr("disabled");
            $('#fbc23_ .outentry :input').attr('disabled', 'disabled');
            $("#tabs").tabs({disabled: [1, 2]});
        } else {
            $('#on').hide();
            $('#off').show();
            $("#fbc23_ :input").attr('disabled', 'disabled');
            $('#fbc23_ .outentry :input').removeAttr("disabled");
            $("#tabs").tabs();
            if ($("#act").val() == 'save') {
                $("#tabs").tabs({disabled: [1]});
            } else {
                $("#tabs").tabs({disabled: false});
            }
        }
        $('.onterus').removeAttr('disabled');
        $('.offterus :input').attr('readonly', 'readonly');
    }

    function BLAWB(ID_MODA) {
        if (ID_MODA == '1' || ID_MODA == '') {
            $('#BLAWB').html('BL');
        } else if (ID_MODA == '4') {
            $('#BLAWB').html('AWB');
        } else {
            $('#BLAWB').html('');
        }
    }

    function cekValidasi(car) {
        $(function () {
            $('#msgbox').html('');
            $("#msgbox").html('<div style="margin-left: 15px; margin-top: 15px;"><img src="' + base_url + 'img/loaders/facebook.gif" /> loading...</div>');
            $("#msgbox").dialog({
                resizable: false,
                height: 500,
                modal: true,
                width: 700,
                title: 'Validasi Dokumen',
                open: function () {
                    $.ajax({
                        type: 'POST',
                        url: site_url + '/bc23/alertcekstatus/' + car + '/t_bc23hdr',
                        success: function (data) {
                            $("#msgbox").html(data);
                            $('#URSTATUS').html($('#STATUSCEK').val());
                            call(site_url + '/bc23/refreshHeader/' + car, 'tab-Header');
                        }
                    });
                },
                buttons: {
                    Close: function () {
                        $(this).dialog("close");
                    }
                }
            });
        });
    }

    function gotoQUEUED(car,tipe,sts) {
        jConfirm('Data akan dibentuk EDIFACT ?', site_name,
        function(r) {
            if (r == true) {
                //jloadings();
                $(function() {
                    $('#msgbox').html('');
                    $("#msgbox").html('<div style="margin-left: 15px; margin-top: 15px;"><img src="' + base_url + 'img/loaders/facebook.gif" /> loading...</div>');
                    $("#msgbox").dialog({
                        resizable: false,
                        height: 400,
                        modal: true,
                        width: 600,
                        title: 'Edifact / unEdifact',
                        open: function() {
                            $.ajax({
                                type: 'POST',
                                url: site_url+'/bc23/queued',
                                data: '&CAR=' + car,
                                error: function(){
                                    Clearjloadings();
                                },
                                success: function(data) {
                                    Clearjloadings();
                                    $("#msgbox").html(data);
                                    call(site_url + '/bc23/refreshHeader/' + car, 'tab-Header');
                                }
                            });
                        },
                        buttons: {
                            Close: function() {
                                $(this).dialog("close");
                            }
                        }
                    });
                });
            }else {
                return false;
            }
        });
    }
</script> 
<span id="DivHeaderForm">
    <span id="divtmp"></span>
    <form id="fbc23_" name="fbc23_" action="<?= site_url('bc23/setHeader'); ?>" method="post" autocomplete="off" onsubmit="return false;">
    <?= $MAXLENGTH ?>
        <input type="hidden" name="act" id="act" value="<?= $act; ?>" />
        <input type="hidden" name="HEADER[STATUS]" id="STATUS" value="<?= $HEADER['STATUS']; ?>" />
        <input type="hidden" name="HEADER[CAR]" id="CAR" value="<?= $HEADER['CAR']; ?>" />
        
            <span id="off">
                <button onclick="onoff(true);return false;" class="btn onterus"><span class="icon-pen"></span> Edit </button>
            </span>
            <span id="on">
                <button onclick="save_post_msg('fbc23_');onoff(false);" class="btn onterus"><span class="icon-download"></span> Simpan</button>
                <button onclick="onoff(false);" class="btn onterus"><span class="icon-undo"></span> Batal</button>
            </span>
            <span class="msgheader_" style="margin-left:20px">&nbsp;</span>
            <?php if ($HEADER['CAR']) { ?>
                <button id="btnQUEUED" onclick="gotoQUEUED($('#CAR').val(),'<?= $TIPE_TRADER;?>',$('#STATUS').val())" class="btn onterus" style="float: right; margin-right: 5px;" >
                    <span class="icon-mail"></span> Edifact / UnEdifact
                </button>
            <?php } ?>
                
        <fieldset style="margin-top: 5px;">
            <table width="100%">             
            <?php if ($HEADER['CAR']) { ?>
                <tr>
                    <td> &nbsp;&nbsp;&nbsp;Nomor Pengajuan</td>
                    <td><b><?= substr($HEADER['CAR'], 0, 6) . '-' . substr($HEADER['CAR'], 7, 6) . '-' . substr($HEADER['CAR'], 12, 8) . '-' . substr($HEADER['CAR'], 20, 6); ?></b></td>
                    <td align="right"><button onclick="cekValidasi($('#CAR').val())" class="btn onterus outentry"><span class="icon-check"></span> Periksa Kelengkapan [ <span id="URSTATUS"><?= $HEADER['URSTATUS']; ?></span> ] </button> </td>
                </tr> 
            <?php } ?>
                <tr> 
                    <td style="width: 135px;"> &nbsp;&nbsp;&nbsp;KPBC Pendaftaran</td>
                    <td>
                        <input type="text"  name="HEADER[KDKPBC]" id="KDKPBC" value="<?= $HEADER['KDKPBC']; ?>" onblur="checkCode(this);" grp="kpbc" urai="URKDKPBC" class="ssstext"/> <button onclick="tb_search('kpbc', 'KDKPBC;URKDKPBC', 'Kode Kpbc', this.form.id, 650, 400);" class="btn" >...</button> <span id="URKDKPBC"><?= $HEADER['URKDKPBC']; ?></span><br> 
                    </td>
                </tr>                       
                <tr>
                    <td>A. Tujuan</td>                             
                    <td> <?= form_dropdown('HEADER[TUJUAN]', $TUJUAN, $HEADER['TUJUAN'], 'wajib="yes" id="TUJUAN" class="vvltext"'); ?> </td>                            
                </tr>
                <tr>
                    <td>B. Jenis Barang</td> 
                    <td> <?= form_dropdown('HEADER[JNSBARANG]', $JNSBARANG, $HEADER['JNSBARANG'], 'wajib="yes" id="JNSBARANG" class="vvltext"'); ?> </td>
                </tr>                        
                <tr>
                    <td>C. Tujuan Pengiriman</td>
                    <td> <?= form_dropdown('HEADER[TUJUANKIRIM]', $TUJUANKIRIM, $HEADER['TUJUANKIRIM'], 'wajib="yes" id="TUJUANKIRIM" class="vvltext"'); ?></td>                            
                </tr>
            </table>
        </fieldset>
                
        
        <div class="judul">D. DATA PEMBERITAHUAN :</div>
        <table width="100%" border="0">
            <tr>
                <td width="50%" valign="top">
                    <fieldset>
                        <legend>Pemasok</legend>
                        <table width="100%">
                            <tr>
                                <td width="135px">Nama </td>
                                <td>
                                    <input wajib="no" type="text" name="HEADER[PASOKNAMA]" id="PASOKNAMA" value="<?= $HEADER['PASOKNAMA']; ?>" url="<?= site_url('referensi/autoComplate/PEMASOK'); ?>" urai="PASOKNAMA;PASOKALMT;PASOKNEG;URPASOKNEG" onfocus="Autocomp(this.id)" class="ltext"/>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top">Alamat </td>
                                <td>
                                    <textarea name="HEADER[PASOKALMT]" id="PASOKALMT" wajib="no" class="ltext" onkeyup="limitChars(this.id, 70, 'limitAlamatPemasok')"><?= $HEADER['PASOKALMT']; ?></textarea> <div id="limitAlamatPemasok"></div></td>
                            </tr>
                            <tr>
                                <td>Negara Asal </td>
                                <td>
                                    <input type="text" name="HEADER[PASOKNEG]" id="PASOKNEG" value="<?= $HEADER['PASOKNEG']; ?>" onblur="checkCode(this);" grp="negara" urai="URPASOKNEG" class="sssstext" urai="PASOKNEG;URPASOKNEG" wajib="no"/> &nbsp;
                                    <button onclick="tb_search('negara', 'PASOKNEG;URPASOKNEG', 'Kode Negara', this.form.id, 650, 400)" class="btn"> ... </button>
                                    <span id="URPASOKNEG"><?= $HEADER['URPASOKNEG']; ?></span>
                                </td>
                            </tr>
                        </table>
                    </fieldset>    
                    <fieldset>
                        <legend>Importir</legend>
                        <table width="100%" >
                            <tr>
                                <td width="135px">2. Identitas</td>
                                <td><?= form_dropdown('HEADER[USAHAID]', $JENIS_IDENTITAS, $HEADER['USAHAID'], 'wajib="yes" id="USAHAID" class="mtext"'); ?> 
                                    <input type="text" name="HEADER[USAHANPWP]" id="USAHANPWP" value="<?= $this->fungsi->FORMATNPWP($HEADER['USAHANPWP']); ?>" class="smtext" wajib="no" url="<?= site_url('referensi/autoComplate/IMPORTIR/IMPNPWP'); ?>" urai="USAHAID;USAHANPWP;USAHANAMA;USAHAALMT" onfocus="Autocomp(this.id);unformatNPWP(this.id);" onblur="formatNPWP(this.id);" <?= $ro; ?> maxlenght="15"/> 
                                </td>                                                              
                            </tr> 
                            <tr>
                                <td>3. Nama</td>
                                <td>
                                    <input type="text" name="HEADER[USAHANAMA]" id="USAHANAMA" value="<?= $HEADER['USAHANAMA']; ?>" class="ltext" maxlength="50" wajib="no" url="<?= site_url('referensi/autoComplate/IMPORTIR/IMPNAMA'); ?>" urai="USAHAID;USAHANPWP;USAHANAMA;USAHAALMT" onfocus="Autocomp(this.id);" <?= $ro; ?>/>
                                </td>
                            </tr>
                            <tr >
                                <td>&nbsp;&nbsp;&nbsp;Skep Izin</td>
                                <td>
                                    <input type="text" name="HEADER[REGISTRASI]" id="REGISTRASI" value="<?= $HEADER['REGISTRASI']; ?>" class="ltext" maxlength="50" wajib="no"/>
<?= form_dropdown('HEADER[KDTPB]', $KDTPB, $HEADER['KDTPB'], 'wajib="yes" id="KDTPB" class="smtext"'); ?>
                                </td>
                            </tr>                                
                            <tr>
                                <td style="vertical-align: top">&nbsp;&nbsp;&nbsp;Alamat</td>
                                <td>
                                    <textarea name="HEADER[USAHAALMT]" id="USAHAALMT" class="ltext" wajib="no" onkeyup="limitChars(this.id, 70, 'limitUSAHAALMT')" <?= $ro; ?>><?= $HEADER['USAHAALMT']; ?></textarea>
                                    <div id="limitINDALMT"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>4. Status </td>
                                <td> 
<?= form_dropdown('HEADER[USAHASTATUS]', $STATUS_PERUSAHAAN, $HEADER['USAHASTATUS'], 'wajib="yes" id="USAHASTATUS" class="ltext"'); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;APIP / APIU</td>
                                <td>
<?= form_dropdown('HEADER[APIKD]', $APIKD, $HEADER['APIKD'], 'id="APIKD" class="stext" '); ?>
                                    <input type="text" name="HEADER[APINO]" id="APINO" value="<?= $HEADER['APINO'] ?>" url="<?= site_url(); ?>/autocomplete/importir" class="smtext" maxlength="15"/>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
<?php if ($TIPE_TRADER == '2') { ?>                    
                        <fieldset>
                            <legend>PPJK</legend>
                            <table width="100%">
                                <tr>
                                    <td width="135px">6. Identitas</td>
                                    <td>                                  
                                        <?= form_dropdown('HEADER[PPJKID]', $JENIS_IDENTITAS, $HEADER['PPJKID'], 'wajib="yes" id="PPJKID" class="mtext"'); ?> 
                                        <input type="text" name="HEADER[PPJKNPWP]" id="PPJKNPWP" value="<?= $this->fungsi->FORMATNPWP($HEADER['PPJKNPWP']); ?>" class="smtext" wajib="no" readonly="readonly" maxlenght="15"/> 
                                    </td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>7. Nama</td>
                                    <td><input type="text" name="HEADER[PPJKNAMA]" id="PPJKNAMA" value="<?= $HEADER['PPJKNAMA']; ?>" class="ltext" readonly="readonly"/></td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top" >&nbsp;&nbsp;&nbsp; Alamat</td>
                                    <td>
                                        <textarea name="HEADER[PPJKALMT]" id="PPJKALMT" class="ltext" onkeyup="limitChars(this.id, 70, 'limitPPJKALMT')" readonly="readonly"><?= $HEADER['PPJKALMT']; ?></textarea>
                                        <div id="limitPPJKALMT"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>8. No & Tgl Surat Ijin PPJK</td>
                                    <td>
                                        <input type="text" name="HEADER[PPJKNO]" id="PPJKNO" value="<?= $HEADER['PPJKNO']; ?>" class="mtext" readonly="readonly"/> <input type="text" name="HEADER[PPJKTG]" id="PPJKTG" class="sstext" value="<?= $HEADER['PPJKTG']; ?>" maxlength="10" formatDate="yes"  readonly="readonly"/>
                                    </td>
                                </tr>
                            </table>
                        </fieldset> 
<?php } ?>                    
                    <fieldset>
                        <legend>Data Sarana Angkut</legend>
                        <table width="100%">
                            <tr>
                                <td width="135px">9. Moda - Nama</td>
                                <td>
<?= form_dropdown('HEADER[MODA]', $MODA, $HEADER['MODA'], '  id="MODA" class="stext"'); ?> <input type="text" name="HEADER[ANGKUTNAMA]" id="ANGKUTNAMA" value="<?= $HEADER['ANGKUTNAMA']; ?>" class="mtext" url="<?= site_url('referensi/autoComplate/MODA/ANGKUTNAMA'); ?>" urai="MODA;ANGKUTNAMA;ANGKUTFL;URANGKUTFL" onfocus="Autocomp(this.id);" />
                                </td>
                            </tr>
                            <tr>
                                <td>10. No. Voy/Flight </td>
                                <td>
                                    <input type="text" name="HEADER[ANGKUTNO]" id="ANGKUTNO" value="<?= $HEADER['ANGKUTNO']; ?>" class="stext" maxlength="7" />
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bendera</td>
                                <td>
                                    <input type="text" name="HEADER[ANGKUTFL]" id="ANGKUTFL" value="<?= $HEADER['ANGKUTFL']; ?>" class="sssstext" onblur="checkCode(this);" grp="negara" urai="URANGKUTFL" urai="ANGKUTFL;URANGKUTFL"/> <button onclick="tb_search('negara', 'ANGKUTFL;URANGKUTFL', 'Kode Negara', this.form.id, 650, 400);" class="btn">...</button> <span id="URANGKUTFL"><?= $HEADER['URANGKUTFL']; ?></span>
                                </td>
                            </tr>
                        </table>
                    </fieldset>                   
                    <fieldset>
                        <legend>Data Pelabuhan</legend>
                        <table width="100%">
                            <tr>
                                <td width="135px">11. Pelabuhan Muat</td>
                                <td>
                                    <input type="text" name="HEADER[PELMUAT]" id="PELMUAT" value="<?= $HEADER['PELMUAT']; ?>" onblur="checkCode(this);" grp="pelabuhan" urai="URPELMUAT" urai="PELMUAT;URPELMUAT" class="ssstext"/> <button onclick="tb_search('pelabuhan', 'PELMUAT;URPELMUAT', 'Kode Pelabuhan Muat', this.form.id, 650, 400);" class="btn">...</button> <span id="URPELMUAT"><?= $HEADER['URPELMUAT']; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>12. Pelabuhan Transit</td>
                                <td>
                                    <input type="text" name="HEADER[PELTRANSIT]" id="PELTRANSIT" value="<?= $HEADER['PELTRANSIT']; ?>" onblur="checkCode(this);" grp="pelabuhan" urai="URPELTRANSIT" urai="PELTRANSIT;URPELTRANSIT" class="ssstext"/> <button onclick="tb_search('pelabuhan', 'PELTRANSIT;URPELTRANSIT', 'Kode Pelabuhan Transit', this.form.id, 650, 400);" class="btn">...</button> <span id="URPELTRANSIT"><?= $HEADER['URPELTRANSIT']; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>13. Pelabuhan Bongkar</td>
                                <td>
                                    <input type="text" name="HEADER[PELBKR]" id="PELBKR" value="<?= $HEADER['PELBKR']; ?>" onblur="checkCode(this);" grp="pelabuhan" urai="URPELBKR" urai="PELBKR;URPELBKR" class="ssstext"/> <button onclick="tb_search('pelabuhan', 'PELBKR;URPELBKR', 'Kode Pelabuhan Bongkar', this.form.id, 650, 400);" class="btn">...</button> <span id="URPELBKR"><?= $HEADER['URPELBKR']; ?></span>
                                </td>
                            </tr>
                        </table>
                    </fieldset>                                                         
                </td>
                <td valign="top">
                    <fieldset>
                        <legend>F. Diisi oleh Bea & Cukai</legend>
                        <table width="100%">
                            <tr>
                                <td width="135px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No / Tgl Pendaftaran</td>
                                <td>
                                    <input type="text" id="BC23NO" value="<?= $HEADER['BC23NO']; ?>" class="mtext" readonly="readonly"/>
                                    <input type="text" id="BC23TG" class="sstext" value="<?= $HEADER['BC23TG']; ?>" maxlength="10" formatDate="yes" onfocus="ShowDP(this.id)" readonly="readonly"/>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;KPBC Bongkar</td>
                                <td>
                                    <input type="text"  name="HEADER[KDKPBCBONGKAR]" id="KDKPBCBONGKAR" value="<?= $HEADER['KDKPBCBONGKAR']; ?>" onblur="checkCode(this);" grp="kpbc" urai="URKDKPBCBONGKAR" class="ssstext" maxlength="6" /> <button onclick="tb_search('kpbc', 'KDKPBCBONGKAR;URKDKPBCBONGKAR', 'Kode Kpbc', this.form.id, 650, 400);" class="btn">...</button> <span id="URKDKPBCBONGKAR"><?= $HEADER['URKDKPBCBONGKAR']; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;KPBC Pengawas</td>
                                <td>
                                    <input type="text"  name="HEADER[KDKPBCAWAS]" id="KDKPBCAWAS" value="<?= $HEADER['KDKPBCAWAS']; ?>" onblur="checkCode(this);" grp="kpbc" urai="URKDKPBCAWAS" class="ssstext"/> <button onclick="tb_search('kpbc', 'KDKPBCAWAS;URKDKPBCAWAS', 'Kode Kpbc', this.form.id, 650, 400);" class="btn">...</button> <span id="URKDKPBCAWAS"><?= $HEADER['URKDKPBCAWAS']; ?></span><br> 
                                </td>
                            </tr>
                            </td>
                        </table>
                    </fieldset>
                    <fieldset>
                        <legend>Data Dokumen</legend>
                        <table  width="100%">
                            <td colspan="2" >                                 
                            <tr>
                                <td>14. Invoice</td>
                                <td>
                                    <input type="text" value="<?= $HEADER['NOINVOICE']; ?>" readonly="readonly"/> 
                                    <input type="text" value="<?= $HEADER['TGINVOICE']; ?>" class="sstext date" formatDate="yes" maxlength="10" readonly="readonly"/>
                                </td>
                            </tr>
                            <tr> 
                                <td>15. SK/PEB</td>
                                <td>
                                    <input type="text" value="<?= $HEADER['SKPEB']; ?>" readonly="readonly"/> 
                                    <input type="text" value="<?= $HEADER['TGSKPEB']; ?>" class="sstext date" formatDate="yes" maxlength="10" readonly="readonly"/>
                                </td>
                            </tr>
                            <tr> 
                                <td>16. L/C</td>
                                <td>
                                    <input type="text" value="<?= $HEADER['LC']; ?>" readonly="readonly"/> 
                                    <input type="text" value="<?= $HEADER['TGLC']; ?>" class="sstext date" formatDate="yes" maxlength="10" readonly="readonly"/>
                                </td>
                            </tr> 
                            <tr> 
                                <td>17. BL/AWB</td>
                                <td>
                                    <input type="text" value="<?= ($HEADER['BL']!='')?$HEADER['BL']:$HEADER['AWB']; ?>" readonly="readonly"/>
                                    <input type="text" value="<?= ($HEADER['BL']!='')?$HEADER['TGBL']:$HEADER['TGAWB']; ?>" class="sstext date" formatDate="yes" maxlength="10" readonly="readonly"/>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    18.  <?= form_dropdown('HEADER[DOKTUPKD]', $DOKTUPKD, $HEADER['DOKTUPKD'], 'id="DOKTUPKD" class="stext" style="width:100px"'); ?>
                                </td>
                                <td>
                                    <input type="text" name="HEADER[DOKTUPNO]" id="DOKTUPNO" value="<?= $HEADER['DOKTUPNO'] ?>" format="angka"/>
                                    <input type="text" name="HEADER[DOKTUPTG]" id="DOKTUPTG" value="<?= $HEADER['DOKTUPTG'] ?>" class="sstext" formatDate="yes">
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nomor Pos&nbsp;</td>
                                <td>
                                    <input type="text" name="HEADER[POSNO]" id="POSNO" value="<?= $HEADER['POSNO'] ?>" class="ssstext" format="angka" >&nbsp;&nbsp;Sub Pos &nbsp;
                                    <input type="text" name="HEADER[POSSUB]" id="POSSUB" value="<?= $HEADER['POSSUB'] ?>" class="ssstext" format="angka">&nbsp;&nbsp;Sub-Sub Pos
                                    <input type="text" name="HEADER[POSSUBSUB]" id="POSSUBSUB" value="<?= $HEADER['POSSUBSUB'] ?>" class="ssstext" format="angka">
                                </td>
                            </tr>
                            <tr id="dataBaru2" class="outentry" >
                                <td colspan="2"><br />
                                    <h4><span class="info_2">&nbsp;</span>Detil Dokumen</h4>
<?= $data_dokumen; ?>
                                </td>
                            </tr>
                            </td>
                            </tr>
                        </table>
                    </fieldset>
                    <fieldset>
                        <legend>Lain - Lain</legend>
                        <table width="100%">
                            <tr>
                                <td>19. Tempat Penimbunan</td>
                                <td>
                                    <input type="text" name="HEADER[TMPTBN]" id="TMPTBN" value="<?= $HEADER['TMPTBN']; ?>" onblur="checkCode(this);" grp="timbun" urai="URTMPTBN" urai="TMPTBN;URTMPTBN" class="ssstext" wajib="no" maxlength="4"/>&nbsp;
                                    <button onclick="tb_search('timbun', 'TMPTBN;URTMPTBN', 'Kode Timbun', this.form.id, 650, 400, 'KDKPBC;')" class="btn"> ... </button>
                                    <span id="URTMPTBN"><?= $HEADER['URTMPTBN'] == '' ? $URTMPTBN : $HEADER['URTMPTBN']; ?></span>
                                </td>
                            </tr>                       
                            <tr>
                                <td>20. Valuta </td>
                                <td>
                                    <input type="text" name="HEADER[KDVAL]" id="KDVAL" value="<?= $HEADER['KDVAL']; ?>" onblur="checkCode(this);" grp="valuta" urai="URKDVAL" urai="KDVAL;URKDVAL" class="ssstext" wajib="no" maxlength="3" />
                                    <button onclick="tb_search('valuta', 'KDVAL;URKDVAL', 'Kode Valuta', this.form.id, 650, 400)" class="btn"> ... </button> 
                                    <span id="URKDVAL"><?= $HEADER['URKDVAL'] == '' ? $URKDVAL : $HEADER['URKDVAL']; ?></span>
                                </td>                                    
                            </tr>
                            <tr>
                                <td>21. NDPBM (Kurs) Rp. </td>
                                <td>
                                    <input type="text" name="nilai_ndpbm" id="nilai_ndpbm" class="stext" align="right" value="<?= number_format($HEADER['NDPBM'], 4); ?>" wajib="yes" onkeyup="this.value = ThausandSeperator('NDPBM', this.value, 4);
                                            prosesHargaHeader('fbc23_')" style="text-align:right;width:100px;"/>
                                    <input type="hidden" name="HEADER[NDPBM]" id="NDPBM" value="<?= $HEADER['NDPBM'] ?>" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                    <fieldset>
                        <legend>Data Harga</legend>
                        <table width="100%">
                            <tr>
                                <td width="15%">22. FOB</td>
                                <td width="35%">
                                    <input type="text" name="FOBUR" id="FOBUR" value="<?= number_format($HEADER['FOB'], 2); ?>" class="stext" style="text-align:right;width:100px;" readonly="readonly">
                                    <input type="hidden" name="HEADER[FOB]" id="FOB" value="<?= $HEADER['FOB'] ?>" />
                                </td>                                
                            </tr>
                            <tr>
                                <td>23. Freight</td>
                                <td>
                                    <input type="text" name="FREIGHTUR" id="FREIGHTUR" value="<?= number_format($HEADER['FREIGHT'], 2); ?>" class="stext" style="text-align:right;width:100px;" readonly="readonly" />
                                    <input type="hidden" name="HEADER[FREIGHT]" id="FREIGHT" value="<?= $HEADER['FREIGHT'] ?>" />
                                </td>
                                <td>24. Asuransi</td>
                                <td>
                                    <input type="text" name="ASURANSIUR" id="ASURANSIUR" value="<?= number_format($HEADER['ASURANSI'], 2); ?>" class="stext" style="text-align:right;width:100px;" readonly="readonly"/>
                                    <input type="hidden" name="HEADER[ASURANSI]" id="ASURANSI" value="<?= $HEADER['ASURANSI'] ?>" /></td>
                            </tr>
                            <tr>
                                <td>25. Nilai CIF </td>
                                <td>
                                    <input type="text" name="CIFUR" id="CIFUR" value="<?= number_format($HEADER['CIF'], 2); ?>" class="stext" style="text-align:right;width:100px;" readonly="readonly">
                                    <input type="hidden" name="HEADER[CIF]" id="CIF" value="<?= $HEADER['CIF'] ?>" />
                                </td>
                                <td>CIF (Rp)</td>
                                <td>
                                    <input type="text" name="HEADER[CIFRP]" id="CIFRP" value="<?= number_format($HEADER['CIFRP'], 2); ?>" class="stext" style="text-align:right;width:100px;" readonly="readonly" >
                                </td>
                            </tr>
                            <tr>
                                <td>28. Bruto (Kg) </td>
                                <td colspan="3">
                                    <input type="text" name="BRUTOUR" id="BRUTOUR" value="<?= number_format($HEADER['BRUTO'], 2); ?>" class="sstext" wajib="yes" format="angka" onkeyup="this.value = ThausandSeperator('BRUTO', this.value, 2);" style="text-align:right;"/> 
                                    <input type="hidden" name="HEADER[BRUTO]" id="BRUTO" value="<?= $HEADER['BRUTO'] ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td>29. Netto (Kg) </td>
                                <td colspan="3">
                                    <input type="text" name="NETTOUR" id="NETTOUR" value="<?= number_format($HEADER['NETTO'], 2); ?>" class="sstext" wajib="yes" format="angka" onkeyup="this.value = ThausandSeperator('NETTO', this.value, 2);" style="text-align:right;"/> 
                                    <input type="hidden" name="HEADER[NETTO]" id="NETTO" value="<?= $HEADER['NETTO']; ?>"/>
                                </td>   
                            </tr> 
                            <tr> 
                                <td colspan="4" >
                                    <input type="hidden" name="HEADER[KDASS]" id="KDASS" value="<?= $HEADER['KDASS']; ?>" />
                                    <input type="hidden" name="HEADER[BTAMBAHAN]" id="BTAMBAHAN" value="<?= $HEADER['BTAMBAHAN']; ?>" />
                                    <input type="hidden" name="HEADER[DISKON]" id="DISKON" value="<?= $HEADER['DISKON']; ?>" />
                                    <input type="hidden" name="HEADER[KDHRG]" id="KDHRG" value="<?= $HEADER['KDHRG']; ?>" />
                                    <input type="hidden" name="HEADER[NILINV]" id="NILINV" value="<?= $HEADER['NILINV']; ?>" />
                                </td>
                            </tr>

                            <tr>
                                <td colspan="4" align="center"><br />
                                    <button onclick="EditHarga('NDPBM;KDVAL;CIF;FOB;FREIGHT;ASURANSI;KDHRG;BTAMBAHAN;DISKON;KDASS;CIFRP;NILINV', this.form.id)" class="btn"> Edit Harga </button>
                                </td>
                            </tr>
                        </table>
                    </fieldset>                    
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table width="100%" id="dataBaru" class="outentry">
                        <tr style="vertical-align: top;">
                            <td width="50%">
                                <fieldset>
                                    <legend><b>26. Kontainer / Peti Kemas</b></legend>
<?= $DATA_CONT; ?>
                                </fieldset>
                            </td>
                            <td>
                                <fieldset>
                                    <legend><b>27. Jumlah dan Jenis Kemasan</b></legend>
<?= $DATA_KEMASAN; ?>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2"><br>
                    <h4>DATA PUNGUTAN</h4>
                    <table border="0" width="500px" cellpadding="0" cellspacing="0" class="table-bordered">
                        <thead>
                            <tr>
                                <td width="200px" align="left" style="height: 30px;">
                                    <button onclick="detilPungutan($('#CAR').val())" class="btn onterus"><span class="icon-list"></span> Jenis Pungutan </button>
                                </td>
                                <td width="150px" align="center">Dibayar (Rp) </td>
                                <td width="150px" align="center">Ditangguhkan (Rp)</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="padding-left:10px">BM</td>
                                <td align="right"><?= number_format($data_pgt[1][0]) ?>&nbsp;</td>
                                <td align="right"><?= number_format($data_pgt[1][2]) ?>&nbsp;</td>                            
                            </tr>
                            <tr>
                                <td style="padding-left:10px">Cukai</td>
                                <td align="right" ><?= number_format($data_pgt[5][0]) ?>&nbsp;</td>
                                <td align="right" ><?= number_format($data_pgt[5][2]) ?>&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="padding-left:10px;" class="border-brl">PPN</td>
                                <td align="right"><?= number_format($data_pgt[2][0]) ?>&nbsp;</td>
                                <td align="right"><?= number_format($data_pgt[2][2]) ?>&nbsp;</td>                           
                            </tr>
                            <tr>
                                <td style="padding-left:10px;" class="border-brl">PPnBM</td>
                                <td align="right"><?= number_format($data_pgt[3][0]) ?>&nbsp;</td>
                                <td align="right"><?= number_format($data_pgt[3][2]) ?>&nbsp;</td>                                
                            </tr>
                            <tr>
                                <td style="padding-left:10px;" class="border-brl">PPh</td>
                                <td align="right"><?= number_format($data_pgt[4][0]) ?>&nbsp;</td>
                                <td align="right"><?= number_format($data_pgt[4][2]) ?>&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="padding-left:10px;" class="border-brl"><button onclick="showPNBP($('#CAR').val())" class="btn onterus"> PNBP </button></td>                                                            
                                <td align="right"><?= number_format($data_pgt[8][0]) ?>&nbsp;</td>
                                <td align="right"><?= number_format($data_pgt[8][2]) ?>&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="padding-left:10px;" class="border-brl"><b>TOTAL</b></td>
                                <td align="right"><b><?= number_format($data_pgt['TOTAL'][0]) ?>&nbsp;</b></td>
                                <td align="right"><b><?= number_format($data_pgt['TOTAL'][2]) ?>&nbsp;</b></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <table width="50%" border="0" align="center" cellpadding="5">
                        <tr><td colspan="2" class="rowheight" align="center"><h5>DATA PENANDATANGAN DOKUMEN</h5></td></tr>
                        <tr align="center">
                            <td width="50%">TEMPAT</td>
                            <td width="50%">TANGGAL (DD-MM-YYYY)</td>
                        </tr>
                        <tr align="center">
                            <td>
                                <input type="text" name="HEADER[KOTATTD]" id="KOTATTD" value="<?= $HEADER['KOTATTD']; ?>" class="stext" style="text-align:center; width:150px"/></td>
                            <td>
                                <input type="text" name="HEADER[TGLTTD]" id="TGLTTD" value="<?= ($HEADER['TGLTTD'] == '') ? date('Y-m-d') : $HEADER['TGLTTD']; ?>" class="stext date" style="text-align:center; width:150px"/></td>
                        </tr>
                        <tr align="center">
                            <td colspan="2">NAMA</td>
                        </tr>
                        <tr align="center"><td colspan="2"><input type="text" name="HEADER[NAMATTD]" id="NAMATTD" value="<?= $HEADER['NAMATTD']; ?>" class="stext" style="text-align:center; width:150px"/></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</span>
