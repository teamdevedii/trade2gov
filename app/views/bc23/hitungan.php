<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
$arr = explode(";", $data);
foreach ($arr as $isi) {
    $arrX = explode("|", $isi);
    $val[$arrX[0]] = $arrX[1];
}
?>
<div class="content_luar">
    <div class="content_dalam">
        <form method="post" name="harga" id="harga" onsubmit="return false;"  >
            <h4><span class="info_2">&nbsp;</span>Isi sesuai Invoice/Dokumen lain</h4>
            <table border="0">
                <tr>
                    <td width="100px;">Kode Harga</td>
                    <td width="220px;">:
                        <?= form_dropdown('HEADER[KDHRG]', $KODE_HARGA, $val['KDHRG'], 'id="KODE_HARGAHarga" class="stext" onchange="kode(this.value)" onblur="kode(this.value)" style="width: 122px"'); ?>
                    </td>
                    <td class="hargacif" width="100px;">Harga CIF</td>
                    <td width="220px;">:
                        <input type="text" name="HARGA_CIFUR" class="smtext" id="HARGA_CIFUR" onkeyup="this.value = ThausandSeperator('HARGA_CIF', this.value, 4); prosesHarga('harga');" style="text-align:right;" value="<?= $this->fungsi->FormatRupiah($val['NILINV'], 4); ?>"/>
                        <input type="hidden"  name="HARGA_CIF" id="HARGA_CIF" value="<?= $val['NILINV'] ?>" />
                    </td>
                </tr>
                <tr>
                    <td>Valuta</td>
                    <td>:
                        <input type="text" name="HEADER[KODE_VALUTA]" id="KODE_VALUTA" value="<?= $val['KDVAL'] ?>" url="<?= site_url('referensi/autoComplate/VALUTA/KDEDI'); ?>" onfocus="Autocomp(this.id)" urai="KODE_VALUTA" class="ssstext" wajib="yes" />
                    </td>
                    <td>Biaya Tambahan</td>
                    <td>:
                        <input type="text" name="BIAYAUR" class="smtext" id="BIAYAUR" onkeyup="this.value = ThausandSeperator('BIAYAHarga', this.value, 4); prosesHarga('harga');" style="text-align:right;" value="<?= $this->fungsi->FormatRupiah($val['BTAMBAHAN'], 4); ?>"/>
                        <input type="hidden" name="BIAYAHarga" id="BIAYAHarga" value="<?= $val['BTAMBAHAN'] ?>" />
                    </td>
                </tr>
                <tr>
                    <td>NDPBM</td>
                    <td>:&nbsp;<input type="text" name="NDPBMUR" id="NDPBMUR" class="smtext" onkeyup="this.value = ThausandSeperator('NDPBMHarga', this.value, 4); prosesHarga('harga')" style="text-align:right;" value="<?= $this->fungsi->FormatRupiah($val['NDPBM'], 4); ?>"/>
                        <input type="hidden"  name="NDPBMHarga" id="NDPBMHarga" value="<?= $val['NDPBM'] ?>" />
                    </td>
                    <td>Discount</td>
                    <td>:
                        <input type="text" name="DISCOUNTUR" class="smtext" id="DISCOUNTUR" onkeyup="this.value = ThausandSeperator('DISCOUNTHarga', this.value, 4); prosesHarga('harga')" style="text-align:right;" value="<?= $this->fungsi->FormatRupiah($val['DISKON'], 4); ?>"/>
                        <input type="hidden"  name="DISCOUNTHarga" id="DISCOUNTHarga" value="<?= $val['DISKON'] ?>" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4">&nbsp;</td>
                </tr>
            </table>
            <h4><span class="info_2">&nbsp;</span>Nilai ini yang akan tercantum di PIB</h4>
            <table border="0">
                <tr>
                    <td width="100px;">Asuransi Bayar di</td>
                    <td width="220px;">:
                        <?= form_dropdown('HEADER[KDASS]', $KODE_ASURANSI, $val['KDASS'], 'id="KODE_ASURANSIHarga" class="sstext" style="width: 122px"'); ?>
                    </td>
                    <td class="fob" width="100px;">FOB</td>
                    <td width="220px;">:
                        <input type="text" name="FOBURHarga" id="FOBURHarga" readonly class="smtext" style="text-align:right;" value="<?= $val['FOB']; ?>"/>
                        <input type="hidden" name="FOBHarga" id="FOBHarga" value="<?= $val['FOB']; ?>"/>
                    </td>
                </tr>
                <tr>
                    <td>Nilai Asuransi</td>
                    <td>:
                        <input type="text" name="NILAI_ASURANSIUR" id="NILAI_ASURANSIUR" class="smtext" onkeyup="this.value = ThausandSeperator('NILAI_ASURANSI', this.value, 4);prosesHarga('harga')" style="text-align:right;" value="<?= $this->fungsi->FormatRupiah($val['ASURANSI'], 4); ?>"/>
                        <input type="hidden"  name="NILAI_ASURANSI" id="NILAI_ASURANSI" value="<?= $val['ASURANSI'] ?>" />
                    </td>
                    <td>CIF</td>
                    <td>:
                        <input type="text" name="CIFURHarga" id="CIFURHarga" readonly class="smtext" style="text-align:right;" value="<?= $val['CIF']; ?>"/>
                        <input type="hidden" name="CIFHarga" id="CIFHarga" value="<?= $val['CIF']; ?>"/>
                    </td>        
                </tr>
                <tr>
                    <td>Freight</td>
                    <td>:
                        <input type="text" name="NILAI_FREIGHTUR" id="NILAI_FREIGHTUR" class="smtext" onkeyup="this.value = ThausandSeperator('NILAI_FREIGHT', this.value, 4);prosesHarga('harga');" style="text-align:right;" value="<?= $this->fungsi->FormatRupiah($val['FREIGHT'], 4); ?>"/>
                        <input type="hidden"  name="NILAI_FREIGHT" id="NILAI_FREIGHT" value="<?= $val['FREIGHT'] ?>" />
                    </td>
                    <td>CIF Rp</td>
                    <td>:
                        <input type="text" name="CIFRPHarga" id="CIFRPHarga" readonly class="smtext" style="text-align:right;" value="<?= $val['CIFRP']; ?>"/></td>        
                </tr>   
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button onclick="getHarga('harga');" class="btn">Simpan</button>
                        <button onclick="cancel('harga');" class="btn">Batal</button>
                    </td>
                </tr>        
            </table>
        </form>
    </div>
</div>
<script>
    var kd_Harga = $("#harga #KODE_HARGAHarga").val();
    kode(kd_Harga);
    
function prosesHarga(form) {
    
    var KDHARGA = $("#" + form + " #KODE_HARGAHarga").val();
    if (KDHARGA != "") {
        var NDPBM       = parseFloat($("#"+form+" #NDPBMHarga").val()?$("#"+form+" #NDPBMHarga").val():0);
        var HARGA_CIF   = parseFloat($("#"+form+" #HARGA_CIF").val()?$("#"+form+" #HARGA_CIF").val():0);
        var BIAYA       = parseFloat($("#"+form+" #BIAYAHarga").val()?$("#"+form+" #BIAYAHarga").val():0);
        var DISCOUNT    = parseFloat($("#"+form+" #DISCOUNTHarga").val()?$("#"+form+" #DISCOUNTHarga").val():0);
        var NILAI_ASURANSI= parseFloat($("#"+form+" #NILAI_ASURANSI").val()?$("#"+form+" #NILAI_ASURANSI").val():0);
        var NILAI_FREIGHT = parseFloat($("#"+form+" #NILAI_FREIGHT").val()?$("#"+form+" #NILAI_FREIGHT").val():0);
        
        if (KDHARGA != 1){ //khusus harga CIF
            $("#"+form+" #FOBHarga").val(HARGA_CIF + BIAYA - DISCOUNT);
            $("#"+form+" #FOBURHarga").val(ThausandSeperator('', HARGA_CIF + BIAYA - DISCOUNT, 4));
        }else{
            $("#"+form+" #FOBHarga").val('0');
            $("#"+form+" #FOBURHarga").val('0');
        }
        $("#"+form+" #CIFHarga").val(NILAI_ASURANSI + HARGA_CIF + NILAI_FREIGHT + BIAYA - DISCOUNT);
        var CIF = parseFloat($("#"+form+" #CIFHarga").val()?$("#"+form+" #CIFHarga").val():0);
        $("#"+form+" #CIFURHarga").val(ThausandSeperator('', CIF, 4));
        $("#"+form+" #CIFRPHarga").val(ThausandSeperator('', NDPBM * CIF, 4));
        
    }
}
</script>
