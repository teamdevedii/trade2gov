<?php if (!defined('BASEPATH')){exit('No direct script access allowed');}?>

<div class="content_luar">
    <div class="content_dalam">  
        <form id="fuploadexcel_" method="post" autocomplete="off" onsubmit="return false;" action="<?= site_url('bc23/uploadexcel/upload/'.$car); ?>" enctype="multipart/form-data">            
            <input id="CAR" name="CAR" type="hidden" value="<?= $car ?>"/>
            <table width="100%">                
                <tr> 
                    <td width="50px">File Excel</td>
                    <td>
                        <input type="file" name="fileexcel" id="fileexcel" />
                        <button class="btn" onclick="Kirim();">Upload</button>
                    </td>
                </tr>                
                <tr>
                    <td colspan="2" >
                        <hr style="margin-bottom: 5px" >
                        <div id="message"></div>
                    </td>
                </tr>                
            </table>
        </form>   
    </div>
</div>
<script>
    function Kirim(){                         
        if($('#fileexcel').val()!==''){
            var file = document.getElementById('fileexcel');
            var car = document.getElementById('CAR');
            var formData = new FormData(); 
            formData.append("fileexcel", file.files[0]);            
            jloadings();
            $.ajax({
                url: $('#fuploadexcel_').attr('action'),
                data : formData,
                type:'POST',
                processData: false,
                contentType: false,
                success: function (response) {
                    var arrdata = response.rtn;
                    Clearjloadings();
                    $("#message").html(arrdata);
                }
            });
        }
    }            
</script>