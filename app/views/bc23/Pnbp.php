<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
?>

<div class="content_luar">
    <div class="content_dalam">
        <form name="fpnbp_" id="fpnbp_" action="<?= site_url('bc23/setHeaderPnbp'); ?>" method="post" autocomplete="off" onsubmit="return false;" > 
            <input type="hidden" name="POPUP[CAR]" id="CAR" value="<?= $CAR ?>" />
            <fieldset>
                <table>
                    <tr>
                    <td>Nilai PNBP (Rp)</td>
                    <td>
                        <input type="text" name=POPUP[NILBEBAN] id="NILBEBAN" class="numtext" value="<?= number_format($POPUP['NILBEBAN'], 2); ?>"/>
                    </td>                        
                    <td>
                        <?= form_dropdown('POPUP[KDFASIL]', $JENIS_KERINGANAN, $POPUP['KDFASIL'], 'id="PNBP" class="stext" '); ?>
                    </td>                                     
                </tr>
                </table>
            </fieldset>
            <button onclick="save_post_msg('fpnbp_');" class="btn onterus">
                <!-- onoff(false);-->
                <span class="icon-download"></span> Simpan
            </button>
            <button type="reset" class="btn onterus">
                <span class="icon-undo"></span> Batal
            </button>
        </form>
    </div>
</div>
