<?php if (!defined('BASEPATH')){exit('No direct script access allowed');}?>

<div class="content_luar">
    <div class="content_dalam">
        <form id="fuploadBUP_" method="post" autocomplete="off" onsubmit="return false;" action="<?= site_url('bc23/uploadbupbc23/upload'); ?>" enctype="multipart/form-data">
            <table width="100%">
                <tr>
                    <td colspan="2">* file back up dari modul tpb versi dekstop.</td>
                </tr>
                <tr>
                    <td width="50px">File BUP</td>
                    <td>
                        <input type="file" name="fileBUP" id="fileBUP" accept="File BackUp TPB/*.B23"/>
                        <button class="btn" onclick="sendFile();">Upload</button>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" >
                        <hr style="margin-bottom: 5px" >
                        <div id="message"></div>
                    </td>
                </tr>
                
            </table>
        </form>
    </div>
</div>
<script>
    function sendFile(){
        if($('#fileBUP').val()!==''){
            var file = document.getElementById('fileBUP');
            var formData = new FormData();
            formData.append("fileBUP", file.files[0]);
            jloadings();
            $.ajax({
                url: $('#fuploadBUP_').attr('action'),
                data : formData,
                type:'POST',
                processData: false,
                contentType: false,
                success: function (response) {
                    Clearjloadings();
                    $("#message").html(response);
                }
            });
        }
        /*
            xhrFields: {
                       onprogress: function (e) {
                           if (e.lengthComputable===true) {
                               $("#progress").show();
                               $('#bar').css('width', Math.round(e.loaded / e.total)  * 100 + '%');
                               $("#bar").html(Math.round(e.loaded / e.total)  * 100 + '%');
                           }
                       }
                   },
         */

            
        
        //alert(fileData);
        /*var options = {
        beforeSend: function(){
            $("#progress").show();
            $("#bar").width('0%');
            $("#message").html("");
            $("#bar").html("0%");},
        uploadProgress: function (event, position, total, percentComplete){
            $("#bar").width(percentComplete + '%');
            $("#bar").html(percentComplete + '%');},
        success: function (){
            $("#bar").width('100%');
            $("#percent").html('100%');},
        complete: function (response){
            $("#message").html("<font color='green'>" + response.responseText + "</font>");},
        error: function (){
            $("#message").html("<font color='red'> ERROR: unable to upload files</font>");
        }};
        $("#fuploadBUP_").ajaxForm(options);*/
    }
</script>