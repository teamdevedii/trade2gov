<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);
?>
<div>
        <form id="frmRegistrasi" name="frmRegistrasi" action="<?= $act ?>" onsubmit="return false;" class="form uniformForm" method="post" autocomplete=off>
            <?= $MAXLENGTH ?>
            <input type="hidden" id="KODE_TRADER" name="DATA[KODE_TRADER]" value="<?= $DATA['KODE_TRADER'] ?>">
            <table width="100%" border="0">
                <tr>
                    <td width="50%" valign="top">
                        <table width="100%" border="0">
                            <tr>
                                <td colspan="2">
                                    <h5><span class="ui-icon ui-icon-carat-1-e" style="display: inline-block ; margin-left:-2px">&nbsp;</span> DATA PERUSAHAAN</h5>
                                </td>
                            </tr>
                            <tr>
                                <td width="150px">Identitas *</td>
                                <td>: <?= form_dropdown('DATA[KODE_ID]', $JENIS_IDENTITAS, ($DATA['KODE_ID'] == '') ? '5' : $DATA['KODE_ID'], 'id="KODE_ID" wajib="yes" '); ?>&nbsp;<input type="text" name="DATA[ID]" id="ID" class="smtext" value="<?= $DATA['ID']; ?>" wajib="yes" onfocus="unformatNPWP(this.id);" onblur="formatNPWP(this.id);" wajib="yes" />
                                </td>
                            </tr>
                            <tr>
                                <td>Nama *</td>
                                <td>: <input type="text" name="DATA[NAMA]" id="NAMA" class="ltext" value="<?= $DATA['NAMA']; ?>" wajib="yes"/></td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">Alamat *</td>
                                <td><font style="vertical-align:top"> :</font> <textarea name="DATA[ALAMAT]" id="ALAMAT" class="ltext" wajib="yes"><?= $DATA['ALAMAT'] ?></textarea></td>
                            </tr>
                            <tr>
                                <td>Nomor Telepon *</td>
                                <td>: <input type="text" name="DATA[TELEPON]" id="TELEPON" class="smtext" value="<?= $DATA['TELEPON']; ?>" wajib="yes"/></td>
                            </tr>
                            <tr>
                                <td>Nomor Fax *</td>
                                <td>: <input type="text" name="DATA[FAX]" id="FAX" class="smtext" value="<?= $DATA['FAX']; ?>" wajib="yes"/></td>
                            </tr>
                            <tr>
                                <td>Tipe Trader</td>
                                <td>: <?= form_dropdown('DATA[TIPE_TRADER]', $TIPE_TRADER, $DATA['TIPE_TRADER'], 'id="TIPE_TRADER" wajib="yes" onchange="hidePPJK(this.value)"'); ?></td>
                            </tr>
                            <tr id="PPJK">
                                <td>No/Tgl PPJK</td>
                                <td>: <input type="text" name="DATA[NO_PPJK]" id="NO_PPJK" class="sstext" value="<?= $DATA['NO_PPJK']; ?>"/> &nbsp;<input type="text" name="DATA[TANGGAL_PPJK]" id="TANGGAL_PPJK" class="sstext" value="<?= $DATA['TANGGAL_PPJK']; ?>" formatDate="yes"/></td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top" >Dokumen Terdaftar</td>
                                <td><?php
                                        $dokumen = explode(",", $DATA['DOKUMEN']);
                                        foreach ($dokumen as $val) {
                                            $arr[$val] = 'checked';
                                        }
                                    ?>
                                    <div class="field-group control-group">	
                                        <div class="field">&nbsp;
                                            <input type="checkbox" name="DATA[DOKUMEN][BC20]" id="DOKUMEN[BC20]" value="BC20" <?= $arr['BC20'] ?>/>
                                            <label for="DOKUMEN[BC20]">BC 2.0 / PIB</label>
                                        </div>
                                        <div class="field">&nbsp;
                                            <input type="checkbox" name="DATA[DOKUMEN][BC30]" id="DOKUMEN[BC30]" value="BC30" <?= $arr['BC30'] ?>/>
                                            <label for="DOKUMEN[BC30]">BC 3.0 / PEB</label>
                                        </div>
                                        <div class="field">&nbsp;
                                            <input type="checkbox" name="DATA[DOKUMEN][BC23]" id="DOKUMEN[BC23]" value="BC23" <?= $arr['BC23'] ?>/>
                                            <label for="DOKUMEN[BC23]">BC 2.3 / TPB</label>
                                        </div>
                                        <div class="field">&nbsp;
                                            <input type="checkbox" name="DATA[DOKUMEN][BC10]" id="DOKUMEN[BC10]" value="BC10" <?= $arr['BC10'] ?>/>
                                            <label for="DOKUMEN[BC10]">RKSAP/JKSP dan Manifest </label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                        <table width="100%" border="0">
                            <tr>
                                <td colspan="2"><h5><span class="ui-icon ui-icon-carat-1-e" style="display: inline-block ; margin-left:-2px">&nbsp;</span>DATA PEMILIK PERUSAHAAN</h5></td>
                            </tr>
                            <tr>
                                <td width="150px">Nama *</td>
                                <td>: <input type="text" name="DATA[NAMA_PEMILIK]" id="NAMA_PEMILIK" class="mtext" value="<?= $DATA['NAMA_PEMILIK']; ?>" wajib="yes"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Tempat/Tanggal Lahir *</td>
                                <td>: <input type="text" name="DATA[TEMPAT_LAHIR_PEMILIK]" id="TEMPAT_LAHIR_PEMILIK" class="mtext" value="<?= $DATA['TEMPAT_LAHIR_PEMILIK']; ?>" wajib="yes"/>&nbsp;<input type="text" name="DATA[TANGGAL_LAHIR_PEMILIK]" id="TANGGAL_LAHIR_PEMILIK" class="sstext" formatDate="yes" value="<?= $DATA['TANGGAL_LAHIR_PEMILIK']; ?>" wajib="yes"/></td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">Alamat *</td>
                                <td><font style="vertical-align:top"> :</font> <textarea name="DATA[ALAMAT_PEMILIK]" id="ALAMAT_PEMILIK" class="ltext" wajib="yes"><?= $DATA['ALAMAT_PEMILIK']; ?></textarea></td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">e-Mail *</td>
                                <td>: <input type="text" name="DATA[EMAIL_PEMILIK]" id="EMAIL_PEMILIK" class="ltext" value="<?= $DATA['EMAIL_PEMILIK']; ?>" wajib="yes"/></td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>                       
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><div class="msg_"></div></td>
                </tr>
                <tr>
                    <td colspan="2" align="center">                        
                        <button class="btn btn-small" onclick="save_post_msg('frmRegistrasi','','_content_');"> &nbsp; Proses &nbsp; </button>
                        <button class="btn btn-small" onclick="document.getElementById('frmRegistrasi').reset();"> &nbsp; Reset  &nbsp; </button>
                    </td>
                </tr>
            </table>
        </form>    
</div> 
<script>
    $(document).ready(function() {
        
        declareDP('frmRegistrasi');
        hidePPJK($('#TIPE_TRADER').val());
        f_MAXLENGTH('frmRegistrasi');         
    });
    function getNoreg(kpbc) {
        call(site_url + '/usermanage/getLastnoreg/' + kpbc, 'NOREG');
    }
    /*
     $('input[name=logo]').change(function() {
     if ($('#ID').val() == '') {
     jAlert('Mohon lengkapi data terlebih dahulu', site_name);
     } else {
     uploadlogo('logo', 'logo', $('#ID').val());
     }
     });
     */
    function hidePPJK(kd) {
        if (kd == '2') {
            $('#PPJK').show();
            $('#NO_PPJK').focus();
        } else {
            $('#NO_PPJK').val('');
            $('#TANGGAL_PPJK').val('');
            $('#PPJK').hide();
        }
    }
</script>