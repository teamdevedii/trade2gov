<!-- Menu Admini User -->
<?php 
switch ($tipe_user){
    case'1': //Super Admin  ?>
    <ul id="mainNav">
        <li id="navDashboard" class="nav">
            <span class="icon-home"></span>
            <a href="#">Home</a>
        </li>
        <li id="navPages" class="nav">
            <span class="icon-document-alt-stroke"></span> 
            <a href="#">Management User</a>
            <ul class="subNav">
                <li><a href="#" onclick="call('<?= site_url('usermanage/callOut/registrasi'); ?>', '_content_', 'Registrasi')">Buat Registrasi</a></li>
                <li><a href="#" onclick="call('<?= site_url('usermanage/regOnline'); ?>', '_content_','Daftar Perusahaan yang melakukan registrasi online.')">Registrasi</a></li>
                <li><a href="#" onclick="call('<?= site_url('usermanage/lstPerusahaan/perusahaan'); ?>', '_content_', 'Daftar Perusahaan')">Daftar Perusahaan</a></li>                                
            </ul>
        </li>
        <li id="navPages" class="nav">
            <span class="icon-document-alt-stroke"></span> 
            <a href="#">Request</a>
            <ul class="subNav">
                <li><a href="#" onclick="call('<?= site_url('usermanage/reqRegistrasi/request'); ?>', '_content_', 'Request Registrasi')">Request Registrasi</a></li>
            </ul>
        </li>
        <li id="navPages" class="nav">
            <span class="icon-document-alt-stroke"></span>
            <a href="#">Data Pendukung</a> 
            <ul class="subNav">
                <li><a href="#" onclick="call('<?= site_url('referensi/dataPendukung/bank'); ?>','_content_', 'Daftar Bank')">Bank</a></li>
                <li><a href="#" onclick="call('<?= site_url('referensi/dataPendukung/daerah'); ?>','_content_', 'Daftar Daerah')">Daerah</a></li>
                <li><a href="#" onclick="call('<?= site_url('referensi/dataPendukung/gudang'); ?>', '_content_', 'Daftar Gudang')">Gudang</a></li>
                <li><a href="#" onclick="call('<?= site_url('referensi/dataPendukung/hanggar'); ?>','_content_', 'Daftar Hanggar')">Hanggar</a></li>
                <li><a href="#" onclick="call('<?= site_url('referensi/dataPendukung/hs'); ?>','_content_', 'Daftar HS')">Hs</a></li> 
                <li><a href="#" onclick="call('<?= site_url('referensi/dataPendukung/izin'); ?>',   '_content_', 'Daftar Izin')">Izin</a></li>
                <li><a href="#" onclick="call('<?= site_url('referensi/dataPendukung/kemasan'); ?>','_content_', 'Daftar Kemasan')">Kemasan</a></li>
                <li><a href="#" onclick="call('<?= site_url('referensi/dataPendukung/kpbc'); ?>',   '_content_', 'Daftar KPBC')">Kpbc</a></li>
                <li><a href="#" onclick="call('<?= site_url('referensi/dataPendukung/kpker'); ?>',  '_content_', 'Daftar KPKER')">Kpker</a></li>
                <li><a href="#" onclick="call('<?= site_url('referensi/dataPendukung/manifest_grup'); ?>','_content_', 'Daftar Manifest Grup')">Manifest Grup</a></li>
                <li><a href="#" onclick="call('<?= site_url('referensi/dataPendukung/negara'); ?>', '_content_', 'Daftar Negara')">Negara</a></li>
                <li><a href="#" onclick="call('<?= site_url('referensi/dataPendukung/pelabuhan'); ?>', '_content_','Daftar Pelabuhan')">Pelabuhan</a></li>
                <li><a href="#" onclick="call('<?= site_url('referensi/dataPendukung/satuan'); ?>', '_content_', 'Daftar Satuan')">Satuan</a></li>
                <li><a href="#" onclick="call('<?= site_url('referensi/dataPendukung/setting'); ?>','_content_', 'Daftar Setting')">Setting</a></li>
                <li><a href="#" onclick="call('<?= site_url('referensi/dataPendukung/tambat'); ?>','_content_', 'Daftar Tambat')">Tambat</a></li>
                <li><a href="#" onclick="call('<?= site_url('referensi/dataPendukung/tod2'); ?>','_content_', 'Daftar Tod')">Tod</a></li>
            </ul>
        </li>
        <li id="navPages" class="nav">
            <span class="icon-document-alt-user"></span>
            <a href="#">User</a>
            <ul class="subNav">
                <li><a href="#" onclick="call('<?= site_url('usermanage/viwProfile')?>', '_content_')">Profile</a></li>
                <li><a href="#" onclick=" window.open('<?= site_url('login/logout');?>','_self') ">Log Out</a></li>
            </ul>
        </li>
    </ul>
    <?php  break;
    case'2': //Admin BC  ?>
    <ul id="mainNav">
        <li id="navDashboard" class="nav">
            <span class="icon-home"></span>
            <a href="#">Home</a>
        </li>
        <li id="navPages" class="nav">
            <span class="icon-document-alt-stroke"></span>
            <a href="#">Management User</a>
            <ul class="subNav">
                <li><a href="#" onclick="call('<?= site_url('usermanage/lstPerusahaan/perusahaan'); ?>', '_content_','Daftar Perusahaan')">Daftar Perusahaan</a></li>
                <li><a href="#" onclick="call('<?= site_url('usermanage/regOnlineBC'); ?>', '_content_','Daftar Registrasi Online')">Registrasi Online</a></li>
            </ul>
        </li>
        <li id="navPages" class="nav">
            <span class="icon-document-alt-user"></span>
            <a href="#">User</a>
            <ul class="subNav">
                <li><a href="#" onclick="call('<?= site_url('usermanage/viwProfile')?>', '_content_')">Profile</a></li>
                <li><a href="" onclick=" window.open('<?= site_url('login/logout');?>','_self')">Log Out</a></li>
            </ul>
        </li>
    </ul>
    <?php  break;
    case'3': //Suport  ?>
    <ul id="mainNav">
        <li id="navDashboard" class="nav">
            <span class="icon-home"></span>
            <a href="#">Home</a>
        </li>
        <li id="navPages" class="nav">
            <span class="icon-document-alt-stroke"></span>
            <a href="#">Management User</a>
            <ul class="subNav">
                <li><a href="#" onclick="call('<?= site_url('usermanage/lstPerusahaan/perusahaan'); ?>', '_content_','Daftar Perusahaan')">Daftar Perusahaan</a></li>
                <li><a href="#" onclick="call('<?= site_url('usermanage/regOnline'); ?>', '_content_','Daftar Registrasi Online')">Registrasi Online</a></li>
            </ul>
        </li>
        <li id="navPages" class="nav">
            <span class="icon-document-alt-user"></span>
            <a href="#">User</a>
            <ul class="subNav">
                <li><a href="#">Profile</a></li>
                <li><a href="" onclick=" window.open('<?= site_url('login/logout');?>','_self') ">Log Out</a></li>
            </ul>
        </li>
    </ul>
    <?php  break;
    case'4': //Admin User 
        $arrDok = explode(',', $dokumen);
        $menuDok['BC20'] = "<li id=\"navPages\" class=\"nav\"><span class=\"icon-document-alt-stroke\"></span><a href=\"#\">Dokumen PIB</a>".
                "<ul class=\"subNav\">".
                 "<li><a href=\"#\" onclick=\"call('".site_url('bc20/daftar/draft')."', '_content_', 'Data Pemberitahuan Impor Barang')\">Daftar PIB</a></li>".
                "</ul></li>";
        $menuDok['BC30'] = "<li id=\"navPages\" class=\"nav\"><span class=\"icon-document-alt-stroke\"></span><a href=\"#\">Dokumen PEB</a>".
                "<ul class=\"subNav\">".
                "<li><a href=\"#\" onclick=\"call('".site_url('bc30/daftar')."', '_content_', 'Data Pemberitahuan Ekspor Barang')\">Daftar PEB</a></li>".
                "<li><a href=\"#\" onclick=\"call('".site_url('bc30/daftarPKBE')."', '_content_', 'Data Pemberitahuan Ekspor Barang')\">Daftar PKBE</a></li>".
                "</ul></li>"; 
        $menuDok['BC23'] = "<li id=\"navPages\" class=\"nav\"><span class=\"icon-document-alt-stroke\"></span><a href=\"#\">Dokumen TPB</a>".
                "<ul class=\"subNav\">".                    
                    "<li><a href=\"#\" onclick=\"call('".site_url('bc23/daftarTPB')."', '_content_', 'Data Tempat Penimbunan Berikat ')\">Daftar TPB</a></li>".
                "</ul></li>";
        $menuDok['BC10'] = "<li id=\"navDashboard\" class=\"nav\"><span class=\"icon-chat\"></span><a href=\"#\">Pengangkut</a>".
                "<ul class=\"subNav\">".
                    "<li><a href=\"#\" onclick=\"call('".site_url('bc10/lstBC10')."', '_content_', 'Data RKSP / JKSP')\">RKSP / JKSP</a></li>".
                    "<li><a href=\"#\" onclick=\"call('".site_url('bc11/lstBC11')."', '_content_', 'Data Manifest')\">Manifest</a></li>".
                "</ul></li>";
        ?>
    <ul id="mainNav">
        <li id="navDashboard" class="nav">
                <span class="icon-home"></span>
            <a href="#">Home</a>
        </li>
        <li id="navDashboard" class="nav">
            <span class="icon-chat"></span>
            <a href="#">Komunikasi</a>
            <ul class="subNav">
                 <li><a href="#" onclick="call('<?= site_url('komunikasi/dataSent'); ?>', '_content_', 'Data dokumen yang dikirim')">Dok. Terkirim</a></li>
                <li><a href="#" onclick="call('<?= site_url('komunikasi/dataReceive'); ?>', '_content_', 'Data dokumen yang diterima')">Res. Diterima</a></li>
            </ul>
        </li>
        <?php foreach($arrDok as $dok){
            echo $menuDok[$dok];        
        }?>
        <li id="navTables" class="nav">
            <span class="icon-list"></span>
            <a href="#">Data Pendukung</a>
            <ul class="subNav">
                <li><a href="#" onclick="call('<?= site_url('referensi/dataPendukung/tod'); ?>', '_content_', 'Daftar Tod')">Biaya TOD</a></li>
                <li><a href="#" onclick="call('<?= site_url('referensi/dataPendukung/importir'); ?>', '_content_', 'Daftar Importir')">Importir</a></li>
                <li><a href="#" onclick="call('<?= site_url('referensi/dataPendukung/indentor'); ?>', '_content_','Daftar Indentor')">Indentor/QQ</a></li>
                <li><a href="#" onclick="call('<?= site_url('referensi_1/dataPendukung/indentoreks'); ?>', '_content_', 'Daftar Indentor Eksportir')">Indentor/QQ Eksportir</a></li>
                <li><a href="#" onclick="call('<?= site_url('referensi_1/dataPendukung/parties'); ?>', '_content_','Daftar Parties')">Parties</a></li>
                <li><a href="#" onclick="call('<?= site_url('referensi_1/dataPendukung/pembeli'); ?>', '_content_','Daftar Pembeli')">Pembeli</a></li>
                <li><a href="#" onclick="call('<?= site_url('referensi/dataPendukung/pemasok'); ?>', '_content_','Daftar Pemasok')">Pemasok</a></li>
                <li><a href="#" onclick="call('<?= site_url('referensi/dataPendukung/tarif'); ?>', '_content_','Daftar Tarif')">Pos Tarif/HS</a></li>
                <li><a href="#" onclick="call('<?= site_url('referensi/dataPendukung/barang'); ?>', '_content_','Daftar Barang')">Barang</a></li>
                <li><a href="#" onclick="call('<?= site_url('referensi/dataPendukung/moda'); ?>', '_content_','Daftar Moda')">Moda</a></li>
                <li><a href="#" onclick="call('<?= site_url('referensi/dataPendukung/partner'); ?>', '_content_','Daftar Partner')">Partner</a></li>
            </ul>
        </li>
    </ul>
    <?php  break;
    case'5': //User Operator  ?> 
    <ul id="mainNav">
        <li id="navDashboard" class="nav">
            <span class="icon-home"></span>
            <a href="#">Home</a>
        </li>
        <li id="navPages" class="nav">
            <span class="icon-document-alt-stroke"></span>
            <a href="#">Management User</a>
            <ul class="subNav">
                <li><a href="#" onclick="call('<?= site_url('usermanage/lstPerusahaan/perusahaan'); ?>', '_content_','Daftar Perusahaan')">Daftar Perusahaan</a></li>
                <!--<li><a href="#" onclick="call('<?= site_url('usermanage/regOnline'); ?>', '_content_')">Registrasi Online</a></li>-->
            </ul>
        </li>
        <li id="navPages" class="nav">
            <span class="icon-document-alt-user"></span>
            <a href="#">User</a>
            <ul class="subNav">
                <li><a href="#">Profile</a></li>
                <li><a href="" onclick=" window.open('<?= site_url('login/logout');?>','_self') ">Log Out</a></li>
            </ul>
        </li>
    </ul>
    <?php  break;
}
?>