<?php if (!defined('BASEPATH'))
exit('No direct script access allowed'); 

?>
<form id="fkontainer_" name="fkontainer_" action="<?= site_url('bc11/kontainer/'.$KONTAINER['CAR']); ?>" onsubmit="return false;" method="post" autocomplete="off" >
    <input type="hidden" name="act" value="<?= $act; ?>" />
    <input type="hidden" name="KONTAINER[CAR]" id="CAR" value="<?= $KONTAINER['CAR']; ?>" />
    <input type="hidden" name="KONTAINER[NOPOS]" id="NOPOS" value="<?= $KONTAINER['NOPOS']; ?>" />
    <input type="hidden" name="KONTAINER[NOPOSSUB]" id="NOPOSSUB" value="<?= $KONTAINER['NOPOSSUB']; ?>" />
    <input type="hidden" name="KONTAINER[NOPOSSUBSUB]" id="NOPOSSUBSUB" value="<?= $KONTAINER['NOPOSSUBSUB']; ?>" />
    <input type="hidden" name="KONTAINER[KDGRUP]" id="KDGRUP" value="<?= $KONTAINER['KDGRUP']; ?>" />   
    <input type="hidden" name="KONTAINER[SERI]" id="SERI" value="<?= $KONTAINER['SERI']; ?>" /> 
    
    <table cellspacing="5">
        <tr>
            <td>Nomor :</td>
            <td>Ukuran :</td>
            <td>Tipe :</td>

        </tr>
        <tr>
            <td><input type="text" name="KONTAINER[NOCONT]" id="NOCONT" class="mtext" value="<?= $KONTAINER['NOCONT']; ?>" maxlength="17" style="" wajib="yes"/></td>
            <td><?= form_dropdown('KONTAINER[UKCONT]', $UKURAN_CONTAINER, $KONTAINER['UKCONT'], 'id="UKURAN" class="sstext" wajib="yes" '); ?></td>
            <td><?= form_dropdown('KONTAINER[TIPECONT]', $JENIS_CONTAINER, $KONTAINER['TIPECONT'], 'id="TIPE" class="sstext" wajib="yes" '); ?></td>
        </tr>
        <tr>
            <td colspan="3">
                <button onclick="save_post_msg('fkontainer_', '', 'fkontainer_list');$('#fkontainer_form').html('');" class="btn">Simpan</button>
                <button onclick="cancel('fkontainer_');$('#fkontainer_form').html('');" class="btn">Batal</button>
            </td>
        </tr>	        
    </table>
</form>
<script>
    $(function () {
        $('#fkontainer_form').show();
    });
</script>