<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<form id="fdokumen_" nama='fdokumen_' action="<?= site_url('bc11/dokumen/'.$DOKUMEN['CAR']) ?>" onsubmit="return false;" method="post" autocomplete="off">
    <input type='hidden' name="act" value="<?= $act; ?>" />
    <input type='hidden' name="DOKUMEN[CAR]" id="CAR" value="<?= $DOKUMEN['CAR']; ?>" />
    <input type="hidden" name="DOKUMEN[NOPOS]" id="NOPOS" value="<?= $DOKUMEN['NOPOS']; ?>" />
    <input type="hidden" name="DOKUMEN[NOPOSSUB]" id="NOPOSSUB" value="<?= $DOKUMEN['NOPOSSUB']; ?>" />
    <input type="hidden" name="DOKUMEN[NOPOSSUBSUB]" id="NOPOSSUBSUB" value="<?= $DOKUMEN['NOPOSSUBSUB']; ?>" />
    <input type="hidden" name="DOKUMEN[KDGRUP]" id="KDGRUP" value="<?= $DOKUMEN['KDGRUP']; ?>" />   
    <input type="hidden" name="DOKUMEN[SERI]" id="SERI" value="<?= $DOKUMEN['SERI']; ?>" />
    <table>
        <tr><td>Kode Dokumen :</td><td>Kode Kantor :</td></tr>
        <tr>
            <td><?= form_dropdown('DOKUMEN[DOKKODE]', $DOKKODE, $DOKUMEN['DOKKODE'], 'id="DOKKODE"  class="text"'); ?>
            </td>
            <td>
                <input type="text" name="DOKUMEN[DOKKPBC]" id="DOKNO" class="sstext" value="<?= $DOKUMEN['DOKKPBC']; ?>" maxlength="10" wajib="yes"/>
            </td>
        </tr>
        <tr><td>Nomor :</td><td>Tanggal :</td></tr>
        <tr><td><input type="text" name="DOKUMEN[DOKNO]" id="DOKNO" class="ltext" value="<?= $DOKUMEN['DOKNO']; ?>" maxlength="30" wajib="yes"/></td>
            <td>
                <input type="text" name="DOKUMEN[DOKTG]" id="DOKTG" class="sstext" value="<?= $DOKUMEN['DOKTG']; ?>" maxlength="10" formatDate="yes" wajib="yes"/>
            </td>
        </tr>
        <tr>
            <td colspan="2"><br>
                <button onclick="save_post_msg('fdokumen_', '', 'fdokumen_list');$('#fdokumen_form').html('');" class="btn">Simpan</button>
                <button onclick="cancel('fdokumen_');$('#fdokumen_form').html('');" class="btn">Batal</button>
            </td>
        </tr>
    </table>
</form>
<script>
    $(function () {
        declareDP('fdokumen_');
        $('#fdokumen_form').show();
    })
</script>