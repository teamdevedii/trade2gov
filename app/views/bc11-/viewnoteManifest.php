<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
?>

<span id="DivHeaderForm">
    <span id="divtmp"></span>
    <form id="frmnoteManifest" name="frmnoteManifest" action="<?= site_url('bc30/setHeaderPKBE'); ?>" method="post" autocomplete="off" onsubmit="return false;" class="form uniformForm">

        <div class="content_luar">
            <div class="content_dalam" >
                <center>

                    <table border="0" width="620px" cellpadding="0" cellspacing="0" class="table-bordered">            
                        <tr>
                            <td><textarea name="NOTE" id="NOTE" rows="15" cols="82"><?= $HEADER['NOTE']; ?></textarea></td>                
                        </tr>  
                        <tr>
                            <td>
                                <span id="off">
                                    <button id="" onclick="onoff(true);
                                            return false;" class="btn onterus"><span class="icon-pen"></span> Edit </button>
                                </span>
                                <span id="on">
                                    <button onclick="save_header('#frmnoteManifest');" class="btn onterus">
                                        <!-- onoff(false);-->
                                        <span class="icon-download"></span> Simpan
                                    </button>
                                    <button onclick="onoff(false);" class="btn onterus">
                                        <span class="icon-undo"></span> Batal
                                    </button>
                                </span>
                            </td>
                        </tr>
                    </table>
                </center>
            </div>
        </div>
    </form>
</span>

<script>
    $(document).ready(function() {
        onoff(false);
    });
    function onoff(on) {

        if ($('#frmnoteManifest #CAR').val() == '' || $('#frmnoteManifest #act').val() == 'save') {
            $('#frmnoteManifest .outentry').hide();
        } else {
            $('#frmnoteManifest .outentry').show();
        }
        if (on) {
            $('#on').show();
            $('#off').hide();
            $("#frmnoteManifest :input").removeAttr("disabled");
            $('#frmnoteManifest .outentry :input').attr('disabled', 'disabled');
            $("#tabs").tabs({disabled: [1, 2]});
        } else {
            $('#on').hide();
            $('#off').show();
            $("#frmnoteManifest :input").attr('disabled', 'disabled');
            $('#frmnoteManifest .outentry :input').removeAttr("disabled");
            $("#tabs").tabs();
            if ($("#act").val() == 'save') {
                $("#tabs").tabs({disabled: [1]});
            } else {
                $("#tabs").tabs({disabled: false});
            }
        }
        $('.onterus').removeAttr('disabled');
        $('.offterus :input').attr('readonly', 'readonly');
    }
</script>