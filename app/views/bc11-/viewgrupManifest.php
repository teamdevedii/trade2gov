<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
//print_r($CAR);die();
//print_r($DETIL);die();
?>
<script>
    $(document).ready(function() {
        onoffDtlManif(false);
        declareDP('f_viewgrupManifest');
    });
    
    function onoffDtlManif(on) {
        //cmdPKB
        if ($('#f_viewgrupManifest #CAR').val() == '') {
            $('#f_viewgrupManifest.outentry').hide();
        } else {
            $('#f_viewgrupManifest.outentry').show();
        }
        if (on) {
            $('#f_viewgrupManifest #on').show();
            $('#f_viewgrupManifest #off').hide();
            $("#f_viewgrupManifest :input").removeAttr("disabled");
            $('#f_viewgrupManifest .outentry :input').attr('disabled', 'disabled');
            $("#tabs").tabs({disabled: [1, 2]});
        } else {
            $('#f_viewgrupManifest #on').hide();
            $('#f_viewgrupManifest #off').show();
            $("#f_viewgrupManifest :input").attr('disabled', 'disabled');
            $('#f_viewgrupManifest .outentry :input').removeAttr("disabled");
        }
        $('#f_viewgrupManifest .onterus').removeAttr('disabled');
        $('#f_viewgrupManifest .offterus :input').attr('readonly', 'readonly');
    }
    
    function cekValidasiManifDTL(car, kdgrup, nopos, nopossub, nopossubsub) {
        //alert(car+'|'+kdgrup+'|'+nopos+'|'+nopossub+'|'+nopossubsub);return false;
       Dialog(site_url + '/bc11/alertcekstatusManifDTL/' +car+ '/' +kdgrup+ '/' +nopos+ '/' +nopossub+ '/' +nopossubsub+ '/f_viewgrupManifest','divValidasiDetil','Validasi Detil',600,400);
    }

    function comboajaib(kdgrup) {
        //alert(car+'|'+kdgrup); //return false;
        var car = $('#f_viewgrupManifest #car').val(); 
        call(site_url + '/bc11/pageNavManif/' + car + '/' + kdgrup + '/1' ,'navManif', '');
    }

    function navPageManif(tipe, page, jmlpage) {
       // alert('asd asd');return false;
       //alert(tipe+'|'+page+'|'+jmlpage); return false;
        var goPage = 1;
        var car = $('#f_viewgrupManifest #car').val(); 
        var kdgrup = $('#f_viewgrupManifest #KDGRUP').val();   
        //var nopos = $('#f_viewgrupManifest #NOPOS').val();
        jmlpage = parseInt(jmlpage);
        page = parseInt(page);
        switch (tipe) {
            case 'first':
                goPage = 1;
                break;
            case 'preview':
                if (page > 1) {
                    goPage = page - 1;
                } else if (page <= 1) {
                    goPage = 1;
                }
                break;
            case 'next':
                if (page < jmlpage) {
                    goPage = page + 1;
                } else if (page >= jmlpage) {
                    goPage = jmlpage;
                }
                break;
            case 'last':
                goPage = jmlpage;
                break;
        }
        //alert(car+'|'+kdgrup+'|'+goPage);return false;
        call(site_url + '/bc11/pageNavManif/' + car + '/' + kdgrup + '/' + goPage , 'navManif', '');
        

    }
</script>
<div id="navManif">
    <div class="content_luar">
        <div class="content_dalam">
            <form name="f_viewgrupManifest" id="f_viewgrupManifest" action="<?= site_url('bc11/createPOPDetailMANIFEST/proses'); ?>" method="post" autocomplete="off" onsubmit="return false;" >
                <?= $MAXLENGTH ?>
                <input type="hidden" name="act" id="act" value="<?= $act; ?>" readonly="readonly" />
                <input type="hidden" name="DETIL[CAR]" id="car" value="<?= $DETIL['CAR']; ?>" readonly="readonly" />    
                <input type="hidden" name="DETIL[KDGRUP]" id="KDGRUP" value="<?= $DETIL['KDGRUP']; ?>" readonly="readonly" /> 
                

                <span class="info_2"></span><?= $judul; ?>
                <table style="width: 90%">
                    <tr>
                        <td colspan="2">
                            <span id="off">
                            <button onclick="onoffDtlManif(true);
                                    return false;" class="btn onterus"><span class="icon-pen"></span> Edit </button>                            
                            </span>
                            <span id="on">
                             <button onclick="save_post_msg('f_viewgrupManifest','','detilRKSP');
                                    onoffDtlManif(false);" class="btn onterus"><span class="icon-download"></span> Simpan</button>
                            <button onclick="onoffDtlManif(false);
                                    cancel('f_viewgrupManifest');" class="btn onterus"><span class="icon-undo"></span> Batal</button>             
                            </span><span class="msgheader_" style="margin-left:20px">&nbsp;</span>
                        </td>
                        <td style="text-align: right" class="outentry">
                            <button onclick="cekValidasiManifDTL($('#f_viewgrupManifest #car').val(),$('#f_viewgrupManifest #KDGRUP').val(),$('#f_viewgrupManifest #NOPOS').val(),$('#f_viewgrupManifest #NOPOSSUB').val(),$('#f_viewgrupManifest #NOPOSSUBSUB').val())" class="btn" id="DTLOK"><span class="<?= ($DETIL['DTLOK']!='1')?'icon-x':'icon-check'; ?>"></span> <b style="color: #ffffff"><?= $DETIL['UDTLOK'] ?></b></button>
                        </td>
                        <td style="float: right" class="outentry">  
                            <button onclick="navPageManif('first', $('#f_viewgrupManifest #NOPOS').val(), $('#f_viewgrupManifest #JMNOPOS').val());
                                return false;" class="btn" style="height: 22px; width:25px; vertical-align: middle;" > <span class="icon-first"></span></button>

                            <button onclick="navPageManif('preview', $('#f_viewgrupManifest #NOPOS').val(), $('#f_viewgrupManifest #JMNOPOS').val());
                                return false;" class="btn" style="-webkit-transform: rotate(180deg);-moz-transform: rotate(180deg);-o-transform: rotate(180deg);-ms-transform: rotate(180deg);transform: rotate(180deg); height: 22px; width:25px; vertical-align: middle;" > <span class="icon-play"></span></button>

                            <input type="text" id="NOBARANG" class="ssstext" value="<?= $DETIL['NOPOS']; ?>" style="text-align: center;" readonly="readonly"/>

                            <button onclick="navPageManif('next', $('#f_viewgrupManifest #NOPOS').val(), $('#f_viewgrupManifest #JMNOPOS').val());
                                return false;" class="btn" style="height: 22px; width:25px; vertical-align: middle;"> <span class="icon-play"></span></button> 

                            <button onclick="navPageManif('last', $('#f_viewgrupManifest #NOPOS').val(), $('#f_viewgrupManifest #JMNOPOS').val());
                                return false;" class="btn" style="height: 22px; width:25px; vertical-align: middle;"><span class="icon-last"></span></button>
                        </td>
                    </tr>
                </table>
                <div class="content_luar">
                    <div class="content_dalam">
                        <table style=" width:100%">
                            <tr>
                                <td>Group Pos : </td>
                                <td class="outentry"><?= form_dropdown('DETIL[KDGRUP]', $KDGRUP, strtoupper($DETIL['KDGRUP']), 'id="KDGRUP" style="width:760px" onchange="comboajaib($(this).val())"'); ?></td>
                            </tr>
                        </table>
                        <table style=" width:100%"> 
                            <tr><td>&nbsp;</td></tr>
                            <tr>
                                <td style=" width: 50%">                                                           
                                    <table> 
                                        <tr>
                                            <td width="100px">Nomor Aju </td>
                                            <td><b><?= substr($DETIL['CAR'], 0, 6) . '-' . substr($DETIL['CAR'], 7, 6) . '-' . substr($DETIL['CAR'], 12, 8) . '-' . substr($DETIL['CAR'], 20, 6); ?></b></td>
                                        </tr>
                                        <tr>
                                            <td>No. POS</td>
                                            <td>
                                                <input type="text" name="DETIL[NOPOS]" id="NOPOS" value="<?= str_pad($DETIL['NOPOS'], 4, '0', STR_PAD_LEFT); ?>" size="8" readonly/>                                    
                                                <input type="hidden" name="JMNOPOS" id="JMNOPOS" class="sssstext" value="<?= $JMNOPOS ?>"readonly="readonly"/>
                                                <input type="text" name="DETIL[NOPOSSUB]" id="NOPOSSUB" value="<?= str_pad($DETIL['NOPOSSUB'], 4, '0', STR_PAD_LEFT); ?>" size="8" readonly/>
                                                <input type="text" name="DETIL[NOPOSSUBSUB]" id="NOPOSSUBSUB" value="<?= str_pad($DETIL['NOPOSSUBSUB'], 4, '0', STR_PAD_LEFT); ?>" size="8" readonly/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>No. / Tgl BL</td>
                                            <td>
                                                <input  type="text" name="DETIL[NOBL]" id="NOBL" value="<?= $DETIL['NOBL']; ?>" size="20" /> 
                                                <input  type="text" name="DETIL[TGBL]" id="TGBL" size="8" value="<?= $DETIL['TGBL']; ?>" formatDate="yes"/>
                                            </td>                               
                                        </tr>
                                        <tr>
                                            <td>Mother Vessel</td>
                                            <td><input type="text" size="33" id="MOTHERVESSEL" name="DETIL[MOTHERVESSEL]" value="<?= $DETIL['MOTHERVESSEL']; ?>"/></td>
                                        </tr>
                                    </table>                
                                </td>
                                <td style=" width: 50%">  
                                    <fieldset>
                                        <legend>Informasi Master BL</legend>
                                        <table>                                   
                                            <tr>
                                                <td >No. / Tgl Master</td>
                                                <td><input type="text" name="DETIL[MNOBL]" id="MNOBL" value="<?= $DETIL['MNOBL']; ?>" size="8"/>                                        
                                                    <input type="text" name="DETIL[MTGBL]" id="MTGBL" size="8" value="<?= $DETIL['MTGBL']; ?>" formatDate="yes"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Total Kemasan</td>
                                                <td><input type="text" name="DETIL[MJMLKEMAS]" id="MJMLKEMAS" value="<?= $DETIL['MJMLKEMAS']; ?>" size="5"/> </td>
                                                <td>Total Kontainer</td>
                                                <td><input type="text" name="DETIL[MJMLCONT]" id="MJMLCONT" value="<?= $DETIL['MJMLCONT']; ?>" size="5"/> </td>
                                            </tr>                                
                                        </table>
                                    </fieldset>
                                </td>
                            </tr>
                        </table>        
                        <table style=" width:100%">
                            <tr>
                                <td style=" width: 100%">
                                    <fieldset>
                                        <legend>Pelabuhan</legend>
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 50%">
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="100px">Asal</td>
                                                            <td>
                                                                <input  type="text" name="DETIL[DPELASAL]" id="DPELASAL" value="<?= $DETIL['DPELASAL']; ?>" url="<?= site_url('referensi/autoComplate/PELABUHAN/KODE_PELABUHAN'); ?>" onfocus="Autocomp(this.id)" urai="DPELASAL;DPELASALUR" class="ssstext"  maxlength="5"/>&nbsp;
                                                                <button onclick="tb_search('pelabuhan', 'DPELASAL;DPELASALUR', 'Kode Pelabuhan', 'Muat', 650, 400)" class="btn"> ... </button>
                                                                <span id="DPELASALUR"><?= $DETIL['DPELASALUR'] == '' ? $DPELASALUR : $DETIL['DPELASALUR']; ?></span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100px;vertical-align: top;">Sebelumnya / Transit Akhir</td>
                                                            <td>
                                                                <input  type="text" name="DETIL[DPELSEBELUM]" id="DPELSEBELUM" url="<?= site_url('referensi/autoComplate/PELABUHAN/KODE_PELABUHAN'); ?>" onfocus="Autocomp(this.id)" urai="DPELSEBELUM;DPELSEBELUMUR" class="ssstext" value="<?= $DETIL['DPELSEBELUM']; ?>" maxlength="5"/>&nbsp; 
                                                                <button onclick="tb_search('pelabuhan', 'DPELSEBELUM;DPELSEBELUMUR', 'Kode Pelabuhan', 'transit', 650, 400)" class="btn"> ... </button>
                                                                <span id="DPELSEBELUMUR"><?= $DETIL['DPELSEBELUMUR'] == '' ? $DPELSEBELUMUR : $DETIL['DPELSEBELUMUR']; ?></span>
                                                            </td>
                                                        </tr>
                                                        
                                                    </table>
                                                </td>
                                                <td style="width: 50%">
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="100px">Tujuan </td>
                                                            <td>
                                                                <input type="text" name="DETIL[DPELBONGKAR]" id="DPELBONGKAR" value="<?= $DETIL['DPELBONGKAR']; ?>" url="<?= site_url('referensi/autoComplate/PELABUHAN/KODE_PELABUHAN'); ?>" onfocus="Autocomp(this.id)" urai="DPELBONGKAR;DPELBONGKARUR" class="ssstext"  maxlength="5"/>&nbsp; 
                                                                <button onclick="tb_search('pelabuhan', 'DPELBONGKAR;DPELBONGKARUR', 'Kode Pelabuhan', 'bongkar', 650, 400)" class="btn" > ... </button>
                                                                <span id="DPELBONGKARUR"><?= $DETIL['DPELBONGKARUR'] == '' ? $DPELBONGKARUR : $DETIL['DPELBONGKARUR']; ?></span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="100px">Akhir </td>
                                                            <td >
                                                                <input  type="text" name="DETIL[DPELLANJUT]" id="DPELLANJUT" value="<?= $DETIL['DPELLANJUT']; ?>" url="<?= site_url('referensi/autoComplate/PELABUHAN/KODE_PELABUHAN'); ?>" onfocus="Autocomp(this.id)" urai="DPELLANJUT;DPELLANJUTUR" class="ssstext" maxlength="5"/>&nbsp;
                                                                <button onclick="tb_search('pelabuhan', 'DPELLANJUT;DPELLANJUTUR', 'Kode Pelabuhan', 'Selanjutnya', 650, 400)" class="btn"> ... </button>
                                                                <span id="DPELLANJUTUR"><?= $DETIL['DPELLANJUTUR'] == '' ? $DPELLANJUTUR : $DETIL['DPELLANJUTUR']; ?></span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset> 
                                </td>                
                            </tr>
                        </table>                
                        <table style=" width:100%">
                            <tr>
                                <td style=" width: 100%">
                                    <fieldset>
                                        <legend>Parties</legend>
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 50%">
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="100px">Nama Pengirim </td>
                                                            <td>
                                                                <input type="text" size="20" name="DETIL[NMPENGIRIM]" id="NMPENGIRIM" value="<?= $DETIL['NMPENGIRIM']; ?>"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                &nbsp;&nbsp;&nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="100px">Nama Penerima </td>
                                                            <td>
                                                                <input type="text" size="20" name="DETIL[NMPENERIMA]" id="NMPENERIMA" value="<?= $DETIL['NMPENERIMA']; ?>"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                &nbsp;&nbsp;&nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="100px">Nama Pemberitahu </td>
                                                            <td>
                                                                <input type="text" size="20" name="DETIL[NMPEMBERITAU]" id="NMPEMBERITAU" value="<?= $DETIL['NMPEMBERITAU']; ?>"/>
                                                            </td>
                                                        </tr>
                                                        
                                                    </table>
                                                </td>
                                                <td style="width: 50%">
                                                    <table width="100%">
                                                        <tr>
                                                            <td style="vertical-align: top">Alamat Pengirim </td>
                                                            <td>
                                                                <textarea name="DETIL[ALPENGIRIM]" id="ALPENGIRIM" rows="1"  cols="25"><?= $DETIL['ALPENGIRIM']; ?></textarea>                                    
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="vertical-align: top">Alamat Penerima </td>
                                                            <td>
                                                                <textarea name="DETIL[ALPENERIMA]" id="ALPENERIMA" rows="1"  cols="25"><?= $DETIL['ALPENERIMA']; ?></textarea>                                    
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="vertical-align: top">Alamat Pemberitahu </td>
                                                            <td>
                                                                <textarea name="DETIL[ALPEMBERITAU]" id="ALPEMBERITAU" rows="1" cols="25"><?= $DETIL['ALPEMBERITAU']; ?></textarea>                                    
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset> 
                                </td>                
                            </tr>
                        </table>
                        <table style=" width:100%">
                            <tr>
                                <td style=" width: 100%">
                                    <fieldset>
                                        <legend>Mark</legend>
                                        <table>
                                            <tr>
                                                <td style="vertical-align: top">
                                                    <table width="100%" >
                                                        <tr>
                                                            <td style="vertical-align: top">Merk Kemasan</td>
                                                            <td>
                                                                <textarea name="DETIL[MERKKEMAS]" id="MERKKEMAS" rows="1" cols="40"><?= $DETIL['MERKKEMAS']; ?></textarea>                                    
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="vertical-align: top">Uraian Barang</td>
                                                            <td>
                                                                <textarea name="DETIL[URBRG]" id="URBRG" rows="1" cols="40" readonly><?= $DETIL['DESCRIPTION']; ?></textarea>                                    
                                                            </td>
                                                        </tr>  
                                                        <tr>
                                                            <td width="50%" colspan="4">
                                                                <fieldset >
                                                                    <legend><b>Uraian Barang</b></legend>
                                                                    <?= $TBLURBARANG; ?>
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </table>
                                               </td>
                                                <td>
                                                    <fieldset>
                                                        <table width="100%">
                                                            <tr>
                                                                <td width="100px">Total Kemasan </td>
                                                                <td>
                                                                    <input type="text" name="DETIL[MJMLKEMAS]" id="MJMLKEMAS" value="<?= $DETIL['MJMLKEMAS']; ?>"/>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="100px">Bruto </td>
                                                                <td>
                                                                    <input type="text" name="DETIL[BRUTO]" id="BRUTO" value="<?= $DETIL['BRUTO']; ?>"/> Kg
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="100px">Volume </td>
                                                                <td>
                                                                    <input type="text" name="DETIL[VOLUME]" id="VOLUME" value="<?= $DETIL['VOLUME']; ?>"/> M3
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                </td>

                                                            </tr>
                                                            <tr>
                                                                <td width="50%" colspan="4">
                                                                    <fieldset >
                                                                        <legend><b>Kontainer</b></legend>
                                                                        <?= $TBLKONTAINER; ?>
                                                                    </fieldset>
                                                                </td>
                                                            </tr>                                                
                                                        </table>                                            
                                                    </fieldset>
                                            <tr>
                                                <td width="60%" colspan="4">
                                                    <fieldset >
                                                        <legend><b>Dokumen Pabean</b></legend>
                                                        <?= $TBLDOKUMEN; ?>
                                                    </fieldset>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><?php
                                                    $chkConsolidate[$DETIL['CONSOLIDATE']] = 'checked';
                                                    ?>
                                                    <input type="checkbox" name="DETIL[CONSOLIDATE]" id="CONSOLIDATEIN" value="1" <?= $chkConsolidate['1'] ?>>
                                                    <label for="CONSOLIDATEIN">Consolidate Good</label>                                                   
                                                </td>
                                                
                                                <td><?php
                                                    $chkParsial[$DETIL['PARTIAL']] = 'checked';
                                                    ?>
                                                    <input type="checkbox" name="DETIL[PARTIAL]" id="PARTIALIN" value="1" <?= $chkParsial['1'] ?>>
                                                    <label for="PARTIALIN">Partial Shipment</label>                                                    
                                                </td>
                                            </tr>

                                            </td>
                                            </tr>	 			
                                        </table>	
                                    </fieldset>   
                            <tr>

                            </tr>
                            </td>                        
                            </tr>
                        </table>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
