<?php
if (!defined('BASEPATH')){
    exit('No direct script access allowed');
}
$dokumen = explode(",", $PERUSAHAAN['DOKUMEN']);
foreach ($dokumen as $val) {
    $arrDok[$val] = 'checked="checked"';
    $arrA[$val] = true;
}
?>
<script>
    $(document).ready(function () {
        $("#tabs").tabs();
        declareDP('frmProfilePerusahaan');
        declareDP('frmProfileUser');
        hideTIPETRADER($('#TIPE_TRADER').val());
        $("#profile").accordion({
            heightStyle: "content"
        });
        $("#myform").dialog({
            autoOpen: false,
            modal: true,
            resizable: false,
            beforeClose: function () {
                var query = $.ajax({
                    type: "POST",
                    cache: false,
                    url: "<?php echo site_url('/usermanage/ubahpasword'); ?>",
                    data: {mytext: $("#mytext").val()},
                    success: function (theresponse) {
                        $("#response").html(theresponse);
                    },
                    error: function () {
                        $("#response").html("AJAX request failed.");
                    }
                });

                $("#response").show();
            }
        });
        formatNPWP('ID');
    });
    function hideTIPETRADER(id) {
        $('#PPJK').hide();
        switch (id) {
            case '1':
                $('').show();
                break;
            case '2':
                $('#PPJK').show();
                break;
            case '3':
                $('').show();
                break;
        }
    }
</script>
<div class="content_luar">
    <div class="content_dalam">
        <div id="tabs">
            <ul>
                <li><a href="#tab-ProfilePerusahaan">Profil Perusahaan</a></li>
                <li><a href="#tab-ProfileUser">Profil User</a></li>
<?php if($arrA['BC20']){ ?><li><a href="#tab-PIB">Setting PIB</a></li> <?php }?>
<?php if($arrA['BC30']){ ?><li><a href="#tab-PEB">Setting PEB</a></li> <?php }?>
<?php if($arrA['BC23']){ ?><li><a href="#tab-TPB">Setting TPB</a></li> <?php }?>
<?php if($arrA['BC10']){ ?><li><a href="#tab-Manifest">Setting Manifest</a></li> <?php }?>
            </ul>
            <div id="tab-ProfilePerusahaan" style=" padding-left: 1px; padding-right: 1px;">
                <form id="frmProfilePerusahaan" name="frmProfilePerusahaan" action="<?= site_url('usermanage/setProfile') ?>" onsubmit="return false;" class="form uniformForm" method="post" autocomplete=off>
                    <?= $MAXLENGTH ?>
                    <input type="hidden" id="KODE_TRADER" name="DATA[KODE_TRADER]" value="<?= $PERUSAHAAN['KODE_TRADER'] ?>">
                    <table width="100%" border="0">
                        <tr>
                            <td width="50%" valign="top">
                                <table width="100%" border="0">
                                    <tr>
                                        <td colspan="2">
                                            <h5>DATA PERUSAHAAN</h5>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="150px">Identitas *</td>
                                        <td>: <?= form_dropdown('DATA[KODE_ID]', $JENIS_IDENTITAS, $PERUSAHAAN['KODE_ID'], 'id="KODE_ID" wajib="yes" disabled'); ?>&nbsp;<input type="text" name="DATA[ID]" id="ID" class="mtext" value="<?= $PERUSAHAAN['ID']; ?>" wajib="yes" onfocus="unformatNPWP(this.id);" onblur="formatNPWP(this.id);" wajib="yes" disabled/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nama *</td>
                                        <td>: <input type="text" name="DATA[NAMA]" id="NAMA" class="ltext" value="<?= $PERUSAHAAN['NAMA']; ?>" wajib="yes" disabled/></td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top;">Alamat *</td>
                                        <td><font style="vertical-align:top"> :</font> <textarea name="DATA[ALAMAT]" id="ALAMAT" class="ltext" wajib="yes"><?= $PERUSAHAAN['ALAMAT'] ?></textarea></td>
                                    </tr>
                                    <tr>
                                        <td>Nomor Telepon *</td>
                                        <td>: <input type="text" name="PERUSAHAAN[TELEPON]" id="TELEPON" class="smtext" value="<?= $PERUSAHAAN['TELEPON']; ?>" wajib="yes"/></td>
                                    </tr>
                                    <tr>
                                        <td>Nomor Fax *</td>
                                        <td>: <input type="text" name="DATA[FAX]" id="FAX" class="smtext" value="<?= $PERUSAHAAN['FAX']; ?>" wajib="yes"/></td>
                                    </tr>
                                    <tr>
                                        <td>Tipe Trader</td>
                                        <td>: <?= form_dropdown('DATA[TIPE_TRADER]', $TIPE_TRADER, $PERUSAHAAN['TIPE_TRADER'], 'id="TIPE_TRADER" wajib="yes" onchange="hideTIPETRADER(this.value)"'); ?></td>
                                    </tr>
                                    <tr id="PPJK">
                                        <td>No/Tgl PPJK</td>
                                        <td>: <input type="text" name="DATA[NO_PPJK]" id="NO_PPJK" class="sstext" value="<?= $PERUSAHAAN['NO_PPJK']; ?>"/> &nbsp;<input type="text" name="DATA[TANGGAL_PPJK]" id="TANGGAL_PPJK" class="sstext" value="<?= $PERUSAHAAN['TANGGAL_PPJK']; ?>" formatDate="yes"/></td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top" >Dokumen Terdaftar</td>
                                        <td>
                                            <table style="width: 100%">
                                                <tr>
                                                    <td style="width: 50%">
                                                        <input type="checkbox" name="PERUSAHAAN[DOKUMEN][BC20]" id="DOKUMEN[BC20]" value="BC20" <?= $arrDok['BC20'] ?> disabled="disabled"/>
                                                        <label for="DOKUMEN[BC20]">BC 2.0 / PIB</label>
                                                    </td>
                                                    <td style="width: 50%">
                                                        <input type="checkbox" name="PERUSAHAAN[DOKUMEN][BC30]" id="DOKUMEN[BC30]" value="BC30" <?= $arrDok['BC30'] ?> disabled="disabled"/>
                                                        <label for="DOKUMEN[BC30]">BC 3.0 / PEB</label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" name="PERUSAHAAN[DOKUMEN][BC23]" id="DOKUMEN[BC23]" value="BC23" <?= $arrDok['BC23'] ?> disabled="disabled"/>
                                                        <label for="DOKUMEN[BC23]">BC 2.3 / TPB</label>
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" name="PERUSAHAAN[DOKUMEN][BC10]" id="DOKUMEN[BC10]" value="BC10" <?= $arrDok['BC10'] ?> disabled="disabled"/>
                                                        <label for="DOKUMEN[BC10]">Manifest</label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top">
                                <table width="100%" border="0">
                                    <tr>
                                        <td colspan="2"><h5>DATA PEMILIK PERUSAHAAN</h5></td>
                                    </tr>
                                    <tr>
                                        <td width="150px">Nama *</td>
                                        <td>: <input type="text" name="PERUSAHAAN[NAMA_PEMILIK]" id="NAMA_PEMILIK" class="mtext" value="<?= $PERUSAHAAN['NAMA_PEMILIK']; ?>" wajib="yes"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Tempat/Tanggal Lahir *</td>
                                        <td>: <input type="text" name="DATA[TEMPAT_LAHIR_PEMILIK]" id="TEMPAT_LAHIR_PEMILIK" class="mtext" value="<?= $PERUSAHAAN['TEMPAT_LAHIR_PEMILIK']; ?>" wajib="yes"/>&nbsp;<input type="text" name="DATA[TANGGAL_LAHIR_PEMILIK]" id="TANGGAL_LAHIR_PEMILIK" class="sstext" formatDate="yes" value="<?= $PERUSAHAAN['TANGGAL_LAHIR_PEMILIK']; ?>" wajib="yes"/></td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top;">Alamat *</td>
                                        <td><font style="vertical-align:top"> :</font> <textarea name="DATA[ALAMAT_PEMILIK]" id="ALAMAT_PEMILIK" class="ltext" wajib="yes"><?= $PERUSAHAAN['ALAMAT_PEMILIK']; ?></textarea></td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top;">e-Mail *</td>
                                        <td>: <input type="text" name="DATA[EMAIL_PEMILIK]" id="EMAIL_PEMILIK" class="ltext" value="<?= $PERUSAHAAN['EMAIL_PEMILIK']; ?>" wajib="yes"/></td>
                                    </tr> 
                                    <tr>
                                        <td colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>EDI Number</td>
                                        <td>: <input type="text" name="DATA[EDINUMBER]" id="EDINUMBER" class="mtext" value="<?= $PERUSAHAAN['EDINUMBER']; ?>" wajib="yes" disabled /></td>
                                    </tr>
                                    <tr>
                                        <td>Unlock code</td>
                                        <td>: <input type="text" name="DATA[UNLOCKCODE]" id="UNLOCKCODE" class="vltext" value="<?= $PERUSAHAAN['UNLOCKCODE']; ?>" wajib="yes" disabled /></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <button class="btn btn-small" onclick="save_post_msg('frmProfilePerusahaan');"> &nbsp; Simpan &nbsp; </button>
                                <button class="btn btn-small" onclick="document.getElementById('frmProfilePerusahaan').reset();"> &nbsp; Batal  &nbsp; </button>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
            <div id="tab-ProfileUser" style=" padding-left: 1px; padding-right: 1px;">
                <form id="frmProfileUser" name="frmProfileUser" action="<?= site_url('usermanage/setProfileUser') ?>" onsubmit="return false;" class="form uniformForm" method="post" autocomplete=off>
                    <?= $MAXLENGTH ?>
                    <input type="hidden" id="KODE_TRADER" name="DATA[KODE_TRADER]" value="<?= $USER['KODE_TRADER'] ?>">
                    <table width="100%" border="0">
                        <tr>
                            <td width="50%" valign="top">
                                <table width="100%" border="0">
                                    <tr>
                                        <td colspan="2">
                                            <h5>DATA USER</h5>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="150px">Username *</td>
                                        <td>: <input type="text" name="DATA[USERNAME]" id="USERNAME" class="ltext" value="<?= $USER['USERNAME']; ?>" wajib="yes" disabled /></td>
                                    </tr>
                                    <tr>
                                        <td>Nama *</td>
                                        <td>: <input type="text" name="DATA[NAMA]" id="NAMA" class="ltext" value="<?= $USER['NAMA']; ?>" wajib="yes"/></td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top;">Alamat *</td>
                                        <td><font style="vertical-align:top"> :</font> <textarea name="DATA[ALAMAT]" id="ALAMAT" class="ltext" wajib="yes"><?= $USER['ALAMAT'] ?></textarea></td>
                                    </tr>
                                    <tr>
                                        <td>Nomor Telepon *</td>
                                        <td>: <input type="text" name="DATA[TELEPON]" id="TELEPON" class="smtext" value="<?= $USER['TELEPON']; ?>" wajib="yes"/></td>
                                    </tr>
                                    <tr>
                                        <td>Last login</td>
                                        <td>: <input type="text" name="DATA[LAST_LOGIN]" id="LAST_LOGIN" class="smtext" value="<?= $USER['LAST_LOGIN']; ?>" wajib="yes"/></td>
                                    </tr>
                                    <tr>
                                        <td>Expired date</td>
                                        <td>: <input type="text" name="DATA[EXPIRED_DATE]" id="EXPIRED_DATE" class="smtext" value="<?= $USER['EXPIRED_DATE']; ?>" disabled /></td>
                                    <tr>
                                        <td><br/>
                                            <button class="btn btn-primary" onclick="Dialog( site_url + '/usermanage/changePassword','msgbox','Ubah Password',400,200)" > &nbsp; Ubah password &nbsp; </button>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <button class="btn btn-small" onclick="save_post_msg('frmProfileUser');"> &nbsp; Simpan &nbsp; </button>
                                <button class="btn btn-small" onclick="document.getElementById('frmProfileUser').reset();"> &nbsp; Reset  &nbsp; </button>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
<?php if($arrA['BC20']){?>
            <script>$(document).ready(function () {declareDP('frmSettingPIB');f_MAXLENGTH('frmSettingPIB');});</script>
            <div id="tab-PIB" style=" padding-left: 1px; padding-right: 1px;">
                <form id="frmSettingPIB" name="frmSettingPIB" action="<?= site_url('usermanage/setProfilePIB') ?>" onsubmit="return false;" class="form uniformForm" method="post" autocomplete=off>
                    <?= $PIB['MAXLENGTH'] ?>
                    <input type="hidden" id="KODE_TRADER" name="DATA[KODE_TRADER]" value="<?= $PIB['KODE_TRADER'] ?>">
                    <table width="100%" border="0">
                        <tr>
                            <td width="50%" valign="top">
                                <table width="100%" border="0">
                                    <tr>
                                        <td width="150px">Kantor Pelayanan BC</td>
                                        <td> : 
                                            <input type="text" id="KDKPBC" value="<?= $PIB['KPBC']; ?>" url="<?= site_url('referensi/autoComplate/KPBC'); ?>" urai="KDKPBC;URKPBC" wajib="yes" onfocus="Autocomp(this.id)" class="ssstext" maxlength="6" disabled /> &nbsp;
                                            <span id="URKPBC"><?= $PIB['URAIAN_KPBC']; ?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nomor registrasi</td>
                                        <td>: <input type="text" name="DATA[NOREG]" id="NOREG" class="ssstext" value="<?= $PIB['NOREG']; ?>" wajib="yes" disabled /></td>
                                    </tr>
                                    <tr>
                                        <td>Aju terakhir</td>
                                        <td>: <input type="text" name="DATA[LAST_AJU]" id="LAST_AJU" class="ssstext" value="<?= $PIB['LAST_AJU']; ?>" wajib="yes"/></td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top;" >Status </td>
                                        <td nowrap><?php
                                            $impstatus = explode(",", $PIB['IMPSTATUS']);
                                            foreach ($impstatus as $val) {
                                                $arr[$val] = 'checked';
                                            }
                                            ?>
                                            <table width="100%">
                                                <tr>
                                                    <td>: <input type="checkbox" name="DATA[IMPSTATUS][1]" id="IMPSTATUS[1]" value="1" <?= $arr[1] ?> /> <label for="IMPSTATUS[1]">&nbsp;Importir Umum</label></td>
                                                    <td><input type="checkbox" name="DATA[IMPSTATUS][4]" id="IMPSTATUS[4]" value="4" <?= $arr[4] ?>/> <label for="IMPSTATUS[4]">&nbsp;Agen Tunggal</label></td>
                                                    <td><input type="checkbox" name="DATA[IMPSTATUS][7]" id="IMPSTATUS[7]" value="7" <?= $arr[7] ?>/> <label for="IMPSTATUS[7]">&nbsp;DAHANA</label></td>
                                                </tr>
                                                <tr>
                                                    <td >&nbsp; <input type="checkbox" name="DATA[IMPSTATUS][2]" id="IMPSTATUS[2]" value="2" <?= $arr[2] ?>/> <label for="IMPSTATUS[2]">&nbsp;Importir Produser</label></td>
                                                    <td ><input type="checkbox" name="DATA[IMPSTATUS][5]" id="IMPSTATUS[5]" value="5" <?= $arr[5] ?>/> <label for="IMPSTATUS[5]">&nbsp;BULOG</label></td>
                                                    <td ><input type="checkbox" name="DATA[IMPSTATUS][8]" id="IMPSTATUS[8]" value="8" <?= $arr[8] ?>/> <label for="IMPSTATUS[8]">&nbsp;IPTN</label></td>
                                                </tr>
                                                <tr>
                                                    <td >&nbsp; <input type="checkbox" name="DATA[IMPSTATUS][3]" id="IMPSTATUS[3]" value="3" <?= $arr[3] ?>/> <label for="IMPSTATUS[3]">&nbsp;Importir Terdaftar</label></td>
                                                    <td ><input type="checkbox" name="DATA[IMPSTATUS][6]" id="IMPSTATUS[6]" value="6" <?= $arr[6] ?>/> <label for="IMPSTATUS[6]">&nbsp;PERTAMINA</label></td>
                                                    <td >&nbsp;</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="50%" valign="top">
                                <table width="100%" border="0">
                                    <tr>
                                        <td width="150px">Nilai PNBP</td>
                                        <td>: <input type="text" name="DATA[PNBP]" id="PNBP" class="stext" value="<?= $PIB['PNBP']; ?>" wajib="yes" style=" text-align: right;" /></td>
                                    </tr>
                                    <tr>
                                        <td>No. SRP</td>
                                        <td>: <input type="text" name="DATA[SRP]" id="SRP" class="stext" wajib="yes" value="<?= $PIB['SRP'] ?>" /></td>
                                    </tr>
                                    <tr>
                                        <td>APIP / APIU</td>
                                        <td>: <?= form_dropdown('DATA[APIKD]', $APIKD, $PIB['APIKD'], 'id="APIKD" class="stext" '); ?>
                                            <input type="text" name="DATA[APINO]" id="APINO" value="<?= $PIB['APINO'] ?>" url="<?= site_url(); ?>/autocomplete/importir" class="smtext"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nama Penandatangan</td>
                                        <td>: <input type="text" name="DATA[NAMA_TTD]" id="NAMA_TTD" class="ltext" value="<?= $PIB['NAMA_TTD']; ?>" wajib="yes"/></td>
                                    </tr>
                                    <tr>
                                        <td>Kota Penandatangan</td>
                                        <td>: <input type="text" name="DATA[KOTA_TTD]" id="KOTA_TTD" class="ltext" value="<?= $PIB['KOTA_TTD']; ?>" wajib="yes"/></td>                       </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <button class="btn btn-small" onclick="save_post_msg('frmSettingPIB');"> &nbsp; Simpan &nbsp; </button>
                                <button class="btn btn-small" onclick="document.getElementById('frmSettingPIB').reset();"> &nbsp; Reset  &nbsp; </button>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
<?php }?>
<?php if($arrA['BC30']){?>
            <script>$(document).ready(function () {declareDP('frmSettingPEB');f_MAXLENGTH('frmSettingPEB');});</script>
            <div id="tab-PEB" style=" padding-left: 1px; padding-right: 1px;">
                <form id="frmSettingPEB" name="frmSettingPEB" action="<?= site_url('usermanage/setProfilePEB') ?>" onsubmit="return false;" class="form uniformForm" method="post" autocomplete=off>
                    <?= $PEB['MAXLENGTH'] ?>
                    <input type="hidden" id="KODE_TRADER" name="DATA[KODE_TRADER]" value="<?= $PEB['KODE_TRADER'] ?>">
                    <table width="100%" border="0">
                        <tr>
                            <td width="50%" valign="top">
                                <table width="100%" border="0">
                                    <tr>
                                        <td width="150px">Kantor Pelayanan Bea Cukai</td>
                                        <td> : 
                                            <input type="text" id="KDKPBC" value="<?= $PEB['KPBC']; ?>" class="ssstext" disabled/> &nbsp;
                                            <span id="URKPBC"><?= $PEB['URAIAN_KPBC']; ?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nomor registrasi</td>
                                        <td>: <input type="text" name="DATA[NOREG]" id="NOREG" class="ssstext" value="<?= $PEB['NOREG']; ?>" wajib="yes" disabled/></td>
                                    </tr>
                                    <tr>
                                        <td>Aju terakhir</td>
                                        <td>: <input type="text" name="DATA[LAST_AJU]" id="LAST_AJU" class="ssstext" value="<?= $PEB['LAST_AJU']; ?>" wajib="yes"/></td>
                                    </tr>
                                    <tr>
                                        <td>Nomor induk perusahaan</td>
                                        <td>: <input type="text" name="DATA[NIPER]" id="NIPER" class="ssstext" value="<?= $PEB['NIPER']; ?>" wajib="yes"/></td>
                                    </tr>
                                    <tr>
                                        <td>Nomor SIUP</td>
                                        <td>: <input type="text" name="DATA[NOSIUP]" id="NOSIUP" class="ltext" value="<?= $PEB['NOSIUP']; ?>" wajib="yes"/></td>
                                    </tr>
                                    <tr>
                                        <td>Taggal SIUP</td>
                                        <td>: <input type="text" name="DATA[TGSIUP]" id="TGSIUP" class="sstext" value="<?= $PEB['TGSIUP']; ?>" formatDate="yes" /></td>
                                    </tr>
                                    <tr>
                                        <td>Jenis Perusahaan</td>
                                        <td>: <?= form_dropdown('DATA[JNPERUS]', $JNS_PERUSAHAAN, $PEB['JNPERUS'], 'id="JNPERUS" class="lmtext" '); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nomor TDP</td>
                                        <td>: <input type="text" name="DATA[NOTDP]" id="NOTDP" class="ltext" value="<?= $PEB['NOTDP']; ?>" wajib="yes"/></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal TDP</td>
                                        <td>: <input type="text" name="DATA[TGTDP]" id="TGTDP" class="sstext" value="<?= $PEB['TGTDP']; ?>" wajib="yes" formatDate="yes" />
                                        </td>
                                    </tr>
                                    
                                </table>
                            </td>
                            <td valign="top">
                                <table width="100%" border="0">
                                    <tr>
                                        <td width="150px">Status Eksportir</td>
                                        <td>: <?= form_dropdown('DATA[STATUSH]', $STATUS_EKS, $PEB['STATUSH'], 'id="STATUSH" class="lmtext" '); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nomor MOU</td>
                                        <td>: <input type="text" name="DATA[NOMOU]" id="NOMOU" class="ltext" value="<?= $PEB['NOMOU']; ?>" wajib="yes"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal MOU</td>
                                        <td>: <input type="text" name="DATA[TGMOU]" id="TGMOU" class="sstext" value="<?= $PEB['TGMOU']; ?>" wajib="yes" formatDate="yes"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Jenis konsolidator</td>
                                        <td>: <?= form_dropdown('DATA[JNKONS]', $STATUS_KONS, $PEB['JNKONS'], 'id="JNKONS" class="lmtext" '); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Nomor Konsolidator</td>
                                        <td>: <input type="text" name="DATA[NOKONSOLIDATOR]" id="NOKONSOLIDATOR" class="ltext" value="<?= $PEB['NOKONSOLIDATOR']; ?>" wajib="yes"/></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Konsolidator</td>
                                        <td>: <input type="text" name="DATA[TGKONSOLIDATOR]" id="TGKONSOLIDATOR" class="sstext" value="<?= $PEB['TGKONSOLIDATOR']; ?>" wajib="yes" formatDate="yes"/></td>
                                    </tr>
                                    <tr>
                                        <td>Nama Penanda Tangan</td>
                                        <td>: <input type="text" name="DATA[NAMA_TTD]" id="NAMA_TTD" class="ltext" value="<?= $PEB['NAMA_TTD']; ?>"/></td>
                                    </tr>
                                    <tr>
                                        <td>Kota Penanda Tangan</td>
                                        <td>: <input type="text" name="DATA[KOTA_TTD]" id="KOTA_TTD" class="ltext" value="<?= $PEB['KOTA_TTD']; ?>"/></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <button class="btn btn-small" onclick="save_post_msg('frmSettingPEB');"> &nbsp; Simpan &nbsp; </button>
                                <button class="btn btn-small" onclick="document.getElementById('frmSettingPEB').reset();"> &nbsp; Reset  &nbsp; </button>
                            </td>
                        </tr>
                    </table>
                </form>
</div> 
<?php }?>
<?php if($arrA['BC23']){?>
            <script>$(document).ready(function () {declareDP('frmSettingTPB');f_MAXLENGTH('frmSettingTPB');});</script>
            <div id="tab-TPB" style=" padding-left: 1px; padding-right: 1px;">
                <form id="frmSettingTPB" name="frmSettingTPB" action="<?= site_url('usermanage/setProfileTPB') ?>" onsubmit="return false;" class="form uniformForm" method="post" autocomplete=off>
                     <?= $TPB['MAXLENGTH'] ?>
                    <input type="hidden" id="KODE_TRADER" name="DATA[KODE_TRADER]" value="<?= $TPB['KODE_TRADER'] ?>"></input>
                    <table width="100%" border="0">
                        <tr>
                            <td width="50%" valign="top">
                                <table width="100%" border="0">                                   
                                    <tr>
                                        <td width="150px">Kantor Pelayanan Bea Cukai</td>
                                        <td> : 
                                            <input type="text" id="KDKPBC" value="<?= $TPB['KPBC']; ?>" class="ssstext" disabled/> &nbsp;
                                            <span id="URKPBC"><?= $TPB['URAIAN_KPBC']; ?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nomor registrasi</td>
                                        <td>: <input type="text" name="DATA[NOREG]" id="NOREG" class="ssstext" value="<?= $TPB['NOREG']; ?>" wajib="yes" disabled/></td>
                                    </tr>
                                    <tr>
                                        <td>Aju terakhir</td>
                                        <td>: <input type="text" name="DATA[LAST_AJU]" id="LAST_AJU" class="ssstext" value="<?= $TPB['LAST_AJU']; ?>" wajib="yes"/></td>
                                    </tr>
                                    <tr>
                                        <td>Status Usaha</td>
                                        <td>: <?= form_dropdown('DATA[USAHASTATUS]', $STATUS_PERUSAHAAN, $TPB['USAHASTATUS'], 'id="USAHASTATUS" class="lmtext" '); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>No Skep Izin</td>
                                        <td>: <input type="text" name="DATA[REGISTRASI]" id="REGISTRASI" class="ltext" value="<?= $TPB['REGISTRASI']; ?>" wajib="yes"/></td>
                                    </tr>
                                </table>
                            </td>
                            <td width="50%" valign="top">
                                <table width="100%" border="0">
                                    <tr>
                                        <td>APIP / APIU</td>
                                        <td>: <?= form_dropdown('DATA[APIKD]', $APIKD, $TPB['APIKD'], 'id="APIKD" class="stext" '); ?>
                                            <input type="text" name="DATA[APINO]" id="APINO" value="<?= $TPB['APINO'] ?>" url="<?= site_url(); ?>/autocomplete/importir" class="smtext"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Jenis TPB</td>
                                        <td>: <?= form_dropdown('DATA[KDTPB]', $KDTPB, $TPB['KDTPB'], 'id="KDTPB" class="lmtext" '); ?>                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nama Penanda Tangan</td>
                                        <td>: <input type="text" name="DATA[NAMA_TTD]" id="NAMA_TTD" class="ltext" value="<?= $TPB['NAMA_TTD']; ?>"/></td>
                                    </tr>
                                    <tr>
                                        <td>Kota Penanda Tangan</td>
                                        <td>: <input type="text" name="DATA[KOTA_TTD]" id="KOTA_TTD" class="ltext" value="<?= $TPB['KOTA_TTD']; ?>"/></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <button class="btn btn-small" onclick="save_post_msg('frmSettingTPB');"> &nbsp; Simpan &nbsp; </button>
                                <button class="btn btn-small" onclick="document.getElementById('frmSettingTPB').reset();"> &nbsp; Reset  &nbsp; </button>
                            </td>
                        </tr>
                    </table>                   
                </form>
            </div>
<?php }?>
<?php if($arrA['BC10']){?>
            <script>$(document).ready(function () {declareDP('frmSettingManifest');f_MAXLENGTH('frmSettingManifest');});</script>
            <div id="tab-Manifest" style=" padding-left: 1px; padding-right: 1px;">
                <form id="frmSettingManifest" name="frmSettingManifest" action="<?= site_url('usermanage/setProfileManifest') ?>" onsubmit="return false;" class="form" method="post" autocomplete=off>
                    <?= $MANIFEST['MAXLENGTH'] ?>
                    <table width="100%" border="0">
                        <tr>
                            <td width="50%" valign="top">
                                <table width="100%" border="0">                                   
                                    <tr>
                                        <td width="150px">Kantor Pelayanan Bea Cukai</td>
                                        <td> : 
                                            <input type="text" id="KDKPBC" value="<?= $MANIFEST['KPBC']; ?>" class="ssstext" disabled/> &nbsp;
                                            <span id="URKPBC"><?= $MANIFEST['URAIAN_KPBC']; ?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nomor registrasi</td>
                                        <td>: <input type="text" name="DATA[NOREG]" id="NOREG" class="ssstext" value="<?= $MANIFEST['NOREG']; ?>" wajib="yes" disabled/></td>
                                    </tr>
                                    <tr>
                                        <td>Aju terakhir</td>
                                        <td>: <input type="text" name="DATA[LAST_AJU]" id="LAST_AJU" class="ssstext" value="<?= $MANIFEST['LAST_AJU']; ?>" wajib="yes"/></td>
                                    </tr>
                                    <tr>
                                        <td>Kode Agen</td>
                                        <td>: <input type="text" name="DATA[KODEAGEN]" id="KODEAGEN" class="ssstext" value="<?= $MANIFEST['KODEAGEN']; ?>" wajib="yes"/></td>
                                    </tr>
                                </table>
                            </td>
                            <td width="50%" valign="top">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <button class="btn btn-small" onclick="save_post_msg('frmSettingManifest');"> &nbsp; Simpan &nbsp; </button>
                            </td>
                        </tr>
                    </table>                   
                </form>
            </div>
<?php }?>
        </div>
    </div>
</div>

