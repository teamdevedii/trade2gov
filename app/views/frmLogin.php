<div class="widget" style="width: 400px">
    <div class="widget-header" style="text-align: left; vertical-align: middle;">
        <span class="icon-key-stroke"></span><h3>Log in</h3>
    </div> 
    <div class="widget-content">
        <form id="frmLogin" name="frmLogin" onSubmit="javascript:return login()" autocomplete=off action="<?= site_url('login/ceklogin') . '/' . $this->session->userdata('session_id'); ?>" method="post" class="form uniformForm validateForm"> 
            <table >
                <tr >
                    <td style="width: 80px">e-Mail</td>
                    <td>: <input type="text" name="_usr" id="_usr" class="validate[required,custom[email]]" placeholder="anda@mail.com" value="" style="width:200px;"></td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td>: <input type="password" name="_pass" id="_pass" class="validate[required,minSize[5]]" placeholder="cth. P@ssw0rd" value="" style="width:200px;"/></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <img alt="" id="captcha"  height="40" onClick="change_captcha()" style="cursor:pointer" title="Click to change a code"/>
                    </td>
                </tr>
                <tr>
                    <td>Captcha</td>
                    <td>: <input type="text" name="_code" id="_code" class="validate[required]"  maxlength="5"  style="width:80px; text-align:center"></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button type="button" onclick="login()"  class="btn btn-small"> Login </button>
                        <span id="pesan"></span>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
<script>
    $(document).ready(function() {
        startUp();
        change_captcha();
    });
    function notifikasi(){
        alert($(this).attr('name'));
    }
    function login() {
        var a = 0;
        $('#frmLogin input[type="text"]').each(function(n, element) {
            if ($(element).val() == '') {
                $(element).focus();
                a = a + 1;
            }
        });
        if(a > 0){
            return false;
        }else{
            //ShowErrorBox('#notify','testr','ok');
            ExecFormLogin('#pesan', '#frmLogin');
        }
        return false;
    }
    function ExecFormLogin(DivID, formId) {
        $.ajax({
            type: 'POST',
            url: $(formId).attr('action') + '/ajax',
            data: $(formId).serialize(),
            dataType: 'html',
            beforeSend: function() {
                $(DivID).removeClass('notify');
                $(DivID).css({"display": "center", "padding-right": "60px"});
                $(DivID).html('<img src=\"' + base_url + 'img/loaders/facebook.gif\" alt=\"loading\" border=\"0\" />');
            },
            success: function(data) {
                if (typeof(data) != 'undefined') {
                    var arrayDataTemp = data.split("|");
                    if (arrayDataTemp[0] == 0) {
                        $(DivID).html(arrayDataTemp[1]);
                    }
                    else if (arrayDataTemp[0] == 1) {
                        $(DivID).html(arrayDataTemp[1]);
                        window.location.reload(true);
                    }
                    else if (arrayDataTemp[0] == 2) {
                        $(DivID).html(arrayDataTemp[1]);
                        setTimeout(function() {
                            window.location.reload(true)
                        }, 2000);
                    }
                }
                change_captcha();
            }
        });
    }
    function clearNotify(DivID){
        $(DivID).removeClass('notify');
        $(DivID).css({"display": "center", "padding-right": "60px"});
        $(DivID).html('');
    }
</script>