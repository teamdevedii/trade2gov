<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
$ro = '';
if ($TIPE_TRADER == '1') {
    $ro = 'readonly';
}
?>
<span id="DivHeaderForm">
    <span id="divtmp"></span>
    <form id="frmMANIFESTHdr" name="frmMANIFESTHdr" action="<?= site_url('bc11/setHeaderMANIFEST'); ?>" method="post" autocomplete="off" onsubmit="return false;">
        <?= $MAXLENGTH ?>
        <input type="hidden" name="act" id="act" value="<?= $act; ?>" />
        <input type="hidden" name="HEADER[CAR]" id="CAR" value="<?= $HEADER['CAR']; ?>" />     
        <h4>
            <span id="off">
                <button id="" onclick="onoff(true);
                        return false;" class="btn onterus"><span class="icon-pen"></span> Edit </button>
            </span>
            <span id="on">
                <button onclick="save_post_msg('frmMANIFESTHdr');" class="btn onterus">
                    <!-- onoff(false);-->
                    <span class="icon-download"></span> Simpan
                </button>
                <button onclick="onoff(false);" class="btn onterus">
                    <span class="icon-undo"></span> Batal
                </button>
            </span>
            <span class="msgheader_" style="margin-left:20px">&nbsp;</span>
            <?php if ($HEADER['CAR']) { ?>
                <button id="btnQUEUED" onclick="gotoQUEUED($('#noaju').val())" class="btn onterus" style="float: right; margin-right: 5px;" ><span class="icon-mail"></span> Edifact / UnEdifact</button>
            <?php } ?>            
        </h4>
        <div class="content_luar">
            <div class="content_dalam">
                <table style=" width:100%">
                    <tr>
                        <td style=" width: 50%">
                            <table>
                                <tr>
                                    <td>                                        
                                        <b> <label -top:5px;padding-botstyle="paddingtom:5px;">Manifest <?= $HEADER['URJNSMANIFEST']; ?></label></b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                </tr>

                                <tr>
                                    <td>Jenis Manifest</td>
                                    <td><?php
                                        $arr[$HEADER['JNSMANIFEST']] = 'checked';
                                        ?>
                                        <input type="radio" onclick="refreshdetil()" name="HEADER[JNSMANIFEST]" id="JNSMANIFESTIN" value="I" <?= $arr['I'] ?>>
                                        <label for="JNSMANIFESTIN">Inward</label>

                                        <input type="radio" onclick="refreshdetil()" name="HEADER[JNSMANIFEST]" id="JNSMANIFESTOUT" value="O" <?= $arr['O'] ?>> 
                                        <label for="JNSMANIFESTOUT">Outward</label>
                                        <div id="divJnsManif"></div>
                                    </td>
                                </tr>

                                <tr>
                                    <td width="100px">Nomor Aju </td>
                                    <td><b><?= substr($HEADER['CAR'], 0, 6) . '-' . substr($HEADER['CAR'], 7, 6) . '-' . substr($HEADER['CAR'], 12, 8) . '-' . substr($HEADER['CAR'], 20, 6); ?></b></td>
                                </tr>

                            </table>
                            <fieldset>
                                <legend>Perusahaan Pengangkut</legend>
                                <table width="100%">
                                    <tr>
                                        <td>ID</td>
                                        <td>
                                            <?= form_dropdown('HEADER[IDPGUNA]', $IDAGEN, $HEADER['IDPGUNA'], 'id="IDPGUNA" class="smtext" readonly' . $ro); ?>
                                            <input type="text" name="HEADER[IDPGUNA]" id="IDPGUNA" url="<?= site_url('referensi/autoComplate/EKSPORTIR/NPWPEKS'); ?>" urai="IDEKS;EKSNPWP;NAMAEKS;ALMTEKS" onfocus="Autocomp(this.id);
                                                    unformatNPWP(this.id);" onblur="formatNPWP(this.id);" value="<?= $this->fungsi->FORMATNPWP($HEADER['NPWPPGUNA']); ?>" class="offterus smtext" maxlength="15" <?= $ro; ?>/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nama</td>
                                        <td>
                                            <input type="text" name="HEADER[NPWPPGUNA]" id="NPWPPGUNA" value="<?= $HEADER['NPWPPGUNA']; ?>" size="5" readonly/> 
                                            <input type="text" name="HEADER[NAMAPGUNA]" id="NAMAPGUNA" value="<?= $HEADER['NAMAPGUNA']; ?>" readonly/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top">Alamat</td>
                                        <td>
                                            <textarea name="HEADER[ALMTGUNA]" id="ALMTGUNA" readonly><?= $HEADER['ALMTGUNA']; ?></textarea>                                   
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                            <table>
                                <tr>
                                    <td width="100px">Jenis Sarana Angkut</td>
                                    <td>
                                        <?= form_dropdown('HEADER[MODA]', $MODA, $HEADER['MODA'], ' onchange="kode(this.value)" onblur="kode(this.value)" id="MODA" class="stext"'); ?> 
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style=" width: 50%">
                            <table width="100%">
                                <tr>
                                    <td style="text-align: right;" >
                                        <button id="btnValidasi" onclick="cekValidasi($('#CAR').val())" class="btn onterus">
                                            <span class="icon-check"></span> Periksa Kelengkapan [ <span id="URSTATUS"><?= $HEADER['URSTATUS']; ?></span> ]
                                        </button>
                                    </td>
                                </tr>
                            </table>
                            Jenis Manifest
                            &nbsp;
                            <?php
                            $arr[$HEADER['MANIFEST']] = 'checked';
                            ?>
                            <input type="radio" onclick="refreshdetil()" name="HEADER[MANIFEST]" id="MANIFEST0" value="0" <?= $arr['0'] ?>>
                            <label for="MANIFEST0">Biasa</label>

                            <input type="radio" onclick="refreshdetil()" name="HEADER[MANIFEST]" id="MANIFEST1" value="1"  <?= $arr['1'] ?>>
                            <label for="MANIFEST1">FTZ</label>

                            <input type="radio" onclick="refreshdetil()" name="HEADER[MANIFEST]" id="MANIFEST2" value="2" <?= $arr['2'] ?>>
                            <label for="MANIFEST2">Gabungan</label>

                            <div id="divTipeManif"></div>
                            <fieldset>
                                <legend>Respon</legend>
                                <table width="100%">
                                    <tr>
                                        <td width="25%">KPBC</td>
                                        <td width="75%">
                                            <input type="text"  name="HEADER[KPBC]" id="KPBC" value="<?= $HEADER['KPBC']; ?>" url="<?= site_url('referensi/autoComplate/KPBC'); ?>" urai="KPBC;URKPBC" onfocus="Autocomp(this.id)" class="ssstext" maxlength="6" /> &nbsp;
                                            <button onclick="tb_search('kpbc', 'KPBC;URKPBC', 'Kode Kpbc', 'kpbc', 650, 400);" class="btn"> ... </button>
                                            <span id="URKPBC"><?= $HEADER['URAIAN_KPBC'] == '' ? $URKANTOR_TUJUAN : $HEADER['URAIAN_KPBC']; ?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="25%">BC 1.0 </td>
                                        <td width="75%">
                                            <input type="text"  size="5" name="HEADER[NOBC10]" id="NOBC10" value="<?= $HEADER['NOBC10']; ?>"/> &nbsp;
                                            <input type="text"  name="HEADER[TGBC10]" id="TGBC10" value="<?= $HEADER['TGBC10']; ?>" formatDate="yes"/> &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="25%">BC 1.1 </td>
                                        <td width="75%">
                                            <input type="text"  size="5" name="HEADER[NOBC11]" id="NOBC11" value="<?= $HEADER['NOBC11']; ?>"/> &nbsp;
                                            <input type="text"  name="HEADER[TGBC11]" id="TGBC11" value="<?= $HEADER['TGBC11']; ?>" formatDate="yes"/> &nbsp;
                                        </td>
                                    </tr>     
                                    <tr>
                                        <td>&nbsp;</td>                                
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>                                
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>                                
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                </table>        
                <table style=" width:100%">
                    <tr>
                        <td width="50%" style="vertical-align: top">
                            <fieldset>
                                <legend>Sarana Angkut</legend>                       
                                <table width="100%">
                                    <tr>
                                        <td>Nama / Reg Angkut</td>
                                        <td><input type="text" name="HEADER[NMANGKUT]" id="NMANGKUT" value="<?= $HEADER['NMANGKUT']; ?>" size="15" url="<?= site_url('referensi/autoComplate/MODA/NMANGKUT'); ?>" urai="REGKAPAL;NMANGKUT;VOYFLAG;URVOYFLAG" onfocus="Autocomp(this.id);"/> 
                                            <input type="text" name="HEADER[REGKAPAL]" id="REGKAPAL" value="<?= $HEADER['REGKAPAL']; ?>" class="sstext" url="<?= site_url('referensi/autoComplate/MODA/REGKAPAL'); ?>" urai="NMANGKUT;REGKAPAL;VOYFLAG;URVOYFLAG" onfocus="Autocomp(this.id);" size="10"/></td>
                                    </tr>
                                    <tr>
                                        <td width="100px">Nomor Voyage</td>
                                        <td>
                                            <input type="text" name="HEADER[NOVOY]" id="NOVOY" size="15" value="<?= $HEADER['NOVOY']; ?>"/>             
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100px">Bendera </td>
                                        <td>
                                            <input type="text" name="HEADER[VOYFLAG]" id="VOYFLAG" value="<?= $HEADER['VOYFLAG']; ?>" class="sssstext" url="<?= site_url('referensi/autoComplate/NEGARA/KODE_NEGARA'); ?>" onfocus="Autocomp(this.id)" urai="VOYFLAG;URVOYFLAG"/> 
                                            <button onclick="tb_search('negara', 'VOYFLAG;URVOYFLAG', 'Kode Negara', 'bendera', 650, 400)" class="btn"> ... </button>
                                            <span id="URVOYFLAG"><?= $HEADER['URVOYFLAG']; ?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100px">Tanggal / Jam Tiba</td>
                                        <td>
                                            <input type="text" name="HEADER[TGTIBABERANGKAT]" id="TGTIBABERANGKAT" value="<?= $HEADER['TGTIBABERANGKAT']; ?>" maxlength="10" formatDate="yes"/>                             
                                            <input type="text" name="HEADER[JAMTIBABERANGKAT]" id="JAMTIBABERANGKAT" size="8" value="<?= $HEADER['JAMTIBABERANGKAT']; ?>"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100px">Kade </td>
                                        <td>
                                            <input type="text" name="HEADER[KADE]" id="KADE" value="<?= $HEADER['KADE']; ?>" url="<?= site_url('referensi/autoComplate/TIMBUN/KDGDG'); ?>" onfocus="Autocomp(this.id, $('#KDKPBC').val())" urai="KADE;URTMPTBN" class="ssstext" maxlength="4"/>&nbsp;
                                            <button onclick="tb_search('timbun', 'KADE;URTMPTBN', 'Kode Timbun', 'kade', 650, 400, 'KDKPBC;')" class="btn"> ... </button>
                                            <span id="URTMPTBN"><?= $HEADER['URTMPTBN'] == '' ? $URTMPTBN : $HEADER['URTMPTBN']; ?></span>
                                        </td>
                                    </tr> 	 			
                                </table>
                            </fieldset>
                        </td>
                        <td style=" width: 50%">
                            <fieldset>
                                <legend>Summary Manifest </legend>
                                <table>
                                    <tr>
                                        <td>
                                            <table width="100%">
                                                <tr>
                                                    <td width="100px">Jumlah BL </td>
                                                    <td>
                                                        <input type="text" name="HEADER[HJMLBL]" id="HJMLBL" value="<?= $HEADER['HJMLBL']; ?>"/>
                                                    </td>                                                    
                                                </tr>
                                                <tr>
                                                    <td width="100px">Jumlah Kemasan </td>
                                                    <td>
                                                        <input type="text" name="HEADER[HJMLKEMAS]" id="HJMLKEMAS" value="<?= $HEADER['HJMLKEMAS']; ?>"/>
                                                    </td>                                            
                                                </tr>
                                                <tr>
                                                    <td width="100px">Berat Kotor </td>
                                                    <td>
                                                        <input type="text" name="HEADER[HBRUTO]" id="HBRUTO" value="<?= $HEADER['HBRUTO']; ?>"/> Kg
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="100px">Volume </td>
                                                    <td>
                                                        <input type="text" name="HEADER[HVOLUME]" id="HVOLUME" value="<?= $HEADER['HVOLUME']; ?>"/> M3
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="100px">Jumlah Kontainer </td>
                                                    <td>
                                                        <input type="text" name="HEADER[HJMLCONT]" id="HJMLCONT" value="<?= $HEADER['HJMLCONT']; ?>"/>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table>
                                                <tr>
                                                    <td width="100px">Note </td>
                                                    <td>
                                                        <textarea name="HEADER[NOTE]" id="NOTE" rows="6" cols="55"><?= $HEADER['NOTE']; ?></textarea>   
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>                                
                                    </tr>	 			
                                </table>	
                            </fieldset>
                        </td>      
                    </tr>
                </table>      
                <table style=" width:100%">
                            <tr>
                                <td style=" width: 100%">
                                    <fieldset>
                                        <legend>Pelabuhan</legend>
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 50%">
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="50px">Asal / Muat</td>
                                                            <td >
                                                                <input type="text" name="HEADER[PELMUAT]" id="PELMUAT" value="<?= $HEADER['PELMUAT']; ?>" url="<?= site_url('referensi/autoComplate/PELABUHAN/KODE_PELABUHAN'); ?>" onfocus="Autocomp(this.id)" urai="PELMUAT;PELMUATUR" class="ssstext" maxlength="5"/>&nbsp;
                                                                <button onclick="tb_search('pelabuhan', 'PELMUAT;PELMUATUR', 'Kode Pelabuhan', 'Muat', 650, 400)" class="btn"> ... </button>
                                                                <span id="PELMUATUR"><?= $HEADER['PELMUATUR'] == '' ? $PELMUATUR : $HEADER['PELMUATUR']; ?></span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="50px">Sebelumnya</td>
                                                            <td >
                                                                <input type="text" name="HEADER[PELAKHIR]" id="PELAKHIR" url="<?= site_url('referensi/autoComplate/PELABUHAN/KODE_PELABUHAN'); ?>" onfocus="Autocomp(this.id)" urai="PELTRANSIT;PELTRANSITUR" class="ssstext" value="<?= $HEADER['PELAKHIR']; ?>" maxlength="5"/>&nbsp; 
                                                                <button onclick="tb_search('pelabuhan', 'PELAKHIR;PELTRANSITUR', 'Kode Pelabuhan', 'transit', 650, 400)" class="btn"> ... </button>
                                                                <span id="PELTRANSITUR"><?= $HEADER['PELTRANSITUR'] == '' ? $PELTRANSITUR : $HEADER['PELTRANSITUR']; ?></span>
                                                            </td>
                                                        </tr>
                                                        
                                                    </table>
                                                </td>
                                                <td style="width: 50%">
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="50px">Bongkar </td>
                                                            <td >
                                                                <input type="text" name="HEADER[PELBKR]" id="PELBKR" value="<?= $HEADER['PELBKR']; ?>" url="<?= site_url('referensi/autoComplate/PELABUHAN/KODE_PELABUHAN'); ?>" onfocus="Autocomp(this.id)" urai="PELBKR;PELBKRUR" class="ssstext" maxlength="5"/>&nbsp; 
                                                                <button onclick="tb_search('pelabuhan', 'PELBKR;PELBKRUR', 'Kode Pelabuhan', 'bongkar', 650, 400)" class="btn" > ... </button>
                                                                <span id="PELBKRUR"><?= $HEADER['PELBKRUR'] == '' ? $PELBKRUR : $HEADER['PELBKRUR']; ?></span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="50px">Selanjutnya </td>
                                                            <td >
                                                                <input type="text" name="HEADER[PELNEXT]" id="PELNEXT" value="<?= $HEADER['PELNEXT']; ?>" url="<?= site_url('referensi/autoComplate/PELABUHAN/KODE_PELABUHAN'); ?>" onfocus="Autocomp(this.id)" urai="PELNEXT;PELBERIKUTUR" class="ssstext" maxlength="5"/>&nbsp;
                                                                <button onclick="tb_search('pelabuhan', 'PELNEXT;PELBERIKUTUR', 'Kode Pelabuhan', 'Selanjutnya', 650, 400)" class="btn"> ... </button>
                                                                <span id="PELBERIKUTUR"><?= $HEADER['PELBERIKUTUR'] == '' ? $PELBERIKUTUR : $HEADER['PELBERIKUTUR']; ?></span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset> 
                                </td>                
                            </tr>
                        </table>
                <table style=" width:100%">
                    <tr>
                        <td style=" width: 100%">
                            <?php if ($HEADER['CAR']) { ?>
                                <fieldset>
                                    <legend>Group Detil</legend>
                                    <div id="detilRKSP">
                                        <?= $DETIL; ?>
                                    </div>
                                </fieldset>
                            <?php } ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</span>
<script>
        $(document).ready(function() {
        onoff(false);
        declareDP('frmMANIFESTHdr');
    });
    function onoff(on) {

            if ($('#frmMANIFESTHdr #CAR').val() == '' || $('#frmMANIFESTHdr #act').val() == 'save') {
            $('#frmMANIFESTHdr .outentry').hide();
        } else {
            $('#frmMANIFESTHdr .outentry').show();
        }
            if (on) {
            $('#on').show();
            $('#off').hide();
            $("#frmMANIFESTHdr :input").removeAttr("disabled");
            $('#frmMANIFESTHdr .outentry :input').attr('disabled', 'disabled');
            $("#tabs").tabs({disabled: [1, 2]});
            } else {
            $('#on').hide();
            $('#off').show();
            $("#frmMANIFESTHdr :input").attr('disabled', 'disabled');
            $('#frmMANIFESTHdr .outentry :input').removeAttr("disabled");
            $("#tabs").tabs();
                if ($("#act").val() == 'save') {
                $("#tabs").tabs({disabled: [1]});
            } else {
        $("#tabs").tabs({disabled: false});
            }
        }
        $('.onterus').removeAttr('disabled');
    $('.offterus :input').attr('readonly', 'readonly');
        }

        function gotoQUEUED() {
        jConfirm('Data akan dibentuk EDIFACT ?', site_name,
                function(r) {
                    if (r == true) {
                        jloadings();
                        $(function() {
                            $('#msgbox').html('');
                            $("#msgbox").html('<div style="margin-left: 15px; margin-top: 15px;"><img src="' + base_url + 'img/loaders/facebook.gif" /> loading...</div>');
                            $("#msgbox").dialog({
                                resizable: false,
                                height: 500,
                                modal: true,
                                width: 700,
                                title: 'Edifact / unEdifact',
                                open: function() {
                                    $.ajax({
                                        type: 'POST',
                                        url: site_url + '/bc11/queued',
                                        data: '&CAR=' + $('#CAR').val(),
                                        error: function() {
                                            Clearjloadings();
                                        },
                                        success: function(data) {
                                            Clearjloadings();
                                            $("#msgbox").html(data);
                                            call(site_url + '/bc11/refreshHeader/' + $('#noaju').val());
                                        }
                                    });
                                },
                                buttons: {
                                    Close: function() {
                                        $(this).dialog("close");
                                    }
                                }
                            });
                        });
                    } else {
                        return false;
                    }
                });
    }

        function cekValidasi(car) {
            //alert(car);return false;
            $(function() {
            $('#msgbox').html('');
                $("#msgbox").html('<div style="margin-left: 15px; margin-top: 15px;"><img src="' + base_url + 'img/loaders/facebook.gif" /> loading...</div>');
                $("#msgbox").dialog({
                resizable: false,
                height: 500,
                modal: true,
                width: 700,
                    title: 'Validasi Dokumen',
                        open: function() {
                    $.ajax({
                        type: 'POST',
                        url: site_url + '/bc11/alertcekstatus/' + car + '/t_bc11hdr',
                            success: function(data) {
                            $("#msgbox").html(data);
                            $('#URSTATUS').html($('#STATUSCEK').val());
                    //call(site_url + '/bc11/refreshHeader/' + car, 'frmMANIFESTHdr');
                }
                });
                    },
                buttons: {
                        Close: function() {
                $(this).dialog("close");
        }
    }
            });
        });
    }

            function kode(data) {
        if (data == 3) {
            $('.sarana').html('Negara');
        } else if (data == 4) {
            $('.sarana').html('Bandara');
        } else {
    $('.sarana').html('Pelabuhan');
        }
        }

        function refreshdetil() {
        //MANIFEST
        var car = $('#frmMANIFESTHdr #CAR').val();//alert(car);return false;
        if($('#MANIFEST0').is(":checked")){
            var manifest = '0';
            }else if($('#MANIFEST1').is(":checked")){
        var manifest = '1';
            }else if($('#MANIFEST2').is(":checked")){
        var manifest = '2';
        }
        //JNSMANIFEST
            if($('#JNSMANIFESTIN').is(":checked")){
        var jnsmanifest = 'I';
            }else if($('#JNSMANIFESTOUT').is(":checked")){
        var jnsmanifest = 'O';
        }
        //alert(jnsmanifest+'|'+manifest);return false;
        var url = site_url + '/bc11/daftarGroup/' + jnsmanifest + '/' + manifest + '/' + car;
    call(url, 'detilRKSP');
        
    }


</script>
