<?php
if (!defined('BASEPATH')){
    exit('No direct script access allowed');
}
?>
<script>
    $(document).ready(function(){
        $('#tabs_bc11').tabs();
    });
</script>
<div class="content_luar">
    <div class="content_dalam">
        <div id="tabs_bc11">
            <ul>
                <li><a href="#tab-Header">Data Header</a></li>
                <li><a href="#tab-Detil">Data Detil-</a></li>
            </ul>
            <div id="tab-Header"><?= $HEADER ?></div>
            <div id="tab-Detil"><?= $DETIL ?></div>
        </div>
    </div>
</div>