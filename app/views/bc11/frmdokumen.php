<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<form id="frmdokumen" nama='frmdokumen' action="<?= site_url('bc11/setDokumen') ?>" onsubmit="return false;" method="post" autocomplete="off">
    <input type='hidden' name="act" value="<?= $act; ?>" />
    <input type='hidden' name="DOKUMEN[CAR]" id="CAR" value="<?= $DOKUMEN['CAR']; ?>" />
    <input type="hidden" name="DOKUMEN[NOPOS]" id="NOPOS" value="<?= $DOKUMEN['NOPOS']; ?>" />
    <input type="hidden" name="DOKUMEN[NOPOSSUB]" id="NOPOSSUB" value="<?= $DOKUMEN['NOPOSSUB']; ?>" />
    <input type="hidden" name="DOKUMEN[NOPOSSUBSUB]" id="NOPOSSUBSUB" value="<?= $DOKUMEN['NOPOSSUBSUB']; ?>" />
    <input type="hidden" name="DOKUMEN[KDGRUP]" id="KDGRUP" value="<?= $DOKUMEN['KDGRUP']; ?>" />   
    <input type="hidden" name="DOKUMEN[SERI]" id="SERI" value="<?= $DOKUMEN['SERI']; ?>"/>
    <fieldset>
        <table>
            <tr>
                <td>Jenis Dokumen :</td>
                <td>Nomor :</td>
                <td>Tanggal :</td>
            </tr>
            <tr>
                <td>
                    <?= form_dropdown('DOKUMEN[DOKKODE]', $DOKKODE, $DOKUMEN['DOKKODE'], 'id="DOKKODE" class="smtext"'); ?>
                </td>
                <td>
                    <input type="text" name="DOKUMEN[DOKNO]" id="DOKNO" class="sstext" value="<?= $DOKUMEN['DOKNO']; ?>" maxlength="6"/>
                </td>
                <td>
                    <input type="text" name="DOKUMEN[DOKTG]" id="DOKTG" class="sstext" value="<?= $DOKUMEN['DOKTG']; ?>" maxlength="10" formatDate="yes"/>
                </td>
            </tr>
            <tr>
                <td colspan="3">Kode Kantor :</td>
            </tr>
            <tr>
                <td colspan="3">
                    <input type="text" name="DOKUMEN[DOKKPBC]" id="DOKKPBC" value="<?= $DOKUMEN['DOKKPBC']; ?>" class="ssstext" onblur="checkCode(this);" grp="kpbc" urai="URDOKKPBC" maxlength="6"/> <button onclick="tb_search('kpbc', 'DOKKPBC;URDOKKPBC', 'Kode Kpbc', this.form.id, 650, 400);" class="btn"> ... </button> <span id="URDOKKPBC"><?= $DOKUMEN['URDOKKPBC']; ?></span>
                </td>
            </tr>
<?php if(strpos('|01|00|', $STATUSHDR)){ ?>
            <tr>
                <td colspan="3"><br>
                    <button onclick="save_post_msg('frmdokumen', '', 'fdokumen_list');$('#fdokumen_form').html('');" class="btn">Simpan</button>
                    <button onclick="$('#fdokumen_form').html('');" class="btn">Batal</button>
                </td>
            </tr>
<?php }?>
        </table>
    </fieldset>
    
</form>
<script>
    $(function () {
        declareDP('frmdokumen');
        $('#fdokumen_form').show();
    })
</script>