<span id="DivHeaderForm">
    <span id="divtmp"></span>
    <form id="fbc10_" name="fbc10_" action="<?= site_url('bc23/setHeader'); ?>" method="post" autocomplete="off" onsubmit="return false;">
        <?= $MAXLENGTH ?>
        <input type="hidden" name="act" id="act" value="<?= $act; ?>" />
        <input type="hidden" name="HEADER[CAR]" id="CAR" value="<?= $HEADER['CAR']; ?>" />     
        <h4>
            <span id="off">
                <button onclick="onoff(true);
                        return false;" class="btn onterus"><span class="icon-pen"></span> Edit </button>
            </span>
            <span id="on">
                <button onclick="save_post_msg('#fbc10_');
                        onoff(false);" class="btn onterus"><span class="icon-download"></span> Simpan</button>
                <button onclick="onoff(false);" class="btn onterus"><span class="icon-undo"></span> Batal</button>
            </span><span class="msgheader_" style="margin-left:20px">&nbsp;</span>
        </h4>
        <div class="content_luar">
            <div class="content_dalam">
                <table style=" width:100%">
                    <tr>
                        <td style=" width: 50%">

                            <fieldset>
                                <legend>Dokumen</legend>
                                <table>
                                    <tr>
                                        <td height="25px;">Nomor Aju </td>
                                        <td><b><?= substr($HEADER['CAR'], 0, 6) . '-' . substr($HEADER['CAR'], 7, 6) . '-' . substr($HEADER['CAR'], 12, 8) . '-' . substr($HEADER['CAR'], 20, 6); ?></b></td>
                                        <td><button id="btnValidasi" onclick="cekValidasi($('#noaju').val())" class="btn onterus"><span class="icon-check"></span> Status [ <span id="URSTATUS"><?= $HEADER['URSTATUS']; ?></span> ] </button></td>
                                    </tr>
                                    <tr>
                                        <td width="25%">KPBC</td>
                                        <td width="75%">
                                            <input type="text"  name="HEADER[KDKPBC]" id="KDKPBC" value="<?= $HEADER['KDKPBC']; ?>" url="<?= site_url('referensi/autoComplate/KPBC'); ?>" urai="KDKPBC;URKPBC" wajib="yes" onfocus="Autocomp(this.id)" class="ssstext" maxlength="6" /> &nbsp;
                                            <button onclick="tb_search('kpbc', 'KDKPBC;URKPBC', 'Kode Kpbc', 'kpbc', 650, 400);" class="btn"> ... </button>
                                            <span id="URKPBC"><?= $HEADER['URAIAN_KPBC'] == '' ? $URKANTOR_TUJUAN : $HEADER['URAIAN_KPBC']; ?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nomor BC 1.0</td>
                                        <td><input type="text"  size="20" id="NOBC10" name="NOBC10" value="<?= $HEADER['NOBC10']; ?>"/></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal BC 1.0</td>
                                        <td><input type="text" size="20" id="TGBC10" name="TGBC10" value="<?= $HEADER['TGBC10']; ?>"/></td>
                                    </tr>
                                </table>
                            </fieldset>
                            <table>
                                <tr>
                                    <td width="100px">Jenis Sarana Angkut</td>
                                    <td>
                                        <?= form_dropdown('HEADER[MODA]', $MODA, $HEADER['MODA'], ' onchange="changeModa(this.value)" wajib="yes"  id="MODA" class="stext"'); ?> 
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style=" width: 50%">
                            <fieldset>
                                <legend>Perusahaan Pengangkut</legend>
                                <table width="100%">
                                    <tr>
                                        <td>ID</td>
                                        <td>
                                            <?= form_dropdown('HEADER[IDAGEN]', $IDAGEN, $HEADER['IDAGEN'], 'wajib="yes" id="IDAGEN" class="smtext" readonly' . $ro); ?>
                                            <input type="text" name="HEADER[IDEKS]" id="IDEKS" url="<?= site_url('referensi/autoComplate/EKSPORTIR/NPWPEKS'); ?>" urai="IDEKS;EKSNPWP;NAMAEKS;ALMTEKS" onfocus="Autocomp(this.id);
                                                    unformatNPWP(this.id);" onblur="formatNPWP(this.id);" value="<?= $this->fungsi->FORMATNPWP($HEADER['NPWPEKS']); ?>" class="offterus smtext" maxlength="15" wajib="yes" <?= $ro; ?>/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nama</td>
                                        <td><input type="text" name="KDAGEN" id="KDAGEN" value="<?= $HEADER['KDAGEN']; ?>" size="5" readonly/> <input type="text" name="NMAGEN" id="NMAGEN" value="<?= $HEADER['NMAGEN']; ?>" readonly/></td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top">Alamat</td>
                                        <td>
                                            <textarea name="ALAGEN" id="ALAGEN" readonly><?= $HEADER['ALAGEN']; ?></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100px">Pemberitahu </td>
                                        <td>
                                            <input type="text" name="PEMBERITAHU" id="PEMBERITAHU" value="<?= $HEADER['PEMBERITAHU']; ?>" readonly/>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                </table>        
                <table style=" width:100%">
                    <tr>
                        <td>
                            <fieldset>
                                <legend>Data Sarana Angkut</legend>
                                <table>
                                    <tr>
                                        <td>
                                            <table width="100%">
                                                <tr>
                                                    <td>Nama / Reg Angkut</td>
                                                    <td>
                                                        <input type="text" name="NMMODA" id="NMMODA" value="<?= $HEADER['NMMODA']; ?>" url="<?= site_url('referensi/autoComplate/MODA/NMMODA'); ?>" urai="REGMODA;NMMODA;FLAGMODA;URFLAGMODA" onfocus="Autocomp(this.id);"/>                                                        
                                                        <input type="text" name="REGMODA" id="REGMODA" value="<?= $HEADER['REGMODA']; ?>" url="<?= site_url('referensi/autoComplate/MODA/REGMODA'); ?>" urai="NMMODA;REGMODA;FLAGMODA;URFLAGMODA" onfocus="Autocomp(this.id);" size="10"/></td>
                                                    <td>
                                                        &nbsp;
                                                        &nbsp;
                                                        &nbsp;
                                                        &nbsp;
                                                        &nbsp;
                                                        &nbsp;                                                
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="100px">Bendera </td>
                                                    <td>
                                                        <input type="text" name="HEADER[FLAGMODA]" id="FLAGMODA" value="<?= $HEADER['FLAGMODA']; ?>" class="sssstext" url="<?= site_url('referensi/autoComplate/NEGARA/KODE_NEGARA'); ?>" onfocus="Autocomp(this.id)" urai="ANGKUTFL;URANGKUTFL"/> 
                                                        <button onclick="tb_search('negara', 'FLAGMODA;URANGKUTFL', 'Kode Negara', 'bendera', 650, 400)" class="btn"> ... </button>
                                                        <span id="URFLAGMODA"><?= $HEADER['URFLAGMODA']; ?></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="100px">Kade </td>
                                                    <td>
                                                        <input type="text" name="HEADER[KADE]" id="KADE" value="<?= $HEADER['KADE']; ?>" url="<?= site_url('referensi/autoComplate/TIMBUN/KDGDG'); ?>" onfocus="Autocomp(this.id, $('#KDKPBC').val())" urai="TMPTBN;URTMPTBN" class="ssstext" wajib="yes" maxlength="4"/>&nbsp;
                                                        <button onclick="tb_search('timbun', 'TMPTBN;URTMPTBN', 'Kode Timbun', 'kade', 650, 400, 'KDKPBC;')" class="btn"> ... </button>
                                                        <span id="URTMPTBN"><?= $HEADER['URTMPTBN'] == '' ? $URTMPTBN : $HEADER['URTMPTBN']; ?></span>
                                                    </td>
                                                </tr>                     
                                            </table>
                                        </td>
                                        <td>
                                            <table width="100%" id="angkutanUdata">
                                                <tr>
                                                    <td width="100px">Draft Depan </td>
                                                    <td>
                                                        <input type="text" name="DRAFTDEPAN" id="DRAFTDEPAN" value="<?= $HEADER['DRAFTDEPAN']; ?>"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="100px">Draft Belakang </td>
                                                    <td>
                                                        <input type="text" name="DRAFTBELAKANG" id="DRAFTBELAKANG" value="<?= $HEADER['DRAFTBELAKANG']; ?>"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="100px">GT </td>
                                                    <td>
                                                        <input type="text" name="GT" id="GT" value="<?= $HEADER['GT']; ?>"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="100px">LOA </td>
                                                    <td>
                                                        <input type="text" name="LOA" id="LOA" value="<?= $HEADER['LOA']; ?>"/>
                                                    </td>
                                                </tr>          
                                            </table>
                                        </td>
                                    </tr>	 			
                                </table>	
                            </fieldset>
                        </td>                
                    </tr>
                </table>
                <table style=" width:100%">
                    <tr>
                        <td style=" width: 100%">
                            <fieldset>
                                <legend>Pelabuhan </legend>
                                <table>
                                    <tr>
                                        <td>
                                            <table width="100%">
                                                <tr>
                                                    <td width="100px">Asal / Muat</td>
                                                    <td width="75%">
                                                        <input type="text" name="HEADER[PELMUAT]" id="PELMUAT" value="<?= $HEADER['PELMUAT']; ?>" url="<?= site_url('referensi/autoComplate/PELABUHAN/KODE_PELABUHAN'); ?>" onfocus="Autocomp(this.id)" urai="PELMUAT;PELMUATUR" class="ssstext" wajib="yes" maxlength="5"/>&nbsp;
                                                        <button onclick="tb_search('pelabuhan', 'PELMUAT;PELMUATUR', 'Kode Pelabuhan', 'Muat', 650, 400)" class="btn"> ... </button>
                                                        <span id="PELMUATUR"><?= $HEADER['PELMUATUR'] == '' ? $PELMUATUR : $HEADER['PELMUATUR']; ?></span>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td width="100px">Transit Akhir </td>
                                                    <td>
                                                        <input type="text" name="HEADER[PELTRANSIT]" id="PELTRANSIT" url="<?= site_url('referensi/autoComplate/PELABUHAN/KODE_PELABUHAN'); ?>" onfocus="Autocomp(this.id)" urai="PELTRANSIT;PELTRANSITUR" class="ssstext" value="<?= $HEADER['PELTRANSIT']; ?>" maxlength="5"/>&nbsp; 
                                                        <button onclick="tb_search('pelabuhan', 'PELTRANSIT;PELTRANSITUR', 'Kode Pelabuhan', 'transit', 650, 400)" class="btn"> ... </button>
                                                        <span id="PELTRANSITUR"><?= $HEADER['PELTRANSITUR'] == '' ? $PELTRANSITUR : $HEADER['PELTRANSITUR']; ?></span></td>
                                                </tr>                                                           
                                            </table>
                                        </td>
                                        <td>
                                            <table width="100%">
                                                <tr>
                                                    <td width="100px">Bongkar </td>
                                                    <td>
                                                        <input type="text" name="HEADER[PELBONGKAR]" id="PELBONGKAR" value="<?= $HEADER['PELBONGKAR']; ?>" url="<?= site_url('referensi/autoComplate/PELABUHAN/KODE_PELABUHAN'); ?>" onfocus="Autocomp(this.id)" urai="PELBKR;PELBKRUR" class="ssstext" wajib="yes" maxlength="5"/>&nbsp; 
                                                        <button onclick="tb_search('pelabuhan', 'PELBKR;PELBKRUR', 'Kode Pelabuhan', 'bongkar', 650, 400)" class="btn" > ... </button>
                                                        <span id="PELBKRUR"><?= $HEADER['PELBKRUR'] == '' ? $PELBKRUR : $HEADER['PELBKRUR']; ?></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="100px">Selanjutnya </td>
                                                    <td width="75%">
                                                        <input type="text" name="HEADER[PELBERIKUT]" id="PELBERIKUT" value="<?= $HEADER['PELBERIKUT']; ?>" url="<?= site_url('referensi/autoComplate/PELABUHAN/KODE_PELABUHAN'); ?>" onfocus="Autocomp(this.id)" urai="PELBERIKUT;PELBERIKUTUR" class="ssstext" wajib="yes" maxlength="5"/>&nbsp;
                                                        <button onclick="tb_search('pelabuhan', 'PELBERIKUT;PELBERIKUTUR', 'Kode Pelabuhan', 'Selanjutnya', 650, 400)" class="btn"> ... </button>
                                                        <span id="PELBERIKUTUR"><?= $HEADER['PELBERIKUTUR'] == '' ? $PELBERIKUTUR : $HEADER['PELBERIKUTUR']; ?></span>
                                                    </td>
                                                </tr>                                               
                                            </table>
                                        </td>
                                    </tr>	 			
                                </table>	
                            </fieldset>
                        </td>                
                    </tr>
                </table>
                <table style=" width:100%">
                    <tr>
                        <td style=" width: 50%">
                            <fieldset>
                                <legend>Jadwal Kedatangan</legend>
                                <table width="100%">
                                    <tr>
                                        <td width="100px">Nomor Voy </td>
                                        <td>
                                            <input type="text" name="ETANOVOY" id="ETANOVOY" value="<?= $HEADER['ETANOVOY']; ?>"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100px">Tanggal </td>
                                        <td>
                                            <input type="text" name="ETATGVOY" id="ETATGVOY" value="<?= $HEADER['ETATGVOY']; ?>"/>
                                        </td>
                                        <td>Jam </td>
                                        <td>
                                            <input type="text" name="ETAWKVOY" id="ETAWKVOY" value="<?= $HEADER['ETAWKVOY']; ?>"/>
                                        </td>
                                    </tr>                           
                                </table>
                            </fieldset>                    
                        </td>
                        <td style=" width: 50%">
                            <fieldset>
                                <legend>Jadwal Keberangkatan</legend>
                                <table width="100%">
                                    <tr>
                                        <td width="100px">Nomor Voy </td>
                                        <td>
                                            <input type="text" name="ETDNOVOY" id="ETDNOVOY" value="<?= $HEADER['ETDNOVOY']; ?>"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100px">Tanggal </td>
                                        <td>
                                            <input type="text" name="ETDTGVOY" id="ETDTGVOY" value="<?= $HEADER['ETDTGVOY']; ?>"/>
                                        </td>
                                        <td>Jam </td>
                                        <td>
                                            <input type="text" name="ETDWKVOY" id="ETDWKVOY" value="<?= $HEADER['ETDWKVOY']; ?>"/>
                                        </td>
                                    </tr>                               
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</span>
<script>
    $(document).ready(function() {
        onoff(false);
    });
    function onoff(on) {

        if ($('#fbc10_ #CAR').val() == '' || $('#fbc10_ #act').val() == 'save') {
            $('#fbc10_ .outentry').hide();
        } else {
            $('#fbc10_ .outentry').show();
        }
        if (on) {
            $('#on').show();
            $('#off').hide();
            $("#fbc10_ :input").removeAttr("disabled");
            $('#fbc10_ .outentry :input').attr('disabled', 'disabled');
            $("#tabs").tabs({disabled: [1, 2]});
        } else {
            $('#on').hide();
            $('#off').show();
            $("#fbc10_ :input").attr('disabled', 'disabled');
            $('#fbc10_ .outentry :input').removeAttr("disabled");
            $("#tabs").tabs();
            if ($("#act").val() == 'save') {
                $("#tabs").tabs({disabled: [1]});
            } else {
                $("#tabs").tabs({disabled: false});
            }
        }
        $('.onterus').removeAttr('disabled');
        $('.offterus :input').attr('readonly', 'readonly');
    }
    function changeModa(val) {
        if (val == '4') {
            $('#angkutanUdata').hide();
        } else {
            $('#angkutanUdata').show();
        }
    }
</script>