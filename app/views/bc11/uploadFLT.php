<form id="fuploadFLT" method="post" autocomplete="off" onsubmit="return false;" action="<?= site_url('bc20/uploadbup/upload'); ?>">
    <input type="hidden" name="act" id="act" value="<?= $act ?>" />
    <table width="100%">
        <tr>
            <td colspan="2">* file import dari modul manifest / entri versi dekstop.</td>
        </tr>
        <tr>
            <td width="50px">File FLT</td>
            <td>
                <input type="file" name="fileFLT" id="fileFLT" accept="File BackUp Modul Manifest atau Entry |*.FLT"/>
                <button class="btn" onclick="sendFile();">Preview</button>
                <span id="message"></span>
            </td>
            <td rowspan="3" style="float: right; vertical-align: bottom; width: 75px;"><button class="btn siap" onclick="importFLT();">Import</button>&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
        

    </table>
</form>
<hr style="margin-bottom: 5px" >
<form id="frmHeader" name="frmHeader" method="post" autocomplete="off" onsubmit="return false;" class="form">
    <table width="100%" border="0">
        <tr>
            <td width="55%" valign="top">
                <table width="100%" border="0">
                    <tr>
                        <td style="width:135px;">Jenis Manifest</td>
                        <td>
                            
                            <input type="radio" onclick="" name="HEADER[JNSMANIFEST]" id="JNSMANIFESTIN" value="I">
                            <label for="JNSMANIFESTIN">Inward</label>

                            <input type="radio" onclick="" name="HEADER[JNSMANIFEST]" id="JNSMANIFESTOUT" value="O"> 
                            <label for="JNSMANIFESTOUT">Outward</label>
                        </td>
                    </tr>
                    <tr>
                        <td>Jenis Moda</td>
                        <td>
                            <?= form_dropdown('HEADER[MODA]', $MODA, $HEADER['MODA'], 'id="MODA" class="lmtext" '); ?>
                        </td>
                    </tr>
                    <tr>
                        <td width="125px">KPBC Pendaftaran </td>
                        <td>
                            <input type="text" id="KPBC" class="ssstext" readonly="readonly"/> 
                            <span id="URKPBC"></span>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="vertical-align: top;">
                <fieldset id="GABUNGAN">
                    <legend>
                        <input type="checkbox" id="FLTGABUNGAN">
                        <label for="FLTGABUNGAN"> File Gabungan</label>
                    </legend>
                    <button onclick="getCar();" class="btn"> No. Aju </button> <span id="URCAR"></span>
                    <input type="hidden" id="CAR" value=""> 
                </fieldset>
            </td>
        </tr>
    </table>
    <fieldset>
        <legend>Perusahaan Pengangkut</legend>
        <table width="100%">
            <tr>
                <td width="125px">Identitas</td>
                <td>
                    <?= form_dropdown('', $IDPGUNA, $DATA['IDPGUNA'], 'id="IDPGUNA" class="mtext" '); ?>
                    <input type="text" id="NPWPPGUNA" value="<?= $DATA['NPWPPGUNA'] ?>" class="smtext" readonly="readonly"/>
                </td>
            </tr>
            <tr>
                <td>Nama</td>
                <td>
                    <input type="text" id="KODEAGEN" value="<?= $DATA['KODEAGEN']; ?>" class="ssstext" readonly="readonly"/>
                    <input type="text" id="NAMAPGUNA" value="<?= $DATA['NAMAPGUNA']; ?>" class="ltext" readonly="readonly"/>
                </td>
            </tr>
            <tr>
                <td style="vertical-align: top">Alamat</td>
                <td>
                    <textarea id="ALMTGUNA" cols="36" rows="1" readonly="readonly"><?= $DATA['ALMTGUNA']; ?></textarea>
                </td>
            </tr>
        </table>
    </fieldset>
    <fieldset>
        <legend>Sarana Pengangkut</legend>
        <table width="100%">
            <tr>
                <td width="125px">Nama Sarana Angkut</td>
                <td>
                    <input type="text" id="NMANGKUT" class="vltext" readonly="readonly"/>
                    <input type="text" id="REGKAPAL" class="ssstext" readonly="readonly"/>
                </td>
            </tr>
            <tr>
                <td style="vertical-align: top">Bendera</td>
                <td>
                    <input type="text" id="VOYFLAG" class="sssstext" readonly="readonly"/>
                    <span id="URVOYFLAG"></span>
                </td>
            </tr>
            <tr>
                <td>No Voyage / Flight</td>
                <td><input type="text" id="NOVOY" value="" class="sstext" readonly="readonly"/></td>
            </tr>
            <tr>
                <td style="vertical-align: top">Tanggal / Jam <span id="URTIBABERANGKAT">Jalan</span></td>
                <td>
                    <input type="text" id="TGTIBABERANGKAT" class="sstext" formatDate="yes" readonly="readonly"/>
                    <input type="text" id="JAMTIBABERANGKAT" class="ssstext" readonly="readonly"/>
                </td>
            </tr>
        </table>
    </fieldset>
    <fieldset>
        <legend> Pelabuhan </legend>
        <table width="100%">
            <tr>
                <td width="50%">
                    <table width="100%">
                        <tr>
                            <td width="125px">Asal / Muat</td>
                            <td>
                                <input type="text" id="PELMUAT" class="ssstext" readonly="readonly"/>
                                <span id="URPELMUAT"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>Sebelum / Transit Akhir</td>
                            <td>
                                <input type="text" id="PELTRANSIT" class="ssstext" readonly="readonly"/>
                                <span id="URPELTRANSIT"></span>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table width="100%">
                        <tr>
                            <td width="115px">Bongkar</td>
                            <td>
                                <input type="text" id="PELBKR" class="ssstext" readonly="readonly"/>
                                <span id="URPELBKR"></span></td>
                        </tr>
                        <tr>
                            <td>Berikutnya</td>
                            <td>
                                <input type="text" id="PELNEXT" class="ssstext" readonly="readonly"/>
                                <span id="URPELNEXT"></span></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </fieldset>
</form>
<script>
    $(document).ready(function() {
        $('.siap').hide();
        $('#frmHeader #GABUNGAN :input').attr('disabled', 'disabled');
    });
    function sendFile(){
        $('.siap').hide();
        $('#frmHeader #GABUNGAN :input').attr('disabled', 'disabled');
        if($('#fileFLT').val()!==''){
            var file = document.getElementById('fileFLT');
            var formData = new FormData();
            formData.append('fileFLT', file.files[0]);
            jloadings();
            $.ajax({
                url: site_url + '/bc11/uploadFLT/upload',
                data : formData,
                type:'POST',
                processData: false,
                contentType: false,
                success: function (response) {
                    Clearjloadings();
                    var arrRespons = response.split('~|~');
                    if(arrRespons[0].trim() == 'OKE'){
                        var input = arrRespons[1].split('|-|');
                        for (var i = 0; i < (input.length); i++){
                            var comp = input[i].split('|;|');
                            if (typeof($("#" + comp[0]).get(0)) == "undefined"){
                                return false;
                                break;
                            }
                            var tipe = $("#" + comp[0]).get(0).tagName;
                            if (tipe === 'INPUT' || tipe === 'SELECT') {
                                $("#" + comp[0]).val(comp[1]);
                            } else {
                                $("#" + comp[0]).html(comp[1]);
                            }
                        }
                        $("#message").html('Preview data header.');
                        $('.siap').show();
                        $('#frmHeader #GABUNGAN :input').removeAttr("disabled");
                    }else{
                        $("#message").html(arrRespons[1]);
                    }
                }
            });
        }
    }
    
    function getCar(){
        if ($('#FLTGABUNGAN').is(':checked')) {
            tb_search('carBC10', 'URCAR;KPBC;URKPBC;IDPGUNA;NPWPPGUNA;KODEAGEN;NAMAPGUNA;ALMTGUNA;NMANGKUT;REGKAPAL;VOYFLAG;URVOYFLAG;NOVOY;TGTIBABERANGKAT;JAMTIBABERANGKAT;PELMUAT;URPELMUAT;PELTRANSIT;URPELTRANSIT;PELBKR;URPELBKR;PELNEXT;URPELNEXT;CAR', 'Manifest yang telah terbentuk.', 'frmHeader', 750, 500)
        }
    }
    
    function importFLT(){
        var jnsImport = '';
        var Car = $('#CAR').val();
        if($('#FLTGABUNGAN').is(':checked')){
            if(Car != ''){
                jnsImport = 'gabungan';
            }else{
                jConfirm('Nomor Pengajuan belum dipilih.',site_name);
                return false;
            }
        }else{
            jnsImport = 'baru';
        }
        jloadings();
        $.ajax({
            url: site_url + '/bc11/insertFLT',
            type:'POST',
            timeout: 60000,
            cache: false,
            data: {
                keyoke: $.now(),
                car: Car,
                jnsImport:jnsImport
            },
            success: function(data) {
                Clearjloadings();
                jAlert('Load File Berhasil', site_name);
                closedialog('msgbox');
            }
        });
    }
</script>