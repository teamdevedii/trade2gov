<?php if (!defined('BASEPATH')){exit('No direct script access allowed');} 
    $arrMBL[$DETIL['MBL']] = 'checked="checked"';
?>
<fieldset>
    <form id="frmDetil" nama='frmDetil' action="<?= site_url('bc11/setDetil/') ?>" onsubmit="return false;" method="post" autocomplete="off">
        <?= $MAXLENGTH; ?>
        <input type='hidden' name="act" value="<?= $act; ?>" />
        <input type='hidden' name="DETIL[CAR]" id="CAR" value="<?= $DETIL['CAR']; ?>" />
        <input type='hidden' name="DETIL[KDGRUP]" id="KDGRUP" value="<?= $DETIL['KDGRUP']; ?>" />
        <input type='hidden' name="DETIL[EXPORT]" id="EXPORT" value="<?= $DETIL['EXPORT']; ?>" />
        <input type='hidden' id="STATUSHDR" value="<?= $STATUSHDR; ?>" />
        <table class="tabelclrajax" width="100%" cellpadding="0px" cellspacing="0px">
            <tr>
                <td colspan="2">
                    <button onclick="save_post_msg('frmDetil','','flstDetil_list');$('#flstDetil_form').html('');" class="btn"> Simpan </button>
                    <button onclick="$('#flstDetil_form').html('');" class="btn"> Batal </button>
                    <span style="float: right;">
                        <button id="btnValidasi" class="btn onterus">
                            <span class="icon-check"></span> StatusSSS [ <span id="URSTATUS"><?= $DETIL['URDTLOK']; ?></span> ]
                        </button>
                    </span>
                    <hr style="margin: 4px;">
                </td>
            </tr>
            <tr>
                <td width="50%">
                    <table width="100%" cellpadding="0px" cellspacing="0px">
                        <tr>
                            <td width="125px">Nomor Pos</td>
                            <td>
                                <input type="text" name="DETIL[NOPOS]" id="NOPOS" value="<?= $DETIL['NOPOS']; ?>" class="ssstext" readonly="readonly"/> -
                                <input type="text" name="DETIL[NOPOSSUB]" id="NOPOSSUB" value="<?= $DETIL['NOPOSSUB']; ?>" class="ssstext" readonly="readonly"/> -
                                <input type="text" name="DETIL[NOPOSSUBSUB]" id="NOPOSSUBSUB" value="<?= $DETIL['NOPOSSUBSUB']; ?>" class="ssstext" readonly="readonly"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Nomor / Tgl BL / AWB</td>
                            <td>
                                <input type="text" name="DETIL[NOBL]" id="NOBL" value="<?= $DETIL['NOBL']; ?>" class="mtext"/> -
                                <input type="text" name="DETIL[TGBL]" id="TGBL" value="<?= $DETIL['TGBL']; ?>" class="sstext" formatDate="yes"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Mother Vessel</td>
                            <td>
                                <input type="text" name="DETIL[MOTHERVESSEL]" id="MOTHERVESSEL" value="<?= $DETIL['MOTHERVESSEL']; ?>" class="vltext"/>
                            </td>
                        </tr>
                        <?= (trim($DATADOKUMEN)==='')?'':'<tr><td colspan="2" class="dataReferensi">Dokumen :<br>' . $DATADOKUMEN . '</td></tr>'; ?>
                    </table>
                </td>
                <td>
                    <fieldset>
                        <legend>
                            <input type="checkbox" id="MBL" name="DETIL[MBL]" value="Y" <?= $arrMBL['Y'] ?> onclick="checkMBL();"> 
                            <label for="MBL"> Informasi Master BL / AWB &nbsp;</label>
                        </legend>
                        <table width="100%" cellpadding="0px" cellspacing="0px">
                            <tr>
                                <td width="125px">No / Tgl Master</td>
                                <td>
                                    <input type="text" name="DETIL[MNOBL]" id="MNOBL" value="<?= $DETIL['MNOBL']; ?>" class="mtext"/> /
                                    <input type="text" name="DETIL[MTGBL]" id="MTGBL" value="<?= $DETIL['MTGBL']; ?>" class="sstext" formatDate="yes"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Total Kemasan</td>
                                <td>
                                    <input type="text" name="DETIL[MJMLKEMAS]" id="MJMLKEMAS" value="<?= number_format($DETIL['MJMLKEMAS']); ?>" class="sstext" numeral="yes" format="0,0" style="text-align: right;"/>
                                    <input type="text" name="DETIL[MJNSKEMAS]" id="MJNSKEMAS" value="<?= $DETIL['MJNSKEMAS']; ?>" onblur="checkCode(this);" grp="kemasan" urai="URMJNSKEMAS" class="sssstext"/>
                                    <button onclick="tb_search('kemasan', 'MJNSKEMAS;URMJNSKEMAS', 'Kode Kemasan', this.form.id, 700, 420)" class="btn">...</button>
                                    <span id="URMJNSKEMAS"><?= $DETIL['URMJNSKEMAS']; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Total Kontainer</td>
                                <td>
                                    <input type="text" name="DETIL[MJMLCONT]" id="MJMLCONT" value="<?= number_format($DETIL['MJMLCONT']); ?>" class="sstext" numeral="yes" format="0,0" style="text-align: right;"/>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <fieldset>
                        <legend> Pelabuhan </legend>
                        <table width="100%">
                            <tr>
                                <td width="50%">
                                    <table width="100%">
                                        <tr>
                                            <td width="125px">Asal / Muat</td>
                                            <td><input type="text" id="DPELASAL" name="DETIL[DPELASAL]" value="<?= $DETIL['DPELASAL'] ?>" onblur="checkCode(this);" grp="pelabuhan" urai="URDPELASAL" class="ssstext"/> <button onclick="tb_search('pelabuhan', 'DPELASAL;URDPELASAL', 'Pelabuhan Muat', this.form.id, 650, 400)" class="btn"> ... </button> <span id="URDPELASAL"><?= $DETIL['URDPELASAL']; ?></span> </td>
                                        </tr>
                                        <tr>
                                            <td>Sebelum / Transit Akhir</td>
                                            <td><input type="text" id="DPELSEBELUM" name="DETIL[DPELSEBELUM]" value="<?= $DETIL['DPELSEBELUM'] ?>" onblur="checkCode(this);" grp="pelabuhan" urai="URDPELSEBELUM" class="ssstext"/> <button onclick="tb_search('pelabuhan', 'DPELSEBELUM;URDPELSEBELUM', 'Pelabuhan Transit', this.form.id, 650, 400)" class="btn"> ... </button> <span id="URDPELSEBELUM"><?= $DETIL['URDPELSEBELUM']; ?></span></td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td width="125px">Bongkar</td>
                                            <td><input type="text" id="DPELBONGKAR" name="DETIL[DPELBONGKAR]" value="<?= $DETIL['DPELBONGKAR'] ?>" onblur="checkCode(this);" grp="pelabuhan" urai="URDPELBONGKAR" class="ssstext"/> <button onclick="tb_search('pelabuhan', 'DPELBONGKAR;URDPELBONGKAR', 'Pelabuhan Bongkar', this.form.id, 650, 400)" class="btn"> ... </button> <span id="URDPELBONGKAR"><?= $DETIL['URDPELBONGKAR']; ?></span></td>
                                        </tr>
                                        <tr>
                                            <td>Berikutnya</td>
                                            <td><input type="text" id="DPELLANJUT" name="DETIL[DPELLANJUT]" value="<?= $DETIL['DPELLANJUT'] ?>" onblur="checkCode(this);" grp="pelabuhan" urai="URDPELLANJUT" class="ssstext"/> <button onclick="tb_search('pelabuhan', 'DPELLANJUT;URDPELLANJUT', 'Pelabuhan Berikutnya', this.form.id, 650, 400)" class="btn"> ... </button> <span id="URDPELLANJUT"><?= $DETIL['URDPELLANJUT']; ?></span></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <fieldset>
                        <legend> Parties </legend>
                        <table width="100%" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="50%">
                                    <table width="100%" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="125px">Nama Pengirim</td>
                                            <td>
                                                <input type="text" name="DETIL[NMPENGIRIM]" id="NMPENGIRIM" value="<?= $DETIL['NMPENGIRIM']; ?>" class="lmtext"/>
                                                <button onclick="tb_search('partiesPengirim', 'NMPENGIRIM;ALPENGIRIM', 'Pengirim Barang', this.form.id, 650, 400)" class="btn"> ... </button>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="50%">
                                    <table width="100%" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="125px">Alamat Pengirim</td>
                                            <td><textarea cols="28" rows="1" name="DETIL[ALPENGIRIM]" id="ALPENGIRIM"><?= $DETIL['ALPENGIRIM']; ?></textarea></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="50%">
                                    <table width="100%"cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="125px">Nama Penerima</td>
                                            <td><input type="text" name="DETIL[NMPENERIMA]" id="NMPENERIMA" value="<?= $DETIL['NMPENERIMA']; ?>" class="lmtext"/>
                                            <button onclick="tb_search('partiesPenerima', 'NMPENERIMA;ALPENERIMA', 'Penerima Barang', this.form.id, 650, 400)" class="btn"> ... </button>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="50%">
                                    <table width="100%" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="125px">Alamat Penerima</td>
                                            <td><textarea cols="28" rows="1" name="DETIL[ALPENERIMA]" id="ALPENERIMA"><?= $DETIL['ALPENERIMA']; ?></textarea></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="50%">
                                    <table width="100%" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="125px">Nama Pemberitahu</td>
                                            <td><input type="text" name="DETIL[NMPEMBERITAU]" id="NMPEMBERITAU" value="<?= $DETIL['NMPEMBERITAU']; ?>" class="lmtext"/>
                                            <button onclick="tb_search('partiesPemberitahu', 'NMPEMBERITAU;ALPEMBERITAU', 'Pemberitahu', this.form.id, 650, 400)" class="btn"> ... </button>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="50%">
                                    <table width="100%" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="125px">Alamat Pemberitahu</td>
                                            <td><textarea cols="28" rows="1" name="DETIL[ALPEMBERITAU]" id="ALPEMBERITAU"><?= $DETIL['ALPEMBERITAU']; ?></textarea></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <fieldset>
                        <legend> Mark </legend>
                        <table width="100%" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="50%">
                                    <table width="100%" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="125px">Merk Kemasan</td>
                                            <td>
                                                <textarea cols="28" rows="1" name="DETIL[MERKKEMAS]" id="MERKKEMAS" ><?= $DETIL['MERKKEMAS'] ?></textarea></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" class="dataReferensi">
                                                <?= (trim($DATABARANG)==='')?'':'<b>Uraian Barang</b><br>' . $DATABARANG; ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="50%">
                                    <fieldset>
                                        <table width="100%">
                                            <tr>
                                                <td width="110px">Jml / Jns Kemasan</td>
                                                <td>
                                                    <input type="text" name="DETIL[JMLKEMAS]" id="JMLKEMAS" value="<?= number_format($DETIL['JMLKEMAS']); ?>" class="sstext" numeral="yes" format="0,0" style="text-align: right;"/>
                                                    <input type="text" name="DETIL[JNSKEMAS]" id="JNSKEMAS" value="<?= $DETIL['JNSKEMAS']?>" class="sssstext" onblur="checkCode(this);" grp="kemasan" urai="URJNSKEMAS">
                                                    <button onclick="tb_search('kemasan', 'JNSKEMAS;URJNSKEMAS', 'Kode Kemasan', this.form.id, 700, 420)" class="btn">...</button>
                                                    <span id="URJNSKEMAS"><?= $DETIL['URJNSKEMAS']?></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Total Bruto</td>
                                                <td>
                                                    <input type="text" name="DETIL[BRUTO]" id="BRUTO" value="<?= number_format($DETIL['BRUTO'],2); ?>" class="sstext" numeral="yes" format="0,0.00" style="text-align: right;"/> Kgm
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Total Volume</td>
                                                <td>
                                                    <input type="text" name="DETIL[VOLUME]" id="VOLUME" value="<?= number_format($DETIL['VOLUME'] , 2); ?>" class="sstext" numeral="yes" format="0,0.00" style="text-align: right;"/> m&#179;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="dataReferensi">
                                                    <?= (trim($DATAKONTAINER)==='')?'':'<b>Data Kontainer</b>' . $DATAKONTAINER; ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </form>
</fieldset>
<script>
    $(document).ready(function() {
        //onoff(false);
        formatNumeral('frmDetil');
        declareDP('frmDetil');
        f_MAXLENGTH('frmDetil');
        checkMBL();
        $('#fbarang_list').show();
        if($('#STATUSHDR').val() !== '' && $('#STATUSHDR').val() !== '00' && $('#STATUSHDR').val() !== '01'){
            frmDisable();
        }
    });
    function checkMBL(){
        var acc = true;
        if($('#MBL').is(':checked')){
            acc = false;
        }
        $('#MNOBL').attr('disabled',acc);
        $('#MTGBL').attr('disabled',acc);
        $('#MJMLKEMAS').attr('disabled',acc);
        $('#URMJMLKEMAS').attr('disabled',acc);
        $('#MJNSKEMAS').attr('disabled',acc);
        $('#MJMLCONT').attr('disabled',acc);
        $('#URMJMLCONT').attr('disabled',acc);
    }
    
    function frmDisable(){
        $("#frmDetil :input").attr('disabled','disabled');
        $('#frmDetil div').addClass('disabled');
        $("#frmDetil .dataReferensi :input").removeAttr("disabled");
    }
</script>