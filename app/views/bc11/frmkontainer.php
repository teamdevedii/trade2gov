<?php if (!defined('BASEPATH'))
exit('No direct script access allowed'); 

?>
<form id="frmkontainer" name="frmkontainer" action="<?= site_url('bc11/setKontainer'); ?>" onsubmit="return false;" autocomplete="off" >
    <input type="hidden" name="act" value="<?= $act; ?>" />
    <input type="hidden" name="KONTAINER[CAR]" id="CAR" value="<?= $KONTAINER['CAR']; ?>" />
    <input type="hidden" name="KONTAINER[NOPOS]" id="NOPOS" value="<?= $KONTAINER['NOPOS']; ?>" />
    <input type="hidden" name="KONTAINER[NOPOSSUB]" id="NOPOSSUB" value="<?= $KONTAINER['NOPOSSUB']; ?>" />
    <input type="hidden" name="KONTAINER[NOPOSSUBSUB]" id="NOPOSSUBSUB" value="<?= $KONTAINER['NOPOSSUBSUB']; ?>" />
    <input type="hidden" name="KONTAINER[KDGRUP]" id="KDGRUP" value="<?= $KONTAINER['KDGRUP']; ?>" />   
    <input type="hidden" name="KONTAINER[SERI]" id="SERI" value="<?= $KONTAINER['SERI']; ?>" /> 
    <fieldset>
        <table cellspacing="0">
            <tr>
                <td style="width: 120px" >Nomor </td>
                <td>: <input type="text" name="KONTAINER[NOCONT]" id="NOCONT" class="mtext" value="<?= $KONTAINER['NOCONT']; ?>" maxlength="30"/></td>
            </tr>
            <tr>
                <td>Ukuran </td>
                <td>: <?= form_dropdown('KONTAINER[UKCONT]', $UKURAN_CONTAINER, $KONTAINER['UKCONT'], 'id="UKURAN" class="sstext" wajib="yes" '); ?></td>
            </tr>
            <tr>
                <td>Tipe </td>
                <td>: <?= form_dropdown('KONTAINER[TIPECONT]', $JENIS_CONTAINER, $KONTAINER['TIPECONT'], 'id="TIPE" class="sstext" wajib="yes" '); ?></td>
            </tr>
            <tr>
                <td>SealNumber </td>
                <td>: <input type="text" name="KONTAINER[SEALNUM]" id="SEALNUM" class="smtext" value="<?= $KONTAINER['SEALNUM']; ?>" maxlength="15"/></td>
            </tr>
<?php if(strpos('|01|00|', $STATUSHDR)){ ?>
            <tr>
                <td colspan="3">
                    <button onclick="save_post_msg('frmkontainer', '', 'fkontainer_list');$('#fkontainer_form').html('');" class="btn">Simpan</button>
                    <button onclick="cancel('frmkontainer');$('#fkontainer_form').html('');" class="btn">Batal</button>
                </td>
            </tr>
<?php }?>
        </table>
    </fieldset>
</form>
<script>
    $(document).ready(function(){
        $('#fkontainer_form').show();
    });
</script>