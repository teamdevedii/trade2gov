<?php
if (!defined('BASEPATH')){exit('No direct script access allowed');}
?>
<span id="DivHeaderForm">
    <form id="frmHeader" name="frmHeader" action="<?= site_url('bc11/setHeader'); ?>" method="post" autocomplete="off" onsubmit="return false;" class="form">
        <?= $MAXLENGTH ?>
        <input type="hidden" name="act" id="act" value="<?= $act; ?>" />
        <input type="hidden" name="HEADER[CAR]" id="CAR" value="<?= $HEADER['CAR']; ?>" />
        <input type="hidden" id="STATUS" value="<?= $HEADER['STATUS']; ?>" />
        <h4>
            <span id="off">
                <button id="" onclick="onoff(true); return false;" class="btn onterus"><span class="icon-pen"></span> Edit </button>
            </span>
            <span id="on">
                <button onclick="save_header('frmHeader');" class="btn onterus">
                    <span class="icon-download"></span> Simpan
                </button>
                <button onclick="onoff(false);" class="btn onterus">
                    <span class="icon-undo"></span> Batal
                </button>
            </span>
            <?php if (strpos('|01|02|05|', $HEADER['STATUS'])) { ?>
                <button id="btnQUEUED" onclick="gotoQUEUED($('#CAR').val())" class="btn onterus" style="float: right; margin-right: 5px;" ><span class="icon-mail"></span> Bentuk / Batal EDIFACT</button>
            <?php } ?>
        </h4>
        <table width="100%" border="0">
            <tr>
                <td width="50%" valign="top">
                    <table width="100%" border="0">
<?php if ($HEADER['CAR']) { ?>
                            <tr>
                                <td height="25px;">Nomor Aju</td>
                                <td><b><?= substr($HEADER['CAR'], 0, 6) . '-' . substr($HEADER['CAR'], 6, 6) . '-' . substr($HEADER['CAR'], 12, 8) . '-' . substr($HEADER['CAR'], 20, 6); ?></b></td>
                            </tr>
<?php } ?>
                        <tr>
                            <td width="125px">KPBC Pendaftaran </td>
                            <td>
                                <input type="text"  name="HEADER[KPBC]" id="KPBC" value="<?= $HEADER['KPBC']; ?>" class="ssstext" onblur="checkCode(this);" grp="kpbc" urai="URKPBC"/> &nbsp; <button onclick="tb_search('kpbc', 'KPBC;URKPBC', 'Kode Kpbc', this.form.id, 650, 400);" class="btn"> ... </button> <span id="URKPBC"><?= $HEADER['URKPBC']; ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td>Jenis Manifest</td>
                            <td>
                                <?php $arr[$HEADER['JNSMANIFEST']] = 'checked="checked"'; ?>
                                <input type="radio" onclick="" name="HEADER[JNSMANIFEST]" id="JNSMANIFESTIN" value="I" <?= $arr['I'] ?>>
                                <label for="JNSMANIFESTIN">Inward</label>

                                <input type="radio" onclick="" name="HEADER[JNSMANIFEST]" id="JNSMANIFESTOUT" value="O" <?= $arr['O'] ?>> 
                                <label for="JNSMANIFESTOUT">Outward</label>
                            </td>
                        </tr>
                        <tr>
                            <td>Tipe Manifest</td>
                            <td>
                                <?php $arr[$HEADER['TIPEMANIFEST']] = 'checked="checked"'; ?>
                                <input type="radio" onclick="" name="HEADER[TIPEMANIFEST]" id="TIPEMANIFEST0" value="0" <?= $arr['0'] ?>>
                                <label for="TIPEMANIFEST0">Biasa</label>

                                <input type="radio" onclick="" name="HEADER[TIPEMANIFEST]" id="TIPEMANIFEST1" value="1" <?= $arr['1'] ?>>
                                <label for="TIPEMANIFEST1">FTZ</label>

                                <input type="radio" onclick="" name="HEADER[TIPEMANIFEST]" id="TIPEMANIFEST2" value="2" <?= $arr['2'] ?>>
                                <label for="TIPEMANIFEST2">Gabungan</label>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="vertical-align: top;">
<?php if ($HEADER['CAR']) { ?>
                    <table width="100%">
                        <tr>
                            <td style="text-align: right;" >
                                <button id="btnValidasi" onclick="cekValidasi($('#CAR').val())" class="btn onterus"><span class="icon-check"></span> Validasi Data Manifest [ <span id="URSTATUS"><?= $HEADER['URSTATUS']; ?></span> ] </button>
                            </td>
                        </tr>
                    </table><br>
<?php } ?>
                    
                </td>
            </tr>
        </table>
        <div class="judul">Data Manifest :</div>
        <table width="100%" border="0" >
            <tr>
                <td width="50%" valign="top">
                    <fieldset>
                        <legend>Perusahaan Pengangkut</legend>
                        <table width="100%">
                            <tr>
                                <td width="125px">Identitas</td>
                                <td>
                                    <?= form_dropdown('HEADER[IDPGUNA]', $IDPGUNA, $HEADER['IDPGUNA'], 'id="IDPGUNA" class="mtext" '); ?>
                                    <input type="text" name="HEADER[NPWPPGUNA]" id="NPWPPGUNA" value="<?= $HEADER['NPWPPGUNA'] ?>" class="offterus smtext" readonly="readonly"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Nama</td>
                                <td>
                                    <input type="text" name="HEADER[KODEAGEN]" id="KODEAGEN" value="<?= $HEADER['KODEAGEN']; ?>" class="ssstext" readonly="readonly" />
                                    <input type="text" name="HEADER[NAMAPGUNA]" id="NAMAPGUNA" value="<?= $HEADER['NAMAPGUNA']; ?>" class="ltext" readonly="readonly"/></td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top">Alamat</td>
                                <td>
                                    <textarea name="HEADER[ALMTGUNA]" id="ALMTGUNA" cols="36" rows="1" readonly="readonly"><?= $HEADER['ALMTGUNA']; ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td>Nama Nahkoda</td>
                                <td>
                                    <input type="text" name="HEADER[NAMANAHKODA]" id="NAMANAHKODA" value="<?= $HEADER['NAMANAHKODA']; ?>" class="ltext"/>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                    <table style="width:100%; padding-top:4px; padding:4px;">
                        <tr>
                            <td style="width:135px;">Jenis Moda</td>
                            <td>
                                <?= form_dropdown('HEADER[MODA]', $MODA, $HEADER['MODA'], 'id="MODA" class="lmtext" '); ?>
                            </td>
                        </tr>
                    </table>
                    <fieldset style=" margin-top: -4px;">
                        <legend>Sarana Pengangkut</legend>
                        <table width="100%">
                            <tr>
                                <td width="125px">Nama Sarana Angkut</td>
                                <td>
                                    <input type="text" name="HEADER[NMANGKUT]" id="NMANGKUT" value="<?= $HEADER['NMANGKUT'] ?>" class="vltext" url="<?= site_url('referensi/autoComplate/MODAMANIFEST/NMMODA'); ?>" urai="REGKAPAL;NMANGKUT;VOYFLAG;URVOYFLAG" onfocus="Autocomp(this.id,$('#MODA').val());"/>
                                    <input type="text" name="HEADER[REGKAPAL]" id="REGKAPAL" value="<?= $HEADER['REGKAPAL'] ?>" class="ssstext"/>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top">Bendera</td>
                                <td>
                                    <input type="text" name="HEADER[VOYFLAG]" id="VOYFLAG" value="<?= $HEADER['VOYFLAG']; ?>" class="sssstext" onblur="checkCode(this);" grp="negara" urai="URVOYFLAG"/> <button onclick="tb_search('negara', 'VOYFLAG;URVOYFLAG', 'Kode Negara', this.form.id, 650, 400)" class="btn"> ... </button> <span id="URVOYFLAG"><?= $HEADER['URVOYFLAG']; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>No Voyage / Flight</td>
                                <td><input type="text" name="HEADER[NOVOY]" id="NOVOY" value="<?= $HEADER['NOVOY']; ?>" class="sstext"/></td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top">Tanggal / Jam <span id="URTIBABERANGKAT">Jalan</span></td>
                                <td>
                                    <input type="text" name="HEADER[TGTIBABERANGKAT]" id="TGTIBABERANGKAT" value="<?= $HEADER['TGTIBABERANGKAT']; ?>" class="sstext" formatDate="yes"/>
                                    <input type="text" name="HEADER[JAMTIBABERANGKAT]" id="JAMTIBABERANGKAT" value="<?= $HEADER['JAMTIBABERANGKAT']; ?>" class="ssstext" onblur="formatTime(this.id)"/>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top">Kade</td>
                                <td>
                                    <input type="text" name="HEADER[KADE]" id="KADE" value="<?= $HEADER['KADE']; ?>" class="ssstext" onblur="checkCode(this);" grp="kade" urai="URKADE" nocomplate="true"/> <button onclick="tb_search('kade', 'KADE;URKADE', 'Kode KADE', this.form.id, 650, 400)" class="btn"> ... </button> <span id="URKADE"><?= $HEADER['URKADE']; ?></span>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
                <td width="50%" valign="top">
                    <fieldset>
                        <legend>Dokumen</legend>
                        <table width="100%">
                            <tr>
                                <td width="125px">No / Tgl BC 1.0</td>
                                <td>
                                    <input type="text" name="HEADER[NOBC10]" id="NOBC10" value="<?= $HEADER['NOBC10']; ?>" class="ssstext"/> / 
                                    <input type="text" name="HEADER[TGBC10]" id="TGBC10" value="<?= $HEADER['TGBC10']; ?>" class="sstext" formatDate="yes" />
                                </td>
                            </tr>
                            <tr>
                                <td>No / Tgl BC 1.1</td>
                                <td>
                                    <input type="text" name="HEADER[NOBC11]" id="NOBC11" value="<?= $HEADER['NOBC11']; ?>" class="ssstext" readonly="readonly"/> / 
                                    <input type="text" name="HEADER[TGBC11]" id="TGBC11" value="<?= $HEADER['TGBC11']; ?>" class="sstext" formatDate="yes" readonly="readonly"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <fieldset>
                                        <?php 
                                            $arr[$HEADER['KPPT']] = 'checked="checked"';
                                        ?>
                                        <legend>
                                            <input type="checkbox" id="KPPT" name="HEADER[KPPT]" <?= $arr['1'] ?> value="1" onclick="chkKPPT();"> 
                                            <label for="KPPT"> Manifest KPPT &nbsp;</label>
                                        </legend>
                                        <table>
                                            <tr>
                                                <td width="115px">No / Tgl BC 1.1 asal</td>
                                                <td>
                                                    <input type="text" name="HEADER[NOBC11ASAL]" id="NOBC11ASAL" value="<?= $HEADER['NOBC11ASAL']; ?>" class="ssstext" /> / 
                                                    <input type="text" name="HEADER[TGBC11ASAL]" id="TGBC11ASAL" value="<?= $HEADER['TGBC11ASAL']; ?>" class="sstext" formatDate="yes"/>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </td>
                            </tr>

                            
                        </table>
                    </fieldset>
                    <fieldset style="margin-top: 40px;">
                        <legend> Summary </legend>
                        <table width="100%">
                            <tr>
                                <td width="125px">Jumlah BL</td>
                                <td><input type="text" name="HEADER[HJMLBL]" value="<?= number_format($HEADER['HJMLBL']) ?>" class="numtext" readonly="readonly"/></td>
                                <td rowspan="5" style="text-align: center">
                                    Catatan<br><textarea cols="20" rows="3" name="HEADER[NOTE]" id="NOTE"><?= $HEADER['NOTE'] ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td>Jumlah Kemasan</td>
                                <td><input type="text" name="HEADER[HJMLKEMAS]" value="<?= number_format($HEADER['HJMLKEMAS']) ?>" class="numtext" readonly="readonly"/></td>
                            </tr>
                            <tr>
                                <td>Berat Kotor</td>
                                <td><input type="text" name="HEADER[HBRUTO]" value="<?= number_format($HEADER['HBRUTO'],4) ?>" class="numtext" readonly="readonly"/></td>
                            </tr>
                            <tr>
                                <td>Volume</td>
                                <td><input type="text" name="HEADER[HVOLUME]" value="<?= number_format($HEADER['HVOLUME'],4) ?>" class="numtext" readonly="readonly"/></td>
                            </tr>
                            <tr>
                                <td>Jumlah Kontainer</td>
                                <td><input type="text" name="HEADER[HJMLCONT]" value="<?= number_format($HEADER['HJMLCONT']) ?>" class="numtext" readonly="readonly"/></td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <fieldset>
                        <legend> Pelabuhan </legend>
                        <table width="100%">
                            <tr>
                                <td width="50%">
                                    <table width="100%">
                                        <tr>
                                            <td width="125px">Asal / Muat</td>
                                            <td><input type="text" id="PELMUAT" name="HEADER[PELMUAT]" value="<?= $HEADER['PELMUAT'] ?>" onblur="checkCode(this);" grp="pelabuhan" urai="URPELMUAT" class="ssstext"/> <button onclick="tb_search('pelabuhan', 'PELMUAT;URPELMUAT', 'Pelabuhan Muat', this.form.id, 650, 400)" class="btn"> ... </button> <span id="URPELMUAT"><?= $HEADER['URPELMUAT']; ?></span> </td>
                                        </tr>
                                        <tr>
                                            <td>Sebelum / Transit Akhir</td>
                                            <td><input type="text" id="PELTRANSIT" name="HEADER[PELTRANSIT]" value="<?= $HEADER['PELTRANSIT'] ?>" onblur="checkCode(this);" grp="pelabuhan" urai="URPELTRANSIT" class="ssstext"/> <button onclick="tb_search('pelabuhan', 'PELTRANSIT;URPELTRANSIT', 'Pelabuhan Transit', this.form.id, 650, 400)" class="btn"> ... </button> <span id="URPELTRANSIT"><?= $HEADER['URPELTRANSIT']; ?></span></td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td width="125px">Bongkar</td>
                                            <td><input type="text" id="PELBKR" name="HEADER[PELBKR]" value="<?= $HEADER['PELBKR'] ?>" onblur="checkCode(this);" grp="pelabuhan" urai="URPELBKR" class="ssstext"/> <button onclick="tb_search('pelabuhan', 'PELBKR;URPELBKR', 'Pelabuhan Bongkar', this.form.id, 650, 400)" class="btn"> ... </button> <span id="URPELBKR"><?= $HEADER['URPELBKR']; ?></span></td>
                                        </tr>
                                        <tr>
                                            <td>Berikutnya</td>
                                            <td><input type="text" id="PELNEXT" name="HEADER[PELNEXT]" value="<?= $HEADER['PELNEXT'] ?>" onblur="checkCode(this);" grp="pelabuhan" urai="URPELNEXT" class="ssstext"/> <button onclick="tb_search('pelabuhan', 'PELNEXT;URPELNEXT', 'Pelabuhan Berikutnya', this.form.id, 650, 400)" class="btn"> ... </button> <span id="URPELNEXT"><?= $HEADER['URPELNEXT']; ?></span></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </form>
</span>
<script>
    $(document).ready(function() {
        chkKPPT();
        onoff(false);
        declareDP('frmHeader');
        f_MAXLENGTH('frmHeader');
        formatNPWP('frmHeader #NPWPPGUNA');
    });
    
    function chkKPPT(){
        if($('#KPPT').is(':checked')){
            $('#NOBC11ASAL').attr('readonly',false);
            $('#TGBC11ASAL').attr('readonly',false);
            $('#TGBC11ASAL').datepicker('enable');
        }else{
            $('#NOBC11ASAL').attr('readonly',true);
            $('#TGBC11ASAL').attr('readonly',true);
            $('#TGBC11ASAL').datepicker('disable');
        }
    }
    
    function onoff(on) {
        if($('#frmHeader #STATUS').val()=='00' || $('#frmHeader #STATUS').val()=='01' || $('#frmHeader #STATUS').val()=='05' || $('#frmHeader #STATUS').val()==''){
            if (on) {
                $('#on').show();
                $('#off').hide();
                $("#frmHeader :input").removeAttr("disabled");
                $('#frmHeader .outentry :input').attr('disabled', 'disabled');
                $("#tabs_bc11").tabs({disabled: [1, 2]});
            } else {
                $('#on').hide();
                $('#off').show();
                $("#frmHeader :input").attr('disabled', 'disabled');
                $('#frmHeader .outentry :input').removeAttr("disabled");
                $("#tabs_bc11").tabs();
                if ($("#act").val() == 'save') {
                    $("#tabs_bc11").tabs({disabled: [1]});
                } else {
                    $("#tabs_bc11").tabs({disabled: false});
                }
                cancel('frmHeader');
                formatTanggal('frmHeader');
            }
            $('.onterus').removeAttr('disabled');
            $('.offterus :input').attr('readonly', 'readonly');
        }else{
            $('#on').hide();
            $('#off').show();
            $("#frmHeader :input").attr('disabled','disabled');
            $('#frmHeader div').addClass('disabled');
        }
        if($('#frmHeader #STATUS').val()=='01' || $('#frmHeader #STATUS').val()=='02'){
            $('#frmHeader #btnQUEUED').show();
            $('#frmHeader #btnQUEUED').removeAttr("disabled");
        }else{
            $('#frmHeader #btnQUEUED').hide();
        }
    }

    function cekValidasi(car) {
        $(function() {
            $('#msgbox').html('');
            $("#msgbox").html('<div style="margin-left: 15px; margin-top: 15px;"><img src="' + base_url + 'img/loaders/facebook.gif" /> loading...</div>');
            $("#msgbox").dialog({
                resizable: false,
                height: 450,
                modal: true,
                width: 700,
                title: 'Validasi Dokumen',
                open: function() {
                    $.ajax({
                        type: 'POST',
                        url: site_url + '/bc11/cekKelengkapan/' + car ,
                        success: function(data) {
                            var arr = data.split('|');
                            $("#msgbox").html(arr[0]);
                            call(site_url + '/bc11/create/' + arr[1], '_content_');
                        }
                    });
                },
                buttons: {
                    Close: function() {
                        $(this).dialog("close");
                    }
                }
            });
        });
    }

    function gotoQUEUED() {
        jConfirm('Data akan dibentuk EDIFACT ?', site_name,
                function(r) {
                    if (r == true) {
                        jloadings();
                        $(function() {
                            $('#msgbox').html('');
                            $("#msgbox").html('<div style="margin-left: 15px; margin-top: 15px;"><img src="' + base_url + 'img/loaders/facebook.gif" /> loading...</div>');
                            $("#msgbox").dialog({
                                resizable: false,
                                height: 500,
                                modal: true,
                                width: 700,
                                title: 'Edifact / unEdifact',
                                open: function() {
                                    $.ajax({
                                        type: 'POST',
                                        url: site_url + '/bc11/queued',
                                        data: '&CAR=' + $('#CAR').val(),
                                        error: function() {
                                            Clearjloadings();
                                        },
                                        success: function(data) {
                                            Clearjloadings();
                                            $("#msgbox").html(data);
                                            call(site_url + '/bc11/create/' + $('#CAR').val(), '_content_');
                                        }
                                    });
                                },
                                buttons: {
                                    Close: function() {
                                        $(this).dialog("close");
                                    }
                                }
                            });
                        });
                    } else {
                        return false;
                    }
                });
    }

    function save_header(formid) {
        var dataSend = $('#' + formid).serialize();
        jConfirm('Anda yakin Akan memproses data ini?', site_name,
            function(r) {
                if (r == true) {
                    jloadings();
                    $.ajax({
                        type: 'POST',
                        url: $('#' + formid).attr('action'),
                        data: dataSend,
                        success: function(data) {
                            var arr = data.split('|');
                            $("#msgbox").dialog({
                                resizable: false,
                                height: 500,
                                modal: true,
                                width: 700,
                                title: 'Save Header Manifes',
                                open: function() {
                                    call(site_url + '/bc11/create/' + arr[1], '_content_');
                                    $(this).html(arr[0]);
                                },
                                buttons: {
                                    Close: function() {$(this).dialog("close");}
                                }
                            });
                            
                            
                            Clearjloadings();
                        }
                    });
                } else {
                    return false;
                }
            });
    }
</script>