<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<form id="frmbarang" name="frmbarang" action="<?= site_url('bc11/setBarang'); ?>" onsubmit="return false;" method="post" autocomplete="off" >
    <input type="hidden" name="act" value="<?= $act; ?>" />
    <input type="hidden" name="BARANG[CAR]" id="CAR" value="<?= $BARANG['CAR']; ?>" />
    <input type="hidden" name="BARANG[NOPOS]" id="NOPOS" value="<?= $BARANG['NOPOS']; ?>" />
    <input type="hidden" name="BARANG[NOPOSSUB]" id="NOPOSSUB" value="<?= $BARANG['NOPOSSUB']; ?>" />
    <input type="hidden" name="BARANG[NOPOSSUBSUB]" id="NOPOSSUBSUB" value="<?= $BARANG['NOPOSSUBSUB']; ?>" />
    <input type="hidden" name="BARANG[KDGRUP]" id="KDGRUP" value="<?= $BARANG['KDGRUP']; ?>" />   
    <input type="hidden" name="BARANG[SERI]" id="SERI" value="<?= $BARANG['SERI']; ?>" />
    <fieldset>
        <table cellspacing="5">
            <tr>
                <td>Kode HS :</td>
                <td>Deskripsi :</td>
            </tr>
            <tr>
                <td>
                    <input type="text" name="BARANG[HS]" id="HS" class="smtext" value="<?= $BARANG['HS']; ?>" maxlength="12" style="" wajib="yes"/>
                    <button onclick="tb_search('kodehs', 'HS;DESCRIPTION', 'Kode HS', this.form.id, 650, 400)" class="btn"> ... </button>
                </td>
                <td>
                    <textarea name="BARANG[DESCRIPTION]" id="DESCRIPTION" rows="2" cols="25"><?= $BARANG['DESCRIPTION']; ?></textarea>
                </td>
            </tr>
			
<?php if(strpos('|00|01|', $STATUSHDR)){ ?>
            <tr>
                <td colspan="2">
                    <button onclick="save_post_msg('frmbarang', '', 'fbarang_list');$('#fbarang_form').html('');" class="btn">Simpan</button>
                    <button onclick="cancel('frmbarang'); $('#frmbarang').html('');" class="btn">Batal</button>
                </td>
            </tr>
<?php }?>
        </table>
    </fieldset>
</form>
<script>
    $(document).ready(function(){
        $('#fbarang_form').show();
    });
</script>
