<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
?>
<div class="content_luar">
    <div class="content_dalam">
        <form id="cetak_" method="post" autocomplete="off" onsubmit="return false;"> 
            <input id="CAR" type="hidden" value="<?= $car ?>"/>
            <fieldset>
                <legend> Pilihan Pencetakan </legend>
                <table width="100%">
                    <tr>
                        <td>
                            <input type="radio" name="CETAK" id="semuaPos" value="1" checked> <label for="semuaPos">&nbsp;Semua Pos</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="radio" name="CETAK" id="posTertentu" value="2"> <label for="posTertentu">&nbsp;Pos Tertentu</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="radio" name="CETAK" id="Respon" value="3"> <label for="Respon">&nbsp;Respon</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br>
                            <button onclick="cetakNewWin($('#CAR').val());" class="btn" ><span class="icon-new-window"></span> Cetak </button>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
</div>
<script>
    function cetakNewWin(car){
        if($('input[name="CETAK"]:checked').val()=='1'){
            window.open(site_url + '/bc11/cetakDokumen/' + car + '/1','Cetak Dokumen Manifest','scrollbars=yes, resizable=yes,width=1100,height=700');
        }
        else if($('input[name="CETAK"]:checked').val()=='2'){
            Dialog(site_url + '/bc11/lstGroupPos/' + car, 'cetakDokumen', 'Pilih Pos yang akan dicetak.', 850, 500);
        }
        else if($('input[name="CETAK"]:checked').val()=='3'){
            Dialog(site_url + '/bc11/lstRespon/' + car, 'cetakDokumen', 'Pilih respon yang akan dicetak.', 850, 500);
        }
    }
</script>