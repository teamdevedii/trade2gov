<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);
//die($this->newsession->userdata('KODE_TIPE_USER').' - ' . $DATA['STATUS']);
?>	
<div class="content_luar">
    <div class="content_dalam">
        <form id="frmPerusahaan" name="frmPerusahaan" action="<?= site_url('usermanage/editPerusahaan') ?>" onsubmit="return false;" class="form uniformForm" method="post" autocomplete=off>
<?= $MAXLENGTH ?>
            <input type="hidden" id="KODE_TRADER" name="DATA[KODE_TRADER]" value="<?= $DATA['KODE_TRADER'] ?>">
            <input type="hidden" id="USER_ID" name="DATA[USER_ID]" value="<?= $DATA['USER_ID'] ?>">            
            <table width="100%" border="0">
                <tr>
                    <td width="50%" valign="top">
                        <table width="100%" border="0">
                            <tr>
                                <td colspan="2"><u><b>DATA PERUSAHAAN</b></u></td>
                            </tr>
                            <tr>
                                <td width="120px">Identitas *</td>
                                <td>: <?= form_dropdown('DATA[KODE_ID]', $JENIS_IDENTITAS, ($DATA['KODE_ID'] == '') ? '5' : $DATA['KODE_ID'], 'id="KODE_ID" wajib="yes" '); ?>&nbsp;<input type="text" name="DATA[ID]" id="ID" class="smtext" value="<?= $DATA['ID']; ?>" wajib="yes" onfocus="unformatNPWP(this.id);" onblur="formatNPWP(this.id);" wajib="yes" />
                                </td>
                            </tr>
                            <tr>
                                <td>Nama *</td>
                                <td>: <input type="text" name="DATA[NAMA]" id="NAMA" class="ltext" value="<?= $DATA['NAMA']; ?>" wajib="yes"/></td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">Alamat *</td>
                                <td><font style="vertical-align:top"> :</font> <textarea name="DATA[ALAMAT]" id="ALAMAT" class="ltext" wajib="yes"><?= $DATA['ALAMAT'] ?></textarea></td>
                            </tr>
                            <tr>
                                <td>Nomor Telepon *</td>
                                <td>: <input type="text" name="DATA[TELEPON]" id="TELEPON" class="smtext" value="<?= $DATA['TELEPON']; ?>" wajib="yes"/></td>
                            </tr>
                            <tr>
                                <td>Nomor Fax *</td>
                                <td>: <input type="text" name="DATA[FAX]" id="FAX" class="smtext" value="<?= $DATA['FAX']; ?>" wajib="yes"/></td>                            
                        </table>
                    </td>
                    <td valign="top">
                        <table width="100%" border="0">
                            <tr>
                                <td colspan="2">
                                    <u><b>DATA PEMILIK PERUSAHAAN</b></u></td>
                            </tr>
                            <tr>
                                <td width="120px">Nama *</td>
                                <td>: <input type="text" name="DATA[NAMA_PEMILIK]" id="NAMA_PEMILIK" class="mtext" value="<?= $DATA['NAMA_PEMILIK']; ?>" wajib="yes"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Tempat/Tanggal Lahir *</td>
                                <td>: <input type="text" name="DATA[TEMPAT_LAHIR_PEMILIK]" id="TEMPAT_LAHIR_PEMILIK" class="mtext" value="<?= $DATA['TEMPAT_LAHIR_PEMILIK']; ?>" wajib="yes"/>&nbsp;<input type="text" name="DATA[TANGGAL_LAHIR_PEMILIK]" id="TANGGAL_LAHIR_PEMILIK" class="sstext" formatDate="yes" value="<?= $DATA['TANGGAL_LAHIR_PEMILIK']; ?>" wajib="yes"/></td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">Alamat *</td>
                                <td><font style="vertical-align:top"> :</font> <textarea name="DATA[ALAMAT_PEMILIK]" id="ALAMAT_PEMILIK" class="ltext" wajib="yes"><?= $DATA['ALAMAT_PEMILIK']; ?></textarea></td>
                            </tr>                                                     
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button class="btn" onclick="save_post_msg('frmPerusahaan');"> &nbsp; Simpan &nbsp; </button>
                        <button class="btn" onclick="document.getElementById('frmPerusahaan').reset();"> &nbsp; Reset  &nbsp; </button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>

<script>
    $(document).ready(function() {       
        declareDP('frmPerusahaan');        
        f_MAXLENGTH('frmPerusahaan');         
    });
</script>