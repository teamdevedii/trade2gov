<?php
if (!defined('BASEPATH')){exit('No direct script access allowed');}
?>
<div class="content_luar">
    <div class="content_dalam" style="height: 100%;">
        <h4><?= $judul; ?></h4>
        <div style=" height: 300px; overflow-y: scroll;">
            <pre style="font:'Lucida Console', Monaco, monospace; font-size:10px; text-align: left; "><?= $data; ?></pre>
        </div>
    </div>
</div>