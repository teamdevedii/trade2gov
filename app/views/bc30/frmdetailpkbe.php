<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);
$ro = '';
if ($act == 'update')
    $ro = 'readonly';
?>	
<script>
    $(document).ready(function() {
        //onoff(false);
        declareDP('fdetailpkbe');
        f_bedaKPBC($('#fdetailpkbe #KDKTRPEB').val());
        f_MAXLENGTH('fdetailpkbe');
    });

    function kode(data) {
        if (data == 1) {
            $('.jnsdok').html('Nomor PEB');
        } else if (data == 2) {
            $('.jnsdok').html('Nomor Dokumen');
        }
    }

    function f_bedaKPBC(strVal) {
        if (strVal != $('#frmPKBEHdr #KDKTR').val()) {
            $('#identitas').show();
            //$("#identitas :input").prop("disabled", false);

        } else {
            $('#identitas').hide();
            //$("#identitas :input").prop("disabled", true);
            //$("#identitas :input").prop("disabled", true);
        }
    }

</script>
<div class="content_luar">
    <div class="content_dalam">
        <form name="fdetailpkbe" id="fdetailpkbe" action="<?= site_url('bc30/insert_detail_pkbe/proses'); ?>" method="post" autocomplete="off" onsubmit="return false;" >
            <?= $MAXLENGTH ?>
            <input type="hidden" name="act" id="act" value="<?= $act; ?>" readonly="readonly" />
            <input type="hidden" name="DETPKBE[CAR]" id="car" value="<?= $DETPKBE['CAR']; ?>" readonly="readonly" />
            <input type="hidden" name="DETPKBE[KDKTR]" id="car" value="<?= $DETPKBE['KDKTR']; ?>" readonly="readonly" />
            <input type="hidden" name="DETPKBE[SERICONT]" id="car" value="<?= $DETPKBE['SERICONT']; ?>" readonly="readonly" />
            <input type="hidden" name="DETPKBE[SERIPE]" id="car" value="<?= $DETPKBE['SERIPE']; ?>" readonly="readonly" />
            <h4><span class="info_2">&nbsp;</span><?= $judul; ?></h4>
            <table width="100%">
                <tr>
                    <td>
                        <fieldset>
                            <table width="100%">
                                <tr>
                                    <td>No. Container</td>
                                    <td>
                                        <input type="text" name="DETPKBE[NOCONT]" id="NOCONT" value="1"  maxlength="5" width="5" readonly <?= $ro; ?>/>
                                    </td>
                                    <td>PE ke-</td>
                                    <td>
                                        <input type="text" name="DETPKBE[PE]" id="PE" value="<?= $DETPKBE['SERIPE']; ?>"  maxlength="50" readonly <?= $ro; ?>/>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <table>
                                <tr>
                                    <td>Jenis Dokumen</td>
                                    <td> 
                                        <?= form_dropdown('DETPKBE[JNDOKEKSPOR]', $DOK, $DETPKBE['JNDOKEKSPOR'], 'id="JNDOKEKSPOR" class="stext" onchange="kode(this.value)" onblur="kode(this.value)" style="width: 122px"'); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="jnsdok" width="100px;">Nomor Dokumen</td>
                                    <td>
                                        <input type="text" class="ssstext" name="DETPKBE[NOPEB]" id="NOPEB" value="<?= $DETPKBE['NOPEB']; ?>"  maxlength="50" wajib="yes"/>
                                    </td>
                                    <td>Tanggal PEB &nbsp;&nbsp;
                                        <input type="text" name="DETPKBE[TGPEB]" id="TGPEB" value="<?= $DETPKBE['TGPEB'] ?>" class="sstext"  formatDate="yes" wajib="yes">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Kategori Brg Ekspor</td>
                                    <td>
                                        <?= form_dropdown('DETPKBE[KATEKS]', $KATEKS, $DETPKBE['KATEKS'], 'id="KATEKS" class="stext" onchange="kode(this.value)" onblur="kode(this.value)" style="width: 122px"'); ?>
                                        &nbsp;&nbsp;
                                        <?= form_dropdown('DETPKBE[EXMERAH]', $JNSPER, $DETPKBE['EXMERAH'], 'id="EXMERAH" class="stext" onchange="kode(this.value)" onblur="kode(this.value)" style="width: 122px"'); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>KPBC Pendaftaran </td>
                                    <td colspan="3">
                                        <input type="text"  name="DETPKBE[KDKTRPEB]" id="KDKTRPEB" value="<?= $DETPKBE['KDKTRPEB']; ?>" url="<?= site_url('referensi/autoComplate/KPBC'); ?>" urai="KDKTRPEB;URKDKTRPEB" wajib="yes" onfocus="Autocomp(this.id)" class="ssstext" maxlength="6" onblur="f_bedaKPBC(this.value)" /> &nbsp;
                                        <button onclick="tb_search('kpbc', 'KDKTRPEB;URKDKTRPEB', 'Kode Kpbc', this.form.id, 650, 400);" class="btn"> ... </button>
                                        <span id="URKDKTRPEB"><?= $DETPKBE['UKDKTRPEB'] ?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?= form_dropdown('DETPKBE[JNDOK]', $JNSRES, $DETPKBE['JNDOK'], 'id="JNDOK" class="stext" onblur="kode(this.value)" style="width: 122px"'); ?>
                                    </td>
                                    <td>No.&nbsp;&nbsp;<input type="text" name="DETPKBE[NOPE]" id="NOPE" value="<?= $DETPKBE['NOPE']; ?>"  maxlength="50" wajib="yes"/>
                                    </td>
                                    <td>Tanggal PE &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="DETPKBE[TGPE]" id="TGPE" value="<?= $DETPKBE['TGPE'] ?>" class="sstext"  formatDate="yes" wajib="yes">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Keterangan</td>
                                    <td>
                                        <input type="text" name="DETPKBE[KETERANGAN]" id="KETERANGAN" value="<?= $DETPKBE['KETERANGAN']; ?>"  maxlength="50" wajib="yes"/>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <fieldset id="identitas">
                            <table style="width:100%">
                                <tr>
                                    <td width="125px">Identitas</td>
                                    <td>
                                        <?= form_dropdown('DETPKBE[IDEKS]', $IDEKS, $DETPKBE[IDEKS], 'id="IDEKS2" class="offterus smtext"'); ?>
                                        <input type="text" name="DETPKBE[NPWPEKS]" id="NPWPEKS2" url="<?= site_url('referensi/autoComplate/EKSPORTIR/NPWPEKS'); ?>" urai="IDEKS2;NPWPEKS2;NAMAEKS2;ALMTEKS2" onfocus="Autocomp(this.id);
                                                unformatNPWP(this.id);" onblur="formatNPWP(this.id);" value="<?= $this->fungsi->FORMATNPWP($DETPKBE['NPWPEKS']); ?>" class="smtext" maxlength="15"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Nama </td>
                                    <td><input type="text" name="DETPKBE[NAMAEKS]" id="NAMAEKS2" value="<?= $DETPKBE['NAMAEKS']; ?>" class="vltext"/></td>
                                </tr>
                                <tr>
                                    <td>Alamat </td>
                                    <td>
                                        <textarea name="DETPKBE[ALMTEKS]" id="ALMTEKS2" rows="1" cols="35"><?= $DETPKBE['ALMTEKS']; ?></textarea>
                                </tr>
                                <tr>
                                    <td>Jumlah Kemasan</td>
                                    <td><input type="text" class="stext" name="DETPKBE[JMKEMAS]" id="JMKEMAS" value="<?= $DETPKBE['JMKEMAS']; ?>"  maxlength="50" wajib="yes"/></td>
                                    <td>Pengemas</td>
                                    <td colspan="3">
                                        <input type="text"  name="DETPKBE[JNKEMAS]" id="JNKEMAS" value="<?= $DETPKBE['JNKEMAS']; ?>" url="<?= site_url('referensi/autoComplate/KEMASAN'); ?>" urai="JNKEMAS;UJNKEMAS" wajib="yes" onfocus="Autocomp(this.id)" class="sssstext" maxlength="6" onblur="f_bedaKPBC(this.value)" /> &nbsp;
                                        <button onclick="tb_search('kemasan', 'JNKEMAS;UJNKEMAS', 'Kode Kemasan', this.form.id, 650, 400);" class="btn"> ... </button>
                                        <span id="UJNKEMAS"><?= $DETPKBE['UJNKEMAS'] ?></span>
                                    </td>
                                </tr>   
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><div class="msg_"></div></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button onclick="save_post_msg('fdetailpkbe', '', 'detilPKBE');" class="btn">Simpan</button>
                        <button onclick="cancel('fdetailpkbe');" class="btn">Batal</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>