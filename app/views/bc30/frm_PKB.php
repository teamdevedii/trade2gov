<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
//print_r($CAR);die();
?>
<script>
    $(document).ready(function() {
        onoffPkb(false);
        declareDP('fpkb');
        f_MAXLENGTH('fpkb');
    });

    function onoffPkb(on) {
        //cmdPKB
        if ($('#fpkb #CAR').val() == '') {
            $('#fpkb.outentry').hide();
        } else {
            $('#fpkb.outentry').show();
        }
        if (on) {
            $('#fpkb #on').show();
            $('#fpkb #off').hide();
            $("#fpkb :input").removeAttr("disabled");
            $('#fpkb .outentry :input').attr('disabled', 'disabled');
            $("#tabs").tabs({disabled: [1, 2]});
        } else {
            $('#fpkb #on').hide();
            $('#fpkb #off').show();
            $("#fpkb :input").attr('disabled', 'disabled');
            $('#fpkb .outentry :input').removeAttr("disabled");
        }
        $('#fpkb .onterus').removeAttr('disabled');
        $('#fpkb .offterus :input').attr('readonly', 'readonly');
    }

    function cekValidasiPKB(car) {
        //alert(car);return false;
        $(function() {
            $('#msgbox').html('');
            $("#msgbox").html('<div style="margin-left: 15px; margin-top: 15px;"><img src="' + base_url + 'img/loaders/facebook.gif" /> loading...</div>');
            $("#msgbox").dialog({
                resizable: false,
                height: 500,
                modal: true,
                width: 700,
                title: 'Validasi Dokumen',
                open: function() {
                    $.ajax({
                        type: 'POST',
                        url: site_url + '/bc30/alertcekstatusPKB/' + car + '/t_bc30pkb',
                        success: function(data) {
                            $("#msgbox").html(data);
                            $('#URSTATUS').html($('#STATUSCEK').val());
                            call(site_url + '/bc30/frmPKB/' + car, 'fpkb');
                        }
                    });
                },
                buttons: {
                    Close: function() {
                        $(this).dialog("close");
                    }
                }
            });
        });
    }
</script>
<div class="content_luar" id="navPKB" style="height: 95%; margin: 3px; padding: 1px">
    <div class="content_dalam" style="height: 100%; width: 100%">
        <form name="fpkb" id="fpkb" action="<?= site_url('bc30/setHeaderPKB'); ?>" method="post" autocomplete="off" onsubmit="return false;" >
            <?= $MAXLENGTH ?>
            <input type="hidden" name="act" id="act" value="save" />
            <input type="hidden" name="DTLPKB[CAR]" id="car" value="<?= $CAR ?>" />
            <table width="100%">
                <tr>
                    <td colspan="2">

                        <h4>
                            <span id="off">
                                <button onclick="onoffPkb(true);
                        return false;" class="btn onterus"><span class="icon-pen"></span> Edit </button>
                            </span>
                            <span id="on">
                                <button onclick="save_post_msg('fpkb','','navPKB');
                        onoffPkb(false);" class="btn onterus"><span class="icon-download"></span> Simpan</button>
                                <button onclick="onoffPkb(false);" class="btn onterus"><span class="icon-undo"></span> Batal</button>
                            </span><span class="msgheader_" style="margin-left:20px">&nbsp;</span>
                            <span style="float: right" class="outentry">
                            <button onclick="cekValidasiPKB($('#fpkb #car').val());" class="btn" id="PKBOK"><span id="ULENGKAP" class="<?= ($DTLPKB['LENGKAP']!='1')?'icon-x':'icon-check'; ?>"></span> <b style="color: #ffffff"><?= $DTLPKB['ULENGKAP'] ?></b></button>
                        </span>
                        </h4>
                        
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <fieldset>
                            <legend>Data Pemohon</legend>
                            <table width="100%">
                                <tr>
                                    <td>
                                        Nama/NPWP
                                    </td>
                                    <td>
                                        <input readonly type="text" name="DTLPKB[NAMAEKS]" id="NAMAEKS" value="<?= $PEMOHON['NAMAEKS']; ?>" class="mtext" /> &nbsp;&nbsp; <input type="text" readonly name="HEADER[NPWPEKS]" id="NPWPEKS"  value="<?= $PEMOHON['NPWPEKS']; ?>" class="smtext"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Alamat
                                    </td>
                                    <td>
                                        <textarea readonly name="DTLPKB[ALMTEKS]" id="ALMTEKS" readonly="readonly" rows="1" cols="35" class="offterus"><?= $PEMOHON['ALMTEKS']; ?></textarea>
                                    </td>
                                    <td>
                                     NIPPER
                                     </td>
                                     <td>
                                     <input readonly type="text" name="DTLPKB[NIPER]" id="NIPER" value="<?= $PEMOHON['NIPER']; ?>" class="ssstext"/>
                                    </td>
                                </tr>
                                <tr> 
                                    <td>
                                        Telepon 
                                    </td>
                                    <td>
                                        <input readonly type="text" name="DTLPKB[TELPEKS]" id="" value="<?= $PEMOHON['TELPEKS']; ?>" class="stext"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        Fax &nbsp;&nbsp;&nbsp; <input readonly type="text" value="<?= $PEMOHON['FAXEKS']; ?>" class="stext"/> 
                                    </td>
                                    <td>
                                        Jenis Usaha
                                    </td>
                                    <td>
                                         <?= form_dropdown('DTLPKB[JNKONS]', $JNUSAHA, $DTLPKB['JNKONS'], 'id="JNKONS" class="mtext"'); ?>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <fieldset>
                            <legend>Khusus KITE dengan Pembebasan dan Pengendalian</legend>
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 62%; vertical-align: top;">
                                        <?= form_dropdown('DTLPKB[FASILITAS]', $KDKITE, $DTLPKB['FASILITAS'], 'id="FASILITAS" class="vltext"'); ?>
                                    </td>
                                    <td style="width: 50%">
                                        <table>
                                            <tr>
                                                <td style="width: 100px">Zoning KITE </td>
                                                <td><?= form_dropdown('DTLPKB[KPKER]', $ZONING_KITE, $DTLPKB['KPKER'], 'id="KPKER" class="mtext"'); ?></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td><input type="text" name="DTLPKB[KWBC]" id="KWBC" value="<?= $DTLPKB['KWBC']; ?>" class="ssstext"/></td>
                                            </tr>
                                        </table>                                        
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td style="width: 50%">
                        <fieldset>
                            <legend>Kesiapan Barang</legend>
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 150px;">1. Jenis Barang</td>
                                    <td>
                                        <?= form_dropdown('DTLPKB[JNBRGGAB]', $JNBRGGAB, $DTLPKB['JNBRGGAB'], 'id="JNBRGGAB" class="mtext"'); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2. Tempat Simpan Barang</td>
                                    <td>
                                        <?= form_dropdown('DTLPKB[GUDANG]', $GUDANG, $DTLPKB['GUDANG'], 'id="GUDANG" class="mtext"'); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">3. Permintaan Pemeriksaan</td>
                                </tr>
                                <tr>
                                    <td>a. Tanggal</td>
                                    <td>
                                        <input type="text" name="DTLPKB[TGSIAP]" id="TGSIAP1" value="<?= $DTLPKB['TGSIAP']; ?>" class="sstext" formatDate="yes"/> Jam <input type="text" name="DTLPKB[WKSIAP]" id="WKSIAP" value="<?= $DTLPKB['WKSIAP']; ?>" class="sstext"/>
                                    </td>
                                </tr>
                                <tr style="vertical-align: top;">
                                    <td>b. Alamat</td>
                                    <td><textarea name="DTLPKB[ALMTSIAP]" id="ALMTSIAP" rows="1" cols="30" class="offterus"><?= $DTLPKB['ALMTSIAP']; ?></textarea></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Telepon</td>
                                    <td>
                                        <input type="text" id="NOPHONE" name="DTLPKB[NOPHONE]" value="<?= $DTLPKB['NOPHONE']; ?>" class="sstext"> Fax
                                        <input type="text" id="NOFAX" name="DTLPKB[NOFAX]" value="<?= $DTLPKB['NOFAX']; ?>" class="sstext">
                                    </td>
                                </tr>
                                <tr>
                                    <td>c. Ct. Person</td>
                                    <td>
                                        <input type="text" id="PETUGAS" name="DTLPKB[PETUGAS]" value="<?= $DTLPKB['PETUGAS']; ?>" class="sstext">
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                    <td style="width: 50%; vertical-align: top;">
                        <fieldset>
                            <table>
                                <tr>
                                    <td colspan="2">
                                        4. Pelaksana Stuffering
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;&nbsp;&nbsp;&nbsp;Tanggal
                                    </td>
                                    <td style="width: 50%;">
                                        &nbsp;<input type="text" name="DTLPKB[TGSTUFF]" id="TGSTUFF" value="<?= $DTLPKB['TGSTUFF']; ?>" class="sstext" formatDate="yes"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        &nbsp;&nbsp;&nbsp;&nbsp;Tempat
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        &nbsp;<input type="text" name="DTLPKB[ALMTSTUFF]" id="ALMTSTUFF" value="<?= $DTLPKB['ALMTSTUFF']; ?>" class="mtext"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        5.Jumlah peti kemas
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        &nbsp;<input type="text" name="DTLPKB[JMCONT20]" id="JMCONT20" value="<?= $DTLPKB['JMCONT20']; ?>" class="sssstext"/>&nbsp; X 20 Feet <input type="text" name="DTLPKB[JMCONT40]" id="JMCONT40" value="<?= $DTLPKB['JMCONT40']; ?>" class="sssstext"/>&nbsp; X 40 Feet
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        6. Cara Stuffering 
                                    </td>
                                    <td>
                                        &nbsp;<?= form_dropdown('DTLPKB[STUFF]', $STUFF, $DTLPKB['STUFF'], 'id="STUFF" class="mtext"'); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        7. Dlm hal part of
                                    </td>
                                    <td>
                                        &nbsp;<?= form_dropdown('DTLPKB[JNPARTOF]', $JNPARTOF, $DTLPKB['JNPARTOF'], 'id="JNPARTOF" class="mtext"'); ?>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>