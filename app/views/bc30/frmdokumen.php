<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<script>
    $(function () {
        declareDP('fdokumen_');
        bankDHE($('#fdokumen_ #KDDOK').val());
        $('#fdokumen_form').show();
    })
    function bankDHE(kdDOk){
        $("#fdokumen_ #nonBank :input").attr('disabled','disabled');
        $('#fdokumen_ #nonBank').hide();
        $("#fdokumen_ #Bank :input").attr('disabled','disabled');
        $('#fdokumen_ #Bank').hide();
        if(kdDOk==='111'){
            $("#fdokumen_ #Bank :input").removeAttr("disabled");
            $('#fdokumen_ #Bank').show();
        }else{
            $("#fdokumen_ #nonBank :input").removeAttr("disabled");
            $('#fdokumen_ #nonBank').show();
        }
    }
</script>
<form id="fdokumen_" nama='fdokumen_' action="<?= site_url('bc30/dokumen/'.$DOKUMEN['CAR']) ?>" onsubmit="return false;" method="post" autocomplete="off">
    <input type='hidden' name="act" value="<?= $act; ?>" />
    <input type='hidden' name="DOKUMEN[CAR]" id="CAR" value="<?= $DOKUMEN['CAR']; ?>" />
    <input type='hidden' name="DOKUMEN[DOKID]" value="<?= $DOKUMEN['DOKID']; ?>" />
    <table>
        <tr><td colspan="2">Kode Dokumen :</td></tr>
        <tr>
            <?php
                if($act=='save'){
            ?>
        <td colspan="2"><?= form_dropdown('DOKUMEN[KDDOK]', $KDDOK, $DOKUMEN['KDDOK'], 'id="KDDOK"  class="text" onchange="bankDHE(this.value)"'); ?>                     
            <?php   
                }else{
            ?>
        <td colspan="2"><?= form_dropdown('DOKUMEN[KDDOK]', $KDDOK, $DOKUMEN['KDDOK'], 'id="KDDOK"  class="text" disabled onchange="bankDHE(this.value)"'); ?>
        <input type='hidden' name="DOKUMEN[KDDOK]" value="<?= $DOKUMEN['KDDOK']; ?>" id = "KDDOK" /></td>  
             <?php
                } 
            ?>
        </tr>
        <tr><td>Nomor :</td><td>Tanggal :</td></tr>
        <tr>
            <td id="nonBank">
                <input type="text" name="DOKUMEN[NODOK]" id="NODOK1" class="ltext" value="<?= $DOKUMEN['NODOK']; ?>" maxlength="30" wajib="yes"/>
            </td>
            <td id="Bank">
                <input type="text" name="DOKUMEN[NODOK]" id="NODOK2" value="<?= $DOKUMEN['NODOK']; ?>" url="<?= site_url('referensi/autoComplate/BANK/CONCAT~|NMBANK,KDBANK|~'); ?>" urai="NODOK2"  onfocus="Autocomp(this.id)" class="ltext"/> <button onclick="tb_search('bank', 'NODOK2', 'Kode Bank DHE', this.form.id, 650, 400);" class="btn">...</button>
            </td>
            <td>
                <input type="text" name="DOKUMEN[TGDOK]" id="TGDOK" class="sstext" value="<?= $DOKUMEN['TGDOK']; ?>" maxlength="10" formatDate="yes" wajib="yes"/>
            </td>
        </tr>
        <tr>
            <td colspan="2"><br>
                <button onclick="save_post_msg('fdokumen_', '', 'fdokumen_list') && $('#fdokumen_form').html('');" class="btn">Simpan</button>
                <button onclick="cancel('fdokumen_');$('#fdokumen_form').html('');" class="btn">Batal</button>
            </td>
        </tr>
    </table>
</form>
