<?php
if (!defined('BASEPATH')){
    exit('No direct script access allowed');
}
$arr = explode(";", $data);
foreach ($arr as $isi) {
    $arrX = explode("|", $isi);
    $val[$arrX[0]] = $arrX[1];
}

?>
<div class="content_luar">
    <div class="content_dalam">
        <form method="post" name="harga" id="harga" onsubmit="return false;"  >
            <h4><span class="info_2">&nbsp;</span>Isi sesuai Invoice/Dokumen lain</h4>
            <table border="0">
                <tr>
                    <td width="100px;">Kode Harga</td>
                    <td width="220px;">:
                        <?= form_dropdown('HEADER[KDHRG]', $KODE_HARGA, $val['KDHRG'], 'id="KODE_HARGAHarga" class="stext" onchange="kode(this.value)" onblur="kode(this.value)" style="width: 122px"'); ?>
                    </td>
                    <td class="hargafob" width="100px;">Harga Invoice</td>
                    <td width="220px;">:
                        <input type="text" name="NILAI_INVUR" class="smtext" id="NILAI_INVUR" onkeyup="this.value = ThausandSeperator('NILAI_INV', this.value, 4); prosesHarga('harga');" style="text-align:right;" value="<?= $this->fungsi->FormatRupiah($val['FOB'], 4); ?>"/>
                        <input type="hidden"  name="NILAI_INV" id="NILAI_INV" value="<?= $val['FOB'] ?>" />
                    </td>
                </tr>
                <tr>
                    <td>Nilai Asuransi</td>
                    <td width="220px;">:
                        <input type="text" name="NILAI_ASURANSIUR" class="smtext" id="NILAI_ASURANSIUR" onkeyup="this.value = ThausandSeperator('NILAI_ASURANSI', this.value, 4); prosesHarga('harga');" style="text-align:right;" value="<?= $this->fungsi->FormatRupiah($val['ASURANSI'], 4); ?>"/>
                        <input type="hidden"  name="NILAI_ASURANSI" id="NILAI_ASURANSI" value="<?= $val['ASURANSI'] ?>" />
                    </td>
                    <td>Harga FOB</td>
                    <td>:
                        <input readonly="readonly" type="text" name="HARGA_FOBUR" class="smtext" id="HARGA_FOBUR" onkeyup="this.value = ThausandSeperator('HARGA_FOB', this.value, 4); prosesHarga('harga');" style="text-align:right;" value="<?= $this->fungsi->FormatRupiah($val['FOB'], 4); ?>"/>
                        <input type="hidden" name="HARGA_FOB" id="HARGA_FOB" value="<?= $val['FOB'] ?>" />
                    </td>
                </tr>
                <tr>
                    <td>Freight</td>
                    <td width="220px;">:
                        <input type="text" name="NILAI_FREIGHTUR" class="smtext" id="NILAI_FREIGHTUR" onkeyup="this.value = ThausandSeperator('NILAI_FREIGHT', this.value, 4); prosesHarga('harga');" style="text-align:right;" value="<?= $this->fungsi->FormatRupiah($val['FREIGHT'], 4); ?>"/>
                        <input type="hidden"  name="NILAI_FREIGHT" id="NILAI_FREIGHT" value="<?= $val['FREIGHT'] ?>" />
                    </td>
                    <td>Harga CIF</td>
                    <td>:
                        <input readonly="readonly" type="text" name="HARGA_CIFUR" class="smtext" id="HARGA_CIFUR" onkeyup="this.value = ThausandSeperator('HARGA_CIF', this.value, 4); prosesHarga('harga');" style="text-align:right;" value="<?= $this->fungsi->FormatRupiah($val['BTAMBAHAN'], 4); ?>"/>
                        <input type="hidden" name="HARGA_CIF" id="HARGA_CIF" value="<?= $val['BTAMBAHAN'] ?>" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button onclick="getHarga('harga');" class="btn">Simpan</button>
                        <button onclick="cancel('harga');" class="btn">Batal</button>
                    </td>
                </tr>        
            </table>
        </form>
    </div>
</div>
<script>
    var kd_Harga = $("#harga #KODE_HARGAHarga").val();
    kode(kd_Harga);
     
    function kode(data) {
        
        var Asuransi = parseFloat($('#NILAI_ASURANSI').val());
        var Freight  = parseFloat($('#NILAI_FREIGHT').val());
        var Invoice  = parseFloat($('#NILAI_INV').val());
        if (data == 1) {
            var HargaFOB = (Invoice).toFixed(4);
            $('#HARGA_FOBUR').val(ThausandSeperator('HARGA_FOB', HargaFOB, 4));

            var HargaCIF = parseFloat(Asuransi + Freight + Invoice).toFixed(4);
            $('#HARGA_CIFUR').val(ThausandSeperator('HARGA_FOB', HargaCIF, 4));
            
        } else if (data == 2) {
            var HargaCIF = (Invoice).toFixed(4);
            $('#HARGA_CIFUR').val(ThausandSeperator('HARGA_CIF', HargaCIF, 4));

            var HargaFOB = parseFloat(Invoice - Freight - Asuransi).toFixed(4);
            $('#HARGA_FOBUR').val(ThausandSeperator('HARGA_FOB', HargaFOB, 4));
        } else if (data == 3) {
            
        }
        prosesHarga('hargaa');
    }
    
    function prosesHarga(form) {
        var KDHARGA = $("#" + form + " #KODE_HARGAHarga").val();
        if (KDHARGA != "") {
            var Asuransi = parseFloat($('#NILAI_ASURANSI').val());
            var Freight  = parseFloat($('#NILAI_FREIGHT').val());
            var Invoice  = parseFloat($('#NILAI_INV').val());
            
            if (KDHARGA != 1){ //khusus harga CIF
                var HargaFOB = (Invoice).toFixed(4);
                $('#HARGA_FOBUR').val(ThausandSeperator('HARGA_FOB', HargaFOB, 4));

                var HargaCIF = parseFloat(Asuransi + Freight + Invoice).toFixed(4);
                $('#HARGA_CIFUR').val(ThausandSeperator('HARGA_FOB', HargaCIF, 4));
            }else{
                var HargaCIF = (Invoice).toFixed(4);
                $('#HARGA_CIFUR').val(ThausandSeperator('HARGA_CIF', HargaCIF, 4));

                var HargaFOB = parseFloat(Invoice - Freight - Asuransi).toFixed(4);
                $('#HARGA_FOBUR').val(ThausandSeperator('HARGA_FOB', HargaFOB, 4));
            }
        }
    } 
    
    
    function getHarga(form) {
        var NILAI_FREIGHT       = ($("#" + form + " #NILAI_FREIGHT").val()) ? $("#" + form + " #NILAI_FREIGHT").val() : 0;
        var NILAI_FREIGHTUR     = ($("#" + form + " #NILAI_FREIGHTUR").val()) ? $("#" + form + " #NILAI_FREIGHTUR").val() : 0;
        var NILAI_ASURANSI      = ($("#" + form + " #NILAI_ASURANSI").val()) ? $("#" + form + " #NILAI_ASURANSI").val() : 0;
        var NILAI_ASURANSIUR    = ($("#" + form + " #NILAI_ASURANSIUR").val()) ? $("#" + form + " #NILAI_ASURANSIUR").val() : 0;
        var HARGA_FOB           = ($("#" + form + " #HARGA_FOB").val()) ? $("#" + form + " #HARGA_FOB").val() : 0;
        var HARGA_FOBUR         = ($("#" + form + " #HARGA_FOBUR").val()) ? $("#" + form + " #HARGA_FOBUR").val() : 0;
        
        if (KODE_HARGAHarga != "") {
        document.getElementById('FREIGHT').value    = NILAI_FREIGHT;
        document.getElementById('URFREIGHT').value  = NILAI_FREIGHTUR;
        document.getElementById('ASURANSI').value   = NILAI_ASURANSI;
        document.getElementById('URASURANSI').value = NILAI_ASURANSIUR;
        document.getElementById('FOB').value        = HARGA_FOB;
        document.getElementById('URFOB').value      = HARGA_FOBUR;
        }
        closedialog('divHitung');
        $("#BRUTOUR").focus();
    }
</script>
