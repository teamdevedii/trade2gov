<?php
if (!defined('BASEPATH')){exit('No direct script access allowed');}
if($HEADER['TIPE_TRADER']!='2'){$ro = 'offterus';}
?>
<script>
    $(document).ready(function() {
        declareDP('fbc30_');
        onoffHdr(false);
        //prosesHargaHeader('fbc30_');
        JnsByr($('#DELIVERY').val());
        KatagoriEks($('#KATEKS').val());
        f_MAXLENGTH('fbc30_');
        formatNPWP('NPWPEKS');
        formatNumeral('fbc30_');
        if($('#NPWPPPJK').length){
            formatNPWP('NPWPPPJK');
        }
    });
    
    function JnsByr(id){
        $("#ASURANSI").removeAttr('readonly');
        $("#FREIGHT").removeAttr('readonly');
        $("#FOB").removeAttr('readonly');
        switch(id){
            case'CFR':
                $("#ASURANSI").attr('readonly', 'readonly');
                $("#FREIGHT").removeAttr('readonly');
                $("#FOB").removeAttr('readonly');
                break;
            case'CIF':
                $("#ASURANSI").removeAttr('readonly');
                $("#FREIGHT").removeAttr('readonly');
                $("#FOB").removeAttr('readonly');
                break;
            case'FOB':case'CIP':case'CPT':case'DAF':case'DDP':case'DDU':case'DEQ':case'DES':case'EXW':case'FAS':case'FCA':
                $("#ASURANSI").attr('readonly', 'readonly');
                $("#FREIGHT").attr('readonly', 'readonly');
                $("#FOB").removeAttr("readonly");
                break;
            default :
                $("#ASURANSI").removeAttr('readonly');
                $("#FREIGHT").removeAttr('readonly');
                $("#FOB").removeAttr('readonly');
                break;
        }
    }
    function hitungNilai() {
        var JnsByr       = $('#DELIVERY').val();
        var trf_asuransi = parseFloat($('#NASURANSI').val());
        var trf_freight  = parseFloat($('#NFREIGHT').val());
        var asuransi     = parseFloat(ReplaceAll($('#ASURANSI').val(),',',''));
        var freight      = parseFloat(ReplaceAll($('#FREIGHT').val(),',',''));
        var fob          = parseFloat(ReplaceAll($('#FOB').val(),',',''));
        //alert('('+fob+'+'+freight+')'+'*'+'('+trf_asuransi+'/'+'100'+')'+'='+asuransi);
        
        if (JnsByr == 'CFR'){
            var asuransi = (fob+freight) * (trf_asuransi / 100);
            $('#ASURANSI').val(numeral(asuransi).format('0,0.0000'));
        }else if (JnsByr == 'CIF'){
           
        }else{
            var Nil_freight  = fob *(trf_freight / 100);
            $('#FREIGHT').val(numeral(Nil_freight).format('0,0.0000'));
            var Nil_asuransi = (fob+Nil_freight)*(trf_asuransi / 100);
            $('#ASURANSI').val(numeral(Nil_asuransi).format('0,0.0000'));
        }
    } 
    function onoffHdr(on) {
        //cmdPKB
        if($('#fbc30_ #CAR').val()=='' || $('#fbc30_ #act').val()=='save'){
           $('#fbc30_ .outentry').hide();
        }else{
           $('#fbc30_ .outentry').show(); 
        }
        if (on) {
            $('#fbc30_ #on').show();
            $('#fbc30_ #off').hide();
            $("#fbc30_ :input").removeAttr("disabled");
            $('#fbc30_ .outentry :input').attr('disabled','disabled');
            $("#tabs").tabs({disabled: [1, 2]});
        } else {
            $('#fbc30_ #on').hide();
            $('#fbc30_ #off').show();
            $("#fbc30_ :input").attr('disabled','disabled');
            $('#fbc30_ .outentry :input').removeAttr("disabled");
            $("#tabs").tabs();
            if ($("#fbc30_ #act").val() == 'save') {
                $("#tabs").tabs({disabled: [1]});
            } else {
                $("#tabs").tabs({disabled: false});
            }
        }
        $('#fbc30_ .onterus').removeAttr('disabled');
        $('#fbc30_ .offterus :input').attr('readonly','readonly');
    }
    function KatagoriEks(id){
        $('#cmdPKB').hide();
        $('#cmdPEBGAB').hide();
        $('#lokasiTPB').hide();
        $('#pkbpeb').hide();
        switch(id){
            case'21':
            case'22':
            case'23':
                $('#cmdPKB').show();
                $('#cmdPEBGAB').show();
                $('#pkbpeb').show();
                break;
            case'41':
            case'42':
            case'43':
            case'44':
            case'45':
            case'46':
                $('#lokasiTPB').show();
                break;
        }
    }
    function QQ(){
        $('#indentor #IDQQ').val($('#fbc30_ #IDEKS').val());
        $('#indentor #NPWPQQ').val($('#fbc30_ #NPWPEKS').val());
        $('#indentor #NAMAQQ').val($('#fbc30_ #NAMAEKS').val());
        $('#indentor #ALMTQQ').val($('#fbc30_ #ALMTEKS').val());
        $('#indentor #NIPERQQ').val($('#fbc30_ #NIPER').val());
        $('#indentor #NPWPQQ').focus();
    }
    function indentorQQ(inpField,frmId){
        //alert(frmId); return false;
        var id = inpField.split(";");
        var data = "";
        for (var a = 0; a < id.length; a++) {
            data += ';' + id[a] + '|' + $('#' + frmId + ' #' + id[a]).val();
        }
        data = data.substr(1);
        //alert(data);return false;
        DialogPost(site_url + '/bc30/indentor', 'divIndentor', '.: Data Pemilik Barang / Indentor / QQ :.', 480, 250, data);
    } 
    function cekValidasiHDR(car) {
        $(function() {
            $('#msgbox').html('');
            $("#msgbox").html('<div style="margin-left: 15px; margin-top: 15px;"><img src="' + base_url + 'img/loaders/facebook.gif" /> loading...</div>');
            $("#msgbox").dialog({
                resizable: false,
                height: 500,
                modal: true,
                width: 700,
                title: 'Validasi Dokumen',
                open: function() {
                    $.ajax({
                        type: 'POST',
                        url: site_url + '/bc30/alertcekstatusHDR/' +car+ '/fbc30_',
                        success: function(data) {
                            $("#msgbox").html(data);
                            $('#URSTATUS').html($('#STATUSCEK').val());
                            call(site_url + '/bc30/refreshHeaderPEB/' + car, 'tab-Header');
                        }
                    });
                },
                buttons: {
                    Close: function() {
                        $(this).dialog("close");
                    }
                }
            });
        });
    }
    function EditHarga(inputField) {
        var id = inputField.split(";");
        var data = "";
        for (var a = 0; a < id.length; a++) {
            data += ';' + id[a] + '|' + $('#' + id[a]).val();
        }
        data = data.substr(1);
        Dialog(site_url + '/bc30/hitungan/' + data, 'divHitung', '.: FORM EDIT HARGA :.', 600, 200);
    }
    function getTOD(idModa,Regiona, Tgdaft){
        if(idModa != '' && Regiona != '' && idModa != 'undefined' && Regiona != 'undefined'){
            $.ajax({
                type: 'POST',
                url: site_url + '/bc30/getTOD',
                data: {idModa:idModa,Regiona:Regiona,Tgdaft:Tgdaft},
                success: function(data) {
                    var arr = data.split('|');
                    $('#REGION').val(arr[0]);
                    $('#NFREIGHT').val(arr[1]);
                    $('#NASURANSI').val(arr[2]);
                    $('#URNEGTUJU').html(arr[3]);
                }
            });
        }
    }
    
</script>
<span id="DivHeaderForm">
    <span id="divtmp"></span>
    <form id="fbc30_" name="fbc30_" action="<?= site_url('bc30/setHeader'); ?>" method="post" autocomplete="off" onsubmit="return false;">
        <?= $MAXLENGTH?>
        <input type="hidden" name="act" id="act" value="<?= $act; ?>" />
        <input type="hidden" name="HEADER[CAR]" id="CAR" value="<?= $HEADER['CAR']; ?>" />
        <input type="hidden" name="HEADER[STATUS]" id="STATUS" value="<?= $HEADER['STATUS']; ?>" />
        <input type="hidden" name="HEA" id="STATUS" value="<?= $HEADER['TIPE_TRADER']; ?>" />
        <h4>
            <span id="off">
                <button onclick="onoffHdr(true);return false;" class="btn onterus"><span class="icon-pen"></span> Edit </button>
            </span>
            <span id="on">
                <button onclick="save_post_msg('fbc30_');onoffHdr(false);" class="btn onterus"><span class="icon-download"></span> Simpan</button>
                <button onclick="onoffHdr(false);" class="btn onterus"><span class="icon-undo"></span> Batal</button>
            </span><span class="msgheader_" style="margin-left:20px">&nbsp;</span>
            <?php if ($HEADER['CAR']) { ?>
                <button id="btnQUEUED" onclick="gotoQUEUED($('#CAR').val(),'<?= $TIPE_TRADER;?>',$('#STATUS').val())" class="btn onterus" style="float: right; margin-right: 5px;" ><span class="icon-mail"></span> Edifact / UnEdifact</button>
            <?php } ?>
        </h4>
        <table width="100%" border="0">
            <tr>
                <td width="50%" valign="top">
                    <table width="100%">
                        <tr>
                            <td width="150px">A. KANTOR PABEAN</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td> &nbsp;&nbsp;&nbsp;1. Nomor Pengajuan</td>
                            <td><b><?= substr($HEADER['CAR'], 0, 6) . '-' . substr($HEADER['CAR'], 6, 6) . '-' . substr($HEADER['CAR'], 12, 8) . '-' . substr($HEADER['CAR'], 20, 6); ?></b></td>
                        </tr>
                        <tr>
                            <td> &nbsp;&nbsp;&nbsp;2. Ktr Pabean Muat</td>
                            <td>
                                <input type="text"  name="HEADER[KDKTR]" id="KDKTR" value="<?= $HEADER['KDKTR']; ?>" url="<?= site_url('referensi/autoComplate/KPBC'); ?>" onblur="checkCode(this);" grp="kpbc" urai="URKDKTR" class="ssstext"/>
                                <button onclick="tb_search('kpbc', 'KDKTR;URKDKTR', 'Kode Kpbc', this.form.id, 650, 400);" class="btn">...</button> 
                                <span id="URKDKTR"><?= $HEADER['URKDKTR']; ?></span><br>
                                
                            </td>
                        </tr>
                        <tr>
                            <td> &nbsp;&nbsp;&nbsp;3. Hanggar</td>
                            <td>
                                <input type="text"  name="HEADER[KDHANGGAR]" id="KDHANGGAR" value="<?= $HEADER['KDHANGGAR']; ?>" url="<?= site_url('referensi/autoComplate/HANGGAR/KDHGR'); ?>" urai="KDHANGGAR;URKDHANGGAR" onfocus="Autocomp(this.id, $('#KDKTR').val())" class="ssstext" /> <button onclick="tb_search('hanggar', 'KDHANGGAR;URKDHANGGAR', 'Kode Hanggar', this.form.id, 650, 400,'KDKTR;');" class="btn">...</button> <span id="URKDHANGGAR"><?= $HEADER['URKDHANGGAR']; ?></span><br>
                                
                            </td>
                        </tr>
                        <tr>
                            <td>B. JENIS EKSPOR</td>
                            <td><?= form_dropdown('HEADER[JNEKS]', $JNEKS, $HEADER['JNEKS'], 'id="JNPIB" class="ltext"'); ?></td>
                        </tr>
                        <tr>
                            <td>C. KATAGORI EKSPOR</td>
                            <td><?= form_dropdown('HEADER[KATEKS]', $KATEKS, $HEADER['KATEKS'], 'id="KATEKS" class="vltext" onblur="KatagoriEks(this.value)"'); ?></td>
                        </tr>
                        <tr>
                            <td>D. CARA PERDAGANGAN</td>
                            <td><?= form_dropdown('HEADER[JNDAG]', $JNDAG, $HEADER['JNDAG'], 'id="JNDAG" class="mtext"'); ?>
                                <span class="outentry" ><button class="btn" id="cmdPKB" style="float: right; margin-right: 10px" onclick="frmPKB($('#CAR').val())"> PKB </button></span>
                            </td>
                        </tr>
                        <tr>
                            <td>E. CARA PEMBAYARAN</td>
                            <td><?= form_dropdown('HEADER[JNBYR]', $JNBYR, $HEADER['JNBYR'], 'id="JNBYR" class="ltext"'); ?>
                            </td>
                        </tr>
                        <tr id="pkbpeb">
                            <td>PEB Gabungan</td>
                            <td>
                                <input type="text" name="HEADER[JMBRGGAB]" id="JMBRGGAB" value="<?= $HEADER['JMBRGGAB']; ?>" class="sssstext" />
                                <span class="outentry"><button class="btn" id="cmdPEBGAB" style="float: right; margin-right: 10px" onclick="frmPEBGab($('#CAR').val(),$('#JMBRGGAB').val())"> PEB Gabungan</button></span>
                            </td>
                        </tr>
                        <tr id="lokasiTPB">
                            <td> &nbsp;&nbsp;&nbsp;&nbsp;Lokasi TPB</td>
                            <td><input type="text"  name="HEADER[LOKTPB]" id="LOKTPB" value="<?= $HEADER['LOKTPB']; ?>" onblur="checkCode(this);" grp="kpbc" urai="URLOKTPB" class="sstext" maxlength="9" />
                                <button onclick="tb_search('kpbc', 'LOKTPB;URLOKTPB', 'Kode Kpbc', this.form.id, 650, 400);" class="btn">...</button>
                                <span id="URLOKTPB"><?= $HEADER['URLOKTPB'] ?></span>
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <table width="100%">
                        <tr>
                            <td style="text-align: right;" >
                                <button onclick="cekValidasiHDR($('#CAR').val())" class="btn onterus outentry"><span class="icon-check"></span> Periksa Kelengkapan [ <span id="URSTATUS"><?= $HEADER['URSTATUS']; ?></span> ] </button> 
                            </td>
                        </tr>
                    </table>
                    <br>
                    <fieldset>
                        <legend> H. KOLOM KUSUS BEA DAN CUKAI </legend>
                        <table width="100%">
                            <tr>
                                <td width="135px">No / Tgl Pendaftaran</td>
                                <td>
                                    <input type="text" id="NODAFT" value="<?= $HEADER['NODAFT']; ?>" class="mtext" readonly="readonly"/>
                                    <input type="text" id="TGDAFT" class="sstext" value="<?= $HEADER['TGDAFT']; ?>" maxlength="10" formatDate="yes" readonly="readonly"/>
                                </td>
                            </tr>
                            <tr>
                                <td>No / Tgl BC 1.1</td>
                                <td>
                                    <input type="text" id="NOBC11" value="<?= $HEADER['NOBC11']; ?>" class="mtext" readonly="readonly"/>
                                    <input type="text" id="TGBC11" class="sstext" value="<?= $HEADER['TGBC11']; ?>" maxlength="10" formatDate="yes" readonly="readonly"/>
                                </td>
                            </tr>
                            <tr>
                                <td>No Pos - No SubPos</td>
                    <td>
                        <input type="text" id="POSBC11" value="<?= $HEADER['POSBC11']; ?>" class="ssstext" readonly="readonly"/> - 
                        <input type="text" id="SUBPOSBC11" value="<?= $HEADER['SUBPOSBC11']; ?>" class="ssstext" readonly="readonly" />
                    </td>
                            </tr>
                            <tr>
                                <td colspan="2"><button class="btn" style="float: right; margin-right: 10px"> Dokumen Lain </button></td>
                            </tr>
                            
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
        <div class="judul">DATA PEMBERITAHUAN :</div>
        <table width="100%" border="0">
            <tr>
                <td width="50%" valign="top">
                    <fieldset>
                        <legend>Eksportir</legend>
                        <table width="100%" class="<?= $ro ?>">
                            <tr>
                                <td width="135px">Identitas</td>
                                <td>
                                    <?= form_dropdown('HEADER[IDEKS]', $JENIS_IDENTITAS, $HEADER['IDEKS'], 'id="IDEKS" class="mtext"'); ?> <input type="text" name="HEADER[NPWPEKS]" id="NPWPEKS" url="<?= site_url('referensi/autoComplate/EKSPORTIR/NPWPEKS'); ?>" urai="IDEKS;NPWPEKS;NAMAEKS;ALMTEKS;NIPER;STATUSH;NOTDP;TGTDP" onfocus="Autocomp(this.id);unformatNPWP(this.id);" onblur="formatNPWP(this.id);" value="<?= $HEADER['NPWPEKS']; ?>" class="smtext"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Nama</td>
                                <td><input type="text" name="HEADER[NAMAEKS]" id="NAMAEKS" value="<?= $HEADER['NAMAEKS']; ?>" url="<?= site_url('referensi/autoComplate/EKSPORTIR/NAMAEKS'); ?>" urai="IDEKS;NPWPEKS;NAMAEKS;ALMTEKS;NIPER;STATUSH;NOTDP;TGTDP" onfocus="Autocomp(this.id);" class="ltext"/> 
                                    <button onclick="tb_search('eksportir', 'IDEKS;NPWPEKS;NAMAEKS;ALMTEKS;NIPER;STATUSH;NOTDP;TGTDP', 'Eksportir', this.form.id, 650, 400)" class="btn">...</button></td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top" >Alamat</td>
                                <td>
                                    <textarea name="HEADER[ALMTEKS]" id="ALMTEKS" class="ltext" onkeyup="limitChars(this.id, 70, 'limitALMTEKS')"><?= $HEADER['ALMTEKS']; ?></textarea>
                                    <div id="limitALMTEKS"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>NIPER</td>
                                <td>
                                    <input type="text" name="HEADER[NIPER]" id="NIPER" value="<?= $HEADER['NIPER']; ?>" class="ssstext"/> [Kusus fasilitas KITE]
                                </td>
                            </tr>
                            <tr>
                                <td>Status</td>
                                <td>
                                    <?= form_dropdown('HEADER[STATUSH]', $STATUSH, $HEADER['STATUSH'], 'id="STATUSH" class="vltext" '); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>No & Tgl TDP</td>
                                <td>
                                    <input type="text" name="HEADER[NOTDP]" id="NOTDP" value="<?= $HEADER['NOTDP']; ?>" class="stext"/> <input type="text" name="HEADER[TGTDP]" id="TGTDP" class="sstext date" value="<?= $HEADER['TGTDP']; ?>" maxlength="10" formatDate="yes"/>
                                    <button onclick="javascript : indentorQQ('IDQQ;NPWPQQ;NAMAQQ;ALMTQQ;NIPERQQ', this.form.id);" class="btn" >Indentor</button>
                                </td>
                            </tr>
                        </table>
                        
                        <input type="hidden" name="HEADER[IDQQ]" id="IDQQ" value="<?= $HEADER['IDQQ']?>">
                        <input type="hidden" name="HEADER[NPWPQQ]" id="NPWPQQ" value="<?= $HEADER['NPWPQQ']?>">
                        <input type="hidden" name="HEADER[NAMAQQ]" id="NAMAQQ" value="<?= $HEADER['NAMAQQ']?>">
                        <input type="hidden" name="HEADER[ALMTQQ]" id="ALMTQQ" value="<?= $HEADER['ALMTQQ']?>">
                        <input type="hidden" name="HEADER[NIPERQQ]" id="NIPERQQ" value="<?= $HEADER['NIPERQQ']?>">
                        
                    </fieldset>
                    
                    <fieldset>
                        <legend>Penerima</legend>
                        <table width="100%">
                            <tr>
                                <td width="135px">Nama</td>
                                <td>
                                    <input type="text" name="HEADER[NAMABELI]" id="NAMABELI" url="<?= site_url('referensi/autoComplate/PEMBELI'); ?>" urai="NAMABELI;ALMTBELI;NEGBELI;URNEGBELI" onfocus="Autocomp(this.id);" value="<?= $HEADER['NAMABELI']; ?>" class="mtext"/>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top" >Alamat</td>
                                <td>
                                    <textarea name="HEADER[ALMTBELI]" id="ALMTBELI" class="ltext" onkeyup="limitChars(this.id, 70, 'limitALMTBELI')"><?= $HEADER['ALMTBELI']; ?></textarea>
                                    <div id="limitALMTBELI"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>Negara</td>
                                <td> 
                                    <input type="text" name="HEADER[NEGBELI]" id="NEGBELI" value="<?= $HEADER['NEGBELI']; ?>" class="sssstext" onblur="checkCode(this);" grp="negara" urai="URNEGBELI"/> <button onclick="tb_search('negara', 'NEGBELI;URNEGBELI', 'Kode Negara', this.form.id, 650, 400);" class="btn">...</button> <span id="URNEGBELI"><?= $HEADER['URNEGBELI']; ?></span>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                    <?php 
                    if ($HEADER['TIPE_TRADER'] == '2') {?>
                        <fieldset>
                            <legend>PPJK</legend>
                            <table width="100%" class="offterus">
                                <tr>
                                    <td width="135px">Identitas</td>
                                    <td>
                                        <?= form_dropdown('HEADER[IDPPJK]', $JENIS_IDENTITAS , $HEADER['IDPPJK'], 'id="IDPPJK" class="mtext"'); ?> <input type="text" name="HEADER[NPWPPPJK]" id="NPWPPPJK" value="<?= $HEADER['NPWPPPJK']; ?>" class="smtext"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Nama</td>
                                    <td><input type="text" name="HEADER[NAMAPPJK]" id="NAMAPPJK" value="<?= $HEADER['NAMAPPJK']; ?>" class="ltext"/></td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top" >Alamat</td>
                                    <td>
                                        <textarea name="HEADER[ALMTPPJK]" id="ALMTPPJK" class="ltext" onkeyup="limitChars(this.id, 70, 'limitALMTPPJK')"><?= $HEADER['ALMTPPJK']; ?></textarea>
                                        <div id="limitALMTPPJK"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>NPP PPJK</td>
                                    <td>
                                        <input type="text" name="HEADER[NOPPJK]" id="NOPPJK" value="<?= $HEADER['NOPPJK']; ?>" class="mtext"/> <input type="text" name="HEADER[TGPPJK]" id="TGPPJK" class="sstext" value="<?= $HEADER['TGPPJK']; ?>" maxlength="10"/>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    <?php } ?>
                    <fieldset>
                        <legend>Data Sarana Angkut</legend>
                        <table width="100%">
                            <tr>
                                <td width="135px">Moda - Nama</td>
                                <td>
                                    <?= form_dropdown('HEADER[MODA]', $MODA, $HEADER['MODA'], '  id="MODA" class="stext"'); ?> <input type="text" name="HEADER[ANGKUTNAMA]" id="ANGKUTNAMA" value="<?= $HEADER['ANGKUTNAMA']; ?>" class="mtext" url="<?= site_url('referensi/autoComplate/MODA/ANGKUTNAMA'); ?>" urai="MODA;ANGKUTNAMA;ANGKUTFL;URANGKUTFL" onfocus="Autocomp(this.id);" />
                                </td>
                            </tr>
                            <tr>
                                <td>No. Voy/Flight </td>
                                <td>
                                    <input type="text" name="HEADER[ANGKUTNO]" id="ANGKUTNO" value="<?= $HEADER['ANGKUTNO']; ?>" class="stext" maxlength="7" />
                                </td>
                            </tr>
                            <tr>
                                <td>Bendera</td>
                                <td>
                                    <input type="text" name="HEADER[ANGKUTFL]" id="ANGKUTFL" value="<?= $HEADER['ANGKUTFL']; ?>" class="sssstext" onblur="checkCode(this);" grp="negara" urai="URANGKUTFL"/> <button onclick="tb_search('negara', 'ANGKUTFL;URANGKUTFL', 'Kode Negara', this.form.id, 650, 400);" class="btn">...</button> <span id="URANGKUTFL"><?= $HEADER['URANGKUTFL']; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Perkiraan Tgl Ekspor&nbsp;</td>
                                <td>
                                    <input type="text" name="HEADER[TGEKS]" id="TGEKS" value="<?= $HEADER['TGEKS'] ?>" formatDate="yes" class="sstext">&nbsp;
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
                <td valign="top">
                    <fieldset>
                        <legend>Data Dokumen</legend>
                        <table width="100%">
                            <tr id="DATA_DOKUMEN" class="outentry" >
                                <td colspan="2">
                                    <?= $DATA_DOKUMEN; ?>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                    <fieldset>
                        <legend>Data Pelabuhan</legend>
                        <table width="100%">
                            <tr>
                                <td width="135px">Pel Muat Asal</td>
                                <td>
                                    <input type="text" name="HEADER[PELMUAT]" id="PELMUAT" value="<?= $HEADER['PELMUAT']; ?>" onblur="checkCode(this);" grp="pelabuhan" urai="URPELMUAT" class="ssstext"/> <button onclick="tb_search('pelabuhan', 'PELMUAT;URPELMUAT', 'Kode Pelabuhan Muat', this.form.id, 650, 400);" class="btn">...</button> <span id="URPELMUAT"><?= $HEADER['URPELMUAT']; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Pel/Temp Muat Ekspor</td>
                                <td>
                                    <input type="text" name="HEADER[PELMUATEKS]" id="PELMUATEKS" value="<?= $HEADER['PELMUATEKS']; ?>" onblur="checkCode(this);" grp="pelabuhan" urai="URPELMUATEKS" class="ssstext"/> <button onclick="tb_search('pelabuhan', 'PELMUATEKS;URPELMUATEKS', 'Kode Pelabuhan Muat Ekspor', this.form.id, 650, 400);" class="btn">...</button> <span id="URPELMUATEKS"><?= $HEADER['URPELMUATEKS']; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Pel Transit Ekspor</td>
                                <td>
                                    <input type="text" name="HEADER[PELTRANSIT]" id="PELTRANSIT" value="<?= $HEADER['PELTRANSIT']; ?>" onblur="checkCode(this);" grp="pelabuhan" urai="URPELTRANSIT" class="ssstext"/> <button onclick="tb_search('pelabuhan', 'PELTRANSIT;URPELTRANSIT', 'Kode Pelabuhan Transit', this.form.id, 650, 400);" class="btn">...</button> <span id="URPELTRANSIT"><?= $HEADER['URPELTRANSIT']; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Pel Bongkar</td>
                                <td>
                                    <input type="text" name="HEADER[PELBONGKAR]" id="PELBONGKAR" value="<?= $HEADER['PELBONGKAR']; ?>" onblur="checkCode(this);" grp="pelabuhan" urai="URPELBONGKAR" class="ssstext"/> <button onclick="tb_search('pelabuhan', 'PELBONGKAR;URPELBONGKAR', 'Kode Pelabuhan Bongkar', this.form.id, 650, 400);" class="btn">...</button> <span id="URPELBONGKAR"><?= $HEADER['URPELBONGKAR']; ?></span>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                    <fieldset>
                        <legend>Data Tempat Pemeriksaan</legend>
                        <table width="100%">
                            <tr>
                                <td width="135px">Lokasi / Tanggal Pemeriksa</td>
                                <td>
                                    <?= form_dropdown('HEADER[KDLOKBRG]', $TMP_PERIKSA, $HEADER['KDLOKBRG'], '  id="KDLOKBRG" class="ltext"'); ?>
                                    <input type="text" name="HEADER[TGSIAP]" id="TGSIAP" value="<?= $HEADER['TGSIAP'] ?>" onmousemove="ShowDP(this.id)" class="sstext" maxlength="10" formatDate="yes">
                                </td>
                            </tr>
                            <tr>
                                <td>KPBC Periksa</td>
                                <td>
                                    <input type="text"  name="HEADER[KDKTRPRIKS]" id="KDKTRPRIKS" value="<?= $HEADER['KDKTRPRIKS']; ?>" onblur="checkCode(this);" grp="kpbc" urai="URKDKTRPRIKS" onfocus="Autocomp(this.id)" class="ssstext" maxlength="6" /> <button onclick="tb_search('kpbc', 'KDKTRPRIKS;URKDKTRPRIKS', 'Kode Kpbc', this.form.id, 650, 400);" class="btn">...</button> <span id="URKDKTRPRIKS"><?= $HEADER['URKDKTRPRIKS']; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Daerah Asal Barang</td>
                                <td>
                                    <input type="text" name="HEADER[PROPBRG]" id="PROPBRG" value="<?= $HEADER['PROPBRG']; ?>" onblur="checkCode(this);" grp="daerah" urai="URPROPBRG" class="ssstext"/> <button onclick="tb_search('propinsi', 'PROPBRG;URPROPBRG', 'Propinsi asal barang', this.form.id, 650, 400);" class="btn">...</button> &nbsp;<span id="URPROPBRG"><?= $HEADER['URPROPBRG'];?></span>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                    <fieldset id="n_harga">
                        <legend>Data Transaksi Ekspor</legend>
                        <table width="100%">
                            <tr>
                                <td width="135px">Cara Penyerahan Barang</td>
                                <td>
                                    <?= form_dropdown('HEADER[DELIVERY]', $DELIVERY, $HEADER['DELIVERY'], '  id="DELIVERY" class="vltext" onchange="JnsByr(this.value)"'); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Valuta Asing</td>
                                <td>
                                    <input type="text" name="HEADER[KDVAL]" id="KDVAL" value="<?= $HEADER['KDVAL']; ?>" onblur="checkCode(this);" grp="valuta" urai="URKDVAL"  class="ssstext" maxlength="3" /> <button onclick="tb_search('valuta', 'KDVAL;URKDVAL', 'Kode Valuta', this.form.id, 650, 400);" class="btn">...</button> <span id="URKDVAL"><?= $HEADER['URKDVAL']; ?></span>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td>Nilai Tukar Rupiah</td>
                                <td>
                                    <input type="text" name="HEADER[NILKURS]" id="NILKURS" value="<?= number_format($HEADER['NILKURS'],2); ?>" class="sstext" numeral="yes" format="0,0.00" style="text-align: right;"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Jenis Asuransi</td>
                                <td>
                                    <?= form_dropdown('HEADER[KDASS]', $KDASS, $HEADER['KDASS'], '  id="KDASS" class="smtext"'); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>FOB</td>
                                <td>
                                    <input type="text" name="HEADER[FOB]" id="FOB" value="<?= number_format($HEADER['FOB'],4); ?>" class="stext" numeral="yes" format="0,0.0000" style="text-align: right;" onblur="hitungNilai();"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Nilai Freight</td>
                                <td>
                                    <input type="text" name="HEADER[FREIGHT]" id="FREIGHT" value="<?= number_format($HEADER['FREIGHT'],4); ?>" class="stext" numeral="yes" format="0,0.0000" style="text-align: right;" onblur="hitungNilai();"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Nilai Asuransi</td>
                                <td>
                                    <input type="text" name="HEADER[ASURANSI]" id="ASURANSI" value="<?= number_format($HEADER['ASURANSI'],4); ?>" class="stext" numeral="yes" format="0,0.0000" style="text-align: right;" onblur="hitungNilai();"/>
                                </td>
                            </tr>
                            
                            <!-- <tr>
                                <td colspan="2" align="center"><br />
                                    <button onclick="EditHarga('CIF;FOB;FREIGHT;ASURANSI;KDHRG', this.form.id)" class="btn"> Edit Harga </button>
                                </td>
                            </tr>-->
                           
                            
                        </table>
                    </fieldset>
                    <fieldset>
                        <legend>Lain Lain</legend>
                        <table width="100%">
                            <tr>
                                <td width="135px">Negara Tujuan</td>
                                <td>
                                    <input type="text" name="HEADER[NEGTUJU]" id="NEGTUJU" value="<?= $HEADER['NEGTUJU']; ?>" class="sssstext" onblur="getTOD($('#MODA').val(),$(this).val(),$('#TGDAFT').val());hitungNilai();" > 
                                    <button onclick="tb_search('negara', 'NEGTUJU;URNEGTUJU', 'Kode Negara', this.form.id, 650, 400);" class="btn">...</button>
                                    <span id="URNEGTUJU"><?= $HEADER['URNEGTUJU']; ?></span>
                                    <input type="hidden" id="REGION" value="<?= $HITUNG['REGION']; ?>" class="sssstext">
                                    <input type="hidden" id="NFREIGHT" value="<?= $HITUNG['NFREIGHT']; ?>" class="sssstext">
                                    <input type="hidden" id="NASURANSI" value="<?= $HITUNG['NASURANSI']; ?>" class="sssstext">
                                </td>
                            </tr>
                            <tr>
                                <td>Komoditi</td>
                                <td colspan="3">
                                    <?= form_dropdown('HEADER[KOMODITI]', $KOMODITI, $HEADER['KOMODITI'], ' id="KOMODITI" class="smtext"'); ?>
                                </td>
                            </tr>
                            <tr>
                                <td width="100px">Volume</td>
                                <td>
                                    <input type="text" name="HEADER[VOLUME]" id="VOLUME" value="<?= number_format($HEADER['VOLUME'],4); ?>" class="sstext" numeral="yes" format="0,0.0000" style="text-align: right;"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Bruto</td>
                                <td>
                                    <input type="text" name="HEADER[BRUTO]" id="BRUTO" value="<?= number_format($HEADER['BRUTO'],4); ?>" class="sstext" numeral="yes" format="0,0.0000" style="text-align: right;"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Netto</td>
                                <td>
                                    <input type="text" name="HEADER[NETTO]" id="NETTO" value="<?= number_format($HEADER['NETTO'],4); ?>" class="sstext" numeral="yes" format="0,0.0000" style="text-align: right;"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Jumlah Barang</td>
                                <td>
                                    <input type="text" name="HEADER[JMBRG]" id="JMBRG" value="<?= number_format($HEADER['JMBRG'],4); ?>" class="sstext" numeral="yes" format="0,0.0000" style="text-align: right;"/>
                            </tr>
                            
                            <tr>
                                <td>Nilai BK (Rupiah)</td>
                                <td>
                                    <input readonly="readonly" type="text" name="HEADER[NILPE]" id="NILPE" value="<?= number_format($HEADER['NILPE'],4); ?>" class="sstext" numeral="yes" format="0,0.0000" style="text-align: right;"/>
                                </td>
                            </tr>
                            
                            <tr>
                                <td>PNBP</td>
                                <td>
                                    <input  type="text" name="HEADER[PNBP]" id="PNBP" value="<?= number_format($HEADER['PNBP'],4); ?>" class="sstext" numeral="yes" format="0,0.0000" style="text-align: right;"/>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr class="outentry" style="vertical-align: top;" >
                 <td width="50%">
                    <fieldset>
                        <legend><b>Kontainer / Peti Kemas</b></legend>
                        <?= $DATA_CONTAINER; ?>
                    </fieldset>
                </td>
                <td>
                    <fieldset>
                        <legend><b>Jumlah dan Jenis Kemasan</b></legend>
                        <?= $DATA_KEMASAN; ?>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <table width="50%" border="0" align="center" cellpadding="5">
                        <tr><td colspan="2" class="rowheight" align="center"><h5>DATA PENANDATANGAN DOKUMEN</h5></td></tr>
                        <tr align="center">
                            <td width="50%">TEMPAT</td>
                            <td width="50%">TANGGAL (DD-MM-YYYY)</td>
                        </tr>
                        <tr align="center">
                            <td>
                                <input type="text" name="HEADER[KOTA_TTD]" id="KOTA_TTD" value="<?= $HEADER['KOTA_TTD']; ?>" class="stext" style="text-align:center; width:150px"/></td>
                            <td>
                                <input type="text" name="HEADER[TGL_TTD]" id="TGL_TTD" value="<?= ($HEADER['TGL_TTD']=='')?date('Y-m-d'):$HEADER['TGL_TTD']; ?>" class="stext date" style="text-align:center; width:150px"/></td>
                        </tr>
                        <tr align="center">
                            <td colspan="2">NAMA</td>
                        </tr>
                        <tr align="center"><td colspan="2"><input type="text" name="HEADER[NAMA_TTD]" id="NAMA_TTD" value="<?= $HEADER['NAMA_TTD']; ?>" class="stext" style="text-align:center; width:150px"/></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</span>