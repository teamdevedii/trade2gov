<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
?>
<div class="content_luar">
    <div class="content_dalam">
        <form id="cetak_" method="post" autocomplete="off" onsubmit="return false;">
            <input id="CAR" type="text" value="<?= $car ?>">
            <fieldset>
                <legend> Pilihan Pencetakan </legend>
                <table width="100%">
                    <tr>
                        <td>
                            <input type="radio" name="CETAK" id="cetakPKBE" value="1" checked >     <label for="cetakPKBE">&nbsp;Cetak Dokumen PKBE</label><br>
                            <!--<input type="radio" name="CETAK" id="cetakKontainerKe" value="2">       <label for="cetakKontainerKe">&nbsp;Cetak Kontainer Ke:</label><br>-->
                            <input type="radio" name="CETAK" id="cetakPerPengStaffering" value="3"> <label for="cetakPerPengStaffering">&nbsp;Permohonan Pengawasan Stuffing</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br>
                            <button onclick="cetakNewWin($('.CETAK').val());" class="btn"><span class="icon-new-window"></span> Cetak </button>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
</div>
<script>
    $('#msgboxSSPCPx').html('');
    function cetakNewWin(id){
        if($('input[name="CETAK"]:checked').val()=='2'){
            //cetak($car,$jnPage,$frmSSPCP=0)
            Dialog(site_url+'/bc30/cetakPKBE/'+$('#CAR').val()+'/2/1', 'msgbox2', ' Data PKBE ', 650, 600);
        }else{
            window.open(site_url+"/bc30/cetakPKBE/"+$('#CAR').val()+'/'+$('input[name="CETAK"]:checked').val(),"Cetak Dokumen PKBE","scrollbars=yes, resizable=yes,width=1100,height=700");
        }
    }
</script>