<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
//print_r($CAR);die();
?>
<script>
    $(document).ready(function() {
        onoffPebGab(false);
        declareDP('fpebgab');
        f_MAXLENGTH('fpebgab');
        formatNPWP('fpebgab #NPWPEKSG');
        formatNumeral('fpebgab');
    });

    function onoffPebGab(on) {
        //cmdPKB
        if ($('#fpebgab #CAR').val() == '') {
            $('#fpebgab.outentry').hide();
        } else {
            $('#fpebgab.outentry').show();
        }
        if (on) {
            $('#fpebgab #on').show();
            $('#fpebgab #off').hide();
            $("#fpebgab :input").removeAttr("disabled");
            $('#fpebgab .outentry :input').attr('disabled', 'disabled');
        } else {
            $('#fpebgab #on').hide();
            $('#fpebgab #off').show();
            $("#fpebgab :input").attr('disabled', 'disabled');
            $('#fpebgab .outentry :input').removeAttr("disabled");
        }
        $('#fpebgab .onterus').removeAttr('disabled');
        $('#fpebgab .offterus :input').attr('readonly', 'readonly');
    }

    function addGabungan() {
        var total = ($('#JMBRGGAB').val());
        var awl = ($('#JMLGAB').val() == '') ? 0 : parseInt($('#JMLGAB').val());
        //alert(awl+'|'+total);
        if (awl < total) {
            $('#SERIEKS').val(awl + 1);
            $("#fpebgab #fieldset1 :input").val('');
            $("#fpebgab #fieldset2 :input").val('');
            $("#fpebgab #fieldset3 :input").val('');
        } else{
            jAlert('Jumlah Gabungan Lebih Dari Total di Header.',site_name);
            onoffPebGab(false);
        }
    }

    function navPage(tipe, page, jmlpage) {
        //alert(jmlpage); return false;
        var goPage = 1;
        var car = $('#fpebgab #CAR').val();
        jmlpage = parseInt(jmlpage);
        page = parseInt(page);
        switch (tipe) {
            case 'first':
                goPage = 1;
                break;
            case 'preview':
                if (page > 1) {
                    goPage = page - 1;
                } else if (page <= 1) {
                    goPage = 1;
                }
                break;
            case 'next':
                if (page < jmlpage) {
                    goPage = page + 1;
                } else if (page >= jmlpage) {
                    goPage = jmlpage;
                }
                break;
            case 'last':
                goPage = jmlpage;
                break;
        }
        call(site_url + '/bc30/pagePEBgabungan/' + car + '/' + goPage + '/' + jmlpage, 'navPebGab', '');
        //alert(goPage);
    }
    
    function GoToPage(car, jmbrggab){
        //alert(jmbrggab);return false;
        Dialog(site_url + '/bc30/GoToPagePEBg/' + car+'/'+jmbrggab, 'divgotoPEBg', '.: Pilih Data :.', 400, 450);
    }
    
    function cekValidasiPEBgab(car, serieks, jmbrggab) {
        //alert(car+'|'+jmbrggab+'|'+serieks);return false;
        $keys=car+','+serieks+','+jmbrggab;
        //alert($keys); return false;
        $(function() {
            $('#msgbox').html('');
            $("#msgbox").html('<div style="margin-left: 15px; margin-top: 15px;"><img src="' + base_url + 'img/loaders/facebook.gif" /> loading...</div>');
            $("#msgbox").dialog({
                resizable: false,
                height: 500,
                modal: true,
                width: 700,
                title: 'Validasi Dokumen',
                open: function() {
                    $.ajax({
                        type: 'POST',
                        url: site_url + '/bc30/alertcekstatusPEBgab/' +car+ '/' +serieks+ '/' +jmbrggab+ '/fpebgab',
                        success: function(data) {
                            $("#msgbox").html(data);
                            $('#URSTATUS').html($('#STATUSCEK').val());
                            call(site_url + '/bc30/RefFrmPEBGab/' + $keys, 'navPebGab');
                        }
                    });
                },
                buttons: {
                    Close: function() {
                        $(this).dialog("close");
                    }
                }
            });
        });
    }
</script>
<div class="content_luar" id="navPebGab" style="height: 98%; margin: 1px;">
    <div class="content_dalam" style="width: 98.6%">
        <form name="fpebgab" id="fpebgab" action="<?= site_url('bc30/setHeaderPEBGab'); ?>" method="post" autocomplete="off" onsubmit="return false;" >
            <?= $MAXLENGTH ?>
            <input type="hidden" name="act" id="act" value="<?= $act; ?>" />
            <input type="hidden" name="PEBGAB[CAR]" id="CAR" value="<?= $CAR ?>" />
            <input type="hidden" name="JMLGAB" id="JMLGAB" value="<?= $JMLGAB ?>" />
            <table style="width: 100%">
                <tr>
                    <td colspan="2">
                        <span id="off">
                            <button onclick="onoffPebGab(true);return false;" class="btn onterus"><span class="icon-pen"></span> Edit </button>
                            <button onclick="onoffPebGab(true);addGabungan();return false;" class="btn onterus"><span class="icon-aperture"></span> Add </button>
                        </span>
                        <span id="on">
                            <button onclick="save_post_msg('fpebgab','','navPebGab');onoffPebGab(false);" class="btn onterus"><span class="icon-download"></span> Simpan</button>
                            <button onclick="onoffPebGab(false);
                                    cancel('fpebgab');" class="btn onterus"><span class="icon-undo"></span> Batal</button>
                        </span><span class="msgheader_" style="margin-left:20px">&nbsp;</span>
                    </td>
                    <td style="float: right" class="outentry"> 
                        <button onclick="navPage('first', $('#fpebgab #SERIEKS').val(), $('#fpebgab #JMBRGGAB').val());
                                return false;" class="btn" style="height: 22px; width:25px; vertical-align: middle;" > <span class="icon-first"></span></button>

                        <button onclick="navPage('preview', $('#fpebgab #SERIEKS').val(), $('#fpebgab #JMBRGGAB').val());return false;" class="btn" style="-webkit-transform: rotate(180deg);-moz-transform: rotate(180deg);-o-transform: rotate(180deg);-ms-transform: rotate(180deg);transform: rotate(180deg); height: 22px; width:25px; vertical-align: middle;" > <span class="icon-play"></span></button>

                        <input type="text" id="NOBARANG" class="ssstext" value="<?= $PEBGAB['SERIEKS']; ?>" style="text-align: center;" readonly="readonly"/>

                        <button onclick="navPage('next', $('#fpebgab #SERIEKS').val(), $('#fpebgab #JMBRGGAB').val());
                                return false;" class="btn" style="height: 22px; width:25px; vertical-align: middle;"> <span class="icon-play"></span></button> 

                        <button onclick="navPage('last', $('#fpebgab #SERIEKS').val(), $('#fpebgab #JMBRGGAB').val());
                                return false;" class="btn" style="height: 22px; width:25px; vertical-align: middle;"><span class="icon-last"></span></button>
                        
                        
                            &nbsp;
                        <button onclick="GoToPage($('#CAR').val(),$('#JMBRGGAB').val())" class="btn" style="height: 22px; width:60px; vertical-align: middle;">Go To</button>
                    </td>
                </tr>
            </table>
            <table style="width: 100%;">
                <tr>
                    <td colspan="2">
                        <fieldset>
                            <table width="100%">
                                <tr>
                                    <td>Nomor Aju</td>
                                    <td>
                                        : <b><?= substr($CAR, 0, 6) . '-' . substr($CAR, 6, 6) . '-' . substr($CAR, 12, 8) . '-' . substr($CAR, 20, 6); ?></b>
                                    </td>
                                    <td style="text-align: right" class="outentry">
                                        <button onclick="cekValidasiPEBgab($('#fpebgab #CAR').val(),$('#fpebgab #SERIEKS').val(),$('#fpebgab #JMBRGGAB').val())" class="btn" id="GABOK"><span class="<?= ($PEBGAB['GABOK']!='1')?'icon-x':'icon-check'; ?>"></span> <b style="color: #ffffff"><?= $PEBGAB['UGABOK'] ?></b></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td> Gabungan Ke-</td>
                                    <td>
                                        : <input type="text" name="PEBGAB[SERIEKS]" id="SERIEKS" value="<?= $PEBGAB['SERIEKS']; ?>" class="sssstext" readonly="readonly"/>
                                        &nbsp; dari &nbsp;
                                        <input type="text" name="PEBGAB[JMBRGGAB]" id="JMBRGGAB" class="sssstext" value="<?= $PEBGAB['JMBRGGAB'] ?>"readonly="readonly"/>
                                    </td>

                                    <td style="float: right">Nomor LPBC &nbsp;
                                        <input type="text" name="PEBGAB[NOLPSEG]" id="NOLPSEG" class="stext" readonly="readonly"/>
                                        &nbsp;&nbsp;&nbsp;
                                        <input type="text" name="PEBGAB[TGLPSEG]" id="TGLPSEG" class="sstext" readonly="readonly"/>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <fieldset id="fieldset1">
                            <legend>Data Eksportir</legend>
                            <table width="100%" class="<?= $ro ?>">
                                <tr>
                                    <td width="135px">Identitas</td>
                                    <td>
                                        <?= form_dropdown('PEBGAB[IDEKSG]', $JENIS_IDENTITAS, $PEBGAB['IDEKSG'], 'id="IDEKSG" class="mtext"'); ?> 
                                        <input type="text" name="PEBGAB[NPWPEKSG]" id="NPWPEKSG" url="<?= site_url('referensi/autoComplate/EKSPORTIR/NPWPEKS'); ?>" urai="IDEKSG;NPWPEKSG;NAMAEKSG;ALMTEKSG;NIPERG" onfocus="Autocomp(this.id);unformatNPWP(this.id);" onblur="formatNPWP(this.id);" value="<?= $PEBGAB['NPWPEKSG']; ?>" class="smtext"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Nama</td>
                                    <td><input type="text" name="PEBGAB[NAMAEKSG]" id="NAMAEKSG" value="<?= $PEBGAB['NAMAEKSG']; ?>" url="<?= site_url('referensi/autoComplate/EKSPORTIR/NAMAEKS'); ?>" urai="IDEKSG;NPWPEKSG;NAMAEKSG;ALMTEKSG;NIPERG" onfocus="Autocomp(this.id);" class="ltext"/></td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top" >Alamat</td>
                                    <td>
                                        <textarea name="PEBGAB[ALMTEKSG]" id="ALMTEKSG" class="ltext" onkeyup="limitChars(this.id, 70, 'limitALMTEKSG')"><?= $PEBGAB['ALMTEKSG']; ?></textarea>
                                        <div id="limitALMTEKSG"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>NIPER</td>
                                    <td>
                                        <input type="text" name="PEBGAB[NIPERG]" id="NIPERG" value="<?= $PEBGAB['NIPERG']; ?>" class="ssstext"/>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td style="width: 50%">
                        <fieldset id="fieldset2">
                            <legend>Fasilitas Yang Diminta</legend>
                            <table>
                                <?php
                                $fasilitas = $PEBGAB['FASILITASG'];
                                $arr[$fasilitas] = 'checked';
                                ?>
                                <tr>
                                    <td>
                                        <input type="radio" name="PEBGAB[FASILITASG]" id='FASILITASGA' value="A" <?= $arr['A'] ?>/>
                                        <label for='FASILITASGA'>A. Pembebasan BM & Penangguhan PPN & PPnBM</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="radio" name="PEBGAB[FASILITASG]" id='FASILITASGB' value="B" <?= $arr['B'] ?>/>
                                        <label for='FASILITASGB'>B. Pengembalian BM & Penangguhan PPN & PPnBM</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="radio" name="PEBGAB[FASILITASG]" id='FASILITASGAC'value="C" <?= $arr['C'] ?> />
                                        <label for='FASILITASGAC'>C. Gabungan A dan B</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Zoning KITE <?= form_dropdown('PEBGAB[KPKERG]', $ZONING_KITE, $PEBGAB['KPKERG'], 'id="KPKERG" class="mtext"'); ?>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                    <td style="width: 50%; vertical-align: top;">
                        <fieldset id="fieldset3">
                            <legend>Nilai Ekspor</legend> <!-- filed nilai ekspor ok --->
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 80px;">Valuta</td>
                                    <td>
                                        <input type="text"  name="PEBGAB[KDVALG]" id="KDVALG" value="<?= $PEBGAB['KDVALG']; ?>" onblur="checkCode(this);" grp="valuta" urai="UKDVALG" class="ssstext" maxlength="6" /> &nbsp;
                                        <button onclick="tb_search('valuta', 'KDVALG;UKDVALG', 'Kode Valuta', this.form.id, 650, 400);" class="btn"> ... </button>
                                        <span id="UKDVALG"><?= $PEBGAB['UKDVALG'] ?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>FOB</td>
                                    <td>
                                        <input type="text" name="PEBGAB[FOBG]" id="FOBG" value="<?= number_format($PEBGAB['FOBG'],2); ?>" class="stext" numeral="yes" format="0,0.00" style="text-align: right;"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Item Barang </td>
                                    <td>
                                        <input type="textd" name="PEBGAB[JMBRGG]" id="JMBRGG" class="sssstext" value="<?= $PEBGAB['JMBRGG']; ?>"/>&nbsp; 
                                        <?php if ($PEBGAB['NPWPEKSG'] != '' && $PEBGAB['JMBRGG'] > 0) { ?><span class="outentry" style="float: right;"><button class="btn" id="cmdPKB" style="float: right; margin-right: 10px" onclick="frmPEBGabDtl($('#CAR').val(), $('#JMBRGG').val(), $('#SERIEKS').val())"> PEB </button></span>
                                        <?php } ?>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>