<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR); 
?>
<span id="fbarang_form" style=" width: 100%">
    <?php if (!$list) { ?>
    <h4>
        <table style="width: 100%">
            <tr>
                <td style="width: 70%">
                    <div class="ibutton" >
                        <button onclick="save_detil('#fbarang_', 'msgbarang_');cekValidasiDtl($('#fbarang_ #NOMOR_AJU').val(),$('#fbarang_ #seri').val());return false;" class="btn"><span class="icon-download"></span> Simpan</button>
                        <button onclick="$('#fbarang_form').slideDown('slow');$('#fbarang_form').html('');" class="btn"><span class="icon-undo"></span> Batal</button>
                        <span class="msgbarang_" style="margin-left:20px">&nbsp;</span>
                    </div>
                </td>
                <td style="text-align: center"> 
                    <button onclick="navPage('first',$('#fbarang_ #seri').val(),$('#fbarang_ #JMLDTL').val());return false;" class="btn" style="width:5px; padding-left: 22px;">
                        <span class="icon-first" style=""></span>
                    </button> 
                    <button onclick="navPage('preview',$('#fbarang_ #seri').val(),$('#fbarang_ #JMLDTL').val());return false;" class="btn" style="width:5px; padding-left: 22px">
                        <span class="icon-preview"></span>
                    </button>
                    <input type="text" id="NOBARANG" class="ssstext" value="<?= $sessDETIL['SERIAL']; ?>" style="text-align: center;" />
                    <button onclick="navPage('next',$('#fbarang_ #seri').val(),$('#fbarang_ #JMLDTL').val());return false;" class="btn" style="width:5px; padding-left: 22px">
                        <span class="icon-play"></span>
                    </button> 
                    <button onclick="navPage('last',$('#fbarang_ #seri').val(),$('#fbarang_ #JMLDTL').val());return false;" class="btn" style="width:5px; padding-left: 22px">
                        <span class="icon-last"></span>
                    </button> 
                </td>
            </tr>
        </table>
    </h4>
    <div id="navBarang"><?= $from ?></div>
    <script>
    function navPage(tipe,page,jmlpage){
        var goPage = 1;
        var car  = $('#fbarang_ #NOMOR_AJU').val();
        jmlpage = parseInt(jmlpage);
        page = parseInt(page);
        switch (tipe){
            case 'first':
                goPage = 1;
                break;
            case 'preview':
                if(page>1){
                    goPage = page - 1; 
                }else if(page<=1){
                    goPage = 1; 
                }
                break;
            case 'next':
                if(page<jmlpage){
                    goPage = page + 1; 
                }else if(page>=jmlpage){
                    goPage = jmlpage; 
                }
                break;
            case 'last':
                goPage = jmlpage;
                break;
        }
        $('#navBarang').slideDown("slow"); 
        $('#navBarang').html('<div style="margin-left: auto; margin-top: auto;"><img src="' + base_url + 'img/_load.gif" /> loading...</div>');
        $.ajax({
            url: site_url +'/bc20/pageBarang/'+car+'/'+goPage,
            type : 'POST'
        }).done(function(data){
            $('#navBarang').toggle(function(){
                $('#NOBARANG').val(goPage);
                $('#navBarang').html(data);
                $('#navBarang').slideDown("slow"); 
            });
        });
        //alert(goPage);
    
    }
    </script>
<?php } ?>
</span>
<?php if (!$edit) { ?>
    <div id="fbarang_list" style="margin-top:10px"><?= $list ?></div>
<?php } ?>