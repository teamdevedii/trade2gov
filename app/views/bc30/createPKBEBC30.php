<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
$ro = '';
if ($TIPE_TRADER == '1') {
    $ro = 'readonly';
}
?>
<div id="DivHeaderForm">
    <span id="divtmp"></span>
    <form id="frmPKBEHdr" name="frmPKBEHdr" action="<?= site_url('bc30/setHeaderPKBE'); ?>" method="post" autocomplete="off" onsubmit="return false;" class="form uniformForm">
        <?= $MAXLENGTH ?>
        <input type="hidden" name="act" id="act" value="<?= $act; ?>" />
        <input type="hidden" name="HEADER[CAR]" id="noaju" value="<?= $HEADER['CAR']; ?>" />
        <input type="hidden" name="HEADER[SERIPE]" id="seripe" value="<?= $HEADER['SERIPE']; ?>" />
        <div class="content_luar">
            <div class="content_dalam">
                <table style="width: 100%">
                    <tr>
                        <td colspan="2">
                            <span id="off">
                                <button id="" onclick="onoff(true);return false;" class="btn onterus"><span class="icon-pen"></span> Edit </button>
                            </span>
                            <span id="on">
                                <button onclick="save_post_msg('frmPKBEHdr');" class="btn onterus">
                                    <span class="icon-download"></span> Simpan
                                </button>
                                <button onclick="onoff(false);" class="btn onterus">
                                    <span class="icon-undo"></span> Batal
                                </button>
                            </span>
                            <span class="msgheader_" style="margin-left:20px">&nbsp;</span>
                            <?php if ($HEADER['CAR']) { ?>
                                <button id="btnQUEUED" onclick="gotoQUEUED($('#noaju').val())" class="btn onterus" style="float: right; margin-right: 5px;" ><span class="icon-mail"></span> Bentuk / Batal Edifact</button>
                            <?php } ?>
                            <?php if ($HEADER['CAR']) { ?>
                                <fieldset style="margin-top:5px;">
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="width: 100px;">Nomor Aju</td>
                                            <td> : <b><?= substr($HEADER['CAR'], 0, 6) . '-' . substr($HEADER['CAR'], 7, 6) . '-' . substr($HEADER['CAR'], 12, 8) . '-' . substr($HEADER['CAR'], 20, 6); ?></b>
                                                <span style="float: right;">
                                                    <button id="btnValidasi" onclick="cekValidasi($('#noaju').val())" class="btn onterus">
                                                        <span class="icon-check"></span> 
                                                        Periksa Kelengkapan [ <span id="URSTATUS"><?= $HEADER['URSTATUS']; ?></span> ]
                                                    </button>
                                                </span>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            <?php } ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%; vertical-align: top;" >
                            <fieldset>
                                <legend>Konsolidator / Eksportir</legend>
                                <table style="width:100%">
                                    <tr>
                                        <td width="125px">Identitas</td>
                                        <td>
                                            <?= form_dropdown('HEADER[IDEKS]', $IDEKS, $HEADER['IDEKS'], 'id="IDEKS" class="offterus smtext"'); ?>
                                            <input type="text" name="HEADER[NPWPEKS]" id="NPWPEKS" url="<?= site_url('referensi/autoComplate/EKSPORTIR/NPWPEKS'); ?>" urai="IDEKS;NPWPEKS;NAMAEKS;ALMTEKS" onfocus="Autocomp(this.id);unformatNPWP(this.id);" onblur="formatNPWP(this.id);" value="<?= $this->fungsi->FORMATNPWP($HEADER['NPWPEKS']); ?>" class="offterus smtext" <?= $ro; ?>/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nama </td>
                                        <td><input type="text" name="HEADER[NAMAEKS]" id="NAMAEKS" value="<?= $HEADER['NAMAEKS']; ?>" readonly="readonly" class="offterus vltext"/></td>
                                    </tr>
                                    <tr>
                                        <td>Alamat </td>
                                        <td>
                                            <textarea name="HEADER[ALMTEKS]" id="ALMTEKS" readonly="readonly" rows="1" cols="35" class="offterus"><?= $HEADER['ALMTEKS']; ?></textarea>
                                    </tr>
                                    <tr>
                                        <td>NIPPER </td>
                                        <td><input type="text" name="HEADER[NIPER]" id="NIPER" value="<?= $HEADER['NIPER']; ?>" class="sstext"/></td>
                                    </tr>
                                    <tr>
                                        <td>Stat. Kons </td>
                                        <td><?= form_dropdown('HEADER[JNKONS]', $STATUS_KONS, $HEADER['JNKONS'], 'id="JNKONS" class="mtext"'); ?></td>
                                    </tr>
                                </table>
                            </fieldset>
                            <fieldset>
                                <legend>Kontainer</legend>
                                <table width="100%">
                                    <tr>
                                        <td style="width: 125px">No. Container </td>
                                        <td>
                                            <input type="text" class="ssstext" name="HEADER[NOCONT1]" id="NOCONT1" value="<?= substr($HEADER['NOCONT'], 0, 4) ?>" />
                                            <input type="text" class="mtext" name="HEADER[NOCONT2]" id="NOCONT2" value="<?= substr($HEADER['NOCONT'], 4) ?>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Size</td>
                                        <td>
                                            <?= form_dropdown('HEADER[SIZE]', $SIZE_CONT, $HEADER['SIZE'], 'id="SIZE" class="smtext"'); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>NIP Petugas Stuffing</td>
                                        <td>
                                            <input type="mstext" name="HEADER[NIPSTUFFING]" id="NIPSTUFFING" value="<?= $HEADER['NIPSTUFFING']; ?>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nama Petugas Stuffing</td>
                                        <td>
                                            <input type="mstext" name="HEADER[NAMASTUFFING]" id="NAMASTUFFING" value="<?= $HEADER['NAMASTUFFING']; ?>" />
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                        <td style="width: 50%;vertical-align: top;">
                            <fieldset>
                                <legend>Kantor Pelayanan Bea Cukai (KPBC)</legend>
                                <table style="width:100%">
                                    <tr>
                                        <td width="125px">KPBC Pendaftaran</td>
                                        <td colspan="3">
                                            <input type="text"  name="HEADER[KDKTR]" id="KDKTR" value="<?= $HEADER['KDKTR']; ?>" onblur="checkCode(this);" grp="kpbc" urai="URKDKTR" class="ssstext"/>
                                            <button onclick="tb_search('kpbc', 'KDKTR;URKDKTR', 'Kode Kpbc', this.form.id, 650, 400);" class="btn"> ... </button>
                                            <span id="URKDKTR"><?= $HEADER['UKDKTR'] ?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>KPBC Pemeriksaan</td>
                                        <td colspan="3">
                                            <input type="text"  name="HEADER[KDKTRPERIKSA]" id="KDKTRPERIKSA" value="<?= $HEADER['KDKTRPERIKSA']; ?>" onblur="checkCode(this);" grp="kpbc" urai="URKDKTRPERIKSA" class="ssstext"/>
                                            <button onclick="tb_search('kpbc', 'KDKTRPERIKSA;URKDKTRPERIKSA', 'Kode Kpbc', this.form.id, 650, 400);" class="btn"> ... </button>
                                            <span id="URKDKTRPERIKSA"><?= $HEADER['UKDKTRPERIKSA'] ?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>KPBC Pemuatan</td>
                                        <td colspan="3">
                                            <input type="text"  name="HEADER[KDKTRMUAT]" id="KDKTRMUAT" value="<?= $HEADER['KDKTRMUAT']; ?>" onblur="checkCode(this);" grp="kpbc" urai="URKDKTRMUAT" class="ssstext"/>
                                            <button onclick="tb_search('kpbc', 'KDKTRMUAT;URKDKTRMUAT', 'Kode Kpbc', this.form.id, 650, 400);" class="btn"> ... </button>
                                            <span id="URKDKTRMUAT"><?= $HEADER['UKDKTRMUAT'] ?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Hanggar TPS</td>
                                        <td>
                                            <input type="text"  name="HEADER[KDHANGGAR]" id="KDHANGGAR" value="<?= $HEADER['KDHANGGAR']; ?>" url="<?= site_url('referensi/autoComplate/HANGGAR/KDHGR'); ?>" urai="KDHANGGAR;URKDHANGGAR" onfocus="Autocomp(this.id, $('#KDKTR').val())" class="ssstext" />
                                            <button onclick="tb_search('hanggar', 'KDHANGGAR;URKDHANGGAR', 'Kode Hanggar', this.form.id, 650, 400, 'KDKTR;');" class="btn">...</button> 
                                            <span id="URKDHANGGAR"><?= $HEADER['URHGR']; ?></span>

                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                            <fieldset>
                                <legend>Pengangkut</legend>
                                <table style="width:100%">
                                    <tr>
                                        <td width="125px">Negara Tujuan</td>
                                        <td colspan="3">
                                            <input type="text"  name="HEADER[KDNEGTUJU]" id="KDNEGTUJU" value="<?= $HEADER['KDNEGTUJU']; ?>" onblur="checkCode(this);" grp="negara" urai="URKDNEGTUJU" class="sssstext"/>
                                            <button onclick="tb_search('negara', 'KDNEGTUJU;URKDNEGTUJU', 'Kode Negara', this.form.id, 650, 400);" class="btn"> ... </button>
                                            <span id="URKDNEGTUJU"><?= $HEADER['UKDNEGTUJU'] ?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100px">Moda - Nama</td>
                                        <td>
                                            <?= form_dropdown('HEADER[JNSCARRIER]', $MODA, $HEADER['JNSCARRIER'], '   id="JNSCARRIER" class="stext"'); ?>
                                            <input type="text" name="HEADER[CARRIER]" id="CARRIER" value="<?= $HEADER['CARRIER']; ?>" class="mtext" url="<?= site_url('referensi/autoComplate/MODA/ANGKUTNAMA'); ?>" urai="JNSCARRIER;CARRIER;BENDERA;UBENDERA" onfocus="Autocomp(this.id);" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Bendera</td>
                                        <td>
                                            <input type="text"  name="HEADER[BENDERA]" id="BENDERA" value="<?= $HEADER['BENDERA']; ?>" onblur="checkCode(this);" grp="negara" urai="UBENDERA" class="sssstext"/>
                                            <button onclick="tb_search('negara', 'BENDERA;UBENDERA', 'Kode Negara', this.form.id, 650, 400);" class="btn">...</button> 
                                            <span id="UBENDERA"><?= $HEADER['UBENDERA']; ?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Alamat Stuffing</td>
                                        <td>
                                            <textarea name="HEADER[ALMTSTUFFING]" id="ALMTSTUFFING" rows="1" cols="35"><?= $HEADER['ALMTSTUFFING']; ?></textarea>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Stuffing</td>
                                        <td>
                                            <input type="text" name="HEADER[TGSTUFFING]" id="TGSTUFFING" value="<?= $HEADER['TGSTUFFING']; ?>" class="sstext" formatDate="yes"/> &nbsp;&nbsp; Jam : <input type="text" name="HEADER[JAMSTUFFING]" id="JAMSTUFFING" value="<?= $HEADER['JAMSTUFFING']; ?>" class="ssstext" onblur="formatTime(this.id)"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Voy / Flight</td>
                                        <td><input type="text" name="HEADER[VOYFLIGHT]" id="VOYFLIGHT" value="<?= $HEADER['VOYFLIGHT']; ?>" class="sstext"/></td>
                                    </tr>

                                </table>
                            </fieldset>
                        </td>
                    </tr>
                    <?php if ($HEADER['CAR']) { ?>
                        <tr class="outentry">
                            <td colspan="2">
                                <fieldset>
                                    <legend>PEB/PE</legend>
                                    <div id="detilPKBE">
                                        <?= $DETIL; ?>
                                    </div>
                                </fieldset>
                            </td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td>
                            <fieldset>
                                <legend>Surat Permohonan Pengawasan</legend>
                                <table>
                                    <tr>
                                        <td width="125px">Nomor Surat</td>
                                        <td>
                                            <input type="text" name="HEADER[NOSURAT]" id="NOSURAT" value="<?= $HEADER['NOSURAT']; ?>" class="mtext"/>
                                            <input type="text" name="HEADER[TGSURAT]" id="TGSURAT" value="<?= $HEADER['TGSURAT']; ?>" class="sstext" formatDate="yes"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Petugas Ctk Person</td>
                                        <td>
                                            <input type="text" name="HEADER[PETUGAS]" id="PETUGAS" value="<?= $HEADER['PETUGAS']; ?>" class="mtext"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Telpon / HP</td>
                                        <td>
                                            <input type="text" name="HEADER[NOPHONE]" id="NOPHONE" value="<?= $HEADER['NOPHONE']; ?>" class="mtext"/>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                        <td>
                            <fieldset>
                                <legend>Penandatangan</legend> 
                                <table>
                                    <tr>
                                        <td width="125px">Kota</td>
                                        <td>
                                            <input type="text" name="HEADER[KOTA]" id="KOTA" value="<?= $HEADER['KOTA']; ?>" class="smtext"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal</td>
                                        <td>
                                            <input type="text" name="HEADER[TANGGAL]" id="TANGGAL" value="<?= $HEADER['TANGGAL']; ?>" class="sstext" formatDate="yes"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nama Penanda tangan</td>
                                        <td>
                                            <input type="text" name="HEADER[NAMATTD]" id="NAMATTD" value="<?= $HEADER['NAMATTD']; ?>" class="lmtext"/>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                </table>


            </div>
        </div>
    </form>
</div>
<script>

    $(document).ready(function () {
        onoff(false);
        declareDP('frmPKBEHdr');
        f_MAXLENGTH('frmPKBEHdr');
    });
    function onoff(on) {
        if ($('#frmPKBEHdr #CAR').val() == '' || $('#fbc23_ #act').val() == 'save') {
            $('#frmPKBEHdr .outentry').hide();
        } else {
            $('#frmPKBEHdr .outentry').show();
        }
        if (on) {
            $('#on').show();
            $('#off').hide();
            $("#frmPKBEHdr :input").removeAttr("disabled");
            $('#frmPKBEHdr .outentry :input').attr('disabled', 'disabled');
        } else {
            $('#on').hide();
            $('#off').show();
            $("#frmPKBEHdr :input").attr('disabled', 'disabled');
            $('#frmPKBEHdr .outentry :input').removeAttr("disabled");
        }
        $('.onterus').removeAttr('disabled');
        $('.offterus :input').attr('readonly', 'readonly');
    }

    function save_header(formid) {
        //$('#msgbox').html('');
        var dataSend = $(formid).serialize();
        jConfirm('Anda yakin Akan memproses data ini?', site_name,
                function (r) {
                    if (r == true) {
                        jloadings();
                        $.ajax({
                            type: 'POST',
                            url: $(formid).attr('action'),
                            data: dataSend,
                            success: function (data) {
                                Clearjloadings();//alert(data); return false;
                                if (data.search("MSG") >= 0) {
                                    arrdata = data.split('#');
                                    if (arrdata[1] == "OK") {
                                        //$("#tabs_bc20").tabs({disabled: []});
                                        $("form").find("#noaju").val(arrdata[3]);
                                        $(".msgheader_").css('color', 'green');
                                        $(".msgheader_").html(arrdata[2]);
                                        if (formid == "#frmPKBEHdr") {
                                            cekValidasi(arrdata[3]);
                                            if ($('#frmPKBEHdr #act').val() == 'save') {
                                                $('#frmPKBEHdr').load(site_url + '/bc30/createPKBE/' + arrdata[3]);
                                            }
                                        }
                                        if (arrdata[4])
                                            $("#DivHeaderForm").load(arrdata[4]);
                                    } else {
                                        $(".msgheader_").css('color', 'red');
                                        $(".msgheader_").html(arrdata[2]);
                                    }
                                } else {
                                    $(".msgheader_").css('color', 'red');
                                    if (formid == '#fpass') {
                                        $(".msgheader_").html(arrdata[2]);
                                    } else {
                                        $(".msgheader_").html('Proses Gagal.');
                                    }
                                }
                            }
                        });
                    } else {
                        return false;
                    }
                });
    }

    function cekValidasi(car) {
        //alert(car);return false;
        $(function () {
            $('#msgbox').html('');
            $("#msgbox").html('<div style="margin-left: 15px; margin-top: 15px;"><img src="' + base_url + 'img/loaders/facebook.gif" /> loading...</div>');
            $("#msgbox").dialog({
                resizable: false,
                height: 500,
                modal: true,
                width: 700,
                title: 'Validasi Dokumen',
                open: function () {
                    $.ajax({
                        type: 'POST',
                        url: site_url + '/bc30/alertcekstatus/' + car + '/t_bc30pkbehdr',
                        success: function (data) {
                            $("#msgbox").html(data);
                            $('#URSTATUS').html($('#STATUSCEK').val());
                            call(site_url + '/bc30/refreshHeader/' + car, 'DivHeaderForm');
                        }
                    });
                },
                buttons: {
                    Close: function () {
                        $(this).dialog("close");
                    }
                }
            });
        });
    }

    function gotoQUEUED() {
        jConfirm('Data akan dibentuk EDIFACT ?', site_name,
                function (r) {
                    if (r == true) {
                        jloadings();
                        $(function () {
                            $('#msgbox').html('');
                            $("#msgbox").html('<div style="margin-left: 15px; margin-top: 15px;"><img src="' + base_url + 'img/loaders/facebook.gif" /> loading...</div>');
                            $("#msgbox").dialog({
                                resizable: false,
                                height: 500,
                                modal: true,
                                width: 700,
                                title: 'Edifact / unEdifact',
                                open: function () {
                                    $.ajax({
                                        type: 'POST',
                                        url: site_url + '/bc30/queued',
                                        data: '&CAR=' + $('#noaju').val(),
                                        error: function () {
                                            Clearjloadings();
                                        },
                                        success: function (data) {
                                            Clearjloadings();
                                            $("#msgbox").html(data);
                                            call(site_url + '/bc30/refreshHeader/' + $('#noaju').val(), 'DivHeaderForm');
                                        }
                                    });
                                },
                                buttons: {
                                    Close: function () {
                                        $(this).dialog("close");
                                    }
                                }
                            });
                        });
                    } else {
                        return false;
                    }
                });
    }
</script>