<div class="content_luar">
    <div class="content_dalam">
        <fieldset>
            <legend>Entry NIK</legend>
            <form method="post" name="srp" id="srp" onsubmit="return false;">
                <table width="100%">
                    <tr>
                        <td colspan="2">Khusus modul untuk PPJK, NIK Importir dientry secara manual.</td>
                    </tr>
                    <tr>
                        <td width="80x">NIK Importir</td>
                        <td>
                            <input type="text" id="SRP" value="" class="ltext" wajib="yes" maxlength="16"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">Keterangan</td>
                        <td>
                            * Isikan hanya 'angka' dari NIK. Misalnya: S-001234/XYZ/BC/2003 cukup ditulis 1234 pada kolom di atas.<br>
                            * Langsung klik [Ok] tanpa mengisi NIK jika Anda bermaksud mengirim data PIB tanpa nomor NIK importir. (Diperkenankan bagi Importir yang baru pertama kali melakukan importasi barang, atau importir non NIK)
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top" colspan="2">
                            <button onclick="queueNik('<?= $CAR ?>',$('#SRP').val());" class="btn" > Ok </button>
                        </td>
                    </tr>
                </table>
            </form>
        </fieldset>
    </div>
</div>
<div id="msgQueue"></div>
<script>
    function queueNik(car,srp){
        jloadings();
        $('#msgbox').html('');
        $("#msgbox").html('<div style="margin-left: 15px; margin-top: 15px;"><img src="' + base_url + 'img/loaders/facebook.gif" /> loading...</div>');
        $.ajax({
            type: 'POST',
            url: site_url + '/bc20/queued/' + srp,
            data: '&CAR=' + car,
            error: function(){
                Clearjloadings();
            },
            success: function(data) {
                Clearjloadings();
                $("#msgbox").html(data);
                call(site_url + '/bc20/refreshHeader/' + car, 'tab-Header');
            }
        });
    }
</script>