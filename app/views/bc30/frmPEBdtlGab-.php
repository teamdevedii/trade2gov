<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
die('tset');
die($DOKGAB);
//print_r($CAR);die();
?>
<script>
    $(document).ready(function() {
        onoffPebDtlGab(false);
        declareDP('fpebdtlgab');
        f_MAXLENGTH('fpebdtlgab');
    });

    function onoffPebDtlGab(on) {
        //cmdPKB
        if ($('#fpebdtlgab #CAR').val() == '') {
            $('#fpebdtlgab.outentry').hide();
        } else {
            $('#fpebdtlgab.outentry').show();
        }
        if (on) {
            $('#fpebdtlgab #on').show();
            $('#fpebdtlgab #off').hide();
            $("#fpebdtlgab :input").removeAttr("disabled");
            $('#fpebdtlgab .outentry :input').attr('disabled', 'disabled');
        } else {
            $('#fpebdtlgab #on').hide();
            $('#fpebdtlgab #off').show();
            $("#fpebdtlgab :input").attr('disabled', 'disabled');
            $('#fpebdtlgab .outentry :input').removeAttr("disabled");
        }
        $('#fpebdtlgab .onterus').removeAttr('disabled');
        $('#fpebdtlgab .offterus :input').attr('readonly', 'readonly');
    }

    function addGabungan() {
        var awl = ($('#SERIBRGG').val() == '') ? 0 : parseInt($('#SERIBRGG').val());
        if (awl <= $('#JMBRGG').val()) {
            $('#SERIBRGG').val(awl + 1);
            $("#fpebdtlgab #fielDtlGab1 :input").val('');
            $("#fpebdtlgab #fielDtlGab2 :input").val('');
            $("#fpebdtlgab #fielDtlGab3 :input").val('');
            $("#fpebdtlgab #fielDtlGab4 :input").val('');
        } else {
            alert('data suudah penuh');
            onoffPebDtlGab(false);
        }
    }

</script>

<div class="content_luar" style="height: 95%; margin: 3px; padding: 1px">
    <div class="content_dalam" style="height: 100%; width: 100%">
        <form name="fpebdtlgab" id="fpebdtlgab" action="<?= site_url('bc30/setHeaderPKB'); ?>" method="post" autocomplete="off" onsubmit="return false;" >
            <?= $MAXLENGTH ?>
            <input type="text" name="act" id="act" value="save" />
            <input type="text" name="PEBDTLGAB[CAR]" id="car" value="<?= $CAR ?>" />
            <table width="100%">
                <tr>
                    <td colspan="2">
                        <table width="100%">
                            <tr>
                                <td colspan="2">
                                    <h4>
                                        <span id="off">
                                            <button onclick="onoffPebDtlGab(true);
                                        return false;" class="btn onterus"><span class="icon-pen"></span> Edit </button>
                                            <button onclick="onoffPebDtlGab(true);
                                        addGabungan();
                                        return false;" class="btn onterus"><span class="icon-aperture"></span> Add </button>
                                        </span>
                                        <span id="on">
                                            <button onclick="save_post_msg('fpebdtlgab');
                                        onoffPebDtlGab(false);" class="btn onterus"><span class="icon-download"></span> Simpan</button>
                                            <button onclick="onoffPebDtlGab(false);
                                        cancel('fpebdtlgab');" class="btn onterus"><span class="icon-undo"></span> Batal</button>
                                        </span><span class="msgheader_" style="margin-left:20px">&nbsp;</span>
                                    </h4>
                                    <span style="float: right">
                                        <button id="btnValidasi" onclick="cekValidasi($('#car').val())" class="btn onterus">
                                            <span class="icon-check"></span> Periksa Kelengkapan [ <span id="URSTATUS"><?= $DTLPKB['URSTATUS']; ?></span> ]
                                        </button>
                                    </span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <fieldset id="fielDtlGab1">
                            <table width="100%">
                                <tr>
                                    <td style="width: 100px;">Nomor Aju</td>
                                    <td> : <b><?= substr($CAR, 0, 6) . '-' . substr($CAR, 7, 6) . '-' . substr($CAR, 12, 8) . '-' . substr($CAR, 20, 6); ?></b>
                                    </td>
                                </tr>
                                <tr>
                                    <td> Gabungan Ke-</td>
                                    <td><input type="textd" name="" id="" class="sssstext" /> &nbsp; Barang Ke- &nbsp; <input type="textd" name="" id="" class="sssstext" /> &nbsp; dari <input type="textd" name="" id="" class="sssstext" readonly /></td>
                                    <td>Nomor LPBC</td>
                                    <td><input type="textd" name="" id="" class="sstext" /> &nbsp;&nbsp;<input type="textd" name="" id="" class="sstext" /></td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset id="fielDtlGab2">
                            <legend>Data Barang</legend>
                            <table width="100%">
                                <tr>
                                    <td>No. HS/Uraian Barang &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" name="" id="" value="" class="sstext"></td>
                                </tr>
                                <tr>
                                    <td><input type="text" name="PEBDTLGAB[xx]" id="" value="<?= $PEBDTLGAB['']; ?>" class="ltext"></td>
                                    <td>Merek</td>
                                    <td> <input type="text" name="PEBDTLGAB[DMERKG]" id="DMERKG" value="<?= $PEBDTLGAB['DMERKG']; ?>" class="ssstext"></td>
                                </tr>
                                <tr>
                                    <td><input type="text" name="PEBDTLGAB[xx]" id="" value="<?= $PEBDTLGAB['']; ?>" class="ltext"></td>
                                    <td>Ukuran</td>
                                    <td><input type="text" name="PEBDTLGAB[SIZEG]" id="" value="<?= $PEBDTLGAB['SIZEG']; ?>" class="ssstext"></td>
                                </tr>
                                <tr>
                                    <td><input type="text" name="PEBDTLGAB[xx]" id="DMERKG" value="<?= $PEBDTLGAB['']; ?>" class="ltext"></td>
                                    <td>Tipe</td>
                                    <td><input type="text" name="PEBDTLGAB[TYPEG]" id="TYPEG" value="<?= $PEBDTLGAB['TYPEG']; ?>" class="ssstext"></td>
                                </tr>
                                <tr>
                                    <td><input type="text" name="PEBDTLGAB[xx]" id="" value="<?= $PEBDTLGAB['']; ?>" class="ltext"></td>
                                    <td>Kode</td>
                                    <td><input type="text" name="PEBDTLGAB[KDBRGG]" id="KDBRGG" value="<?= $PEBDTLGAB['KDBRGG']; ?>" class="ssstext"></td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                    <td>
                        <fieldset id="fielDtlGab3">
                            <legend>Harga</legend>
                            <table width="100%">
                                <tr>
                                    <td>Harga FOB</td>
                                    <td>
                                        <input type="text" name="URFOB" id="URFOB" class="smtext" value="<?= $this->fungsi->FormatRupiah($PEBDTLGAB['FOB'], 4); ?>" onkeyup="this.value = ThausandSeperator('FOB', this.value, 4);" style="text-align:right;"/>
                                        <input type="hidden" name="PEBDTLGAB[FOB]" id="FOB" value="<?= $PEBDTLGAB['FOB']; ?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Jumlah Satuan</td>
                                    <td>
                                        <input type="text" name="URFOB" id="URFOB" class="smtext" value="<?= $this->fungsi->FormatRupiah($PEBDTLGAB['FOB'], 4); ?>" onkeyup="this.value = ThausandSeperator('FOB', this.value, 4);" style="text-align:right;"/>
                                        <input type="hidden" name="PEBDTLGAB[FOB]" id="FOB" value="<?= $PEBDTLGAB['FOB']; ?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Satuan</td>
                                    <td colspan="3">
                                        <input type="text"  name="PEBDTLGAB[KDKTR]" id="KDKTR" value="<?= $PEBDTLGAB['KDKTR']; ?>" url="<?= site_url('referensi/autoComplate/KPBC'); ?>" urai="KDKTR;KDKTRPEB" onfocus="Autocomp(this.id)" class="ssstext" maxlength="6" /> &nbsp;
                                        <button onclick="tb_search('satuan', 'KDKTR;URKDKTR', 'Kode Kpbc', this.form.id, 650, 400);" class="btn"> ... </button>
                                        <span id="URKDKTR"><?= $PEBDTLGAB['UKDKTR'] ?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Netto</td>
                                    <td>
                                        <input type="text" name="URNETTO" id="URNETTO" class="smtext" value="<?= $this->fungsi->FormatRupiah($PEBDTLGAB['NETTO'], 4); ?>" onkeyup="this.value = ThausandSeperator('NETTO', this.value, 4);" style="text-align:right;"/> Kilogram (KGM)
                                        <input type="hidden" name="PEBDTLGAB[NETTO]" id="NETTO" value="<?= $PEBDTLGAB['NETTO']; ?>"></td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset id="fielDtlGab4">
                            <legend>Kemasan</legend>
                            <table width="100%">
                                <tr>
                                    <td>Jumlah</td>
                                    <td>
                                        <input type="text" name="URFOB" id="URFOB" class="smtext" value="<?= $this->fungsi->FormatRupiah($PEBDTLGAB['FOB'], 4); ?>" onkeyup="this.value = ThausandSeperator('FOB', this.value, 4);" style="text-align:right;"/>
                                        <input type="hidden" name="PEBDTLGAB[FOB]" id="FOB" value="<?= $PEBDTLGAB['FOB']; ?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Jenis</td>
                                    <td colspan="3">
                                        <input type="text"  name="PEBDTLGAB[KDKTR]" id="KDKTR" value="<?= $PEBDTLGAB['KDKTR']; ?>" url="<?= site_url('referensi/autoComplate/KPBC'); ?>" urai="KDKTR;KDKTRPEB" onfocus="Autocomp(this.id)" class="ssstext" maxlength="6" /> &nbsp;
                                        <button onclick="tb_search('kemasan', 'KDKTR;URKDKTR', 'Kode Kpbc', this.form.id, 650, 400);" class="btn"> ... </button>
                                        <span id="URKDKTR"><?= $PEBDTLGAB['UKDKTR'] ?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Netto</td>
                                    <td>
                                        <input type="text" name="URNETTO" id="URNETTO" class="smtext" value="<?= $this->fungsi->FormatRupiah($PEBDTLGAB['NETTO'], 4); ?>" onkeyup="this.value = ThausandSeperator('NETTO', this.value, 4);" style="text-align:right;"/> Kilogram (KGM)
                                        <input type="hidden" name="PEBDTLGAB[NETTO]" id="NETTO" value="<?= $PEBDTLGAB['NETTO']; ?>"></td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                    <td>
                        <fieldset>
                            <legend>Dokumen Gabungan</legend>
                            <?= $DOKGAB ?>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>