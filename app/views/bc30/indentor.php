<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
$arr = explode(";", $data);
foreach ($arr as $isi) {
    $arrX = explode("|", $isi);
    $val[$arrX[0]] = $arrX[1];
}
$val['NPWPQQ'] = str_replace('.', '', str_replace('-', '', $val['NPWPQQ']));

?>
<script>
    $(document).ready(function() {
        //declareDP('fbc30_');
        //onoffHdr(false);
       // KatagoriEks($('#KATEKS').val());
       // f_MAXLENGTH('fbc30_');
       // formatNPWP('NPWPEKS');
        formatNPWP('indentor #NPWPQQ');
       // if($('#NPWPPPJK').length){
          //  formatNPWP('NPWPPPJK');
        //}
    });
    
</script>
<div class="content_luar">
    <div class="content_dalam">
        <form method="post" name="indentor" id="indentor" onsubmit="return false;"  >
            <table width="100%">
                <tr>
                    <td width="100px">Identitas</td>
                    <td>
                        <?= form_dropdown('HEADER[IDQQ]', $JENIS_IDENTITAS, $val['IDQQ'], 'id="IDQQ" class="mtext" '); ?>
                        <input type="smtext" name="HEADER[NPWPQQ]" id="NPWPQQ" url="<?= site_url('referensi/autoComplate/INDENTOREKS/NPWPQQ'); ?>" urai="indentor #IDQQ;indentor #NPWPQQ;indentor #NAMAQQ;indentor #ALMTQQ;indentor #NIPERQQ" onblur="formatNPWP('indentor #'+this.id);" onfocus="Autocomp('indentor #'+this.id); unformatNPWP('indentor #'+this.id);" value="<?= $val['NPWPQQ']; ?>" class="smtext"/>
                        <button onclick="QQ();" class="btn" style="width: 30px; padding-left:10px"><span class="icon-comment-stroke"></span></button>
                    </td>
                </tr>
                <tr>
                    <td>Nama</td>
                    <td><input type="text" name="HEADER[NAMAQQ]" id="NAMAQQ" value="<?= $val['NAMAQQ']; ?>" class="ltext"  url="<?= site_url('referensi/autoComplate/INDENTOREKS/NAMAQQ'); ?>" urai="indentor #IDQQ;indentor #NPWPQQ;indentor #NAMAQQ;indentor #ALMTQQ;indentor #NIPERQQ" onfocus="Autocomp('indentor #'+this.id);"/></td>
                </tr>
                <tr>
                    <td style="vertical-align: top" >Alamat</td>
                    <td>
                        <textarea name="HEADER[ALMTQQ]" id="ALMTQQ" class="ltext" onkeyup="limitChars(this.id, 70, 'limitALMTQQ')"><?= $val['ALMTQQ']; ?></textarea>
                        <div id="limitALMTQQ"></div>
                    </td>
                </tr>
                <tr>
                    <td>NIPER</td>
                    <td>
                        <input type="text" name="HEADER[NIPERQQ]" id="NIPERQQ" value="<?= $val['NIPERQQ']; ?>" class="ssstext"/>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: top" colspan="2">
                        <button onclick="getIndentor();" class="btn" > Simpan </button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
