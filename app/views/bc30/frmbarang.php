<script>
    $('document').ready(function() {
        $('#tabsPE').tabs();
        declareDP('fbarang_');
        f_MAXLENGTH('fbarang_');
        tarif();
        tabs33();
        FormatHS('HS');
        formatNumeral('fbarang_');
    });
    function tarif() {
        var JenisTarif = $('#KDPE').val();
        switch (JenisTarif) {
            case '1':
                $('#advalorum').show();
                $('#spesifik').hide();
                break;
            case '2':
                $('#advalorum').hide();
                $('#spesifik').show();
                break;
            case '':
                $('#advalorum').hide();
                $('#spesifik').hide();
                break;
        }
    }
    
    function tabs33(){
        //tab-Izin,tab-Bea,tab-PJT
        if($('#KATEKS').val() == '33'){
            $('#PPPJT').show();
        }else{
            $('#PPPJT').hide();
        }
    }
    

    function navPageBrg(tipe, page, jmlpage) {
        //alert(tipe+'|'+page+'|'+jmlpage); return false;
        var goPage = 1;
        var car = $('#fbarang_ #CAR').val();
        jmlpage = parseInt(jmlpage);
        page = parseInt(page);
        switch (tipe) {
            case 'first':
                goPage = 1;
                break;
            case 'preview':
                if (page > 1) {
                    goPage = page - 1;
                } else if (page <= 1) {
                    goPage = 1;
                }
                break;
            case 'next':
                if (page < jmlpage) {
                    goPage = page + 1;
                } else if (page >= jmlpage) {
                    goPage = jmlpage;
                }
                break;
            case 'last':
                goPage = jmlpage;
                break;
        }
        //alert(goPage);
        call(site_url + '/bc30/pageDtlBarang/' + car + '/' + goPage, 'navBarang', '');
        //

    }

    function nilaiBKperRp() {
        var KDPE       = $('#KDPE').val();
        var HPATOK     = parseFloat(ReplaceAll($('#HPATOK').val(),',',''));
        var JMSATPE_A  = parseFloat(ReplaceAll($('#JMSATPE_A').val(),',',''));
        var TARIPPE_A  = parseFloat(ReplaceAll($('#TARIPPE_A').val(),',',''));
        var NILVALPE_A = parseFloat(ReplaceAll($('#NILVALPE_A').val(),',',''));
        //spesifik
        var JMSATPE_S  = parseFloat(ReplaceAll($('#JMSATPE_S').val(),',',''));
        var TARIPPE_S  = parseFloat(ReplaceAll($('#TARIPPE_S').val(),',',''));
        var NILVALPE_S = parseFloat(ReplaceAll($('#NILVALPE_S').val(),',',''));
        //alert(KDPE+'|'+HPATOK+'|'+JMSATPE_A+'|'+TARIPPE_A+'|'+NILVALPE_A);
        switch (KDPE) {
            case '1':
                var NilaiBK = (HPATOK * NILVALPE_A * JMSATPE_A) / 100 * TARIPPE_A;
                $('#PEPERBRG_A').val(numeral(NilaiBK).format('0,0.00'));
                break;
            case '2':
                var NilaiBKpe = parseFloat(TARIPPE_S * JMSATPE_S * NILVALPE_S);
                $('#PEPERBRG_S').val(numeral(NilaiBKpe).format('0,0.00'));
                break;
        }
    }
    function harga(){
        var FOBPERBRG = parseFloat(ReplaceAll($('#FOBPERBRG').val(),',',''));
        var JMSATUAN = parseFloat(ReplaceAll($('#JMSATUAN').val(),',',''));
        var harga = FOBPERBRG / JMSATUAN;
        $('#FOBPERSAT').val(numeral(harga).format('0,0.0000'));
    }
    /*function cekValidasiDtl(car, seri) {
        //alert('sini'); return false;
        //alert(car+'|'+seri);return false;
        $(function() {
            $('#msgbox').html('');
            $("#msgbox").html('<div style="margin-left: 15px; margin-top: 15px;"><img src="' + base_url + 'img/loaders/facebook.gif" /> loading...</div>');
            $("#msgbox").dialog({
                resizable: false,
                height: 500,
                modal: true,
                width: 700,
                title: 'Validasi Dokumen',
                open: function() {
                    $.ajax({
                        type: 'POST',
                        url: site_url + '/bc30/alertcekstatusDtl/' +car+ '/' +seri, '/fbarang_',
                        success: function(data) {
                            $("#msgbox").html(data);
                            $('#URSTATUS').html($('#STATUSCEK').val());
                            call(site_url + '/bc30/barang/' + car + '/'+ seri, 'fbarang_');
                        }
                    });
                },
                buttons: {
                    Close: function() {
                        $(this).dialog("close");
                    }
                }
            });
        });
    }*/
    



function cekValidasiDtl(car, seri) {
        //$('#msgbox').html('');
        //$("#msgbox").html('<div style="margin-left: 15px; margin-top: 15px;"><img src="' + base_url + 'img/loaders/facebook.gif" /> loading...</div>');
        $("#msgbox").dialog({
            resizable: false,
            height: 500,
            modal: true,
            width: 600,
            title: 'Validasi Dokumen',
            open: function() {
                $.ajax({
                    type: 'POST',
                    url: site_url + '/bc30/alertcekstatusDtl/' + car + '/' + seri,
                    success: function(data) {
                        var arr = data.trim();
                        var arr = arr.split('|');
                        var test = arr[0];
                        if (test == 'OK') {
                            var isi = "<span class='icon-check'></span> <b style='color: #ffffff'>Lengkap</b>";
                        } else if (test == 'ER') {
                            var isi = "<span class='icon-x'></span> <b style='color: #ffffff'>Tidak Lengkap</b>";
                        }
                        $('#DTLOK').html(isi);
                        $("#msgbox").html(arr[1]);
                    }
                });
            },
            buttons: {
                Close: function() {
                    $(this).dialog("close");
                }
            }
        });
    }
</script>
<div id="navBarang">
    <table style="width: 100%">
        <tr>
            <td style="width: 70%">
                <button onclick="save_post_msg('fbarang_', '', 'fbarang_list');
                        $('#fbarang_form').html('');" class="btn">Simpan</button>
                <button onclick="cancel('fbarang_');
                        $('#fbarang_form').html('');" class="btn">Batal</button>
            </td>
            <td style="text-align: center"> 
                <button onclick="navPageBrg('first', $('#fbarang_ #SERIBRG').val(), $('#fbarang_ #TOTALBRG').val());
                        return false;" class="btn" style="height: 22px; width:25px; vertical-align: middle;" > <span class="icon-first"></span></button>

                <button onclick="navPageBrg('preview', $('#fbarang_ #SERIBRG').val(), $('#fbarang_ #TOTALBRG').val());
                        return false;" class="btn" style="-webkit-transform: rotate(180deg);-moz-transform: rotate(180deg);-o-transform: rotate(180deg);-ms-transform: rotate(180deg);transform: rotate(180deg); height: 22px; width:25px; vertical-align: middle;" > <span class="icon-play"></span></button>

                <input type="text" id="NOBARANG" class="ssstext" value="<?= $DETIL['SERIBRG']; ?>" style="text-align: center;" readonly="readonly"/>

                <button onclick="navPageBrg('next', $('#fbarang_ #SERIBRG').val(), $('#fbarang_ #TOTALBRG').val());
                        return false;" class="btn" style="height: 22px; width:25px; vertical-align: middle;"> <span class="icon-play"></span></button> 

                <button onclick="navPageBrg('last', $('#fbarang_ #SERIBRG').val(), $('#fbarang_ #TOTALBRG').val());
                        return false;" class="btn" style="height: 22px; width:25px; vertical-align: middle;"><span class="icon-last"></span></button>
            </td>
        </tr>
    </table>
    <hr style="margin-top: 5px; margin-bottom: 5px;">

    <form id="fbarang_" name="fbarang_" action="<?= site_url('bc30/barang/' . $DETIL['CAR']); ?>" onsubmit="return false;" method="post" autocomplete="off">
        <?= $MAXLENGTH ?>
        <input type="hidden" name="act" id="act" value="<?= $act; ?>" />
        <input type="hidden" name="car" id="car" value="<?= $DETIL['CAR']; ?>" />
        <input type="hidden" name="seri" id="car" value="<?= $DETIL['SERIBRG']; ?>" />
        <input type="hidden" name="DETIL[KATEKS]" id="KATEKS" value="<?= $DETIL['KATEKS']; ?>" />
        <fieldset>
            <table width="100%" border="0">
                <tr>
                    <td width="50%" valign="top">
                        Nomor Aju &nbsp; : &nbsp; <b><?= substr($DETIL['CAR'], 0, 6) . '-' . substr($DETIL['CAR'], 7, 6) . '-' . substr($DETIL['CAR'], 12, 8) . '-' . substr($DETIL['CAR'], 20, 6); ?></b>
                        <input type="hidden" name="DETIL[CAR]" id="CAR" value="<?= $DETIL['CAR']; ?>"/>
                    </td>
                    <td style="text-align: right">
                        <button onclick="cekValidasiDtl($('#fbarang_ #CAR').val(), $('#fbarang_ #SERIBRG').val());" class="btn" id="DTLOK">
                            <span class="<?= ($DETIL['DTLOK']!= '1') ? 'icon-x' : 'icon-check'; ?>"></span> 
                            <b style="color: #ffffff"><?= $DETIL['UDTLOK'] ?></b>
                        </button>   
                    </td>
                </tr>
                <tr>
                    <td>
                        Barang ke - <input type="text" name="DETIL[SERIBRG]" id="SERIBRG" value="<?= $DETIL['SERIBRG']; ?>" class="ssstext text" readonly="readonly"/> 
                        Dari Total <input type="text" id="TOTALBRG" value="<?= $DETIL['TOTALBRG']; ?>" class="ssstext text" readonly="readonly"/>
                    </td>
                    <td></td>
                </tr>
            </table>
        </fieldset>

        <table width="100%">
            <tr>
                <td width="50%" valign="top">
                    <fieldset>
                        <legend>Data Barang</legend>
                        <table width="100%">
                            <tr>
                                <td width="135px">Nomor HS</td>
                                <td>
                                    <input type="text" name="DETIL[HS]" id="HS" url="<?= site_url('referensi/autoComplate/HS-BARANG'); ?>" class="ltext"  value="<?= $DETIL['HS']; ?>" onfocus="Autocomp(this.id);unFormatHS(this.id)" urai="HS;URBRG1;URBRG2;URBRG3;URBRG4" onblur="FormatHS(this.id)" on />
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">Uraian Barang</td>
                                <td>
                                    <input type="text" name="DETIL[URBRG1]" id="URBRG1" class="ltext text" value="<?= $DETIL['URBRG1']; ?>"/> <button onclick="tb_search('barang', 'BRGURAI;MERK;TIPE;SPFLAIN;NOHS;SERITRP', 'Barang', this.form.id, 650, 400, 'NOHS;SERITRP;')" class="btn">...</button><br>
                                    <input type="text" name="DETIL[URBRG2]" id="URBRG2" class="ltext text" value="<?= $DETIL['URBRG2']; ?>"/><br>
                                    <input type="text" name="DETIL[URBRG3]" id="URBRG3" class="ltext text" value="<?= $DETIL['URBRG3']; ?>"/><br>
                                    <input type="text" name="DETIL[URBRG4]" id="URBRG4" class="ltext text" value="<?= $DETIL['URBRG4']; ?>"/>
                                </td>
                            </tr>
                            <tr> 
                                <td>Merk</td>
                                <td><input type="text" name="DETIL[DMERK]" id="DMERK" class="text" value="<?= $DETIL['DMERK']; ?>"/></td>
                            </tr>
                            <tr>
                                <td>Ukuran</td>
                                <td><input type="text" name="DETIL[SIZE]" id="SIZE" class="text" value="<?= $DETIL['SIZE']; ?>"/></td>
                            </tr>
                            <tr>
                                <td>Tipe</td>
                                <td><input type="text" name="DETIL[TYPE]" id="TYPE" class="text" value="<?= $DETIL['TYPE']; ?>"/></td>
                            </tr>
                            <tr>
                                <td>Kode</td>
                                <td><input type="text" name="DETIL[KDBRG]" id="KDBRG" class="text" value="<?= $DETIL['KDBRG']; ?>"/></td>
                            </tr>
                        </table>
                    </fieldset>
                    <fieldset>
                        <legend>Kemasan</legend>
                        <table width="100%">
                            <tr>
                                <td width="135px">Jumlah</td>
                                <td>
                                    <input type="text" name="URJMKOLI" id="URJMKOLI" class="numtext text" value="<?= number_format($DETIL['JMKOLI'], 0); ?>" onkeyup="this.value = ThausandSeperator('JMKOLI', this.value);"/><input type="hidden" name="DETIL[JMKOLI]" id="JMKOLI" value="<?= $DETIL['JMKOLI'] ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td>Jenis</td>
                                <td>
                                    <input type="text" name="DETIL[JNKOLI]" id="JNKOLI" value="<?= $DETIL['JNKOLI']; ?>" class="ssstext text" maxlength="2" onblur="checkCode(this);" grp="kemasan" urai="URJNKOLI" />
                                    <button onclick="tb_search('kemasan', 'JNKOLI;URJNKOLI', 'Kode Kemasan', this.form.id, 650, 400)" class="btn">...</button> 
                                    <span id="URJNKOLI"><?= $DETIL['URJNKOLI']; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Netto</td>
                                <td>
                                    <input type="text" name="DETIL[NETDET]" id="NETDET" class="numtext text" value="<?= number_format($DETIL['NETDET'], 4); ?>" numeral="yes" format="0,0.0000"/>
                                    Kgm
                                </td>
                            </tr>
                            <tr>
                                <td>Volume</td>
                                <td>
                                    <input type="text" name="DETIL[DVOLUME]" id="DVOLUME" class="numtext text" value="<?= number_format($DETIL['DVOLUME'], 4); ?>" numeral="yes" format="0,0.0000"/>
                                    M<sup>3</sup>
                                </td>
                            </tr>
                            <tr>
                                <td>Negara Asal</td>
                                <td>
                                    <input type="text" name="DETIL[NEGASAL]" id="NEGASAL" class="sssstext text" value="<?= $DETIL['NEGASAL']; ?>" onblur="checkCode(this);" grp="negara" urai="URNEGASAL"/> 
                                    <button onclick="tb_search('negara', 'NEGASAL;URNEGASAL', 'Kode Negara', this.form.id, 650, 400)" class="btn">...</button> 
                                    <span id="URNEGASAL"><?= $DETIL['URNEGASAL']; ?></span>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
                <td valign="top">
                    <fieldset>
                        <legend>Harga</legend>
                        <table width="100%">
                            <tr>
                                <td width="135px">Harga FOB</td>
                                <td>
                                    <input type="text" name="DETIL[FOBPERBRG]" id="FOBPERBRG" class="numtext text" value="<?= number_format($DETIL['FOBPERBRG'], 4); ?>" numeral="yes" format="0,0.0000" onblur="harga();"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Jumlah Satuan</td>
                                <td>
                                    <input type="text" name="DETIL[JMSATUAN]" id="JMSATUAN" class="numtext text" value="<?= number_format($DETIL['JMSATUAN'], 4); ?>" numeral="yes" format="0,0.0000" onblur="harga();"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Jenis Satuan</td>
                                <td>
                                    <input type="text" name="DETIL[JNSATUAN]" id="JNSATUAN" class="ssstext text"  value="<?= $DETIL['JNSATUAN']; ?>" onblur="checkCode(this);" grp="satuan" urai="URJNSATUAN" />
                                    <button onclick="tb_search('satuan', 'JNSATUAN;URJNSATUAN', 'Kode Satuan', this.form.id, 650, 400)" class="btn">...</button> 
                                    <span id="URJNSATUAN"><?= $DETIL['URJNSATUAN']; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Harga Satuan FOB</td>
                                <td>
                                    <input type="text" name="DETIL[FOBPERSAT]" id="FOBPERSAT" class="numtext text" value="<?= number_format($DETIL['FOBPERSAT'], 4); ?>" numeral="yes" format="0,0.0000"/>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                    <div id="tabsPE">
                        <ul style="height: 31px; margin: 0;padding: 0;" >
                            <li><a href="#tab-Izin">Izin Khusus</a></li>
                            <li><a href="#tab-Bea">Bea Keluar</a></li>
                            <li id="PPPJT"><a href="#tab-PJT">Pengirim/Penerima (PJT)</a></li>
                        </ul>
                        <div id="tab-Izin">
                            <table>
                                <tr>
                                    <td width="135px">Jenis Izin</td>
                                    <td><?= form_dropdown('DETIL[KDIZIN]', $KDIZIN, $DETIL['KDIZIN'], 'id="KDIZIN" class="ltext text"'); ?></td>
                                </tr>
                                <tr>
                                    <td>Nomor</td>
                                    <td><input type="text" name="DETIL[NOIZIN]" id="NOIZIN" class="text" value="<?= $DETIL['NOIZIN']; ?>"/></td>
                                </tr>
                                <tr>
                                    <td>Tanggal</td>
                                    <td>
                                        <input type="text" name="DETIL[TGIZIN]" id="TGIZIN" value="<?= $DETIL['TGIZIN'] ?>" onmousemove="ShowDP(this.id)" class="sstext" maxlength="10" formatDate="yes">
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="tab-Bea">
                            <table>
                                <tr>
                                    <td width="135px">Jenis Tarif</td>
                                    <td><?= form_dropdown('DETIL[KDPE]', $JENIS_TARIF, $DETIL['KDPE'], 'id="KDPE" onchange="tarif()" class="ltext text"'); ?></td>
                                </tr>
                                <tr id="advalorum">
                                    <td colspan="2">
                                        <fieldset>
                                            <table>
                                                <!--hide spesifik-->
                                                <tr>
                                                    <td>H. Patokan</td>
                                                    <td>

                                                        <input type="text" name="DETIL[KDVALPE_A]" id="KDVALPE_A" value="<?= $DETIL['KDVALPE']; ?>" onblur="checkCode(this);" grp="valuta" urai="KDVALPE_A2"  class="ssstext" maxlength="3" />
                                                        <button onclick="tb_search('valuta2', 'KDVALPE_A;KDVALPE_A2', 'Kode Valuta', this.form.id, 650, 400);" class="btn">...</button>

                                                        <input type="text" name="DETIL[HPATOK]" id="HPATOK" class="numtext text" value="<?= number_format($DETIL['HPATOK'], 2); ?>" numeral="yes" format="0,0.00" onblur="nilaiBKperRp();"/>
                                                        &nbsp;/&nbsp;
                                                        <input type="text" name="DETIL[JNSATPE_A]" id="JNSATPE_A" value="<?= $DETIL['JNSATPE']; ?>" onblur="checkCode(this);" grp="satuan" urai="UJNSATPE_A"  class="ssstext" maxlength="3" />
                                                        <button onclick="tb_search('satuan', 'JNSATPE_A;UJNSATPE_A', 'Kode Satuan', this.form.id, 650, 400);" class="btn">...</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Jumlah</td>
                                                    <td>
                                                        <input type="text" name="DETIL[JMSATPE_A]" id="JMSATPE_A" class="numtext text" value="<?= number_format($DETIL['JMSATPE'], 4); ?>" numeral="yes" format="0,0.0000" onblur="nilaiBKperRp();"/>
                                                        <span id="UJNSATPE_A"><?= $DETIL['UJNSATPE']; ?></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Tarif BK</td>
                                                    <td>
                                                        <input type="text" name="DETIL[TARIPPE_A]" id="TARIPPE_A" class="numtext text" value="<?= number_format($DETIL['TARIPPE'], 2); ?>" numeral="yes" format="0,0.00" onblur="nilaiBKperRp();"/>
                                                        %
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>1 <span id="KDVALPE_A2"><?= $DETIL['KDVALPE']; ?></span> = Rp</td>
                                                    <td>
                                                        <input type="text" name="DETIL[NILVALPE_A]" id="NILVALPE_A" class="numtext text" value="<?= number_format($DETIL['NILVALPE'], 4); ?>" numeral="yes" format="0,0.0000" onblur="nilaiBKperRp();"/>
                                                        &nbsp; Nilai BK Rp &nbsp;
                                                        <input type="text" name="DETIL[PEPERBRG_A]" id="PEPERBRG_A" class="numtext text" value="<?= number_format($DETIL['PEPERBRG'], 2); ?>" numeral="yes" format="0,0.00"/>
                                                        
                                                    </td>
                                                </tr>
                                                <!--hide spesifik-->
                                            </table>
                                        </fieldset>
                                    </td>
                                </tr>
                                <tr id="spesifik">
                                    <td colspan="2">
                                        <fieldset>
                                            <table>
                                                <!--show spesifik-->
                                                <tr>
                                                    <td>Tarif PE</td>
                                                    <td>
                                                        <input type="text" name="DETIL[KDVALPE_S]" id="KDVALPE" value="<?= $DETIL['KDVALPE']; ?>" onblur="checkCode(this);" grp="valuta2" urai="KDVALPE2" class="ssstext" maxlength="3" />
                                                        <button onclick="tb_search('valuta2', 'KDVALPE;KDVALPE2', 'Kode Valuta', this.form.id, 650, 400);" class="btn">...</button>
                                                        <input type="text" name="DETIL[TARIPPE_S]" id="TARIPPE_S" class="numtext text" value="<?= number_format($DETIL['TARIPPE'], 4); ?>" numeral="yes" format="0,0.0000" onblur="nilaiBKperRp();"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Jumlah</td>
                                                    <td>
                                                        <input type="text" name="DETIL[JMSATPE_S]" id="JMSATPE_S" class="numtext text" value="<?= number_format($DETIL['JMSATPE'], 4); ?>" numeral="yes" format="0,0.0000" onblur="nilaiBKperRp();"/>

                                                        <input type="text" name="DETIL[JNSATPE_S]" id="JNSATPE" value="<?= $DETIL['JNSATPE']; ?>" onblur="checkCode(this);" grp="satuan" urai="UJNSATPE" class="ssstext" maxlength="3" /> 
                                                        <button onclick="tb_search('satuan', 'JNSATPE;UJNSATPE', 'Kode Satuan', this.form.id, 650, 400);" class="btn">...</button>
                                                        <span id="UJNSATPE"><?= $DETIL['UJNSATPE']; ?></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>1 <span id="KDVALPE2"><?= $DETIL['KDVALPE']; ?></span> = Rp</td>
                                                    <td>
                                                        <input type="text" name="DETIL[NILVALPE_S]" id="NILVALPE_S" class="numtext text" value="<?= number_format($DETIL['NILVALPE'], 4); ?>" numeral="yes" format="0,0.0000" onblur="nilaiBKperRp();"/>
                                                        &nbsp; Nilai BK Rp &nbsp;
                                                        <input type="text" name="DETIL[PEPERBRG_S]" id="PEPERBRG_S" class="numtext text" value="<?= number_format($DETIL['PEPERBRG'], 2); ?>" numeral="yes" format="0,0.00"/>
                                                    </td>
                                                </tr>
                                                <!--show spesifik-->
                                            </table>
                                        </fieldset>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="tab-PJT">
                            <table>
                                <tr>
                                    <td width="135px">Identitas</td>
                                    <td>
                                        <?= form_dropdown('DETIL[IDEKST]', $JENIS_IDENTITAS, $DETIL['IDEKST'], 'id="IDEKST" class="mtext"'); ?> <input type="text" name="DETIL[NPWPEKST]" id="NPWPEKST" url="<?= site_url('referensi/autoComplate/EKSPORTIR/NPWPEKS'); ?>" urai="IDEKST;NPWPEKST;NAMAEKST;ALMTEKST" onfocus="Autocomp(this.id);
                                                unformatNPWP(this.id);" onblur="formatNPWP(this.id);" value="<?= $DETIL['NPWPEKST']; ?>" class="smtext"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Nama</td>
                                    <td><input type="text" name="DETIL[NAMAEKST]" id="NAMAEKST" value="<?= $DETIL['NAMAEKST']; ?>" url="<?= site_url('referensi/autoComplate/EKSPORTIR/NAMAEKS'); ?>" urai="IDEKST;NPWPEKST;NAMAEKST;ALMTEKST" onfocus="Autocomp(this.id);" class="ltext"/></td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top" >Alamat</td>
                                    <td>
                                        <textarea name="DETIL[ALMTEKST]" id="ALMTEKST" class="ltext" onkeyup="limitChars(this.id, 70, 'ALMTEKST')"><?= $DETIL['ALMTEKST']; ?></textarea>
                                        <div id="limitALMTEKST"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Penerima</td>
                                    <td>
                                        <input type="text" name="DETIL[NAMABELIT]" id="NAMABELIT" class="text" value="<?= $DETIL['NAMABELIT']; ?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Alamat</td>
                                    <td>
                                        <input type="text" name="DETIL[ALMTBELIT]" id="ALMTBELIT" class="text" value="<?= $DETIL['ALMTBELIT']; ?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Negara</td>
                                    <td>
                                        <input type="text" name="DETIL[NEGBELIT]" id="NEGBELIT" value="<?= $DETIL['NEGBELIT']; ?>" class="ssstext text" maxlength="2" url="<?= site_url('referensi/autoComplate/NEGARA'); ?>" urai="NEGBELIT;UNEGBELIT" onfocus="Autocomp(this.id);" />
                                        <button onclick="tb_search('negara', 'NEGBELIT;UNEGBELIT', 'Kode Negara', this.form.id, 650, 400)" class="btn">...</button>
                                        <span id="UNEGBELIT"><?= $DETIL['UNEGBELIT']; ?></span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </td>
            </tr>
        </table>

    </form>
</div>
<hr style="margin-top: 10px; margin-bottom: 10px;" >