<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
//print_r($CAR);die();
?>
<script>
    $(document).ready(function() {
        onoffGabDtl(false);
        declareDP('fpebdtlgab');
        formatNumeral('fpebdtlgab');
        f_MAXLENGTH('fpebdtlgab');
    });

    function onoffGabDtl(on) {
        //cmdPKB
        if ($('#fpebdtlgab #CAR').val() == '') {
            $('#fpebdtlgab.outentry').hide();
        } else {
            $('#fpebdtlgab.outentry').show();
        }
        if (on) {
            $('#fpebdtlgab #on').show();
            $('#fpebdtlgab #off').hide();
            $("#fpebdtlgab :input").removeAttr("disabled");
            $('#fpebdtlgab .outentry :input').attr('disabled', 'disabled');
        } else {
            $('#fpebdtlgab #on').hide();
            $('#fpebdtlgab #off').show();
            $("#fpebdtlgab :input").attr('disabled', 'disabled');
            $('#fpebdtlgab .outentry :input').removeAttr("disabled");
        }
        $('#fpebdtlgab .onterus').removeAttr('disabled');
        $('#fpebdtlgab .offterus :input').attr('readonly', 'readonly');
    }

    function addGabunganDtl() {
        var total = ($('#JMBRGG').val());
        //var awl = ($('#SERIBRGG').val() == '') ? 0 : parseInt($('#SERIBRGG').val());
        var awl = ($('#SERIBRG').val() == '') ? 0 : parseInt($('#SERIBRG').val());
        //alert(awl);
        if (awl < total) {
            $('#SERIBRGG').val(awl + 1);
            $("#fpebdtlgab #fielDtlGab1 :input").val('');
            $("#fpebdtlgab #fielDtlGab2 :input").val('');
            $("#fpebdtlgab #fielDtlGab3 :input").val('');
            $("#fpebdtlgab #fielDtlGab4 :input").val('');
        } else if(awl >= total) {
            jAlert('Jumlah Gabungan Lebih Dari Total di Header.',site_name);
            onoffGabDtl(false);
        }
    }

    function navPageGabDtl(tipe, page, jmlpage) {
        //alert(page); return false;
        var goPage = 1;
        var car = $('#fpebgab #CAR').val();
        var jmbrgg = $('#fpebgab #JMBRGG').val();
        var serieks = $('#fpebgab #SERIEKS').val();
        jmlpage = parseInt(jmlpage);
        page = parseInt(page);
        switch (tipe) {
            case 'first':
                goPage = 1;
                break;
            case 'preview':
                if (page > 1) {
                    goPage = page - 1;
                } else if (page <= 1) {
                    goPage = 1;
                }
                break;
            case 'next':
                if (page < jmlpage) {
                    goPage = page + 1;
                } else if (page >= jmlpage) {
                    goPage = jmlpage;
                }
                break;
            case 'last':
                goPage = jmlpage;
                break;
        }
        call(site_url + '/bc30/pageDtlPEBgabungan/' + car + '/' + goPage + '/' + jmbrgg + '/' + serieks, 'navPebGabDtl', '');
        //alert(goPage);
    }
    
    function GoToPageDtl(car, serieks, seribrgg, jmbrgg){
        //alert(car+'|'+serieks+'|'+seribrgg+'|'+jmbrgg);
        Dialog(site_url + '/bc30/GoToPagePEBgDtl/' + car+ '/'+serieks+'/'+ seribrgg+'/'+jmbrgg, 'divgotoPEBgDtl', '.: Pilih Data :.', 400, 450);
    }
    
    function totalHrgGab() {
        var FOBPERBRGG  = parseFloat(ReplaceAll($('#FOBPERBRGG').val(),',',''));
        var JMSATUANG  = parseFloat(ReplaceAll($('#JMSATUANG').val(),',',''));
        
        var HrgSatuan = FOBPERBRGG / JMSATUANG;
        $('#FOBPERSATG').val(numeral(HrgSatuan).format('0,0.0000'));
    }
    
    function cekValidasiPEBgabDTL(car, serieks, seribrgg, jmbrgg) {
        //alert(car+'|'+serieks+'|'+seribrgg+'|'+jmbrgg);return false; JMBRGG
        $(function() {
            $('#msgbox').html('');
            $("#msgbox").html('<div style="margin-left: 15px; margin-top: 15px;"><img src="' + base_url + 'img/loaders/facebook.gif" /> loading...</div>');
            $("#msgbox").dialog({
                resizable: false,
                height: 500,
                modal: true,
                width: 700,
                title: 'Validasi Dokumen',
                open: function() {
                    $.ajax({
                        type: 'POST',
                        url: site_url + '/bc30/alertcekstatusPEBgabDTL/' + car + '/'+ serieks +'/'+ seribrgg + '/fpebdtlgab',
                        success: function(data) {
                            $("#msgbox").html(data);
                            $('#URSTATUS').html($('#STATUSCEK').val());
                            call(site_url + '/bc30/frmPEBGabDtl/' +car+ '/'+jmbrgg+'/'+serieks, 'navPebGabDtl');
                        }
                    });
                },
                buttons: {
                    Close: function() {
                        $(this).dialog("close");
                    }
                }
            });
        });
    }
</script>

<div class="content_luar" id="navPebGabDtl" style="height: 95%; margin: 3px; padding: 1px">
    <div class="content_dalam" style="height: 100%; width: 100%">
        <form name="fpebdtlgab" id="fpebdtlgab" action="<?= site_url('bc30/setHeaderPEBdtlGab'); ?>" method="post" autocomplete="off" onsubmit="return false;" >
            <?= $MAXLENGTH ?>
            <input type="hidden" name="act" id="act" value="save" />
            <input type="hidden" name="PEBDTLGAB[CAR]" id="car" value="<?= $CAR ?>" />
            <input type="hidden" name="SERIBRG" id="SERIBRG" value="<?= $SERI['SERIBRG'] ?>" />
            <input type="hidden" name="JMBRGG_C" id="JMBRGG_C" value="<?= $SERI['JMBRGG_C'] ?>" />
            <table width="100%">
                <tr>
                    <td colspan="2">
                        <table width="90%">
                            <tr>
                                <td colspan="2">
                                        <span id="off">
                                            <button onclick="onoffGabDtl(true);
                                                    return false;" class="btn onterus"><span class="icon-pen"></span> Edit </button>
                                            <button onclick="onoffGabDtl(true);
                                                    addGabunganDtl();
                                                    return false;" class="btn onterus"><span class="icon-aperture"></span> Add </button>
                                        </span>
                                        <span id="on">
                                            <button onclick="save_post_msg('fpebdtlgab','','navPebGabDtl');
                                                    onoffGabDtl(false);" class="btn onterus"><span class="icon-download"></span> Simpan</button>
                                            <button onclick="onoffGabDtl(false);
                                                    cancel('fpebdtlgab');" class="btn onterus"><span class="icon-undo"></span> Batal</button>
                                        </span><span class="msgheader_" style="margin-left:20px">&nbsp;</span>
                                </td>
                                <td style="text-align: right" class="outentry"> 
                                    <button onclick="navPageGabDtl('first', $('#fpebdtlgab #SERIBRGG').val(), $('#fpebdtlgab #JMBRGG').val());
                                            return false;" class="btn" style="height: 22px; width:25px; vertical-align: middle;" > <span class="icon-first"></span></button>

                                    <button onclick="navPageGabDtl('preview', $('#fpebdtlgab #SERIBRGG').val(), $('#fpebdtlgab #JMBRGG').val());
                                            return false;" class="btn" style="-webkit-transform: rotate(180deg);-moz-transform: rotate(180deg);-o-transform: rotate(180deg);-ms-transform: rotate(180deg);transform: rotate(180deg); height: 22px; width:25px; vertical-align: middle;" > <span class="icon-play"></span></button>

                                    <input type="text" id="NOBARANG" class="ssstext" value="<?= $PEBDTLGAB['SERIBRGG']; ?>" style="text-align: center;" readonly="readonly"/>

                                    <button onclick="navPageGabDtl('next', $('#fpebdtlgab #SERIBRGG').val(), $('#fpebdtlgab #JMBRGG').val());
                                            return false;" class="btn" style="height: 22px; width:25px; vertical-align: middle;"> <span class="icon-play"></span></button> 

                                    <button onclick="navPageGabDtl('last', $('#fpebdtlgab #SERIBRGG').val(), $('#fpebdtlgab #JMBRGG').val());
                                            return false;" class="btn" style="height: 22px; width:25px; vertical-align: middle;"><span class="icon-last"></span></button>
                                    <button onclick="GoToPageDtl($('#CAR').val(),$('#SERIEKS').val(),$('#SERIBRGG').val(),$('#JMBRGG').val())" class="btn" style="height: 22px; width:60px; vertical-align: middle;">Go To</button>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <fieldset>
                            <table width="100%">
                                <tr>
                                    <td style="width: 100px;">Nomor Aju</td>
                                    <td> 
                                        : <b><?= substr($CAR, 0, 6) . '-' . substr($CAR, 7, 6) . '-' . substr($CAR, 12, 8) . '-' . substr($CAR, 20, 6); ?></b>
                                    </td>
                                    <td style="text-align: right" class="outentry">
                                        <button onclick="cekValidasiPEBgabDTL($('#fpebdtlgab #car').val(),$('#fpebdtlgab #SERIEKS').val(),$('#fpebdtlgab #SERIBRGG').val(),$('#fpebdtlgab #JMBRGG').val())" class="btn" id="DTLGOK"><span class="<?= ($PEBDTLGAB['DTLGOK']!='1')?'icon-x':'icon-check'; ?>"></span> <b style="color: #ffffff"><?= $PEBDTLGAB['UDTLGOK'] ?></b></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td> Gabungan Ke-</td>
                                    <td><input type="textd" name="PEBDTLGAB[SERIEKS]" id="SERIEKS" value="<?= $SERIEKS ?>" class="sssstext" /> &nbsp; 
                                        Barang Ke- &nbsp; <input type="textd" name="PEBDTLGAB[SERIBRGG]" id="SERIBRGG" value="<?= $PEBDTLGAB['SERIBRGG']; ?>" class="sssstext" /> &nbsp;
                                        dari <input type="textd" name="PEBDTLGAB[JMBRGG]" id="JMBRGG" value="<?= $SERI['JMBRGG_C'] ?>" class="sssstext" readonly /></td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset id="fielDtlGab1">
                            <legend>Data Barang</legend>
                            <table width="100%">
                                <tr>
                                    <td>No. HS/Uraian Barang &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" name="PEBDTLGAB[HSG]" id="HSG" value="<?= $PEBDTLGAB['HSG']; ?>" class="sstext"></td>
                                </tr>
                                <tr>
                                    <td><input type="text" name="PEBDTLGAB[URBRGG1]" id="URBRGG1" value="<?= substr($PEBDTLGAB['URBRGG'], 0, 40) ?>" class="ltext"></td>
                                    <td>Merek</td>
                                    <td> <input type="text" name="PEBDTLGAB[DMERKG]" id="DMERKG" value="<?= $PEBDTLGAB['DMERKG']; ?>" class="ssstext"></td>
                                </tr>
                                <tr>
                                    <td><input type="text" name="PEBDTLGAB[URBRGG2]" id="URBRGG2" value="<?= substr($PEBDTLGAB['URBRGG'], 41, 80) ?>" class="ltext"></td>
                                    <td>Ukuran</td>
                                    <td><input type="text" name="PEBDTLGAB[SIZEG]" id="" value="<?= $PEBDTLGAB['SIZEG']; ?>" class="ssstext"></td>
                                </tr>
                                <tr>
                                    <td><input type="text" name="PEBDTLGAB[URBRGG3]" id="URBRGG3" value="<?= substr($PEBDTLGAB['URBRGG'], 81, 120) ?>" class="ltext"></td>
                                    <td>Tipe</td>
                                    <td><input type="text" name="PEBDTLGAB[TYPEG]" id="TYPEG" value="<?= $PEBDTLGAB['TYPEG']; ?>" class="ssstext"></td>
                                </tr>
                                <tr>
                                    <td><input type="text" name="PEBDTLGAB[URBRGG4]" id="URBRGG4" value="<?= substr($PEBDTLGAB['URBRGG'], 121, 160) ?>" class="ltext"></td>
                                    <td>Kode</td>
                                    <td><input type="text" name="PEBDTLGAB[KDBRGG]" id="KDBRGG" value="<?= $PEBDTLGAB['KDBRGG']; ?>" class="ssstext"></td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        EKS PEB Barang ke: &nbsp;&nbsp&nbsp <input type="text" name="PEBDTLGAB[EXSERIBRGG]" id="EXSERIBRGG" value="<?= $PEBDTLGAB['EXSERIBRGG']; ?>" class="mtext">
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                    <td style="vertical-align: top;">
                        <fieldset id="fielDtlGab2">
                            <legend>Harga</legend>
                            <table width="100%">
                                <tr>
                                    <td>Harga FOB</td>
                                    <td>
                                        <input type="text" name="PEBDTLGAB[FOBPERBRGG]" id="FOBPERBRGG" value="<?= number_format($PEBDTLGAB['FOBPERBRGG'],2); ?>" class="stext" numeral="yes" format="0,0.00" style="text-align: right;" onblur="totalHrgGab();"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Jumlah Satuan</td>
                                    <td>
                                        <input type="text" name="PEBDTLGAB[JMSATUANG]" id="JMSATUANG" value="<?= number_format($PEBDTLGAB['JMSATUANG'],4); ?>" class="stext" numeral="yes" format="0,0.0000" style="text-align: right;" onblur="totalHrgGab();"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Satuan</td>
                                    <td colspan="3">
                                        <input type="text"  name="PEBDTLGAB[JNSATUANG]" id="JNSATUANG" value="<?= $PEBDTLGAB['JNSATUANG']; ?>" onblur="checkCode(this);" grp="satuan" urai="UJNSATUANG" class="ssstext" maxlength="6" /> &nbsp;
                                        <button onclick="tb_search('satuan', 'JNSATUANG;UJNSATUANG', 'Kode Satuan', this.form.id, 650, 400);" class="btn"> ... </button>
                                        <span id="UJNSATUANG"><?= $PEBDTLGAB['UJNSATUAN'] ?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Harga Satuan</td>
                                    <td>
                                        <input type="text" name="PEBDTLGAB[FOBPERSATG]" id="FOBPERSATG" value="<?= number_format($PEBDTLGAB['FOBPERSATG'],4); ?>" class="stext" numeral="yes" format="0,0.0000" style="text-align: right;"/>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">
                        <fieldset id="fielDtlGab3">
                            <legend>Kemasan</legend>
                            <table width="100%">
                                <tr>
                                    <td>Jumlah</td>
                                    <td>
                                        <input type="text" name="JUMLAH_KEMASANUR" id="JUMLAH_KEMASANUR" class="numtext" value="<?= number_format($PEBDTLGAB['JMKOLIG'], 0); ?>" onkeyup="this.value = ThausandSeperator('JMKOLIG', this.value, 2);"/>
                                        <input type="hidden" name="PEBDTLGAB[JMKOLIG]" id="JMKOLIG" value="<?= $PEBDTLGAB['JMKOLIG'] ?>" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Jenis</td>
                                    <td colspan="3">
                                        <input type="text"  name="PEBDTLGAB[JNKOLIG]" id="JNKOLIG" value="<?= $PEBDTLGAB['JNKOLIG']; ?>" onblur="checkCode(this);" grp="kemasan" urai="UJNKOLIG" class="ssstext" maxlength="6" /> &nbsp;
                                        <button onclick="tb_search('kemasan', 'JNKOLIG;UJNKOLIG', 'Kode Kemasan', this.form.id, 650, 400);" class="btn"> ... </button>
                                        <span id="UJNKOLIG"><?= $PEBDTLGAB['UJNKOLIG'] ?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Netto</td>
                                    <td>
                                        <input type="text" name="PEBDTLGAB[NETDETG]" id="NETDETG" value="<?= number_format($PEBDTLGAB['NETDETG'],4); ?>" class="stext" numeral="yes" format="0,0.0000" style="text-align: right;"/>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                    <td style="vertical-align: top;">
                        <fieldset class="outentry" id="DokGab">
                            <legend>Dokumen Gabungan</legend>
                            <?= $DOKGAB ?>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>