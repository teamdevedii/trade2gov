<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<script>
    $(function () {
        declareDP('fdokgab_');
        bankDHE($('#fdokgab_ #KDDOK').val());
        $('#fdokgab_').show();
    })
    function bankDHE(kdDOk){
        $("#fdokgab_ #nonBank :input").attr('disabled','disabled');
        $('#fdokgab_ #nonBank').hide();
        $("#fdokgab_ #Bank :input").attr('disabled','disabled');
        $('#fdokgab_ #Bank').hide();
        if(kdDOk==='111'){
            $("#fdokgab_ #Bank :input").removeAttr("disabled");
            $('#fdokgab_ #Bank').show();
        }else{
            $("#fdokgab_ #nonBank :input").removeAttr("disabled");
            $('#fdokgab_ #nonBank').show();
        }
    }
</script>
<div id="fdokgab_list">
    <form id="fdokgab_" nama='fdokgab_' action="<?= site_url('bc30/DokGab/'.$CAR) ?>" onsubmit="return false;" method="post" autocomplete="off">
    <input type='text' name="act" value="<?= $act; ?>" />
    <input type='hidden' name="serieks" id="SERIEKS" value="<?= $SERIEKS ?>" />
    <input type='hidden' name="seribrgg" id="SERIBRGG" value="<?= $SERIBRGG ?>" />
    <input type='text' name="DOKGAB[CAR]" id="CAR" value="<?= $CAR ?>" />
    <input type='text' name="DOKGAB[SERIEKS]" id="SERIEKS" value="<?= $SERIEKS ?>" />
    <input type='text' name="DOKGAB[SERIBRGG]" id="SERIBRGG" value="<?= $SERIBRGG ?>" />
    <table>
        <tr><td>Kode Dokumen :</td><td>Dari KPBC :</td></tr>
        <tr>
            <td>
                <?= form_dropdown('DOKGAB[KDDOKG]', $KDDOK, $DOKGAB['KDDOKG'], 'id="KDDOKG"  class="text" onchange="bankDHE(this.value)"'); ?>
            </td>
            <td>
                <input  type="text"  name="DOKGAB[KTRSSTBG]" id="KTRSSTBG" value="<?= $DOKGAB['KTRSSTBG']; ?>" url="<?= site_url('referensi/autoComplate/KPBC'); ?>" urai="KTRSSTBG;UKTRSSTBG" onfocus="Autocomp(this.id)" class="ssstext" maxlength="6" /> &nbsp;
                <button onclick="tb_search('kpbc', 'KTRSSTBG;UKTRSSTBG', 'Kode Kpbc', this.form.id, 650, 400);" class="btn"> ... </button>
                <span id="UKTRSSTBG"><?= $DOKGAB['URAIAN_KPBC'] ?></span>
            </td>   
            
        </tr>
        <tr><td>Nomor :</td><td>Tanggal :</td></tr>
        <tr>
           
            <td>
                <input  type="text" name="DOKGAB[NODOKG]" id="NODOKG" class="ltext" value="<?= $DOKGAB['NODOKG']; ?>" maxlength="30" wajib="yes"/>
            </td>
            <td>
                <input type="text" name="DOKGAB[TGDOKG]" id="TGDOK" class="sstext" value="<?= $DOKGAB['TGDOKG']; ?>" maxlength="10" formatDate="yes" wajib="yes"/>
            </td>
        </tr>
        <tr>
            <td colspan="2"><br>
                <button onclick="save_post_msg('fdokgab_', '', 'fdokgab_') && $('#fdokgab_').html('');" class="btn">Simpan</button>
                <button onclick="cancel('fdokgab_');$('#fdokgab_').html('');" class="btn">Batal</button>
            </td>
        </tr>
    </table>
    
    <script>
    function save_dok_gab(formid, iddiv, divcon) { 
        //alert (divcon); return false;
    if (formid === "" || typeof (formid) === "undefined")
        var formid = "";
    if (iddiv === "" || typeof (iddiv) === "undefined")
        var iddiv = "msgConfirm";
    if (divcon === "" || typeof (divcon) === "undefined")
        var divcon = "_content_";
        var hdrMsg = '';
        var isiMsg = '';
    if($('#' + formid).attr('action')==='' || $('#' + formid).attr('action') === site_url){return false;}
    if (validasi(iddiv, '#' + formid)) {
        jloadings();
        $.ajax({
            type: 'POST',
            url: $('#' + formid).attr('action'),
            data: $('#' + formid).serialize(),
            success: function (datax) {
                $('#' + iddiv).removeClass();
                var arrdata = datax.split('|');
                if (arrdata[0].trim()==='MSG') {
                    arrdata[2] = ReplaceAll(arrdata[2], '`', '|');
                    if(arrdata[2]!=='msgbox' && arrdata[2]!==''){call(arrdata[2], divcon);}
                    if (arrdata[1] === 'OK') {
                        $('#' + iddiv).addClass('notify notify-success');
                        hdrMsg = 'Success';
                        if(arrdata[2]==='msgbox'){$('#msgbox').dialog('close');}
                    } else {
                        $('#' + iddiv).addClass('notify notify-error');
                        hdrMsg = 'Error';
                    }
                    isiMsg = arrdata[3];
                } else {
                    $('#' + iddiv).addClass('notify');
                    hdrMsg = 'Notifikasi';
                    isiMsg = datax;
                }
                $('#' + iddiv).slideDown("slow");
                $('#' + iddiv).html('<a onclick="$(\'#'+iddiv+'\').hide();" class="closex">&times;</a><h5>'+hdrMsg+'</h5><p>'+isiMsg+'</p>');
                Clearjloadings();
            }
        });
        return true;
    }
    return false;
}
    </script>
    
    
    
</form>
</div>
