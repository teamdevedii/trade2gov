<?php
if (!defined('BASEPATH')){exit('No direct script access allowed');}
?>
<script>
    function frmPKB(car){
        Dialog(site_url + '/bc30/frmPKB/' + car, 'navPKB', '.: PKB - Pemberitahuan Persiapan Barang :.', 900, 570);
    }
    function frmPEBGab(car, jmbrggab){
        //alert(car+'|'+jmbrggab);return false;
        Dialog(site_url + '/bc30/frmPEBGab/' + car +'/'+jmbrggab, 'navPebGab', '.: PEB Gabungan :.', 800, 455);
    }
    function frmPEBGabDtl(car,jmbrgg, serieks){
        Dialog(site_url + '/bc30/frmPEBGabDtl/' + car+'/'+jmbrgg+'/'+serieks, 'navPebGabDtl', '.: PEB Detail Gabungan :.', 900, 500);
    }
    
    function getIndentor(){
    $('#fbc30_ #IDQQ').val($('#indentor #IDQQ').val());
    $('#fbc30_ #NPWPQQ').val($('#indentor #NPWPQQ').val());
    $('#fbc30_ #NAMAQQ').val($('#indentor #NAMAQQ').val());
    $('#fbc30_ #ALMTQQ').val($('#indentor #ALMTQQ').val());
    $('#fbc30_ #NIPERQQ').val($('#indentor #NIPERQQ').val());
    closedialog('divIndentor');
    }
    
    function gotoQUEUED(car,tipe,sts){
        //alert('sini'); return false;
        jConfirm('Data akan dibentuk EDIFACT ?', site_name,
        function(r) {
            if (r == true) {
                //jloadings();
                $(function() {
                    $('#msgbox').html('');
                    $("#msgbox").html('<div style="margin-left: 15px; margin-top: 15px;"><img src="' + base_url + 'img/loaders/facebook.gif" /> loading...</div>');
                    $("#msgbox").dialog({
                        resizable: false,
                        height: 400,
                        modal: true,
                        width: 600,
                        title: 'Edifact / unEdifact',
                        open: function() {
                            if(sts=='010'){
                                if(tipe=='2'){
                                    call(site_url + '/bc30/getSRP/' + car, 'msgbox');
                                }else{
                                    $.ajax({
                                        type: 'POST',
                                        url: site_url+'/bc30/queuedPEB',
                                        data: '&CAR=' + car,
                                        error: function(){
                                            Clearjloadings();
                                        },
                                        success: function(data) {
                                            Clearjloadings();
                                            $("#msgbox").html(data);
                                            call(site_url + '/bc30/refreshHeaderPEB/' + car, 'tab-Header');
                                        }
                                    });
                                }
                            }else{
                                $.ajax({
                                    type: 'POST',
                                    url: site_url+'/bc30/queuedPEB',
                                    data: '&CAR=' + car,
                                    error: function(){
                                        Clearjloadings();
                                    },
                                    success: function(data) {
                                        Clearjloadings();
                                        $("#msgbox").html(data);
                                        call(site_url + '/bc30/refreshHeaderPEB/' + car, 'tab-Header');
                                    }
                                });
                            }
                            
                        },
                        buttons: {
                            Close: function() {
                                $(this).dialog("close");
                            }
                        }
                    });
                });
            }else {
                return false;
            }
        });
    }
    
</script>
<div class="content_luar">
    <div class="content_dalam">
        <div id="tabs">
            <ul>
                <li><a href="#tab-Header">Data Header</a></li>
                <li><a href="#tab-Barang">Data Barang</a></li>
            </ul>
            <div id="tab-Header"><?= $HEADER; ?></div>
            <div id="tab-Barang"><?= $BARANG; ?></div>
        </div>
    </div>
</div>
<div id="shadow"></div>
