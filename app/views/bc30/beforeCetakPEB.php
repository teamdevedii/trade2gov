<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
?>
<div class="content_luar">
    <div class="content_dalam">
        <form id="cetak_" method="post" autocomplete="off" onsubmit="return false;">
            <input id="CAR_C" type="text" value="<?= $car ?>">
            <fieldset>
                <legend> Pilihan Pencetakan </legend>
                <table width="100%">
                    <tr>
                        <td>
                            <input type="radio" name="CETAK" id="cetakPEB" value="1" checked >      <label for="cetakPEB">&nbsp;Cetak Semua</label><br>
                            <!--<input type="radio" name="CETAK" id="cetakHalTertentu" value="3">       <label for="cetakHalTertentu">&nbsp;Halaman tertentu</label><br>-->
                            <input type="radio" name="CETAK" id="cetakSSPCP" value="2">             <label for="cetakSSPCP">&nbsp;SSPCP</label><br>
                            <input type="radio" name="CETAK" id="cetakRespon" value="3"> <label for="cetakRespon">&nbsp;Respon</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br>
                            <button onclick="cetakNewWin($('.CETAK').val());" class="btn"><span class="icon-new-window"></span> Cetak </button>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
</div>
<script>
    $('#msgboxSSPCPx').html('');
    function cetakNewWin(id){
        if($('input[name="CETAK"]:checked').val()=='2'){
            //cetak($car,$jnPage,$frmSSPCP=0)
            Dialog(site_url+'/bc30/cetakPEB/'+$('#CAR_C').val()+'/2/1', 'msgbox2', ' Data PEB ', 650, 450);
        }else if($('input[name="CETAK"]:checked').val()=='3'){
            Dialog(site_url+"/bc30/beforecetakrespon/"+$('#CAR_C').val()+'', 'beforeCetakRespon', ' CETAK RESPON PEB ', 800, 450);
        }else{
            window.open(site_url+"/bc30/cetakPEB/"+$('#CAR_C').val()+'/'+$('input[name="CETAK"]:checked').val(),"Cetak Dokumen PKBE","scrollbars=yes, resizable=yes,width=1100,height=700");
        }
    }
</script>