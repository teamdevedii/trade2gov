<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
?>
<div class="content_luar">
    <div class="content_dalam">
        <form id="cetak" method="post" autocomplete="off" action="<?= site_url('bc30/setSSPCP'); ?>" onsubmit="return false;" >
            <input id="CAR" name="SSPCP[CAR]" type="hidden" value="<?= $car ?>">
            <input id="NPWPEKS" name="SSPCP[NPWPEKS]" type="hidden" value="<?= $SSPCP['NPWPEKS'] ?>">
            <fieldset>
                <legend> Data SSPCP (Bisa diubah sesuai keperluan) </legend>
                <table width="100%">
                    <tr>
                        <td width="150px">Kantor Pabean</td>
                        <td>
                            <input type="text" name="SSPCP[KDKPBC]" id="KDKPBC" value="<?= $SSPCP['KDKPBC']; ?>" url="<?= site_url('referensi/autoComplate/KPBC'); ?>" urai="KDKPBC;URKPBC" wajib="yes" onfocus="Autocomp(this.id)" class="ssstext" maxlength="6"/>
                            <input type="button" name="cari" id="cari" class="button" onclick="tb_search('kpbc', 'KDKPBC;URKPBC', 'Kode Kpbc', 'cetak_', 650, 400)" value="...">&nbsp;
                            <span id="URKPBC"><?= $SSPCP['URKPBC']; ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td>NPWP Kantor Pabean</td>
                        <td>
                            <input type="text" name="SSPCP[NPWP]" id="NPWP" value="<?= $SSPCP['NPWP']; ?>" onfocus="unformatNPWP(this.id);" onblur="formatNPWP(this.id);" class="smtext" maxlength="15"/> Masa Pajak <input type="text" name="SSPCP[MASAPAJAK]" id="MASAPAJAK" value="<?= $SSPCP['MASAPAJAK']; ?>" class="sssstext" maxlength="2" style="text-align: center" /> Tahun Pajak <input type="text" name="SSPCP[TAHUNPAJAK]" id="TAHUNPAJAK" value="<?= $SSPCP['TAHUNPAJAK']; ?>" class="ssstext" maxlength="4" style="text-align: center"/>
                        </td>
                    </tr> 
                    <tr>
                        <td>Nama</td>
                        <td>
                            <input type="text" name="SSPCP[NAMA]" id="NAMA" value="<?= $SSPCP['NAMA']; ?>" class="ltext" maxlength="50"/> Kode Pos <input type="text" name="SSPCP[KDPOS]" id="KDPOS" value="<?= $SSPCP['KDPOS']; ?>" class="ssstext" maxlength="6"/>
                        </td>
                    </tr>
                    <tr>
                        <td>Dok. Dasar Pembayaran</td>
                        <td>
                            <input type="text" name="SSPCP[DOKDSRBYR]" id="NAMA" value="<?= $SSPCP['DOKDSRBYR']; ?>" class="ltext" maxlength="50"/>
                        </td>
                    </tr>
                    <tr>
                        <td>Nomor/Tanggal Pengajuan PEB</td>
                        <td>
                            <input type="text" name="a" id="NOAJU" value="<?= substr($SSPCP['CAR'],0,6).'-'.substr($SSPCP['CAR'],6,6).'-'.substr($SSPCP['CAR'],12,8).'-'.substr($SSPCP['CAR'],20,6); ?>" class="ltext" maxlength="50"/>
                            <input type="text" name="a" id="TGAJUPEB" class="sstext date" value="<?= substr($SSPCP['CAR'],18,2).'-'.substr($SSPCP['CAR'],16,2).'-'.substr($SSPCP['CAR'],12,4); ?>" maxlength="10" formatDate="yes"/>
                        </td>
                    </tr> 
                    <tr>
                        <td>Nomor/Tanggal Pendaftaran PEB</td>
                        <td>
                            <input type="text" name="a" id="NAMA" value="<?= $SSPCP['KDKTR'].'/'.$SSPCP['TGDAFT']; ?>" class="ltext" maxlength="50"/>
                            <input type="text" name="a" id="TGDAFT" class="sstext date" value="<?= $SSPCP['TGDAFT']; ?>" maxlength="10" formatDate="yes"/> 
                        </td>
                    </tr> 
                    <tr>
                        <td colspan="2" ><hr style="margin-bottom: 10px;" ></td>
                    </tr> 
                    <tr>
                        <td colspan="2" class="table-bordered">
                            <table width="100%">
                                
                                <tr>
                                    <td>Bea Keluar</td>
                                    <td style="text-align: center;">412211</td>
                                    <td style="text-align: center;"><input type="text" name="SSPCPAKUN[412211]" id="412211" value="<?= $SSPCP['412211']; ?>" class="smtext numtext" maxlength="20"/></td>
                                </tr>
                                <tr>
                                    <td>Denda Administrasi Bea Keluar</td>
                                    <td style="text-align: center;">412212</td>
                                    <td style="text-align: center;"><input type="text" name="SSPCPAKUN[412212]" id="412212" value="<?= $SSPCP['412212']; ?>" class="smtext numtext" maxlength="20"/></td>
                                </tr>
                                <tr>
                                    <td>Bunga Bea Keluar</td>
                                    <td style="text-align: center;">412213</td>
                                    <td style="text-align: center;"><input type="text" name="SSPCPAKUN[412213]" id="412213" value="<?= $SSPCP['412213']; ?>" class="smtext numtext" maxlength="20"/></td>
                                </tr>
                                <tr>
                                    <td>PNBP / Pendapatan DJBC</td>
                                    <td style="text-align: center;">423216</td>
                                    <td style="text-align: center;"><input type="text" name="SSPCPAKUN[423216]" id="423216" value="<?= $SSPCP['423216']; ?>" class="smtext numtext" maxlength="20"/></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" ><hr style="margin-bottom: 10px;" ></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                             <table width="100%">
                                <tr>
                                    <td width="15%">NTB / NTP</td>
                                    <td width="35%"><input type="text" name="SSPCP[NTB]" id="NTB" value="<?= $SSPCP['NTB']; ?>" wajib="yes" class="smtext" maxlength="30"/></td>
                                    <td width="15%">NTPN</td>
                                    <td width="35%"><input type="text" name="SSPCP[NTPN]" id="NTPN" value="<?= $SSPCP['NTPN']; ?>" wajib="yes" class="smtext" maxlength="30"/></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" >
                           <button onclick="cetakSSPCPNewWin(this.form.id);" class="btn"><span class="icon-new-window"></span> Cetak </button>
                        </td>
                    </tr>
                </table>
                
            </fieldset>
        </form>
    </div>
</div>
<script>
    $(document).ready(function() {
        declareDP('cetak');
    });
    
    function cetakSSPCPNewWin(id){
        var dataSend = $('#'+id).serialize();
        $.ajax({
            type: 'POST',
            url: $('#'+id).attr('action'),
            data: dataSend,
            success: function(data) {
                window.open(site_url+"/bc30/cetakPEB/"+$('#CAR').val()+'/2',"Cetak Dokumen PIB","scrollbars=yes, resizable=yes,width=1000,height=700");
            }

        });
    return false;
    }
</script>