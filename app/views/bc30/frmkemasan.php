<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<form id="fkemasan_" name="fkemasan_" action="<?= site_url('bc30/kemasan/'.$KEMASAN['CAR']); ?>" onsubmit="return false;" method="post" autocomplete="off">
    <input type="hidden" name="act" value="<?= $act; ?>" />
    <input type="hidden" name="KEMASAN[CAR]" id="CAR" value="<?= $KEMASAN['CAR']; ?>" />
    <table cellspacing="2px">
        <tr>
            <td>Jumlah :</td>
            <td>Merk :</td>
            <td>Jenis Kemasan :</td>
            
        </tr>
        <tr>
            <td>
                <input type="text" id="JUMLAHUR" wajib="yes" class="stext" value="<?= $KEMASAN['JMKEMAS']; ?>" maxlength="18" onkeyup="this.value = ThausandSeperator('JUMLAH', this.value, 2);"/>
                <input type="hidden" name="KEMASAN[JMKEMAS]" id="JUMLAH" value="<?= $KEMASAN['JMKEMAS'] ?>" />
            </td>
            <td><input type="text" name="KEMASAN[MERKKEMAS]" id="MERK_KEMASAN" class="stext" value="<?= $KEMASAN['MERKKEMAS']; ?>" maxlength="30" wajib="yes" /></td>
            <td>
                <input type="text" name="KEMASAN[JNKEMAS]" id="KODE_KEMASAN" class="sssstext" value="<?= $KEMASAN['JNKEMAS']; ?>" onblur="checkCode(this);" grp="kemasan" urai="URAIAN_KEMASAN" maxlength="2" wajib="yes"/> <button onclick="tb_search('kemasan', 'KODE_KEMASAN;URAIAN_KEMASAN', 'Kode Kemasan', this.form.id, 700, 420)" class="btn">...</button>
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td><span id="URAIAN_KEMASAN"><?= $KEMASAN['URAIAN']; ?></span></td>
        </tr>
        <tr>
            <td colspan="3">
                <button onclick="save_post_msg('fkemasan_', '', 'fkemasan_list') && $('#fkemasan_form').html('');" class="btn">Simpan</button>
                <button onclick="cancel('fkemasan_');$('#fkemasan_form').html('');" class="btn">Batal</button>
            </td>
        </tr>
    </table>
</form>    
<script>
    $(function () {
        $('#fkemasan_form').show();
    });
</script>