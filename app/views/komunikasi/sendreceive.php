<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
?>
<div class="content_luar">
    <div class="content_dalam">
        <form id="communicate_" method="post" autocomplete="off" onsubmit="return false;" action="<?= site_url('komunikasi/communicate'); ?>">
            <input id="CAR" type="hidden" value="<?= $car ?>">
            <table width="100%">
                <tr>
                    <td>
                        <button class="btn" onclick="Communicate();" >Komunikasi &nbsp&nbsp <span class="icon-bolt"></span></button>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend> Status Komunikasi </legend>
                            <table width="100%">
                                <tr>
                                    <td>
                                        <div id="cont_com"></div>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
<script>
    function Communicate(){
        $('#cont_com').slideDown("slow");
        $('#cont_com').html('Communication...<br><img src="' + base_url + 'img/communicate2.gif" />');
        $.ajax({
            url: $('#communicate_').attr('action'),
            type : 'POST'
        }).done(function(data){
            $('#cont_com').toggle(function() {
                $('#cont_com').html(data);
                $('#cont_com').slideDown("slow"); 
            });
        });
    }
</script>