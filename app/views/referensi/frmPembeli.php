<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);

?>
<div class="content_luar">
    <div class="content_dalam">
        <form name="fpembeli" id="fpembeli" action="<?= site_url('referensi_1/pembeli'); ?>" method="post" autocomplete="off" onsubmit="return false;" >
            <input type="text" name="act" id="act" value="<?= $act; ?>" onchange="act();" readonly="readonly" />
            <input type="text" name="PEMBELI[KODE_TRADER]" value="<?= $PEMBELI['KODE_TRADER']; ?>" readonly="readonly" />
            <h4><span class="info_2">&nbsp;</span><?= $judul; ?></h4>
            <table width="100%">
                <tr>
                    <td width="50%" style="vertical-align: top; ">
                        <fieldset>
                        <legend>Data Parties</legend>
                        <table width="100%">
                            <tr>
                                <td>Nama Beli</td>
                                <td>:
                                    <input wajib="yes" type="text" name="PEMBELI[NAMABELI]" id="NAMABELI" value="<?= $PEMBELI['NAMABELI']; ?>" class="ltext" readonly="readonly"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Alamat Beli</td>
                                <td>:
                                    <input wajib="yes" type="text" name="PEMBELI[ALMTBELI]" id="ALMTBELI" value="<?= $PEMBELI['ALMTBELI']; ?>" class="ltext"/>
                                </td>
                            <tr>
                                <td>Negara Beli</td>
                                <td>:
                                    <input type="text" name="PEMBELI[NEGBELI]" id="NEGBELI" value="<?= $PEMBELI['NEGBELI']; ?>" class="sssstext" onblur="checkCode(this);" grp="negara" urai="URNEGBELI"/> <button onclick="tb_search('negara', 'NEGBELI;URNEGBELI', 'Kode Negara', this.form.id, 650, 400);" class="btn">...</button> <span id="URNEGBELI"><?= $PEMBELI['URNEGBELI']; ?></span>
                                </td>
                            </tr>
                            
                        </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><div class="msg_"></div></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button onclick="save_post_msg('fpembeli');" class="btn">Simpan</button>
                        <button onclick="cancel('fpembeli');" class="btn">Batal</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>