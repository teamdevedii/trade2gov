<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);
$ro['update'] = 'readonly="readonly"';
?>
<div class="content_luar">
    <div class="content_dalam">
        <form name="fpartner" id="fpartner" action="<?= site_url('referensi/partner'); ?>" method="post" autocomplete="off" onsubmit="return false;" >
            <input type="hidden" name="act" id="act" value="<?= $act; ?>" readonly/>
            <table width="100%">
                <tr>
                    <td width="50%" style="vertical-align: top; ">
                        <fieldset>
                        <legend>Data Partner</legend>
                        <table width="100%">
                            <tr>
                                <td width="80px">Kode KPBC </td>
                                <td>:
                                    <input type="text"  name="PARTNER[KDKPBC]" id="KDKPBC" value="<?= $PARTNER['KDKPBC']; ?>" url="<?= site_url('referensi/autoComplate/KPBC'); ?>" urai="KDKPBC;URKDKPBC" wajib="yes" onfocus="Autocomp(this.id)" class="ssstext" maxlength="6" <?= $ro[$act];?>/> &nbsp;
                                    <button onclick="tb_search('kpbc', 'KDKPBC;URKDKPBC', 'Kode Kpbc', this.form.id, 650, 400);" class="btn"> ... </button>
                                <span id="URKDKPBC"><?= $PARTNER['URKDKPBC'];?></span>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top">Edi Number</td>
                                <td>:
                                    <input wajib="yes" type="text" name="PARTNER[NOMOREDI]" id="NOMOREDI" value="<?= $PARTNER['NOMOREDI']; ?>" class="ltext" <?= $ro[$act];?>/></td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top">Nama Local </td>
                                <td>:
                                    <input wajib="yes" type="text" name="PARTNER[NAMALOKAL]" id="NAMALOKAL" value="<?= $PARTNER['NAMALOKAL']; ?>" class="ltext"/></td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top">Keterangan </td>
                                <td>:
                                    <input wajib="yes" type="text" name="PARTNER[KETERANGAN]" id="KETERANGAN" value="<?= $PARTNER['KETERANGAN']; ?>" class="ltext"/></td>
                            </tr>
                        </table>
                    </fieldset>
                    </td>
                    <td width="50%" style="vertical-align: top; "></td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><div class="msg_"></div></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button onclick="save_post_msg('fpartner');" class="btn">Simpan</button>
                        <button onclick="cancel('fpartner');" class="btn">Batal</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>