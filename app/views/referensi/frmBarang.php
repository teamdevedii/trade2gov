<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);
?>	
<div class="content_luar">
    <div class="content_dalam">
        <form name="fbarang" id="fbarang" action="<?= site_url('referensi/barang'); ?>" method="post" autocomplete="off" onsubmit="return false;" >
            <input type="hidden" name="act" id="act" value="<?= $act; ?>" readonly="readonly" />
            <input type="hidden" name="BARANG[KODE_TRADER]" value="<?= $BARANG['KODE_TRADER']; ?>" readonly="readonly" />
            <h4><span class="info_2">&nbsp;</span><?= $judul; ?></h4>
            <table width="100%">
                <tr>
                    <td width="50%" style="vertical-align: top; ">
                        <fieldset>
                        <legend>Data Barang</legend>
                        <table width="100%">
                            <tr>
                                <td width="150px">Kode HS Seri *</td>
                                <td >:
                                    <input type="text" name="BARANG[NOHS]" id="NOHS" url="<?= site_url('referensi/autoComplate/HS-TARIF'); ?>" class="stext" wajib="yes" value="<?= $this->fungsi->FormatHS($BARANG['NOHS']); ?>" maxlength="15" readonly="readonly" onblur="FormatHS(this.id)" onfocus="unFormatHS(this.id)" wajib='yes'/> - <input type="text" name="BARANG[SERITRP]" id="SERITRP" style="text-align: center;" class="sssstext" wajib="yes" value="<?= $BARANG['SERITRP']; ?>" readonly="readonly"/> &nbsp;<span style="vertical-align: top;"><input type="button" name="cari" id="cari" class="button" onclick="tb_search('hs', 'NOHS;SERITRP;TRPBM;TRPPPN;TRPPBM;TRPCUK;TRPPPH', 'Kode Hs', this.form.id, 650, 400)" value="..."></span>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">Uraian Barang *</td>
                                <td><span style="vertical-align: top;">:</span>
                                    <textarea name="BARANG[BRGURAI]" id="BRGURAI" wajib="yes" class="ltext"><?= $BARANG['BRGURAI']; ?></textarea></td>
                            </tr>
                            <tr>
                                <td>Merk</td>
                                <td>:
                                    <input type="text" name="BARANG[MERK]" id="MERK" class="text" value="<?= $BARANG['MERK']; ?>" maxlength="15" />
                                </td>
                            </tr>
                            <tr>
                                <td>Tipe</td>
                                <td>:
                                    <input type="text" name="BARANG[TIPE]" id="TIPE" class="text" value="<?= $BARANG['TIPE']; ?>" maxlength="15" />
                                </td>
                            </tr>
                            <tr>
                                <td>Sepesifikasi Lain</td>
                                <td>:
                                    <input type="text" name="BARANG[SPFLAIN]" id="SPFLAIN" class="text" value="<?= $BARANG['SPFLAIN']; ?>" maxlength="15" />
                                </td>
                            </tr>
                        </table>
                        </fieldset>
                    </td>
                    <td style="vertical-align: top; ">
                        <fieldset>
                        <legend>Tarif HS</legend>
                        <table width="100%">
                            <tr>
                                <td width="100px">Tarif BM</td>
                                <td>: <span id="TRPBM"><?= $BARANG['TRPBM']; ?></span></td>
                            </tr>
                            <tr>
                                <td>Tarif Cukai</td>
                                <td>: <span id="TRPCUK"><?= $BARANG['TRPCUK']; ?></span></td>
                            </tr>
                            <tr>
                                <td>Tarif PPh</td>
                                <td>: <span id="TRPPPH"><?= $BARANG['TRPPPH']; ?></span></td>
                            </tr>
                            <tr>
                                <td>Tarif PPnBM</td>
                                <td>: <span id="TRPPBM"><?= $BARANG['TRPPBM']; ?></span></td>
                            </tr>
                            <tr>
                                <td>Tarif PPn</td>
                                <td>: <span id="TRPPPN"><?= $BARANG['TRPPPN']; ?></span></td>
                            </tr>
                            
                        </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><div class="msg_"></div></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button onclick="save_post_msg('fbarang');" class="btn">Simpan</button>
                        <button onclick="cancel('fbarang');" class="btn">Batal</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
<script>