<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);
?>	
<div class="content_luar">
    <div class="content_dalam">
        <form name="fimportir" id="fimportir" action="<?= site_url('referensi/importir'); ?>" method="post" autocomplete="off" onsubmit="return false;" >
            <input type="hidden" name="act" id="act" value="<?= $act; ?>" readonly="readonly" />
            <input type="hidden" name="IMPORTIR[KODE_TRADER]" value="<?= $IMPORTIR['KODE_TRADER']; ?>" readonly="readonly" />
            <h4><span class="info_2">&nbsp;</span><?= $judul; ?></h4>
            <table width="100%">
                <tr>
                    <td width="50%" style="vertical-align: top; ">
                        <fieldset>
                            <legend>Data Importir</legend>
                            <table width="100%">
                                <tr>
                                    <td width="25%">Identitas</td>
                                    <td width="75%">
                                        <?= form_dropdown('IMPORTIR[IMPID]', $JENIS_IDENTITAS, $IMPORTIR['IMPID'], 'wajib="yes" id="IMPID" class="smtext" style="display: none;"'); ?> <input type="text" name="IMPORTIR[IMPNPWP]" id="IMPNPWP" url="<?= site_url('referensi/autoComplate/IMPORTIR/IMPNPWP'); ?>" urai="IMPID;IMPNPWP;IMPNAMA;IMPALMT;APIKD;APINO" onfocus="Autocomp(this.id);
                                                                                                unformatNPWP(this.id);" onblur="formatNPWP(this.id);" value="<?= $this->fungsi->FORMATNPWP($IMPORTIR['IMPNPWP']); ?>" class="smtext" maxlength="15" wajib="yes" readonly />
                                    </td>
                                </tr> 
                                <tr>
                                    <td>Nama</td>
                                    <td>
                                        <input type="text" name="IMPORTIR[IMPNAMA]" id="IMPNAMA" value="<?= $IMPORTIR['IMPNAMA']; ?>" class="ltext" maxlength="50" wajib="yes" <?= $ro; ?>/>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top" >Alamat</td>
                                    <td>
                                        <textarea name="IMPORTIR[IMPALMT]" id="IMPALMT" class="ltext" wajib="yes" onkeyup="limitChars(this.id, 70, 'limitIMPALMT')" <?= $ro; ?>><?= $IMPORTIR['IMPALMT']; ?></textarea>
                                        <div id="limitIMPALMT"></div>
                                    </td>
                                </tr>            
                                <tr>
                                    <td>Status </td>
                                    <td><?php
                                        $impstatus = explode(",", $IMPORTIR['IMPSTATUS']);
                                        foreach ($impstatus as $val) {
                                            $arr[$val] = 'checked';
                                        }
                                        ?>
                                        <table width="100%">
                                            <tr>
                                                <td><input type="checkbox" name="IMPORTIR[IMPSTATUS][1]" id="IMPSTATUS[1]" value="1" <?= $arr[1] ?> /> <label for="IMPSTATUS[1]">&nbsp;Importir Umum</label></td>
                                                <td><input type="checkbox" name="IMPORTIR[IMPSTATUS][4]" id="IMPSTATUS[4]" value="4" <?= $arr[4] ?>/> <label for="IMPSTATUS[4]">&nbsp;Agen Tunggal</label></td>
                                                <td><input type="checkbox" name="IMPORTIR[IMPSTATUS][7]" id="IMPSTATUS[7]" value="7" <?= $arr[7] ?>/> <label for="IMPSTATUS[7]">&nbsp;DAHANA</label></td>
                                            </tr>
                                            <tr>
                                                <td ><input type="checkbox" name="IMPORTIR[IMPSTATUS][2]" id="IMPSTATUS[2]" value="2" <?= $arr[2] ?>/> <label for="IMPSTATUS[2]">&nbsp;Importir Produser</label></td>
                                                <td ><input type="checkbox" name="IMPORTIR[IMPSTATUS][5]" id="IMPSTATUS[5]" value="5" <?= $arr[5] ?>/> <label for="IMPSTATUS[5]">&nbsp;BULOG</label></td>
                                                <td ><input type="checkbox" name="IMPORTIR[IMPSTATUS][8]" id="IMPSTATUS[8]" value="8" <?= $arr[8] ?>/> <label for="IMPSTATUS[8]">&nbsp;IPTN</label></td>
                                            </tr>
                                            <tr>
                                                <td ><input type="checkbox" name="IMPORTIR[IMPSTATUS][3]" id="IMPSTATUS[3]" value="3" <?= $arr[3] ?>/> <label for="IMPSTATUS[3]">&nbsp;Importir Terdaftar</label></td>
                                                <td ><input type="checkbox" name="IMPORTIR[IMPSTATUS][6]" id="IMPSTATUS[6]" value="6" <?= $arr[6] ?>/> <label for="IMPSTATUS[6]">&nbsp;PERTAMINA</label></td>
                                                <td >&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>APIP / APIU</td>
                                    <td>
<?= form_dropdown('IMPORTIR[APIKD]', $JENIS_API, $IMPORTIR['APIKD'], 'id="APIKD" class="smtext" '); ?>
                                        <input type="text" name="IMPORTIR[APINO]" id="APINO" value="<?= $IMPORTIR['APINO'] ?>" url="<?= site_url(); ?>/autocomplete/importir" class="smtext" maxlength="15"/>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><div class="msg_"></div></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button onclick="save_post_msg('fimportir');" class="btn">Simpan</button>
                        <button onclick="cancel('fimportir');" class="btn">Batal</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>