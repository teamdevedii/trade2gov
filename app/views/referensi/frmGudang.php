<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);
$ro = '';
if($act == 'update')$ro = 'readonly';
?>	
<div class="content_luar">
    <div class="content_dalam">
        <form name="fgudang" id="fgudang" action="<?= site_url('referensi/gudang'); ?>" method="post" autocomplete="off" onsubmit="return false;" >
            <input type="hidden" name="act" id="act" value="<?= $act; ?>" readonly="readonly" />
            <h4><span class="info_2">&nbsp;</span><?= $judul; ?></h4>
            <table width="100%">
                <tr>
                    <td width="50%" style="vertical-align: top; ">
                        <fieldset>
							<legend>Data Gudang</legend>
								<table width="100%">
									<tr>
										<td>Kode KPBC</td>
										<td>
											<input type="text" name="GUDANG[KDKPBC]" id="KDKPBC" value="<?= $GUDANG['KDKPBC']; ?>" class="ltext" maxlength="50" wajib="yes" <?= $ro; ?>/>
										</td>
									</tr>
									<tr>
										<td>Kode Gudang</td>
										<td>
											<input type="text" name="GUDANG[KDGDG]" id="KDGDG" value="<?= $GUDANG['KDGDG']; ?>" class="ltext" maxlength="50" wajib="yes" <?= $ro; ?>/>
										</td>
									</tr>
									<tr>
										<td>Uraian</td>
										<td>
											<input type="text" name="GUDANG[URAIAN]" id="URAIAN" value="<?= $GUDANG['URAIAN']; ?>" class="ltext" maxlength="50" wajib="yes"/>
										</td>
									</tr>
								</table>
						</fieldset>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center"><div class="msg_"></div></td>
				</tr>
				<tr>
					<td colspan="2">
						<button onclick="save_post_msg('fgudang');" class="btn">Simpan</button>
						<button onclick="cancel('fgudang');" class="btn">Batal</button>
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>
<script>