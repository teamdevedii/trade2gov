<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);
$ro = '';
if($act == 'update')$ro = 'readonly';
?>	
<div class="content_luar">
    <div class="content_dalam">
        <form name="fsetting" id="fsetting" action="<?= site_url('referensi/setting'); ?>" method="post" autocomplete="off" onsubmit="return false;" >
            <input type="hidden" name="act" id="act" value="<?= $act; ?>" readonly="readonly" />
            <h4><span class="info_2">&nbsp;</span><?= $judul; ?></h4>
            <table width="100%">
                <tr>
                    <td width="50%" style="vertical-align: top; ">
                        <fieldset>
							<legend>Data Setting</legend>
								<table width="100%">
									<tr>
										<td>Kode Modul</td>
										<td>
											<input type="text" name="SETTING[MODUL]" id="MODUL" value="<?= $SETTING['MODUL']; ?>" class="ltext" maxlength="50" wajib="yes" <?= $ro; ?>/>
										</td>
									</tr>
									<tr>
										<td>Variabel</td>
										<td>
											<input type="text" name="SETTING[VARIABLE]" id="VARIABLE" value="<?= $SETTING['VARIABLE']; ?>" class="ltext" maxlength="50" wajib="yes" <?= $ro; ?>/>
										</td>
									</tr>
									<tr>
										<td>Value</td>
										<td>
											<input type="text" name="SETTING[VALUE]" id="VALUE" value="<?= $SETTING['VALUE']; ?>" class="ltext" maxlength="50" wajib="yes"/>
										</td>
									</tr>
								</table>
						</fieldset>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center"><div class="msg_"></div></td>
				</tr>
				<tr>
					<td colspan="2">
						<button onclick="save_post_msg('fsetting');" class="btn">Simpan</button>
						<button onclick="cancel('fsetting');" class="btn">Batal</button>
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>