<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);
$ro = '';
if ($act == 'update')
    $ro = 'readonly';
?>	
<div class="content_luar">
    <div class="content_dalam">
        <form name="fnegara" id="fnegara" action="<?= site_url('referensi/negara'); ?>" method="post" autocomplete="off" onsubmit="return false;" >
            <input type="hidden" name="act" id="act" value="<?= $act; ?>" readonly="readonly" />
            <h4><span class="info_2">&nbsp;</span><?= $judul; ?></h4>
            <table width="100%">
                <tr>
                    <td width="50%" style="vertical-align: top; ">
                        <fieldset>
                            <legend>Data Negara</legend>
                            <table width="100%">
                                <tr>
                                    <td>Kode Negara</td>
                                    <td>
                                        <input type="text" name="NEGARA[KODE_NEGARA]" id="KODE_NEGARA" value="<?= $NEGARA['KODE_NEGARA']; ?>" class="ltext" maxlength="50" wajib="yes" <?= $ro; ?>/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Uraian</td>
                                    <td>
                                        <input type="text" name="NEGARA[URAIAN_NEGARA]" id="URAIAN_NEGARA" value="<?= $NEGARA['URAIAN_NEGARA']; ?>" class="ltext" maxlength="50" wajib="yes"/>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><div class="msg_"></div></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button onclick="save_post_msg('fnegara');" class="btn">Simpan</button>
                        <button onclick="cancel('fnegara');" class="btn">Batal</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>