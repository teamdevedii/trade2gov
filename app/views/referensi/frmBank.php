<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);
$ro = '';
if ($act == 'update')$ro = 'readonly';
?>	
<div class="content_luar">
    <div class="content_dalam">
        <form name="fbank" id="fbank" action="<?= site_url('referensi/bank'); ?>" method="post" autocomplete="off" onsubmit="return false;" >
            <input type="hidden" name="act" id="act" value="<?= $act; ?>" readonly="readonly" />
            <h4><span class="info_2">&nbsp;</span><?= $judul; ?></h4>
            <table width="100%">
                <tr>
                    <td width="50%" style="vertical-align: top; ">
                        <fieldset>
                            <legend>Data Bank</legend>
                            <table width="100%">
                                <tr>
                                    <td style="width: 150px">Kode Bank</td>
                                    <td>
                                        <input type="text" name="BANK[KDBANK]" id="KDBANK" value="<?= $BANK['KDBANK']; ?>" class="ltext" maxlength="50" wajib="yes" <?= $ro; ?> />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Nama Bank</td>
                                    <td>
                                        <input type="text" name="BANK[NMBANK]" id="NMBANK" value="<?= $BANK['NMBANK']; ?>" class="ltext" maxlength="50" wajib="yes"/>
                                    </td>
                                </tr>

                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><div class="msg_"></div></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button onclick="save_post_msg('fbank');" class="btn">Simpan</button>
                        <button onclick="cancel('fbank');" class="btn">Batal</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>