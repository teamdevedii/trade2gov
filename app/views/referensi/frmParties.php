<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);

?>
<script>
    $(document).ready(function() {
        act($('#act').val());
    });
    
    function act(id){
        switch(id){
            case'add':
                $("#KODEPARTIES").removeAttr('readonly');
                $("#NAMAPARTIES").removeAttr('readonly');
                break;
            case'update':
                $("#KODEPARTIES").attr('readonly', 'readonly');
                $("#NAMAPARTIES").attr('readonly', 'readonly');
                break;
        }
    }    
</script>
<div class="content_luar">
    <div class="content_dalam">
        <form name="fparties" id="fparties" action="<?= site_url('referensi_1/parties'); ?>" method="post" autocomplete="off" onsubmit="return false;" >
            <input type="text" name="act" id="act" value="<?= $act; ?>" onchange="act();" readonly="readonly" />
            <input type="text" name="PARTIES[KODE_TRADER]" value="<?= $PARTIES['KODE_TRADER']; ?>" readonly="readonly" />
            <h4><span class="info_2">&nbsp;</span><?= $judul; ?></h4>
            <table width="100%">
                <tr>
                    <td width="50%" style="vertical-align: top; ">
                        <fieldset>
                        <legend>Data Parties</legend>
                        <table width="100%">
                            <tr>
                                <td>Kode Parties</td>
                                <td>
                                    <?= form_dropdown('PARTIES[KODEPARTIES]', $KD_PARTIES, $PARTIES['KODEPARTIES'], '  id="KODEPARTIES" class="smtext"'); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Nama Parties</td>
                                <td>:
                                    <input wajib="yes" type="text" name="PARTIES[NAMAPARTIES]" id="NAMAPARTIES" value="<?= $PARTIES['NAMAPARTIES']; ?>" class="ltext"/></td>
                            <tr>
                                <td>Alamat Parties</td>
                                <td>:
                                    <input wajib="yes" type="text" name="PARTIES[ALMTPARTIES]" id="ALMTPARTIES" value="<?= $PARTIES['ALMTPARTIES']; ?>" class="ltext"/></td>
                            </tr>
                            
                        </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><div class="msg_"></div></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button onclick="save_post_msg('fparties');" class="btn">Simpan</button>
                        <button onclick="cancel('fparties');" class="btn">Batal</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>