<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);
$ro = '';
if ($act == 'update')
    $ro = 'readonly';
?>	
<div class="content_luar">
    <div class="content_dalam">
        <form name="fdaerah" id="fdaerah" action="<?= site_url('referensi/daerah'); ?>" method="post" autocomplete="off" onsubmit="return false;" >
            <input type="hidden" name="act" id="act" value="<?= $act; ?>" readonly="readonly" />
            <h4><span class="info_2">&nbsp;</span><?= $judul; ?></h4>
            <table width="100%">
                <tr>
                    <td width="50%" style="vertical-align: top; ">
                        <fieldset>
                            <legend>Data Daerah</legend>
                            <table width="100%">
                                <tr>
                                    <td>Kode Daerah</td>
                                    <td>
                                        <input type="text" name="DAERAH[KDDAERAH]" id="KDDAERAH" value="<?= $DAERAH['KDDAERAH']; ?>" class="ltext" maxlength="50" wajib="yes" <?= $ro; ?>/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Uraian Daerah</td>
                                    <td>
                                        <input type="text" name="DAERAH[URDAERAH]" id="URDAERAH" value="<?= $DAERAH['URDAERAH']; ?>" class="ltext" maxlength="50" wajib="yes"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Ibukota</td>
                                    <td>
                                        <input type="text" name="DAERAH[IBUKOTA]" id="IBUKOTA" value="<?= $DAERAH['IBUKOTA']; ?>" class="ltext" maxlength="50" wajib="yes"/>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><div class="msg_"></div></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button onclick="save_post_msg('fdaerah');" class="btn">Simpan</button>
                        <button onclick="cancel('fdaerah');" class="btn">Batal</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>