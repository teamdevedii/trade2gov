<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);
?>	
<div class="content_luar">
    <div class="content_dalam">
        <form name="findentor" id="findentor" action="<?= site_url('referensi/indentor'); ?>" method="post" autocomplete="off" onsubmit="return false;" >
            <input type="hidden" name="act" id="act" value="<?= $act; ?>" readonly="readonly" />
            <input type="hidden" name="INDENTOR[KODE_TRADER]" value="<?= $INDENTOR['KODE_TRADER']; ?>" readonly="readonly" />
            <input type="hidden" name="INDENTOR[INDID]" value="<?= $INDENTOR['INDID']; ?>" readonly="readonly" />
            <input type="hidden" name="INDENTOR[INDNPWP]" value="<?= $INDENTOR['INDNPWP']; ?>" readonly="readonly" />
            <h4><span class="info_2">&nbsp;</span><?= $judul; ?></h4>
            <table width="100%">
                <tr>
                    <td width="50%" style="vertical-align: top; ">
                        <fieldset>
                        <legend>Data Pemilik Barang / Indentor / QQ</legend>
                        <table width="100%">
                            <tr>
                                <td>Nama</td>
                                <td>
                                    <input type="text" name="INDENTOR[INDNAMA]" id="INDNAMA" value="<?= $INDENTOR['INDNAMA']; ?>" class="ltext" wajib="yes" url="<?= site_url('referensi/autoComplate/INDENTOR/INDNAMA'); ?>" urai="INDNPWP;INDNAMA;INDALMT;INDID" onfocus="Autocomp(this.id);" readonly=readonly />
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top">Alamat</td>
                                <td>
                                    <textarea name="INDENTOR[INDALMT]" id="INDALMT" class="ltext" wajib="yes" onkeyup="limitChars(this.id, 70, 'limitINDALMT')"><?= $INDENTOR['INDALMT']; ?></textarea>
                                    <div id="limitINDALMT"></div>
                                </td>
                            </tr>
                        </table>
						</fieldset>
                    </td>    
                </tr>
                <tr>
                    <td colspan="2" align="center"><div class="msg_"></div></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button onclick="save_post_msg('findentor');" class="btn">Simpan</button>
                        <button onclick="cancel('findentor');" class="btn">Batal</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
<script>