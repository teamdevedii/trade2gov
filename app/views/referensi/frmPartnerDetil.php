<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);
$ds['update'] = 'readonly="readonly"';
$c[$PARTNERDETIL['DIRECTION']] = 'checked';
?>
<form name="fpartnerdetil" id="fpartnerdetil" action="<?= site_url('referensi/partnerdetil'); ?>" method="post" autocomplete="off" onsubmit="return false;" >
    <input type="hidden" name="act" id="act" value="<?= $act; ?>"/>
    <input type="hidden" name="key" id="key" value="<?= $key; ?>"/>
    <input type="hidden" name="PARTNERDETIL[KODE_TRADER]" value="<?= $PARTNERDETIL['KODE_TRADER']; ?>" />
    <input type="hidden" name="PARTNERDETIL[NOMOREDI]" value="<?= $PARTNERDETIL['NOMOREDI']; ?>" />
        <fieldset>
        <legend>Data Partner Detil</legend>
        <table width="100%" class="tabelclrajax">
            <tr>
                <td width="80px">Kode KPBC </td>
                <td style="white-space: nowrap">:
                    <input type="text" name="PARTNERDETIL[KDKPBC]" class="ssstext" maxlength="6" value="<?= $PARTNERDETIL['KDKPBC']; ?>" readonly="readonly"/> &nbsp;
                    <span ><?= $PARTNERDETIL['URAIAN_KPBC'];?></span>
                </td>
            </tr>
            <tr>
                <td>APRF</td>
                <td>: <?= form_dropdown('PARTNERDETIL[APRF]', $APRF, $PARTNERDETIL['APRF'], 'wajib="yes" id="APRF" class="vltext" '.$ds[$act]); ?></td>
            </tr>
            <tr>
                <td>Direction</td>
                <td>: 
                    <input type="radio" value="S" id="direction2" name="PARTNERDETIL[DIRECTION]" <?= $c['S']?>><label for="direction2">Send</label>
                    <input type="radio" value="R" id="direction1" name="PARTNERDETIL[DIRECTION]" <?= $c['R']?>><label for="direction1">Receive</label>
                    </td>
            </tr>
            <tr>
                <td colspan="2">
                    <button onclick="save_post_msg('fpartnerdetil','','filltd');" class="btn">Simpan</button>
                    <button onclick="cancel('fpartnerdetil');" class="btn">Batal</button>
                </td>
            </tr>
        </table>
    </fieldset>
</form>
