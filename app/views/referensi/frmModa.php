<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);
?>	
<div class="content_luar">
    <div class="content_dalam">
        <form name="fmoda" id="fmoda" action="<?= site_url('referensi/moda'); ?>" method="post" autocomplete="off" onsubmit="return false;" >
            <input type="hidden" name="act" id="act" value="<?= $act; ?>" readonly="readonly" />
            <input type="hidden" name="MODA[KODE_TRADER]" value="<?= $MODA['KODE_TRADER']; ?>" readonly="readonly" />
            <input type="hidden" name="MODA[MODA]" value="<?= $MODA['MODA']; ?>" readonly="readonly" />
            <h4><span class="info_2">&nbsp;</span><?= $judul; ?></h4>
            <table width="100%">
                <tr>
                    <td width="50%" style="vertical-align: top; ">
                        <fieldset>
                            <legend>Data Sarana Angkut</legend>
                            <table width="100%">
                                <tr>
                                    <td width="25%">Moda - Nama</td>
                                    <td width="75%">
                                        <input type="text" name="MODA[ANGKUTNAMA]" id="ANGKUTNAMA" value="<?= $MODA['ANGKUTNAMA']; ?>" wajib="yes" class="mtext" url="<?= site_url('referensi/autoComplate/MODA/ANGKUTNAMA'); ?>" urai="MODA;ANGKUTNAMA;ANGKUTFL;URANGKUTFL" onfocus="Autocomp(this.id);" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Bendera</td>
                                    <td>
                                        <input type="text" name="MODA[ANGKUTFL]" id="ANGKUTFL" value="<?= $MODA['ANGKUTFL']; ?>" class="sstext" url="<?= site_url('referensi/autoComplate/NEGARA/KODE_NEGARA'); ?>" onfocus="Autocomp(this.id)" urai="ANGKUTFL;URANGKUTFL"/> <input type="button" name="cari" id="cari" class="button" onclick="tb_search('negara', 'ANGKUTFL;URANGKUTFL', 'Kode Negara', this.form.id, 650, 400)" value="...">
                                        <span id="URANGKUTFL"><?= $MODA['URANGKUTFL'] == '' ? $URANGKUTFL : $MODA['URANGKUTFL']; ?></span>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>    
                </tr>
                <tr>
                    <td colspan="2" align="center"><div class="msg_"></div></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button onclick="save_post_msg('fmoda');" class="btn">Simpan</button>
                        <button onclick="cancel('fmoda);" class="btn">Batal</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>