<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);
$ro = '';
if ($act == 'update')
    $ro = 'readonly';
?>	
<div class="content_luar">
    <div class="content_dalam">
        <form name="fsatuan" id="fsatuan" action="<?= site_url('referensi/satuan'); ?>" method="post" autocomplete="off" onsubmit="return false;" >
            <input type="hidden" name="act" id="act" value="<?= $act; ?>" readonly="readonly" />
            <h4><span class="info_2">&nbsp;</span><?= $judul; ?></h4>
            <table width="100%">
                <tr>
                    <td width="50%" style="vertical-align: top; ">
                        <fieldset>
                            <legend>Data Satuan</legend>
                            <table width="100%">
                                <tr>
                                    <td>Kode EDI</td>
                                    <td>
                                        <input type="text" name="SATUAN[KDEDI]" id="KDEDI" value="<?= $SATUAN['KDEDI']; ?>" class="ltext" maxlength="50" wajib="yes" <?= $ro; ?>/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Uraian EDI</td>
                                    <td>
                                        <input type="text" name="SATUAN[UREDI]" id="UREDI" value="<?= $SATUAN['UREDI']; ?>" class="ltext" maxlength="50" wajib="yes"/>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><div class="msg_"></div></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button onclick="save_post_msg('fsatuan');" class="btn">Simpan</button>
                        <button onclick="cancel('fsatuan');" class="btn">Batal</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>