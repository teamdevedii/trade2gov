<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);
$ro = '';
if ($act == 'update')
    $ro = 'readonly';
?>	
<div class="content_luar">
    <div class="content_dalam">
        <form name="fTod2" id="fTod2" action="<?= site_url('referensi/tod2'); ?>" method="post" autocomplete="off" onsubmit="return false;" >
            <input type="hidden" name="act" id="act" value="<?= $act; ?>" readonly="readonly" />
            <h4><span class="info_2">&nbsp;</span><?= $judul; ?></h4>
            <table width="100%">
                <tr>
                    <td width="50%" style="vertical-align: top; ">
                        <fieldset>
                            <legend>Data TOD</legend>
                            <table width="100%">
                                <tr>
                                    <td>REGION</td>
                                    <td>
                                        <input type="text" name="TOD[REGION]" id="REGION" value="<?= $TOD['REGION']; ?>" class="ltext" maxlength="2" wajib="yes" <?= $ro; ?>/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Jenis Moda</td>
                                    <td>                                    
                                        <?= form_dropdown('TOD[JNSMODA]', $MODA, $TOD['JNSMODA'], 'wajib="yes" id="JNSMODA"'); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Tanggal Berlaku</td>
                                    <td>
                                        <input type="text" id="TGLBERLAKU" name="TOD[TGLBERLAKU]" class="sstext" value="<?= $TOD['TGLBERLAKU']; ?>" maxlength="10" formatDate="yes" onfocus="ShowDP(this.id)" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Nilai Freight</td>
                                    <td>
                                        <input type="text" name="TOD[NFREIGHT]" id="NFREIGHT" value="<?= $TOD['NFREIGHT']; ?>" class="ltext"  wajib="yes"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Nilai Asuransi</td>
                                    <td>
                                        <input type="text" name="TOD[NASURANSI]" id="NASURANSI" value="<?= $TOD['NASURANSI']; ?>" class="ltext"  wajib="yes"/>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><div class="msg_"></div></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button onclick="save_post_msg('fTod2');" class="btn">Simpan</button>
                        <button onclick="cancel('fTod2');" class="btn">Batal</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>

<script>
    $(document).ready(function () {        
        declareDP('fTod2');       
    });    
</script>
