<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);
$ro = '';
if ($act == 'update')
    $ro = 'readonly';
?>	
<div class="content_luar">
    <div class="content_dalam">
        <form name="fhs" id="fhs" action="<?= site_url('referensi/hs'); ?>" method="post" autocomplete="off" onsubmit="return false;" >
            <input type="hidden" name="act" id="act" value="<?= $act; ?>" readonly="readonly" />
            <h4><span class="info_2">&nbsp;</span><?= $judul; ?></h4>
            <table width="100%">
                <tr>
                    <td width="50%" style="vertical-align: top; ">
                        <fieldset>
                            <legend>Data Hs</legend>
                            <table width="100%">
                                <tr>
                                    <td>HS</td>
                                    <td>
                                        <input type="text" name="HS[HS]" id="HS" value="<?= $HS['HS']; ?>" class="ltext" maxlength="50" wajib="yes" <?= $ro; ?>/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Deskripsi</td>
                                    <td>
                                        <input type="text" name="HS[DESCRIPTION]" id="DESCRIPTION" value="<?= $HS['DESCRIPTION']; ?>" class="ltext" maxlength="50" wajib="yes"/>
                                    </td>
                                </tr>                                
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><div class="msg_"></div></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button onclick="save_post_msg('fhs');" class="btn">Simpan</button>
                        <button onclick="cancel('fhs');" class="btn">Batal</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>