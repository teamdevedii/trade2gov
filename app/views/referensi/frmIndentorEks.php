<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);

?>
<div class="content_luar">
    <div class="content_dalam">
        <form name="findentoreks" id="findentoreks" action="<?= site_url('referensi_1/indentorEks'); ?>" method="post" autocomplete="off" onsubmit="return false;" >
            <input type="text" name="act" id="act" value="<?= $act; ?>" onchange="act();" readonly="readonly" />
            <input type="text" name="INDENTOREKS[KODE_TRADER]" value="<?= $INDENTOREKS['KODE_TRADER']; ?>" readonly="readonly" />
            <h4><span class="info_2">&nbsp;</span><?= $judul; ?></h4>
            <table width="100%">
                <tr>
                    <td width="50%" style="vertical-align: top; ">
                        <fieldset>
                        <legend>Data Parties</legend>
                        <table width="100%">
                            <tr>
                                <td>ID Indentor</td>
                                <td>:
                                    <input wajib="yes" type="text" name="INDENTOREKS[IDQQ]" id="IDQQ" value="<?= $INDENTOREKS['IDQQ']; ?>" class="ltext" readonly="readonly"/>
                                </td>
                            </tr>
                            <tr>
                                <td>NPWP Indentor</td>
                                <td>:
                                    <input wajib="yes" type="text" name="INDENTOREKS[NPWPQQ]" id="NPWPQQ" value="<?= $INDENTOREKS['NPWPQQ']; ?>" class="ltext" readonly="readonly"/>
                                </td>
                            </tr>
                            
                            <tr>
                                <td>Nama Indeontor</td>
                                <td>:
                                    <input wajib="yes" type="text" name="INDENTOREKS[NAMAQQ]" id="NAMAQQ" value="<?= $INDENTOREKS['NAMAQQ']; ?>" class="ltext"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Alamat Indentor</td>
                                <td>:
                                    <input wajib="yes" type="text" name="INDENTOREKS[ALMTQQ]" id="ALMTQQ" value="<?= $INDENTOREKS['ALMTQQ']; ?>" class="ltext"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Niper</td>
                                <td>:
                                    <input wajib="yes" type="text" name="INDENTOREKS[NIPERQQ]" id="NIPERQQ" value="<?= $INDENTOREKS['NIPERQQ']; ?>" class="ltext"/>
                                </td>
                            </tr>
                        </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><div class="msg_"></div></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button onclick="save_post_msg('findentoreks');" class="btn">Simpan</button>
                        <button onclick="cancel('findentoreks');" class="btn">Batal</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>