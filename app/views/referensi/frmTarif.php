<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);
?>	
<div class="content_luar">
    <div class="content_dalam">
        <form name="ftarif" id="ftarif" action="<?= site_url('referensi/tarif'); ?>" method="post" autocomplete="off" onsubmit="return false;" >
            <input type="hidden" name="act" id="act" value="<?= $act; ?>" readonly="readonly" />
            <input type="hidden" name="TARIF[KODE_TRADER]" value="<?= $TARIF['KODE_TRADER']; ?>" readonly="readonly" />
            <h4><span class="info_2">&nbsp;</span><?= $judul; ?></h4>
            <table width="100%">
                <tr>
                    <td width="50%" style="vertical-align: top; ">
                        <fieldset>
                            <legend>Tarif HS</legend>
                            <table width="100%">
                                <tr>
                                    <td width="150px">Kode HS Seri *</td>
                                    <td >:
                                        <input type="text" name="TARIF[NOHS]" id="NOHS" url="<?= site_url('referensi/autoComplate/HS-TARIF'); ?>" class="stext" wajib="yes" value="<?= $this->fungsi->FormatHS($TARIF['NOHS']); ?>" maxlength="15" readonly="readonly" onblur="FormatHS(this.id)" onfocus="unFormatHS(this.id)" wajib='yes'/> - <input type="text" name="TARIF[SERITRP]" id="SERITRP" style="text-align: center;" class="sssstext" wajib="yes" value="<?= $TARIF['SERITRP']; ?>" readonly="readonly"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100px">Tarif BM</td>
                                    <td>: <input type="text" name="TARIF[TRPBM]" id="TRPBM" value="<?= $TARIF['TRPBM']; ?>" maxlength="50" wajib="yes" <?= $ro; ?>/></td>
                                </tr>
                                <tr>
                                    <td>Tarif Cukai</td>
                                    <td>: <input type="text" name="TARIF[TRPCUK]" id="TRPCUK" value="<?= $TARIF['TRPCUK']; ?>" maxlength="50" wajib="yes" <?= $ro; ?>/></td>
                                </tr>
                                <tr>
                                    <td>Tarif PPh</td>
                                    <td>: <input type="text" name="TARIF[TRPPPH]" id="TRPPPH" value="<?= $TARIF['TRPPPH']; ?>" maxlength="50" wajib="yes" <?= $ro; ?>/></td>
                                </tr>
                                <tr>
                                    <td>Tarif PPnBM</td>
                                    <td>: <input type="text" name="TARIF[TRPPBM]" id="TRPPBM" value="<?= $TARIF['TRPPBM']; ?>" maxlength="50" wajib="yes" <?= $ro; ?>/></td>
                                </tr>
                                <tr>
                                    <td>Tarif PPn</td>
                                    <td>: <input type="text" name="TARIF[TRPPPN]" id="TRPPPN" value="<?= $TARIF['TRPPPN']; ?>" maxlength="50" wajib="yes" <?= $ro; ?>/></td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><div class="msg_"></div></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button onclick="save_post_msg('ftarif');" class="btn">Simpan</button>
                        <button onclick="cancel('ftarif);" class="btn">Batal</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>