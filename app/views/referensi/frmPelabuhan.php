<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);
$ro = '';
if ($act == 'update')
    $ro = 'readonly';
?>	
<div class="content_luar">
    <div class="content_dalam">
        <form name="fpelabuhan" id="fpelabuhan" action="<?= site_url('referensi/pelabuhan'); ?>" method="post" autocomplete="off" onsubmit="return false;" >
            <input type="hidden" name="act" id="act" value="<?= $act; ?>" readonly="readonly" />
            <h4><span class="info_2">&nbsp;</span><?= $judul; ?></h4>
            <table width="100%">
                <tr>
                    <td width="50%" style="vertical-align: top; ">
                        <fieldset>
                            <legend>Data Pelabuhan</legend>
                            <table width="100%">
                                <tr>
                                    <td>Kode Pelabuhan</td>
                                    <td>
                                        <input type="text" name="PELABUHAN[KODE_PELABUHAN]" id="KODE_PELABUHAN" value="<?= $PELABUHAN['KODE_PELABUHAN']; ?>" class="ltext" maxlength="50" wajib="yes" <?= $ro; ?>/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Uraian</td>
                                    <td>
                                        <input type="text" name="PELABUHAN[URAIAN_PELABUHAN]" id="URAIAN_PELABUHAN" value="<?= $PELABUHAN['URAIAN_PELABUHAN']; ?>" class="ltext" maxlength="50" wajib="yes"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Negara</td>
                                    <td>
                                        <input type="text" name="PELABUHAN[KODE_NEGARA]" id="KODE_NEGARA" value="<?= $PELABUHAN['KODE_NEGARA']; ?>" url="<?= site_url('referensi/autoComplate/NEGARA'); ?>" onfocus="Autocomp(this.id)" class="ssstext" urai="KODE_NEGARA;NEG_PELABUHAN" wajib="yes"/> &nbsp;
                                        <input type="button" name="cari" id="cari" class="button" onclick="tb_search('negara', 'KODE_NEGARA;NEG_PELABUHAN', 'Kode Negara', this.form.id, 650, 400)" value="...">
                                        <span id="NEG_PELABUHAN"><?= $PELABUHAN['NEG_PELABUHAN'] == '' ? $NEG_PELABUHAN : $PELABUHAN['NEG_PELABUHAN']; ?></span>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><div class="msg_"></div></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button onclick="save_post_msg('fpelabuhan');" class="btn">Simpan</button>
                        <button onclick="cancel('fpelabuhan');" class="btn">Batal</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>