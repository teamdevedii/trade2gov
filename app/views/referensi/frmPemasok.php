<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);
?>	
<div class="content_luar">
    <div class="content_dalam">
        <form name="fpemasok" id="fpemasok" action="<?= site_url('referensi/pemasok'); ?>" method="post" autocomplete="off" onsubmit="return false;" >
            <input type="hidden" name="act" id="act" value="<?= $act; ?>" readonly="readonly" />
            <input type="hidden" name="PEMASOK[KODE_TRADER]" value="<?= $PEMASOK['KODE_TRADER']; ?>" readonly="readonly" />
            <h4><span class="info_2">&nbsp;</span><?= $judul; ?></h4>
            <table width="100%">
                <tr>
                    <td width="50%" style="vertical-align: top; ">
                        <fieldset>
                        <legend>Data Pemasok</legend>
                        <table width="100%">
                            <tr>
                                <td width="25%">Nama </td>
                                <td width="75%">
                                    <input wajib="yes" type="text" name="PEMASOK[PASOKNAMA]" id="PASOKNAMA" value="<?= $PEMASOK['PASOKNAMA']; ?>" url="<?= site_url('referensi/autoComplate/PEMASOK'); ?>" urai="PASOKNAMA;PASOKALMT;PASOKNEG;URPASOKNEG" onfocus="Autocomp(this.id)" class="ltext" readonly="readonly"/>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top">Alamat </td>
                                <td>
                                    <textarea name="PEMASOK[PASOKALMT]" id="PASOKALMT" wajib="yes" class="ltext" onkeyup="limitChars(this.id, 70, 'limitAlamatPemasok')"><?= $PEMASOK['PASOKALMT']; ?></textarea> <div id="limitAlamatPemasok"></div></td>
                            </tr>
                            <tr>
                                <td>Negara Asal </td>
                                <td>
                                    <input type="text" name="PEMASOK[PASOKNEG]" id="PASOKNEG" value="<?= $PEMASOK['PASOKNEG']; ?>" url="<?= site_url('referensi/autoComplate/NEGARA'); ?>" onfocus="Autocomp(this.id)" class="ssstext" urai="PASOKNEG;URPASOKNEG" wajib="yes"/> &nbsp;
                                    <input type="button" name="cari" id="cari" class="button" onclick="tb_search('negara', 'PASOKNEG;URPASOKNEG', 'Kode Negara', this.form.id, 650, 400)" value="...">
                                    <span id="URPASOKNEG"><?= $PEMASOK['URPASOKNEG'] == '' ? $URPASOKNEG : $PEMASOK['URPASOKNEG']; ?></span>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                    </td>    
                </tr>
                <tr>
                    <td colspan="2" align="center"><div class="msg_"></div></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button onclick="save_post_msg('fpemasok');" class="btn">Simpan</button>
                        <button onclick="cancel('fpemasok);" class="btn">Batal</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>