<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);
$ro = '';
if ($act == 'update')$ro = 'readonly';
?>	
<div class="content_luar">
    <div class="content_dalam">
        <form name="fkpbc" id="fkpbc" action="<?= site_url('referensi/kpbc'); ?>" method="post" autocomplete="off" onsubmit="return false;" >
            <input type="hidden" name="act" id="act" value="<?= $act; ?>" readonly="readonly" />
            <h4><span class="info_2">&nbsp;</span><?= $judul; ?></h4>
            <table width="100%">
                <tr>
                    <td width="50%" style="vertical-align: top; ">
                        <fieldset>
                            <legend>Data KPBC</legend>
                            <table width="100%">
                                <tr>
                                    <td>Kode KPBC</td>
                                    <td>
                                        <input type="text" name="KPBC[KDKPBC]" id="KDKPBC" value="<?= $KPBC['KDKPBC']; ?>" class="ltext" maxlength="50" wajib="yes" <?= $ro; ?>/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Uraian KPBC</td>
                                    <td>
                                        <input type="text" name="KPBC[URAIAN_KPBC]" id="URAIAN_KPBC" value="<?= $KPBC['URAIAN_KPBC']; ?>" class="ltext" maxlength="50" wajib="yes"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>No. Register</td>
                                    <td>
                                        <input type="text" name="KPBC[LAST_NOREG]" id="LAST_NOREG" value="<?= $KPBC['LAST_NOREG']; ?>" class="ltext" maxlength="50" wajib="yes"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Eselon</td>
                                    <td>
                                        <input type="text" name="KPBC[ESELON]" id="ESELON" value="<?= $KPBC['ESELON']; ?>" class="ltext" maxlength="50" wajib="yes"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Kota</td>
                                    <td>
                                        <input type="text" name="KPBC[KOTA]" id="KOTA" value="<?= $KPBC['KOTA']; ?>" url="<?= site_url('referensi/autoComplate/KOTA'); ?>" onfocus="Autocomp(this.id)" class="ssstext" urai="KOTA;URKOTA" wajib="yes"/> &nbsp;<input type="button" name="cari" id="cari" class="button" onclick="tb_search('negara', 'KOTA;URKOTA', 'Kode Negara', this.form.id, 650, 400)" value="...">
                                        <span id="URKOTA"><?= $KPBC['URKOTA'] == '' ? $URKOTA : $KPBC['KOTA']; ?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>NPWP</td>
                                    <td>
                                        <input type="text" name="KPBC[NPWP]" id="NPWP" value="<?= $KPBC['NPWP']; ?>" class="ltext" maxlength="50" wajib="yes"/>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><div class="msg_"></div></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button onclick="save_post_msg('fkpbc');" class="btn">Simpan</button>
                        <button onclick="cancel('fkpbc');" class="btn">Batal</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>