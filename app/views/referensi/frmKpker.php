<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);
$ro = '';
if ($act == 'update')$ro = 'readonly';
?>	
<div class="content_luar">
    <div class="content_dalam">
        <form name="fkpker" id="fkpker" action="<?= site_url('referensi/kpker'); ?>" method="post" autocomplete="off" onsubmit="return false;" >
            <input type="hidden" name="act" id="act" value="<?= $act; ?>" readonly="readonly" />
            <h4><span class="info_2">&nbsp;</span><?= $judul; ?></h4>
            <table width="100%">
                <tr>
                    <td width="50%" style="vertical-align: top; ">
                        <fieldset>
                            <legend>Data KPKER</legend>
                            <table width="100%">
                                <tr>
                                    <td>Kode KPKER</td>
                                    <td>
                                        <input type="text" name="KPKER[KDKPKER]" id="KDKPKER" value="<?= $KPKER['KDKPKER']; ?>" class="ltext" maxlength="50" wajib="yes" <?= $ro; ?>/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Uraian KPKER</td>
                                    <td>
                                        <input type="text" name="KPKER[URAIAN]" id="URAIAN" value="<?= $KPKER['URAIAN']; ?>" class="ltext" maxlength="50" wajib="yes"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>NKWBC</td>
                                    <td>
                                        <input type="text" name="KPKER[KWBC]" id="KWBC" value="<?= $KPKER['KWBC']; ?>" class="ltext" maxlength="50" wajib="yes"/>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><div class="msg_"></div></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button onclick="save_post_msg('fkpker');" class="btn">Simpan</button>
                        <button onclick="cancel('fkpker');" class="btn">Batal</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>