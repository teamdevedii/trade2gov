<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);
$ro = '';
if ($act == 'update')
    $ro = 'readonly';
?>	
<div class="content_luar">
    <div class="content_dalam">
        <form name="fhanggar" id="fhanggar" action="<?= site_url('referensi/hanggar'); ?>" method="post" autocomplete="off" onsubmit="return false;" >
            <input type="hidden" name="act" id="act" value="<?= $act; ?>" readonly="readonly" />
            <h4><span class="info_2">&nbsp;</span><?= $judul; ?></h4>
            <table width="100%">
                <tr>
                    <td width="50%" style="vertical-align: top; ">
                        <fieldset>
                            <legend>Data Hanggar</legend>
                            <table width="100%">
                                <tr>
                                    <td>Kode KPBC</td>
                                    <td>
                                        <input type="text" name="HANGGAR[KDKPBC]" id="KDKPBC" value="<?= $HANGGAR['KDKPBC']; ?>" class="ltext" maxlength="50" wajib="yes" <?= $ro; ?>/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Kode Hanggar</td>
                                    <td>
                                        <input type="text" name="HANGGAR[KDHGR]" id="KDHGR" value="<?= $HANGGAR['KDHGR']; ?>" class="ltext" maxlength="50" wajib="yes" <?= $ro; ?>/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Uraian</td>
                                    <td>
                                        <input type="text" name="HANGGAR[URHGR]" id="URHGR" value="<?= $HANGGAR['URHGR']; ?>" class="ltext" maxlength="50" wajib="yes"/>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><div class="msg_"></div></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button onclick="save_post_msg('fhanggar');" class="btn">Simpan</button>
                        <button onclick="cancel('fhanggar');" class="btn">Batal</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>