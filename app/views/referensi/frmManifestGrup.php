<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);
$ro = '';
if ($act == 'update')
    $ro = 'readonly';
?>	 
<div class="content_luar">
    <div class="content_dalam">
        <form name="fManifestGrup" id="fManifestGrup" action="<?= site_url('referensi/manifest_grup'); ?>" method="post" autocomplete="off" onsubmit="return false;" >
            <input type="hidden" name="act" id="act" value="<?= $act; ?>" readonly="readonly" />
            <h4><span class="info_2">&nbsp;</span><?= $judul; ?></h4>
            <table width="100%">
                <tr>
                    <td width="50%" style="vertical-align: top; ">
                        <fieldset>
                            <legend>Data Manifest Grup</legend>
                            <table width="100%">
                                <tr>
                                    <td>Kode Grup</td>
                                    <td>
                                        <input type="text" name="MANIFESTGRUP[KDGRUP]" id="KDGRUP" value="<?= $MANIFESTGRUP['KDGRUP']; ?>" class="ltext" maxlength="50" wajib="yes" <?= $ro; ?>/>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top" >Uraian</td>
                                    <td>
                                        <textarea name="MANIFESTGRUP[URKDGRUP]" id="URKDGRUP" class="ltext" wajib="yes" ><?= $MANIFESTGRUP['URKDGRUP']; ?></textarea>                                        
                                    </td>
                                </tr>  
                                <tr>
                                    <td>Ada Detil</td>
                                    <td>                                    
                                        <?= form_dropdown('MANIFESTGRUP[ADADETIL]', $ADADETIL, $MANIFESTGRUP['ADADETIL'], 'wajib="yes" id="ADADETIL"'); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Inward</td>
                                    <td>
                                        <?= form_dropdown('MANIFESTGRUP[INWARD]', $INWARD, $MANIFESTGRUP['INWARD'], 'wajib="yes" id="INWARD"'); ?>                                      
                                    </td>
                                </tr>
                                <tr>
                                    <td>Outward</td>
                                    <td>
                                        <?= form_dropdown('MANIFESTGRUP[OUTWARD]', $OUTWARD, $MANIFESTGRUP['OUTWARD'], 'wajib="yes" id="OUTWARD"'); ?>                                          
                                    </td>
                                </tr>
                                <tr>
                                    <td>Ftz</td>
                                    <td>
                                        <?= form_dropdown('MANIFESTGRUP[FTZ]', $FTZ, $MANIFESTGRUP['FTZ'], 'wajib="yes" id="FTZ"'); ?>                                                                                  
                                    </td>
                                </tr>
                                <tr>
                                    <td>Ekspor</td>
                                    <td>
                                        <?= form_dropdown('MANIFESTGRUP[EKSPOR]', $EKSPOR, $MANIFESTGRUP['EKSPOR'], 'wajib="yes" id="EKSPOR"'); ?>                                                                                                                          
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><div class="msg_"></div></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button onclick="save_post_msg('fManifestGrup');" class="btn">Simpan</button>
                        <button onclick="cancel('fManifestGrup');" class="btn">Batal</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>