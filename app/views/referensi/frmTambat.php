<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);
$ro = '';
if ($act == 'update')
    $ro = 'readonly';
?>	
<div class="content_luar">
    <div class="content_dalam">
        <form name="fTambat" id="fTambat" action="<?= site_url('referensi/tambat'); ?>" method="post" autocomplete="off" onsubmit="return false;" >
            <input type="hidden" name="act" id="act" value="<?= $act; ?>" readonly="readonly" />
            <h4><span class="info_2">&nbsp;</span><?= $judul; ?></h4>
            <table width="100%">
                <tr>
                    <td width="50%" style="vertical-align: top; ">
                        <fieldset>
                            <legend>Data Tambat</legend>
                            <table width="100%">
                                <tr>
                                    <td>Kode Tambat</td>
                                    <td>
                                        <input type="text" name="TAMBAT[KD_TAMBAT]" id="KD_TAMBAT" value="<?= $TAMBAT['KD_TAMBAT']; ?>" class="ltext" maxlength="50" wajib="yes" <?= $ro; ?>/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Nama Tambat</td>
                                    <td>
                                        <input type="text" name="TAMBAT[NM_TAMBAT]" id="NM_TAMBAT" value="<?= $TAMBAT['NM_TAMBAT']; ?>" class="ltext" maxlength="50" wajib="yes"/>
                                    </td>
                                </tr>                                
                                <tr>
                                    <td>Nama Pemilik</td>
                                    <td>
                                        <input type="text" name="TAMBAT[NM_PEMILIK]" id="NM_PEMILIK" value="<?= $TAMBAT['NM_PEMILIK']; ?>" class="ltext" maxlength="50" wajib="yes"/>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><div class="msg_"></div></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button onclick="save_post_msg('fTambat');" class="btn">Simpan</button>
                        <button onclick="cancel('fTambat');" class="btn">Batal</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>