<div class="widget" style="width: 400px">
    <div class="widget-header" style="text-align: left; vertical-align: middle;">
        <span class="icon-key-stroke"></span><h3>Request</h3>
    </div> 
    <div class="widget-content">
        <form id="frmRequest" name="frmRequest" onSubmit="javascript:return false;" autocomplete=off action="<?= site_url('outLogin/setRequest'); ?>" method="post" class="form uniformForm validateForm"> 
            <table >
                <tr >
                    <td style="width: 80px">e-Mail</td> 
                    <td>: <input type="text" name="DATA[EMAIL]" id="DATA[EMAIL]" class="validate[required,custom[email]]" placeholder="anda@mail.com" value="<?= $DATA['ID_REQUEST'] ?>" style="width:200px;"></td>
                </tr>
                <tr>
                    <td>Nomor Telepon</td>
                    <td>: <input type="text" name="DATA[TELP]" id="DATA[TELP]"  class="validate[required]" wajib="yes" placeholder="No Telepon" value="<?= $DATA['TELP']; ?>" style="width:200px;"/></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Uraian</td>
                    <td><font style="vertical-align:top"> :</font> <textarea name="DATA[URAIAN]" id="URAIAN" class="validate[required]" wajib="yes"><?= $DATA['URAIAN'] ?></textarea></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <img alt="" id="captcha"  height="40" onClick="change_captcha()" style="cursor:pointer" title="Click to change a code"/>
                    </td>
                </tr>
                <tr>
                    <td>Captcha</td>
                    <td>: <input type="text" name="_code" id="_code" class="validate[required]"  maxlength="5"  style="width:80px; text-align:center"></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button class="btn btn-small" onclick="request();"> &nbsp; Request &nbsp; </button>
                        <button class="btn btn-small" onclick="document.getElementById('frmRequest').reset();"> &nbsp; Reset  &nbsp; </button>
                        <button class="btn btn-small" onclick="change_captcha();"> &nbsp; capcha &nbsp; </button>
                        <div id="pesan"></div>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
<script>
    $(document).ready(function() {
        startUp();
        change_captcha();
    });
    function notifikasi(){
        alert($(this).attr('name'));
    }
    function request() {
        var a = 0;
        $('#frmRequest input[type="text"]').each(function(n, element) {
            if ($(element).val() == '') {
                $(element).focus();
                a = a + 1;
            }
        });
        if(a > 0){
            return false;
        }else{
            //ShowErrorBox('#notify','testr','ok');
            ExecFormRequest('#pesan', '#frmRequest');
        }
        return false;
    }
    function ExecFormRequest(DivID, formId) {
        $.ajax({
            type: 'POST',
            url: $(formId).attr('action') + '/ajax',
            data: $(formId).serialize(),
            dataType: 'html',
            beforeSend: function() {
                $(DivID).css({"display": "center", "padding-right": "60px"});
                $(DivID).html('<img src=\"' + base_url + 'img/loaders/facebook.gif\" alt=\"loading\" border=\"0\" />');
            },
            success: function(data) {
                if (data === "Sukses") {
                    $(DivID).css({"display": "left"});
                    $(DivID).html('Sukses');
                } else {
                    $(DivID).html(data);
                }
                change_captcha();
            }
        });
    }
    
</script>