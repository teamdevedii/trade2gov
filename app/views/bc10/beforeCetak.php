<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
?>
<div class="content_luar">
    <div class="content_dalam">
        <form id="cetak_" method="post" autocomplete="off" onsubmit="return false;"> 
            <fieldset>
                <legend> Pilihan Pencetakan </legend>
                <table width="100%">
                    <tr>
                        <td>
                            <input type="radio" name="CETAK" id="semuaPos" value="1" checked> <label for="semuaPos">&nbsp;Dokumen RKSP</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="radio" name="CETAK" id="posTertentu" value="2"> <label for="posTertentu">&nbsp;Respon</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input id="CAR" type="hidden" value="<?= $car ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br>
                            <button onclick="cetakNewWin($('#CAR').val());" class="btn" ><span class="icon-new-window"></span> Cetak </button>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
</div>
<script>
    function cetakNewWin(car){
        var car = '<?= $car; ?>';
        if($('input[name="CETAK"]:checked').val()=='1'){
            window.open(site_url + '/bc10/cetakDokumen/' + car + '/1','Cetak Dokumen RKSP','scrollbars=yes, resizable=yes,width=1100,height=700');
        }
        else if($('input[name="CETAK"]:checked').val()=='2'){
            Dialog(site_url + '/bc10/lstGroupPos/' + car, 'cetakDokumen', 'Pilih Pos yang akan dicetak.', 650, 600);
        }
    }
</script>