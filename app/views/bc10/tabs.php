<?php
if (!defined('BASEPATH')){
    exit('No direct script access allowed');
}
?>
<script>
    $(document).ready(function(){
        $('#tabs_bc10').tabs();
    });
</script>
<div class="content_luar">
    <div class="content_dalam">
        <div id="tabs_bc10">
            <ul>
                <li><a href="#tab-Header">Data Header</a></li>
            </ul>
            <div id="tab-Header"><?= $HEADER ?></div>
        </div>
    </div>
</div>