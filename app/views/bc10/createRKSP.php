<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
$ro = '';
if ($TIPE_TRADER == '1') {
    $ro = 'readonly';
}
?>
<span id="DivHeaderForm">
    <span id="divtmp"></span>
    <form id="frmRKSPHdr" name="frmRKSPHdr" action="<?= site_url('bc10/setHeaderRKSP'); ?>" method="post" autocomplete="off" onsubmit="return false;" class="form uniformForm">
        <input type="hidden" name="act" id="act" value="<?= $act; ?>" />
        <input type="hidden" name="HEADER[CAR]" id="CAR" value="<?= $HEADER[CAR]; ?>" />
        <h4>
            <span id="off">
                <button id="" onclick="onoff(true);
                        return false;" class="btn onterus"><span class="icon-pen"></span> Edit </button>
            </span>
            <span id="on">
                <button onclick="save_post_msg('frmRKSPHdr');" class="btn onterus">
                    <!-- onoff(false);-->
                    <span class="icon-download"></span> Simpan
                </button>
                <button onclick="onoff(false);" class="btn onterus">
                    <span class="icon-undo"></span> Batal
                </button>
            </span>

            <span class="msgheader_" style="margin-left:20px">&nbsp;</span>
            <?php if ($HEADER['CAR']) { ?>
                <button id="btnQUEUED" onclick="gotoQUEUED($('#noaju').val())" class="btn onterus" style="float: right; margin-right: 5px;" ><span class="icon-mail"></span> Edifact / UnEdifact</button>
            <?php } ?>

        </h4>
        <div class="content_luar">
            <div class="content_dalam">

                <table style=" width:100%">
                    <tr>
                        <td style=" width: 50%">
                            <table>
                                <tr>
                                    <td>Jenis Dokumen</td>
                                    <td><?php
                                        $jadwalkapal = explode(",", $HEADER['JNSMANIFEST']);
                                        foreach ($jadwalkapal as $val) {
                                            $arr[$val] = 'checked';
                                        }
                                        ?>
                                        <input type="radio" name="HEADER[JNSMANIFEST]" id="JNSMANIFEST" value="R">RKSP
                                        <input type="radio" name="HEADER[JNSMANIFEST]" id="JNSMANIFEST" value="J">JKSP     
                                    </td>
                                </tr>
                            </table>
                            <fieldset>
                                <legend>Dokumen</legend>
                                <table>
                                    <tr>
                                        <td height="25px;">Nomor Aju </td>
                                        <td><b><?= substr($HEADER['CAR'], 0, 6) . '-' . substr($HEADER['CAR'], 7, 6) . '-' . substr($HEADER['CAR'], 12, 8) . '-' . substr($HEADER['CAR'], 20, 6); ?></b></td>

                                    </tr>
                                    <tr>
                                        <td width="25%">KPBC</td>
                                        <td width="75%">
                                            <input type="text"  name="HEADER[KDKPBC]" id="KDKPBC" value="<?= $HEADER['KDKPBC']; ?>" url="<?= site_url('referensi/autoComplate/KPBC'); ?>" urai="KDKPBC;URKPBC" onfocus="Autocomp(this.id)" class="ssstext" maxlength="6" /> &nbsp;
                                            <button onclick="tb_search('kpbc', 'KDKPBC;URKPBC', 'Kode Kpbc', 'kpbc', 650, 400);" class="btn"> ... </button>
                                            <span id="URKPBC"><?= $HEADER['URAIAN_KPBC'] == '' ? $URKANTOR_TUJUAN : $HEADER['URAIAN_KPBC']; ?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nomor BC 1.0</td>
                                        <td><input type="text"  size="20" id="NOBC10" name="HEADER[NOBC10]" value="<?= $HEADER['NOBC10']; ?>"/></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal BC 1.0</td>
                                        <td><input type="text" name="HEADER[TGBC10]" id="TGBC10" class="sstext" value="<?= $HEADER['TGBC10']; ?>" maxlength="10" formatDate="yes" readonly="readonly"/></td>
                                    </tr>
                                </table>
                            </fieldset>
                            <table>
                                <tr>
                                    <td width="100px">Jenis Sarana Angkut</td>
                                    <td>
                                        <?= form_dropdown('HEADER[MODA]', $MODA, $HEADER['MODA'], ' onchange="changeModa(this.value)" id="MODA" class="stext"'); ?> 
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style=" width: 50%">
                            <table width="100%">
                                <tr>
                                    <td style="text-align: right;" >
                                        <button id="btnValidasi" onclick="cekValidasi($('#CAR').val())" class="btn onterus">
                                            <span class="icon-check"></span> Periksa Kelengkapan [ <span id="URSTATUS"><?= $HEADER['URSTATUS']; ?></span> ]
                                        </button>
                                    </td>
                                </tr>
                            </table>
                            <fieldset>
                                <legend>Perusahaan Pengangkut</legend>
                                <table width="100%">
                                    <tr>
                                        <td>ID</td>
                                        <td>
                                            <?= form_dropdown('HEADER[KDIDAGEN]', $KDIDAGEN, $HEADER['KDIDAGEN'], 'id="KDIDAGEN" class="smtext" readonly' . $ro); ?>
                                            <input type="text" name="HEADER[KDIDAGEN]" id="KDIDAGEN" url="<?= site_url('referensi/autoComplate/EKSPORTIR/NPWPEKS'); ?>" urai="IDEKS;EKSNPWP;NAMAEKS;ALMTEKS" onfocus="Autocomp(this.id);
                                                    unformatNPWP(this.id);" onblur="formatNPWP(this.id);" value="<?= $this->fungsi->FORMATNPWP($HEADER['NOIDAGEN']); ?>" class="offterus smtext" maxlength="15" <?= $ro; ?>/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nama</td>
                                        <td><input type="text" name="HEADER[KDAGEN]" id="KDAGEN" value="<?= $HEADER['KDAGEN']; ?>" size="5" readonly/> 
                                            <input type="text" name="HEADER[NMAGEN]" id="NMAGEN" value="<?= $HEADER['NMAGEN']; ?>" readonly/></td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top">Alamat</td>
                                        <td>
                                            <textarea name="HEADER[ALAGEN]" id="ALAGEN" readonly><?= $HEADER['ALAGEN']; ?></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100px">Pemberitahu </td>
                                        <td>
                                            <input type="text" name="HEADER[PEMBERITAHU]" id="PEMBERITAHU" value="<?= $HEADER['PEMBERITAHU']; ?>" readonly/>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                </table>        
                <table style=" width:100%">
                    <tr>
                        <td>
                            <fieldset>
                                <legend>Data Sarana Angkut</legend>
                                <table>
                                    <tr>
                                        <td>
                                            <table width="100%">
                                                <tr>
                                                    <td>Nama / Reg Angkut</td>
                                                    <td>
                                                        <input type="text" name="HEADER[NMMODA]" id="NMMODA" value="<?= $HEADER['NMMODA']; ?>" url="<?= site_url('referensi/autoComplate/MODA/NMMODA'); ?>" urai="REGMODA;NMMODA;FLAGMODA;URFLAGMODA" onfocus="Autocomp(this.id);"/>                                                        
                                                        <input type="text" name="HEADER[REGMODA]" id="REGMODA" value="<?= $HEADER['REGMODA']; ?>" url="<?= site_url('referensi/autoComplate/MODA/REGMODA'); ?>" urai="NMMODA;REGMODA;FLAGMODA;URFLAGMODA" onfocus="Autocomp(this.id);" size="10"/></td>
                                                    <td>
                                                        &nbsp;
                                                        &nbsp;
                                                        &nbsp;
                                                        &nbsp;
                                                        &nbsp;
                                                        &nbsp;                                                
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="100px">Bendera </td>
                                                    <td>
                                                        <input type="text" name="HEADER[FLAGMODA]" id="FLAGMODA" value="<?= $HEADER['FLAGMODA']; ?>" class="sssstext" url="<?= site_url('referensi/autoComplate/NEGARA/KODE_NEGARA'); ?>" onfocus="Autocomp(this.id)" urai="ANGKUTFL;URANGKUTFL"/> 
                                                        <button onclick="tb_search('negara', 'FLAGMODA;URANGKUTFL', 'Kode Negara', 'bendera', 650, 400)" class="btn"> ... </button>
                                                        <span id="URFLAGMODA"><?= $HEADER['URFLAGMODA']; ?></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="100px">Kade </td>
                                                    <td>
                                                        <input type="text" name="HEADER[KADE]" id="KADE" value="<?= $HEADER['KADE']; ?>" url="<?= site_url('referensi/autoComplate/TIMBUN/KDGDG'); ?>" onfocus="Autocomp(this.id, $('#KDKPBC').val())" urai="TMPTBN;URTMPTBN" class="ssstext" maxlength="4"/>&nbsp;
                                                        <button onclick="tb_search('timbun', 'TMPTBN;URTMPTBN', 'Kode Timbun', 'kade', 650, 400, 'KDKPBC;')" class="btn"> ... </button>
                                                        <span id="URTMPTBN"><?= $HEADER['URTMPTBN'] == '' ? $URTMPTBN : $HEADER['URTMPTBN']; ?></span>
                                                    </td>
                                                </tr>                     
                                            </table>
                                        </td>
                                        <td>
                                            <table width="100%" id="angkutanUdata">
                                                <tr>
                                                    <td width="100px">Draft Depan </td>
                                                    <td>
                                                        <input type="text" name="HEADER[DRAFTDEPAN]" id="DRAFTDEPAN" value="<?= $HEADER['DRAFTDEPAN']; ?>"/>
                                                    </td>
                                                    <td>
                                                        &nbsp;
                                                        &nbsp;
                                                        &nbsp;
                                                        &nbsp;
                                                        &nbsp;
                                                        &nbsp;                                                
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="100px">Draft Belakang </td>
                                                    <td>
                                                        <input type="text" name="HEADER[DRAFTBELAKANG]" id="DRAFTBELAKANG" value="<?= $HEADER['DRAFTBELAKANG']; ?>"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="100px">GT </td>
                                                    <td>
                                                        <input type="text" name="HEADER[GT]" id="GT" value="<?= $HEADER['GT']; ?>"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="100px">LOA </td>
                                                    <td>
                                                        <input type="text" name="HEADER[LOA]" id="LOA" value="<?= $HEADER['LOA']; ?>"/>
                                                    </td>
                                                </tr>          
                                            </table>
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td width="50px">Memo</td>
                                                    <td>
                                                        <textarea name="HEADER[MEMO]" id="MEMO" rows="5" cols="40"><?= $HEADER['MEMO']; ?></textarea>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>	 			
                                </table>	
                            </fieldset>
                        </td>                
                    </tr>
                </table>
                <table style=" width:100%">
                    <tr>
                        <td style=" width: 100%">
                            <fieldset>
                                <legend>Pelabuhan </legend>
                                <table>
                                    <tr>
                                        <td>
                                            <table width="100%">
                                                <tr>
                                                    <td width="100px">Asal / Muat</td>
                                                    <td width="75%">
                                                        <input type="text" name="HEADER[PELMUAT]" id="PELMUAT" value="<?= $HEADER['PELMUAT']; ?>" url="<?= site_url('referensi/autoComplate/PELABUHAN/KODE_PELABUHAN'); ?>" onfocus="Autocomp(this.id)" urai="PELMUAT;PELMUATUR" class="ssstext" maxlength="5"/>&nbsp;
                                                        <button onclick="tb_search('pelabuhan', 'PELMUAT;PELMUATUR', 'Kode Pelabuhan', 'Muat', 650, 400)" class="btn"> ... </button>
                                                        <span id="PELMUATUR"><?= $HEADER['PELMUATUR'] == '' ? $PELMUATUR : $HEADER['PELMUATUR']; ?></span>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td width="100px">Transit Akhir </td>
                                                    <td>
                                                        <input type="text" name="HEADER[PELTRANSIT]" id="PELTRANSIT" url="<?= site_url('referensi/autoComplate/PELABUHAN/KODE_PELABUHAN'); ?>" onfocus="Autocomp(this.id)" urai="PELTRANSIT;PELTRANSITUR" class="ssstext" value="<?= $HEADER['PELTRANSIT']; ?>" maxlength="5"/>&nbsp; 
                                                        <button onclick="tb_search('pelabuhan', 'PELTRANSIT;PELTRANSITUR', 'Kode Pelabuhan', 'transit', 650, 400)" class="btn"> ... </button>
                                                        <span id="PELTRANSITUR"><?= $HEADER['PELTRANSITUR'] == '' ? $PELTRANSITUR : $HEADER['PELTRANSITUR']; ?></span></td>
                                                </tr>                                                           
                                            </table>
                                        </td>
                                        <td>
                                            <table width="100%">
                                                <tr>
                                                    <td width="100px">Bongkar </td>
                                                    <td>
                                                        <input type="text" name="HEADER[PELBONGKAR]" id="PELBONGKAR" value="<?= $HEADER['PELBONGKAR']; ?>" url="<?= site_url('referensi/autoComplate/PELABUHAN/KODE_PELABUHAN'); ?>" onfocus="Autocomp(this.id)" urai="PELBKR;PELBKRUR" class="ssstext" maxlength="5"/>&nbsp; 
                                                        <button onclick="tb_search('pelabuhan', 'PELBONGKAR;PELBKRUR', 'Kode Pelabuhan', 'bongkar', 650, 400)" class="btn" > ... </button>
                                                        <span id="PELBKRUR"><?= $HEADER['PELBKRUR'] == '' ? $PELBKRUR : $HEADER['PELBKRUR']; ?></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="100px">Selanjutnya </td>
                                                    <td width="75%">
                                                        <input type="text" name="HEADER[PELBERIKUT]" id="PELBERIKUT" value="<?= $HEADER['PELBERIKUT']; ?>" url="<?= site_url('referensi/autoComplate/PELABUHAN/KODE_PELABUHAN'); ?>" onfocus="Autocomp(this.id)" urai="PELBERIKUT;PELBERIKUTUR" class="ssstext" maxlength="5"/>&nbsp;
                                                        <button onclick="tb_search('pelabuhan', 'PELBERIKUT;PELBERIKUTUR', 'Kode Pelabuhan', 'Selanjutnya', 650, 400)" class="btn"> ... </button>
                                                        <span id="PELBERIKUTUR"><?= $HEADER['PELBERIKUTUR'] == '' ? $PELBERIKUTUR : $HEADER['PELBERIKUTUR']; ?></span>
                                                    </td>
                                                </tr>                                               
                                            </table>
                                        </td>
                                    </tr>	 			
                                </table>	
                            </fieldset>
                        </td>                
                    </tr>
                </table>
                <table style=" width:100%">
                    <tr>
                        <td style=" width: 50%">
                            <fieldset>
                                <legend>Jadwal Kedatangan</legend>
                                <table width="100%">
                                    <tr>
                                        <td width="100px">Nomor Voy </td>
                                        <td>
                                            <input type="text" name="HEADER[ETANOVOY]" id="ETANOVOY" value="<?= $HEADER['ETANOVOY']; ?>"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100px">Tanggal </td>
                                        <td>                                            
                                            <input type="text" name="HEADER[ETATGVOY]" class="sstext" id="ETATGVOY" value="<?= $HEADER['ETATGVOY']; ?>" maxlength="10" formatDate="yes"  readonly="readonly"/>
                                            Jam
                                            <input type="text" name="HEADER[ETAWKVOY]" class="sstext" id="ETAWKVOY" value="<?= $HEADER['ETAWKVOY']; ?>"/>
                                        </td>

                                    </tr>                           
                                </table>
                            </fieldset>                    
                        </td>
                        <td style=" width: 50%">
                            <fieldset>
                                <legend>Jadwal Keberangkatan</legend>
                                <table width="100%">
                                    <tr>
                                        <td width="100px">Nomor Voy </td>
                                        <td>
                                            <input type="text" name="HEADER[ETDNOVOY]" id="ETDNOVOY" value="<?= $HEADER['ETDNOVOY']; ?>"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100px">Tanggal </td>
                                        <td>                                            
                                            <input type="text" name="HEADER[ETDTGVOY]" id="ETDTGVOY" class="sstext" value="<?= $HEADER['ETDTGVOY']; ?>" maxlength="10" formatDate="yes" readonly="readonly"/>
                                            Jam
                                            <input type="text" name="HEADER[ETDWKVOY]" id="ETDWKVOY" class="sstext" value="<?= $HEADER['ETDWKVOY']; ?>"/>
                                        </td>

                                    </tr>                               
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</span>
<script>
    $(document).ready(function() {
        onoff(false);
        declareDP('frmRKSPHdr');
    });
    function onoff(on) {

        if ($('#frmRKSPHdr #CAR').val() == '' || $('#frmRKSPHdr #act').val() == 'save') {
            $('#frmRKSPHdr .outentry').hide();
        } else {
            $('#frmRKSPHdr .outentry').show();
        }
        if (on) {
            $('#on').show();
            $('#off').hide();
            $("#frmRKSPHdr :input").removeAttr("disabled");
            $('#frmRKSPHdr .outentry :input').attr('disabled', 'disabled');
            $("#tabs").tabs({disabled: [1, 2]});
        } else {
            $('#on').hide();
            $('#off').show();
            $("#frmRKSPHdr :input").attr('disabled', 'disabled');
            $('#frmRKSPHdr .outentry :input').removeAttr("disabled");
            $("#tabs").tabs();
            if ($("#act").val() == 'save') {
                $("#tabs").tabs({disabled: [1]});
            } else {
                $("#tabs").tabs({disabled: false});
            }
        }
        $('.onterus').removeAttr('disabled');
        $('.offterus :input').attr('readonly', 'readonly');
    }

    function changeModa(val) {
        if (val == '4') {
            $('#angkutanUdata').hide();
        } else {
            $('#angkutanUdata').show();
        }
    }

    function cekValidasi(car) {
        //alert(car);return false;
        $(function() {
            $('#msgbox').html('');
            $("#msgbox").html('<div style="margin-left: 15px; margin-top: 15px;"><img src="' + base_url + 'img/loaders/facebook.gif" /> loading...</div>');
            $("#msgbox").dialog({
                resizable: false,
                height: 500,
                modal: true,
                width: 700,
                title: 'Validasi Dokumen',
                open: function() {
                    $.ajax({
                        type: 'POST',
                        url: site_url + '/bc10/alertcekstatus/' + car + '/t_bc10hdr',
                        success: function(data) {
                            $("#msgbox").html(data);
                            $('#URSTATUS').html($('#STATUSCEK').val());
                            call(site_url + '/bc10/refreshHeader/' + car, 'frmRKSPHdr');
                        }
                    });
                },
                buttons: {
                    Close: function() {
                        $(this).dialog("close");
                    }
                }
            });
        });
    }

    function gotoQUEUED() {
        jConfirm('Data akan dibentuk EDIFACT ?', site_name,
                function(r) {
                    if (r == true) {
                        jloadings();
                        $(function() {
                            $('#msgbox').html('');
                            $("#msgbox").html('<div style="margin-left: 15px; margin-top: 15px;"><img src="' + base_url + 'img/loaders/facebook.gif" /> loading...</div>');
                            $("#msgbox").dialog({
                                resizable: false,
                                height: 500,
                                modal: true,
                                width: 700,
                                title: 'Edifact / unEdifact',
                                open: function() {
                                    $.ajax({
                                        type: 'POST',
                                        url: site_url + '/bc10/queued',
                                        data: '&CAR=' + $('#CAR').val(),
                                        error: function() {
                                            Clearjloadings();
                                        },
                                        success: function(data) {
                                            Clearjloadings();
                                            $("#msgbox").html(data);
                                            call(site_url + '/bc10/refreshHeader/' + $('#noaju').val());
                                        }
                                    });
                                },
                                buttons: {
                                    Close: function() {
                                        $(this).dialog("close");
                                    }
                                }
                            });
                        });
                    } else {
                        return false;
                    }
                });
    }

    function save_header(formid) {
        //alert($(formid).attr('action')); return false;
        //$('#msgbox').html('');
        var dataSend = $(formid).serialize();
        jConfirm('Anda yakin Akan memproses data ini?', site_name,
                function(r) {
                    if (r == true) {
                        jloadings();
                        $.ajax({
                            type: 'POST',
                            url: $(formid).attr('action'),
                            data: dataSend,
                            success: function(data) {
                                //alert(data);
                                Clearjloadings();
                            }
                        });
                    } else {
                        return false;
                    }
                });
    }
</script>