<?php
if (!defined('BASEPATH')){
    exit('No direct script access allowed');
}
?>
<span id="DivHeaderForm">
    <form id="frmHeader" name="frmHeader" action="<?= site_url('bc10/saveHeader'); ?>" method="post" autocomplete="off" onsubmit="return false;" class="form uniformForm">
        <?= $MAXLENGTH ?>
        <input type="hidden" name="act" id="act" value="<?= $act; ?>" />
        <input type="hidden" name="HEADER[CAR]" id="CAR" value="<?= $HEADER['CAR']; ?>" />
        <input type="hidden" id="STATUS" value="<?= $HEADER['STATUS']; ?>" />
        
        <h4>
            <span id="off">
                <button id="" onclick="onoff(true); return false;" class="btn onterus"><span class="icon-pen"></span> Edit </button>
            </span>
            <span id="on">
                <button onclick="save_header('frmHeader');" class="btn onterus">
                    <span class="icon-download"></span> Simpan
                </button>
                <button onclick="onoff(false);" class="btn onterus">
                    <span class="icon-undo"></span> Batal
                </button>
            </span>
<?php if (strpos('|01|02|', $HEADER['STATUS'])>0) { ?>
    <button id="btnQUEUED" onclick="gotoQUEUED($('#CAR').val())" class="btn onterus" style="float: right; margin-right: 5px;" >
        <span class="icon-mail"></span> Bentuk / Batal Edifact
    </button>
<?php } ?>
        </h4>
        <table style=" width:100%">
            <tr>
                <td style=" width: 45%; vertical-align: top;">
                    <table>
                        <tr>
                            <td>Jenis Dokumen</td>
                            <td><?php
                                $jadwalkapal = explode(",", $HEADER['JNSMANIFEST']);
                                foreach ($jadwalkapal as $val) {
                                    $arr[$val] = 'checked';
                                }
                                ?>
                                <input type="radio" name="HEADER[JNSMANIFEST]" id="RKSP" value="R" <?= $arr['R'] ?>> <label for="RKSP"> RKSP </label>
                                <input type="radio" name="HEADER[JNSMANIFEST]" id="JKSP" value="J" <?= $arr['J'] ?>> <label for="JKSP"> JKSP </label>
                            </td>
                        </tr>
                    </table>
                    <fieldset>
                        <legend>Dokumen</legend>
                        <table>
                            <tr>
                                <td>Nomor Aju </td>
                                <td><b><?= substr($HEADER['CAR'], 0, 6) . '-' . substr($HEADER['CAR'], 6, 6) . '-' . substr($HEADER['CAR'], 12, 8) . '-' . substr($HEADER['CAR'], 20, 6); ?></b></td>
                            </tr>
                            <tr>
                                <td width="125px;">KPBC</td>
                                <td>
                                    <input type="text"  name="HEADER[KDKPBC]" id="KDKPBC" value="<?= $HEADER['KDKPBC']; ?>" class="ssstext" onblur="checkCode(this);" grp="kpbc" urai="URKDKPBC"/> &nbsp;
                                    <button onclick="tb_search('kpbc', 'KDKPBC;URKDKPBC', 'Kode Kpbc', 'kpbc', 650, 400);" class="btn"> ... </button>
                                    <span id="URKDKPBC"><?= $HEADER['URKDKPBC']; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Nomor BC 1.0</td>
                                <td><input type="text" class="sstext" value="<?= $HEADER['NOBC10']; ?>" readonly="readonly"/></td>
                            </tr>
                            <tr>
                                <td>Tanggal BC 1.0</td>
                                <td><input type="text" class="smtext" value="<?= $HEADER['TGBC10']; ?>" formatDate="yes" readonly="readonly"/></td>
                            </tr>
                        </table>
                    </fieldset>
                    <table>
                        <tr>
                            <td width="135px">Jenis Sarana Angkut</td>
                            <td>
                                <?= form_dropdown('HEADER[MODA]', $MODA, $HEADER['MODA'], ' id="MODA" class="vltext" onchange="getModa(this.value)"'); ?>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style=" width: 55%;vertical-align: top;">
                    <table width="100%">
                        <tr>
                            <td style="text-align: right;" >
                                <button id="btnValidasi" onclick="cekValidasi($('#CAR').val())" class="btn onterus">
                                    <span class="icon-check"></span> Periksa Kelengkapan [ <span id="URSTATUS"><?= $HEADER['URSTATUS']; ?></span> ]
                                </button>
                            </td>
                        </tr>
                    </table>
                    <fieldset>
                        <legend>Perusahaan Pengangkut</legend>
                        <table width="100%">
                            <tr>
                                <td>Identitas</td>
                                <td>
                                    <?= form_dropdown('HEADER[KDIDAGEN]', $KDIDAGEN, $HEADER['KDIDAGEN'], 'id="KDIDAGEN" class="mtext" '); ?>
                                    <input type="text" name="HEADER[NOIDAGEN]" id="NOIDAGEN" value="<?= $HEADER['NOIDAGEN'] ?>" class="offterus smtext" readonly="readonly"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Nama</td>
                                <td>
                                    <input type="text" name="HEADER[KDAGEN]" id="KDAGEN" value="<?= $HEADER['KDAGEN']; ?>" class="ssstext" readonly="readonly" />
                                    <input type="text" name="HEADER[NMAGEN]" id="NMAGEN" value="<?= $HEADER['NMAGEN']; ?>" class="ltext" readonly="readonly"/></td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top">Alamat</td>
                                <td>
                                    <textarea name="HEADER[ALAGEN]" id="ALAGEN" cols="36" rows="1" readonly="readonly"><?= $HEADER['ALAGEN']; ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td width="100px">Pemberitahu </td>
                                <td>
                                    <input type="text" name="HEADER[PEMBERITAHU]" id="PEMBERITAHU" value="<?= $HEADER['PEMBERITAHU'];?>" class="vltext"/>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
        <fieldset>
            <legend>Data Sarana Angkut</legend>
            <table style="width: 100%">
                <tr>
                    <td style="width: 50%;vertical-align: top;">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 125px;vertical-align: top;">Nama/Reg Angkut</td>
                                <td>
                                    <input type="text" name="HEADER[NMMODA]" id="NMMODA" value="<?= $HEADER['NMMODA']; ?>" url="<?= site_url('referensi/autoComplate/MODAMANIFEST/NMMODA'); ?>" urai="REGMODA;NMMODA;FLAGMODA;URFLAGMODA;GT;LOA;DRAFTDEPAN;DRAFTBELAKANG" onfocus="Autocomp(this.id,$('#MODA').val());" class="ltext"/>
                                    <input type="text" name="HEADER[REGMODA]" id="REGMODA" value="<?= $HEADER['REGMODA']; ?>" url="<?= site_url('referensi/autoComplate/MODAMANIFEST/REGMODA'); ?>" urai="REGMODA;REGMODA;FLAGMODA;URFLAGMODA" onfocus="Autocomp(this.id,$('#MODA').val());" class="ssstext"/>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">Bendera</td>
                                <td>
                                    <input type="text" name="HEADER[FLAGMODA]" id="FLAGMODA" value="<?= $HEADER['FLAGMODA']; ?>" class="sssstext" onblur="checkCode(this);" grp="negara" urai="URFLAGMODA"/> 
                                    <button onclick="tb_search('negara', 'FLAGMODA;URFLAGMODA', 'Kode Negara', this.form.id, 650, 400)" class="btn"> ... </button> <span id="URFLAGMODA"><?= $HEADER['URFLAGMODA']; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">Kade</td>
                                <td>
                                    <input type="text" name="HEADER[KADE]" id="KADE" value="<?= $HEADER['KADE']; ?>" class="ssstext" onblur="checkCode(this);" grp="kade" urai="URKADE" nocomplate="true"/>&nbsp;
                                    <button onclick="tb_search('kade', 'KADE;URKADE', 'Kode Kade', this.form.id, 650, 400)" class="btn"> ... </button>
                                    <span id="URKADE"><?= $HEADER['URKADE']; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">Jumlah Kemasan </td>
                                <td>
                                    <input type="text" name="HEADER[JMLKMS]" id="JMLKMS" value="<?= number_format($HEADER['JMLKMS']); ?>" class="sstext" numeral="yes" format="0,0" style="text-align: right;"/>
                                    <input type="text" name="HEADER[JNSKMS]" id="JNSKMS" value="<?= $HEADER['JNSKMS']; ?>" class="sssstext" onblur="checkCode(this);" grp="kemasan" urai="URJNSKMS"/> 
                                    <button onclick="tb_search('kemasan', 'JNSKMS;URJNSKMS', 'Jenis Kemasan', this.form.id, 650, 400)" class="btn"> ... </button> <span id="URJNSKMS"><?= $HEADER['URJNSKMS']; ?></span>
                                    
                                </td>
                            </tr>
                            <tr id="HandleAgen">
                                <td style="vertical-align: top;">Handling Agent </td>
                                <td>
                                    <input type="text" name="HEADER[AGENHANDLE]" id="AGENHANDLE" value="<?= $HEADER['AGENHANDLE']; ?>" class="ltext"/>
                                </td>
                            </tr>
                            
                        </table>
                    </td>
                    <td style="width: 30%;vertical-align: top;">&nbsp;
                        <table style="width: 100%;" id="segmenLaut">
                            <tr>
                                <td style="width: 125px;vertical-align: top;">Draft Depan</td>
                                <td>
                                  <input type="text" id="DRAFTDEPAN" name="HEADER[DRAFTDEPAN]" value="<?= number_format($HEADER['DRAFTDEPAN'], 2); ?>" class="sstext" numeral="yes" format="0,0.00" style="text-align:right;"/>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">Draft Belakang</td>
                                <td>
                                    <input type="text" name="HEADER[DRAFTBELAKANG]" id="DRAFTBELAKANG" value="<?= number_format($HEADER['DRAFTBELAKANG'], 2); ?>" class="sstext" numeral="yes" format="0,0.00" style="text-align: right;"/>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">GT</td>
                                <td>
                                    <input type="text" name="HEADER[GT]" id="GT" value="<?= number_format($HEADER['GT'], 2); ?>" class="sstext" numeral="yes" format="0,0.00" style="text-align: right;"/> Tons
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">LOA</td>
                                <td>
                                    <input type="text" name="HEADER[LOA]" id="LOA" value="<?= number_format($HEADER['LOA'], 2); ?>" class="sstext" numeral="yes" format="0,0.00" style="text-align: right;"/> Meter 
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 20%;vertical-align: top;">
                        <table style="width: 100px">
                            <tr><td style="text-align: center;"> Memo </td></tr>
                            <tr>
                                <td><textarea name="HEADER[MEMO]" id="MEMO" cols="20" rows="3"><?= $HEADER['MEMO'] ?></textarea></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </fieldset>
        <fieldset>
            <legend>Pelabuhan</legend>
            <table style="width: 100%">
                <tr>
                    <td style="width: 50%">
                        <table width="100%">
                            <tr>
                                <td width="125px">Asal / Muat</td>
                                <td>
                                    <input type="text" name="HEADER[PELMUAT]" id="PELMUAT" value="<?= $HEADER['PELMUAT']; ?>" onblur="checkCode(this);" grp="pelabuhan" urai="URPELMUAT" class="ssstext"/>
                                    <button onclick="tb_search('pelabuhan', 'PELMUAT;URPELMUAT', 'Kode Pelabuhan Muat', this.form.id , 650, 400)" class="btn"> ... </button>
                                    <span id="URPELMUAT"><?= $HEADER['URPELMUAT']; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td width="125px">Transit / Akhir</td>
                                <td>
                                    <input type="text" name="HEADER[PELTRANSIT]" id="PELTRANSIT" value="<?= $HEADER['PELTRANSIT']; ?>" onblur="checkCode(this);" grp="pelabuhan" urai="URPELTRANSIT" class="ssstext"/> 
                                    <button onclick="tb_search('pelabuhan', 'PELTRANSIT;URPELTRANSIT', 'Kode Pelabuhan Transit', this.form.id, 650, 400)" class="btn"> ... </button>
                                    <span id="URPELTRANSIT"><?= $HEADER['URPELTRANSIT']; ?></span>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 50%">
                        <table width="100%">
                            <tr>
                                <td width="125px">Bongkar</td>
                                <td>
                                    <input type="text" name="HEADER[PELBONGKAR]" id="PELBONGKAR" value="<?= $HEADER['PELBONGKAR']; ?>" onblur="checkCode(this);" grp="pelabuhan" urai="URPELBONGKAR" class="ssstext"/>
                                    <button onclick="tb_search('pelabuhan', 'PELBONGKAR;URPELBONGKAR', 'Kode Pelabuhan Bongkar', this.form.id , 650, 400)" class="btn" > ... </button>
                                    <span id="URPELBONGKAR"><?= $HEADER['URPELBONGKAR']; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td width="125px">Selanjutnya</td>
                                <td>
                                    <input type="text" name="HEADER[PELBERIKUT]" id="PELBERIKUT" value="<?= $HEADER['PELBERIKUT']; ?>" onblur="checkCode(this);" grp="pelabuhan" urai="URPELBERIKUT" class="ssstext"/>
                                    <button onclick="tb_search('pelabuhan', 'PELBERIKUT;URPELBERIKUT', 'Kode Pelabuhan Selanjutnya', this.form.id , 650, 400)" class="btn"> ... </button>
                                    <span id="URPELBERIKUT"><?= $HEADER['URPELBERIKUT']; ?></span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </fieldset>
        <table style=" width:100%">
            <tr>
                <td style=" width: 50%">
                    <fieldset>
                        <legend>Jadwal Kedatangan</legend>
                        <table width="100%">
                            <tr>
                                <td width="125px" id="NOETA">Nomor Voy </td>
                                <td>
                                    <input type="text" name="HEADER[ETANOVOY]" id="ETANOVOY" value="<?= $HEADER['ETANOVOY']; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Tanggal </td>
                                <td>
                                    <input type="text" name="HEADER[ETATGVOY]" class="sstext" id="ETATGVOY" value="<?= $HEADER['ETATGVOY']; ?>" maxlength="10" formatDate="yes"/>
                                    &nbsp;&nbsp;&nbsp; Jam &nbsp;
                                    <input type="text" name="HEADER[ETAWKVOY]" class="ssstext" id="ETAWKVOY" value="<?= $HEADER['ETAWKVOY']; ?>" onblur="formatTime(this.id)"/>
                                </td>
                            </tr>                           
                        </table>
                    </fieldset>
                </td>
                <td style=" width: 50%">
                    <fieldset>
                        <legend>Jadwal Keberangkatan</legend>
                        <table width="100%">
                            <tr>
                                <td width="125px" id="NOETD">Nomor Voy </td>
                                <td>
                                    <input type="text" name="HEADER[ETDNOVOY]" id="ETDNOVOY" value="<?= $HEADER['ETDNOVOY']; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Tanggal </td>
                                <td>                                            
                                    <input type="text" name="HEADER[ETDTGVOY]" id="ETDTGVOY" class="sstext" value="<?= $HEADER['ETDTGVOY']; ?>" maxlength="10" formatDate="yes"/>
                                    &nbsp;&nbsp;&nbsp; Jam &nbsp;
                                    <input type="text" name="HEADER[ETDWKVOY]" id="ETDWKVOY" class="ssstext" value="<?= $HEADER['ETDWKVOY']; ?>" onblur="formatTime(this.id)"/>
                                </td>
                            </tr>                               
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </form>
</span>
<script>
    $(document).ready(function() {
        onoff(false);
        declareDP('frmHeader');
        getModa($('#MODA').val());
        f_MAXLENGTH('frmHeader');
        formatNPWP('frmHeader #NOIDAGEN');
        formatNumeral('frmHeader');
    });
    function onoff(on) {
        if($('#frmHeader #STATUS').val()=='00' || $('#frmHeader #STATUS').val()=='01' || $('#frmHeader #STATUS').val()=='05' || $('#frmHeader #STATUS').val()==''){
            if (on) {
                $('#on').show();
                $('#off').hide();
                $("#frmHeader :input").removeAttr("disabled");
            } else {
                $('#on').hide();
                $('#off').show();
                $("#frmHeader :input").attr('disabled', 'disabled');
            }
            $('.onterus').removeAttr('disabled');
            $('.offterus :input').attr('readonly', 'readonly');
        }else{
            $('#on').hide();
            $('#off').show();
            $("#frmHeader :input").attr('disabled','disabled');
            $('#frmHeader div').addClass('disabled');
        }
        if($('#frmHeader #STATUS').val()=='01' || $('#frmHeader #STATUS').val()=='02'){
            $('#frmHeader #btnQUEUED').show();
            $('#frmHeader #btnQUEUED').removeAttr("disabled");
        }else{
            $('#frmHeader #btnQUEUED').hide();
        }
    }

    function getModa(val) {
        if(val == '1'){
            $('#segmenLaut').show();
            $('#HandleAgen').hide();
            $('#NOETA').html('No Voy');
            $('#NOETD').html('No Voy');
        }else if(val == '4'){
            $('#segmenLaut').hide();
            $('#HandleAgen').show();
            $('#NOETA').html('No Flight');
            $('#NOETD').html('No Flight');
        }else{
            $('#segmenLaut').hide();
            $('#HandleAgen').hide();
            $('#NOETA').html('No Kedatangan');
            $('#NOETD').html('No Kedatangan');
        }
        
    }

    function cekValidasi(car) {
        //alert(car);return false;
        $(function() {
            $('#msgbox').html('');
            $("#msgbox").html('<div style="margin-left: 15px; margin-top: 15px;"><img src="' + base_url + 'img/loaders/facebook.gif" /> loading...</div>');
            $("#msgbox").dialog({
                resizable: false,
                height: 500,
                modal: true,
                width: 700,
                title: 'Validasi Dokumen',
                open: function() {
                    $.ajax({
                        type: 'POST',
                        url: site_url + '/bc10/validasiRKSP/' + car,
                        success: function(data) {
                            $("#msgbox").html(data);
                            call(site_url + '/bc10/create/' + car, '_content_');
                        }
                    });
                },
                buttons: {
                    Close: function() {
                        $(this).dialog("close");
                    }
                }
            });
        });
    }

    function gotoQUEUED() {
        jConfirm('Data akan dibentuk EDIFACT ?', site_name,
                function(r) {
                    if (r == true) {
                        jloadings();
                        $(function() {
                            $('#msgbox').html('');
                            $("#msgbox").html('<div style="margin-left: 15px; margin-top: 15px;"><img src="' + base_url + 'img/loaders/facebook.gif" /> loading...</div>');
                            $("#msgbox").dialog({
                                resizable: false,
                                height: 500,
                                modal: true,
                                width: 700,
                                title: 'Edifact / unEdifact',
                                open: function() {
                                    $.ajax({
                                        type: 'POST',
                                        url: site_url + '/bc10/queued',
                                        data: '&CAR=' + $('#CAR').val(),
                                        error: function() {
                                            Clearjloadings();
                                        },
                                        success: function(data) {
                                            Clearjloadings();
                                            $("#msgbox").html(data);
                                            call(site_url + '/bc10/create/' + $('#CAR').val(), '_content_');
                                        }
                                    });
                                },
                                buttons: {
                                    Close: function() {
                                        $(this).dialog("close");
                                    }
                                }
                            });
                        });
                    } else {
                        return false;
                    }
                });
    }

    function save_header(formid) {
        var dataSend = $('#' + formid).serialize();
        jConfirm('Anda yakin Akan memproses data ini?', site_name,
            function(r) {
                if (r == true) {
                    jloadings();
                    $.ajax({
                        type: 'POST',
                        url: $('#' + formid).attr('action'),
                        data: dataSend,
                        success: function(data) {
                            var arr = data.split('|');
                            $("#msgbox").dialog({
                                resizable: false,
                                height: 500,
                                modal: true,
                                width: 700,
                                title: 'Bentuk / Batal Edifact',
                                open: function() {
                                    call(site_url + '/bc10/create/' + arr[1], '_content_');
                                    $(this).html(arr[0]);
                                },
                                buttons: {
                                    Close: function() {
                                        $(this).dialog("close");
                                    }
                                }
                            });
                            
                            Clearjloadings();
                        }
                    });
                } else {
                    return false;
                }
            });
    }
</script>