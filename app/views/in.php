<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    error_reporting(E_ERROR);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>.: Trade to Goverment :.</title>
        <script>
            var base_url = '<?= base_url(); ?>';
            var site_url = '<?= site_url(); ?>';
            var site_name = '.: Trade to Goverment :.';
        </script>
        <meta charset="utf-8" />
	<meta name="viewport" content="width=device-width,initial-scale=1" />
        <meta name="description" content="trade 2 goverment"/>
        <meta name="author" content="muklis"/>
        <link rel="shortcut icon" href="img/logo.ico" type="image/x-icon" >
        <link rel="stylesheet" type="text/css" href="css/all.css" />
        <link rel="stylesheet" type="text/css" href="css/newtable.css"/>
        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <script type="text/javascript" src="js/newtable.js"></script>
        <script type="text/javascript" src="js/all.js"></script>
        <script type="text/javascript" src="js/main.js"></script>
        <script type="text/javascript" src="js/numeral.min.js"></script>
    </head>
    <body>
        <div id="wrapper">
            <div id="header" style="background: #F7F7F7;" >
                <img src="img/logo.png" style="float: left; padding: 5px 5px 5px 40px; height: 65px" >
                <div id="topNav">
                    <ul>
                        <li><b>SELAMAT DATANG </b> <a href="#menuProfile" class="menu">{_welcome_}</a>
                            <div id="menuProfile" class="menu-container menu-dropdown">{_menuProfile_}</div>
                        </li>
                        <li><a href="#" onClick="window.open('<?= site_url('login/logout');?>','_self')">Log Out</a></li>
                    </ul>
                </div>
            </div>
            <div id="sidebar">{_menu_}</div>
            <div id="content">		
                <div id="contentHeader">
                    <h1>Grid Layout</h1>
                </div>
                <div class="container">
                    <div class="grid-24">
                        <div class="widget">
                            <div id="msgConfirm" class="notify" style="position:fixed;bottom:10px;right:10px;float:left; display: none; z-index: 100;" ></div>
                            <div class="widget-content" id="_content_">{_content_}</div>
                        </div>					
                    </div>
                </div>
            </div>
        </div>
        <div id="footer">Copyright &copy; 2014, EDI-Indonesia.</div>  
    </body>
<div id="msgbox"></div>
<div id="msgbox2"></div>
</html>
<script>
    $(document).ready(function(){
        var hgt = $(window).height() - 141;
        $('#content').css('min-height',hgt+'px');
        //autoRespon();
    });
</script>