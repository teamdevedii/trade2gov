<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
error_reporting(E_ERROR);
?>
<div class="content_luar" style="margin-bottom:2px;">
<div class="content_dalam">
<h4><span class="info_">&nbsp;</span>Ubah Password</h4>
<form id="fpass" action="<?= "updatePass" ?>" method="post" autocomplete="off">
<table width="100%" border="0">
    <tr>
    	<td width="14%">Password Lama *</td>
        <td width="86%" colspan="2">: <input type="password" name="oldPass" id="oldPass" class="text" wajib="yes"/></td>
        
    </tr>
    <tr>
    	<td>Password Baru *</td>
        <td colspan="2">: <input type="password" name="newPass" id="newPass" class="text" wajib="yes"/></td>
       
    </tr>
    <tr>
    	<td>Ulang Password Baru *</td>
        <td colspan="2">: <input type="password" name="renewPass" id="renewPass" class="text" wajib="yes"/></td>
        
    </tr>
    <tr>
    	<td>&nbsp;</td>
        <td>&nbsp;
        <span id="divImgCapt"><?php echo $cap; ?></span> &nbsp; <a href="javascript:void(0)" onclick="reload_captcha()">[ reload ]</a>
        </td>
        
    </tr>
    <tr>
    	<td>Key Code *</td>
        <td colspan="2">: <input type="text" name="kode" id="kode" class="text" wajib="yes"/></td>
        
    </tr>
</table>    
<table width="100%" border="0" >
	<tr>
    	<td colspan="2">&nbsp;</td>
    </tr>
    <tr>
        <td width="15%">
        <a href="javascript:void(0);" class="button save" id="ok_" onclick="save_header('#fpass');"><span><span class="icon"></span>&nbsp;Submit&nbsp;</span></a>&nbsp; &nbsp;<a href="javascript:void(0);" class="button cancel" id="ok_" onclick="cancel('fpass');"><span><span class="icon"></span>&nbsp;Reset&nbsp;</span></a>&nbsp;</td>
        <td  width="85%"  align="left" ><div class="msgheader_">&nbsp;</div></td>
    </tr>
</table>
</form>
</div>
</div>

<script>
function reload_captcha()
{
	var url = site_url + '/user/reload_captcha/'+Math.random(); 	
		$.post(url, {},function(data){
			$('#divImgCapt').html(data);
	});	
}
</script>