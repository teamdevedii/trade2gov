<?php
if (!defined('BASEPATH')){
    exit('No direct script access allowed');
}
if ($act == "save") {
    $disable = "";
    $readonly = "";
    $addUrl = "add/user";
} else {
    $disable = "readonly=readonly";
    $readonly = "readonly=readonly";
    $addUrl = "editUser/user";
}
?>	
<div class="content_luar">
    <div class="content_dalam">
        <a href="javascript:void(0)" 
           onclick="window.location.href = '<?= site_url() . "/user/userlist/draftUser" ?>'" style="float:right;margin:-5px 0px 0px 0px" class="button retry" id="ok_"><span><span class="icon"></span>&nbsp;Selesai&nbsp;</span></a>
        <form name="fuser" id="fuser" action="<?= site_url() . "/user/" . $addUrl; ?>" method="post" autocomplete="off">
            <input type="hidden" name="act" id="act" value="<?= $act; ?>" />
            <input type="hidden" name="idUser" id="idUser" value="<?= $sess['USER_ID']; ?>" />
            <h4><span class="info_2">&nbsp;</span><?= $judul; ?></h4>
            <span id="divtmp"></span>
            <table width="100%" border="0">
                <tr><td width="41%">

                        <table width="100%" border="0">
                            <tr>
                                <td width="26%">Nama </td>
                                <td width="73%">: <input type="text" name="USER[NAMA]" id="NAMA" value="<?= $sess['NAMA'] ?>"  class="mtext"></td>
                                <td width="1%">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td>: <textarea name="USER[ALAMAT]" id="ALAMAT" class="mtext"><?= $sess['ALAMAT']; ?></textarea></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Nomor Telepon</td>
                                <td>: <input type="text" name="USER[TELEPON]" id="TELEPON" value="<?= $sess['TELEPON']; ?>" class="mtext"   /></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>: <input type="text" name="USER[EMAIL]" id="EMAIL" value="<?= $sess['EMAIL']; ?>" class="mtext"   /></td>
                                <td>&nbsp;</td>
                            </tr> 
                            <tr>
                                <td>Jabatan</td>
                                <td>: <input type="text" name="USER[JABATAN]" id="JABATAN" value="<?= $sess['JABATAN'] ?>"  class="mtext"></td>
                                <td>&nbsp;</td>
                            </tr> 
                        </table>

                    </td><td width="59%" valign="top">
                        <table width="100%" border="0">
                            <tr>
                                <td width="22%" >Username</td>
                                <td width="78%">: <input type="text" name="USER[USERNAME]" id="USERNAME" value="<?= $sess['USERNAME']; ?>" class="mtext" wajib="yes"   <?= $readonly ?>/></td>
                            </tr>
                            <tr>
                                <td >Password</td>
                                <td>: <input type="password" name="USER[PASSWORD]" id="PASSWORD" value="<?= $sess['PASSWORD']; ?>" class="mtext" wajib="yes"  <?= $readonly ?>/></td>
                            </tr>
                            <tr>
                                <td>Konfirmasi Password</td>
                                <td>: <input type="password" name="KONFPASSWORD" id="KONFPASSWORD" value="<?= $sess['PASSWORD']; ?>" class="mtext" wajib="yes"  <?= $readonly ?>/></td>
                            </tr>
                            <tr>
                                <td>Group User</td>
                                <td>: <?= form_dropdown('USER_ROLE[KODE_ROLE]', $kode_role, $sess['KODE_ROLE'], ' class="mtext" '); ?></td>
                            </tr>
                            <tr>
                                <td>Status</td>
                                <td>: <?= form_dropdown('USER[STATUS_USER]', $status_user, $sess['STATUS_USER'], ' class="mtext" '); ?></td>
                            </tr> 
                        </table>
                    </td>

                    <? if($act!="priview"){?>
                <tr>
                    <td colspan="6">&nbsp;</td> 
                </tr>    
                <tr>
                    <td colspan="6" >
                        <a href="javascript:void(0);" class="button save" id="ok_" onclick="save_header('#fuser');"><span><span class="icon"></span>&nbsp;<?= $act; ?>&nbsp;</span></a>&nbsp;<a href="javascript:;" class="button cancel" id="cancel_" onclick="cancel('fuser');"><span><span class="icon"></span>&nbsp;reset&nbsp;</span></a><span class="msgheader_" style="margin-left:20px">&nbsp;</span>
                    </td>
                </tr>  
                <? }?>
            </table>

        </form>
    </div>
</div>
