<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);
?>
<?php
if ($act == "Ubah") {
    $disable = "disabled=disabled";
} else {
    $disable = "";
    $url = "../edit";
}
?>
<div class="content_luar" style="margin-bottom:2px;">
    <div class="content_dalam">
        <h4><span class="info_">&nbsp;</span>Profil User</h4>
        <form id="fprofil_" action="<?= $url; ?>" method="post" autocomplete="off">
            <table width="100%" border="0">
                <tr><td width="40%" valign="top">
                        <table width="100%" border="0">
                            <tr>
                                <td width="27%">Nama</td>
                                <td width="73%">:  <input type="text" name="PROFIL[NAMA]" id="NAMA" class="text" value="<?= $sess['NAMA'] ?>" <?= $disable; ?>/></td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td>: <input type="text" name="PROFIL[ALAMAT]" id="ALAMAT" class="text" value="<?= $sess['ALAMAT'] ?>" <?= $disable; ?>/></td></tr>
                            <tr>
                                <td>Nomor Telepon</td>
                                <td>: <input type="text" name="PROFIL[TELEPON]" id="TELEPON" class="text" value="<?= $sess['TELEPON'] ?>" <?= $disable; ?>/></td>		
                            <tr>
                                <td>Email</td>
                                <td>: <input type="text" name="PROFIL[EMAIL]" id="EMAIL" class="text" value="<?= $sess['EMAIL'] ?>" <?= $disable; ?>/></td>
                            </tr>
                        </table>   
                    <td width="60%" valign="top">
                        <table width="100%" border="0">
                            <tr>
                                <td width="12%">Jabatan</td>
                                <td width="88%">: <input type="text" name="PROFIL[JABATAN]" id="JABATAN" class="text" value="<?= $sess['JABATAN']; ?>" disabled="disabled"/></td>
                            </tr>
                            <tr>
                                <td>User ID</td>
                                <td>: <input type="text" name="PROFIL[USERNAME]" id="USERNAME" class="text" value="<?= $sess['USERNAME']; ?>" disabled="disabled"/></td>
                            </tr>
                            <tr>
                                <td>User Role</td>
                                <td>: <?= form_dropdown('PROFIL1[KODE_ROLE]', $kode_role, $sess['KODE_ROLE'], ' class="text" disabled '); ?></td>
                            </tr>
                            <tr>
                                <td>Tipe Pengguna</td>
                                <td>: <?= form_dropdown('PROFIL[TIPE_TRADER]', $tipe_trader, $sess['TIPE_TRADER'], 'id="tipe_trader" class="sstext" disabled'); ?></td>
                            </tr>
<?php if ($sess['TIPE_TRADER'] == '2') { ?>
                                <tr>
                                    <td>No/Tgl PPJK</td>
                                    <td>: <input type="text" name="PROFIL[NO_PPJK]" id="NO_PPJK" class="ltext" style="width:130px;" value="<?= $sess['NO_PPJK'] ?>" disabled/>&nbsp;
                                        <input type="text" name="PROFIL[TANGGAL_PPJK]" id="TANGGAL_PPJK" class="stext date" value="<?= $sess['TANGGAL_PPJK'] ?>" disabled /></td>
                                </tr>
<?php } ?>
                        </table>
                    </td></tr></table>    

            <table width="100%" border="0">
                <tr>
                    <td  colspan="8">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="8">
                        <a href="javascript:void(0);" class="button save" id="ok_" onclick="rubah('#fprofil_');"><span><span class="icon"></span>&nbsp;<?= $act; ?>&nbsp;</span></a>&nbsp; &nbsp; <?php if ($act != "Ubah") { ?><a href="javascript:void(0);" class="button cancel" id="ok_" onclick="window.location.href = '<?php echo site_url('user/profil/lihat'); ?>'"><span><span class="icon"></span>&nbsp;Batal&nbsp;</span></a><?php } ?>&nbsp;&nbsp;&nbsp;<span class="msgheader_" align="left">&nbsp;</span></td>
                </tr>
            </table>
        </form>
    </div>
</div>

<script>
    function rubah(formid) {
<?php if ($act == "Ubah") { ?>
            window.location = site_url + "/user/profil/ubah";
<?php } else { ?>
            save_header(formid)            
<?php } ?>
    }
</script>