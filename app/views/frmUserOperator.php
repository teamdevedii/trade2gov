<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(E_ERROR);
?>
<div class="widget" style="width: 1080px; margin: 10px 0px 10px 0px;">    
    <div class="widget-content">
        <form id="frmUserOperator" name="frmUserOperator" action="<?= site_url('usermanage/mngUserOperator'); ?>" onsubmit="return false;" class="form uniformForm" method="post" autocomplete=off>
            <?= $MAXLENGTH ?>            
            <input type="hidden" name="act" id="act" value="<?= $act; ?>" readonly="readonly" />
            <input type="hidden" name="DATA[USER_ID]" id="USER_ID" value="<?= $DATA['USER_ID']; ?>" />      
            <table width="100%" border="0"> 
                <tr>
                    <td width="50%" valign="top">
                        <table width="100%" border="0">
                            <tr>
                                <td colspan="2"><h5><span class="ui-icon ui-icon-carat-1-e" style="display: inline-block ; margin-left:-2px">&nbsp;</span>DATA USER OPERATOR</h5></td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">e-Mail *</td>
                                <td>: <input type="text" name="DATA[USERNAME]" id="USERNAME" class="ltext" value="<?= $DATA['USERNAME']; ?>" wajib="yes"/></td>
                            </tr>
                            <tr>
                                <td>Nama </td>
                                <td>: <input type="text" name="DATA[NAMA]" id="NAMA" class="ltext" value="<?= $DATA['NAMA']; ?>" wajib="yes"/></td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">Alamat </td>
                                <td><font style="vertical-align:top"> :</font> <textarea name="DATA[ALAMAT]" id="ALAMAT" class="ltext" wajib="yes"><?= $DATA['ALAMAT'] ?></textarea></td>
                            </tr>
                            <tr>
                                <td>Nomor Telepon </td>
                                <td>: <input type="text" name="DATA[TELEPON]" id="TELEPON" class="smtext" value="<?= $DATA['TELEPON']; ?>" wajib="yes"/></td>
                            </tr>
                            <tr>
                                <td>Jabatan </td>
                                <td>: <input type="text" name="DATA[JABATAN]" id="JABATAN" class="smtext" value="<?= $DATA['JABATAN']; ?>" wajib="yes"/></td>
                            </tr>                                                     
                            <tr>
                                <td colspan="2" align="center">                      
                                    <button class="btn btn-small" onclick="save_post_msg('frmUserOperator','','_content_');"> &nbsp; Simpan &nbsp; </button>
                                    <button class="btn btn-small" onclick="document.getElementById('frmUserOperator').reset();"> &nbsp; Reset  &nbsp; </button>
                                </td>
                            </tr>                                 
                        </table>
                    </td>
                </tr>
        </form>
    </div>
</div>
