<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
?>
<div class="content_luar">
    <div class="content_dalam" >
        <center>
            
        <table border="0" width="620px" cellpadding="0" cellspacing="0" class="table-bordered">
            <thead>
            <tr>
                <td width="21%" align="center" style="height: 30px;"> Jenis Pungutan </td>
                <td width="20%" align="center">Dibayar (Rp) </td>
                <td width="22%" align="center">Ditangguhkan Pemr. (Rp) </td>
                <td width="18%" align="center">Ditangguhkan (Rp)</td>
                <td width="19%" align="center">Dibebaskan (Rp)</td>
            </tr>
            </thead>
            <tr>
                <td style="padding-left:10px">1. BM</td>
                <td align="right"><?= number_format($data_pgt[1][0]) ?>&nbsp;</td>
                <td align="right"><?= number_format($data_pgt[1][1]) ?>&nbsp;</td>
                <td align="right"><?= number_format($data_pgt[1][2]) ?>&nbsp;</td>
                <td align="right"><?= number_format($data_pgt[1][4]) ?>&nbsp;</td>
            </tr>
            <tr>
                <td style="padding-left:10px">2. Cukai Tembakau</td>
                <td align="right" ><?= number_format($data_pgt[5][0]) ?>&nbsp;</td>
                <td align="right" ><?= number_format($data_pgt[5][1]) ?>&nbsp;</td>
                <td align="right" ><?= number_format($data_pgt[5][2]) ?>&nbsp;</td>
                <td align="right" ><?= number_format($data_pgt[5][4]) ?>&nbsp;</td>
            </tr>
            <tr>
                <td style="padding-left:10px">&nbsp;&nbsp;&nbsp;&nbsp;Cukai EA</td>
                <td align="right" ><?= number_format($data_pgt[6][0]) ?>&nbsp;</td>
                <td align="right" ><?= number_format($data_pgt[6][1]) ?>&nbsp;</td>
                <td align="right" ><?= number_format($data_pgt[6][2]) ?>&nbsp;</td>
                <td align="right" ><?= number_format($data_pgt[6][4]) ?>&nbsp;</td>
            </tr>
            <tr>
                <td style="padding-left:10px">&nbsp;&nbsp;&nbsp;&nbsp;Cukai Minuman</td>
                <td align="right" ><?= number_format($data_pgt[7][0]) ?>&nbsp;</td>
                <td align="right" ><?= number_format($data_pgt[7][1]) ?>&nbsp;</td>
                <td align="right" ><?= number_format($data_pgt[7][2]) ?>&nbsp;</td>
                <td align="right" ><?= number_format($data_pgt[7][4]) ?>&nbsp;</td>
            </tr>
            <tr>
                <td style="padding-left:10px;" class="border-brl">3. PPN</td>
                <td align="right"><?= number_format($data_pgt[2][0]) ?>&nbsp;</td>
                <td align="right"><?= number_format($data_pgt[2][1]) ?>&nbsp;</td>
                <td align="right"><?= number_format($data_pgt[2][2]) ?>&nbsp;</td>
                <td align="right"><?= number_format($data_pgt[2][4]) ?>&nbsp;</td>
            </tr>
            <tr>

                <td style="padding-left:10px;" class="border-brl">4. PPnBM</td>
                <td align="right"><?= number_format($data_pgt[3][0]) ?>&nbsp;</td>
                <td align="right"><?= number_format($data_pgt[3][1]) ?>&nbsp;</td>
                <td align="right"><?= number_format($data_pgt[3][2]) ?>&nbsp;</td>
                <td align="right"><?= number_format($data_pgt[3][4]) ?>&nbsp;</td>
            </tr>
            <tr>
                <td style="padding-left:10px;" class="border-brl">5. PPh</td>
                <td align="right"><?= number_format($data_pgt[4][0]) ?>&nbsp;</td>
                <td align="right"><?= number_format($data_pgt[4][1]) ?>&nbsp;</td>
                <td align="right"><?= number_format($data_pgt[4][2]) ?>&nbsp;</td>
                <td align="right"><?= number_format($data_pgt[4][4]) ?>&nbsp;</td>
            </tr>
            <tr>
                <td style="padding-left:10px;" class="border-brl"><b>TOTAL</b></td>
                <td align="right"><b><?= number_format($data_pgt['TOTAL'][0]) ?>&nbsp;</b></td>
                <td align="right"><b><?= number_format($data_pgt['TOTAL'][1]) ?>&nbsp;</b></td>
                <td align="right"><b><?= number_format($data_pgt['TOTAL'][2]) ?>&nbsp;</b></td>
                <td align="right"><b><?= number_format($data_pgt['TOTAL'][4]) ?>&nbsp;</b></td>
            </tr>
        </table>
        </center>
    </div>
</div>
