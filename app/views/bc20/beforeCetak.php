<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
?>
<div class="content_luar">
    <div class="content_dalam">
        <form id="cetak_" method="post" autocomplete="off" onsubmit="return false;">
            <input id="CAR" type="hidden" value="<?= $car ?>">
            <fieldset>
                <legend> Pilihan Pencetakan </legend>
                <table width="100%">
                    <tr>
                        <td>
                            <input type="radio" name="CETAK" id="cetakPIB" value="1" checked > <label for="cetakPIB">&nbsp; Dokumen PIB</label><br>
                            <input type="radio" name="CETAK" id="cetakSSPCP" value="2"> <label for="cetakSSPCP">&nbsp; Dokumen SSPCP</label><br>
                            <input type="radio" name="CETAK" id="cetakTandaTerima" value="3"> <label for="cetakTandaTerima">&nbsp; Tanda Terima</label><br>
                            <input type="radio" name="CETAK" id="cetakRespon" value="4"> <label for="cetakRespon">&nbsp; Respon</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <button onclick="cetakNewWin($('.CETAK').val());" class="btn"><span class="icon-new-window"></span> Cetak </button>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
</div>
<script>
    $('#msgboxSSPCPx').html('');
    function cetakNewWin(id){
        if($('input[name="CETAK"]:checked').val()=='2'){
            //cetak($car,$jnPage,$frmSSPCP=0)
            Dialog(site_url+'/bc20/cetak/'+$('#CAR').val()+'/2/1', 'msgbox2', ' DATA SSPCP ', 650, 600);
        }else if($('input[name="CETAK"]:checked').val()=='4'){
            Dialog(site_url+'/bc20/beforecetakrespon/'+$('#CAR').val(), 'beforeCetakRespon', ' CETAK RESPON PIB ', 800, 450);
        }else{
            window.open(site_url+"/bc20/cetak/"+$('#CAR').val()+'/'+$('input[name="CETAK"]:checked').val(),"Cetak Dokumen PIB","scrollbars=yes, resizable=yes,width=1100,height=700");
        }
    }
</script>