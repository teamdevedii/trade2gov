<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
?>

<div class="content_luar">
    <div class="content_dalam">
        <form id="fpph_" method="post" autocomplete="off" onsubmit="return false;">
            Pilihan tarif PPh Pasal 22 untuk menyesuaikan tarif berdasarkan PMK Nomor 175/PMK.011/2013<br><br>
            <fieldset>
                <legend>Data Pelabuhan</legend>
                <center>
                Tarif PPh : 
                <select id="TarifPPh">
                    <option value="0.50">0.50</option>
                    <option value="2.50">2.50</option>
                    <option value="7.50">7.50</option>
                </select>
                </center>
            </fieldset>
            
            <button onclick="AddPPh();" class="btn"> OK </button> &nbsp;
            <button onclick="closedialog('shadow');" class="btn onterus"> Cancel </button>
        </form>
    </div>
</div>
<script>
    $(document).ready(function() {
        $("#fpph_ #TarifPPh").val($("#fbarang_ #TRPPPH").val());
    });
    function AddPPh() {
        $("#fbarang_ #TRPPPH").val($("#fpph_ #TarifPPh").val());
        closedialog('shadow');
    }
</script>