<?php
if (!defined('BASEPATH')){
    exit('No direct script access allowed');
}
$arr = explode(";", $data);
foreach ($arr as $isi) {
    $arrX = explode("|", $isi);
    $val[$arrX[0]] = $arrX[1];
}
$val['INDNPWP'] = str_replace('.', '', str_replace('-', '', $val['INDNPWP']));
?>
<div class="content_luar">
    <div class="content_dalam">
        <form method="post" name="indentor" id="indentor" onsubmit="return false;"  >
            <table width="100%">
                <tr>
                    <td width="100px">Jenis Identitas</td>
                    <td>
                        <?= form_dropdown('HEADER[INDID]', $INDID, $val['INDID'], 'wajib="yes" id="INDID" class="mtext"'); ?>
                        <input type="text" name="HEADER[INDNPWP]" id="INDNPWP" value="<?= $this->fungsi->FORMATNPWP($val['INDNPWP']); ?>" class="smtext" wajib="yes" url="<?= site_url('referensi/autoComplate/INDENTOR/INDNPWP'); ?>" urai="indentor #INDNPWP;indentor #INDNAMA;indentor #INDALMT;indentor #INDID" onfocus="Autocomp('indentor #'+this.id); unformatNPWP('indentor #'+this.id);" onblur="formatNPWP('indentor #'+this.id);"/>
                        <button onclick="QQ();" class="btn" style="width: 30px; padding-left:10px"><span class="icon-comment-stroke"></span></button>
                    </td>
                </tr>
                <tr>
                    <td>Nama</td>
                    <td>
                        <input type="text" name="HEADER[INDNAMA]" id="INDNAMA" value="<?= $val['INDNAMA']; ?>" class="ltext" wajib="yes" url="<?= site_url('referensi/autoComplate/INDENTOR/INDNAMA'); ?>" urai="INDNPWP;INDNAMA;INDALMT;INDID" onfocus="Autocomp(this.id);"/>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: top">Alamat</td>
                    <td>
                        <textarea name="HEADER[INDALMT]" id="INDALMT" class="ltext" wajib="yes" onkeyup="limitChars(this.id, 70, 'limitINDALMT')"><?= $val['INDALMT']; ?></textarea>
                        <div id="limitINDALMT"></div>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: top" colspan="2">
                        <button onclick="getIndentor();" class="btn" > Simpan </button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
