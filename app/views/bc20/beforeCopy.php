<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
?>
<div class="content_luar">
    <div class="content_dalam">
        <form id="cetak_" method="post" autocomplete="off" onsubmit="return false;">
            <input id="CAR" type="hidden" value="<?= $car ?>">
            <fieldset>
                <legend> Pilihan Hasil Kopian </legend>
                <table width="100%">
                    <tr>
                        <td>
                            <input type="radio" name="COPY" id="copyPIBLengkap" value="1" checked > <label for="copyPIBLengkap">&nbsp;Copy PIB Lengkap.</label><br>
                            <input type="radio" name="COPY" id="copyPIBBeda" value="2"> <label for="copyPIBBeda">&nbsp;Kosongkan data yang pasti berbeda.</label><br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br>
                            <button onclick="cetakNewWin('1');" class="btn"><span class="icon-document-alt-stroke"></span> Copy </button>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
</div>
<script>
    $('#msgboxSSPCPx').html('');
    function cetakNewWin(id){
        alert('dalam proses');
    }
</script>