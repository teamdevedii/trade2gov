<?php
if (!defined('BASEPATH')){
    exit('No direct script access allowed');
}
?>
<script>
    $(document).ready(function(){
        $('#tabs_bc20').tabs();
    });
    function EditHarga(inputField) {
        var id = inputField.split(";");
        var data = "";
        for (var a = 0; a < id.length; a++) {
            data += ';' + id[a] + '|' + $('#' + id[a]).val();
        }
        data = data.substr(1);
        Dialog(site_url + '/bc20/hitungan/' + data, 'divHitung', '.: FORM EDIT HARGA :.', 670, 400);
    }
    function detilPungutan(car) {
        Dialog(site_url + '/bc20/pungutanDtl/' + car, 'divHitung', '.: DETIL PUNGUTAN :.', 670, 350);
    }
    function getHarga(form) {
        var KODE_HARGAHarga = ($("#" + form + " #KODE_HARGAHarga").val()) ? $("#" + form + " #KODE_HARGAHarga").val() : '0';
        var HARGA_CIF = ($("#" + form + " #HARGA_CIF").val()) ? $("#" + form + " #HARGA_CIF").val() : '0';
        var KODE_VALUTA = ($("#" + form + " #KODE_VALUTA").val()) ? $("#" + form + " #KODE_VALUTA").val() : '0';
        var NDPBMHarga = ($("#" + form + " #NDPBMHarga").val()) ? $("#" + form + " #NDPBMHarga").val() : '0';
        var NDPBMUR = ($("#" + form + " #NDPBMUR").val()) ? $("#" + form + " #NDPBMUR").val() : '0';
        var FOBHarga = ($("#" + form + " #FOBHarga").val()) ? $("#" + form + " #FOBHarga").val() : '0';
        var FOBURHarga = ($("#" + form + " #FOBURHarga").val()) ? $("#" + form + " #FOBURHarga").val() : '0';
        var CIFHarga = ($("#" + form + " #CIFHarga").val()) ? $("#" + form + " #CIFHarga").val() : '0';
        var CIFRPHarga = ($("#" + form + " #CIFRPHarga").val()) ? $("#" + form + " #CIFRPHarga").val() : '0';
        var CIFURHarga = ($("#" + form + " #CIFURHarga").val()) ? $("#" + form + " #CIFURHarga").val() : '0';
        var NILAI_FREIGHT = ($("#" + form + " #NILAI_FREIGHT").val()) ? $("#" + form + " #NILAI_FREIGHT").val() : '0';
        var NILAI_FREIGHTUR = ($("#" + form + " #NILAI_FREIGHTUR").val()) ? $("#" + form + " #NILAI_FREIGHTUR").val() : '0';
        var BIAYAHarga = ($("#" + form + " #BIAYAHarga").val()) ? $("#" + form + " #BIAYAHarga").val() : '0';
        var DISCOUNTHarga = ($("#" + form + " #DISCOUNTHarga").val()) ? $("#" + form + " #DISCOUNTHarga").val() : '0';
        var ASURANSIHarga = ($("#" + form + " #KODE_ASURANSIHarga").val()) ? $("#" + form + " #KODE_ASURANSIHarga").val() : '0';
        var NILAI_ASURANSI = ($("#" + form + " #NILAI_ASURANSI").val()) ? $("#" + form + " #NILAI_ASURANSI").val() : '0';
        var NILAI_ASURANSIUR = ($("#" + form + " #NILAI_ASURANSIUR").val()) ? $("#" + form + " #NILAI_ASURANSIUR").val() : '0';
        switch(KODE_HARGAHarga){
            case'1': //CIF
                break;
            case'2':
                if(NILAI_ASURANSI == '0' && ASURANSIHarga == '1'){
                    jAlert('Kode Harga CNF, dan bayar asuransi di LN, nilai asuransi harus diisi', site_name);
                    return false;
                }
                break;
            case'3':
                if(NILAI_FREIGHT == '0' && $("#fbc20_ #JNIMP").val() != '4' ){
                    jAlert('Kode Harga FOB, Freight harus diisi', site_name);
                    return false;
                }
                if(NILAI_ASURANSI == '0' && ASURANSIHarga == '1'){
                    jAlert('Kode Harga CNF, dan bayar asuransi di LN, nilai asuransi harus diisi', site_name);
                    return false;
                }
                break;
        }
        if (KODE_HARGAHarga != "") {
            document.getElementById('NILINV').value = HARGA_CIF;
            document.getElementById('KDVAL').value = KODE_VALUTA;
            document.getElementById('NDPBM').value = NDPBMHarga;
            document.getElementById('nilai_ndpbm').value = NDPBMUR;
            document.getElementById('FOB').value = FOBHarga;
            document.getElementById('FOBUR').value = FOBURHarga;
            document.getElementById('CIF').value = CIFHarga;
            document.getElementById('CIFRP').value = CIFRPHarga;
            document.getElementById('CIFUR').value = CIFURHarga;
            document.getElementById('FREIGHT').value = NILAI_FREIGHT;
            document.getElementById('FREIGHTUR').value = NILAI_FREIGHTUR;
            document.getElementById('BTAMBAHAN').value = BIAYAHarga;
            document.getElementById('DISCOUNT').value = DISCOUNTHarga;
            document.getElementById('KDASS').value = ASURANSIHarga;
            document.getElementById('ASURANSI').value = NILAI_ASURANSI;
            document.getElementById('ASURANSIUR').value = NILAI_ASURANSIUR;
            document.getElementById('KDHRG').value = KODE_HARGAHarga;
            if (KODE_HARGAHarga == 2) {
                var span22 = "CNF";
            } else {
                var span22 = "FOB";
            }
            $("#22").html(span22);
        }
        closedialog('divHitung');
        $("#BRUTOUR").focus();
    }
    function kode(data) {
        if (data == 1) {
            $("#harga").find("#NILAI_ASURANSI,#NILAI_FREIGHT").val('');
            $("#harga").find("#NILAI_ASURANSIUR,#NILAI_FREIGHTUR").val('0');
            $('#KODE_ASURANSIHarga').attr('disabled', "true");
            $('#NILAI_ASURANSI,#NILAI_ASURANSIUR').attr('disabled', "true");
            $('#NILAI_FREIGHT,#NILAI_FREIGHTUR').attr('disabled', "true");
            $('.hargacif').html('Harga CIF');
            $('.fob').html('FOB');
        } else if (data == 2) {
            $("#harga").find("#NILAI_FREIGHT").val('');
            $("#harga").find("#NILAI_FREIGHTUR").val('0');
            $('#KODE_ASURANSIHarga').removeAttr("disabled");
            $('#NILAI_ASURANSI,#NILAI_ASURANSIUR').removeAttr("disabled");
            $('#NILAI_FREIGHT,#NILAI_FREIGHTUR').attr('disabled', "true");
            $('.hargacif').html('Harga CNF');
            $('.fob').html('CNF');
        } else if (data == 3) {
            $('#KODE_ASURANSI').removeAttr("disabled");
            $('#NILAI_ASURANSI,#NILAI_ASURANSIUR').removeAttr("disabled");
            $('#NILAI_FREIGHT,#NILAI_FREIGHTUR').removeAttr("disabled");
            $('.hargacif').html('Harga FOB');
            $('.fob').html('FOB');
        }
        prosesHarga('harga');
    }
    function isNumber(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }
    function showBM() {
        jConfirm('Ini untuk melakukan pengisian tarif Spesifik misalnya beras gula dan tarif berdasarkan satuan lainnya.<br> Teruskan?', site_name,
            function(r) {
                if (r == true) {
                    Dialog(site_url + "/bc20/getBm", 'dialog-bm', 'Form Bea Masuk', 450, 300);
                } else {
                    return false;
                }
            });
    }
    function satuan(data) {
        var CIF = parseFloat($('#DNILINV').val());
        var BTDISKON = parseFloat($('#BTDISKON').val());
        var tot = (CIF + BTDISKON) / parseFloat(data);
        if (data != "")
            $('#HARGA_SATUAN').val(tot);
        $('#HARGA_SATUANUR').val(ThausandSeperator('', tot, 2));
        if (data == 0)
            $('#HARGA_SATUAN,#HARGA_SATUANUR').val(0);
    }
    function tarif(id) {
        if (id == 2) {
            $('#tarf').show();
            $('#persens').html('');
        } else {
            $('#persens').html('%');
            $('#tarf').hide();
        }
    }
    function cekAngka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 35 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
    function tarifBm(id) {
        if (id == 2) {
            $('.judul').html('Spesifik(Satuan)');
            $('.dtl').show();
        } else {
            $('.judul').html('');
            $('.dtl').hide();
        }
    }
    function changeBM(id) {
        if (id == 2) {
            $("#Advolorum").hide();
            $("#Spesifik").show();
        } else {
            $("#Spesifik").hide();
            $("#Advolorum").show();
        }
    }
    function prosesHargaHeader(form) {
        var NDPBM = ReplaceAll($("#" + form + " #NDPBM").val(),',','');
        var CIF   = ReplaceAll($("#" + form + " #CIF").val(),',','');
        var TOTAL = parseFloat(NDPBM) * parseFloat(CIF);
        $("#" + form + " #CIFRP").val(ThausandSeperator('', TOTAL, 4));
    }
    function save_header(formid) {
        //$('#msgbox').html('');
        var dataSend = $(formid).serialize();
        jConfirm('Anda yakin Akan memproses data ini?', site_name,
            function(r) {
                if (r == true) {
                    jloadings();
                    $.ajax({
                        type: 'POST',
                        url: $(formid).attr('action'),
                        data: dataSend,
                        success: function(data) {
                            Clearjloadings();
                            if (data.search("MSG") >= 0) {
                                arrdata = data.split('#');
                                if (arrdata[1] == "OK") {
                                    $("#tabs_bc20").tabs({disabled: []});
                                    $("form").find("#CAR").val(arrdata[3]);
                                    $(".msgheader_").css('color', 'green');
                                    $(".msgheader_").html(arrdata[2]);
                                    if (formid == "#fbc20_") {
                                        cekValidasi(arrdata[3]);
                                        if($('#fbc20_ #act').val()=='save'){
                                            $('#tab-Barang').load(site_url + '/bc20/listBarang/' + arrdata[3]);
                                        }
                                    }
                                    if (arrdata[4])$("#DivHeaderForm").load(arrdata[4]);
                                } else {
                                    $(".msgheader_").css('color', 'red');
                                    $(".msgheader_").html(arrdata[2]);
                                }
                            } else {
                                $(".msgheader_").css('color', 'red');
                                if (formid == '#fpass'){
                                    $(".msgheader_").html(arrdata[2]);
                                }else{
                                    $(".msgheader_").html('Proses Gagal.');
                                }
                            }
                        }
                    });
                } else {
                    return false;
                }
            });
    }
    function save_detil(formid, msg) {
        var dataSend = $(formid).serialize();
        $.ajax({
            type: 'POST',
            url: $(formid).attr('action'),
            data: dataSend,
            success: function(data) {
                if (data.search("MSG") >= 0) {
                    arrdata = data.split('#');
                    if (arrdata[1] == "OK") {
                        if(formid == '#fbarang_'){
                            $('#tab-Barang').load(site_url + '/bc20/listBarang/' + arrdata[5]);
                        }else{
                            $("." + msg).css('color', 'green');
                            $("." + msg).html(arrdata[2]);
                            $(formid + 'list').load($(formid).attr('list'));
                            $(formid + 'form').slideDown('slow');
                            $(formid + 'form').html('');
                        }
                    } else {
                        $("." + msg).css('color', 'red');
                        $("." + msg).html(arrdata[2]);
                    }
                } else {
                    $("." + msg).css('color', 'red');
                    $("." + msg).html('Proses Gagal.');
                }
            }
        });
        return false;
    }
    function cekValidasi(car) {
        $('#msgbox').html('');
        $("#msgbox").html('<div style="margin-left: 15px; margin-top: 15px;"><img src="' + base_url + 'img/loaders/facebook.gif" /> loading...</div>');
        $("#msgbox").dialog({
            resizable: false,
            height: 500,
            modal: true,
            width: 700,
            title: 'Validasi Dokumen',
            open: function() {
                $.ajax({
                    type: 'POST',
                    url: site_url + '/bc20/alertcekstatus/' + car + '/t_bc20hdr',
                    success: function(data) {
                        $("#msgbox").html(data);
                        $('#URSTATUS').html($('#STATUSCEK').val());
                        call(site_url + '/bc20/refreshHeader/' + car, 'tab-Header');
                    }
                });
            },
            buttons: {
                Close: function() {
                    $(this).dialog("close");
                }
            }
        });
        
    }
    function cekValidasiDtl(car, seri){
        $('#msgbox').html('');
        $("#msgbox").html('<div style="margin-left: 15px; margin-top: 15px;"><img src="' + base_url + 'img/loaders/facebook.gif" /> loading...</div>');
        $("#msgbox").dialog({
            resizable: false,
            height: 500,
            modal: true,
            width: 600,
            title: 'Validasi Dokumen',
            open: function() {
                $.ajax({
                    type: 'POST',
                    url: site_url + '/bc20/cekStatusDtl/' + car + '/' + seri,
                    success: function(data) {
                        var trm = data.trim();
                        var arr = trm.split('|');
                        var test = arr[0];
                        if (test == 'OK') {
                            var isi = "<span class='icon-check'></span> <b style='color: #ffffff'>Lengkap</b>";
                        } else if (test == 'ER') {
                            var isi = "<span class='icon-x'></span> <b style='color: #ffffff'>Tidak Lengkap</b>";
                        }
                        $('#DTLOK').html(isi);
                        $("#msgbox").html(arr[1]);
                    }
                });
            },
            buttons: {
                Close: function() {
                    $(this).dialog("close");
                }
            }
        });
    }
    function QQ() {
        $('#indentor #INDID').val($('#fbc20_ #IMPID').val());
        $('#indentor #INDNPWP').val($('#fbc20_ #IMPNPWP').val());
        $('#indentor #INDNAMA').val($('#fbc20_ #IMPNAMA').val());
        $('#indentor #INDALMT').val($('#fbc20_ #IMPALMT').val());
        $('#indentor #INDNPWP').focus();
    }
    function getIndentor(){
        
        $('#fbc20_ #INDID').val($('#indentor #INDID').val());
        $('#fbc20_ #INDNPWP').val($('#indentor #INDNPWP').val());
        $('#fbc20_ #INDNAMA').val($('#indentor #INDNAMA').val());
        $('#fbc20_ #INDALMT').val($('#indentor #INDALMT').val());
        closedialog('divIndentor');
    }
    //====================header========================
    function BLAWB(ID_MODA){
        if(ID_MODA=='1' || ID_MODA==''){
            $('#BLAWB').html('BL');
        }else if(ID_MODA=='4'){
            $('#BLAWB').html('AWB');
        }else{
            $('#BLAWB').html('');
        }
    }
    function onoff(on) {
        if($('#STATUS').val()=='000' || $('#STATUS').val()=='010' || $('#STATUS').val()=='060' || $('#STATUS').val()==''){
            if (on) {
                $('#on').show();
                $('#off').hide();
                $('#fbc20_ :input').removeAttr('disabled');
                $('#fbc20_ div').removeClass('disabled');
                $('#fbc20_ .dataReferensi :input').attr('disabled','disabled');
                $('#tabs_bc20').tabs({disabled: [1, 2]});
            } else {
                cancel('fbc20_');
                declareDP('fbc20_');
                $('#on').hide();
                $('#off').show();
                $('#fbc20_ :input').attr('disabled','disabled');
                $('#fbc20_ .dataReferensi :input').removeAttr("disabled");
                $('#fbc20_ div').addClass('disabled');
                $('#tabs_bc20').tabs();
                if ($('#act').val() == 'save') {
                    $('#tabs_bc20').tabs({disabled: [1]});
                } else {
                    $('#tabs_bc20').tabs({disabled: false});
                }
            }
        }else{
            $('#on').hide();
            $('#off').show();
            $("#fbc20_ :input").attr('disabled','disabled');
            $('#fbc20_ div').addClass('disabled');
            $("#fbc20_ .dataReferensi :input").removeAttr("disabled");
            $("#tabs_bc20").tabs();
            if ($("#act").val() == 'save') {
                $("#tabs_bc20").tabs({disabled: [1]});
            } else {
                $("#tabs_bc20").tabs({disabled: false});
            }
        }
        $(".onterus").removeAttr("disabled");
        if($('#STATUS').val()=='010' || $('#STATUS').val()=='020'){
            $('#btnQUEUED').show();
        }else{
            $('#btnQUEUED').hide();
        }
    }
    function jkwaktu() {
        if ($('#JNIMP').val() == '2') {
            $('#jangkawaktu').show();
        } else {
            $('#jangkawaktu').hide();
        }
    }
    function gotoQUEUED(car,tipe,sts){
        jConfirm('Data akan dibentuk EDIFACT ?', site_name,
        function(r) {
            if (r == true) {
                $(function() {
                    $('#msgbox').html('');
                    $("#msgbox").html('<div style="margin-left: 15px; margin-top: 15px;"><img src="' + base_url + 'img/loaders/facebook.gif" /> loading...</div>');
                    $("#msgbox").dialog({
                        resizable: false,
                        height: 400,
                        modal: true,
                        width: 600,
                        title: 'Bentuk / Batal Edifact',
                        open: function() {
                            if(sts=='010'){
                                if(tipe=='2'){
                                    call(site_url + '/bc20/getSRP/' + car, 'msgbox');
                                }else{
                                    $.ajax({
                                        type: 'POST',
                                        url: site_url+'/bc20/queued',
                                        data: '&CAR=' + car,
                                        error: function(){
                                            Clearjloadings();
                                        },
                                        success: function(data) {
                                            Clearjloadings();
                                            $("#msgbox").html(data);
                                            call(site_url + '/bc20/refreshHeader/' + car, 'tab-Header');
                                        }
                                    });
                                }
                            }else{
                                $.ajax({
                                    type: 'POST',
                                    url: site_url+'/bc20/queued',
                                    data: '&CAR=' + car,
                                    error: function(){
                                        Clearjloadings();
                                    },
                                    success: function(data) {
                                        Clearjloadings();
                                        $("#msgbox").html(data);
                                        call(site_url + '/bc20/refreshHeader/' + car, 'tab-Header');
                                    }
                                });
                            }
                            
                        },
                        buttons: {
                            Close: function() {
                                $(this).dialog("close");
                            }
                        }
                    });
                });
            }else {
                return false;
            }
        });
    }
    function navPage(tipe, page, jmlpage) {
    //alert(jmlpage); return false;
        var goPage = 1;
        var car = $('#fbarang_ #CAR').val();
        jmlpage = parseInt(jmlpage);
        page = parseInt(page);
        switch (tipe) {
            case 'first':
                goPage = 1;
                break;
            case 'preview':
                if (page > 1) {
                    goPage = page - 1;
                } else if (page <= 1) {
                    goPage = 1;
                }
                break;
            case 'next':
                if (page < jmlpage) {
                    goPage = page + 1;
                } else if (page >= jmlpage) {
                    goPage = jmlpage;
                }
                break;
            case 'last':
                goPage = jmlpage;
                break;
        }
        call( site_url + '/bc20/pageBarang/' + car + '/' + goPage,'fbarang_form','');
        //alert(goPage);

    }
</script>
<div class="content_luar">
    <div class="content_dalam">
        <div id="tabs_bc20">
            <ul>
                <li><a href="#tab-Header">Data Header</a></li>
                <li><a href="#tab-Barang">Data Barang</a></li>
            </ul>
            <div id="tab-Header"><?= $HEADER ?></div>
            <div id="tab-Barang"><?= $DETIL ?></div>
        </div>
    </div>
</div>