<script>
    $('document').ready(function() {
        var stsHdr          = $('#fbc20_ #STATUS').val();
        var JenisTarif      = $('#KDTRPCUK').val();
        var JenisTarifBm    = $('#KODE_TARIF_BM').val();
        if (JenisTarif == 2)
            tarif(JenisTarif);
        if (JenisTarifBm == 2) {
            $('#bmHidden').show();
            tarifBm(JenisTarifBm);
        }
        if(stsHdr=='000' || stsHdr=='010'){
            $('.editdisable').show();
        }else{
            $('.editdisable').hide();
        }
        f_MAXLENGTH('fbarang_');
        total();
        satuan($('#JMLSAT').val());
        switch($('#KDHRG').val()){
            case '1': 
                $('#URKDHRG').html('CIF');
            break;
            case '2': 
                $('#URKDHRG').html('CNF');
            break;
            case '3': 
                $('#URKDHRG').html('FOB');
            break;
        }
        
        if($('#act').val() == 'update'){
            if($('#KDTRPBM').val() == '2'){
                $("#tipespesifik").html('<table><tr><td align="right">Per</td><td><input type="text" name="TARIF[KDSATBM]" id="KODE_SATUAN_BM" class="ssstext" value="<?= $DETIL["KDSATBM"] ?>" url="<?= site_url('autocomplete/satuan'); ?>" onfocus="Autocomp(this.id,this.form.id);" urai="" />&nbsp;&nbsp;Jumlah Satuan-&nbsp;<input type="text" name="DETIL[SATBMJM]" id="JUMLAH_BM" class="stext" value="<?= $DETIL["SATBMJM"] ?>"/></td></tr></table>');
                $("#tipebm").html('Spesifik<input type="hidden" name="TARIF[KDTRPBM]" id="KODE_TARIF_BM" value="2">');
            }else{
                $("#tipespesifik").html('');
                $("#tipebm").html('Advolorum<input type="hidden" name="TARIF[KDTRPBM]" id="KODE_TARIF_BM" value="1">');
            }
            if($('#KDTRPCUK').val() == '2'){
                $("#tarf").show();
            }
        }
    });
    function showPPh(){
        if($('#APINO').val()==''){
            jAlert('Perubahan Tarif PPH harus memiliki nomor API.',site_name);
        }else{
            jConfirm('Ini untuk melakukan penyesuaian terif PPh Pasal 22,<br>Berdasarkan PMK Nomor 175/PMK.011/2013.<br>Teruskan?', site_name,
            function(r) {
                if (r == true) {
                    Dialog(site_url + "/bc20/getPPh", 'shadow', 'Form PPh', 300, 200);
                } else {
                    return false;
                }
            });
        }
    }
    
    function total() {
        var HKDHRG    = $('#KDHRG').val();
        var HBTAMBAHAN= parseFloat($('#HBTAMBAHAN').val());
        var HDISKON   = parseFloat($('#HDISKON').val());
        var HFREIGHT  = parseFloat($('#HFREIGHT').val());
        var HINVOICE  = parseFloat($('#HINVOICE').val());
        var HASURANSI = parseFloat($('#HASURANSI').val());
        var HNDPBM    = parseFloat($('#HNDPBM').val());
        var DFreight  = 0;
        var DAsuransi = 0;
        var BTDiskon = Math.ceil((HBTAMBAHAN - HDISKON) * parseFloat($('#DNILINV').val()) / HINVOICE * 100 ) / 100;
        $('#BTDISKON').val(BTDiskon);
        $('#BTDISKONUR').val(ThausandSeperator('', BTDiskon, 2));
        if(parseFloat($('#DNILINV').val()) > 0){
            DFreight = Math.ceil(HFREIGHT  * (parseFloat($('#DNILINV').val()) / HINVOICE) * 100) / 100 ; 
            DAsuransi= Math.ceil(HASURANSI * (parseFloat($('#DNILINV').val()) / HINVOICE) * 100) / 100 ;
        }else{
            DFreight = 0;
            DAsuransi= 0;
        }
        $('#DFREIGHT').val(DFreight);
        $('#DFREIGHTUR').val(ThausandSeperator('', DFreight, 2));
        $('#DASURANSI').val(DAsuransi);
        $('#DASURANSIUR').val(ThausandSeperator('', DAsuransi, 2));
        var DCif = 0;
        var DCnf = 0;
        var DFob = 0;
        switch (HKDHRG) {
            case '1':
                DCif = parseFloat($('#DNILINV').val()) + BTDiskon;
                DFob = parseFloat(DCif - DFreight - DAsuransi);
                break;
            case '2':
                DCnf = parseFloat($('#DNILINV').val()) + BTDiskon;
                DFob = parseFloat(DCnf - DFreight);
                DCif = parseFloat(DCnf + DAsuransi);
                break;
            case '3':
                DFob = parseFloat($('#DNILINV').val()) + BTDiskon;
                DCif = parseFloat(DFob + DFreight + DAsuransi);
            break;
        }
        var Dcifrp = (HNDPBM * DCif);

        $('#DCIFUR').val(ThausandSeperator('', DCif, 2));
        $('#DCIF').val(DCif);
        $('#HARGAFOBUR').val(ThausandSeperator('', DFob, 2));
        $('#HARGAFOB').val(DFob);
        $('#CIFRPUR').val(ThausandSeperator('', Dcifrp, 2));
        $('#CIFRP').val(Dcifrp);    
    }
</script>
    <table style="width: 100%">
        <tr>
            <td style="width: 70%">
                <button onclick="save_post_msg('fbarang_', '', 'fbarang_list');$('#fbarang_form').html('');" class="btn editdisable">Simpan</button>
                <button onclick="cancel('fbarang_');$('#fbarang_form').html('');" class="btn editdisable">Batal</button>
            </td>
            <td style="text-align: center"> 
                <button onclick="navPage('first', $('#fbarang_ #SERIAL').val(), $('#fbarang_ #JMLDTL').val());return false;" class="btn" style="height: 22px; width:25px; vertical-align: middle;" > <span class="icon-first"></span></button>
                
                <button onclick="navPage('preview', $('#fbarang_ #SERIAL').val(), $('#fbarang_ #JMLDTL').val());return false;" class="btn" style="-webkit-transform: rotate(180deg);-moz-transform: rotate(180deg);-o-transform: rotate(180deg);-ms-transform: rotate(180deg);transform: rotate(180deg); height: 22px; width:25px; vertical-align: middle;" > <span class="icon-play"></span></button>
                
                <input type="text" id="NOBARANG" class="ssstext" value="<?= $DETIL['SERIAL']; ?>" style="text-align: center;" readonly="readonly"/>
                
                <button onclick="navPage('next', $('#fbarang_ #SERIAL').val(), $('#fbarang_ #JMLDTL').val());return false;" class="btn" style="height: 22px; width:25px; vertical-align: middle;"> <span class="icon-play"></span></button> 
                
                <button onclick="navPage('last', $('#fbarang_ #SERIAL').val(), $('#fbarang_ #JMLDTL').val());return false;" class="btn" style="height: 22px; width:25px; vertical-align: middle;"><span class="icon-last"></span></button>
            </td>
        </tr>
    </table>
<hr style="margin-top: 5px; margin-bottom: 5px;" >
<div id="navBarang">
<form id="fbarang_" name="fbarang_" action="<?= site_url('bc20/barang/'.$DETIL['CAR']); ?>" onsubmit="return false;" method="post" autocomplete="off" enctype="multipart/form-data" list="<?= site_url('bc20/daftarDetil/barang/'.$aju) ?>">
    <table width="100%" border="0">
        <tr>
            <td width="50%" valign="top">
                Nomor Aju &nbsp; : &nbsp; <b><?= substr($DETIL['CAR'], 0, 6) . '-' . substr($DETIL['CAR'], 7, 6) . '-' . substr($DETIL['CAR'], 12, 8) . '-' . substr($DETIL['CAR'], 20, 6); ?></b><br>Seri ke <input type="text" name="SERIAL" id="SERIAL" value="<?= $DETIL['SERIAL']; ?>" class="ssstext text" readonly="readonly" /> dari <input type="text" name="JMLDTL" id="JMLDTL" value="<?= $JMLDTL; ?>" class="ssstext text" readonly="readonly" />
            </td>
            <td style="text-align: right">
                <button onclick="cekValidasiDtl($('#fbarang_ #CAR').val(),$('#fbarang_ #SERIAL').val())" class="btn" id="DTLOK"><span class="<?= ($DETIL['DTLOK']!='1')?'icon-x':'icon-check'; ?>"></span> <b style="color: #ffffff"><?= $DETIL['URDTLOK'] ?></b></button>
            </td>
        </tr>
        
    </table>
    <?= $MAXLENGTH?>
    <input type="hidden" name="act" id="act" value="<?= $act; ?>" />
    <input type="hidden" name="CAR" id="CAR" value="<?= $DETIL['CAR']; ?>" />
    <input type="hidden" name="KDHRG" id="KDHRG" value="<?= $DETIL['KDHRG'] ?>">
    <input type="hidden" name="HFREIGHT" id="HFREIGHT" value="<?= $DETIL['HFREIGHT'] ?>">
    <input type="hidden" name="HINVOICE" id="HINVOICE" value="<?= $DETIL['HINVOICE'] ?>">
    <input type="hidden" name="HASURANSI" id="HASURANSI" value="<?= $DETIL['HASURANSI'] ?>">
    <input type="hidden" name="HBTAMBAHAN" id="HBTAMBAHAN" value="<?= $DETIL['HBTAMBAHAN'] ?>">
    <input type="hidden" name="HDISKON" id="HDISKON" value="<?= $DETIL['HDISKON'] ?>">
    <input type="hidden" name="HNDPBM" id="HNDPBM" value="<?= $DETIL['HNDPBM'] ?>">
    <input type="hidden" name="KDTRPBM" id="KDTRPBM" value="<?= $DETIL['KDTRPBM'] ?>">
    <input type="hidden" name="APINO" id="APINO" value="<?= $DETIL['APINO'] ?>">
    <table width="100%">
        <tr>
            <td width="50%" valign="top">
                <fieldset>
                    <legend>Data Barang</legend>
                    <table width="100%">
                        <tr>
                            <td width="25%">Kode HS Seri</td>
                            <td width="75%">:
                                <input type="text" name="DETIL[NOHS]" id="NOHS" url="<?= site_url('referensi/autoComplate/HS-TARIF'); ?>" class="stext"  value="<?= $this->fungsi->FormatHS($DETIL['NOHS']); ?>" onfocus="Autocomp(this.id);unFormatHS(this.id)" urai="NOHS;SERITRP;TRPBM;TRPPPN;TRPPBM;TRPCUK;TRPPPH" maxlength="15" onblur="FormatHS(this.id)" on /> - 
                                <input type="text" name="DETIL[SERITRP]" id="SERITRP" class="ssstext"  value="<?= $DETIL['SERITRP']; ?>"/> &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">Uraian Barang</td>
                            <td><span style="vertical-align: top;">:</span>
                                <textarea name="DETIL[BRGURAI]" id="BRGURAI"  class="ltext"><?= $DETIL['BRGURAI']; ?></textarea>
                                <span style="vertical-align: top;"><button onclick="tb_search('barang', 'BRGURAI;MERK;TIPE;SPFLAIN;NOHS;SERITRP', 'Kode Barang', this.form.id, 650, 400, 'NOHS;SERITRP;')" class="btn">...</button></span>

                            </td>
                        </tr>
                        <tr>
                            <td>Merk</td>
                            <td>:
                                <input type="text" name="DETIL[MERK]" id="MERK" class="mtext" value="<?= $DETIL['MERK']; ?>" maxlength="15" />
                            </td>
                        </tr>
                        <tr>
                            <td>Tipe</td>
                            <td>:
                                <input type="text" name="DETIL[TIPE]" id="TIPE" class="mtext" value="<?= $DETIL['TIPE']; ?>" maxlength="15" />
                            </td>
                        </tr>
                        <tr>
                            <td>Sepesifikasi Lain</td>
                            <td>:
                                <input type="text" name="DETIL[SPFLAIN]" id="SPFLAIN" class="mtext" value="<?= $DETIL['SPFLAIN']; ?>" maxlength="15" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <fieldset>
                    <legend>Kemasan</legend>
                    <table width="100%">
                        <tr>
                            <td width="25%">Jumlah</td>
                            <td width="75%">:
                                <input type="text" name="JUMLAH_KEMASANUR" id="JUMLAH_KEMASANUR" class="numtext" value="<?= number_format($DETIL['KEMASJM'], 0); ?>" onkeyup="this.value = ThausandSeperator('KEMASJM', this.value, 2);"/>
                                <input type="hidden" name="DETIL[KEMASJM]" id="KEMASJM" value="<?= $DETIL['KEMASJM'] ?>" />
                            </td>
                        </tr>
                        <tr>
                            <td>Jenis Kemasan</td>
                            <td>:
                                <input type="text" name="DETIL[KEMASJN]" id="KEMASJN" value="<?= $DETIL['KEMASJN']; ?>" class="ssstext" maxlength="2" onblur="checkCode(this);" grp="kemasan" urai="urjenis_kemasan" />
                                <button onclick="tb_search('kemasan', 'KEMASJN;urjenis_kemasan', 'Kode Kemasan', this.form.id, 650, 400)" class="btn">...</button><span id="urjenis_kemasan"><?= $DETIL['KODE_KEMASANUR']; ?></span></td>
                        </tr>
                        <tr>
                            <td>Netto</td>
                            <td>:
                                <input type="text" name="NETTOUR" id="NETTOUR" class="numtext"  value="<?= number_format($DETIL['NETTODTL'], 0); ?>" onkeyup="this.value = ThausandSeperator('NETTODTL', this.value, 2);"/>&nbsp; Kilogram (KGM)
                                <input type="hidden" name="DETIL[NETTODTL]" id="NETTODTL" value="<?= $DETIL['NETTODTL'] ?>" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <fieldset>
                    <legend>Tarif dan Fasilitas</legend>
                    <table width="100%">
                        <tr>
                            <td width="25%">
                                <button onclick="showBM()" class="btn onterus"> BM </button>
                            </td>
                            <td width="75%"> :
                                <input type="text" name="TARIF[TRPBM]" id="TRPBM" value="<?= number_format($DETIL['TRPBM'],2); ?>" maxlength="5" class="ssstext"/>&nbsp;%
<?= form_dropdown('FASILITAS[KDFASBM]', $JENIS_KERINGANAN, $DETIL['KDFASBM'], 'id="KODE_FAS_BM" class="stext" '); ?>&nbsp;
                                <input type="text" name="FASILITAS[FASBM]" id="FAS_BM" value="<?= number_format($DETIL['FASBM'],2); ?>" maxlength="5" class="ssstext"/>&nbsp;%
                                <span id="tipebm"></span>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><span id="tipespesifik"></span></td></tr>        
                        <tr>
                            <td>PPN</td>
                            <td>:
                                <input type="text" name="TARIF[TRPPPN]" id="TRPPPN" maxlength="5" class="ssstext" value="<?= number_format($DETIL['TRPPPN'],2); ?>" />&nbsp;%
<?= form_dropdown('FASILITAS[KDFASPPN]', $JENIS_KERINGANAN, $DETIL['KDFASPPN'], 'id="KODE_FAS_PPN" class="stext" '); ?>&nbsp;
                                <input type="text" name="FASILITAS[FASPPN]" id="FAS_PPN" maxlength="5" class="ssstext" value="<?= number_format($DETIL['FASPPN'],2); ?>" />&nbsp;%
                            </td>
                        </tr>
                        <tr>
                            <td>PPnBM</td>
                            <td>:
                                <input type="text" name="TARIF[TRPPBM]" id="TRPPBM" maxlength="5" class="ssstext" value="<?= number_format($DETIL['TRPPBM'],2); ?>" />&nbsp;%
<?= form_dropdown('FASILITAS[KDFASPBM]', $JENIS_KERINGANAN, $DETIL['KDFASPBM'], 'id="KODE_FAS_PPNBM" class="stext" '); ?>&nbsp;
                                <input type="text" name="FASILITAS[FASPBM]" id="FAS_PPNBM" maxlength="5" class="ssstext" value="<?= number_format($DETIL['FASPBM'],2); ?>" />&nbsp;%
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button onclick="showPPh()" class="btn onterus"> PPh </button>
                            </td>
                            <td>:
                                <input type="text" name="TARIF[TRPPPH]"  id="TRPPPH" maxlength="5" class="ssstext" value="<?= number_format($DETIL['TRPPPH'],2); ?>" readonly="readonly"/>&nbsp;%
<?= form_dropdown('FASILITAS[KDFASPPH]', $JENIS_KERINGANAN, $DETIL['KDFASPPH'], 'id="KODE_FAS_PPH" class="stext" '); ?>&nbsp;
                                <input type="text" name="FASILITAS[FASPPH]" id="FAS_PPH" maxlength="5" class="ssstext" value="<?= number_format($DETIL['FASPPH'],2); ?>" />&nbsp;%
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
            <td valign="top">
                <fieldset>
                    <legend>Harga</legend>
                    <table width="100%" >
                        <tr>
                            <td>Total/Detil (<span id="URKDHRG"></span>)</td>
                            <td >:
                                <input type="text" name="INVOICEUR" id="INVOICEUR" class="numtext" value="<?= number_format($DETIL['DNILINV'], 2); ?>" onkeyup="this.value = ThausandSeperator('DNILINV', this.value, 2); total();satuan($('#JMLSAT').val());"/>
                                <input type="hidden" name="DETIL[DNILINV]" id="DNILINV" value="<?= $DETIL['DNILINV'] ?>" />
                            </td>
                            <td>Harga FOB</td>
                            <td>:
                                <input type="text" name="HARGAFOBUR" id="HARGAFOBUR" class="numtext" value="" readonly="readonly"/>
                                <input type="hidden" name="HARGAFOB" id="HARGAFOB"  value="" />
                            </td>
                        </tr>
                        <tr>
                            <td>BT-Diskon</td>
                            <td>:
                                <input type="text" name="BTDISKONUR" id="BTDISKONUR" class="numtext" value="<?= number_format($DETIL['BTDISKON'], 2); ?>" readonly="readonly"/>
                                <input type="hidden" name="BTDISKON" id="BTDISKON"  value="" />
                            </td>
                            <td>Freight</td>
                            <td>:
                                <input type="text" name="DFREIGHTUR" id="DFREIGHTUR" class="numtext"  readonly="readonly"/>
                                <input type="hidden" name="DFREIGHT" id="DFREIGHT"  value="" />
                            </td>
                        </tr>
                        <tr>
                            <td>Jumlah Satuan</td>
                            <td>:
                                <input type="text" name="JUMLAH_SATUANUR" id="JUMLAH_SATUANUR" class="numtext"  value="<?= number_format($DETIL['JMLSAT'], 2); ?>" onkeyup="this.value = ThausandSeperator('JMLSAT', this.value, 2);
                satuan($('#JMLSAT').val());"/>
                                <input type="hidden" name="DETIL[JMLSAT]" id="JMLSAT" value="<?= $DETIL['JMLSAT'] ?>" />
                            </td>
                            <td>Asuransi</td>
                            <td>:
                                <input type="text" name="dasuransiur" id="DASURANSIUR" class="numtext" value="" readonly="readonly"/>
                                <input type="hidden" name="DASURANSI" id="DASURANSI"  value="" />
                            </td>
                        </tr>
                        <tr>
                            <td>Kode Satuan</td>
                            <td colspan="3">:
                                <input type="text" name="DETIL[KDSAT]" id="KDSAT" class="ssstext"  value="<?= $DETIL['KDSAT']; ?>" maxlength="3" onblur="checkCode(this);" grp="satuan" urai="urjenis_satuan" />
                                <button onclick="tb_search('satuan', 'KDSAT;urjenis_satuan', 'Kode Satuan', this.form.id, 650, 400)" class="btn">...</button> <span id="urjenis_satuan"><?= $DETIL['KODE_SATUANUR']; ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td>Harga Satuan</td>
                            <td>:
                                <input type="text" name="HARGA_SATUANUR" id="HARGA_SATUANUR" class="numtext" value="" readonly="readonly"/>
                                <input type="hidden" name="HARGA_SATUAN" id="HARGA_SATUAN"  value="" />
                            </td>
                            <td>Harga CIF</td>
                            <td>:
                                <input type="text" name="CIFUR" id="DCIFUR" class="numtext" value="<?= number_format($DETIL['DCIF'], 2); ?>" readonly="readonly"/>
                                <input type="hidden" name="DETIL[DCIF]" id="DCIF" value="<?= $DETIL['DCIF']; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>CIF Rp</td>
                            <td>:
                                <input type="text" name="CIFRPUR" id="CIFRPUR" class="numtext" value="<?= number_format($DETIL['CIFRP'], 2); ?>" readonly="readonly"/>
                                <input type="hidden" name="DETIL[CIFRP]" id="CIFRP" value="<?= $DETIL['CIFRP']; ?>"/>
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <fieldset>
                    <legend>Cukai</legend>
                    <table width="100%">
                        <tr>
                            <td width="25%">Komoditi</td>
                            <td width="75%">:
                                <?= form_dropdown('TARIF[KDCUK]', $KOMODITI, $DETIL['KDCUK'], 'id="KODE_CUKAI" class="dtext" '); ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Jenis Tarif</td>
                            <td>:
                                <?= form_dropdown('TARIF[KDTRPCUK]', $JENIS_TARIF, $DETIL['KDTRPCUK'], 'id="KDTRPCUK" class="dtext" onchange="tarif(this.value)"'); ?>
                                <input type="text" name="TARIF[TRPCUK]" id="TRPCUK" class="ssstext" value="<?= $DETIL['TRPCUK']; ?>" /><span id="persens">%</span>
                            </td>
                        </tr>
                        <tr id="tarf" style="display:none;">
                            <td colspan="2" style="padding-left:90px;"> 
                                <fieldset style="padding-left:10px;padding-bottom:10px;">
                                    <legend>Jenis Tarif : Spesifik</legend>
                                    <table > 
                                        <tr>		
                                            <td>Per :&nbsp;
                                                <input type="text" class="ssstext" name="TARIF[KDSATCUK]" id="KODE_SATUAN_CUKAI" value="<?= $DETIL['KDSATCUK']; ?>" url="<?= site_url('referensi/autoComplate/SATUAN'); ?>" urai="KODE_SATUAN_CUKAI" onfocus="Autocomp(this.id, this.form.id);"  />&nbsp;Jumlah : &nbsp;<input type="text" class="sstext" name="DETIL[SATCUKJM]" id="JUMLAH_CUKAI" value="<?= number_format($DETIL['SATCUKJM'],2); ?>"/></td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td> 	            	
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>:
<?= form_dropdown('FASILITAS[KDFASCUK]', $JENIS_KERINGANAN, $DETIL['KDFASCUK'], 'id="KODE_FAS_CUKAI" class="text" '); ?>&nbsp;
                                <input type="text" name="FASILITAS[FASCUK]" id="FAS_CUKAI" maxlength="5" class="ssstext" value="<?= $DETIL['FASCUK']; ?>"  />&nbsp;%</td>
                        </tr>
                    </table>
                </fieldset>
                <fieldset>
                    <legend>Lain - Lain</legend>
                    <table width="100%">
                        <tr>
                            <td width="25%">Fasilitas</td>
                            <td width="75%">:
                                <input type="text" name="DETIL[KDFASDTL]" id="KDFASDTL" class="ssstext" value="<?= $DETIL['KDFASDTL']; ?>" onblur="checkCode(this);" grp="fasilitas" urai="URKDFASDTL" maxlength="2"/>
                                <button onclick="tb_search('fasilitas', 'KDFASDTL;URKDFASDTL', 'Kode Fasilitas', this.form.id, 650, 400)" class="btn">...</button> <span id="URKDFASDTL"><?= $DETIL['KDFASDTLUR']; ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td>Negara Asal</td>
                            <td>:
                                <input type="text" name="DETIL[BRGASAL]" id="BRGASAL" class="ssstext" value="<?= $DETIL['BRGASAL']; ?>" onblur="checkCode(this);" grp="negara" urai="URBRGASAL" maxlength="2"/>
                                <button onclick="tb_search('negara', 'BRGASAL;URBRGASAL', 'Kode Negara', this.form.id, 650, 400)" class="btn">...</button>
                                <span id="URBRGASAL"><?= $DETIL['NEGARA_ASALUR']; ?></span>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>
</form>
</div>
<hr style="margin-top: 10px; margin-bottom: 10px;" >