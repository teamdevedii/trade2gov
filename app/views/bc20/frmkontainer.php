<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<form id="fkontainer_" name="fkontainer_" action="<?= site_url('bc20/kontainer/'.$KONTAINER['CAR']); ?>" onsubmit="return false;" method="post" autocomplete="off" >
    <input type="hidden" name="act" value="<?= $act; ?>" />
    <input type="hidden" name="KONTAINER[CAR]" id="CAR" value="<?= $KONTAINER['CAR']; ?>" />
    <input type="hidden" name="KONTAINER[CONTID]" value="<?= $KONTAINER['CONTID']; ?>" />
    <table cellspacing="5">
        <tr>
            <td>Nomor :</td>
            <td>Ukuran :</td>
            <td>Tipe :</td>
        </tr>
        <tr>
            <td><input type="text" name="KONTAINER[CONTNO]" id="CONTNO" class="mtext" value="<?= $KONTAINER['CONTNO']; ?>" maxlength="17" style="" wajib="yes"/></td>
            <td><?= form_dropdown('KONTAINER[CONTUKUR]', $UKURAN, $KONTAINER['CONTUKUR'], 'id="UKURAN" class="sstext" wajib="yes" '); ?></td>
            <td><?= form_dropdown('KONTAINER[CONTTIPE]', $TIPE, $KONTAINER['CONTTIPE'], 'id="TIPE" class="sstext" wajib="yes" '); ?></td>
        </tr>
        <tr>
            <td colspan="3">
                <button onclick="saveContainer();" class="btn editdisable">Simpan</button>
                <button onclick="cancel('fkontainer_');$('#fkontainer_form').html('');" class="btn editdisable">Batal</button>
            </td>
        </tr>	        
    </table>
</form>
<script>
    $(document).ready(function(){
        var stsHdr = $('#fbc20_ #STATUS').val();
        if(stsHdr=='000' || stsHdr=='010'){
            $('.editdisable').show();
        }else{
            $('.editdisable').hide();
        }
        $('#fkontainer_form').show();
    });
    
    function saveContainer(){
        if($('#TIPE').val()!='L'){
            save_post_msg('fkontainer_', '', 'fkontainer_list');$('#fkontainer_form').html('');
        }else{
            jAlert('Tipe Kontainer LCL dianggap kemasan.',site_name);
        }
        
    }
    
</script>