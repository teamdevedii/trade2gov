<?php if (!defined('BASEPATH')){exit('No direct script access allowed');}?>
<div class="content_luar">
    <div class="content_dalam">
        <form id="fuploadBUP_" method="post" autocomplete="off" onsubmit="return false;" action="<?= site_url('bc20/uploadbup/upload'); ?>" enctype="multipart/form-data">
            <table width="100%">
                <tr>
                    <td colspan="2">* file back up dari modul pib versi dekstop.</td>
                </tr>
                <tr>
                    <td width="50px">File BUP</td>
                    <td>
                        <input type="file" name="fileBUP" id="fileBUP" accept="File BackUp PIB/*.BUP"/>
                        <button class="btn" onclick="sendFile();">Upload</button>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" >
                        <hr style="margin-bottom: 5px" >
                        <div id="message"></div>
                    </td>
                </tr>
                
            </table>
        </form>
    </div>
</div>
<script>
    function sendFile(){
        if($('#fileBUP').val()!==''){
            var file = document.getElementById('fileBUP');
            var formData = new FormData();
            formData.append("fileBUP", file.files[0]);
            jloadings();
            $.ajax({
                url: $('#fuploadBUP_').attr('action'),
                data : formData,
                type:'POST',
                processData: false,
                contentType: false,
                success: function (response) {
                    Clearjloadings();
                    $("#message").html(response);
                }
            });
        }
    }
</script>