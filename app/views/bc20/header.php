<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
$ro = '';
if ($TIPE_TRADER == '1') {
    $ro = ' readonly="readonly" ';
}
?>
<span id="DivHeaderForm">
    <span id="divtmp"></span>
    <form id="fbc20_" name="fbc20_" action="<?= site_url('bc20/header'); ?>" method="post" autocomplete="off" onsubmit="return false;" class="form uniformForm">
        <?= $MAXLENGTH ?>
        <input type="hidden" name="act" id="act" value="<?= $act; ?>" />
        <input type="hidden" name="HEADER[CAR]" id="CAR" value="<?= $aju; ?>" />
        <input type="hidden" name="HEADER[STATUS]" id="STATUS" value="<?= $HEADER['STATUS']; ?>" />
        <input type="hidden" name="HEADERDOK[NOMOR_AJU]" id="noajudok" value="<?= $aju; ?>" />
        <h4>
            <span id="off">
                <button id="" onclick="onoff(true);return false;" class="btn onterus"><span class="icon-pen"></span> Edit </button>
            </span>
            <span id="on">
                <button onclick="save_header('#fbc20_');
                        onoff(false);" class="btn onterus"><span class="icon-download"></span> Simpan</button>
                <button onclick="onoff(false);" class="btn onterus"><span class="icon-undo"></span> Batal</button>
            </span>
            <span class="msgheader_" style="margin-left:20px">&nbsp;</span>
            <?php if ($HEADER['CAR']) { ?>
                <button id="btnQUEUED" onclick="gotoQUEUED($('#CAR').val(),'<?= $TIPE_TRADER;?>',$('#STATUS').val())" class="btn onterus" style="float: right; margin-right: 5px;" ><span class="icon-mail"></span> Bentuk / Batal Edifact</button>
<?php } ?>
        </h4>
        <table width="100%" border="0">
            <tr>
                <td width="50%" valign="top">
                    <table width="100%" border="0">
<?php if ($HEADER['CAR']) { ?>
                            <tr>
                                <td height="25px;">Nomor Aju</td>
                                <td><b><?= substr($HEADER['CAR'], 0, 6) . '-' . substr($HEADER['CAR'], 6, 6) . '-' . substr($HEADER['CAR'], 12, 8) . '-' . substr($HEADER['CAR'], 20, 6); ?></b></td>
                            </tr>
<?php } ?>
                        <tr>
                            <td width="25%">KPBC Pendaftaran </td>
                            <td width="75%">
                                <input type="text"  name="HEADER[KDKPBC]" id="KDKPBC" value="<?= $HEADER['KDKPBC']; ?>" onblur="checkCode(this);" grp="kpbc" urai="URKPBC" class="ssstext" maxlength="6" /> &nbsp;
                                <button onclick="tb_search('kpbc', 'KDKPBC;URKPBC', 'Kode Kpbc', this.form.id, 650, 400);" class="btn"> ... </button>
                                <span id="URKPBC"><?= $HEADER['URAIAN_KPBC'] == '' ? $URKANTOR_TUJUAN : $HEADER['URAIAN_KPBC']; ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td>A. Jenis PIB </td>
                            <td><?= form_dropdown('HEADER[JNPIB]', $JNPIB, $HEADER['JNPIB'], 'wajib="yes" id="JNPIB" class="mtext"'); ?></td>
                        </tr>
                        <tr>
                            <td>B. Jenis Impor</td>
                            <td><?= form_dropdown('HEADER[JNIMP]', $JNIMP, $HEADER['JNIMP'], 'wajib="yes" id="JNIMP" class="mtext" onblur="jkwaktu()" onchange="jkwaktu()"'); ?>&nbsp;
                                <span id='jangkawaktu'>Jangka Waktu : <input type="text" name="HEADER[JKWAKTU]" id="JKWAKTU" value="<?= $HEADER['JKWAKTU']; ?>" class="ltext" style="width:30px;"  maxlength="4" /> bulan</span>
                            </td>
                        </tr>
                        <tr>
                            <td>C. Cara Bayar</td>
                            <td><?= form_dropdown('HEADER[CRBYR]', $CRBYR, $HEADER['CRBYR'], 'wajib="yes" id="cara_bayar" class="mtext"'); ?></td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table width="100%">
                        <tr>
                            <td style="text-align: right;" >
                                <button id="btnValidasi" onclick="cekValidasi($('#CAR').val())" class="btn onterus"><span class="icon-check"></span> Periksa Kelengkapan [ <span id="URSTATUS"><?= $HEADER['URSTATUS']; ?></span> ] </button>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <fieldset>
                        <legend><i>Informasi Bea dan Cukai</i></legend>
                        <table width="100%">
                            <tr>
                                <td width="150px">Nomor Pendaftaran*</td>
                                <td>
                                    <input type="text" id="PIBNO" class="stext" value="<?= $HEADER['PIBNO']; ?>" maxlength="6" readonly="readonly"/>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td>Tanggal Pendaftaran*</td>
                                <td>
                                    <input type="text" id="PIBTG" class="sstext" value="<?= $HEADER['PIBTG']; ?>" readonly="readonly"/> YYYY-MM-DD</td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
        <div class="judul">DATA PEMBERITAHUAN :</div>
        <table width="100%" border="0" >
            <tr>
                <td width="50%" valign="top">
                    <fieldset>
                        <legend>Data Pemasok</legend>
                        <table width="100%">
                            <tr>
                                <td width="100px">1. Nama </td>
                                <td>
                                    <input wajib="yes" type="text" name="HEADER[PASOKNAMA]" id="PASOKNAMA" value="<?= $HEADER['PASOKNAMA']; ?>" url="<?= site_url('referensi/autoComplate/PEMASOK'); ?>" urai="PASOKNAMA;PASOKALMT;PASOKNEG;URPASOKNEG" onfocus="Autocomp(this.id)" class="ltext"/>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top">2. Alamat </td>
                                <td>
                                    <textarea name="HEADER[PASOKALMT]" id="PASOKALMT" wajib="yes" class="ltext" onkeyup="limitChars(this.id, 70, 'limitAlamatPemasok')"><?= $HEADER['PASOKALMT']; ?></textarea> <div id="limitAlamatPemasok"></div></td>
                            </tr>
                            <tr>
                                <td>3. Negara Asal </td>
                                <td>
                                    <input type="text" name="HEADER[PASOKNEG]" id="PASOKNEG" value="<?= $HEADER['PASOKNEG']; ?>" onblur="checkCode(this);" grp="negara" urai="URPASOKNEG" class="sssstext" wajib="yes"/> &nbsp;
                                    <button onclick="tb_search('negara', 'PASOKNEG;URPASOKNEG', 'Kode Negara', this.form.id, 650, 400)" class="btn"> ... </button>
                                    <span id="URPASOKNEG"><?= $HEADER['URPASOKNEG']; ?></span>
                                </td>
                            </tr>
                        </table>
                    </fieldset>

                    <fieldset>
                        <legend>Data Importir</legend>
                        <table width="100%">
                            <tr>
                                <td width="100px">4. Identitas</td>
                                <td>
<?= form_dropdown('HEADER[IMPID]', $IMPID, $HEADER['IMPID'], 'wajib="yes" id="IMPID" class="smtext" ' . $ro); ?> <input type="text" name="HEADER[IMPNPWP]" id="IMPNPWP" url="<?= site_url('referensi/autoComplate/IMPORTIR/IMPNPWP'); ?>" urai="IMPID;IMPNPWP;IMPNAMA;IMPALMT;APIKD;APINO" onfocus="Autocomp(this.id);unformatNPWP(this.id);" onblur="formatNPWP(this.id);" value="<?= $this->fungsi->FORMATNPWP($HEADER['IMPNPWP']); ?>" class="smtext" maxlength="15" wajib="yes" <?= $ro; ?>/>
                                </td>
                            </tr> 
                            <tr>
                                <td>5. Nama</td>
                                <td>
                                    <input type="text" name="HEADER[IMPNAMA]" id="IMPNAMA" value="<?= $HEADER['IMPNAMA']; ?>" class="ltext" wajib="yes" urai="IMPID;IMPNPWP;IMPNAMA;IMPALMT;APIKD;APINO" onfocus="Autocomp(this.id);unformatNPWP(this.id);" url="<?= site_url('referensi/autoComplate/IMPORTIR/IMPNAMA'); ?>" <?= $ro; ?>/>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top" >6. Alamat</td>
                                <td>
                                    <textarea name="HEADER[IMPALMT]" id="IMPALMT" class="ltext" wajib="yes" onkeyup="limitChars(this.id, 70, 'limitIMPALMT')" <?= $ro; ?>><?= $HEADER['IMPALMT']; ?></textarea>
                                    <div id="limitIMPALMT"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>7. Status </td>
                                <td><?php
                                    $impstatus = explode(",", $HEADER['IMPSTATUS']);
                                    foreach ($impstatus as $val) {
                                        $arr[$val] = 'checked';
                                        //print_r($impstatus);die();
                                    }
                                    ?>
                                    <table width="100%">
                                        <tr>
                                            <td><input type="checkbox" name="HEADER[IMPSTATUS][1]" id="IMPSTATUS[1]" value="1" <?= $arr[1] ?> /> <label for="IMPSTATUS[1]">&nbsp;Importir Umum</label></td>
                                            <td><input type="checkbox" name="HEADER[IMPSTATUS][4]" id="IMPSTATUS[4]" value="4" <?= $arr[4] ?>/> <label for="IMPSTATUS[4]">&nbsp;Agen Tunggal</label></td>
                                            <td><input type="checkbox" name="HEADER[IMPSTATUS][7]" id="IMPSTATUS[7]" value="7" <?= $arr[7] ?>/> <label for="IMPSTATUS[7]">&nbsp;DAHANA</label></td>
                                        </tr>
                                        <tr>
                                            <td ><input type="checkbox" name="HEADER[IMPSTATUS][2]" id="IMPSTATUS[2]" value="2" <?= $arr[2] ?>/> <label for="IMPSTATUS[2]">&nbsp;Importir Produser</label></td>
                                            <td ><input type="checkbox" name="HEADER[IMPSTATUS][5]" id="IMPSTATUS[5]" value="5" <?= $arr[5] ?>/> <label for="IMPSTATUS[5]">&nbsp;BULOG</label></td>
                                            <td ><input type="checkbox" name="HEADER[IMPSTATUS][8]" id="IMPSTATUS[8]" value="8" <?= $arr[8] ?>/> <label for="IMPSTATUS[8]">&nbsp;IPTN</label></td>
                                        </tr>
                                        <tr>
                                            <td ><input type="checkbox" name="HEADER[IMPSTATUS][3]" id="IMPSTATUS[3]" value="3" <?= $arr[3] ?>/> <label for="IMPSTATUS[3]">&nbsp;Importir Terdaftar</label></td>
                                            <td ><input type="checkbox" name="HEADER[IMPSTATUS][6]" id="IMPSTATUS[6]" value="6" <?= $arr[6] ?>/> <label for="IMPSTATUS[6]">&nbsp;PERTAMINA</label></td>
                                            <td >&nbsp;</td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>8. APIP / APIU</td>
                                <td>
<?= form_dropdown('HEADER[APIKD]', $APIKD, $HEADER['APIKD'], 'id="APIKD" class="smtext" '); ?>
                                    <input type="text" name="HEADER[APINO]" id="APINO" value="<?= $HEADER['APINO'] ?>" url="<?= site_url(); ?>/autocomplete/importir" class="smtext" maxlength="15"/> <button onclick="javascript : indentorXX('INDID;INDNPWP;INDNAMA;INDALMT', this.form.id);" class="btn" >Indentor</button>
                                </td>
                            </tr>
                        </table>
                        <input type="hidden" name="HEADER[INDID]" id="INDID" value="<?= $HEADER['INDID']?>">
                        <input type="hidden" name="HEADER[INDNPWP]" id="INDNPWP" value="<?= $HEADER['INDNPWP']?>">
                        <input type="hidden" name="HEADER[INDNAMA]" id="INDNAMA" value="<?= $HEADER['INDNAMA']?>">
                        <input type="hidden" name="HEADER[INDALMT]" id="INDALMT" value="<?= $HEADER['INDALMT']?>">
                    </fieldset>
<?php if ($TIPE_TRADER == '2') { ?>
                        <fieldset>
                            <legend>Data PPJK</legend>
                            <table width="100%">
                                <tr>
                                    <td width="100px">9. Identitas</td>
                                    <td>
    <?= form_dropdown('HEADER[PPJKID]', $IMPID, $HEADER['PPJKID'], 'wajib="yes" id="PPJKID" class="smtext" readonly'); ?> <input type="text" name="HEADER[PPJKNPWP]" id="PPJKNPWP" onfocus="unformatNPWP(this.id);" onblur="formatNPWP(this.id);"  value="<?= $this->fungsi->FORMATNPWP($HEADER['PPJKNPWP']); ?>" class="smtext" maxlength="15" wajib="yes" readonly/>
                                    </td>
                                </tr> 
                                <tr>
                                    <td>10. Nama</td>
                                    <td><input type="text" name="HEADER[PPJKNAMA]" id="PPJKNAMA" value="<?= $HEADER['PPJKNAMA']; ?>" class="ltext" wajib="yes" readonly/></td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top">11. Alamat</td>
                                    <td><textarea name="HEADER[PPJKALMT]" id="PPJKALMT" class="ltext" wajib="yes" onkeyup="limitChars(this.id, 70, 'limitPPJKALMT')" readonly><?= $HEADER['PPJKALMT']; ?></textarea><div id="limitPPJKALMT"></div></td>
                                </tr>
                                <tr>
                                    <td>12. No/Tgl PPJK</td>
                                    <td><input type="text" name="HEADER[PPJKNO]" id="PPJKNO" value="<?= $HEADER['PPJKNO']; ?>" class="smtext" readonly/> <input type="text" name="HEADER[PPJKTG]" id="PPJKTG" value="<?= $HEADER['PPJKTG']; ?>" class="sstext" formatDate="yes" /></td>
                                </tr>
                            </table>
                        </fieldset>
<?php } ?>
                    <fieldset>
                        <legend>Data Sarana Angkut</legend>
                        <table width="100%">
                            <tr>
                                <td width="100px">13. Moda - Nama</td>
                                <td>
                                    <?= form_dropdown('HEADER[MODA]', $MODA, $HEADER['MODA'], 'onchange="BLAWB($(this).val());" wajib="yes"  id="MODA" class="stext"'); ?>
                                    <input type="text" name="HEADER[ANGKUTNAMA]" id="ANGKUTNAMA" value="<?= $HEADER['ANGKUTNAMA']; ?>" wajib="yes" class="mtext" url="<?= site_url('referensi/autoComplate/MODA/ANGKUTNAMA'); ?>" urai="MODA;ANGKUTNAMA;ANGKUTFL;URANGKUTFL" onfocus="Autocomp(this.id);" />
                                </td>
                            </tr>
                            <tr>
                                <td>14. No. Voy/Flight </td>
                                <td>
                                    <input type="text" name="HEADER[ANGKUTNO]" id="ANGKUTNO" value="<?= $HEADER['ANGKUTNO']; ?>" wajib="yes" class="stext" maxlength="7" />
                                </td>
                            </tr>
                            <tr>
                                <td>15. Bendera</td>
                                <td>
                                    <input type="text" name="HEADER[ANGKUTFL]" id="ANGKUTFL" value="<?= $HEADER['ANGKUTFL']; ?>" class="sssstext" onblur="checkCode(this);" grp="negara" urai="URANGKUTFL"/> 
                                    <button onclick="tb_search('negara', 'ANGKUTFL;URANGKUTFL', 'Kode Negara', this.form.id, 650, 400)" class="btn"> ... </button>
                                    <span id="URANGKUTFL"><?= $HEADER['URANGKUTFL']; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>16. Perkiraan Tgl Tiba&nbsp;</td>
                                <td>
                                    <input type="text" name="HEADER[TGTIBA]" id="TGTIBA" value="<?= $HEADER['TGTIBA'] ?>" class="sstext"  formatDate="yes"> DD-MM-YYYY
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                    <fieldset class="dataReferensi">
                        <legend><b>Kontainer / Peti Kemas</b></legend>
                        <?= $data_container; ?>
                    </fieldset>
                </td>
                <td valign="top">
                    <fieldset>
                        <legend>Data Dokumen</legend>
                        <table width="100%">
                            <tr>
                                <td width="125px">17. Dokumen Penutup </td>
                                <td>
<?= form_dropdown('HEADER[DOKTUPKD]', $DOKTUPKD, $HEADER['DOKTUPKD'], 'id="DOKTUPKD" class="stext" style="width:100px"'); ?>
                                    <input type="text" name="HEADER[DOKTUPNO]" id="DOKTUPNO" value="<?= $HEADER['DOKTUPNO'] ?>" class="sstext" maxlength="6" format="angka"/>
                                    <input type="text" name="HEADER[DOKTUPTG]" id="DOKTUPTG" value="<?= $HEADER['DOKTUPTG'] ?>" class="sstext" formatDate="yes">
                                </td>
                            </tr>
                            <tr>
                                <td>18. Nomor Pos&nbsp;</td>
                                <td>
                                    <input type="text" name="HEADER[POSNO]" id="POSNO" value="<?= $HEADER['POSNO'] ?>" class="ssstext" format="angka" >&nbsp;&nbsp;Sub Pos &nbsp;
                                    <input type="text" name="HEADER[POSSUB]" id="POSSUB" value="<?= $HEADER['POSSUB'] ?>" class="ssstext" format="angka">&nbsp;&nbsp;Sub-Sub Pos
                                    <input type="text" name="HEADER[POSSUBSUB]" id="POSSUBSUB" value="<?= $HEADER['POSSUBSUB'] ?>" class="ssstext" format="angka">
                                </td>
                            </tr>
                            <tr>
                                <td>19. SKEP Fasilitas</td>
                                <td>
                                    <input type="text" name="HEADER[KDFAS]" id="KDFAS" value="<?= $HEADER['KDFAS']; ?>" wajib="yes" class="ssstext" onblur="checkCode(this);" grp="fasilitas" urai="URKDFAS" />&nbsp;
                                    <button onclick="tb_search('skep', 'KDFAS;URKDFAS', 'Kode Fasilitas', this.form.id, 650, 400)" class="btn"> ... </button>
                                    <span id="URKDFAS"><?= $HEADER['URKDFAS'] == '' ? $URKDFAS : $HEADER['URKDFAS']; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>20. Tmp Penimbunan</td>
                                <td>
                                    <input type="text" name="HEADER[TMPTBN]" id="TMPTBN" value="<?= $HEADER['TMPTBN']; ?>" url="<?= site_url('referensi/autoComplate/TIMBUN/KDGDG'); ?>" onfocus="Autocomp(this.id, $('#KDKPBC').val())" urai="TMPTBN;URTMPTBN" class="ssstext" wajib="yes" maxlength="4"/>&nbsp;
                                    <button onclick="tb_search('timbun', 'TMPTBN;URTMPTBN', 'Kode Timbun', this.form.id, 650, 400, 'KDKPBC;')" class="btn"> ... </button>
                                    <span id="URTMPTBN"><?= $HEADER['URTMPTBN']; ?></span>
                                </td>
                            </tr>
                            <tr class="dataReferensi">
                                <td colspan="2"><br />
                                    <h4><span class="info_2">&nbsp;</span>Detil Dokumen</h4>
<?= $data_dokumen; ?>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                    <fieldset>
                        <legend>Data Harga</legend>
                        <table width="100%">
                            <tr>
                                <td width="125px">21. Valuta </td>
                                <td colspan="3">
                                    <input type="text" name="HEADER[KDVAL]" id="KDVAL" value="<?= $HEADER['KDVAL']; ?>" onblur="checkCode(this);" grp="valuta" urai="URKDVAL" class="ssstext" wajib="yes" maxlength="3" />
                                    <button onclick="tb_search('valuta', 'KDVAL;URKDVAL', 'Kode Valuta', this.form.id, 650, 400)" class="btn"> ... </button> <span id="URKDVAL"><?= $HEADER['URKDVAL']; ?></span>
                                </td>                                    
                            </tr>
                            <tr>
                                <td width="23%">22. NDPBM (Kurs) </td>
                                <td width="37%">
                                    <input type="text" name="nilai_ndpbm" id="nilai_ndpbm" class="stext" align="right" value="<?= number_format($HEADER['NDPBM'], 4); ?>" wajib="yes" onkeyup="this.value = ThausandSeperator('NDPBM', this.value, 4);prosesHargaHeader('fbc20_')" style="text-align:right;width:100px;"/>
                                    <input type="hidden" name="HEADER[NDPBM]" id="NDPBM" value="<?= $HEADER['NDPBM'] ?>" />
                                </td>
                                <td width="15%"><span id="22">23. FOB</span></td>
                                <td width="25%">
                                    <input type="text" name="FOBUR" id="FOBUR" value="<?= number_format($HEADER['FOB'], 2); ?>" class="stext" style="text-align:right;width:100px;" readonly="readonly">
                                    <input type="hidden" name="HEADER[FOB]" id="FOB" value="<?= $HEADER['FOB'] ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td>24. Freight</td>
                                <td>
                                    <input type="text" name="FREIGHTUR" id="FREIGHTUR" value="<?= number_format($HEADER['FREIGHT'], 2); ?>" class="stext" style="text-align:right;width:100px;" readonly="readonly" />
                                    <input type="hidden" name="HEADER[FREIGHT]" id="FREIGHT" value="<?= $HEADER['FREIGHT'] ?>" />
                                </td>
                                <td>25. Asuransi</td>
                                <td>
                                    <input type="text" name="ASURANSIUR" id="ASURANSIUR" value="<?= number_format($HEADER['ASURANSI'], 2); ?>" class="stext" style="text-align:right;width:100px;" readonly="readonly"/>
                                    <input type="hidden" name="HEADER[ASURANSI]" id="ASURANSI" value="<?= $HEADER['ASURANSI'] ?>" /></td>
                            </tr>
                            <tr>
                                <td>26. Nilai CIF </td>
                                <td>
                                    <input type="text" name="CIFUR" id="CIFUR" value="<?= number_format($HEADER['CIF'], 2); ?>" class="stext" style="text-align:right;width:100px;" readonly="readonly">
                                    <input type="hidden" name="HEADER[CIF]" id="CIF" value="<?= $HEADER['CIF'] ?>" />
                                </td>
                                <td>27. CIF (Rp)</td>
                                <td>
                                    <input type="text" name="HEADER[CIFRP]" id="CIFRP" value="<?= number_format($HEADER['CIFRP'], 2); ?>" class="stext" style="text-align:right;width:100px;" readonly="readonly" >
                                </td>
                            </tr>
                            <tr>
                                <td>28. Bruto </td>
                                <td colspan="3">
                                    <input type="text" name="BRUTOUR" id="BRUTOUR" value="<?= number_format($HEADER['BRUTO'], 2); ?>" class="sstext" wajib="yes" format="angka" onkeyup="this.value = ThausandSeperator('BRUTO', this.value, 2);" style="text-align:right;"/> Kilogram (KGM)
                                    <input type="hidden" name="HEADER[BRUTO]" id="BRUTO" value="<?= $HEADER['BRUTO'] ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td>29. Netto </td>
                                <td colspan="3">
                                    <input type="text" name="NETTOUR" id="NETTOUR" value="<?= number_format($HEADER['NETTO'], 2); ?>" class="sstext" wajib="yes" format="angka" onkeyup="this.value = ThausandSeperator('NETTO', this.value, 2);" style="text-align:right;"/> Kilogram (KGM)
                                    <input type="hidden" name="HEADER[NETTO]" id="NETTO" value="<?= $HEADER['NETTO']; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" >
                                    <input type="hidden" name="HEADER[KDASS]" id="KDASS" value="<?= $HEADER[KDASS]; ?>" />
                                    <input type="hidden" name="HEADER[BTAMBAHAN]" id="BTAMBAHAN" value="<?= $HEADER[BTAMBAHAN]; ?>" />
                                    <input type="hidden" name="HEADER[DISCOUNT]" id="DISCOUNT" value="<?= $HEADER[DISCOUNT]; ?>" />
                                    <input type="hidden" name="HEADER[KDHRG]" id="KDHRG" value="<?= $HEADER[KDHRG]; ?>" />
                                    <input type="hidden" name="HEADER[NILINV]" id="NILINV" value="<?= $HEADER[NILINV]; ?>" />
                                </td>
                            </tr>

                            <tr>
                                <td colspan="4" align="center"><br />
                                    <button onclick="EditHarga('NDPBM;KDVAL;CIF;FOB;FREIGHT;ASURANSI;KDHRG;BTAMBAHAN;DISCOUNT;KDASS;CIFRP;NILINV', this.form.id)" class="btn"> Edit Harga </button>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                    <fieldset>
                        <legend>Data Pelabuhan</legend>
                        <table width="100%">
                            <tr>
                                <td width="25%">30. Muat </td>
                                <td width="75%">
                                    <input type="text" name="HEADER[PELMUAT]" id="PELMUAT" value="<?= $HEADER['PELMUAT']; ?>" onblur="checkCode(this);" grp="pelabuhan" urai="PELMUATUR" class="ssstext" wajib="yes" maxlength="5"/>&nbsp;
                                    <button onclick="tb_search('pelabuhan', 'PELMUAT;PELMUATUR', 'Kode Pelabuhan', this.form.id, 650, 400)" class="btn"> ... </button>
                                    <span id="PELMUATUR"><?= $HEADER['PELMUATUR'] == '' ? $PELMUATUR : $HEADER['PELMUATUR']; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>31. Transit</td>
                                <td>
                                    <input type="text" name="HEADER[PELTRANSIT]" id="PELTRANSIT" onblur="checkCode(this);" grp="pelabuhan" urai="PELTRANSITUR" class="ssstext" value="<?= $HEADER['PELTRANSIT']; ?>" maxlength="5"/>&nbsp;
                                    <button onclick="tb_search('pelabuhan', 'PELTRANSIT;PELTRANSITUR', 'Kode Pelabuhan', this.form.id, 650, 400)" class="btn"> ... </button>
                                    <span id="PELTRANSITUR"><?= $HEADER['PELTRANSITUR'] == '' ? $PELTRANSITUR : $HEADER['PELTRANSITUR']; ?></span></td>
                            </tr>
                            <tr>
                                <td>32. Bongkar </td>
                                <td>
                                    <input type="text" name="HEADER[PELBKR]" id="PELBKR" value="<?= $HEADER['PELBKR']; ?>" onblur="checkCode(this);" grp="pelabuhan" urai="PELBKRUR" class="ssstext" wajib="yes" maxlength="5"/>&nbsp; <button onclick="tb_search('pelabuhan', 'PELBKR;PELBKRUR', 'Kode Pelabuhan', this.form.id, 650, 400)" class="btn" > ... </button>
                                    <span id="PELBKRUR"><?= $HEADER['PELBKRUR'] == '' ? $PELBKRUR : $HEADER['PELBKRUR']; ?></span>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                    <fieldset class="dataReferensi">
                        <legend><b>Jumlah dan Jenis Kemasan</b></legend>
                        <?= $data_kemasan; ?>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td colspan="2"><br>
                    <h4>DATA PUNGUTAN</h4>
                    <table border="0" width="750px" cellpadding="0" cellspacing="0" class="table-bordered">
                        <thead>
                            <tr>
                                <td width="22%" align="center" style="height: 30px;">
                                    <button onclick="detilPungutan($('#CAR').val())" class="btn onterus"><span class="icon-list"></span> Jenis Pungutan </button>
                                </td>
                                <td width="17%" align="center">Dibayar (Rp) </td>
                                <td width="24%" align="center">Ditangguhkan Pemerintah (Rp) </td>
                                <td width="18%" align="center">Ditangguhkan (Rp)</td>
                                <td width="19%" align="center">Dibebaskan (Rp)</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="padding-left:10px">BM</td>
                                <td align="right"><?= number_format($data_pgt[1][0]) ?>&nbsp;</td>
                                <td align="right"><?= number_format($data_pgt[1][1]) ?>&nbsp;</td>
                                <td align="right"><?= number_format($data_pgt[1][2]) ?>&nbsp;</td>
                                <td align="right"><?= number_format($data_pgt[1][4]) ?>&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="padding-left:10px">Cukai</td>
                                <td align="right" ><?= number_format($data_pgt[5][0]) ?>&nbsp;</td>
                                <td align="right" ><?= number_format($data_pgt[5][1]) ?>&nbsp;</td>
                                <td align="right" ><?= number_format($data_pgt[5][2]) ?>&nbsp;</td>
                                <td align="right" ><?= number_format($data_pgt[5][4]) ?>&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="padding-left:10px;" class="border-brl">PPN</td>
                                <td align="right"><?= number_format($data_pgt[2][0]) ?>&nbsp;</td>
                                <td align="right"><?= number_format($data_pgt[2][1]) ?>&nbsp;</td>
                                <td align="right"><?= number_format($data_pgt[2][2]) ?>&nbsp;</td>
                                <td align="right"><?= number_format($data_pgt[2][4]) ?>&nbsp;</td>
                            </tr>
                            <tr>

                                <td style="padding-left:10px;" class="border-brl">PPnBM</td>
                                <td align="right"><?= number_format($data_pgt[3][0]) ?>&nbsp;</td>
                                <td align="right"><?= number_format($data_pgt[3][1]) ?>&nbsp;</td>
                                <td align="right"><?= number_format($data_pgt[3][2]) ?>&nbsp;</td>
                                <td align="right"><?= number_format($data_pgt[3][4]) ?>&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="padding-left:10px;" class="border-brl">PPh</td>
                                <td align="right"><?= number_format($data_pgt[4][0]) ?>&nbsp;</td>
                                <td align="right"><?= number_format($data_pgt[4][1]) ?>&nbsp;</td>
                                <td align="right"><?= number_format($data_pgt[4][2]) ?>&nbsp;</td>
                                <td align="right"><?= number_format($data_pgt[4][4]) ?>&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="padding-left:10px;" class="border-brl"><b>TOTAL</b></td>
                                <td align="right"><b><?= number_format($data_pgt['TOTAL'][0]) ?>&nbsp;</b></td>
                                <td align="right"><b><?= number_format($data_pgt['TOTAL'][1]) ?>&nbsp;</b></td>
                                <td align="right"><b><?= number_format($data_pgt['TOTAL'][2]) ?>&nbsp;</b></td>
                                <td align="right"><b><?= number_format($data_pgt['TOTAL'][4]) ?>&nbsp;</b></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <table width="50%" border="0" align="center" cellpadding="5">
                        <tr><td colspan="2" class="rowheight" align="center"><h5>DATA PENANDATANGAN DOKUMEN</h5></td></tr>
                        <tr align="center">
                            <td width="50%">TEMPAT</td>
                            <td width="50%">TANGGAL (YYYY-MM-DD)</td>
                        </tr>
                        <tr align="center">
                            <td><input type="text" name="HEADER[KOTA_TTD]" id="tempat_ttd" value="<?= $HEADER['KOTA_TTD']; ?>" class="stext" style="text-align:center; width:150px" readonly="readonly" /></td>
                            <td><input type="text" name="HEADER[TANGGAL_TTD]" id="tanggal_ttd" value="<?= $HEADER['TANGGAL_TTD']; ?>" class="sstext" style="text-align:center; width:150px" readonly="readonly"/></td>
                        </tr>
                        <tr align="center">
                            <td colspan="2">NAMA</td>
                        </tr>
                        <tr align="center"><td colspan="2"><input type="text" name="HEADER[NAMA_TTD]" id="nama_ttd" value="<?= $HEADER['NAMA_TTD']; ?>" class="stext" style="text-align:center; width:150px" readonly="readonly"/></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</span>
<script>
    $(document).ready(function () {
        jkwaktu();
        declareDP('fbc20_');
        onoff(false);
        prosesHargaHeader('fbc20_');
        BLAWB($('#MODA').val());
        f_MAXLENGTH('fbc20_');
    });
    function indentorXX(inpField,frmId){
        var id = inpField.split(";");
        var data = "";
        for (var a = 0; a < id.length; a++) {
            data += ';' + id[a] + '|' + $('#' + frmId + ' #' + id[a]).val();
        }
        data = data.substr(1);
        DialogPost(site_url + '/bc20/indentor', 'divIndentor', '.: Data Pemilik Barang / Indentor / QQ :.', 480, 210, data);
    }
</script>