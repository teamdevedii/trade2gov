<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
?>
<div class="content_luar">
    <div class="content_dalam">
        <form id="cetak" method="post" autocomplete="off" action="<?= site_url('bc20/setSSPCP'); ?>" onsubmit="return false;" >
            <input id="CAR" name="SSPCP[CAR]" type="hidden" value="<?= $car ?>">
            <input id="IMPNPWP" name="SSPCP[IMPNPWP]" type="hidden" value="<?= $SSPCP['IMPNPWP'] ?>">
            <fieldset>
                <legend> Data SSPCP (Bisa diubah sesuai keperluan) </legend>
                <table width="100%">
                    <tr>
                        <td width="150px">Kantor Pabean</td>
                        <td>
                            <input type="text" name="SSPCP[KDKPBC]" id="KDKPBC" value="<?= $SSPCP['KDKPBC']; ?>" url="<?= site_url('referensi/autoComplate/KPBC'); ?>" urai="KDKPBC;URKPBC" wajib="yes" onfocus="Autocomp(this.id)" class="ssstext" maxlength="6"/>
                            <input type="button" name="cari" id="cari" class="button" onclick="tb_search('kpbc', 'KDKPBC;URKPBC', 'Kode Kpbc', 'cetak_', 650, 400)" value="...">&nbsp;
                            <span id="URKPBC"><?= $SSPCP['URKPBC']; ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td>NPWP Kantor Pabean</td>
                        <td>
                            <input type="text" name="SSPCP[NPWP]" id="NPWP" value="<?= $SSPCP['NPWP']; ?>" onfocus="unformatNPWP(this.id);" onblur="formatNPWP(this.id);" class="smtext" maxlength="15"/> Masa Pajak <input type="text" name="SSPCP[MASAPAJAK]" id="MASAPAJAK" value="<?= $SSPCP['MASAPAJAK']; ?>" class="sssstext" maxlength="2" style="text-align: center" /> Tahun Pajak <input type="text" name="SSPCP[TAHUNPAJAK]" id="TAHUNPAJAK" value="<?= $SSPCP['TAHUNPAJAK']; ?>" class="ssstext" maxlength="4" style="text-align: center"/>
                        </td>
                    </tr> 
                    <tr>
                        <td>Nama</td>
                        <td>
                            <input type="text" name="SSPCP[NAMA]" id="NAMA" value="<?= $SSPCP['NAMA']; ?>" class="ltext" maxlength="50"/> Kode Pos <input type="text" name="SSPCP[KDPOS]" id="KDPOS" value="<?= $SSPCP['KDPOS']; ?>" class="ssstext" maxlength="6"/>
                        </td>
                    </tr> 
                    <tr>
                        <td colspan="2" ><hr style="margin-bottom: 10px;" ></td>
                    </tr> 
                    <tr>
                        <td colspan="2" class="table-bordered">
                            <table width="100%">
                                <tr>
                                    <td width="60%">Bea Masuk (BM)</td>
                                    <td width="15%" style="text-align: center;">412111</td>
                                    <td width="25%" style="text-align: center;"><input type="text" name="SSPCPAKUN[412111]" id="412111" value="<?= $SSPCP['412111']; ?>" class="smtext numtext" maxlength="20"/></td>
                                </tr>
                                <tr>
                                    <td>Bea Masuk Ditanggung Pemerintah atas Hibah (SPM) Nihil</td>
                                    <td style="text-align: center;">412112</td>
                                    <td style="text-align: center;"><input type="text" name="SSPCPAKUN[412112]" id="412112" value="<?= $SSPCP['412112']; ?>" class="smtext numtext" maxlength="20"/></td>
                                </tr>
                                <tr>
                                    <td>Bea Masuk Dalam Rangka Kemudahan Impor Tujuan Ekspor (KITE)</td>
                                    <td style="text-align: center;">412114</td>
                                    <td style="text-align: center;"><input type="text" name="SSPCPAKUN[412114]" id="412114" value="<?= $SSPCP['412114']; ?>" class="smtext numtext" maxlength="20"/></td>
                                </tr>
                                <tr>
                                    <td>Denda Administrasi Pabean</td>
                                    <td style="text-align: center;">412113</td>
                                    <td style="text-align: center;"><input type="text" name="SSPCPAKUN[412113]" id="412113" value="<?= $SSPCP['412113']; ?>" class="smtext numtext" maxlength="20"/></td>
                                </tr>
                                <tr>
                                    <td>Denda Administrasi Atas Pengangkutan Barang Tertentu</td>
                                    <td style="text-align: center;">412115</td>
                                    <td style="text-align: center;"><input type="text" name="SSPCPAKUN[412115]" id="412115" value="<?= $SSPCP['412115']; ?>" class="smtext numtext" maxlength="20"/></td>
                                </tr>
                                <tr>
                                    <td>Pendapatan Pabean Lainnya</td>
                                    <td style="text-align: center;">412119</td>
                                    <td style="text-align: center;"><input type="text" name="SSPCPAKUN[412119]" id="412119" value="<?= $SSPCP['412119']; ?>" class="smtext numtext" maxlength="20"/></td>
                                </tr>
                                <tr>
                                    <td>Bea Keluar</td>
                                    <td style="text-align: center;">412211</td>
                                    <td style="text-align: center;"><input type="text" name="SSPCPAKUN[412211]" id="412211" value="<?= $SSPCP['412211']; ?>" class="smtext numtext" maxlength="20"/></td>
                                </tr>
                                <tr>
                                    <td>Denda Administrasi Bea Keluar</td>
                                    <td style="text-align: center;">412212</td>
                                    <td style="text-align: center;"><input type="text" name="SSPCPAKUN[412212]" id="412212" value="<?= $SSPCP['412212']; ?>" class="smtext numtext" maxlength="20"/></td>
                                </tr>
                                <tr>
                                    <td>Bunga Bea Keluar</td>
                                    <td style="text-align: center;">412213</td>
                                    <td style="text-align: center;"><input type="text" name="SSPCPAKUN[412213]" id="412213" value="<?= $SSPCP['412213']; ?>" class="smtext numtext" maxlength="20"/></td>
                                </tr>
                                <tr>
                                    <td>Cukai Hasil Tembakau</td>
                                    <td style="text-align: center;">411511</td>
                                    <td style="text-align: center;"><input type="text" name="SSPCPAKUN[411511]" id="411511" value="<?= $SSPCP['411511']; ?>" class="smtext numtext" maxlength="20"/></td>
                                </tr>
                                <tr>
                                    <td>Cukai Etil Alkohol</td>
                                    <td style="text-align: center;">411512</td>
                                    <td style="text-align: center;"><input type="text" name="SSPCPAKUN[411512]" id="411512" value="<?= $SSPCP['411512']; ?>" class="smtext numtext" maxlength="20"/></td>
                                </tr>
                                <tr>
                                    <td>Cukai Minuman Mengandung Etil Alkohol</td>
                                    <td style="text-align: center;">411513</td>
                                    <td style="text-align: center;"><input type="text" name="SSPCPAKUN[411513]" id="411513" value="<?= $SSPCP['411513']; ?>" class="smtext numtext" maxlength="20"/></td>
                                </tr>
                                <tr>
                                    <td>Pendapatan Cukai Lainnya</td>
                                    <td style="text-align: center;">411519</td>
                                    <td style="text-align: center;"><input type="text" name="SSPCPAKUN[411519]" id="411519" value="<?= $SSPCP['411519']; ?>" class="smtext numtext" maxlength="20"/></td>
                                </tr>
                                <tr>
                                    <td>Denda Administrasi Cukai</td>
                                    <td style="text-align: center;">411514</td>
                                    <td style="text-align: center;"><input type="text" name="SSPCPAKUN[411514]" id="411514" value="<?= $SSPCP['411514']; ?>" class="smtext numtext" maxlength="20"/></td>
                                </tr>
                                <tr>
                                    <td>PNBP / Pendapatan DJBC</td>
                                    <td style="text-align: center;">423216</td>
                                    <td style="text-align: center;"><input type="text" name="SSPCPAKUN[423216]" id="423216" value="<?= $SSPCP['423216']; ?>" class="smtext numtext" maxlength="20"/></td>
                                </tr>
                                <tr>
                                    <td>PPN Impor <span style="float:right;width: 50%;">NPWP ( <?= ((int)$SSPCP['411212']>0)?$this->actMain->formatNPWP($SSPCP['IMPNPWP']):'' ?> )</span></td>
                                    <td style="text-align: center;">411212</td>
                                    <td style="text-align: center;"><input type="text" name="SSPCPAKUN[411212]" id="411212" value="<?= $SSPCP['411212']; ?>" class="smtext numtext" maxlength="20"/></td>
                                </tr>
                                <tr>
                                    <td>PPN Hasil Tembakau / PPN Dalam Negeri</td>
                                    <td style="text-align: center;">411211</td>
                                    <td style="text-align: center;"><input type="text" name="SSPCPAKUN[411211]" id="411211" value="<?= $SSPCP['411211']; ?>" class="smtext numtext" maxlength="20"/></td>
                                </tr>
                                <tr>
                                    <td>PPnBM Impor <span style="float:right;width: 50%;">NPWP ( <?= ((int)$SSPCP['411222']>0)?$this->actMain->formatNPWP($SSPCP['IMPNPWP']):'' ?> )</span></td>
                                    <td style="text-align: center;">411222</td>
                                    <td style="text-align: center;"><input type="text" name="SSPCPAKUN[411222]" id="411222" value="<?= $SSPCP['411222']; ?>" class="smtext numtext" maxlength="20"/></td>
                                </tr>
                                <tr>
                                    <td>PPh Pasal 22 Impor <span style="float:right;width: 50%;">NPWP ( <?= ((int)$SSPCP['411123']>0)?$this->actMain->formatNPWP($SSPCP['IMPNPWP']):'' ?> )</span></td>
                                    <td style="text-align: center;">411123</td>
                                    <td style="text-align: center;"><input type="text" name="SSPCPAKUN[411123]" id="411123" value="<?= $SSPCP['411123']; ?>" class="smtext numtext" maxlength="20"/></td>
                                </tr>
                                <tr>
                                    <td>Bunga Penagihan PPN</td>
                                    <td style="text-align: center;">411622</td>
                                    <td style="text-align: center;"><input type="text" name="SSPCPAKUN[411622]" id="411622" value="<?= $SSPCP['411622']; ?>" class="smtext numtext" maxlength="20"/></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" ><hr style="margin-bottom: 10px;" ></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                             <table width="100%">
                                <tr>
                                    <td width="15%">NTB / NTP</td>
                                    <td width="35%"><input type="text" name="SSPCP[NTB]" id="NTB" value="<?= $SSPCP['NTB']; ?>" wajib="yes" class="smtext" maxlength="30"/></td>
                                    <td width="15%">NTPN</td>
                                    <td width="35%"><input type="text" name="SSPCP[NTPN]" id="NTPN" value="<?= $SSPCP['NTPN']; ?>" wajib="yes" class="smtext" maxlength="30"/></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" >
                           <button onclick="cetakSSPCPNewWin(this.form.id);" class="btn"><span class="icon-new-window"></span> Cetak </button>
                        </td>
                    </tr>
                </table>
                
            </fieldset>
        </form>
    </div>
</div>
<script>
    function cetakSSPCPNewWin(id){
        var dataSend = $('#'+id).serialize();
        $.ajax({
            type: 'POST',
            url: $('#'+id).attr('action'),
            data: dataSend,
            success: function(data) {
                window.open(site_url+"/bc20/cetak/"+$('#CAR').val()+'/2',"Cetak Dokumen PIB","scrollbars=yes, resizable=yes,width=1000,height=700");
            }

        });
    return false;
    }
</script>