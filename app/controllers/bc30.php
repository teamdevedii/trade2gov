<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class bc30 extends CI_Controller {
    public function __construct()  {
        parent::__construct();
        $this->load->model('actBC30');
        if (!$this->newsession->userdata('LOGGED')) {
            $this->newsession->sess_destroy();
            echo '<script> window.location = site_url; </script>';exit();}
        return true;
    }
    
    function daftarPKBE($tipe = "") {
        $data = $this->actBC30->lsvHeaderPKBE($tipe = "");
        if ($this->input->post('ajax')) {
            echo $data;
        } else {
            echo $this->load->view('daftar', $data, true);
            exit();
        }
    }
    
    function createPKBE($car) {
        $data = $this->actBC30->getHeaderPKBE($car);
        echo $this->load->view('bc30/createPKBEBC30', $data, true);
        exit();
    }
    
    function lstDetilPKBE($car) {
        $data = $this->actBC30->detailPKBE($car);
        if ($this->input->post('ajax')) {
            echo $data;
        } else {
            echo $this->load->view('daftar', $data, true);
            exit();
        }
    }
    
    function insert_detail_pkbe($act, $car, $seriCont = '', $seriPE = '',$KDKTR) {
        if ($act == 'proses') {
            echo $this->detailPKBEin();
            die();
        } elseif ($act == 'viewform') {
            $dataForm = $this->actBC30->getFormPKBEDtl($car, $seriCont, $seriPE,$KDKTR);
            echo $this->load->view('bc30/frmdetailpkbe', $dataForm, true, '');
        } elseif ($act == 'delete') {
            $key = $this->input->post('tb_chkfdtlPKBE');
            echo $this->actBC30->delPKBEdtl($key);
            die();
        } 
    }
    
    function detailPKBEin() {
        $this->load->model("actbc30");
        $act = $this->input->post("act");
        if ($act == "add" || $act == "edit") {
            $rtn = $this->actBC30->setDetailPKBEin($act);
            echo($rtn);
        } else {
            echo $this->actbc30->setDetailPKBEin($act);
        }
        die();
    }
    
    function setHeaderPKBE() {
        $exe = $this->actBC30->setHeaderPKBEin();
        echo $exe;
        exit();
    }
    
    function delPKBEhdr(){
        //print_r('sini'); die();
        $key = $this->input->post('tb_chkfhdrPKBE');
            echo $this->actBC30->delPKBEhdr($key);
            die();
    }
    
    function alertcekstatus($car = "") {
        $this->load->model("actBC30");
        if ($car) {
            $data = $this->actBC30->validasiHDRPKBE($car);
        } else {
            $data = 'Cek status Error';
        }
        echo $data;
    }
    
    function refreshHeader($car) {
        $data = $this->actBC30->getHeaderPKBE($car);
        echo $this->load->view('bc30/createPKBEBC30', $data, true);
        exit();
    }
    
    function cariDtlPkbe(){
        $this->actBC30->getDetailPKBE();
        exit();
    }
    
    function queued() {
        $car = $this->input->post("CAR");
        //print_r($car);die();
        $FF = $this->actBC30->genFlatfile($car);
        $arrFF = explode('|', $FF);
        #print_r($arrFF); die('tset');
        if ($arrFF[0] == '2') {
            $SNRF = $this->actBC30->crtQueue($car);
            if ($SNRF!='ERROR') {
                $data = $SNRF . '|' . $arrFF[1];
                $SNRF = $this->actBC30->updateSNRF($car, $data);
                $hsl = "===========================================================================================<BR>"
                        . "Bentuk Edifact berhasil.<BR>";
            } else {
                $hsl = "===========================================================================================<BR>"
                        . "Bentuk Edifact atau Pembatalan Edifact gagal.";
            }
        }
        elseif ($arrFF[0] == '1') {
            $hsl = "===========================================================================================<BR>"
                    . "Batal Edifact berhasil.<BR>";
        }
        else {
            $hsl = "===========================================================================================<BR>"
                    . "Bentuk Edifact atau Pembatalan Edifact gagal.<BR>"
                    . "===========================================================================================<BR>"
                    . $arrFF[0]
                    . "<BR>Silakan validasi data kembali.";
        }
        die($hsl);
    }
    
    function beforecetakPKBE($car) {
        echo $this->load->view('bc30/beforeCetakPKBE', array('car' => $car));
    }
    
    function cetakPKBE($car, $jnPage, $frmSSPCP = 0) {
        if ($jnPage == '2' && $frmSSPCP > 0) {
            $data = $this->actBC30->getSSPCP($car);
            echo $this->load->view('bc20/SSPCP', array('car' => $car, 'SSPCP' => $data));
            return false;
        }
        $dataArr = $this->uri->segment_array();
        $data = $this->actBC30->cetakDokumenPKBE($car, $jnPage, $dataArr);
        return $data;
    }
    
    function frmPKB($car){
        //print_r($car);DIE();
        $dataForm = $this->actBC30->getFormPKB($car);
        echo $this->load->view('bc30/frm_PKB', $dataForm, true);
        exit();
    }
    
    function setHeaderPKB($car) {
        //print_r($car);die();
        $exe = $this->actBC30->setHeaderPKBin();
        echo $exe;
        exit();
    }
    
    function alertcekstatusPKB($car = "") {
        $this->load->model("actBC30");
        if ($car) {
            $data = $this->actBC30->validasiPKB($car);
        } else {
            $data = 'Cek status Error';
        }
        echo $data;
    }
    
    function frmPEBGab($car, $jmbrggab){
        $dataForm = $this->actBC30->getFormPEBGab($car, $jmbrggab);
        #print_r($dataForm);
        #die();
        echo $this->load->view('bc30/frmPEBGab', $dataForm, true);
        exit();
    }
    
    function RefFrmPEBGab($keys){
        //die($keys);
        $key = explode(",", $keys);
        $car        =$key[0];
        $seri       =$key[1];
        $jmbrggab   =$key[2];
        $dataForm = $this->actBC30->getFormPEBGab($car, $jmbrggab, $seri);
        echo $this->load->view('bc30/frmPEBGab', $dataForm, true);
        exit();
    }
    
    function setHeaderPEBGab() {
        $exe = $this->actBC30->setHeaderPEBGabin();
        echo $exe;
        exit();
    }
    
    function alertcekstatusPEBgab($car, $serieks='', $jmbrggab='') {
        //print_r($car.'|'.$jmbrggab.'|'.$serieks);die();
        $this->load->model("actBC30");
        if ($car) {
            $data = $this->actBC30->validasiPEBgab($car, $jmbrggab, $serieks);
        } else {
            $data = 'Cek status Error';
        }
        echo $data;
    }
    
    function frmPEBGabDtl($car, $jmbrgg, $serieks, $seri='1'){
        $dataForm = $this->actBC30->getFormPEBdtlGab($car,$jmbrgg,$serieks,$seri);
        echo $this->load->view('bc30/frmPEBGabDtl', $dataForm, true);
        exit();
    }
    
    function RefFrmPEBGabDtl($car,$jmbrgg,$serieks,$seri){
        //print_r($car.'|'.$jmbrgg.'|'.$serieks.'|'.$seri);die();
        $dataForm = $this->actBC30->getFormPEBdtlGab($car,$jmbrgg,$serieks,$seri);
        echo $this->load->view('bc30/frmPEBGabDtl', $dataForm, true);
        exit();
    }
    
    function setHeaderPEBdtlGab() {
        //print_r('sini');die();
        $exe = $this->actBC30->setHeaderPEBdtlGabin();
        echo $exe;
        exit();
    }
    
    function alertcekstatusPEBgabDTL($car = "", $serieks="", $seribrgg="") {
        //print_r($car.'|'.$serieks.'|'.$seribrgg);die();
        //print_r('sini');die();
        $this->load->model("actBC30");
        if ($car) {
            $data = $this->actBC30->validasiPEBgabDTL($car, $serieks, $seribrgg);
        } else {
            $data = 'Cek status Error';
        }
        echo $data;
    }
    
    function lstDokGab($key){
        $arrKey = explode(",", $key);
            $car        = $arrKey[0];
            $serieks    = $arrKey[1];
            $seribrgg   = $arrKey[2];
        $this->actBC30->getTblDokGab($car, $serieks, $seribrgg);
    }
    
    function cariDokGab(){
        $this->actBC30->getTblDokGab();
        exit();
    }
    
    function DokGab($act, $car, $serieks, $seribrgg) {
        //print_r($act.'|'.$car.'|'.$serieks.'|'.$seribrgg);die();
        if ($this->input->post('act') == 'edit') {
            foreach ($this->input->post('tb_chkfdokgab') as $chkitem) {
                $arrchk = explode("|", $chkitem);
            }
            $car        = $arrchk[0];
            $serieks    = $arrchk[1];
            $seribrgg   = $arrchk[2];
            $ktrsstbg   = $arrchk[3];
            $kddokg     = $arrchk[4];
            $nodokg     = $arrchk[5];
            //print_r($chkitem);die();
            $data = $this->actBC30->getDokGab($car,  $serieks, $seribrgg, $ktrsstbg, $kddokg, $nodokg);
            $data['act'] = 'update';
            return $this->load->view("bc30/frmPEBGabDok", $data);
        } elseif ($act == 'add') {
            $data = $this->actBC30->getDokGab($car,  $serieks, $seribrgg);
            echo $this->load->view("bc30/frmPEBGabDok", $data);
        } else {
            echo $this->actBC30->setDokGab($this->input->post('act'), $car);
        }
    }
    
    function pagePEBgabungan($car,$seri,$jmlPage) {
        //print_r($seri);die();
        $data = $this->actBC30->getFormPEBGab($car, $jmlPage ,$seri);
        return $this->load->view("bc30/frmPEBGab", $data);
    }
    
    function pageDtlPEBgabungan($car, $seri, $jmbrgg, $serieks) {
        //print_r($car.'/'.$seri.'/'.$jmbrgg.'/'.$serieks);die();
        //print_r($jmlpage);die();
        $data = $this->actBC30->getFormPEBdtlGab($car, $jmbrgg , $serieks, $seri);
        //print_r($car.'/'.$seri.'/'.$jmbrgg.'/'.$serieks);die();
        return $this->load->view("bc30/frmPEBGabDtl", $data);
    }
    
    function create($aju = '') {
        $arrheader = $this->actBC30->get_header($aju);
        $header = $this->load->view("bc30/header", $arrheader, true);
        $arrdetil = $this->actBC30->lsvDetil('barang', $aju);
        $detil = $this->load->view('bc30/lstbarang', $arrdetil, true);
        $data['HEADER'] = $header;
        $data['BARANG'] = $detil;
        echo $this->load->view('bc30/tabs', $data, true);
        exit(); 
    }
    
    function setHeader() {
        $car = $this->input->post("car");
        $exe = $this->actBC30->setHeader();
        echo $exe;
        exit();
    }
    
    function deleteHdr(){
        $data   = $this->input->post('tb_chkfBC30');
        $car    = $data[0];
        echo $this->actBC30->delHDR($car);
        die();
    }
    
    function daftar() {
        $data = $this->actBC30->lsvHeader();
        if ($this->input->post('ajax')) {
            echo $data;
        } else {
            echo $this->load->view('daftar', $data, true);
            exit();
        }
    }
    
    function daftarDetil($type, $aju) {
        $data = $this->actBC30->lsvDetil($type, $aju);
        if ($this->input->post('ajax')) {
            echo $data;
        } else {
            echo $this->load->view('list', $data, true);
            exit();
        }
    }
    
    function dokumen($car = '') {
        //print_r('sini say');die();
        if ($this->input->post('act') == 'edit') {
            foreach ($this->input->post('tb_chkfdokumen') as $chkitem) {//print_r($chkitem);die();
                $arrchk = explode("|", $chkitem);
            }
            $car = $arrchk[0];
            $DOKKD = $arrchk[1];
            $DOKNO = $arrchk[2];
            $data = $this->actBC30->getDokumen($car, $DOKKD, $DOKNO);
            return $this->load->view("bc30/frmdokumen", $data);
        } elseif ($this->input->post('act') == 'add') {
            $data = $this->actBC30->getDokumen($car);
            return $this->load->view("bc30/frmdokumen", $data);
        } else {
            echo $this->actBC30->setDokumen($this->input->post("act"), $car);
        }
    }
    
    function kontainer($car = '') {
        if ($this->input->post('act') == 'edit') {
            foreach ($this->input->post('tb_chkfkontainer') as $chkitem) {
                $arrchk = explode("|", $chkitem);
            }
            $car = $arrchk[0];
            $contno = $arrchk[1];
            $data = $this->actBC30->getKontainer($car, $contno);
            $data['act'] = 'update';
            return $this->load->view("bc30/frmkontainer", $data);
        } elseif ($this->input->post('act') == 'add') {
            $data = $this->actBC30->getKontainer($car);
            return $this->load->view("bc30/frmkontainer", $data);
        } else {
            echo $this->actBC30->setKontainer($this->input->post('act'), $car);
        }
    }
    
    function kemasan($car = '') {
        if ($this->input->post('act') == 'edit') {
            foreach ($this->input->post('tb_chkfkemasan') as $chkitem) {
                $arrchk = explode("|", $chkitem);
            }
            $car = $arrchk[0];
            $jns = $arrchk[1];
            $merk = $arrchk[2];
            $data = $this->actBC30->getKemasan($car, $jns, $merk);
            $data['act'] = 'update';
            return $this->load->view("bc30/frmkemasan", $data);
        } elseif ($this->input->post('act') == 'add') {
            $data = $this->actBC30->getKemasan($car);
            return $this->load->view("bc30/frmkemasan", $data);
        } else {
            echo $this->actBC30->setKemasan($this->input->post('act'), $car);
        }
    }
    
    function barang($car) {
        //print_r('sapi');die();
        if ($this->input->post('act') == 'edit') {
            foreach ($this->input->post('tb_chkfbarang') as $chkitem) {
                $arrchk = explode('|', $chkitem);
            }
            $car = $arrchk[0];
            $seri = $arrchk[1];
            //PRINT_R($arrchk);DIE();
            $data = $this->actBC30->getBarang($car, $seri);
            $data['act'] = 'update';
            return $this->load->view("bc30/frmbarang", $data);
        } elseif ($this->input->post('act') == 'add') {
            $data = $this->actBC30->getBarang($car);
            return $this->load->view("bc30/frmbarang", $data);
        } else {
            echo $this->actBC30->setBarang($this->input->post("act"));
            exit();
        }
    }
    
    function alertcekstatusDtl($car = "", $seri="") {
        if ($car && $seri) {
            list($status, $hasil) = $this->actBC30->recordValidasiDetil($car, $seri);
        } 
        else {
            $status = 'Cek status Error';
        }
        $judul = 'Hasil validasi Barang dengan aju : ' . substr($car, 20) . ' tanggal :' . substr($car, 12, 8) . '  Seri :' . $seri;     
        echo $hasil . '|' . $this->load->view('bc30/notifcek', $data, true);
    }
    
    function alertcekstatusHDR($car) {
        //print_r($car.'|'.$seri);die();
        $this->load->model("actBC30");
        if ($car) {
            $data = $this->actBC30->validasiBC30HDR($car, $seri);
        } else {
            $data = 'Cek status Error';
        }
        echo $data;
    }
    
    function pageDtlBarang($car,$seri) {
        //print_r($seri);die();
        $data = $this->actBC30->getBarang($car,$seri);
        return $this->load->view("bc30/frmbarang", $data);
    }
    
    function hitungan($data = "") {
        $this->load->model('actMain');
        $data = array('data' => $data,
            'KODE_HARGA' => $this->actMain->get_mtabel('KODE_HARGA'),
            'KODE_ASURANSI' => $this->actMain->get_mtabel('60', 1, FALSE));
        $this->load->view('bc30/hitungan', $data);
    }
    
    function pageBarang($car, $seri) {
        $this->load->model("actBC20");
        $data = $this->actBC20->lstBarang($car, $seri);
        $data = array_merge($data, array('edit' => "edit"));
        return $this->load->view("bc20/frmbarang", $data);
    }
    
    function getBm() {
        $this->load->model("actBC20");
        $data = $this->actBC20->lstBarang();
        echo $this->load->view("bc20/bm", $data);
    }
    
    function cekStatusDtl($aju, $seri) {
        $this->load->model("actBC20");
        if ($aju && $seri) {
            list($status, $hasil) = $this->actBC20->validasiDetil($aju, $seri);
        } else {
            $status = 'Cek status Error';
        }
        $judul = 'Hasil validasi Barang dengan aju : ' . substr($aju, 20) . ' tanggal :' . substr($aju, 12, 8) . '  Seri :' . $seri;
        $data = array(
            'data' => $status,
            'judul' => $judul);
        echo $hasil . '|' . $this->load->view('bc20/notifcek', $data, true);
    }
    
    function beforecetak($car) {
        echo $this->load->view('bc20/beforeCetak', array('car' => $car));
    }
    
    function cetak($car, $jnPage, $frmSSPCP = 0) {
        $this->load->model("actBC20");
        if ($jnPage == '2' && $frmSSPCP > 0) {
            $data = $this->actBC20->getSSPCP($car);
            echo $this->load->view('bc20/SSPCP', array('car' => $car, 'SSPCP' => $data));
            return false;
        }
        $data = $this->actBC20->cetakDokumen($car, $jnPage);
        return $data;
    }
    
//============Paging GoTo===================

    function GoToPagePEBg($car, $jmbrggab){
        //print_r($jmbrggab);die();
        $data = $this->actBC30->lstGoToPEBg($car, $jmbrggab);
        echo $this->load->view('daftar', $data, true);
        exit();
    }
    
    function GoToPagePEBgDtl($car, $serieks, $seribrgg, $jmbrgg){
        //print_r($car.'|'.$serieks.'|'.$seribrgg.'|'.$jmbrgg);DIE();
        $data = $this->actBC30->lstGoToPEBgDtl($car, $serieks, $seribrgg,$jmbrgg);
        echo $this->load->view('daftar', $data, true);
        exit();
    }
    
    function indentor(){
        $data = $this->input->post('data');
        //print_r($data);die();
        $this->load->model('actMain');
        $arr['data']    = $data;
        //print_r($arr['data'].'|'.'arr');die();
        $arr['JENIS_IDENTITAS']   = $this->actMain->get_mtabel('JENIS_IDENTITAS', 1, false, '', 'KODEDANUR');
        $this->load->view('bc30/indentor', $arr);
    }
    
//flat file

    function refreshHeaderPEB($aju) {
        //print_r($aju);die();
        $arrheader = $this->actBC30->get_header($aju);
        echo $this->load->view("bc30/header", $arrheader, true);
        die(); 
    }
    
    function getSRP($car){
        //print_r('sini');die();
        //$data = $this->input->post('data');
        //$this->load->model('actMain');
        $arr['CAR']    = $car;
        //$arr['INDID']   = $this->actMain->get_mtabel('JENIS_IDENTITAS', 1, false, '', 'KODEDANUR');
        $this->load->view('bc30/srp', $arr);
        //die('dari controler');
    }
    
    function queuedPEB($srp='') {
        $car = $this->input->post("CAR");
        $FF = $this->actBC30->genFlatfilePEB($car,$srp);
        $arrFF = explode('|', $FF);
        if ($arrFF[0] == '2') {
            $SNRF = $this->actBC30->crtQueuePEB($car);
            if ($SNRF!='ERROR') {
                $data = $SNRF . '|' . $arrFF[1];
                $SNRF = $this->actBC30->updateSNRFpeb($car, $data);
                $hsl = "===========================================================================================<BR>"
                     . "Bentuk Edifact berhasil.<BR>";
            } else {
                $hsl = "===========================================================================================<BR>"
                     . "Bentuk Edifact atau Pembatalan Edifact gagal.";
            }
        }
        elseif ($arrFF[0] == '1') {
            $hsl = "===========================================================================================<BR>"
                 . "Batal Edifact berhasil.<BR>";
        }
        else {
            $hsl = "===========================================================================================<BR>"
                    . "Bentuk Edifact atau Pembatalan Edifact gagal.<BR>"
                    . "===========================================================================================<BR>"
                    . $arrFF[0]
                    . "<BR>Silakan validasi data kembali.";
        }
        echo($hsl);
        exit();
    }  
    
    function beforecetakPEB($car) {
        echo $this->load->view('bc30/beforeCetakPEB', array('car' => $car));
    }  
    
    function cetakPEB($car, $jnPage, $frmSSPCP = 0) {
         
        if ($jnPage == '2' && $frmSSPCP > 0) {
            $data = $this->actBC30->getSSPCP($car);
            echo $this->load->view('bc30/SSPCP', array('car' => $car, 'SSPCP' => $data));
            return false;
        }
        $dataArr = $this->uri->segment_array();
        $data = $this->actBC30->cetakDokumenPEB($car, $jnPage, $dataArr);
        return $data;
    }   
    
    function setSSPCP() {
        $data = $this->actBC30->setSSPCP();
        return $data;
    }
    
    function beforecetakrespon($car) {
        //print_r($car);die();
        $this->load->model("actBC30");
        $data = $this->actBC30->daftarRespon($car,'cetakrespon');
        if ($this->input->post('ajax')) {
            echo $data;
        }
        else {
            echo $this->load->view('bc30/beforeCetakRespon', $data, true);
        }
        exit();
    }
    
    //beckup restore
    function downloadBUE($act,$car='',$EDINUMBER='',$file=''){
        if($act=='download'){
            echo $this->actBC30->downloadBUE();
            die();
        }
        elseif($act=='file'){
            //
            if($car!='' && $EDINUMBER!='' && $file!=''){
                $this->load->helper('download');
                $paht_local = getcwd() . '/Transaction/' . $EDINUMBER . '/' . $file . '.BUE';
                ob_start();
                $data = file_get_contents($paht_local);
                force_download($car.'.BUE', $data);
                ob_flush();
            }
            exit();
        }
    }
    
    function downloadBUEpkbe($act,$car='',$EDINUMBER='',$file=''){
        if($act=='download'){
            echo $this->actBC30->downloadBUEpkbe();
            die();
        }
        elseif($act=='file'){
            //
            if($car!='' && $EDINUMBER!='' && $file!=''){
                $this->load->helper('download');
                $paht_local = getcwd() . '/Transaction/' . $EDINUMBER . '/' . $file . '.BUE';
                ob_start();
                $data = file_get_contents($paht_local);
                force_download($car.'.BUE', $data);
                ob_flush();
            }
            exit();
        }
    }
    
    function uploadbue($act){
        if($act=='upload'){
            echo $this->actBC30->uploadBUE();  
            die();
        }
        elseif($act=='viewform'){  
            echo $this->load->view('bc30/uploadBUE','');
        }
    }
    
    function uploadbuepkbe($act){
        if($act=='upload'){
            echo $this->actBC30->uploadBUEpkbe();  
            die();
        }
        elseif($act=='viewform'){  
            echo $this->load->view('bc30/uploadBUEpkbe','');
        }
    }
    
    function getTOD(){
        $data = $this->input->post();
        echo $this->actBC30->getTOD($data['idModa'],$data['Regiona'],$data['Tgdaft']);
    }
}


?>