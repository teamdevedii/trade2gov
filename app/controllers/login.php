<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class login extends CI_Controller {
    function login() {
        parent::__construct();
    }
    function index() {
        if ($this->newsession->userdata('LOGGED')) {
            $role = $this->newsession->userdata('NAMA_ROLE');
            $trader = ", ".$this->newsession->userdata('NAMA_TRADER');
            $data = array(
                '_welcome_' => "Welcome " . $this->newsession->userdata('NAMA').$trader,
                '_role_'    => $role,
                '_menu_'    => $this->load->view('menu', array('tipe_user' => $this->newsession->userdata('KODE_TIPE_USER'),
                                                               'dokumen' => $this->newsession->userdata('DOKUMEN')), true),
                '_content_' => 'TEST HOME on login');//$this->load->view('home', '', true)
			$this->parser->parse('in', $data);
        }
        else{
            $this->newsession->sess_destroy();
            $data = array('_content_' =>  $this->load->view('frmHome','', true));
            $this->parser->parse('out', $data);
        }
    }
	
    function ceklogin($sessid = "", $isajax = "") {
        if (strtolower($_SERVER['REQUEST_METHOD']) != "post") { //|| $this->session->userdata('session_id') != $sessid
            echo "2|Login gagal, mohon coba lagi";
            exit();
        } else {
            $uid    = $this->input->post('_usr');
            $pwd    = $this->input->post('_pass');
            $code   = $this->input->post('_code');
            if (strtolower($code) === $_SESSION['captkodex']) {
                $this->load->model('actUser');
                $hasil = $this->actUser->login($uid, md5($pwd));
                if ($hasil == "1") {
                    echo "1|Login success";
                    exit();
                } else if ($hasil == "2") {
                    echo "0|User Anda sudah Kadaluarsa.<br>Silahkan Hubungi Administrator Kami";
                    exit();
                } else {
                    echo "0|Username atau Password Salah";
                    exit();
                }
            } else {
                echo "0|Kode yang anda masukan Salah";
                exit();
            }
        }
    }
    function logout(){
        //die('test');
        $this->newsession->sess_destroy();
        redirect(base_url());
    }
}