<?php if (!defined('BASEPATH')){exit('No direct script access allowed');}
class Bc23 extends CI_Controller {
    var $kd_trader = '';
    public function __construct() {
        parent::__construct();
        $this->load->model('actBC23');
        $this->kd_trader = $this->newsession->userdata('KODE_TRADER');
        if (!$this->newsession->userdata('LOGGED')) {echo '<script> window.location = site_url; </script>';exit();}
        return true;
    }

    function create($car) {         
        $arrheader  = $this->actBC23->getHeader($car); 
        $data['HEADER'] = $this->load->view("bc23/header", $arrheader,true);
        $data['DETIL']  = 'Detil' ;    
        echo $this->load->view('bc23/tabs', $data, true);
        exit();     
    }
    
    function refreshHeader($car) {
        $data = $this->actBC23->getHeader($car);
        echo $this->load->view("bc23/header", $data, true);
        die();
    }
    
    function header() {
        $this->actBC23->setHeader($this->input->post("act"));
    }
    
    function setHeader() {
        $exe = $this->actBC23->setHeader();
        echo $exe;
        exit();
    }
    
    function daftarTPB($tipe = "") {
        $data = $this->actBC23->lsvHeaderTPB($tipe = "");
        if($this->input->post('ajax')){
            echo $data;
        }else{
            echo $this->load->view('daftar', $data, true);
            exit();
        }
    }
    
    function deleteTPB(){
        $data   = $this->input->post('tb_chkf');
        $car    = $data[0];
        echo $this->actBC23->delTPB($car);
        die();
    }
    
    function daftarDetil($type, $aju) {
        $data = $this->actBC23->lstDetil($type, $aju);
        if ($this->input->post('ajax')) {
            echo $data;
        } else {
            echo $this->load->view('list', $data, true);
            exit();
        }
    }
    
    function edit($car) { //print_r($car);die();         
        $dataHeader = $this->actBC23->getHeader($car);
        $data['HEADER'] = $this->load->view('bc23/header',$dataHeader,true);       
        $dataDetil = $this->actBC23->lstDetil('barang',$car);
        $data['DETIL']  = $this->load->view('bc23/lstbarang',$dataDetil,true);                                 
        echo $this->load->view('bc23/tabs', $data, true);
        exit();        
    }
    
    function kontainer($car) {
        if ($this->input->post('act') == 'edit') {
            foreach ($this->input->post('tb_chkfkontainer') as $chkitem) {
                $arrchk = explode("|", $chkitem);
            }
            $car    = $arrchk[0];
            $contno = $arrchk[1];
            $data   = $this->actBC23->lstKontainer($car, $contno, $this->kd_trader);
            $data['act'] = 'update';
            return $this->load->view("bc23/frmkontainer", $data);
        }
        elseif ($this->input->post('act') == 'add'){
            $data = $this->actBC23->lstKontainer($car);
            return $this->load->view("bc23/frmkontainer", $data);
        }
        else {
            echo $this->actBC23->setKontainer($this->input->post('act'), $car);
        }
    }

    function kemasan($car) {
        if ($this->input->post('act') == 'edit') {
            foreach ($this->input->post('tb_chkfkemasan') as $chkitem) {
                $arrchk = explode("|", $chkitem);
            }
            $car = $arrchk[0];
            $jns = $arrchk[1];
            $merk = $arrchk[2];
            $data = $this->actBC23->lstKemasan($car, $jns, $merk);
            $data['act'] = 'update';
            return $this->load->view("bc23/frmkemasan", $data);
        }
        elseif ($this->input->post('act') == 'add'){
            $data = $this->actBC23->lstKemasan($car);
            return $this->load->view("bc23/frmkemasan", $data);
        }
        else {
            echo $this->actBC23->setKemasan($this->input->post('act'), $car);
        }
    }
    
    function dokumen($car) {
        if ($this->input->post('act') == 'edit') {
            foreach ($this->input->post('tb_chkfdokumen') as $chkitem) {
                $arrchk = explode("|", $chkitem);
            }
            $car    = $arrchk[0];
            $DOKKD  = $arrchk[1];
            $DOKNO  = $arrchk[2];
            $data   = $this->actBC23->lstDokumen($car, $DOKKD, $DOKNO);
            return $this->load->view("bc23/frmdokumen", $data);
        }
        elseif ($this->input->post('act') == 'add') {
            $data = $this->actBC23->lstDokumen($car);
            return $this->load->view("bc23/frmdokumen", $data);
        } 
        else {
            echo $this->actBC23->setDokumen($this->input->post("act"), $car);
        }
    }
    
    function hitungan($data = '') {
        $this->load->model('actMain');
        $arr['data']            = $data;
        $arr['KODE_HARGA']      = $this->actMain->get_mtabel('KODE_HARGA');
        $arr['KODE_ASURANSI']   = $this->actMain->get_mtabel('KODE_ASURANSI', 1, FALSE);
        $this->load->view('bc23/hitungan', $arr);
    }
        
    function pungutanDtl($car) { //print_r($car);die();
        $data['data_pgt'] = $this->actBC23->getPungutan($car);
        echo $this->load->view('bc23/detilpungutan', $data);
    }
    
    function pungutanDtlTarif($car) { //print_r($car);die();
        $data['data_pgt'] = $this->actBC23->getPungutanTarif($car);
        echo $this->load->view('bc23/detilpungutantarif', $data);
    }
    
    function barang($aju = "", $seri = "") {
        $post = $this->input->post();
        if ($post['act'] == 'edit') {
            $arrchk         = explode("|", $post['tb_chkfbarang'][0]);
            $aju            = $arrchk[0];
            $seri           = $arrchk[1];
            $data           = $this->actBC23->lstBarang($aju, $seri);
            $data['act']    = 'update';
            return $this->load->view("bc23/frmbarang", $data);
        }
        elseif ($post['act'] == 'add') {
            $data = $this->actBC23->lstBarang($aju);
            $data['act'] = 'save';
            return $this->load->view('bc23/frmbarang', $data);
        }
        else {
            echo $this->actBC23->setBarang($post['act'],$aju);
        }
    }

    function pageBarang($car, $seri) {
        $data = $this->actBC23->lstBarang($car, $seri);
        return $this->load->view("bc23/frmbarang", $data);
    }
    
    function alertcekstatus($aju = "", $type = "") {
        if ($aju && $type) {
           $return = $this->actBC23->validasiHeader($aju, $type);
        } else {
           $return = 'Cek status Error aju dan type tidak ada.';
        }        
        echo $return;
    }
    
    function cekStatusDtl($car, $seri='1') {
        if ($car && $seri) {
            list($status, $hasil) = $this->actBC23->validasiDetail($car, $seri);
        } 
        else {
            $status = 'Cek status Error';
        }
        $judul = 'Hasil validasi Barang dengan aju : ' . substr($car, 20) . ' tanggal :' . substr($car, 12, 8) . '  Seri :' . $seri;     
        echo $hasil . '|' . $this->load->view('bc23/notifcek', $data, true);
    }
    
    function getBm() {
        $data = $this->actBC23->lstBarang();
        echo $this->load->view("bc23/bm", $data);
    }
    
    function getPnbp($car) {
        //print_r($car);die();
        $data = $this->actBC23->getPnbp($car);
        $this->load->model("actMain");
        $data['JENIS_KERINGANAN'] = $this->actMain->get_mtabel('JENIS_KERINGANAN', 1, false, '', 'KODEDANUR', 'BC23');//print_r($data);die();
        //$data = $this->actBC23->getHeader();
        echo $this->load->view("bc23/Pnbp", $data, true);
        //return ; 
    }
    
    function setHeaderPnbp() {
        //print_r($car);die();
        $exe = $this->actBC23->setHeaderPnbp($car);
        echo $exe;
        exit();
    }
    
    function getPPh($nilPPh) {
        $data['NILPPH'] = $nilPPh;
        echo $this->load->view('bc23/PPh', $data);
    }
    
    function queued($srp='') {
        $car = $this->input->post("CAR");
        $FF = $this->actBC23->genFlatfile($car,$srp);
        $arrFF = explode('|', $FF);
        if ($arrFF[0] == '2') {
            $SNRF = $this->actBC23->crtQueue($car);
            if ($SNRF!='ERROR') {
                $data = $SNRF . '|' . $arrFF[1];
                $SNRF = $this->actBC23->updateSNRF($car, $data);
                $hsl = "===========================================================================================<BR>"
                        . "Proses Bentuk Edifact berhasil.<BR>";
            } else {
                $hsl = "===========================================================================================<BR>"
                        . "Proses Bentuk Edifact atau Pembatalan Edifact gagal.";
            }
        }
        elseif ($arrFF[0] == '1') {
            $hsl = "===========================================================================================<BR>"
                    . "Batal Edifact berhasil.<BR>";
        }
        else {
            $hsl = "===========================================================================================<BR>"
                    . "Bentuk Edifact atau Pembatalan Edifact gagal.<BR>"
                    . "===========================================================================================<BR>"
                    . $arrFF[0]
                    . "<BR>Silakan validasi data kembali.";
        }
        die($hsl);
    }
    
    function beforecetak($car) {
        echo $this->load->view('bc23/beforeCetak', array('car' => $car));
    }
    
    function cetak($car, $jnPage, $frmSSPCP = 0) {
        if ($jnPage == '2' && $frmSSPCP > 0) {
            $data = $this->actBC23->getSSPCP($car);
            echo $this->load->view('bc23/SSPCP', array('car' => $car, 'SSPCP' => $data));
            return false;
        }
        $dataArr = $this->uri->segment_array();
        $data = $this->actBC23->cetakDokumen($car, $jnPage, $dataArr);
        return $data;
    }
    
    function setSSPCP() {
        $data = $this->actBC23->setSSPCP();
        return $data;
    }
            
    function uploadb23($act){
        if($act=='upload'){
            echo $this->actBC23->uploadB23();  
            die();
        }
        elseif($act=='viewform'){  
            echo $this->load->view('bc23/uploadB23','');
        }
    }
    
    function downloadB23($act){
        if($act=='download'){
            echo $this->actBC23->downloadB23();
            die();
        }
        elseif($act=='file'){
            //
            if($car!='' && $EDINUMBER!='' && $file!=''){
                $this->load->helper('download');
                $paht_local = getcwd() . '/Transaction/' . $EDINUMBER . '/' . $file . '.B23';
                ob_start();
                $data = file_get_contents($paht_local);
                force_download($car.'.B23', $data);
                ob_flush();
            }
            exit();
        }
    }
    
     function uploadexcel($act,$car){                   
        if($act=='upload'){            
            echo $this->actBC23->uploadexcel($car);  
            die();
        }
        elseif($act=='viewformex'){  
            echo $this->load->view('bc23/uploadexcel',array('car'=>$car));
        }
    }
    
    function beforecetakrespon($car) {
        $this->load->model("actBC23");
        $data = $this->actBC23->daftarRespon($car,'cetakrespon');
        if ($this->input->post('ajax')) {
            echo $data;
        }
        else {
            echo $this->load->view('bc23/beforeCetakRespon', $data, true);
        }
        exit();
    }
    
    function getSRP($car){
        // print_r('$car');die();
        //$data = $this->input->post('data');
        //$this->load->model('actMain');
        $arr['CAR']    = $car;
        //$arr['INDID']   = $this->actMain->get_mtabel('JENIS_IDENTITAS', 1, false, '', 'KODEDANUR');
        $this->load->view('bc23/srp', $arr);
        //die('dari controler');
    }
     
}

?>
