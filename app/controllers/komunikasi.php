<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');
class komunikasi extends CI_Controller {
    var $kd_trader;
    public function __construct(){
        parent::__construct();
        if (!$this->newsession->userdata('LOGGED')) {echo '<script> window.location = site_url; </script>';exit();}
        $this->load->model("actKomunikasi");
        $this->kd_trader = $this->newsession->userdata('KODE_TRADER');
        return true;
    }
    
    function dataSent(){
        $data = $this->actKomunikasi->getDokOutbound();
        if($this->input->post('ajax')){
            echo $data;
        }else{
            echo $this->load->view('daftar', $data, true);
            exit();
        }
    }
    
    function dataReceive() {
        $data = $this->actKomunikasi->getDokInbound();
        if($this->input->post('ajax')){
            echo $data;
        }else{
            echo $this->load->view('daftar', $data, true);
            exit();
        }
    }
    
    function sendreceive(){
        echo $this->load->view('komunikasi/sendreceive',array('car'=>$car));
    }
    
    function communicate(){
        $hasil = $this->actKomunikasi->communication();
        echo $hasil;
        exit();
    }
    
    function resendDokumen(){
        $data = $this->input->post('tb_chkfDokOutbound'); 
        $act = $this->actKomunikasi->resendDokOutbound($data);
        if($act){
            $rtn = 'MSG|OK|' . site_url('komunikasi/dataSent') . '|Delete data dokumen berhasil.';
        }else{
            $rtn = 'MSG|ERR|' . site_url('komunikasi/dataSent') . '|Delete data dokumen gagal.';
        }
        echo $rtn;
        exit();
    }
    
    function openFLT($car, $kdkpbc, $dok){
        $dirFLT = $this->actKomunikasi->getPathFLT($car,$dok);
        echo '<pre>'.file_get_contents($dirFLT).'</pre>';
    }
    
    function openEDI($car, $kdkpbc, $dok){
        $dirEDI = $this->actKomunikasi->getPathEDI($car,$dok);
        echo '<pre>'.file_get_contents($dirEDI).'</pre>';
    }
    
    function openFILERES($TYPE,$ID,$PARTNER,$APRF){
        $dirFILE = $this->actKomunikasi->getPathFileRes($TYPE,$ID,$PARTNER,$APRF);
        echo '<pre>'.file_get_contents($dirFILE).'</pre>';
    }
    
    function autoRespon(){
        $comm = $this->actKomunikasi->actAutoRespon();
        echo $comm;
    }
    
    function delInbound(){
        $data = $this->input->post('tb_chkfDokInbound'); 
        $act = $this->actKomunikasi->deleteIndound($data);
        if($act){
            $rtn = 'MSG|OK|' . site_url('komunikasi/dataReceive') . '|Delete data dokumen yang diterima berhasil.';
        }else{
            $rtn = 'MSG|ERR|' . site_url('komunikasi/dataReceive') . '|Delete data dokumen yang diterima gagal.';
        }
        echo $rtn;
        exit();
    }
    
    
    function testing(){
        //unlink('TRANSACTION/PHA021041498/20150325/');
    }
}