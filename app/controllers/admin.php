<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Controller {
    var $content = "";
    public function __construct() {
        parent::__construct();
        if (!$this->newsession->userdata('LOGGED')) {echo '<script> window.location = site_url; </script>';exit();}
        return true;
    }
    function settrader($type = "", $act = "") {
        if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
            redirect(base_url());
            exit();
        } else {
            $this->load->model("admin_act");
            $this->admin_act->settrader($type, $act);
        }
    }

    function user($ajax = "") {
        if (!$this->newsession->userdata('LOGGED')) {
            $this->index();
            return;
        }
        $this->load->model("admin_act");
        $arrdata = $this->admin_act->user($ajax);
        $data = $this->load->view('daftar', $arrdata, true);
        if ($this->input->post("ajax") || $ajax) {
            echo $arrdata;
        } else {
            $this->content = $data;
            $this->index();
        }
    }

    function register($ajax = "") {
        if (!$this->newsession->userdata('LOGGED')) {
            $this->index();
            return;
        }
        $this->load->model("admin_act");
        $arrdata = $this->admin_act->register($ajax);
        $data = $this->load->view('daftar', $arrdata, true);
        if ($this->input->post("ajax") || $ajax) {
            echo $arrdata;
        } else {
            $this->content = $data;
            $this->index();
        }
    }

    function form($type = "", $id = "") {
        if (!$this->newsession->userdata('LOGGED')) {
            $this->index();
            return;
        }
        $this->load->model("admin_act");
        $arrdata = $this->admin_act->getuser($type, $id);
        $this->content = $this->load->view('admin/user', $arrdata, true);
        $this->index();
    }

    function reg($type = "", $id = "") {
        if (!$this->newsession->userdata('LOGGED')) {
            $this->index();
            return;
        }
        $this->load->model("admin_act");
        $arrdata = $this->admin_act->getreg($type, $id);
        $this->content = $this->load->view('admin/register', $arrdata, true);
        $this->index();
    }

    function setreg($sessid = "") 
	{
        $this->load->model('actOut');
		$this->actOut->insert_registrasi();
    }

    function setuser($act = "") 
	{
        if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
            redirect(base_url());
            exit();
        } else {
            $this->load->model("admin_act");
            $this->admin_act->setuser($act);
        }
    }

}

?>