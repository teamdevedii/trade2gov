<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');
class referensi extends CI_Controller {
    function autoComplate($strJns, $by = "") {
        $this->load->model("actAutocomplate");
        $keys = $this->input->post('keys', TRUE);
        $grp = $this->input->post('grp', TRUE);
        $json_array = $this->actAutocomplate->getData($strJns, $keys, $by, $grp);
        echo json_encode($json_array);
    }
    function checkCode($type){
        $this->load->model("actAutocomplate");
        $data = $this->input->post();
        echo $this->actAutocomplate->checkCode($type,$data);
    }
    function dataPendukung($type, $key = '') {
        $key = urldecode($key);
        $this->load->model("actReferensi");
        $data = $this->actReferensi->lsvDataPendukung($type, $key);
        if ($this->input->post('ajax')) {
            echo $data;
        } 
        else {
            echo $this->load->view('daftar', array_merge(array('judul' => 'Daftar ' . $type), $data), true);
        }
        exit();
    }
    function barang() {
        $this->load->model("actReferensi");
        $act = $this->input->post("act");
        if ($act == "edit") {
            foreach ($this->input->post('tb_chkfbarang') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $hs = $arrchk[1];
                $seri = $arrchk[2];
                $nama = $arrchk[3];
            }
            $data = $this->actReferensi->getBarang($act, $hs, $seri, $nama);
            $data = array_merge($data, array('act' => "update", 'judul' => 'Edit Data Barang'));
            return $this->load->view("referensi/frmBarang", $data);
        }
        elseif ($act == "add") {
            $data = $this->actReferensi->getBarang();
            $data = array_merge($data, array('act' => "add"));
            return $this->load->view("referensi/frmBarang", $data);
        }
        else {
            echo $this->actReferensi->setBarang($act);
        }
        die();
    }
    function importir() {
        $this->load->model("actReferensi");
        $act = $this->input->post("act");
        if ($act == "edit") {
            foreach ($this->input->post('tb_chkfimportir') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $kode = $arrchk[0];
                $impid = $arrchk[1];
                $npwp = $arrchk[2];
            }
            $data = $this->actReferensi->getImportir($act, $kode, $impid, $npwp);
            $data = array_merge($data, array('act' => "update", 'judul' => 'Edit Data Importir'));
            return $this->load->view("referensi/frmImportir", $data);
        } elseif ($act == "add") {
            $data = $this->actReferensi->getImportir();
            $data = array_merge($data, array('act' => "add"));
            return $this->load->view("referensi/frmImportir", $data);
        } else {
            echo $this->actReferensi->setImportir($act);
        }
        die();
    }
    function indentor() {
        $this->load->model("actReferensi");
        $act = $this->input->post("act");
        if ($act == "edit") {
            foreach ($this->input->post('tb_chkfindentor') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $kode = $arrchk[0];
                $indid = $arrchk[1];
                $npwp = $arrchk[2];
            }
            $data = $this->actReferensi->getIndentor($act, $kode, $indid, $npwp);
            $data = array_merge($data, array('act' => "update", 'judul' => 'Edit Data Indentor/QQ'));
            return $this->load->view("referensi/frmIndentor", $data);
        } elseif ($act == "add") {
            $data = $this->actReferensi->getIndentor();
            $data = array_merge($data, array('act' => "add"));
            return $this->load->view("referensi/frmIndentor", $data);
        } else {
            echo $this->actReferensi->setIndentor($act);
        }
        die();
    }
    function pemasok() {
        $this->load->model("actReferensi");
        $act = $this->input->post("act");
        if ($act == "edit") {
            foreach ($this->input->post('tb_chkfpemasok') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $kode = $arrchk[0];
                $nama = $arrchk[1];
            }
            $data = $this->actReferensi->getPemasok($act, $kode, $nama);
            $data = array_merge($data, array('act' => "update", 'judul' => 'Edit Data Pemasok'));
            return $this->load->view("referensi/frmPemasok", $data);
        } elseif ($act == "add") {
            $data = $this->actReferensi->getPemasok();
            $data = array_merge($data, array('act' => "add"));
            return $this->load->view("referensi/frmPemasok", $data);
        } else {
            echo $this->actReferensi->setPemasok($act);
        }
        die();
    }
    function tarif() {
        $this->load->model("actReferensi");
        $act = $this->input->post("act");
        if ($act == "edit") {
            foreach ($this->input->post('tb_chkftarif') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $kode = $arrchk[0];
                $nohs = $arrchk[1];
                $seritrp = $arrchk[2];
            }
            $data = $this->actReferensi->getTarif($act, $kode, $nohs, $seritrp);
            $data = array_merge($data, array('act' => "update", 'judul' => 'Edit Data POS Tarif / HS'));
            return $this->load->view("referensi/frmTarif", $data);
        } elseif ($act == "add") {
            $data = $this->actReferensi->getTarif();
            $data = array_merge($data, array('act' => "add"));
            return $this->load->view("referensi/frmTarif", $data);
        } else {
            echo $this->actReferensi->setTarif($act);
        }
        die();
    }
    function moda() {
        $this->load->model("actReferensi");
        $act = $this->input->post("act");
        if ($act == "edit") {
            foreach ($this->input->post('tb_chkfmoda') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $kode = $arrchk[0];
                $moda = $arrchk[1];
                $nama = $arrchk[2];
            }
            $data = $this->actReferensi->getModa($act, $kode, $moda, $nama);
            $data = array_merge($data, array('act' => "update", 'judul' => 'Edit Data Kapal / Moda'));
            return $this->load->view("referensi/frmModa", $data);
        } elseif ($act == "add") {
            $data = $this->actReferensi->getModa();
            $data = array_merge($data, array('act' => "add"));
            return $this->load->view("referensi/frmModa", $data);
        } else {
            echo $this->actReferensi->setModa($act);
        }
        die();
    }
    function bank() {
        $this->load->model("actReferensi");
        $act = $this->input->post("act");
        if ($act == "edit") {
            foreach ($this->input->post('tb_chkfbank') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $kode = $arrchk[0];
                $nama = $arrchk[1];
            }
            $data = $this->actReferensi->getBank($act, $kode, $nama);
            $data = array_merge($data, array('act' => "update", 'judul' => 'Edit Data Bank'));
            return $this->load->view("referensi/frmBank", $data);
        } elseif ($act == "add") {
            $data = array('act' => "save", 'judul' => 'Tambah Data Bank');
            return $this->load->view("referensi/frmBank", $data);
        } else {
            echo $this->actReferensi->setBank($act);
        }
        die();
    }
    function daerah() {
        $this->load->model("actReferensi");
        $act = $this->input->post("act");
        if ($act == "edit") {
            foreach ($this->input->post('tb_chkfdaerah') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $kode = $arrchk[0];
            }
            $data = $this->actReferensi->getDaerah($act, $kode);
            $data = array_merge($data, array('act' => "update", 'judul' => 'Edit Data Daerah'));
            return $this->load->view("referensi/frmDaerah", $data);
        } elseif ($act == "add") {
            $data = array('act' => "save", 'judul' => 'Tambah Data Daerah');
            return $this->load->view("referensi/frmDaerah", $data);
        } else {
            echo $this->actReferensi->setDaerah($act);
        }
        die();
    }
    function gudang() {
        $this->load->model("actReferensi");
        $act = $this->input->post("act");
        if ($act == "edit") {
            foreach ($this->input->post('tb_chkfgudang') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $kode = $arrchk[0];
                $gudang = $arrchk[1];
            }
            $data = $this->actReferensi->getGudang($act, $kode, $gudang);
            $data = array_merge($data, array('act' => "update", 'judul' => 'Edit Data Gudang'));
            return $this->load->view("referensi/frmGudang", $data);
        } elseif ($act == "add") {
            $data = array('act' => "save", 'judul' => 'Tambah Data Gudang');
            return $this->load->view("referensi/frmGudang", $data);
        } else {
            echo $this->actReferensi->setGudang($act);
        }
        die();
    }
    function hanggar() {
        $this->load->model("actReferensi");
        $act = $this->input->post("act");
        if ($act == "edit") {
            foreach ($this->input->post('tb_chkfhanggar') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $kode = $arrchk[0];
                $hanggar = $arrchk[1];
            }
            $data = $this->actReferensi->getHanggar($act, $kode, $hanggar);
            $data = array_merge($data, array('act' => "update", 'judul' => 'Edit Data Hanggar'));
            return $this->load->view("referensi/frmHanggar", $data);
        } elseif ($act == "add") {
            $data = array('act' => "save", 'judul' => 'Tambah Data Hanggar');
            return $this->load->view("referensi/frmHanggar", $data);
        } else {
            echo $this->actReferensi->setHanggar($act);
        }
        die();
    }
    function izin() {
        $this->load->model("actReferensi");
        $act = $this->input->post("act");
        if ($act == "edit") {
            foreach ($this->input->post('tb_chkfizin') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $kode = $arrchk[0];
            }
            $data = $this->actReferensi->getIzin($act, $kode);
            $data = array_merge($data, array('act' => "update", 'judul' => 'Edit Data Izin'));
            return $this->load->view("referensi/frmIzin", $data);
        } elseif ($act == "add") {
            $data = array('act' => "save", 'judul' => 'Tambah Data Izin');
            return $this->load->view("referensi/frmIzin", $data);
        } else {
            echo $this->actReferensi->setIzin($act); 
        }
        die();
    }
    function kemasan() {
        $this->load->model("actReferensi");
        $act = $this->input->post("act");
        if ($act == "edit") {
            foreach ($this->input->post('tb_chkfkemasan') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $kode = $arrchk[0];
            }
            $data = $this->actReferensi->getKemasan($act, $kode);
            $data = array_merge($data, array('act' => "update", 'judul' => 'Edit Data Kemasan'));
            return $this->load->view("referensi/frmKemasan", $data);
        } elseif ($act == "add") {
            $data = array('act' => "save", 'judul' => 'Tambah Data Kemasan');
            return $this->load->view("referensi/frmKemasan", $data);
        } else {
            echo $this->actReferensi->setKemasan($act);
        }
        die();
    }
    function kpbc() {
        $this->load->model("actReferensi");
        $act = $this->input->post("act");
        if ($act == "edit") {
            foreach ($this->input->post('tb_chkfkpbc') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $kode = $arrchk[0];
            }
            $data = $this->actReferensi->getKpbc($act, $kode);
            $data = array_merge($data, array('act' => "update", 'judul' => 'Edit Data KPBC'));
            return $this->load->view("referensi/frmKpbc", $data);
        } elseif ($act == "add") {
            $data = array('act' => "save", 'judul' => 'Tambah Data KPBC');
            return $this->load->view("referensi/frmKpbc", $data);
        } else {
            echo $this->actReferensi->setKpbc($act);
        }
        die();
    }
    function kpker() {
        $this->load->model("actReferensi");
        $act = $this->input->post("act");
        if ($act == "edit") {
            foreach ($this->input->post('tb_chkfkpker') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $kode = $arrchk[0];
            }
            $data = $this->actReferensi->getKpker($act, $kode);
            $data = array_merge($data, array('act' => "update", 'judul' => 'Edit Data KPKER'));
            return $this->load->view("referensi/frmKpker", $data);
        } elseif ($act == "add") {
            $data = array('act' => "save", 'judul' => 'Tambah Data KPKER');
            return $this->load->view("referensi/frmKpker", $data);
        } else {
            echo $this->actReferensi->setKpker($act);
        }
        die();
    }
    function negara() {
        $this->load->model("actReferensi");
        $act = $this->input->post("act");
        if ($act == "edit") {
            foreach ($this->input->post('tb_chkfnegara') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $kode = $arrchk[0];
            }
            $data = $this->actReferensi->getNegara($act, $kode);
            $data = array_merge($data, array('act' => "update", 'judul' => 'Edit Data Negara'));
            return $this->load->view("referensi/frmNegara", $data);
        } elseif ($act == "add") {
            $data = array('act' => "save", 'judul' => 'Tambah Data Negara');
            return $this->load->view("referensi/frmNegara", $data);
        } else {
            echo $this->actReferensi->setNegara($act);
        }
        die();
    }
    function pelabuhan() {
        $this->load->model("actReferensi");
        $act = $this->input->post("act");
        if ($act == "edit") {
            foreach ($this->input->post('tb_chkfpelabuhan') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $kode = $arrchk[0];
            }
            $data = $this->actReferensi->getPelabuhan($act, $kode);
            $data = array_merge($data, array('act' => "update", 'judul' => 'Edit Data Pelabuhan'));
            return $this->load->view("referensi/frmPelabuhan", $data);
        } elseif ($act == "add") {
            $data = array('act' => "save", 'judul' => 'Tambah Data Pelabuhan');
            return $this->load->view("referensi/frmPelabuhan", $data);
        } else {
            echo $this->actReferensi->setPelabuhan($act);
        }
        die();
    }
    function satuan() {
        $this->load->model("actReferensi");
        $act = $this->input->post("act");
        if ($act == "edit") {
            foreach ($this->input->post('tb_chkfsatuan') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $kode = $arrchk[0];
            }
            $data = $this->actReferensi->getSatuan($act, $kode);
            $data = array_merge($data, array('act' => "update", 'judul' => 'Edit Data Satuan'));
            return $this->load->view("referensi/frmSatuan", $data);
        } elseif ($act == "add") {
            $data = array('act' => "save", 'judul' => 'Tambah Data Satuan');
            return $this->load->view("referensi/frmSatuan", $data);
        } else {
            echo $this->actReferensi->setSatuan($act);
        }
        die();
    }
    function setting() {
        $this->load->model("actReferensi");
        $act = $this->input->post("act");
        if ($act == "edit") {
            foreach ($this->input->post('tb_chkfsetting') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $kode = $arrchk[0];
                $var = $arrchk[1];
            }
            $data = $this->actReferensi->getSetting($act, $kode, $var);
            $data = array_merge($data, array('act' => "update", 'judul' => 'Edit Data Setting'));
            return $this->load->view("referensi/frmSetting", $data);
        }
        elseif ($act == "add") {
            $data = array('act' => "save", 'judul' => 'Tambah Data Setting');
            return $this->load->view("referensi/frmSetting", $data);
        }
        else {
            echo $this->actReferensi->setSetting($act);
        }
        die();
    }
    function partner(){
        $this->load->model("actReferensi");
        $act = $this->input->post("act");
        if ($act == "edit") {
            foreach ($this->input->post('tb_chkfpartner') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $kode_trader = $arrchk[0];
                $kdkpbc      = $arrchk[1];
                $edinumber   = $arrchk[2];
            }
            $data = $this->actReferensi->getPartner($act, $kode_trader, $kdkpbc, $edinumber);
            return $this->load->view("referensi/frmPartner", $data);
        }
        elseif ($act == 'add') {
            $data = $this->actReferensi->getPartner($act);
            return $this->load->view("referensi/frmPartner", $data);
        }
        else {
            echo $this->actReferensi->setPartner($act);
        }
        die();
    }
    function tod(){
        $this->load->model("actReferensi");
        $act = $this->input->post("act");
        if ($act == "edit") {
            foreach ($this->input->post('tb_chkftod') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $kode_trader  = $arrchk[0];
                $jnsmoda      = $arrchk[1];
                $region       = $arrchk[2];
            }
            $data = $this->actReferensi->getTod($act, $kode_trader, $jnsmoda, $region);
            return $this->load->view("referensi/frmPartner", $data);
        }
        elseif ($act == 'add') {
            $data = $this->actReferensi->getTod($act);
            return $this->load->view("referensi/frmPartner", $data);
        }
        else {
            echo $this->actReferensi->setTod($act);
        }
        die();
    }
    function partnerdetil($keys=''){
        $keys = urldecode($keys);
        $this->load->model("actReferensi");
        $act = $this->input->post("act");
        if ($act == "edit") {
            $key = explode('|', $keys);
            foreach ($this->input->post('tb_chkfpartnerdetil') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $kode_trader= $arrchk[0];
                $kpbc       = $arrchk[1];
                $nomoredi   = $arrchk[2];
                $aprf       = $arrchk[3];                
            }
            $data = $this->actReferensi->getPartnerDetil($act, $kpbc, $nomoredi, $aprf);
            //getPartnerDetil($act, $kdkpbc='', $edinumber='', $aprf='')
            $data['act'] = 'update';
            $data['key'] = $keys;
            return $this->load->view("referensi/frmPartnerDetil", $data);
        }
        elseif ($act == "add") {
            $key = explode('|', $keys);
            $data = $this->actReferensi->getUrKpbc($key[1]);
            $data['PARTNERDETIL']['KODE_TRADER'] = $key[0];
            $data['PARTNERDETIL']['NOMOREDI']    = $key[2];
            $data['act'] = 'insert';
            $data['key'] = $keys;
            return $this->load->view("referensi/frmPartnerDetil", $data);
        }
        else {
            echo $this->actReferensi->setPartnerDetil($act,$keys);
        }
        die();
    }
    function test(){
        return $this->load->view("frmRegistrasi", $data);
    }
    
    function hs() {
        $this->load->model("actReferensi");
        $act = $this->input->post("act");
        if ($act == "edit") {
            foreach ($this->input->post('tb_chkfhs') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $kode = $arrchk[0];
            }
            $data = $this->actReferensi->getHs($act, $kode);
            $data = array_merge($data, array('act' => "update", 'judul' => 'Edit Data HS'));
            return $this->load->view("referensi/frmHs", $data);
        } elseif ($act == "add") {
            $data = array('act' => "save", 'judul' => 'Tambah Data HS');
            return $this->load->view("referensi/frmHs", $data);
        } else {
            echo $this->actReferensi->setHs($act);
        }
        die();
    }
    
    function manifest_grup() {
        $this->load->model("actReferensi");
        $act = $this->input->post("act");
        if ($act == "edit") {
            foreach ($this->input->post('tb_chkfmanifest_grup') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $kode = $arrchk[0];
            }
            $data = $this->actReferensi->getManifestGrup($act, $kode);
            $data = array_merge($data, array('act' => "update", 'judul' => 'Edit Data Manifest Grup'));
            return $this->load->view("referensi/frmManifestGrup", $data);
        } elseif ($act == "add") {
            $data = $this->actReferensi->getManifestGrup($act);
            $data = array_merge($data, array('act' => "save", 'judul' => 'Tambah Data Manifest Grup'));
            return $this->load->view("referensi/frmManifestGrup", $data);
        } else {
            echo $this->actReferensi->setManifestGrup($act);
        }
        die();
    }
    
    function tambat() {
        $this->load->model("actReferensi");
        $act = $this->input->post("act");
        if ($act == "edit") {
            foreach ($this->input->post('tb_chkftambat') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $kode = $arrchk[0];
            }
            $data = $this->actReferensi->getTambat($act, $kode);
            $data = array_merge($data, array('act' => "update", 'judul' => 'Edit Data Tambat'));
            return $this->load->view("referensi/frmTambat", $data);
        } elseif ($act == "add") {
            $data = array('act' => "save", 'judul' => 'Tambah Data Tambat');
            return $this->load->view("referensi/frmTambat", $data);
        } else {
            echo $this->actReferensi->setTambat($act);
        }
        die();
    }
    
    function tod2() {
        $this->load->model("actReferensi");
        $act = $this->input->post("act");
        if ($act == "edit") {
            foreach ($this->input->post('tb_chkftod2') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $kode = $arrchk[0];
            }
            $data = $this->actReferensi->getTod2($act, $kode);
            $data = array_merge($data, array('act' => "update", 'judul' => 'Edit Data Tod'));
            return $this->load->view("referensi/frmTod", $data);
        } elseif ($act == "add") {
            $data = $this->actReferensi->getTod2($act);
            $data = array_merge($data, array('act' => "save", 'judul' => 'Tambah Data Tod'));
            return $this->load->view("referensi/frmTod", $data);
        } else {
            echo $this->actReferensi->setTod2($act);
        }
        die();
    }
    
}

?>
