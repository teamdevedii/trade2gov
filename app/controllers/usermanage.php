<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class usermanage extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->newsession->userdata('LOGGED')) {
            echo '<script> window.location = site_url; </script>';
            exit();
        }
        return true;
    }

    function approvalRegistrasi() {
        $this->load->model('actManageUser');
        $data = $this->input->post('DATA');
        if ($data['STATUS'] = '01' && $this->newsession->userdata('KODE_TIPE_USER') == '2') {
            $rtn = $this->actManageUser->approveBC();
        } else {
            $rtn = $this->actManageUser->approve();
        }
        echo $rtn;
        exit();
    }

    function regOnline() {
        $this->load->model('actManageUser');
        $data = $this->actManageUser->lstUser('regOnline');
        if ($this->input->post('ajax')) {
            echo $data;
        } else {
            echo $this->load->view('daftar', $data, true);
            exit();
        }
    }

    function regOnlineBC() {
        $this->load->model('actManageUser');
        $data = $this->actManageUser->lstUserBC('regOnlineBC');
        if ($this->input->post('ajax')) {
            echo $data;
        } else {
            echo $this->load->view('daftar', $data, true);
            exit();
        }
    }

    function viwApproval($kd_trader_tmp) {
        $this->load->model('actOut');
        $data = $this->actOut->getRegistrasi($kd_trader_tmp);
        echo $this->load->view('frmApproval', $data, true);
        exit();
    }

    function getLastnoreg($KPBC = '') {
        if ($KPBC != '') {
            $this->load->model('actMain');
            $data = $this->actMain->get_uraian("SELECT LAST_NOREG+1 AS NOREG FROM M_KPBC WHERE KDKPBC = '" . $KPBC . "'", 'NOREG');
            echo substr('000000' . $data, -6);
        }
        exit();
    }

    function lstPerusahaan($type, $kd_trader = '') {
        $this->load->model('actManageUser');
        $data = $this->actManageUser->lstPerusahaan($type, $kd_trader);
        echo $this->load->view('daftar', $data, true);
        exit();
    }

    function viwProfile() {
        $this->load->model('actManageUser');
        $data = $this->actManageUser->getProfile();
        echo $this->load->view('frmProfile', $data, true);
        exit();
    }

    function setProfile() {
        $this->load->model('actManageUser');
        $data = $this->actManageUser->setProfile();
        echo $data;
        exit();
    }

    function ubahpassword($sessid = '') {
        $this->load->model('actManageUser');
        $post = $this->input->post('DATA');
        $cekUbahPassword = $this->actManageUser->cekUbahPassword($post);
        echo $cekUbahPassword;
        exit();
    }

    function setProfileUser() {
        $this->load->model('actManageUser');
        $data = $this->actManageUser->setProfileUser();
        echo $data;
        exit();
    }

    function setProfilePIB() {
        $this->load->model('actManageUser');
        $data = $this->actManageUser->setProfilePIB();
        echo $data;
        exit();
    }

    function setProfilePEB() {
        $this->load->model('actManageUser');
        $data = $this->actManageUser->setProfilePEB();
        echo $data;
        exit();
    }

    function setProfileTPB() {
        $this->load->model('actManageUser');
        $data = $this->actManageUser->setProfileTPB();
        echo $data;
        exit();
    }

    function setProfileManifest() {
        $this->load->model('actManageUser');
        $data = $this->actManageUser->setProfileManifest();
        echo $data;
        exit();
    }

    function resetPass($kd_trader) {
        die($msgbox);
    }

    function mngPerusahaan($act, $kd_trader) {
        $this->load->model('actManageUser');
        switch ($act) {
            case'edit':
                $data = $this->actManageUser->getPerusahaan($kd_trader);
                echo $this->load->view("frmPerusahaan", $data, true);
                break;
            case'resetPass':
                $data = $this->actManageUser->resetPassword($kd_trader);
                foreach ($data as $a) {
                    $arrData = explode('|', $a);
                    if ($arrData[0] == 'OK') {
                        $msgbox .= 'User ' . $arrData[1] . ' Data berhasil direset.<br>';
                    } else {
                        $msgbox .= 'User ' . $arrData[1] . ' Data gagal direset.<br>';
                    }
                }
                echo($msgbox);
                break;
        }
    }

    function editPerusahaan() {
        $this->load->model('actManageUser');
        $data = $this->input->post('DATA');
        $rtn = $this->actManageUser->simpanPerusahaan($data);
        echo $rtn;
        exit();
    }

    function nonAktif($req) {
        $this->load->model("actManageUser");
        $data = $this->input->post('tb_chkfperusahaan');
        echo $this->actManageUser->nonAktif($data[0]);
        die();
    }

    function mngUserPerusahaan($act, $kd_trader, $id_user) {
        $this->load->model('actManageUser');
        switch ($act) {
            case'edit':
                echo 'edit user';
                break;
            case'hapus':
                echo 'ubah status user';
                break;
            case'resetPass':
                $data = $this->actManageUser->resetPassword($kd_trader, $id_user);
                foreach ($data as $a) {
                    $arrData = explode('|', $a);
                    if ($arrData[0] == 'OK') {
                        $msgbox .= 'User ' . $arrData[1] . ' Data berhasil direset.<br>';
                    } else {
                        $msgbox .= 'User ' . $arrData[1] . ' Data gagal direset.<br>';
                    }
                }
                echo $msgbox;
                break;
        }
    }

    function frmNoRegistrasi($tipe, $kd_trader) {
        $this->load->model('actManageUser');
        $data = $this->actManageUser->getNoRegistrasi($tipe, $kd_trader);
        $data['DATA']['KODE_TRADER'] = $kd_trader;
        $data['tipe'] = $tipe;
        //print_r($data);die();
        echo $this->load->view('frmNoRegistrasi', $data, true);
        die();
    }

    function setNoRegister() {
        $this->load->model('actManageUser');
        $data = $this->input->post('DATA');
        $tipe = $this->input->post('tipe');
        echo $this->actManageUser->setNoRegister($tipe, $data);
        die();
    }

    function changePassword() {
        echo $this->load->view('frmChangePassword', '', true);
    }

    function regCustomer($act) {
        $this->load->model('actOut');
        $code = $this->input->post('kode');
        if (strtolower($code) === $_SESSION['captkodex']) {
            $this->load->model('actUser');
            $insert = $this->actOut->setRegistrasi($act);
            if ($insert == '1') {
                echo "MSG#OK#Registrasi anda akan diverifikasi oleh admin.<br>Informasi mengenai regisrasi akan dikirim melalui e-Mail atau dapat menghubungi nomor 021-xxxxxxxxx#" . site_url('outLogin/callOut/home');
                exit();
            } elseif ($insert == '0') {
                echo "MSG#ERROR#Registrasi gagal.";
                exit();
            } else {
                echo $insert;
                exit();
            }
        } else {
            echo "MSG#ERROR#Kode Captcha salah.";
            exit();
        }
    }

    function callOut() {
        $this->load->model("actOut");
        $arrdata = $this->actOut->getRegistrasi();
        $data = $this->load->view('frmRegistrasi', $arrdata, true);
        echo $data;
    }

    function reqRegistrasi($tipe) {
        $this->load->model("actManageUser");
        $data = $this->actManageUser->lstreq($tipe);
        if ($this->input->post('ajax')) {
            echo $data;
        } else {
            echo $this->load->view('daftar', $data, true);
            exit();
        }
    }

    function updateStatus($req) {
        $this->load->model("actManageUser");
        $data = $this->input->post('tb_chkfrequest');
        echo $this->actManageUser->updateStatus($data[0]);
        die();
    }

    function daftarUser() {
        $this->load->model("actManageUser");
        $kd_trader = $this->newsession->userdata('KODE_TRADER');
        $data = $this->actManageUser->daftarUser($kd_trader);
        if ($this->input->post('ajax')) {
            echo $data;
        } else {
            echo $this->load->view('daftar', $data, true);
            exit();
        }
    }

    function mngUserOperator($act, $user_id) {
        $this->load->model("actManageUser");
        $kd_trader = $this->newsession->userdata('KODE_TRADER');
        if ($act == "edit") {
            $data = $this->actManageUser->getUserOperator($act, $user_id);
            $data = array_merge($data, array('act' => "update", 'judul' => 'Edit Data User Operator'));
            echo $this->load->view("frmUserOperator", $data, true);
        } elseif ($act == "tambah") {
            $data = array('act' => "save", 'judul' => 'Tambah Data User');
            echo $this->load->view("frmUserOperator", $data, true);
        } else {
            echo $this->actManageUser->setUserOperator();
        }
        die();
    }
}