<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class home extends CI_Controller {
    function index() {
        //die($this->newsession->userdata('LOGGED'));
        if ($this->newsession->userdata('LOGGED')) {
            
            $role = $this->newsession->userdata('NAMA_ROLE');
            $trader = ", ".$this->newsession->userdata('NAMA_TRADER');
            //$login = ", ".$this->newsession->userdata('LAST_LOGIN');
            $data = array(
                '_welcome_' => $this->newsession->userdata('NAMA').$trader.$login,
                '_role_'    => $role,
                '_menu_'    => $this->load->view('menu', array('tipe_user'  => $this->newsession->userdata('KODE_TIPE_USER'),
                                                               'dokumen'    => $this->newsession->userdata('DOKUMEN')), true),
                '_content_' => 'TEST HOME on login',
                '_menuProfile_' => $this->load->view('menuProfile', array('tipe_user'  => $this->newsession->userdata('KODE_TIPE_USER')), true));
            $this->parser->parse('in', $data);
        }
        else{
            $this->newsession->sess_destroy();
            $data = array('_content_' =>  $this->load->view('frmHome','', true));
            $this->parser->parse('out', $data);
        }
    }
}