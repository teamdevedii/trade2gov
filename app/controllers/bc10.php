<?php
if (!defined('BASEPATH')){exit('No direct script access allowed');}
class bc10 extends CI_Controller {
    var $kd_trader = '';
    public function __construct() {
        parent::__construct();
        $this->load->model('actBC10');
        $this->kd_trader = $this->newsession->userdata('KODE_TRADER');
        if (!$this->newsession->userdata('LOGGED')) {echo '<script> window.location = site_url; </script>';exit();}
        return true;
    }
    
    function lstBC10(){
        $data = $this->actBC10->lstBC10();
        if($this->input->post('ajax')){
            echo $data;
        }else{
            echo $this->load->view('daftar', $data, true);
            exit();
        }
    }
    
    function create($cat) {
        $dataHeader = $this->actBC10->getHeader($cat);
        $data['HEADER'] = $this->load->view('bc10/header',$dataHeader,true);
        echo $this->load->view('bc10/tabs', $data, true);
        exit();
    }
    
    function refeshHeader($car){
        $dataHeader = $this->actBC10->getHeader($car);
        echo $this->load->view('bc10/header',$dataHeader,true);
        exit();
    }
    
    function saveHeader() {
        $data = $this->input->post();
        echo $this->actBC10->setHeader($data);
    }
    
    function queued(){
        $car= $this->input->post("CAR");
        $FF = $this->actBC10->genFlatfile($car);
        $arrFF = explode('|', $FF);
        if ($arrFF[0] == '2') {
            $SNRF = $this->actBC10->crtQueue($car);
            if ($SNRF!='ERROR') {
                $data = $SNRF . '|' . $arrFF[1];
                $SNRF = $this->actBC10->updateSNRF($car, $data);
                $hsl = "===========================================================================================<BR>"
                     . "Bentuk Edifact berhasil.<BR>";
            } else {
                $hsl = "===========================================================================================<BR>"
                     . "Bentuk Edifact atau Pembatalan Edifact gagal.";
            }
        }
        elseif ($arrFF[0] == '1') {
            $hsl = "===========================================================================================<BR>"
                 . "Batal Edifact berhasil.<BR>";
        }
        else {
            $hsl = "===========================================================================================<BR>"
                    . "Bentuk Edifact atau Pembatalan Edifact gagal.<BR>"
                    . "===========================================================================================<BR>"
                    . $arrFF[0]
                    . "<BR>Silakan validasi data kembali.";
        }
        echo($hsl);
        exit();
    }
    
    function beforeCetak($car){
        echo $this->load->view('bc10/beforeCetak', array('car' => $car));
    }
    
    function cetakDokumen($car, $jnPage, $arr ) {
        $this->load->library('fpdf');
        $this->load->model('actMain');
        $this->load->model('dokBC10');
        $this->dokBC10->ciMain($this->actMain);
        $this->dokBC10->fpdf($this->fpdf);
        $this->dokBC10->segmentUri($arr);
        $this->dokBC10->showPage($car, $jnPage);
    }
    
    function deleteRKSP(){
        $data = $this->input->post('tb_chkflstBC10');
        echo $this->actBC10->deleteRKSP($data);
    }
    
    function validasiRKSP($car){
        echo $this->actBC10->validasi($car);
    }
}