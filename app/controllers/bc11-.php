<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class bc11 extends CI_Controller {

    var $od_trader = '';

    public function __construct() {
        parent::__construct();
        $this->load->model("actBC11");
        $this->kd_trader = $this->newsession->userdata('KODE_TRADER');
        if (!$this->newsession->userdata('LOGGED')) {echo '<script> window.location = site_url; </script>';exit();}
        return true;
    }

    //Agung    
    function dataDrillDownManifest($type, $car, $kdgrup) {//print_r($type.'|'.$car.'|'.$kdgrup);die();
        $this->load->model("actbc11");
        $data = $this->actbc11->lsvDataDrillDownManifest($type, $car, $kdgrup);
        if ($this->input->post('ajax')) {
            echo $data;
        } 
        else {
            echo $this->load->view('daftar', array_merge(array('judul' => 'Daftar ' . $type), $data), true);
        }
        exit();
    }
    
    function alertcekstatus($car = "") {
        if ($car) {
            $data = $this->actBC11->validasiHDRMANIFEST($car);
            
        } else {
            $data = 'Cek status Error';
        }
        //$data = array( 'data' => $status, 'judul' => $judul, 'status' => $sts);
        //var_dump($data);die();
        echo $data;       
    }
    
    function setHeaderMANIFEST() {
        $exe = $this->actBC11->setHeaderMANIFESTin();
        echo $exe;
        exit();
    }
    
    function delMANIFESThdr(){
        $key = $this->input->post('tb_chkfMANIFEST');
            echo $this->actBC11->delMANIFESThdr($key);
            die();
    }
    
    function daftarMANIFEST() { //echo 'aaa';die();
        
        $data = $this->actBC11->lsvHeaderMANIFEST();
        if ($this->input->post('ajax')) {
            echo $data;
        }else {
            echo $this->load->view('daftar', $data, true);
            exit();
        }
        //$data['DETIL'] = $this->actBC11->detailMANIFEST($tipe = "");
    }
    /*
    function view_detail_manifest($act,$jns,$ftz,$car,$kdgrup) {print_r('sini');die();
        print_r($act);die();
        if ($act == 'viewform') {
            $dataForm = $this->actBC11->viewdaftarGroup($car,$jns,$ftz,$kdgrup);
            echo $this->load->view('bc11/viewgrupManifest', $dataForm, true, '');
        } elseif ($act == 'proses') {
            echo $this->detailMANIFin();//belum diproses
            die();
        }
    }
    */
    function lstDetilMANIF($car) {
        $data = $this->actBC11->daftarGroup($car);
        if ($this->input->post('ajax')) {
            echo $data;
        } else {
            echo $this->load->view('daftar', $data, true);
            exit();
        }
    }
    
    function detailMANIFin($act,$car,$kdgrup,$jumpos,$nopos,$nopossub,$nopossubsub) {
        //print_r($act.'|'.$car.'|'.$kdgrup.'|'.$jumpos.'|'.$nopos);
        $this->load->model("actbc11");
        $act = $this->input->post("act");
        if ($act == "add" || $act == "edit") {
            $rtn = $this->actBC11->setDetailMANIFin($act,$car,$kdgrup,$jumpos,$nopos,$nopossub,$nopossubsub);
            echo($rtn);
        } else {
            echo $this->actbc11->setDetailMANIFin($act,$car,$kdgrup,$jumpos,$nopos,$nopossub,$nopossubsub);
        }
        die();
    } 
    
    function detilMANIFESTlist() {
        $data = $this->actBC11->lsvHeaderMANIFEST();
        //var_dump($data);
        if ($this->input->post('ajax')) {
            echo $data;
        } else {
            echo $this->load->view('daftar', $data, true);
            exit();
        }
    }
/*
    function detilMANIFEST($car) {
        $data = $this->actBC11->getHeaderMANIFEST($car);
        echo $this->load->view('bc11/detilMANIFEST', $data, true);
        exit();
    }
  */  
    function createPOPDetailMANIFEST($act,$car,$kdgrup,$jumpos,$nopos,$nopossub,$nopossubsub) {
        //print_r($act.'|'.$car.'|'.$kdgrup.'|'.$jumpos.'|'.$nopos);
        if ($act == 'proses') {
            echo $this->detailMANIFin($act,$car,$kdgrup,$jumpos,$nopos,$nopossub,$nopossubsub);
            die();
        } elseif ($act == 'viewform') {
            $dataForm = $this->actBC11->getTabelDetailPOPMANIFEST($car,$kdgrup,$jumpos,$nopos,$nopossub,$nopossubsub);
            echo $this->load->view('bc11/viewgrupManifest', $dataForm, true, '');
        } elseif ($act == 'delete') {
            $key = $this->input->post('tb_chkfpopDetilManifest');
            echo $this->actBC11->delManifdtl($key);
            die();
        }
    }
         
    
    function createMANIFEST($car) {
        $data = $this->actBC11->getHeaderMANIFEST($car);       
        echo $this->load->view('bc11/createMANIFEST', $data, true);
        exit();
    }
    function daftarGroup($jns='I',$ftz='0',$car){//print_r('$car');
        $data = $this->actBC11->daftarGroup($jns,$ftz,$car);
        if ($this->input->post('ajax')) {
            echo $data;
        } else {
            echo $this->load->view('daftar', $data, true);
            exit();
        }
    }
    
    
    //Agung

    function refreshHeader($car) {
        $data = $this->actBC11->getHeaderMANIFEST($car);
        echo $this->load->view("bc11/createMANIFEST", $data, true);
        die();
    }
    /*
    function refreshGrup($car) {
        $data = $this->actBC11->daftarGroup($car);
        echo $this->load->view('daftar', $data, true);
        die();
    }

    function refreshkontainer($car, $kdgrup, $nopos, $nopossub, $nopossubsub, $seri) {
        $data = $this->actBC11->lstKontainer($car, $kdgrup, $nopos, $nopossub, $nopossubsub, $seri);
        echo $this->load->view("bc11/frmkontainer", $data, true);
        die();
    }
*/      
    function kontainer($car,$kdgrup,$nopos,$nopossub,$nopossubsub) {//print_r($car.'|'.$kdgrup.'|'.$nopos.'|'.$nopossub.'|'.$nopossubsub);die();
        if ($this->input->post('act') == 'edit') {
            foreach ($this->input->post('tb_chkfkontainer') as $chkitem) {
                $arrchk = explode("|", $chkitem);
            }
            $car            = $arrchk[0];
            $seri           = $arrchk[1];
            $kdgrup         = $arrchk[2];
            $nopos          = $arrchk[3];  
            $nopossub       = $arrchk[4];
            $nopossubsub    = $arrchk[5];
            $data   = $this->actBC11->lstKontainer($car, $kdgrup, $nopos, $nopossub, $nopossubsub, $seri);         
            $data['act'] = 'update';
            return $this->load->view("bc11/frmkontainer", $data);
        }
        elseif ($this->input->post('act') == 'add'){
            $data = $this->actBC11->lstKontainer($car,$kdgrup,$nopos,$nopossub,$nopossubsub);//print_r($data);die();
            return $this->load->view("bc11/frmkontainer", $data);
        }
        else {
            echo $this->actBC11->setKontainer($this->input->post('act'), $car,$kdgrup,$nopos,$nopossub,$nopossubsub);
        }
    }
    
     function dokumen($car,$kdgrup,$nopos,$nopossub,$nopossubsub) {
        if ($this->input->post('act') == 'edit') {
            foreach ($this->input->post('tb_chkfdokumen') as $chkitem) {
                $arrchk = explode("|", $chkitem);
            }
            $car            = $arrchk[0];
            $seri           = $arrchk[1];
            $kdgrup         = $arrchk[2];
            $nopos          = $arrchk[3];  
            $nopossub       = $arrchk[4];
            $nopossubsub    = $arrchk[5];
            $data   = $this->actBC11->lstDokumen($car, $kdgrup, $nopos, $nopossub, $nopossubsub, $seri);
            $data['act'] = 'update';
            return $this->load->view("bc11/frmdokumen", $data);
        }
        elseif ($this->input->post('act') == 'add') {
            $data = $this->actBC11->lstDokumen($car,$kdgrup,$nopos,$nopossub,$nopossubsub);
            return $this->load->view("bc11/frmdokumen", $data);
        } 
        else {
            echo $this->actBC11->setDokumen($this->input->post("act"), $car);
        }
    }
    
    function barang($car,$kdgrup,$nopos,$nopossub,$nopossubsub) {
        if ($this->input->post('act') == 'edit') {
            foreach ($this->input->post('tb_chkfbarang') as $chkitem) {
                $arrchk = explode("|", $chkitem);
            }
            $car            = $arrchk[0];
            $seri           = $arrchk[1];
            $kdgrup         = $arrchk[2];
            $nopos          = $arrchk[3];  
            $nopossub       = $arrchk[4];
            $nopossubsub    = $arrchk[5];
            $data   = $this->actBC11->lstUrBarang($car, $kdgrup, $nopos, $nopossub, $nopossubsub, $seri);//print_r($data);die();
            $data['act'] = 'update';
            return $this->load->view("bc11/frmbarang", $data);
        }
        elseif ($this->input->post('act') == 'add'){
            $data = $this->actBC11->lstUrBarang($car,$kdgrup,$nopos,$nopossub,$nopossubsub);
            return $this->load->view("bc11/frmbarang", $data);
        }
        else {
            echo $this->actBC11->setBarang($this->input->post('act'), $car);
        }
    }
    
    function daftarDetil($type, $aju, $kdgrup, $nopos, $nopossub, $nopossubsub) {//print_r($aju.'|'.$nopos.'|'.$kdgrup.'|'.$nopossub.'|'.$nopossubsub);die();
        $data = $this->actBC11->lstDetil($type, $aju, $kdgrup, $nopos, $nopossub, $nopossubsub);
        if ($this->input->post('ajax')) {
            echo $data;
        } else {
            echo $this->load->view('list', $data, true);
            exit();
        }
    }
    
    function pageNavManif($car,$kdgrup,$nopos,$seri) {
       //print_r($car.'|'.$kdgrup.'|'.$nopos.'|'.$seri);
        $data = $this->actBC11->getTabelDetailPOPMANIFEST($car,$kdgrup,$seri,$nopos,$nopossub,$nopossubsub);
        return $this->load->view("bc11/viewgrupManifest", $data);
    }
    
    /*
    function comboajaibManif($car,$kdgrup) {
        //print_r($car.'|'.$kdgrup);
        $data = $this->actBC11->getTabelDetailPOPMANIFEST($car,$kdgrup,$seri,$nopos,$nopossub,$nopossubsub);
        return $this->load->view("bc11/viewgrupManifest", $data);
    }
    */
    //validasi
    function alertcekstatusManifDTL($car, $kdgrup='', $nopos='', $nopossub='', $nopossubsub='') {
        //print_r($car.'|'.$kdgrup.'|'.$nopos.'|'.$jmnopos);//die();
        $this->load->model("actBC11");
        if ($car) {
            $data = $this->actBC11->validasiManifDTL($car, $kdgrup, $nopos, $nopossub, $nopossubsub);
        } else {
            $data = 'Cek status Error';
        }
        echo $data;
    }
    
    function viewgrupManifest($car, $kdgrup, $jmnopos){
        //print_r($car.'|'.$jmbrggab.'|'.$seri);DIE();
        $dataForm = $this->actBC30->getTabelDetailPOPMANIFEST($car, $kdgrup, $jmnopos);
        echo $this->load->view('bc11/viewgrupManifest', $dataForm, true);
        exit();
    }
    
    function queued() {
        $car = $this->input->post("CAR");
        $FF = $this->actBC11->genFlatfile($car);
        $arrFF = explode('|', $FF);
        print_r($arrFF); die();
        if ($arrFF[0] == '2') {
            $SNRF = $this->actBC11->crtQueue($car);
            if ($SNRF!='ERROR') {
                $data = $SNRF . '|' . $arrFF[1];
                $SNRF = $this->actBC11->updateSNRF($car, $data);
                $hsl = "===========================================================================================<BR>"
                        . "Bentuk Edifact berhasil.<BR>";
            } else {
                $hsl = "===========================================================================================<BR>"
                        . "Bentuk Edifact atau Pembatalan Edifact gagal.";
            }
        }
        elseif ($arrFF[0] == '1') {
            $hsl = "===========================================================================================<BR>"
                    . "Batal Edifact berhasil.<BR>";
        }
        else {
            $hsl = "===========================================================================================<BR>"
                    . "Bentuk Edifact atau Pembatalan Edifact gagal.<BR>"
                    . "===========================================================================================<BR>"
                    . $arrFF[0]
                    . "<BR>Silakan validasi data kembali.";
        }
        die($hsl);
    }
    
    function cetak($car, $jnPage, $frmSSPCP = 0) {        
        $dataArr = $this->uri->segment_array();
        $data = $this->actBC11->cetakDokumen($car, $jnPage, $dataArr);
        return $data;
    }
    
    function beforecetak($car) {
        echo $this->load->view('bc11/beforeCetak', array('car' => $car));
    }

}
