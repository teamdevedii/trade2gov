<?php
if (!defined('BASEPATH')){exit('No direct script access allowed');}
class bc11 extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('actBC11');
        if (!$this->newsession->userdata('LOGGED')) {echo '<script> window.location = site_url; </script>';exit();}
        return true;
    }
    
    function lstBC11(){
        $data = $this->actBC11->lstBC11();
        if($this->input->post('ajax')){
            echo $data;
        }else{
            echo $this->load->view('daftar', $data, true);
            exit();
        }
    }
    
    function lstGrup(){
        $data = $this->actBC11->lstGrup();
        if($this->input->post('ajax')){
            echo $data;
        }else{
            echo $this->load->view('daftar', $data, true);
        }
        exit();
    }
    
    function lstPost($car, $kdgrup, $ekspor, $status){
        $data = $this->actBC11->lstDetil($car, $kdgrup, $ekspor, $status);
        if($this->input->post('ajax')){
            echo $data;
        }else{
            echo $this->load->view('bc11/lst', $data, true);
        }
        exit();
    }
    
    function lstRespon($car){
        $data = $this->actBC11->lstRespon($car);
        if($this->input->post('ajax')){
            echo $data;
        }else{
            echo $this->load->view('bc11/beforeCetakRespon', $data, true);
        }
        exit();
    }
    
    function lstGroupPos($car) {
        $data = $this->actBC11->daftarPos($car);
        if ($this->input->post('ajax')) {
            echo $data;
        }
        else {
            echo $this->load->view('bc11/beforeCetakPos', $data, true);
        }
        exit();
    }
            
    function lstDataPendukung($type, $car, $kdgrup, $nopos, $nopossub, $nopossubsub){
        $arrdata        = $this->actBC11->lstDataPendukung($type, $car, $kdgrup, $nopos, $nopossub, $nopossubsub);
        $data['list']   = $this->load->view('list', $arrdata, true);
        $data['type']   = $type;
        if($this->input->post('ajax')){
            echo $arrdata;
        }else{
            echo $this->load->view("bc11/lst", $data, true);
        }
        exit();
    }
    
    function create($car, $status) {
        $Header         = $this->actBC11->getHeader($car);
        $data['HEADER'] = $this->load->view('bc11/header',$Header,true);
        $Detil          = $this->actBC11->lstGrup($car, $Header['HEADER']['JNSMANIFEST'], $Header['HEADER']['TIPEMANIFEST'], $status);
        $data['DETIL']  = $this->load->view('list', $Detil, true);
        echo $this->load->view('bc11/tabs', $data, true);
        exit();
    }
    
    function formDetil($car, $kdgrup, $nopos, $nopossub, $nopossubsub, $export, $status){
        $data   =   $this->actBC11->getDetil($car, $kdgrup, $nopos, $nopossub, $nopossubsub, $export, $status);
        echo $this->load->view('bc11/detil', $data, true);
        die();
    }
    
    function frmbarang($car, $kdgrup, $nopos, $nopossub, $nopossubsub, $status){
        $post   = $this->input->post();
        if($post['act'] == 'add' || $post['act'] == 'edit'){
            $data   =   $this->actBC11->getBarang($car, $kdgrup, $nopos, $nopossub, $nopossubsub, $post['tb_chkfbarang'][0], $status);
            echo $this->load->view('bc11/frmbarang', $data, true);
        }else{
            echo $this->actBC11->delBarang($car, $kdgrup, $nopos, $nopossub, $nopossubsub, $post['tb_chkfbarang'][0], $status);
        }
        die();
    }
    
    function frmkontainer($car, $kdgrup, $nopos, $nopossub, $nopossubsub, $status){
        $post   = $this->input->post();
        if($post['act'] == 'add' || $post['act'] == 'edit'){
            $data   =   $this->actBC11->getKontainer($car, $kdgrup, $nopos, $nopossub, $nopossubsub, $post['tb_chkfkontainer'][0], $status);
            echo $this->load->view('bc11/frmkontainer', $data, true);
        }else{
            echo $this->actBC11->delKontainer($car, $kdgrup, $nopos, $nopossub, $nopossubsub, $post['tb_chkfkontainer'][0], $status);
        }
        die();
    }
    
    function frmdokumen($car, $kdgrup, $nopos, $nopossub, $nopossubsub, $status){
        $post   = $this->input->post();
        if($post['act'] == 'add' || $post['act'] == 'edit'){
            $data   =   $this->actBC11->getDokumen($car, $kdgrup, $nopos, $nopossub, $nopossubsub, $post['tb_chkfdokumen'][0], $status);
            echo $this->load->view('bc11/frmdokumen', $data, true);
        }else{
            echo $this->actBC11->delDokumen($car, $kdgrup, $nopos, $nopossub, $nopossubsub, $post['tb_chkfdokumen'][0], $status);
        }
        die();
    }
    
    function beforeCetak($car){
        echo $this->load->view('bc11/beforeCetak', array('car' => $car));
    }
    
    function cetakDokumen($car, $jnPage, $arr ) {
        $this->load->library('fpdf');
        $this->load->model('actMain');
        $this->load->model('dokBC11');
        $this->dokBC11->ciMain($this->actMain);
        $this->dokBC11->fpdf($this->fpdf);
        $this->dokBC11->segmentUri($arr);
        $this->dokBC11->showPage($car, $jnPage);
    }
    
    function cetak($car, $jnPage, $frmSSPCP = 0) {
        $dataArr = $this->uri->segment_array();
        $data = $this->cetakDokumen($car, $jnPage, $dataArr);
        return $data;
    }
    
    function setHeader(){
        $data = $this->input->post();
        echo $this->actBC11->setHeader($data);
    }
    
    function setDetil(){
        $data = $this->input->post();
        echo $this->actBC11->setDetil($data);
    }
    
    function setBarang(){
        $data = $this->input->post();
        echo $this->actBC11->setBarang($data);
        die();
    }
    
    function delDetil(){
        $data = $this->input->post('tb_chkflstDetil');
        echo $this->actBC11->delDetil($data);
        die();
    }
    
    function delHeader(){
        $data = $this->input->post('tb_chkflstBC11');
        echo $this->actBC11->delHeader($data);
        die();
    }
    
    function setKontainer(){
        $data = $this->input->post();
        echo $this->actBC11->setKontainer($data);
        exit();
    }
    
    function cekKelengkapan($car){
        echo $this->actBC11->validasiHeader($car);
        exit();
    }
    
    function setDokumen(){
        $data = $this->input->post();
        echo $this->actBC11->setDokumen($data);
        exit();
    }
    
    function queued(){
        $car= $this->input->post("CAR");
        $FF = $this->actBC11->genFlatfile($car);
        $arrFF = explode('|', $FF);
        if ($arrFF[0] == '2') {
            $SNRF = $this->actBC11->crtQueue($car);
            if ($SNRF!='ERROR') {
                $data = $SNRF . '|' . $arrFF[1];
                $SNRF = $this->actBC11->updateSNRF($car, $data);
                $hsl = "===========================================================================================<BR>"
                     . "Bentuk Edifact berhasil.<BR>";
            } else {
                $hsl = "===========================================================================================<BR>"
                     . "Bentuk Edifact atau Pembatalan Edifact gagal.";
            }
        }
        elseif ($arrFF[0] == '1') {
            $hsl = "===========================================================================================<BR>"
                 . "Batal Edifact berhasil.<BR>";
        }
        else {
            $hsl = "===========================================================================================<BR>"
                    . "Bentuk Edifact atau Pembatalan Edifact gagal.<BR>"
                    . "===========================================================================================<BR>"
                    . $arrFF[0]
                    . "<BR>Silakan validasi data kembali.";
        }
        echo($hsl);
        exit();
    }
    
    function uploadFLT($act=''){
        if($act=='upload'){
            echo $this->actBC11->uploadFLT();
        }else{
            $data   = $this->actBC11->getDataLoad();
            echo $this->load->view('bc11/uploadFLT', $data, true);
        }
        exit();
    }
    
    function insertFLT(){
        $arr = $this->input->post();
        echo $this->actBC11->insertFLT($arr);
        die();
    }
}