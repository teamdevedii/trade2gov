<?php
if (!defined('BASEPATH')){exit('No direct script access allowed');}
class bc20 extends CI_Controller {
    var $kd_trader = '';
    public function __construct() {
        parent::__construct();
        $this->load->model('actBC20');
        $this->kd_trader = $this->newsession->userdata('KODE_TRADER');
        if (!$this->newsession->userdata('LOGGED')) {echo '<script> window.location = site_url; </script>';exit();}
        return true; 
    }

    function create() {
        $dataHeader = $this->actBC20->get_header();
        $data['HEADER'] = $this->load->view('bc20/header',$dataHeader,true);
        $data['DETIL']  = 'Detil';
        echo $this->load->view('bc20/tabs', $data, true);
        exit();
    }
    
    function refreshHeader($car) {
        $data = $this->actBC20->get_header($car);
        echo $this->load->view("bc20/header", $data, true);
        die();
    }

    function header() {
        $this->actBC20->set_header($this->input->post("act"));
    }

    function daftar($tipe = "") {
        $data = $this->actBC20->daftar($tipe);
        if ($this->input->post('ajax')) {
            echo $data;
        } else {
            echo $this->load->view('daftar', $data, true);
            exit();
        }
    }

    function daftarDetil($type, $aju) {
        $data = $this->actBC20->lstDetil($type, $aju);
        if ($this->input->post('ajax')) {
            echo $data;
        } else {
            echo $this->load->view('list', $data, true);
            exit();
        }
    }

    function edit($aju) {
        $dataHeader = $this->actBC20->get_header($aju);
        $data['HEADER'] = $this->load->view('bc20/header',$dataHeader,true);
        $dataDetil = $this->actBC20->lstDetil('barang',$aju);
        $data['DETIL']  = $this->load->view('bc20/lstbarang',$dataDetil,true);
        echo $this->load->view('bc20/tabs', $data, true);
        exit();
    }

    function kontainer($car) {
        if ($this->input->post('act') == 'edit') {
            foreach ($this->input->post('tb_chkfkontainer') as $chkitem) {
                $arrchk = explode("|", $chkitem);
            }
            $car    = $arrchk[0];
            $contno = $arrchk[1];
            $data   = $this->actBC20->lstKontainer($car, $contno, $this->kd_trader);
            $data['act'] = 'update';
            return $this->load->view("bc20/frmkontainer", $data);
        }
        elseif ($this->input->post('act') == 'add'){
            $data = $this->actBC20->lstKontainer($car);
            return $this->load->view("bc20/frmkontainer", $data);
        }
        else {
            echo $this->actBC20->setKontainer($this->input->post('act'), $car);
        }
    }

    function kemasan($car) {
        if ($this->input->post('act') == 'edit') {
            foreach ($this->input->post('tb_chkfkemasan') as $chkitem) {
                $arrchk = explode("|", $chkitem);
            }
            $car = $arrchk[0];
            $jns = $arrchk[1];
            $merk = $arrchk[2];
            $data = $this->actBC20->lstKemasan($car, $jns, $merk);
            $data['act'] = 'update';
            return $this->load->view("bc20/frmkemasan", $data);
        }
        elseif ($this->input->post('act') == 'add'){
            $data = $this->actBC20->lstKemasan($car);
            return $this->load->view("bc20/frmkemasan", $data);
        }
        else {
            echo $this->actBC20->setKemasan($this->input->post('act'), $car);
        }
    }

    function dokumen($car) {
        if ($this->input->post('act') == 'edit') {
            foreach ($this->input->post('tb_chkfdokumen') as $chkitem) {
                $arrchk = explode("|", $chkitem);
            }
            $car    = $arrchk[0];
            $DOKKD  = $arrchk[1];
            $DOKNO  = $arrchk[2];
            $data   = $this->actBC20->lstDokumen($car, $DOKKD, $DOKNO);
            return $this->load->view("bc20/frmdokumen", $data);
        }
        elseif ($this->input->post('act') == 'add') {
            $data = $this->actBC20->lstDokumen($car);
            return $this->load->view("bc20/frmdokumen", $data);
        } 
        else {
            echo $this->actBC20->setDokumen($this->input->post("act"), $car);
        }
    }

    function hitungan($data = '') {
        $data = urldecode($data);
        $this->load->model('actMain');
        $arr['data']            = $data;
        $arr['KODE_HARGA']      = $this->actMain->get_mtabel('KODE_HARGA');
        $arr['KODE_ASURANSI']   = $this->actMain->get_mtabel('KODE_ASURANSI', 1, FALSE);
        $this->load->view('bc20/hitungan', $arr);
    }

    function indentor(){
        $data = $this->input->post('data');
        $this->load->model('actMain');
        $arr['data']    = $data;
        $arr['INDID']   = $this->actMain->get_mtabel('JENIS_IDENTITAS', 1, false, '', 'KODEDANUR');
        $this->load->view('bc20/indentor', $arr);
    }
    
    function barang($aju = "", $seri = "") {
        $post = $this->input->post();
        if ($post['act'] == 'edit') {
            $arrchk         = explode("|", $post['tb_chkfbarang'][0]);
            $aju            = $arrchk[0];
            $seri           = $arrchk[1];
            $data           = $this->actBC20->lstBarang($aju, $seri);
            $data['act']    = 'update';
            return $this->load->view("bc20/frmbarang", $data);
        }
        elseif ($post['act'] == 'add') {
            $data = $this->actBC20->lstBarang($aju);
            $data['act'] = 'save';
            return $this->load->view('bc20/frmbarang', $data);
        }
        else {
            echo $this->actBC20->setBarang($post['act'],$aju);
        }
    }

    function pageBarang($car, $seri) {
        $data = $this->actBC20->lstBarang($car, $seri);
        return $this->load->view("bc20/frmbarang", $data);
    }

    function getBm() {
        $data = $this->actBC20->lstBarang();
        echo $this->load->view("bc20/bm", $data);
    }

    function alertcekstatus($aju = "", $type = "") {
        if ($aju && $type) {
            list($status, $judul, $sts) = $this->actBC20->validasi($aju, $type);
        } else {
            $status = 'Cek status Error aju dan type tidak ada.';
        }
        $data = array(
            'data' => $status,
            'judul' => $judul,
            'status' => $sts);
        $this->load->view('bc20/notifcek', $data);
    }

    function cekStatusDtl($aju, $seri='1') {
        if ($aju && $seri) {
            $arr = $this->actBC20->validasiDetil($aju, $seri);
        }
        else {
            $arr[0] = 'Cek status Error';
            $arr[1] = 'ER';
        }
        $judul = 'Hasil validasi Barang dengan aju : ' . substr($aju, 20) . ' tanggal :' . substr($aju, 12, 8) . '  Seri :' . $seri;
        $data = array(
            'data' => $arr[0],
            'judul' => $judul);
        echo $arr[1] . '|' . $this->load->view('bc20/notifcek', $data, true);
    }

    function beforecetak($car) {
        echo $this->load->view('bc20/beforeCetak', array('car' => $car));
    }

    function copydata($car){
        echo $this->load->view('bc20/beforeCopy', array('car' => $car));
    }
    
    function delete(){
        $data   = $this->input->post('tb_chkfdraft');
        $car    = $data[0];
        echo $this->actBC20->deleteDokumen($car);
        die();
    }
    
    function beforecetakrespon($car) {
        $this->load->model("actBC20");
        $data = $this->actBC20->daftarRespon($car,'cetakrespon');
        if ($this->input->post('ajax')) {
            echo $data;
        }
        else {
            echo $this->load->view('bc20/beforeCetakRespon', $data, true);
        }
        exit();
    }

    function cetak($car, $jnPage, $frmSSPCP = 0) {
        if ($jnPage == '2' && $frmSSPCP > 0) {
            $data = $this->actBC20->getSSPCP($car);
            echo $this->load->view('bc20/SSPCP', array('car' => $car, 'SSPCP' => $data));
            return false;
        }
        $dataArr = $this->uri->segment_array();
        $data = $this->actBC20->cetakDokumen($car, $jnPage, $dataArr);
        return $data;
    }

    function setSSPCP() {
        $data = $this->actBC20->setSSPCP();
        return $data;
    }

    function queued($srp='') {
        $car = $this->input->post("CAR");
        $FF = $this->actBC20->genFlatfile($car,$srp);
        $arrFF = explode('|', $FF);
        if ($arrFF[0] == '2') {
            $SNRF = $this->actBC20->crtQueue($car);
            if ($SNRF!='ERROR') {
                $data = $SNRF . '|' . $arrFF[1];
                $SNRF = $this->actBC20->updateSNRF($car, $data);
                $hsl = "===========================================================================================<BR>"
                     . "Bentuk Edifact berhasil.<BR>";
            } else {
                $hsl = "===========================================================================================<BR>"
                     . "Bentuk Edifact atau Pembatalan Edifact gagal.";
            }
        }
        elseif ($arrFF[0] == '1') {
            $hsl = "===========================================================================================<BR>"
                 . "Batal Edifact berhasil.<BR>";
        }
        else {
            $hsl = "===========================================================================================<BR>"
                    . "Bentuk Edifact atau Pembatalan Edifact gagal.<BR>"
                    . "===========================================================================================<BR>"
                    . $arrFF[0]
                    . "<BR>Silakan validasi data kembali.";
        }
        echo($hsl);
        exit();
    }

    function pungutanDtl($car) {
        $data['data_pgt'] = $this->actBC20->getPungutan($car);
        echo $this->load->view('bc20/detilpungutan', $data);
    }

    function getPPh($nilPPh) {
        $data['NILPPH'] = $nilPPh;
        echo $this->load->view('bc20/PPh', $data);
    }
    
    function listBarang($car){
        $tblBarang = $this->actBC20->lstDetil('barang',$car);
        $data['list']   = $this->load->view('list', $tblBarang, true);
        print_r($this->load->view("bc20/lstbarang", $data));
    }
    
    function uploadbup($act){
        if($act=='upload'){
            echo $this->actBC20->uploadBUP();
        }
        elseif($act=='viewform'){
            echo $this->load->view('bc20/uploadBUP','',true);
        }
        die();
    }
    
    function downloadbup($act,$car='',$EDINUMBER='',$file=''){
        if($act=='download'){
            echo $this->actBC20->downloadBUP();
            die();
        }
        elseif($act=='file'){
            //
            if($car!='' && $EDINUMBER!='' && $file!=''){
                $this->load->helper('download');
                $paht_local = getcwd() . '/TRANSACTION/' . $EDINUMBER . '/' . $file . '.BUP';
                ob_start();
                $data = file_get_contents($paht_local);
                force_download($car.'.BUP', $data);
                ob_flush();
            }
            exit();
        }
    }
    
    function getSRP($car){
        //$data = $this->input->post('data');
        //$this->load->model('actMain');
        $arr['CAR']    = $car;
        //$arr['INDID']   = $this->actMain->get_mtabel('JENIS_IDENTITAS', 1, false, '', 'KODEDANUR');
        $this->load->view('bc20/srp', $arr);
        //die('dari controler');
    }
    /*
    function test(){
        $this->load->model('actBC20');
        $exe = $this->actBC20->testTranslasi();
        print_r($exe);
        die();
    }
    */
}