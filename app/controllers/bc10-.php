<?php
if (!defined('BASEPATH')){exit('No direct script access allowed');}
class bc10 extends CI_Controller {
    var $kd_trader = '';
    public function __construct() {
        parent::__construct();
        $this->load->model('actBC10');
        $this->kd_trader = $this->newsession->userdata('KODE_TRADER');
        if (!$this->newsession->userdata('LOGGED')) {redirect();}
        return true;
    }
    
    function alertcekstatus($car = "") {
        $this->load->model("actBC10");
        if ($car) {
            $data = $this->actBC10->validasiHDRRKSP($car);
        } else {
            $data = 'Cek status Error';
        }
        echo $data;       
    }
    
    function daftarRKSP($tipe = "") {
        $data = $this->actBC10->lsvHeaderRKSP($tipe = "");
        if($this->input->post('ajax')){
            echo $data;
        }else{
            echo $this->load->view('daftar', $data, true);
            exit();
        }
    }
    
    function delRKSPhdr(){
        //print_r('sini'); die();
        $key = $this->input->post('tb_chkfhdrRKSP');
        echo $this->actBC10->delRKSPhdr($key);
        die();
    }
    
    function createRKSP($car){
	$data = $this->actBC10->getHeaderRKSP($car);
        echo $this->load->view('bc10/createRKSP', $data, true);
        exit();
    }
    
    function insert_jadwal_kapal($act, $car) {
        if ($act == 'proses') {
            echo $this->detailRKSPin();
            die();
        }
    }
    
    function detailRKSPin() {
        $this->load->model("actbc10");
        $act = $this->input->post("act");
        if ($act == "add" || $act == "edit") {
            $rtn = $this->actBC10->setRKSPin($act);
            echo($rtn);
        } else {
            echo $this->actbc10->setRKSPin($act);
        }
        die();
    }
    
    function lstDetilRKSP($car) {
        $data = $this->actBC10->getHeaderRKSP($car);
        if ($this->input->post('ajax')) {
            echo $data;
        } else {
            echo $this->load->view('daftar', $data, true);
            exit();
        }
    }
    
    function refreshHeader($car) {
        $data = $this->actBC10->getHeaderRKSP($car);
        echo $this->load->view("bc10/createRKSP", $data, true);
        die();
    }

     function header() {
        $this->actbc10->set_header($this->input->post("act"));
    }

    function daftar($tipe = "") {
        $data = $this->actBC20->daftar($tipe);
        if ($this->input->post('ajax')) {
            echo $data;
        } else {
            echo $this->load->view('daftar', $data, true);
            exit();
        }
    }

    function daftarDetil($type, $aju) {
        $data = $this->actBC20->lstDetil($type, $aju);
        if ($this->input->post('ajax')) {
            echo $data;
        } else {
            echo $this->load->view('list', $data, true);
            exit();
        }
    }

    function edit($aju) {
        $dataHeader = $this->actBC20->get_header($aju);
        $data['HEADER'] = $this->load->view('bc20/header',$dataHeader,true);
        $dataDetil = $this->actBC20->lstDetil('barang',$aju);
        $data['DETIL']  = $this->load->view('bc20/lstbarang',$dataDetil,true);;
        echo $this->load->view('bc20/tabs', $data, true);
        exit();
        
    }

    function kontainer($car) {
        if ($this->input->post('act') == 'edit') {
            foreach ($this->input->post('tb_chkfkontainer') as $chkitem) {
                $arrchk = explode("|", $chkitem);
            }
            $car    = $arrchk[0];
            $contno = $arrchk[1];
            $data   = $this->actBC20->lstKontainer($car, $contno, $this->kd_trader);
            $data['act'] = 'update';
            return $this->load->view("bc20/frmkontainer", $data);
        }
        elseif ($this->input->post('act') == 'add'){
            $data = $this->actBC20->lstKontainer($car);
            return $this->load->view("bc20/frmkontainer", $data);
        }
        else {
            echo $this->actBC20->setKontainer($this->input->post('act'), $car);
        }
    }

    function kemasan($car) {
        if ($this->input->post('act') == 'edit') {
            foreach ($this->input->post('tb_chkfkemasan') as $chkitem) {
                $arrchk = explode("|", $chkitem);
            }
            $car = $arrchk[0];
            $jns = $arrchk[1];
            $merk = $arrchk[2];
            $data = $this->actBC20->lstKemasan($car, $jns, $merk);
            $data['act'] = 'update';
            return $this->load->view("bc20/frmkemasan", $data);
        }
        elseif ($this->input->post('act') == 'add'){
            $data = $this->actBC20->lstKemasan($car);
            return $this->load->view("bc20/frmkemasan", $data);
        }
        else {
            echo $this->actBC20->setKemasan($this->input->post('act'), $car);
        }
    }

    function dokumen($car) {
        if ($this->input->post('act') == 'edit') {
            foreach ($this->input->post('tb_chkfdokumen') as $chkitem) {
                $arrchk = explode("|", $chkitem);
            }
            $car    = $arrchk[0];
            $DOKKD  = $arrchk[1];
            $DOKNO  = $arrchk[2];
            $data   = $this->actBC20->lstDokumen($car, $DOKKD, $DOKNO);
            return $this->load->view("bc20/frmdokumen", $data);
        }
        elseif ($this->input->post('act') == 'add') {
            $data = $this->actBC20->lstDokumen($car);
            return $this->load->view("bc20/frmdokumen", $data);
        } 
        else {
            echo $this->actBC20->setDokumen($this->input->post("act"), $car);
        }
    }

     function setHeaderRKSP() {
        $exe = $this->actBC10->setHeaderRKSPin();
        echo $exe;
        exit();
    }
    
    function hitungan($data = '') {
        $this->load->model('actMain');
        $arr['data']            = $data;
        $arr['KODE_HARGA']      = $this->actMain->get_mtabel('KODE_HARGA');
        $arr['KODE_ASURANSI']   = $this->actMain->get_mtabel('KODE_ASURANSI', 1, FALSE);
        $this->load->view('bc20/hitungan', $arr);
    }

    function barang($aju = "", $seri = "") {
        $post = $this->input->post();
        if ($post['act'] == 'edit') {
            $arrchk         = explode("|", $post['tb_chkfbarang'][0]);
            $aju            = $arrchk[0];
            $seri           = $arrchk[1];
            $data           = $this->actBC20->lstBarang($aju, $seri);
            $data['act']    = 'update';
            return $this->load->view("bc20/frmbarang", $data);
        }
        elseif ($post['act'] == 'add') {
            $data = $this->actBC20->lstBarang($aju);
            $data['act'] = 'save';
            return $this->load->view('bc20/frmbarang', $data);
        }
        else {
            echo $this->actBC20->setBarang($post['act'],$aju);
        }
    }

    function pageBarang($car, $seri) {
        $data = $this->actBC20->lstBarang($car, $seri);
        return $this->load->view("bc20/frmbarang", $data);
    }

    function getBm() {
        $data = $this->actBC20->lstBarang();
        echo $this->load->view("bc20/bm", $data);
    }

    function cekStatusDtl($aju, $seri='1') {
        if ($aju && $seri) {
            list($status, $hasil) = $this->actBC20->validasiDetil($aju, $seri);
        } 
        else {
            $status = 'Cek status Error';
        }
        $judul = 'Hasil validasi Barang dengan aju : ' . substr($aju, 20) . ' tanggal :' . substr($aju, 12, 8) . '  Seri :' . $seri;
        $data = array(
            'data' => $status,
            'judul' => $judul);
        echo $hasil . '|' . $this->load->view('bc20/notifcek', $data, true);
    }

    function beforecetak($car) {
        echo $this->load->view('bc10/beforeCetak', array('car' => $car));
    }

    function copydata($car){
        echo $this->load->view('bc20/beforeCopy', array('car' => $car));
    }
    
    function beforecetakrespon($car) {
        $this->load->model("actBC20");
        $data = $this->actBC20->daftarRespon($car,'cetakrespon');
        if ($this->input->post('ajax')) {
            echo $data;
        }
        else {
            echo $this->load->view('bc20/beforeCetakRespon', $data, true);
        }
        exit();
    }

    function cetak($car, $jnPage, $frmSSPCP = 0) {        
        $dataArr = $this->uri->segment_array();
        $data = $this->actBC10->cetakDokumen($car, $jnPage, $dataArr);
        return $data;
    }

    function setSSPCP() {
        $data = $this->actBC20->setSSPCP();
        return $data;
    }

    function queued() {
        $car = $this->input->post("CAR");
        $FF = $this->actBC10->genFlatfile($car);
        $arrFF = explode('|', $FF);
        //print_r($arrFF); die();
        if ($arrFF[0] == '2') {
            $SNRF = $this->actBC10->crtQueue($car);
            if ($SNRF!='ERROR') {
                $data = $SNRF . '|' . $arrFF[1];
                $SNRF = $this->actBC10->updateSNRF($car, $data);
                $hsl = "===========================================================================================<BR>"
                        . "Bentuk Edifact berhasil.<BR>";
            } else {
                $hsl = "===========================================================================================<BR>"
                        . "Bentuk Edifact atau Pembatalan Edifact gagal.";
            }
        }
        elseif ($arrFF[0] == '1') {
            $hsl = "===========================================================================================<BR>"
                    . "Batal Edifact berhasil.<BR>";
        }
        else {
            $hsl = "===========================================================================================<BR>"
                    . "Bentuk Edifact atau Pembatalan Edifact gagal.<BR>"
                    . "===========================================================================================<BR>"
                    . $arrFF[0]
                    . "<BR>Silakan validasi data kembali.";
        }
        die($hsl);
    }

    function pungutanDtl($car) {
        $data['data_pgt'] = $this->actBC20->getPungutan($car);
        echo $this->load->view('bc20/detilpungutan', $data);
    }

    function getPPh($nilPPh) {
        $data['NILPPH'] = $nilPPh;
        echo $this->load->view('bc20/PPh', $data);
    }
    /*
    function test(){
        $this->load->model('actBC20');
        $exe = $this->actBC20->testTranslasi();
        print_r($exe);
        die();
    }
    */
    function listBarang($car){
        $tblBarang = $this->actBC20->lstDetil('barang',$car);
        $data['list']   = $this->load->view('list', $tblBarang, true);
        print_r($this->load->view("bc20/lstbarang", $data));
    }
    
    function uploadbup($act){
        if($act=='upload'){
            echo $this->actBC20->uploadBUP();
            die();
        }elseif($act=='viewform'){
            echo $this->load->view('bc20/uploadBUP','');
        }
    } 
}