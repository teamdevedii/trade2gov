<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');
class referensi_1 extends CI_Controller {
    function autoComplate($strJns, $by = "") {
        $this->load->model("actAutocomplate");
        $keys = $this->input->post('keys', TRUE);
        $grp = $this->input->post('grp', TRUE);
        $json_array = $this->actAutocomplate->getData($strJns, $keys, $by, $grp);
        echo json_encode($json_array);
    }
    
    function checkCode($type){
        $this->load->model("actAutocomplate");
        $data = $this->input->post();
        echo $this->actAutocomplate->checkCode($type,$data);
    }
    
    function dataPendukung($type, $key = '') {
        $key = urldecode($key);
        $this->load->model("actReferensi_1");
        $data = $this->actReferensi_1->lsvDataPendukung($type, $key);
        if ($this->input->post('ajax')) {
            echo $data;
        } 
        else {
            echo $this->load->view('daftar', array_merge(array('judul' => 'Daftar ' . $type), $data), true);
        }
        exit();
    }
    
    function parties() {
        $this->load->model("actReferensi_1");
        $act = $this->input->post("act");
        if ($act == "edit") {
            foreach ($this->input->post('tb_chkfparties') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $kd_trader   = $arrchk[0];
                $kd_parties  = $arrchk[1];
                $nama_paries = $arrchk[2];
            }
            $data = $this->actReferensi_1->getParties($act, $kd_trader, $kd_parties, $nama_paries);
            $data = array_merge($data, array('act' => "update", 'judul' => 'Edit Data Parties'));
            return $this->load->view("referensi/frmParties", $data);
        }
        elseif ($act == "add") {
            $data = $this->actReferensi_1->getParties($act);
            $data = array_merge($data, array('act' => "insert"));
            return $this->load->view("referensi/frmParties", $data);
        }
        else {
            echo $this->actReferensi_1->setParties($this->input->post("act"));
        }
        die();
    }
    
    function pembeli() {
        $this->load->model("actReferensi_1");
        $act = $this->input->post("act");
        if ($act == "edit") {
            foreach ($this->input->post('tb_chkfpembeli') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $kd_trader = $arrchk[0];
                $nm_beli   = $arrchk[1];
            }
            $data = $this->actReferensi_1->getPembeli($act, $kd_trader, $nm_beli);
            $data = array_merge($data, array('act' => "update", 'judul' => 'Edit Data Parties'));
            return $this->load->view("referensi/frmPembeli", $data);
        }
        elseif ($act == "add") {
            $data = $this->actReferensi_1->getPembeli($act);
            $data = array_merge($data, array('act' => "insert"));
            return $this->load->view("referensi/frmPembeli", $data);
        }
        else {
            echo $this->actReferensi_1->setPembeli($this->input->post("act"));
        }
        die();
    }
    
    function indentorEks() {
        $this->load->model("actReferensi_1");
        $act = $this->input->post("act");
        if ($act == "edit") {
            foreach ($this->input->post('tb_chkfindentoreks') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $kd_trader = $arrchk[0];
                $id_qq     = $arrchk[1];
                $npwp_qq   = $arrchk[2];
            }
            $data = $this->actReferensi_1->getIndentorEks($act, $kd_trader, $id_qq, $npwp_qq);
            $data = array_merge($data, array('act' => "update", 'judul' => 'Edit Data Indentor Eksportir'));
            return $this->load->view("referensi/frmIndentorEks", $data);
        }
        elseif ($act == "add") {
            $data = $this->actReferensi_1->getIndentorEks($act);
            $data = array_merge($data, array('act' => "insert"));
            return $this->load->view("referensi/frmIndentorEks", $data);
        }
        else {
            echo $this->actReferensi_1->setIndentorEks($this->input->post("act"));
        }
        die();
    }
   

}
?>