<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');

class Search extends CI_Controller {
    function getsearch($tipe = "", $indexField = "", $formName = "", $getdata = "") {
        $this->load->model("actSearch");
        $arrdata = $this->actSearch->search($tipe, $indexField, $formName, $getdata);
        $data = $this->load->view("listview", $arrdata, true);
        if($this->input->post("ajax")){
            echo $arrdata;
        }else{
            echo $data;
        }
    }

}