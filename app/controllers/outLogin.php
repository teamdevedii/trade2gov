<?php
error_reporting(E_ERROR);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class outLogin extends CI_Controller {
    var $kd_trader = '';

    public function __construct() {
        parent::__construct();
        $this->kd_trader = $this->newsession->userdata('KODE_TRADER');
        return true;
    }
    
    function regCustomer($act){
        $this->load->model('actOut');
        $this->load->model('actUser');
        $insert = explode('|', $this->actOut->setRegistrasi($act));
        if ($insert[0]=='1') {
            echo 'MSG|OK|' . site_url('usermanage/viwApproval/' . $insert[1]) . '|Silahkan Lengkapi Data Dokumen Terdaftar dan Data EDI';
            exit();
        }elseif ($insert[0]=='0') {
            echo "MSG|ER||Registrasi gagal.";
            exit();
        }else{
            echo implode('|', $insert);exit();
        }       
    }
    
    function callOut($type='',$return=false){
        if ($this->newsession->userdata('LOGGED')) $this->index();
        switch ($type){
            case 'home':
                $data = $this->load->view('frmHome','', true);
            break;
            case 'login':
                $data = $this->load->view('frmLogin','', true);
            break;
            case 'request';
                $this->load->model("actOut");
		$arrdata = $this->actOut->getRequest();
                $data = $this->load->view('frmRequest',$arrdata, true);
            break;
            case 'test';
                $data = $this->load->view('frmTest','', true);
            break;
        }
        if($return){
            return $data;
        }else{
            echo $data;
            exit;
        }
    }
    
    function testFunction(){
        /*$this->load->model("actMain");
        echo date("Y-m-d",strtotime(date()." +2 day"));die();*/
        //$arr    = abs(100.45 - 100);
        
        //print_r($arr);
        $tes = strpos("|010|020|",'030').' test';
        echo $tes;
        die();
    }
    
    function setRequest (){
        $this->load->model("actOut");
        $exe = $this->actOut->setRequest();
        return $exe;
        exit();        
    }
    
}