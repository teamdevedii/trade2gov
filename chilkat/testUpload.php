<?php

// The "chilkat_9_5_0.php" is included in the Chilkat PHP Extension download
// The version number (9_5_0) should match version of the Chilkat extension used.
include("chilkat_9_5_0.php");
die('tset');
$ftp = new CkFtp2();
//  Any string unlocks the component for the 1st 30-days.
$success = $ftp->UnlockComponent('Anything for 30-day trial');
if ($success != true) {
    print $ftp->lastErrorText() . "\n";
    exit;
}

$ftp->put_Hostname('10.1.5.78');
$ftp->put_Username('Administrator');
$ftp->put_Password('Bismillah');

//  Connect and login to the FTP server.
$success = $ftp->Connect();
if ($success != true) {
    print $ftp->lastErrorText() . "\n";
    exit;
}

//  Change to the remote directory where the file will be uploaded.
$success = $ftp->ChangeRemoteDir('wsT2G/files');
if ($success != true) {
    print $ftp->lastErrorText() . "\n";
    exit;
}

$localFilename = getcwd() . '/Transaction/PHA021041498/2.BUP';
$remoteFilename = '2.BUP';

//  Begin the upload in a background thread.
//  Only 1 background upload or download may be active at any time.
//  (per instance of an FTP object)
//  To append to an existing file in the background, call
//  AsyncAppendFileStart instead.
$success = $ftp->AsyncPutFileStart($localFilename,$remoteFilename);
if ($success != true) {
    print $ftp->lastErrorText() . "\n";
    exit;
}

//  The application is now free to do anything else
//  while the file is uploading.
//  For this example, we'll simply sleep and periodically
//  check to see if the transfer if finished.  While checking
//  however, we'll report on the progress in both number
//  of bytes tranferred and performance in bytes/second.
while ($ftp->get_AsyncFinished() != true) {

    print $ftp->get_AsyncBytesSent() . ' bytes sent' . " -- " . $ftp->get_UploadRate() . ' bytes per second';
    //  Sleep 1 second.
    $ftp->SleepMs(1000);
}

//  Did the upload succeed?
if ($ftp->get_AsyncSuccess() == true) {
    print 'File Uploaded!' . "\n";
}
else {
    //  The error information for asynchronous ops
    //  is in AsyncLog as opposed to LastErrorText
    print $ftp->asyncLog() . "\n";
}

$ftp->Disconnect();
?>